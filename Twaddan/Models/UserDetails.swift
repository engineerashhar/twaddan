//
//  UserDetails.swift
//  Twaddan
//
//  Created by Spine on 18/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation

struct UserDetails :Identifiable,Codable{
    var id :String = ""
    var NAME:String = ""
    var LNAME:String = ""
    var MOBILE : String = ""
    //    @DocumentID  var id:String?
    //    @ServerTimestamp   var createdTime:Timestamp?
}
