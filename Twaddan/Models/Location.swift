//
//  Location.swift
//  Twaddan
//
//  Created by Spine on 19/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation


class Location{
    func ClearLocDatas()  {
        UserDefaults.standard.set("",forKey: "AHOME")
        UserDefaults.standard.set("",forKey: "ASTREET")
        UserDefaults.standard.set("",forKey: "AFLOOR")
        UserDefaults.standard.set("",forKey: "AOFFICE")
        UserDefaults.standard.set("",forKey: "ADNOTE")
    }
}
