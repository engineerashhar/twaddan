//
//  UserSettings.swift
//  Twaddan
//
//  Created by Spine on 08/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation


class UserSettings: ObservableObject {
    @Published var loggedIn = false
    @Published var Uid: String = "0"
    @Published var Aid : String = UserDefaults.standard.string(forKey: "Aid") ?? "0"
    @Published var Name : String = "0"
    @Published var Email : String = "0"
    @Published var VStatus : Bool = true
    
    
    
}
