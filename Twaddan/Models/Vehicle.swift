//
//  Vehicle.swift
//  Twaddan
//
//  Created by Spine on 17/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation
import FirebaseFirestore
import FirebaseDatabase

struct Vehicle :Identifiable,Codable{
    var id :String =  ""
    var name:String
    var VehicleNumber:String
    //    @DocumentID  var id:String?
    //    @ServerTimestamp   var createdTime:Timestamp?
}

#if DEBUG
let testDataVehicle=[
    Vehicle( id: "1",name: "V1",VehicleNumber: ""),
    Vehicle( id: "2",name: "V2",VehicleNumber: ""),
    Vehicle( id: "3",name: "V3",VehicleNumber: ""),
    Vehicle( id: "4",name: "V4",VehicleNumber: ""),
    Vehicle( id: "5",name: "V5",VehicleNumber: ""),
]


#endif
