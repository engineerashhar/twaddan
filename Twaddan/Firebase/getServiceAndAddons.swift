//
//  getServiceAndAddons.swift
//  Twaddan
//
//  Created by Spine on 31/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation
import FirebaseDatabase

class getServiceAndAddons{
    
    
    func getServiceData( Snap : DataSnapshot,Vechicle : String, id : Int) -> [Services] {
        
        
        var services = [Services]()
        
        for child in Snap.childSnapshot(forPath: "services/\(Vechicle)").children{
            let snapshot = child as! DataSnapshot
            
            //  let snapshot = s.value
            //as! [String: Any]
            let service :Services = Services(id: "\(snapshot.key)\(id)",serviceName: String(describing :  (snapshot.childSnapshot(forPath:"name".localized()).value)!),serviceNameO: String(describing :  (snapshot.childSnapshot(forPath:"name_ar".localized()).value)!), serviceDesc: String(describing :  (snapshot.childSnapshot(forPath:"description".localized()).value)!),serviceDescO:String(describing :  (snapshot.childSnapshot(forPath:"description_ar".localized()).value)!) ,ServiceImage: String(describing :  (snapshot.childSnapshot(forPath:"image_url").value)!),ServicePrice: String(describing :  (snapshot.childSnapshot(forPath:"price").value)!),ServiceTime: String(describing :  (snapshot.childSnapshot(forPath:"time_required").value)!),ServiceVehicle: String(id))
            
            services.append(service)
            
            //let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            // ...
            
            //           var service :Services = Services(id: "", serviceName: "", serviceDesc: "", ServiceImage: "")
            //
            //
            //
            //                     service.id = snapshot.key
            //                     service.serviceName = String(describing :  (snapshot.childSnapshot(forPath:Localize(key:"name")).value)!)
            //                     service.serviceDesc = String(describing :  (snapshot.childSnapshot(forPath:Localize(key:"name")).value)!)
            //                     service.ServiceImage = String(describing :  (snapshot.childSnapshot(forPath:Localize(key:"name")).value)!)
        }
        return services
        
        
    }
    
    
    func getAddondata( Snap : DataSnapshot,id:Int) -> [Addons] {
        
        
        var addons = [Addons]()
        
        for child in Snap.childSnapshot(forPath: "add_on_services").children{
            let snapshot = child as! DataSnapshot
            
            //  let snapshot = s.value
            //as! [String: Any]
            let addon :Addons = Addons(id: "\(snapshot.key)\(id)",addonName: String(describing :  (snapshot.childSnapshot(forPath:"name".localized()).value)!),addonNameO:String(describing :  (snapshot.childSnapshot(forPath:"name_ar".localized()).value)!) ,  addonDesc:  String(describing :  (snapshot.childSnapshot(forPath:"description".localized()).value ?? "")), addonDescO: String(describing :  (snapshot.childSnapshot(forPath:"description_ar".localized()).value ?? "")),addonImage: String(describing :  (snapshot.childSnapshot(forPath:"image_url").value)!),addonPrice: String(describing :  (snapshot.childSnapshot(forPath:"price").value)!),addonVehicle:String(id) )
            
            addons.append(addon)
            
            //let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            // ...
            
            //           var service :Services = Services(id: "", serviceName: "", serviceDesc: "", ServiceImage: "")
            //
            //
            //
            //                     service.id = snapshot.key
            //                     service.serviceName = String(describing :  (snapshot.childSnapshot(forPath:Localize(key:"name")).value)!)
            //                     service.serviceDesc = String(describing :  (snapshot.childSnapshot(forPath:Localize(key:"name")).value)!)
            //                     service.ServiceImage = String(describing :  (snapshot.childSnapshot(forPath:Localize(key:"name")).value)!)
        }
        return addons
        
        
    }
    
}
