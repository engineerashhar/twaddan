//
//  FindOffer.swift
//  Twaddan
//
//  Created by Spine on 31/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation
import FirebaseDatabase


class FindOffer :ObservableObject{
    
    let ref = Database.database().reference()
    @Published var settings = UserSettings()
    
    @Published var Offers : [Offer] = [Offer]()
    @Published var VehicleDriverLink : [VehicleDriver] = [VehicleDriver]()
    
    
    init(){
        loadData()
    }
    
    
    func loadData(){
        
        
        //  self.ref.child("service_providers").keepSynced(true)
        
        //            print("Home Page10")
        
        self.ref.child("service_providers").observeSingleEvent(of: DataEventType.value, with: {
            
            (A) in
            
            
            self.VehicleDriverLink.removeAll()
            self.Offers.removeAll()
            
            
            for B in A.children {
                let snapshot = B as! DataSnapshot
                
                for Offers in snapshot.childSnapshot(forPath: "offers").children {
                    let snap = Offers as! DataSnapshot
                    let O : Offer = Offer(id: Date().timeIntervalSince1970 , OfferImage: String(describing :  (snap.childSnapshot(forPath:"image").value)!), ServiceProviderID: snapshot.key,OfferCode:String(describing :  (snap.childSnapshot(forPath:"promo_code").value)!),Desc:String(describing :  (snap.childSnapshot(forPath:"description".localized()).value)!),TC:String(describing :  (snap.childSnapshot(forPath:"terms_and_conditions".localized()).value )!)
                                          ,Percentage:String(describing :  (snap.childSnapshot(forPath:"percentage").value )!),OfferName: snap.key, show: false)
                    self.Offers.append(O)
                    
                    
                    
                }
                
                for V in snapshot.childSnapshot(forPath: "vehicles").children {
                    let Ve = V as! DataSnapshot
                    let Vehicle = VehicleDriver(VehicleNumber: Ve.key, DriverID: String(describing :  (Ve.childSnapshot(forPath:"driver").value ?? "" )))
                    
                    self.VehicleDriverLink.append(Vehicle)
                }
                
                
                
            }
            
            
            
            
            
        })
        
    }
    
    struct VehicleDriver{
        
        var VehicleNumber:String
        var DriverID:String
    }
    
}
