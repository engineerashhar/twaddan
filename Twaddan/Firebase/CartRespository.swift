//
//  CartRespository.swift
//  Twaddan
//
//  Created by Spine on 17/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation
import FirebaseDatabase
class CartRespository : ObservableObject{
    
    @Published var AllVehicles = [Vehicle]()
    @Published var settings = UserSettings()
    let ref = Database.database().reference()
    
    @Published var userDetails = UserDetails(id: "", NAME: "", LNAME: "", MOBILE: "")
    
    
    
    init(){
        
        readVehicles()
        readUserInfo()
    }
    func readVehicles() {
        
        
        //    print("Selected \(")
        
        
        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").observeSingleEvent(of: DataEventType.value) { (datasnapshot) in
            
            
            self.AllVehicles.removeAll()
            //
            for ID in datasnapshot.childSnapshot(forPath: "Vehicles").children {
                let id = ID as! DataSnapshot
                //
                var Services : String = ""
                
                for SERVICE in id.childSnapshot(forPath:"services").children {
                    let serv_snap = SERVICE as! DataSnapshot
                    Services.append(String(describing :  (serv_snap.childSnapshot(forPath:"name").value)!)
                    )
                    Services.append(" ")
                }
                
                let name : String = Services +
                    " for " + String(describing :  (id.childSnapshot(forPath:"Vehicle").value)!) + " with  " + String(describing :  (datasnapshot.childSnapshot(forPath:"ServiceProvider").value)!)
                
                // var Number : String = ""
                //                           if(self.VNumbers[id.key] != nil){
                //                               Number = self.VNumbers[id.key]!
                //                           }
                var VNumber : String = ""
                
                if(id.childSnapshot(forPath:"Vehicle Number").exists()){
                    VNumber=String(describing :  (id.childSnapshot(forPath:"Vehicle Number").value)! )
                    
                }
                
                let vehicle : Vehicle = Vehicle(id: id.key, name: name, VehicleNumber:VNumber)
                
                self.AllVehicles.append(vehicle)
                
            }
            
            
        }
        
        
        
        
    }
    
    
    func UpdateVehicle(vehicle:Vehicle) {
        
        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(vehicle.id).child("Vehicle Number").setValue(vehicle.VehicleNumber)
    }
    func UpdateUser(userDetails:UserDetails) {
        
        self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("personal_details/name").setValue(userDetails.NAME)
        self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("personal_details/phoneNumber").setValue(userDetails.MOBILE)
    }
    
    func readUserInfo() {
        //  self.cartFetch.readVehicles(settings: self.settings)
        
        self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").observeSingleEvent(of: DataEventType.value) { (datasnapshot) in
            
            
            if(datasnapshot.exists()){
                
                self.userDetails = UserDetails(id: datasnapshot.key, NAME: String(describing:(datasnapshot.childSnapshot(forPath: "personal_details/name").value)!), LNAME: String(describing:(datasnapshot.childSnapshot(forPath: "personal_details/lastName").value)!), MOBILE: String(describing:(datasnapshot.childSnapshot(forPath: "personal_details/phoneNumber").value)!))
                
                //                                     self.NAME =
                //                                     self.LNAME =
                //                                     self.MOBILE =
                //   NOTE : String = ""
                // ADDRESS : String = ""
                
                
            }
            
            
        }    }
    
}
