//
//  ActiveOrderFetch.swift
//  Twaddan
//
//  Created by Spine on 29/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation
import FirebaseDatabase


class ActiveOrderFetch :ObservableObject{
    
    let ref = Database.database().reference()
    @Published var settings = UserSettings()
    
    @Published var ActiveOrderID:[String] = [String]();
    
    init(){
        loadData()
    }
    
    
    func loadData(){
        
        self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("active_orders").child("order_ids").observe(DataEventType.value, with: { Snaps in
            
            self.ActiveOrderID.removeAll();
            
            for OrderID in Snaps.children {
                let orderids = OrderID as! DataSnapshot
                
                
                self.ActiveOrderID.append(String(describing :  (orderids.key )))
                
                
            }
            
        })
        
    }
}
