//
//  BackImageLinker.swift
//  Twaddan
//
//  Created by Spine on 02/05/20.
//  Copyright © 2020 spine. All rights reserved.
//


import Foundation
import FirebaseDatabase


class BackImageLinker :ObservableObject{
    let ref = Database.database().reference()
    @Published var url:String="";
    
    init(){
        loadData()
        
    }
    
    
    func loadData(){
        
        ref.child("twaddan_admin").child("background_image_url").observe(DataEventType.value, with: { Snaps in
            let readit = Snaps.value as! String
            
            self.url=readit
        })
        
    }
}
