//
//  CartFetch.swift
//  Twaddan
//
//  Created by Spine on 12/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation


import FirebaseDatabase
//import SwiftUI


class CartFetch : ObservableObject{
    
    @Published var AllVehicles = [Vehicle]()
    
    @Published var carts = [CartStruct]()
    
    @Published var settings = UserSettings()
    
    
    @Published var selectedServiceProvider:SelectedServiceProvider = SelectedServiceProvider()
    
    
    let ref = Database.database().reference()
    // let settings = UserSettings()
    @Published var VNumbers : [String: String] = [String: String] ()
    
    //  let Aid : String = UserDefaults.standard.string(forKey: "Aid")!
    
    
    init(){
        
        
        readCart()
        
        //  loadData()
    }
    
    func loadData (){}
    
    
    func setVNumbers(key:String , Value:String) {
        
        VNumbers = [key : Value]
        
    }
    
    //    {
    //       print("Started 2 2343543 ")
    //        ref.child("Cart").child(Aid).observe(DataEventType.value, with: {
    //
    //             (Msnapshot) in
    //
    //             self.carts.removeAll()
    //
    //
    //
    //            for ID in Msnapshot.children {
    //                  let id = ID as! DataSnapshot
    //
    //
    //                var addonslist = [Addons]()
    //            var servicelist = [Services]()
    //
    //
    //
    //                for SERVICE in id.childSnapshot(forPath:"services").children {
    //                    let serv_snap = SERVICE as! DataSnapshot
    //
    //                    let service : Services = Services(id: serv_snap.key,
    //                                                      serviceName: String(describing :  (serv_snap.childSnapshot(forPath:Localize(key:"name")).value)!),
    //                                                      serviceDesc: String(describing :  (serv_snap.childSnapshot(forPath:Localize(key:"description")).value)!),
    //                                                      ServiceImage: String(describing :  (serv_snap.childSnapshot(forPath:"image_url").value)!),
    //                                                      ServicePrice: String(describing :  (serv_snap.childSnapshot(forPath:"price").value)!))
    //                    servicelist.append(service)
    //                }
    //
    //                for ADDONS in id.childSnapshot(forPath:"addons").children{
    //
    //
    //                                 let add_snap = ADDONS as! DataSnapshot
    //
    //
    //                    let addon : Addons = Addons(id: add_snap.key,
    //                                                addonName: String(describing :  (add_snap.childSnapshot(forPath:Localize(key:"name")).value)!),
    //                                                addonImage: String(describing :  (add_snap.childSnapshot(forPath:"image_url").value)!),
    //                                                addonPrice: String(describing :  (add_snap.childSnapshot(forPath:"price").value)!))
    //                    addonslist.append(addon)
    //
    //                }
    //
    //                let cart : CartStruct = CartStruct(id:id.key , addons:addonslist , services: servicelist , vehicle: String(describing :  (id.childSnapshot(forPath:"Vehicle").value)!),ServiceProvider: String(describing :  (id.childSnapshot(forPath:"ServiceProvider").value)!),ServiceProvider_id: String(describing :  (id.childSnapshot(forPath:"ServiceProvider_id").value)!),ServiceProvider_image: String(describing :  (id.childSnapshot(forPath:"ServiceProvider_image").value)!))
    //
    //                print("Started 3 ")
    //                self.carts.append(cart)
    //
    //            }
    //        })
    //    }
    
    func readVehicles(settings : UserSettings) {
        
        
        print(UserDefaults.standard.string(forKey: "Aid") ?? "0")
        
        
        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").observeSingleEvent(of: DataEventType.value) { (datasnapshot) in
            
            
            self.AllVehicles.removeAll()
            //
            for ID in datasnapshot.children {
                let id = ID as! DataSnapshot
                //
                var Services : String = ""
                
                for SERVICE in id.childSnapshot(forPath:"services").children {
                    let serv_snap = SERVICE as! DataSnapshot
                    Services.append(String(describing :  (serv_snap.childSnapshot(forPath:"name".localized()).value)!)
                    )
                    Services.append(" ")
                }
                
                let name : String = Services +
                    " for " + String(describing :  (id.childSnapshot(forPath:"Vehicle").value)!) + " with  " + String(describing :  (datasnapshot.childSnapshot(forPath:"ServiceProvider").value)!)
                
                var Number : String = ""
                if(self.VNumbers[id.key] != nil){
                    Number = self.VNumbers[id.key]!
                }
                
                let vehicle : Vehicle = Vehicle(id: id.key, name: name, VehicleNumber:Number )
                
                self.AllVehicles.append(vehicle)
                
            }
            
            
        }
        
        
        
        
    }
    
    func UpdateVehicle(vehicle:Vehicle) {
        
        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(vehicle.id).child("Vehicle Number").setValue(vehicle.VehicleNumber)
    }
    
    //    func readCart(settings : UserSettings) {
    //        print("Started 2 \(settings.Aid)")
    //
    //        self.settings = settings
    //
    //        ref.child("Cart").child(settings.Aid).observe(DataEventType.value, with: {
    //
    //             (Msnapshot) in
    //
    //             self.carts.removeAll()
    //            self.selectedServiceProvider =   SelectedServiceProvider(
    //                                         ServiceProvider: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider").value)!),
    //                                                                         ServiceProvider_id: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider_id").value)!),
    //                                                                         ServiceProvider_image: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider_image").value)!),DriverID: String(describing :  (Msnapshot.childSnapshot(forPath:"DriverID").value)!),TimeToArrive : Msnapshot.childSnapshot(forPath:"ETA").value as! Double)
    //
    //
    //            for ID in Msnapshot.childSnapshot(forPath:"Vehicles").children {
    //                  let id = ID as! DataSnapshot
    //
    //
    //                var addonslist = [Addons]()
    //            var servicelist = [Services]()
    //
    //
    //
    //                for SERVICE in id.childSnapshot(forPath:"services").children {
    //                    let serv_snap = SERVICE as! DataSnapshot
    //
    //                    let service : Services = Services(id: serv_snap.key,
    //                                                      serviceName: String(describing :  (serv_snap.childSnapshot(forPath:Localize(key:"name")).value)!),
    //                                                      serviceDesc: String(describing :  (serv_snap.childSnapshot(forPath:Localize(key:"description")).value)!),
    //                                                      ServiceImage: String(describing :  (serv_snap.childSnapshot(forPath:"image_url").value)!),
    //                                                      ServicePrice: String(describing :  (serv_snap.childSnapshot(forPath:"price").value)!))
    //                    servicelist.append(service)
    //                }
    //
    //                for ADDONS in id.childSnapshot(forPath:"addons").children{
    //
    //
    //                                 let add_snap = ADDONS as! DataSnapshot
    //
    //
    //                    let addon : Addons = Addons(id: add_snap.key,
    //                                                addonName: String(describing :  (add_snap.childSnapshot(forPath:Localize(key:"name")).value)!),
    //                                                addonImage: String(describing :  (add_snap.childSnapshot(forPath:"image_url").value)!),
    //                                                addonPrice: String(describing :  (add_snap.childSnapshot(forPath:"price").value)!))
    //                    addonslist.append(addon)
    //
    //                }
    //
    //                let cart : CartStruct = CartStruct(id:id.key , addons:addonslist , services: servicelist , vehicle: String(describing :  (id.childSnapshot(forPath:"Vehicle").value)!)
    ////                    ,ServiceProvider: String(describing :  (id.childSnapshot(forPath:"ServiceProvider").value)!)
    ////                    ,ServiceProvider_id: String(describing :  (id.childSnapshot(forPath:"ServiceProvider_id").value)!)
    ////                    ,ServiceProvider_image: String(describing :  (id.childSnapshot(forPath:"ServiceProvider_image").value)!),
    ////                     DriverID: String(describing :  (id.childSnapshot(forPath:"DriverID").value)!)
    //                )
    //
    //                print("Started 3 \(self.carts.count)")
    //                self.carts.append(cart)
    //
    //            }
    //        })
    //    }
    
    
    
    func readCart()  {
        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").observe(DataEventType.value, with: {
            
            (Msnapshot) in
            
            self.carts.removeAll()
            
            
            var TA : Double = 1000000
            if( Msnapshot.childSnapshot(forPath:"ETA").exists() ) {
                
                TA = Msnapshot.childSnapshot(forPath:"ETA").value! as! Double
            }
            
            
            self.selectedServiceProvider =   SelectedServiceProvider(
                ServiceProvider: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider").value)!),
                ServiceProvider_id: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider_id").value)!),
                ServiceProvider_image: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider_image").value)!),DriverID: String(describing :  (Msnapshot.childSnapshot(forPath:"DriverID").value)!),TimeToArrive : TA)
            
            
            for ID in Msnapshot.childSnapshot(forPath:"Vehicles").children {
                let id = ID as! DataSnapshot
                
                
                var addonslist = [Addons]()
                var servicelist = [Services]()
                
                
                
                for SERVICE in id.childSnapshot(forPath:"services").children {
                    let serv_snap = SERVICE as! DataSnapshot
                    
                    let service : Services = Services(id: serv_snap.key,
                                                      serviceName: String(describing :  (serv_snap.childSnapshot(forPath:"name".localized()).value)!),serviceNameO: String(describing :  (serv_snap.childSnapshot(forPath:"name_ar".localized()).value)!),
                                                      serviceDesc: String(describing :  (serv_snap.childSnapshot(forPath:"description".localized()).value)!),
                                                      serviceDescO: String(describing :  (serv_snap.childSnapshot(forPath:"description_ar".localized()).value)!),
                                                      ServiceImage: String(describing :  (serv_snap.childSnapshot(forPath:"image_url").value)!),
                                                      ServicePrice: String(describing :  (serv_snap.childSnapshot(forPath:"price").value)!),ServiceTime:  String(describing :  (serv_snap.childSnapshot(forPath:"time_required").value)!))
                    servicelist.append(service)
                }
                
                for ADDONS in id.childSnapshot(forPath:"addons").children{
                    
                    
                    let add_snap = ADDONS as! DataSnapshot
                    
                    
                    let addon : Addons = Addons(id: add_snap.key,
                                                addonName: String(describing :  (add_snap.childSnapshot(forPath:"name".localized()).value)!),addonNameO:String(describing :  (add_snap.childSnapshot(forPath:"name_ar".localized()).value)!) ,addonDesc:  String(describing :  (add_snap.childSnapshot(forPath:"description".localized()).value ?? "")),addonDescO: String(describing :  (add_snap.childSnapshot(forPath:"description_ar".localized()).value ?? "")),
                                                addonImage: String(describing :  (add_snap.childSnapshot(forPath:"image_url").value)!),
                                                addonPrice: String(describing :  (add_snap.childSnapshot(forPath:"price").value)!))
                    addonslist.append(addon)
                    
                }
                
                let cart : CartStruct = CartStruct(id:id.key , addons:addonslist , services: servicelist , vehicle: String(describing :  (id.childSnapshot(forPath:"Vehicle").value)!), vehicleName: String(describing :  (id.childSnapshot(forPath:"VehicleName").value)!)
                                                   //                        ,ServiceProvider: String(describing :  (id.childSnapshot(forPath:"ServiceProvider").value)!),
                                                   //                                                        ServiceProvider_id: String(describing :  (id.childSnapshot(forPath:"ServiceProvider_id").value)!),
                                                   //                                                        ServiceProvider_image: String(describing :  (id.childSnapshot(forPath:"ServiceProvider_image").value)!),DriverID: String(describing :  (id.childSnapshot(forPath:"DriverID").value)!)
                )
                
                //                     print("Started 3 \(self.carts.count)")
                self.carts.append(cart)
                
            }
        })
        
    }
    
    
    
    
}

struct SelectedServiceProvider {
    
    //var id : String = ""
    var ServiceProvider : String = ""
    var ServiceProvider_id : String = ""
    var ServiceProvider_image : String = ""
    var DriverID : String = ""
    var TimeToArrive : Double = 0.0
    
}
struct CartStruct : Identifiable,Equatable{
    
    var id : String
    var addons : [Addons]
    var services : [Services]
    var vehicle : String
    var vehicleName : String
    //    var ServiceProvider : String
    //    var ServiceProvider_id : String
    //    var ServiceProvider_image : String
    //    var DriverID : String
    
}

////
////  CartFetch.swift
////  Twaddan
////
////  Created by Spine on 12/05/20.
////  Copyright © 2020 spine. All rights reserved.
////
//
//import Foundation
//
//
//import FirebaseDatabase
////import SwiftUI
//
//
//class CartFetch : ObservableObject{
//
//    @Published var AllVehicles = [Vehicle]()
//
//    @Published var carts = [CartStruct]()
//
//    @Published var settings = UserSettings()
//
//
//    @Published var SelectedServiceProvider = SelectedServiceProvider()
//
//
//    let ref = Database.database().reference()
//   // let settings = UserSettings()
//    @Published var VNumbers : [String: String] = [String: String] ()
//
//  //  let Aid : String = UserDefaults.standard.string(forKey: "Aid")!
//
//
//    init(){
//
//
//        readCart()
//
//      //  loadData()
//    }
//
//    func loadData (){}
//
//
//    func setVNumbers(key:String , Value:String) {
//
//        VNumbers = [key : Value]
//
//    }
//
////    {
////       print("Started 2 2343543 ")
////        ref.child("Cart").child(Aid).observe(DataEventType.value, with: {
////
////             (Msnapshot) in
////
////             self.carts.removeAll()
////
////
////
////            for ID in Msnapshot.children {
////                  let id = ID as! DataSnapshot
////
////
////                var addonslist = [Addons]()
////            var servicelist = [Services]()
////
////
////
////                for SERVICE in id.childSnapshot(forPath:"services").children {
////                    let serv_snap = SERVICE as! DataSnapshot
////
////                    let service : Services = Services(id: serv_snap.key,
////                                                      serviceName: String(describing :  (serv_snap.childSnapshot(forPath:Localize(key:"name")).value)!),
////                                                      serviceDesc: String(describing :  (serv_snap.childSnapshot(forPath:Localize(key:"description")).value)!),
////                                                      ServiceImage: String(describing :  (serv_snap.childSnapshot(forPath:"image_url").value)!),
////                                                      ServicePrice: String(describing :  (serv_snap.childSnapshot(forPath:"price").value)!))
////                    servicelist.append(service)
////                }
////
////                for ADDONS in id.childSnapshot(forPath:"addons").children{
////
////
////                                 let add_snap = ADDONS as! DataSnapshot
////
////
////                    let addon : Addons = Addons(id: add_snap.key,
////                                                addonName: String(describing :  (add_snap.childSnapshot(forPath:Localize(key:"name")).value)!),
////                                                addonImage: String(describing :  (add_snap.childSnapshot(forPath:"image_url").value)!),
////                                                addonPrice: String(describing :  (add_snap.childSnapshot(forPath:"price").value)!))
////                    addonslist.append(addon)
////
////                }
////
////                let cart : CartStruct = CartStruct(id:id.key , addons:addonslist , services: servicelist , vehicle: String(describing :  (id.childSnapshot(forPath:"Vehicle").value)!),ServiceProvider: String(describing :  (id.childSnapshot(forPath:"ServiceProvider").value)!),ServiceProvider_id: String(describing :  (id.childSnapshot(forPath:"ServiceProvider_id").value)!),ServiceProvider_image: String(describing :  (id.childSnapshot(forPath:"ServiceProvider_image").value)!))
////
////                print("Started 3 ")
////                self.carts.append(cart)
////
////            }
////        })
////    }
//
//    func readVehicles(settings : UserSettings) {
//
//
//        print(self.settings.Aid)
//
//
//                self.ref.child("Cart").child(self.settings.Aid).observeSingleEvent(of: DataEventType.value) { (datasnapshot) in
//
//
//                    self.AllVehicles.removeAll()
//    //
//                    for ID in datasnapshot.children {
//                                     let id = ID as! DataSnapshot
//    //
//                        var Services : String = ""
//
//                        for SERVICE in id.childSnapshot(forPath:"services").children {
//                                           let serv_snap = SERVICE as! DataSnapshot
//                            Services.append(String(describing :  (serv_snap.childSnapshot(forPath:Localize(key:"name")).value)!)
//                            )
//                            Services.append(" ")
//                        }
//
//                        let name : String = Services +
//                        " for " + String(describing :  (id.childSnapshot(forPath:"Vehicle").value)!) + " with  " + String(describing :  (id.childSnapshot(forPath:"ServiceProvider").value)!)
//
//                        var Number : String = ""
//                        if(self.VNumbers[id.key] != nil){
//                            Number = self.VNumbers[id.key]!
//                        }
//
//                        let vehicle : Vehicle = Vehicle(id: id.key, name: name, VehicleNumber:Number )
//
//                        self.AllVehicles.append(vehicle)
//
//                    }
//
//
//                }
//
//
//
//
//            }
//
//    func UpdateVehicle(vehicle:Vehicle) {
//
//        self.ref.child("Cart").child("deliciamlpm_gmail_com").child(vehicle.id).child("Vehicle Number").setValue(vehicle.VehicleNumber)
//    }
//
//    func readCart(settings : UserSettings) {
//        print("Started 2 \(settings.Aid)")
//
//        self.settings = settings
//
//        ref.child("Cart").child(settings.Aid).observe(DataEventType.value, with: {
//
//             (Msnapshot) in
//
//             self.carts.removeAll()
//
//
//
//            for ID in Msnapshot.children {
//                  let id = ID as! DataSnapshot
//
//
//                var addonslist = [Addons]()
//            var servicelist = [Services]()
//
//
//
//                for SERVICE in id.childSnapshot(forPath:"services").children {
//                    let serv_snap = SERVICE as! DataSnapshot
//
//                    let service : Services = Services(id: serv_snap.key,
//                                                      serviceName: String(describing :  (serv_snap.childSnapshot(forPath:Localize(key:"name")).value)!),
//                                                      serviceDesc: String(describing :  (serv_snap.childSnapshot(forPath:Localize(key:"description")).value)!),
//                                                      ServiceImage: String(describing :  (serv_snap.childSnapshot(forPath:"image_url").value)!),
//                                                      ServicePrice: String(describing :  (serv_snap.childSnapshot(forPath:"price").value)!))
//                    servicelist.append(service)
//                }
//
//                for ADDONS in id.childSnapshot(forPath:"addons").children{
//
//
//                                 let add_snap = ADDONS as! DataSnapshot
//
//
//                    let addon : Addons = Addons(id: add_snap.key,
//                                                addonName: String(describing :  (add_snap.childSnapshot(forPath:Localize(key:"name")).value)!),
//                                                addonImage: String(describing :  (add_snap.childSnapshot(forPath:"image_url").value)!),
//                                                addonPrice: String(describing :  (add_snap.childSnapshot(forPath:"price").value)!))
//                    addonslist.append(addon)
//
//                }
//
//                let cart : CartStruct = CartStruct(id:id.key , addons:addonslist , services: servicelist , vehicle: String(describing :  (id.childSnapshot(forPath:"Vehicle").value)!)
////                    ,ServiceProvider: String(describing :  (id.childSnapshot(forPath:"ServiceProvider").value)!)
////                    ,ServiceProvider_id: String(describing :  (id.childSnapshot(forPath:"ServiceProvider_id").value)!)
////                    ,ServiceProvider_image: String(describing :  (id.childSnapshot(forPath:"ServiceProvider_image").value)!),
////                     DriverID: String(describing :  (id.childSnapshot(forPath:"DriverID").value)!)
//                )
//
//                print("Started 3 \(self.carts.count)")
//                self.carts.append(cart)
//
//            }
//        })
//    }
//
//
//
//    func readCart()  {
//                    self.ref.child("Cart").child(self.settings.Aid).observe(DataEventType.value, with: {
//
//                  (Msnapshot) in
//
//                  self.carts.removeAll()
//
//
//                        self.SelectedServiceProvider =
//
//                 for ID in Msnapshot.children {
//                       let id = ID as! DataSnapshot
//
//
//                     var addonslist = [Addons]()
//                 var servicelist = [Services]()
//
//
//
//                     for SERVICE in id.childSnapshot(forPath:"services").children {
//                         let serv_snap = SERVICE as! DataSnapshot
//
//                         let service : Services = Services(id: serv_snap.key,
//                                                           serviceName: String(describing :  (serv_snap.childSnapshot(forPath:Localize(key:"name")).value)!),
//                                                           serviceDesc: String(describing :  (serv_snap.childSnapshot(forPath:Localize(key:"description")).value)!),
//                                                           ServiceImage: String(describing :  (serv_snap.childSnapshot(forPath:"image_url").value)!),
//                                                           ServicePrice: String(describing :  (serv_snap.childSnapshot(forPath:"price").value)!))
//                         servicelist.append(service)
//                     }
//
//                     for ADDONS in id.childSnapshot(forPath:"addons").children{
//
//
//                                      let add_snap = ADDONS as! DataSnapshot
//
//
//                         let addon : Addons = Addons(id: add_snap.key,
//                                                     addonName: String(describing :  (add_snap.childSnapshot(forPath:Localize(key:"name")).value)!),
//                                                     addonImage: String(describing :  (add_snap.childSnapshot(forPath:"image_url").value)!),
//                                                     addonPrice: String(describing :  (add_snap.childSnapshot(forPath:"price").value)!))
//                         addonslist.append(addon)
//
//                     }
//
//                     let cart : CartStruct = CartStruct(id:id.key , addons:addonslist , services: servicelist , vehicle: String(describing :  (id.childSnapshot(forPath:"Vehicle").value)!)
////                        ,ServiceProvider: String(describing :  (id.childSnapshot(forPath:"ServiceProvider").value)!),
////                                                        ServiceProvider_id: String(describing :  (id.childSnapshot(forPath:"ServiceProvider_id").value)!),
////                                                        ServiceProvider_image: String(describing :  (id.childSnapshot(forPath:"ServiceProvider_image").value)!),DriverID: String(describing :  (id.childSnapshot(forPath:"DriverID").value)!)
//                    )
//
//                     print("Started 3 \(self.carts.count)")
//                     self.carts.append(cart)
//
//                 }
//             })
//
//    }
//
//
//}
//struct CartStruct : Identifiable{
//
//    var id : String
//    var addons : [Addons]
//    var services : [Services]
//    var vehicle : String
////    var ServiceProvider : String
////    var ServiceProvider_id : String
////    var ServiceProvider_image : String
////    var DriverID : String
//
//}
//struct SelectedServiceProvider : Identifiable{
//
//    var id : String
//     var ServiceProvider : String
//    var ServiceProvider_id : String
//    var ServiceProvider_image : String
//       var DriverID : String
//
//}
