//
//  LiveLocationFetch.swift
//  Twaddan
//
//  Created by Spine on 30/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation
import FirebaseDatabase
import MapKit


class LiveLocationFetch :ObservableObject{
    
    let ref = Database.database().reference()
    @Published var driver2D : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 11.050976, longitude: 76.071098)
    
    @Published              var user2D  : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 11.050980, longitude: 76.101098)
    
    //    init(OrderID:String){
    //
    //        loadData(OrderID: OrderID)
    //
    //    }
    
    
    func loadData(OrderID:String){
        
        self.ref.child("orders").child("all_orders").child(OrderID).observe(DataEventType.value) { (datasnapshot) in
            
            self.user2D = CLLocationCoordinate2D(latitude:      Double(String(describing :  (datasnapshot.childSnapshot(forPath:"customer_latitude").value)!)) as! CLLocationDegrees
                                                 , longitude:      Double(String(describing :  (datasnapshot.childSnapshot(forPath:"customer_longitude").value)!)) as! CLLocationDegrees
            )
            self.ref.child("drivers").child(String(describing :  (datasnapshot.childSnapshot(forPath:"driver_id").value)!)).child("live_location").observe(DataEventType.value) { (locsnap) in
                
                
                self.driver2D = CLLocationCoordinate2D(latitude:      Double(String(describing :  (locsnap.childSnapshot(forPath:"latitude").value)!)) as! CLLocationDegrees
                                                       , longitude:      Double(String(describing :  (locsnap.childSnapshot(forPath:"longitude").value)!)) as! CLLocationDegrees
                )
            }
            
            //
            
            
        }
        
        
    }
}
