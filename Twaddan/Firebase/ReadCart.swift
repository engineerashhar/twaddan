//
//  ReadCart.swift
//  Twaddan
//
//  Created by Spine on 12/09/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation
import Firebase


class ReadCart :ObservableObject{
    let ref = Database.database().reference()
    @Published var carts = [CartStruct]()
    @Published var selectedServiceProvider = SelectedSP()
    
    
    init() {
        ReadIt()
    }
    
    
    func ReadIt()
    {
        
        //            let appearance = UINavigationBarAppearance()
        //
        //                         appearance.backgroundColor = UIColor(named:"backcolor2")
        ////             appearance.backgroundColor = .red
        //                  appearance.shadowColor = .black
        //
        //
        //                         UINavigationBar.appearance().standardAppearance = appearance
        //
        
        print("i was called")
        //self.cartFetch.readCart(settings: self.settings)
        
        //            print("Started 2 \(UserDefaults.standard.string(forKey: "Aid") ?? "0")")
        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").keepSynced(true)
        
        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").observe(DataEventType.value, with: {
            
            (Msnapshot) in
            
            
            print("read")
            if(Msnapshot.exists()){
                
                print("read2")
                self.ref.child("service_providers").child(String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider_id").value)!)).observeSingleEvent(of: DataEventType.value) { (snap) in
                    
                    print("read3")
                    
                    
                    self.carts.removeAll()
                    
                    self.selectedServiceProvider =   SelectedSP(
                        
                        ServiceProvider: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider").value)!),
                        ServiceProvider_id: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider_id").value)!),
                        ServiceProvider_image: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider_image").value)!),DriverID: String(describing :  (Msnapshot.childSnapshot(forPath:"DriverID").value)!),ETA :Msnapshot.childSnapshot(forPath:"ETA").value as? Double ?? 0.0 ,SPSnap: snap)
                    
                    
                    
                    for ID in Msnapshot.childSnapshot(forPath:"Vehicles").children {
                        let id = ID as! DataSnapshot
                        
                        
                        var addonslist = [Addons]()
                        var servicelist = [Services]()
                        
                        
                        
                        for SERVICE in id.childSnapshot(forPath:"services").children {
                            let serv_snap = SERVICE as! DataSnapshot
                            
                            let service : Services = Services(id: serv_snap.key,
                                                              serviceName: String(describing :  (serv_snap.childSnapshot(forPath:"name".localized()).value)!),
                                                              serviceNameO: String(describing :  (serv_snap.childSnapshot(forPath:"name_ar".localized()).value)!),
                                                              serviceDesc: String(describing :  (serv_snap.childSnapshot(forPath:"description".localized()).value)!),serviceDescO:String(describing :  (serv_snap.childSnapshot(forPath:"description_ar".localized()).value)!) ,
                                                              ServiceImage: String(describing :  (serv_snap.childSnapshot(forPath:"image_url").value)!),
                                                              ServicePrice: String(describing :  (serv_snap.childSnapshot(forPath:"price").value)!),ServiceTime: String(describing :  (serv_snap.childSnapshot(forPath:"time_required").value)!))
                            servicelist.append(service)
                        }
                        
                        for ADDONS in id.childSnapshot(forPath:"addons").children{
                            
                            
                            let add_snap = ADDONS as! DataSnapshot
                            
                            
                            let addon : Addons = Addons(id: add_snap.key,
                                                        addonName: String(describing :  (add_snap.childSnapshot(forPath:"name".localized()).value)!),addonNameO: String(describing :  (add_snap.childSnapshot(forPath:"name_ar".localized()).value)!),addonDesc:  String(describing :  (add_snap.childSnapshot(forPath:"description".localized()).value ?? "")),addonDescO: String(describing :  (add_snap.childSnapshot(forPath:"description_ar".localized()).value ?? "")) ,
                                                        addonImage: String(describing :  (add_snap.childSnapshot(forPath:"image_url").value)!),
                                                        addonPrice: String(describing :  (add_snap.childSnapshot(forPath:"price").value)!))
                            addonslist.append(addon)
                            
                        }
                        
                        let cart : CartStruct = CartStruct(id:id.key , addons:addonslist , services: servicelist , vehicle: String(describing :  (id.childSnapshot(forPath:"Vehicle").value)!), vehicleName: String(describing :( id.childSnapshot(forPath:"VehicleName").value)!)
                                                           //                            ,ServiceProvider: String(describing :  (id.childSnapshot(forPath:"ServiceProvider").value)!),ServiceProvider_id: String(describing :  (id.childSnapshot(forPath:"ServiceProvider_id").value)!),ServiceProvider_image: String(describing :  (id.childSnapshot(forPath:"ServiceProvider_image").value)!)
                                                           
                        )
                        
                        //                         print("Started 3 \(self.carts.count)")
                        self.carts.append(cart)
                        
                    }
                    
                    //                        if(Msnapshot.childSnapshot(forPath:"Vehicles").childrenCount==0){
                    //                                                 self.GoToBookDetails = true
                    //                                             }
                    
                }
            }
        })
        
        
        
        
    }
    
}



