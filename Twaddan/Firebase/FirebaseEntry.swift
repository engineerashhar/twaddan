//
//  FirebaseEntry.swift
//  Twaddan
//
//  Created by Spine on 12/09/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import Firebase

class FirebaseEntry :ObservableObject{
    let ref = Database.database().reference()
    @Published var url:String="";
    
    
    
    func CartEntry1(ForCartS:[DataS],ForCartA:[DataA],serviceSnap:DataSnapshot,SelectedDriver:String,ETA:Double){
        
        
        DispatchQueue.global(qos: .background).async {
            self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider").setValue(serviceSnap.childSnapshot(forPath:"personal_information/name".localized()).value!)
            self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("DriverID").setValue(SelectedDriver)
            if(ETA<10000000){
                self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ETA").setValue(ETA)
            }else{
                self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ETA").setValue(0)
            }
            self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider_id").setValue(serviceSnap.key)
            self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider_image").setValue(serviceSnap.childSnapshot(forPath:"personal_information/image").value!)
            //
            for Data in ForCartS{
                
                let node = String(Data.id+Int.random(in: 0 ... 1000000)).replacingOccurrences(of: ".", with: "")
                
                
                let service = Data.selectedService
                //                            for service in self.selectedServices{
                
                
                
                self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("services").child(service.id).setValue(service.getDict())
                //                            }
                
                
                
                
                let I:Int = Data.id
                
                let C = ForCartA.filter{
                    ($0.id == I ) }
                
                for addon in  C {
                    let addons = addon.selectedAddons
                    self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("addons").child(addons.id).setValue(addons.getDict())
                    
                }
                
                self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("Vehicle").setValue(Data.Vid)
                self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("VehicleName").setValue(Data.VName)
            }
            
            
        }
    }
}
