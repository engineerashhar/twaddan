////
////  SignInWithAppleDelegates.swift
////  Twaddan
////
////  Created by Spine on 10/08/20.
////  Copyright © 2020 spine. All rights reserved.
////
//
//import Foundation
//import AuthenticationServices
//import CryptoKit
//import FirebaseAuth
//
//
//class SignInWithAppleDelegates: ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding{
//    
//
//    
//          func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
//              return self.view.window!
//          }
//   
//    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
//        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
//          guard let nonce = currentNonce else {
//            fatalError("Invalid state: A login callback was received, but no login request was sent.")
//          }
//          guard let appleIDToken = appleIDCredential.identityToken else {
//            print("Unable to fetch identity token")
//            return
//          }
//          guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
//            print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
//            return
//          }
//          // Initialize a Firebase credential.
//          let credential = OAuthProvider.credential(withProviderID: "apple.com",
//                                                    idToken: idTokenString,
//                                                    rawNonce: nonce)
//          // Sign in with Firebase.
//          Auth.auth().signIn(with: credential) { (authResult, error) in
//            if (error != nil) {
//              // Error. If error.code == .MissingOrInvalidNonce, make sure
//              // you're sending the SHA256-hashed nonce as a hex string with
//              // your request to Apple.
//                print(error?.localizedDescription)
//              return
//            }
//            // User is signed in to Firebase with Apple.
//            // ...
//          }
//        }
//      }
//
//      func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
//        // Handle error.
//        print("Sign in with Apple errored: \(error)")
//      }
//
//    
//    
////    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
////        switch authorization.credential {
////        case let appleIDCredential as ASAuthorizationAppleIDCredential:
////
////            // Create an account in your system.
////            let userIdentifier = appleIDCredential.user
////            let fullName = appleIDCredential.fullName
////            let email = appleIDCredential.email
////
////            // For the purpose of this demo app, store the `userIdentifier` in the keychain.
//////            self.saveUserInKeychain(userIdentifier)
////
////            // For the purpose of this demo app, show the Apple ID credential information in the `ResultViewController`.
//////            self.showResultViewController(userIdentifier: userIdentifier, fullName: fullName, email: email)
////
////        case let passwordCredential as ASPasswordCredential:
////
////            // Sign in using an existing iCloud Keychain credential.
////            let username = passwordCredential.user
////            let password = passwordCredential.password
////
////            // For the purpose of this demo app, show the password credential as an alert.
////            DispatchQueue.main.async {
//////                self.showPasswordCredentialAlert(username: username, password: password)
////            }
////
////        default:
////            break
////        }
////    }
//    
//    
//    fileprivate var currentNonce: String?
//
//    @available(iOS 13, *)
//    func startSignInWithAppleFlow() {
//      let nonce = randomNonceString()
//      currentNonce = nonce
//      let appleIDProvider = ASAuthorizationAppleIDProvider()
//      let request = appleIDProvider.createRequest()
//      request.requestedScopes = [.fullName, .email]
//      request.nonce = sha256(nonce)
//
//      let authorizationController = ASAuthorizationController(authorizationRequests: [request])
//      authorizationController.delegate = self
//      authorizationController.presentationContextProvider = self
//      authorizationController.performRequests()
//    }
//
//    @available(iOS 13, *)
//    private func sha256(_ input: String) -> String {
//      let inputData = Data(input.utf8)
//      let hashedData = SHA256.hash(data: inputData)
//      let hashString = hashedData.compactMap {
//        return String(format: "%02x", $0)
//      }.joined()
//
//      return hashString
//    }
//    
//}
//
//
//private func randomNonceString(length: Int = 32) -> String {
//  precondition(length > 0)
//  let charset: Array<Character> =
//      Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
//  var result = ""
//  var remainingLength = length
//
//  while remainingLength > 0 {
//    let randoms: [UInt8] = (0 ..< 16).map { _ in
//      var random: UInt8 = 0
//      let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
//      if errorCode != errSecSuccess {
//        fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
//      }
//      return random
//    }
//
//    randoms.forEach { random in
//      if remainingLength == 0 {
//        return
//      }
//
//      if random < charset.count {
//        result.append(charset[Int(random)])
//        remainingLength -= 1
//      }
//    }
//  }
//
//  return result
//}
