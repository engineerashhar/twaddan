//
//  SceneDelegate.swift
//  Twaddan
//
//  Created by user170583 on 4/22/20.
//  Copyright © 2020 spine. All rights reserved.
//

import UIKit
import SwiftUI
import FirebaseDatabase
import FirebaseDynamicLinks

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    var userSettings = UserSettings()
    var vehicleItems = VehicleItems()
    
    @State var openVA : Bool = false
    
    func scene(_ scene: UIScene, continue userActivity: NSUserActivity) {
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
              let urlToOpen = userActivity.webpageURL else {
            return
        }
        
        
        
        
        //  print(urlToOpen.pathComponents[1])
        let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
            
            
            
            if let windowScene = scene as? UIWindowScene {
                let window = UIWindow(windowScene: windowScene)
                window.rootViewController = UIHostingController(rootView:
                                                                    
                                                                    Intermediate(spid: (URLComponents(url: (dynamiclink?.url)!, resolvingAgainstBaseURL: false)?.queryItems?.first(where: { $0.name == "sp" })?.value)!).environmentObject(self.userSettings).environmentObject(self.vehicleItems)
                                                                
                                                                //                BookingDetails(OpenCart:.constant(false) , GoToBookDetails: .constant(false), serviceSnap: DataSnapshot.init(), SelectedDriver: (URLComponents(url: (dynamiclink?.url)!, resolvingAgainstBaseURL: false)?.queryItems?.first(where: { $0.name == "sp" })?.value)!, ETA: -1,openVA: self.$openVA).environmentObject(self.userSettings).environmentObject(self.vehicleItems)
                )
                self.window = window
                window.makeKeyAndVisible()
            }
            //        print("123456789",dynamiclink.)
            // ...
        }
        
    }
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach vgvz2sw UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        
        // Create the SwiftUI view that provides the window contents.
        
        
        print("123")
        var contentView = SplashScreen() .environmentObject(self.vehicleItems).environment(\.colorScheme, .light).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
        ))
        
        //       if let userActivity = connectionOptions.userActivities.first {
        //          self.scene(scene, continue: userActivity)
        //        } else {
        //          self.scene(scene, openURLContexts: connectionOptions.urlContexts)
        //        }
        
        
        
        if let userActivity = connectionOptions.userActivities.first {
            self.scene(scene, continue: userActivity)
        }else{
            if let windowScene = scene as? UIWindowScene {
                let window = UIWindow(windowScene: windowScene)
                window.rootViewController = UIHostingController(rootView: contentView.environmentObject(userSettings).environmentObject(vehicleItems))
                self.window = window
                window.makeKeyAndVisible()
            }
        }
        
        //        if let urlContext = connectionOptions.urlContexts.first {
        //            if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: urlContext.url) {
        //
        //
        //
        //                if let windowScene = scene as? UIWindowScene {
        //                                              let window = UIWindow(windowScene: windowScene)
        //                                     window.rootViewController = UIHostingController(rootView: BookingDetails(OpenCart:.constant(false) , GoToBookDetails: .constant(false), serviceSnap: DataSnapshot.init(), SelectedDriver: (URLComponents(url: (dynamicLink.url)!, resolvingAgainstBaseURL: false)?.queryItems?.first(where: { $0.name == "sp" })?.value)!, ETA: -1).environmentObject(userSettings).environmentObject(vehicleItems))
        //                                              self.window = window
        //                                              window.makeKeyAndVisible()
        //                                          }
        //
        //
        //
        //                 return
        //               }
        ////                     DeepLinkWithETA(Spid:parameters["spid"]!)
        //
        //
        //
        //
        // //                            UserDefaults.standard.set(parameters["spid"], forKey: "DeepLink")
        //
        //
        //
        //          // Process the URL similarly to the UIApplicationDelegate example.
        // //     }
        //
        //
        // //                        if let scheme = url.scheme,
        // //                                       scheme.localizedCaseInsensitiveCompare("Twaddan") == .orderedSame,
        // //                                       let view = url.host {
        // //
        // //                                       var parameters: [String: String] = [:]
        // //                                       URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
        // //                                           parameters[$0.name] = $0.value
        // //                                       }
        // //
        // //
        // //                            let spcode = String( (URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?[0].value)!)
        // //
        // //
        // //
        // //                //                if let windowScene = scene as? UIWindowScene {
        // //                //                           let window = UIWindow(windowScene: windowScene)
        // //                //                           window.rootViewController = UIHostingController(rootView: TestFile().environmentObject(userSettings))
        // //                //                           self.window = window
        // //                //                           window.makeKeyAndVisible()
        // //                //                       }
        // //
        // //                                UserDefaults.standard.set(spcode, forKey: "DeepLink")
        // //
        // //                        }
        //
        //
        //
        //   }
        //        else{
        //            print("126")
        //    if let windowScene = scene as? UIWindowScene {
        //               let window = UIWindow(windowScene: windowScene)
        //               window.rootViewController = UIHostingController(rootView: contentView.environmentObject(userSettings).environmentObject(vehicleItems))
        //               self.window = window
        //               window.makeKeyAndVisible()
        //           }
        //
        //        }
        
        
    }
    
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        print("125")
        if let urlContext = URLContexts.first {
            
            let sendingAppID = urlContext.options.sourceApplication
            let url = urlContext.url
            print("source application = \(sendingAppID ?? "Unknown")")
            print("url = \(url)")
            //           UserDefaults.standard.set(("url = \(url) source application = \(sendingAppID)"), forKey: "DeepLink")
            
            if let scheme = url.scheme,
               scheme.localizedCaseInsensitiveCompare("Twaddan") == .orderedSame,
               let view = url.host {
                
                var parameters: [String: String] = [:]
                URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
                    parameters[$0.name] = $0.value
                }
                //                     DeepLinkWithETA(Spid:parameters["spid"]!)
                
                if let windowScene = scene as? UIWindowScene {
                    let window = UIWindow(windowScene: windowScene)
                    
                    Intermediate(spid: parameters["spid"]!).environmentObject(self.userSettings).environmentObject(self.vehicleItems)
                    //                                window.rootViewController = UIHostingController(rootView: BookingDetails(OpenCart:.constant(false) , GoToBookDetails: .constant(false), serviceSnap: DataSnapshot.init(), SelectedDriver: parameters["spid"]!, ETA: -1, openVA: self.$openVA).environmentObject(userSettings).environmentObject(vehicleItems))
                    
                    self.window = window
                    window.makeKeyAndVisible()
                }
                
                
                
                //                            UserDefaults.standard.set(parameters["spid"], forKey: "DeepLink")
                
            }
            
            // Process the URL similarly to the UIApplicationDelegate example.
            //     }
            
            
            //                        if let scheme = url.scheme,
            //                                       scheme.localizedCaseInsensitiveCompare("Twaddan") == .orderedSame,
            //                                       let view = url.host {
            //
            //                                       var parameters: [String: String] = [:]
            //                                       URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
            //                                           parameters[$0.name] = $0.value
            //                                       }
            //
            //
            //                            let spcode = String( (URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?[0].value)!)
            //
            //
            //
            //                //                if let windowScene = scene as? UIWindowScene {
            //                //                           let window = UIWindow(windowScene: windowScene)
            //                //                           window.rootViewController = UIHostingController(rootView: TestFile().environmentObject(userSettings))
            //                //                           self.window = window
            //                //                           window.makeKeyAndVisible()
            //                //                       }
            //
            //                                UserDefaults.standard.set(spcode, forKey: "DeepLink")
            //
            //                        }
            
            
            
        }
        
    }
    
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
        print("YYYYYYY","sceneDidDisconnect")
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
        print("YYYYYYY","Become Active")
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
        print("YYYYYYY","sceneWillResignActive")
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
        print("YYYYYYY","sceneWillEnterForeground")
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
        print("YYYYYYY","sceneDidEnterBackground")
    }
    
    
}

