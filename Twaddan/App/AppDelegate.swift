//
//  AppDelegate.swift
//  Twaddan
//
//  Created by user170583 on 4/22/20.
//  Copyright © 2020 spine. All rights reserved.
//

import UIKit

import GoogleSignIn
import SwiftUI
import FirebaseMessaging
import IQKeyboardManagerSwift
import FirebaseDatabase
import FirebaseStorage
import FirebaseFirestore
import FirebaseAnalytics
import FirebaseCore
import Firebase
import FirebaseAuth
import CryptoKit
import AuthenticationServices
import FirebaseDynamicLinks
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,GIDSignInDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    
    
    var userSettings = UserSettings()
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        GMSServices.provideAPIKey("AIzaSyA3I9OOmQXBqjS5pQoIO2q9wOKyZSQyLQk")
        //        GMSPlacesClient.provideAPIKey("AIzaSyC73FXvsbaH0gxh1lHY5CUjpAfmIGFx7KQ")
        setupGlobalAppearance()
        IQKeyboardManager.shared.enable = true
        
        //let appearance = UINavigationBarAppearance()
        //appearance.configureWithOpaqueBackground()
        //appearance.backgroundColor = .red
        //
        //let attrs: [NSAttributedString.Key: Any] = [
        //    .foregroundColor: UIColor.white,
        //    .font: UIFont.monospacedSystemFont(ofSize: 36, weight: .black)
        //]
        //
        //appearance.largeTitleTextAttributes = attrs
        //
        //UINavigationBar.appearance().scrollEdgeAppearance = appearance
        //        
        //        
        // Override point for customization after application launch.
        FirebaseApp.configure()
        Database.database().isPersistenceEnabled = true
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        
        if(UserDefaults.standard.bool(forKey: "PUSH_NOT") == true){
            application.registerForRemoteNotifications()
        }else{
            UIApplication.shared.unregisterForRemoteNotifications()
        }
        
        
        Messaging.messaging().delegate = self
        
        
        
        return true
    }
    
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any])
    -> Bool {
        
        ////         UserDefaults.standard.set(nil, forKey: "DeepLink")
        //
        //          if let scheme = url.scheme,
        //                       scheme.localizedCaseInsensitiveCompare("Twaddan") == .orderedSame,
        //                       let view = url.host {
        //
        //                       var parameters: [String: String] = [:]
        //                       URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
        //                           parameters[$0.name] = $0.value
        //                       }
        //
        //            UserDefaults.standard.set("kkkk", forKey: "DeepLink")
        //
        //
        //        }
        
        return GIDSignIn.sharedInstance().handle(url)
        
        
        
        
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url)
    }
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        // ...
        
        print("QQQQQQQQQQQQQ")
        if let error = error {
            // ...
            print(error.localizedDescription)
            return
        }
        
        
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        Auth.auth().signIn(with: credential){
            (res,err) in
            
            if(err != nil){
                
                print(err!.localizedDescription)
                return
            }
            
            UserDefaults.standard.set(true,forKey:"SOCIAL")
            if(res?.user.email == nil){
                
                let userSettings = UserSettings()
                res?.user.updateEmail(to: (Auth.auth().currentUser?.providerData[0].email)!, completion: { (error) in
                    
                    if(Auth.auth().currentUser?.displayName == nil ){
                        let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
                        
                        changeRequest!.displayName = Auth.auth().currentUser?.providerData[0].displayName
                        changeRequest!.photoURL = Auth.auth().currentUser?.providerData[0].photoURL
                        changeRequest!.commitChanges { error in
                            if let error = error {
                                // An error happened.
                            } else {
                                // Profile updated.
                            }
                        }
                    }
                    LoginMark(user:res!.user )
                    print("update email")
                })
                
            }else{
                
                if(Auth.auth().currentUser?.displayName == nil ){
                    let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
                    
                    changeRequest!.displayName = Auth.auth().currentUser?.providerData[0].displayName
                    changeRequest!.photoURL = Auth.auth().currentUser?.providerData[0].photoURL
                    changeRequest!.commitChanges { error in
                        if let error = error {
                            // An error happened.
                        } else {
                            // Profile updated.
                        }
                    }
                }
                LoginMark(user: res!.user)
                print("login")
            }
            //            print("user" + (res?.user.email)!)
            //
            //           userSettings.loggedIn = true
            //            let contentView = ContentView().environmentObject(userSettings)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
    
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
            // ...
        }
        
        return handled
    }
    
    func resetApp() {
        var vehicleItems = VehicleItems()
        let contentView = SplashScreen().environmentObject(vehicleItems) .environment(\.colorScheme, .light).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
        ))
        UIApplication.shared.windows[0].rootViewController = UIHostingController(rootView: contentView.environmentObject(userSettings))
    }
    
    
    
    
}

//struct ContentView: View {
//
//    @EnvironmentObject var settings: UserSettings
//
//    var body: some View {
//        ZStack {
//            if settings.loggedIn {
//                HomePage().environmentObject(settings)
//            } else {
//                SplashScreen().environmentObject(settings)
//            }
//        }
//    }
//}
func setupGlobalAppearance(){
    
    //   //global Appearance settings
    //    let customFont = UIFont.appRegularFontWith(size: 17)
    //    UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: customFont], for: .normal)
    //    UITextField.appearance().substituteFontName = Constants.App.regularFont
    //    UILabel.appearance().substituteFontName = Constants.App.regularFont
    //    UILabel.appearance().substituteFontNameBold = Constants.App.boldFont
    
}

