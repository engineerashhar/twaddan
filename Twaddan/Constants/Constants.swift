

import Foundation
import UIKit

struct Constants {
    
    // APPLICATION
    
    // Font
    
    
    static let appVersion = "1"
    
    static let regularFont = "Regular_Font".localized()
    
    
    
    static let boldFont = "Bold_Font".localized()
    static let lightFont = "Light_Font".localized()
    static let extraBoldFont = "ExtraBold_Font".localized()
    
    
    
}
