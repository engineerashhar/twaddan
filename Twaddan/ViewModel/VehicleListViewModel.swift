//
//  VehicleListViewModel.swift
//  Twaddan
//
//  Created by Spine on 17/05/20.
//  Copyright © 2020 spine. All rights reserved.
//
import Foundation
import Combine

class VehicleListViewModel:ObservableObject{
    
    @Published var cartRespository = CartRespository()
    @Published var vehicleCellViewModel = [VehicleCellViewModel]()
    
    private var cancellable=Set<AnyCancellable>()
    
    init(){
        
        cartRespository.$AllVehicles.map(){
            
            vehicles in
            
            
            vehicles.map(){
                vehicle in
                
                VehicleCellViewModel(vehicle:vehicle)
                
            }
            
        }.assign(to: \.vehicleCellViewModel, on: self)
        .store(in: &cancellable)
        
        
        
        //        self.vehicleCellViewModel=testDataVehicle.map{ vehicle in
        //
        //            VehicleCellViewModel(vehicle: vehicle)
        //
        //        }
        
        
    }
    
    //    func addTask(task:Task) {
    //        taskRespository.addTask(task: task)
    ////        let TCVM = TaskCellViewModel(task:task)
    ////        self.taskCellViewModel.append(TCVM)
    //    }
    
}
