//
//  UserDetailViewModel.swift
//  Twaddan
//
//  Created by Spine on 18/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation
import Foundation
import Combine

class UserDetailViewModel:ObservableObject ,Identifiable{
    
    @Published var cartRespository = CartRespository()
    @Published  var userDetails :UserDetails = UserDetails ()
    
    var id=""
    //@Published var completionStateIconName=""
    private var cancelables=Set<AnyCancellable>()
    
    init (){
        
        //print("Called")
        
        
        
        //        $vehicle.map{vehicle in
        //            vehicle.completed ? "checkmark.rectangle.fill":"rectangle"
        //        }.assign(to:\.completionStateIconName, on: self)
        //        .store(in: &cancelables)
        
        cartRespository.$userDetails.map(){ userDetails in
            //self.userDetails =   userDetails.id
            userDetails
        }
        .assign(to: \.userDetails, on: self)
        
        .store(in: &cancelables)
        
        
        
        
        cartRespository.$userDetails.compactMap(){ userDetails in
            userDetails.id
            
        }
        .assign(to: \.id, on: self)
        
        .store(in: &cancelables)
        
        
        cartRespository.$userDetails
            .dropFirst()
            .debounce(for: 0.8, scheduler: RunLoop.main)
            .sink(){userDetails in
                print(userDetails)
                self.cartRespository.UpdateUser(userDetails: userDetails)
                //   self.cartRespository.userDetails = userDetails
                
                
            }.store(in: &cancelables)
        
    }
    
}


//class UserDetailViewModel:ObservableObject{
//
//    @Published var cartRespository = CartRespository()
//    @Published var userDetails = UserDetails()
//
//    private var cancellable=Set<AnyCancellable>()
//
//    init(){
//
//        cartRespository.$userDetails.map(){
//
//            userDetails in
//
//
//
//
//            }.assign(to: \.vehicleCellViewModel, on: self)
//            .store(in: &cancellable)
//
//
//
////        self.vehicleCellViewModel=testDataVehicle.map{ vehicle in
////
////            VehicleCellViewModel(vehicle: vehicle)
////
////        }
//
//
//    }
//
////    func addTask(task:Task) {
////        taskRespository.addTask(task: task)
//////        let TCVM = TaskCellViewModel(task:task)
//////        self.taskCellViewModel.append(TCVM)
////    }
//
//}
