//
//  VehicleListViewModel.swift
//  Twaddan
//
//  Created by Spine on 17/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation
import Combine

class VehicleCellViewModel:ObservableObject ,Identifiable{
    
    @Published var cartRespository = CartRespository()
    @Published  var vehicle :Vehicle
    var id=""
    //@Published var completionStateIconName=""
    private var cancelables=Set<AnyCancellable>()
    
    init (vehicle:Vehicle){
        
        //print("Called")
        
        self.vehicle=vehicle
        
        //        $vehicle.map{vehicle in
        //            vehicle.completed ? "checkmark.rectangle.fill":"rectangle"
        //        }.assign(to:\.completionStateIconName, on: self)
        //        .store(in: &cancelables)
        
        
        $vehicle.compactMap(){ vehcles in
            vehicle.id
        }
        .assign(to: \.id, on: self)
        .store(in: &cancelables)
        
        
        $vehicle
            .dropFirst()
            .debounce(for: 0.8, scheduler: RunLoop.main)
            .sink(){vehicle in
                print(vehicle)
                self.cartRespository.UpdateVehicle(vehicle: vehicle)
                
            }.store(in: &cancelables)
        
    }
    
}
