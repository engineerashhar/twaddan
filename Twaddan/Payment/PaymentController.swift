//
//  PaymentController.swift
//  Twaddan
//
//  Created by Spine on 26/07/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation
// Import the SDK at the top of your viewcontroller file
import NISdk

// Implement  CardPaymentDelegate methods to receive events related to card payment

class PaymentController: UIViewController, CardPaymentDelegate {
    
    func paymentDidComplete(with status: PaymentStatus) {
        if(status == .PaymentSuccess) {
            // Payment was successful
        } else if(status == .PaymentFailed) {
            // Payment failed
        } else if(status == .PaymentCancelled) {
            // Payment was cancelled by user
        }
    }
    
    
    func authorizationDidComplete(with status: AuthorizationStatus) {
        if(status == .AuthFailed) {
            // Authentication failed
            return
        }
        // Authentication was successful
    }
    
    // On creating an order, call the following method to show the card payment UI
    func showCardPaymentUI(orderResponse: OrderResponse) {
        let sharedSDKInstance = NISdk.sharedInstance
        sharedSDKInstance.showCardPaymentViewWith(cardPaymentDelegate: self,
                                                  overParent: self,
                                                  for: orderResponse)
    }
}
