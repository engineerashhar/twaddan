//
//  PaymentHandler.swift
//  Twaddan
//
//  Created by Spine on 05/08/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation
import NISdk


import PassKit

typealias CPaymentCompletionHandler = (String) -> Void

class CPaymentHandler:UIViewController, CardPaymentDelegate{
    
    //static let supportedNetworks: [PKPaymentNetwork] = [
    //    .amex,
    //    .masterCard,
    //    .visa,
    //    .quicPay
    //]
    
    //var paymentController: PKPaymentAuthorizationController?
    //var paymentSummaryItems = [PKPaymentSummaryItem]()
    //var paymentStatus = PKPaymentAuthorizationStatus.failure
    
    //     var parent: PaymentPage
    //    init(_ parent: PaymentPage) {
    //               self.parent = parent
    //           }
    var OrderRefNo = "Twaddan"
    var AccessToken = "Token"
    //    var OResponds : OrderResponse = OrderResponse(from: Decoder())
    var paymentAmount: Int = 0
    var cardPaymentDelegate: CardPaymentDelegate?
    var paymentMethod: PaymentMethod?
    var purchasedItems: [Product] = []
    var paymentRequest: PKPaymentRequest?
    var completionHandler: CPaymentCompletionHandler?
    var billingAddress = Billing(firstName: "", lastName: "", address1: "", city: "", countryCode: "")
    
    //    init(paymentAmount: Int,
    //           using paymentMethod: PaymentMethod = .Card,
    //           with purchasedItems: [Product]) {
    //
    ////          self.cardPaymentDelegate = cardPaymentDelegate
    //          self.paymentAmount = paymentAmount
    //          self.paymentMethod = paymentMethod
    //          self.purchasedItems = purchasedItems
    ////          super.init(nibName: nil, bundle: nil)
    //
    //          if(paymentMethod == .ApplePay) {
    //              let merchantId = "merchant.com.spine.twaddan"
    //              assert(!merchantId.isEmpty, "You need to add your apple pay merchant ID above")
    //              paymentRequest = PKPaymentRequest()
    //              paymentRequest?.merchantIdentifier = merchantId
    //              paymentRequest?.countryCode = "AE"
    //              paymentRequest?.currencyCode = "AED"
    //              paymentRequest?.requiredShippingContactFields = [.postalAddress, .emailAddress, .phoneNumber]
    //              paymentRequest?.merchantCapabilities = [.capabilityDebit, .capabilityCredit, .capability3DS]
    //              paymentRequest?.requiredBillingContactFields = [.postalAddress, .name]
    //              paymentRequest?.paymentSummaryItems = self.purchasedItems.map { PKPaymentSummaryItem(label: $0.name, amount: NSDecimalNumber(value: $0.amount)) }
    //              paymentRequest?.paymentSummaryItems.append(PKPaymentSummaryItem(label: "Total", amount: NSDecimalNumber(value: paymentAmount)))
    //          }
    //      }
    
    
    
    
    
    func createOrder(address:Billing,paymentAmount: Int,
                     using paymentMethod: PaymentMethod,
                     with purchasedItems: [Product],completion: @escaping CPaymentCompletionHandler){
        
        
        
        completionHandler = completion
        
        self.cardPaymentDelegate = self
        self.paymentAmount = paymentAmount
        self.paymentMethod = paymentMethod
        self.purchasedItems = purchasedItems
        self.billingAddress = address
        //          super.init(nibName: nil, bundle: nil)
        
        if(paymentMethod == .ApplePay) {
            //                      let merchantId = "merchant.com.spine.twaddan"
            //                      assert(!merchantId.isEmpty, "You need to add your apple pay merchant ID above")
            paymentRequest = PKPaymentRequest()
            
            
            
            
            
            
            //                        paymentRequest.paymentSummaryItems = paymentSummaryItems
            //                        paymentRequest.merchantIdentifier = "merchant.com.spine.twaddan"
            //                        paymentRequest.merchantCapabilities = .capability3DS
            //                        paymentRequest.countryCode = "AE"
            //                        paymentRequest.currencyCode = "AED"
            //    paymentRequest.requiredShippingContactFields = [.phoneNumber, .emailAddress]
            //                        paymentRequest.supportedNetworks = ApplePaymentHandler.supportedNetworks
            
            // Display our payment request
            //                        paymentController = PKPaymentAuthorizationController(paymentRequest: paymentRequest)
            
            paymentRequest?.supportedNetworks = ApplePaymentHandler.supportedNetworks
            
            paymentRequest?.merchantIdentifier = "merchant.com.spine.twaddan"
            paymentRequest?.countryCode = "AE"
            paymentRequest?.currencyCode = "AED"
            //                      paymentRequest?.requiredShippingContactFields = [.postalAddress, .emailAddress, .phoneNumber]
            paymentRequest?.merchantCapabilities = [.capabilityDebit, .capabilityCredit, .capability3DS]
            //                      paymentRequest?.requiredBillingContactFields = [.postalAddress, .name]
            //                    paymentRequest?.paymentSummaryItems = self.purchasedItems.map { PKPaymentSummaryItem(label: Product[0]., amount: NSDecimalNumber(value: $0.amount)) }
            paymentRequest?.paymentSummaryItems.append(PKPaymentSummaryItem(label: purchasedItems[0].name, amount: NSDecimalNumber(value: purchasedItems[0].amount/100)))
            
        }
        
        
        
        
        
        print("111111")
        // Multiply amount always by 100 while creating an order
        //        let orderRequest = OrderRequest(action: "SALE", amount: OrderAmount(currencyCode: "AED", value: paymentAmount * 100), emailAddress: "customer@test.com", billingAddress: Billing(firstName: "Test", lastName: "Customer", address1: "123 Test Street", city: "Dubai", countryCode: "UAE"))
        //        let encoder = JSONEncoder()
        //        let orderRequestData = try! encoder.encode(orderRequest)
        let headers = ["Content-Type": "application/vnd.ni-identity.v1+json","Authorization":"Basic Y2ExM2Q3MGQtZjRhZS00MDMxLTk1YzItODY3NjIwZmU5N2U3OmFjNTdlYmFmLTY1MmYtNDliMi1hZmEwLTNmYTFkYzE5YTQyOA=="]
        let request = NSMutableURLRequest(url: NSURL(string: "https://api-gateway.sandbox.ngenius-payments.com/identity/auth/access-token")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        //        request.httpBody = orderRequestData
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { [weak self] (data, response, error) in
            if (error != nil) {
                print(error)
            }
            if let data = data {
                do {
                    print("111111",response)
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]{
                        
                        guard let accesstoken = json["access_token"] as? NSString else {
                            print("111111 error",json)
                            DispatchQueue.main.async {
                            }
                            return
                        }
                        
                        self?.AccessToken = accesstoken as String
                        
                        
                        
                        
                        
                        // Multiply amount always by 100 while creating an order
                        
                        let orderRequest = OrderRequest(action: "SALE", amount: OrderAmount(currencyCode: "AED", value: paymentAmount), emailAddress: "customer@twaddan.com", billingAddress: self?.billingAddress ?? Billing(firstName: "Twaddan_User", lastName: "TwaddanUser", address1: "Here", city: "Dubai", countryCode: "UAE"))
                        
                        let encoder = JSONEncoder()
                        let orderRequestData = try! encoder.encode(orderRequest)
                        let headers = ["Authorization":"Bearer \(accesstoken)","Content-Type": "application/vnd.ni-payment.v2+json","Accept":"application/vnd.ni-payment.v2+json"]
                        let request = NSMutableURLRequest(url: NSURL(string: "https://api-gateway.sandbox.ngenius-payments.com/transactions/outlets/2b3c58ca-4547-4c61-8b4a-6f3b1ea9b4aa/orders")! as URL,
                                                          cachePolicy: .useProtocolCachePolicy,
                                                          timeoutInterval: 10.0)
                        request.httpMethod = "POST"
                        request.allHTTPHeaderFields = headers
                        request.httpBody = orderRequestData
                        
                        let session = URLSession.shared
                        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { [weak self] (data, response, error) in
                            if (error != nil) {
                                print(error)
                                //                self!.completionHandler!(false)
                                //                self?.displayErrorAndClose(error: error)
                            }
                            if let data = data {
                                //                let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]
                                print(data,response)
                                print("pppppppppp")
                                do
                {
                    //                       if let json : [String:Any] = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]{
                    //                        self?.OrderRefNo = json["access_token"]  as! NStri
                    //
                    //                }
                    
                    let orderResponse: OrderResponse = try JSONDecoder().decode(OrderResponse.self, from: data)
                    //                    self?.OResponds = orderResponse
                    
                    self?.OrderRefNo = orderResponse.reference ?? "twaddan"
                    let sharedSDKInstance = NISdk.sharedInstance
                    DispatchQueue.main.async {
                        //                        self?.dismiss(animated: false, completion: { [weak self] in
                        if(self?.paymentMethod == .Card) {
                            sharedSDKInstance.showCardPaymentViewWith(cardPaymentDelegate: (self?.cardPaymentDelegate!)!,
                                                                      overParent: self?.cardPaymentDelegate as! UIViewController,
                                                                      for: orderResponse)
                            
                            return
                            
                        } else {
                            
                            print("pppppppppp")
                            
                            sharedSDKInstance.initiateApplePayWith(applePayDelegate: self?.cardPaymentDelegate as? ApplePayDelegate,
                                                                   cardPaymentDelegate: (self?.cardPaymentDelegate)!,
                                                                   overParent: self?.cardPaymentDelegate as!UIViewController,
                                                                   for: orderResponse, with: self!.paymentRequest!)
                        }
                        //                        })
                    }
                }
                                
                                
                                catch let error {
                                    
                                    print(error)
                                    //                    self!.completionHandler!(false)
                                    //                    self?.displayErrorAndClose(error: error)
                                }
                            }
                        })
                        dataTask.resume()
                        
                        
                        
                        
                        
                        
                        
                    }
                } catch let error {
                    print(error)
                    
                    //                        self?.displayErrorAndClose(error: error)
                }
            }
        })
        dataTask.resume()
        
        
        
        
        
        
        
    }
    
    
    
    //    func startPayment(label:String,amount:NSDecimalNumber, completion: @escaping CardPaymentCompletionHandler) {
    //
    ////    let amount = PKPaymentSummaryItem(label: "Ammount", amount: amount, type: .final)
    ////    let tax = PKPaymentSummaryItem(label: "Tax", amount: NSDecimalNumber(string: "1.12"), type: .final)
    //    let total = PKPaymentSummaryItem(label: label, amount: amount, type: .pending)
    //
    //    paymentSummaryItems = [total];
    //    completionHandler = completion
    //
    //    // Create our payment request
    //    let paymentRequest = PKPaymentRequest()
    //
    //
    //
    //    paymentRequest.paymentSummaryItems = paymentSummaryItems
    //    paymentRequest.merchantIdentifier = "merchant.com.spine.twaddan"
    //    paymentRequest.merchantCapabilities = .capability3DS
    //    paymentRequest.countryCode = "AE"
    //    paymentRequest.currencyCode = "AED"
    ////    paymentRequest.requiredShippingContactFields = [.phoneNumber, .emailAddress]
    //    paymentRequest.supportedNetworks = ApplePaymentHandler.supportedNetworks
    //
    //    // Display our payment request
    //    paymentController = PKPaymentAuthorizationController(paymentRequest: paymentRequest)
    //    paymentController?.delegate = self
    //    paymentController?.present(completion: { (presented: Bool) in
    //        if presented {
    //            NSLog("Presented payment controller")
    //        } else {
    //            NSLog("Failed to present payment controller")
    //            self.completionHandler!(false)
    //         }
    //     })
    //  }
    
    
    func paymentDidComplete(with status: PaymentStatus) {
        
        print("oooooooo",status.rawValue)
        
        if(status == .PaymentSuccess) {
            //          self.completionHandler!("Success")
            // Payment was successful
            
            
            //        https://api-gateway.sandbox.ngenius-payments.com/transactions/outlets/[outlet-reference]/orders/[order-reference]
            
            
            
            
            //                let encoder = JSONEncoder()
            //                let orderRequestData = try! encoder.encode(orderRequest)
            let headers = ["Authorization":"Bearer \(self.AccessToken)"]
            let request = NSMutableURLRequest(url: NSURL(string: "https://api-gateway.sandbox.ngenius-payments.com/transactions/outlets/2b3c58ca-4547-4c61-8b4a-6f3b1ea9b4aa/orders/\(self.OrderRefNo)")! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "GET"
            request.allHTTPHeaderFields = headers
            //                request.httpBody = orderRequestData
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { [weak self] (data, response, error) in
                if (error != nil) {
                    print("QQQQQQQQQQQQQQ",error)
                    //                self!.completionHandler!(false)
                    //                self?.displayErrorAndClose(error: error)
                    
                    self?.completionHandler!("Failed")
                    //                        RefundRequest(OrderResponds: self!.OrderRefNo)
                }
                if let data = data {
                    //                let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]
                    print(data,response)
                    print("pppppppppp")
                    do
                        {
                            let orderResponse: OrderResponse = try JSONDecoder().decode(OrderResponse.self, from: data)
                            orderResponse.embeddedData?.payment?[0]
                            let json : [String:Any] = try (JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any])!
                            print("QQQQQQQQQQQQQQQ",json)
                            switch String((orderResponse.embeddedData?.payment?[0].state)!) {
                            case "CAPTURED":
                                self?.completionHandler!("Success")
                            default:
                                self?.completionHandler!("Failed")
                            }
                            
                        }
                    
                    
                    catch let error {
                        
                        print("QQQQQQQQQQQQQQq",error)
                        self?.completionHandler!("Failed")
                        //                            RefundRequest(OrderResponds: self!.OrderRefNo)
                        
                        //                    self!.completionHandler!(false)
                        //                    self?.displayErrorAndClose(error: error)
                    }
                }
            })
            dataTask.resume()
            
            
            
            
            
            
            
            
        } else if(status == .PaymentFailed) {
            self.completionHandler!("Failed")
            // Payment failed
        } else if(status == .PaymentCancelled) {
            self.completionHandler!("Cancelled")
            // Payment was cancelled by user
        }
    }
    
    
    func authorizationDidComplete(with status: AuthorizationStatus) {
        
        if(status == .AuthFailed) {
            self.completionHandler!("AuthFailed")
            // Authentication failed
            return
        }
        // Authentication was successful
    }
    
    func RefundRequest(OrderResponds:OrderResponse)  {
        
    }
    
    // On creating an order, call the following method to show the card payment UI
    //    func showCardPaymentUI(orderResponse: OrderResponse) {
    //      let sharedSDKInstance = NISdk.sharedInstance
    //      sharedSDKInstance.showCardPaymentViewWith(cardPaymentDelegate: self,
    //                                            overParent: self,
    //                                            for: orderResponse)
    //    }
    
    
    
    
    
    
    
}

/*
 PKPaymentAuthorizationControllerDelegate conformance.
 */
//extension CPaymentHandler: UIViewController{
//}
//func paymentAuthorizationController(_ controller: PKPaymentAuthorizationController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
//
//    // Perform some very basic validation on the provided contact information
////    if payment.shippingContact?.emailAddress == nil || payment.shippingContact?.phoneNumber == nil {
////        paymentStatus = .failure
////    } else {
//        // Here you would send the payment token to your server or payment provider to process
//        // Once processed, return an appropriate status in the completion handler (success, failure, etc)
//        paymentStatus = .success
////    }
//
//    completion(paymentStatus)
//}

//func paymentAuthorizationControllerDidFinish(_ controller: PKPaymentAuthorizationController) {
//    controller.dismiss {
//        DispatchQueue.main.async {
//            if self.paymentStatus == .success {
//                self.completionHandler!(true)
//            } else {
//                self.completionHandler!(false)
//            }
//        }
//    }
//}

//}
