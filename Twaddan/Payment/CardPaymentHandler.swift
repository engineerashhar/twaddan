//
//  CardPaymentHandler.swift
//  Twaddan
//
//  Created by Spine on 27/07/20.
//  Copyright © 2020 spine. All rights reserved.
//

//
//  CardPaymentHandler.swift
//  Twaddan
//
//  Created by Spine on 27/07/20.
//  Copyright © 2020 spine. All rights reserved.
//

import PassKit
import NISdk

typealias CardPaymentCompletionHandler = (Bool) -> Void

class CardPaymentHandler: NSObject {
    
    
    var paymentController: PKPaymentAuthorizationController?
    var paymentSummaryItems = [PKPaymentSummaryItem]()
    var paymentStatus = PKPaymentAuthorizationStatus.failure
    var completionHandler: CardPaymentCompletionHandler?
    
    func startPayment(label:String,amount:NSDecimalNumber, completion: @escaping CardPaymentCompletionHandler) {
        
        //    let amount = PKPaymentSummaryItem(label: "Ammount", amount: amount, type: .final)
        //    let tax = PKPaymentSummaryItem(label: "Tax", amount: NSDecimalNumber(string: "1.12"), type: .final)
        let total = PKPaymentSummaryItem(label: label, amount: amount, type: .pending)
        
        //    paymentSummaryItems = [total];
        completionHandler = completion
        
        //    // Create our payment request
        //    let paymentRequest = PKPaymentRequest()
        //
        //
        //
        //    paymentRequest.paymentSummaryItems = paymentSummaryItems
        //    paymentRequest.merchantIdentifier = "merchant.com.spine.twaddan"
        //    paymentRequest.merchantCapabilities = .capability3DS
        //    paymentRequest.countryCode = "AE"
        //    paymentRequest.currencyCode = "AED"
        ////    paymentRequest.requiredShippingContactFields = [.phoneNumber, .emailAddress]
        //    paymentRequest.supportedNetworks = CardPaymentHandler.supportedNetworks
        //
        //    // Display our payment request
        //    paymentController = PKPaymentAuthorizationController(paymentRequest: paymentRequest)
        //    paymentController?.delegate = self
        //    paymentController?.present(completion: { (presented: Bool) in
        //        if presented {
        //            NSLog("Presented payment controller")
        //        } else {
        //            NSLog("Failed to present payment controller")
        //            self.completionHandler!(false)
        //         }
        //     })
        
        //        API = "gz"
        //        OUTLET ID = "2b3c58ca-4547-4c61-8b4a-6f3b1ea9b4aa"
        
        let json: [String: Any] = ["title": "ABC",
                                   "dict": ["1":"First", "2":"Second"]]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        let url = URL(string:" https://api-gateway.sandbox.ngenius-payments.com/identity/auth/access-token")
        
        
        //        var request = URLRequest(url: url)
        //        request.httpMethod = "POST"
        
        // insert json data to the request
        //        request.httpBody = jsonData
        
        
        
        //        let sharedSDKInstance = NISdk.sharedInstance
        //             sharedSDKInstance.showCardPaymentViewWith(cardPaymentDelegate: self,
        //                                                   overParent: self,
        //                                                   for: orderResponse)
        //
        
        //            paymentController = PKPaymentAuthorizationController(paymentRequest: paymentRequest)
        //            paymentController?.delegate = self
        //            paymentController?.present(completion: { (presented: Bool) in
        //                if presented {
        //                    NSLog("Presented payment controller")
        //                } else {
        //                    NSLog("Failed to present payment controller")
        //                    self.completionHandler!(false)
        //                 }
        //             })
        
    }
}

/*
 PKPaymentAuthorizationControllerDelegate conformance.
 */
extension CardPaymentHandler: CardPaymentDelegate {
    
    func paymentDidComplete(with status: PaymentStatus) {
        if(status == .PaymentSuccess) {
            // Payment was successful
            paymentStatus = .success
            
        } else if(status == .PaymentFailed) {
            paymentStatus = .failure
            // Payment failed
        } else if(status == .PaymentCancelled) {
            // Payment was cancelled by user
            paymentStatus = .failure
        }
        //         completion(paymentStatus)
    }
    
    
    func authorizationDidComplete(with status: AuthorizationStatus) {
        if(status == .AuthFailed) {
            // Authentication failed
            return
        }
        // Authentication was successful
    }
    
    // On creating an order, call the following method to show the card payment UI
    //     func showCardPaymentUI(orderResponse: OrderResponse) {
    //       let sharedSDKInstance = NISdk.sharedInstance
    //       sharedSDKInstance.showCardPaymentViewWith(cardPaymentDelegate: self,
    //                                             overParent: self,
    //                                             for: orderResponse)
    //     }
    
    //func paymentAuthorizationController(_ controller: PKPaymentAuthorizationController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
    //
    //    // Perform some very basic validation on the provided contact information
    ////    if payment.shippingContact?.emailAddress == nil || payment.shippingContact?.phoneNumber == nil {
    ////        paymentStatus = .failure
    ////    } else {
    //        // Here you would send the payment token to your server or payment provider to process
    //        // Once processed, return an appropriate status in the completion handler (success, failure, etc)
    //        paymentStatus = .success
    ////    }
    //
    //    completion(paymentStatus)
    //}
    //
    //func paymentAuthorizationControllerDidFinish(_ controller: PKPaymentAuthorizationController) {
    //    controller.dismiss {
    //        DispatchQueue.main.async {
    //            if self.paymentStatus == .success {
    //                self.completionHandler!(true)
    //            } else {
    //                self.completionHandler!(false)
    //            }
    //        }
    //    }
    //}
    
}

