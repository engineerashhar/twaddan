//
//  PaymentPage.swift
//  Twaddan
//
//  Created by Spine on 26/07/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation
import SwiftUI
import NISdk



struct PaymentPage: UIViewControllerRepresentable{
    
    let paymentHandler = CPaymentHandler()
    
    var Node : String
    var Order : [String:Any]
    var VehicleList:[VehicleSets]
    @Binding var status : String
    @Binding var ALERT3 : Bool
    @Binding var BLUR : Bool
    var products : [Product]
    @Binding var  showPaymentPortal :Bool
    var billingAddress : Billing
    
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<PaymentPage>) -> CPaymentHandler {
        //          let k : [Product] = [Product(name: "Test", amount: 100)]
        
        //                                                                            self.paymentHandler.startPayment()
        self.paymentHandler.createOrder(address: self.billingAddress, paymentAmount: products[0].amount, using: .Card
                                        , with: products) { (success) in
            self.status  = success
            print(success)
            if(success == "Success"){
                
                EnterOrderToDB(Node: self.Node, Order: self.Order, VehicleList: self.VehicleList)
                self.ALERT3 = true
                self.BLUR = true
                
            }else{
                
            }
            self.showPaymentPortal = false                                                                                                                                                    }
        
        return paymentHandler
        
    }
    func updateUIViewController(_ uiViewController: CPaymentHandler, context: UIViewControllerRepresentableContext<PaymentPage>) {
        
    }
    
    // let paymentHandler = CPaymentHandler()
    
    //    func makeUIViewController(context: UIViewControllerRepresentableContext<PaymentPage>) -> CPaymentHandler {
    ////        let k : [Product] = [Product(name: "Test", amount: 100)]
    ////
    ////        let paymentHandler = CPaymentHandler()
    ////
    ////
    ////
    ////
    ////
    ////        let apm = paymentHandler.createOrder(paymentAmount: 100, using: .Card, with: k) { (success) in
    ////                                                                                                                                                                                      if success {
    ////                                                                                                                                                                                          print("Success")
    ////
    ////
    ////                                                                                                                                                                                      } else {
    ////                                                                                                                                                   print("!Success")
    ////                                                                                                                                                                                      }
    ////                                                                                                                                                                                  }
    ////
    ////               apm.delegate = context.coordinator
    ////               return apm
    ////
    ////                                                                              self.paymentHandler.startPayment()
    //
    //    }
    //    func updateUIViewController(_ uiViewController: CPaymentHandler, context: UIViewControllerRepresentableContext<PaymentPage>) {
    //
    //    }
    
    //    func makeCoordinator() -> Coordinator {
    //           Coordinator(self)
    //       }
    //      class Coordinator: NSObject, UINavigationControllerDelegate, CardPaymentDelegate {
    //           var parent: PaymentPage
    //
    //           init(_ parent: PaymentPage) {
    //               self.parent = parent
    //           }
    //       }
    
}
