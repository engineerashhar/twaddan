//
//  OrderAmount.swift
//  Simple Integration
//
//  Created by Johnny Peter on 23/08/19.
//  Copyright © 2019 Network International. All rights reserved.
//

import Foundation

struct OrderAmount: Encodable {
    let currencyCode: String?
    let value: Int?
    
    private enum AmountCodingKeys: String, CodingKey {
        case currencyCode
        case value
    }
}

struct Billing: Encodable {
    let firstName: String?
    let lastName: String?
    let address1: String?
    let city: String?
    let countryCode: String?
    
    private enum BillingCodingKeys: String, CodingKey {
        case firstName
        case lastName
        case address1
        case city
        case countryCode
    }
}
