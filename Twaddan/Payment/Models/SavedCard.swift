//
//  SavedCard.swift
//  Twaddan
//
//  Created by Spine on 19/08/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation

public struct SavedCard {
    public let cardToken: String?
    public let cardholderName: String?
    public let expiry: String?
    public let maskedPan: String?
    public let scheme: String?
}

extension SavedCard: Codable {
    
    private enum SavedCardCodingKeys: String, CodingKey {
        case cardToken = "cardToken"
        case cardholderName = "cardholderName"
        case expiry = "expiry"
        case maskedPan = "maskedPan"
        case scheme = "scheme"
    }
    
    private enum hrefCodingKeys: String, CodingKey {
        case href
    }
    
    public init(from decoder: Decoder) throws {
        let savedCardContainer = try decoder.container(keyedBy: SavedCardCodingKeys.self)
        
        do {
            let cardTokenContainer = try savedCardContainer.nestedContainer(keyedBy: hrefCodingKeys.self, forKey: .cardToken)
            cardToken = try cardTokenContainer.decodeIfPresent(String.self, forKey: .href)
        } catch {
            self.cardToken = nil
        }
        
        do {
            let cardholderContainer: KeyedDecodingContainer? = try savedCardContainer.nestedContainer(keyedBy: hrefCodingKeys.self, forKey: .cardholderName)
            cardholderName = try cardholderContainer?.decodeIfPresent(String.self, forKey: .href)
        } catch {
            self.cardholderName = nil
        }
        
        do {
            let expiryContainer = try savedCardContainer.nestedContainer(keyedBy: hrefCodingKeys.self, forKey: .expiry)
            expiry = try expiryContainer.decodeIfPresent(String.self, forKey: .href)
        } catch {
            self.expiry = nil
        }
        
        do {
            let maskedPanContainer = try savedCardContainer.nestedContainer(keyedBy: hrefCodingKeys.self, forKey: .maskedPan)
            maskedPan = try maskedPanContainer.decodeIfPresent(String.self, forKey: .href)
        } catch {
            self.maskedPan = nil
        }
        
        do {
            let schemeContainer = try savedCardContainer.nestedContainer(keyedBy: hrefCodingKeys.self, forKey: .scheme)
            scheme = try schemeContainer.decodeIfPresent(String.self, forKey: .href)
        } catch {
            self.scheme = nil
        }
    }
}

