//
//  ApplePayController.swift
//  Twaddan
//
//  Created by Spine on 26/07/20.
//  Copyright © 2020 spine. All rights reserved.
//


import Foundation
import PassKit
import SwiftUI

struct ApplePayController: UIViewControllerRepresentable {
    @Environment(\.presentationMode) var presentationMode
    //    @EnvironmentObject var userData: UserData
    //    @Binding var purchase: Purchase
    @Binding var isPresenting: Bool
    @Binding var paymentStatus: Bool
    
    let items: [PKPaymentSummaryItem]
    
    func updateUIViewController(_ uiViewController: PKPaymentAuthorizationViewController, context: Context) {
        
    }
    
    typealias UIViewControllerType = PKPaymentAuthorizationViewController
    
    
    func makeUIViewController(context: Context) ->  PKPaymentAuthorizationViewController {
        let applePayManager = ApplePayManager(items: items)
        let apm = applePayManager.paymentViewController()!
        apm.delegate = context.coordinator
        return apm
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    class Coordinator: NSObject, PKPaymentAuthorizationViewControllerDelegate  {
        var parent: ApplePayController
        
        init(_ parent: ApplePayController) {
            self.parent = parent
        }
        
        func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
            controller.dismiss(animated: true) {
                //                    self.parent.isPresenting = false
                
            }
        }
        
        func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
            
            parent.paymentStatus = true
            
            print("did authorize payment")
            
        }
        
        func paymentAuthorizationViewControllerWillAuthorizePayment(_ controller: PKPaymentAuthorizationViewController) {
            print("Will authorize payment")
            parent.isPresenting = false
        }
    }
    
    class ApplePayManager: NSObject {
        let currencyCode: String
        let countryCode: String
        let merchantID: String
        let paymentNetworks: [PKPaymentNetwork]
        let items: [PKPaymentSummaryItem]
        
        init(items: [PKPaymentSummaryItem],
             currencyCode: String = "USD",
             countryCode: String = "US",
             merchantID: String = "merchant.com.spine.twaddan",
             paymentNetworks: [PKPaymentNetwork] = [PKPaymentNetwork.quicPay, PKPaymentNetwork.amex, PKPaymentNetwork.masterCard, PKPaymentNetwork.visa]) {
            self.items = items
            self.currencyCode = currencyCode
            self.countryCode = countryCode
            self.merchantID = merchantID
            self.paymentNetworks = paymentNetworks
        }
        
        func paymentViewController() -> PKPaymentAuthorizationViewController? {
            //            if PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks) {
            let request = PKPaymentRequest()
            request.currencyCode = self.currencyCode
            request.countryCode = self.countryCode
            request.supportedNetworks = paymentNetworks
            request.merchantIdentifier = self.merchantID
            request.paymentSummaryItems = items
            request.merchantCapabilities = [.capabilityCredit, .capabilityDebit]
            return PKPaymentAuthorizationViewController(paymentRequest: request)
            //            }
            //            return nil
        }
    }
}

//import UIKit
//import PassKit
//
//class ApplePayController: UIViewController {
//
//}
//
//extension ApplePayController : PKPaymentAuthorizationViewControllerDelegate {
//    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
//        controller.dismiss(animated: true, completion: nil)
//    }
//
//    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
//        completion(PKPaymentAuthorizationResult(status: .success, errors: nil))
//    }
//}


//class ApplePayController: UIViewController {
//
//    //MARK:- OUT LET PROPERTIES
//    @IBOutlet weak var btn_pay: UIButton!
//    @IBOutlet weak var textFieldAmount: UITextField!
//
//
//    //MARK:- LOCAL PROPERTIES
//
//    private var payment : PKPaymentRequest = PKPaymentRequest()
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.btn_pay.addTarget(self, action: #selector(tapForPay), for: .touchUpInside)
//
//        payment.merchantIdentifier = "merchant.com.pushpendra.pay"
//        payment.supportedNetworks = [.quicPay, .masterCard, .visa]
//        payment.supportedCountries = ["IN", "US"]
//        payment.merchantCapabilities = .capability3DS
//        payment.countryCode = "IN"
//        payment.currencyCode = "INR"
//    }
//
//
//
//    @objc func tapForPay(){
//        if !textFieldAmount.text!.isEmpty {
//            let amount = textFieldAmount.text!
//            payment.paymentSummaryItems = [PKPaymentSummaryItem(label: "iPhone XR 128 GB", amount: NSDecimalNumber(string: amount))]
//
//            let controller = PKPaymentAuthorizationViewController(paymentRequest: payment)
//            if controller != nil {
//                controller!.delegate = self
//                present(controller!, animated: true, completion: nil)
//            }
//        }
//    }
//}
//
//extension ApplePayController : PKPaymentAuthorizationViewControllerDelegate {
//    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
//        controller.dismiss(animated: true, completion: nil)
//    }
//
//    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
//        completion(PKPaymentAuthorizationResult(status: .success, errors: nil))
//    }
//}
