//
//  SignLink.swift
//  Twaddan
//
//  Created by Spine on 02/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation
import FirebaseAuth

import FirebaseDatabase

class SignLink : ObservableObject{
    
    let ref = Database.database().reference()
    @Published var isEmailExist : Bool = true
    @Published var isEmailVerified : Bool = false
    @Published var loginStatus : Bool = false
    @Published var errorStatus : String = "Error no read"
    
    
    
    //    init(){
    //        listenLogin()
    //    }
    func handleError(error: Error)  {
        
        /// the user is not registered
        /// user not found
        
        let errorAuthStatus = AuthErrorCode.init(rawValue: error._code)!
        
        
        switch errorAuthStatus {
        case .wrongPassword:
            errorStatus = "wrongPassword"
            print("wrongPassword")
            
        case .invalidEmail:
            errorStatus = "invalidEmail"
            print("invalidEmail")
        case .operationNotAllowed:
            errorStatus = "operationNotAllowed"
            print("operationNotAllowed")
        case .userDisabled:
            errorStatus = "userDisabled"
            print("userDisabled")
        case .userNotFound:
            errorStatus = "userNotFound"
            print("userNotFound")
        //            self.register(auth: Auth.auth())
        case .tooManyRequests:
            errorStatus = "tooManyRequests, oooops"
            print("tooManyRequests, oooops")
        default: fatalError("error not supported here")
        }
        
        
        
    }
    //
    //
    //
    
    
    func signin(Email:String,Password:String) {
        
        
        
        Auth.auth().signIn(withEmail: Email, password: Password) { (result, error) in
            
            guard error == nil else {
                return self.handleError(error: error!)
            }
            
            guard let user = result?.user else{
                fatalError("Not user do not know what went wrong")
                
                
            }
            
            
            print("Signed in user: \(user.email)")
            
            print(user.email!+" "+Email)
            self.ref.child("Users").child(user.uid).child("LLT").setValue([".sv": "timestamp"])
            
        }
        
    }
    
    //
    //
    //
    //
    //
    //
    //    func isEmailVerified ()  {
    //
    //        self.isEmailVerified =  Auth.auth().currentUser?.isEmailVerified
    //
    //    }
    //
    //
    //
    //
    //
    //
    
    
    func register(profile:Profile)  {
        
        
        
        
        Auth.auth().createUser(withEmail: profile.Email, password: profile.Password) { (result, error) in
            
            guard error == nil else {
                return self.handleError(error: error!)
            }
            
            guard let user = result?.user else {
                fatalError("Do not know why this would happen")
            }
            
            
            let P : [String : Any]=[
                
                "email" : profile.Email,
                "lastName": profile.LastName,
                "name".localized() :profile.Name,
                "phoneNumber":profile.Phone,
                "RT" : [".sv": "timestamp"]
            ]
            
            self.ref.child("Users").child((result?.user.uid)!).child("personal_details").setValue(P)
            
            
            //   print("registered user: \(user.email)")
            
        }
        
    }
    //
    func sendVerification(){
        
        
        Auth.auth().currentUser?.sendEmailVerification { (error) in
            
            guard let error = error else {
                return print("user email verification sent")
            }
            
            self.handleError(error: error)
        }
    }
    //
    func checkEmailExist(Email:String){
        
        
        Auth.auth().fetchProviders(forEmail: Email, completion: {
            (providers, error) in
            
            if let error = error {
                self.handleError(error: error)
                print(error.localizedDescription)
            } else if let providers = providers {
                print(providers)
                self.isEmailExist=true
            }else{
                self.isEmailExist=false
                print("Not Exist ")
            }
        })
        
        
        //
        //        ref.child("Users").child(Email).observeSingleEvent(of: .value, with: { Snaps in
        //                      if Snaps.exists(){
        //                        self.isEmailExist = true
        //                        }else{
        //                        self.isEmailExist = false
        //                        }
        //               })
        
    }
    ////
    func signOut() {
        
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    //        func listenLogin() {
    //
    //               Auth.auth().addStateDidChangeListener { (auth, user) in
    //
    //                   if Auth.auth().currentUser != nil {
    //                     // User is signed in.
    //                     // ...
    //                       self.loginStatus=true
    //
    //                       let user = Auth.auth().currentUser
    //                       if let user = user {
    //                         // The user's ID, unique to the Firebase project.
    //                         // Do NOT use this value to authenticate with your backend server,
    //                         // if you have one. Use getTokenWithCompletion:completion: instead.
    //                         //  self.Uid = user.uid
    //                         let email = user.email
    //                         let photoURL = user.photoURL
    //                         var multiFactorString = "MultiFactor: "
    //                         for info in user.multiFactor.enrolledFactors {
    //                           multiFactorString += info.displayName ?? "[DispayName]"
    //                           multiFactorString += " "
    //                         }
    //                         // ...
    //                       }
    //
    //
    //
    //                   } else {
    //                     // No user is signed in.
    //                       self.loginStatus=false
    //                     // ...
    //                   }
    //               }
    //
    //
    //           }
    
    
}


