//
//  Animation.swift
//  Twaddan
//
//  Created by Spine on 20/07/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI

struct DottedAnimation: View {
    
    
    //    @Binding var leftpadding : CGFloat
    //        @Binding var Rightpadding : CGFloat
    //        @Binding var solidlength : CGFloat
    //   var dottedlength : CGFloat
    //     @State private var reavelStroke = false
    var body: some View {
        HStack{
            
            HStack{
                GeometryReader { geo in
                    Path { path in
                        
                        
                        path.move(to: CGPoint(x: 0, y: 0))
                        path.addLine(to: CGPoint(x: geo.frame(in: .local).midX*2, y: 0))
                        //                                                                  path.addLine(to: CGPoint(x: geo.frame(in: .local).midX*2, y: 1))
                        //                                                                  path.addLine(to: CGPoint(x: 1, y: 1))
                    }
                    .stroke(style: StrokeStyle(lineWidth: 1, lineCap: .square, lineJoin: .round,  dash:  [3,5], dashPhase: 1))
                    .foregroundColor(Color("t2"))
                }
            }
            //            .frame(width:self.solidlength,height:1)
            //                .padding(.leading,self.leftpadding)
            //          HStack{
            //                    VStack{
            //                      GeometryReader { geo in
            //
            //
            //                           Path { path in
            //
            //
            //                                  path.move(to: CGPoint(x: 0, y: 0))
            //                                        path.addLine(to: CGPoint(x: geo.frame(in: .local).midX*2, y: 0))
            //                                           path.addLine(to: CGPoint(x: geo.frame(in: .local).midX*2, y: 1))
            //                                           path.addLine(to: CGPoint(x: 1, y: 1))
            //                           }
            //                           .trim(from: self.reavelStroke ? 0.5 : 1, to: 1)
            //                            .stroke(style: StrokeStyle(lineWidth: 2, lineCap: .square, lineJoin: .round,  dash: [1,10], dashPhase: 1))
            //                //                           .frame(width: 300, height: 300)
            //
            //                                               .animation(Animation.easeOut(duration:8).delay(0).repeatForever(autoreverses: false)
            //                            )
            //                        .foregroundColor(Color("ManBackColor"))
            //                           .onAppear
            //                                               {
            //                                                   self.reavelStroke.toggle()
            //                                           }
            //
            //                        }
            //                    }.frame(width: self.dottedlength)
            //                    Spacer()
            //          }.frame(width:self.dottedlength,height:1)        }
        }
    }
}
struct DottedAnimation_Previews: PreviewProvider {
    static var previews: some View {
        //        DottedAnimation(leftpadding: .constant(30), Rightpadding: .constant(30.0), solidlength: .constant((UIScreen.main.bounds.width-60)*2/4), dottedlength: (UIScreen.main.bounds.width-60)/4)
        DottedAnimation()
    }
}
