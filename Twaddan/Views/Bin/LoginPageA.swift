
import SwiftUI


struct ContentView: View {
    
    @State private var scale: CGFloat = 1
    @State private var angle: Double = 0
    @State private var borderThickness: CGFloat = 1
    var body: some View {
        
        Text("Tap here")
            //                    .rotationEffect(.degrees(-angle))
            .padding()
            .border(Color.red, width: borderThickness)
            .rotationEffect(.degrees(angle))
            .animation(Animation.linear.repeatForever())
            //                    .rotationEffect(.degrees(-angle))
            
            .onAppear{
                self.angle += 45
            }
        
        
    }
    
    
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
