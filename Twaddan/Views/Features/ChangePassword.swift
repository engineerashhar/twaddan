//
//  ChangePassword.swift
//  Twaddan
//
//  Created by Spine on 17/08/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import Firebase
import FirebaseDatabase
import FirebaseAuth

struct ChangePassword: View {
    
    //        @Binding var FromDA : Bool
    
    @State var ViewLoader = false
    
    @EnvironmentObject var settings: UserSettings
    
    @State var CNA : Bool = false
    
    @State var password2 : String = ""
    @State var password : String = ""
    @State var Pfailed:Bool = false
    @State var Ufailed:Bool = false
    @State var showsAlert = false
    @State var alert :String = ""
    @State var ErrorStatus:Bool=false
    @State var OpenMenu:Bool=false
    @State var openMenu:Bool=false
    @State var ErrorComments:String=""
    @State var Resend : Bool = false
    @State var openSignUp : Bool = false
    
    @ObservedObject var backImagelinker = BackImageLinker()
    @ObservedObject var signLink = SignLink()
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    let ref = Database.database().reference()
    
    @State var openDAP : Bool =  false
    
    
    var body: some View {
        
        
        //     username=settings.Email
        
        
        
        
        
        
        
        GeometryReader { geometry in
            
            
            ZStack{
                
                //
                //                    VStack{
                //                               VStack{
                //                                  EmptyView()
                //                               }  .background(AnimatedImage(url : URL (string: self.backImagelinker.url))
                //
                //
                //                                                                            .resizable().frame(width:geometry.size.width,height:geometry.size.height) // Resizable like SwiftUI.Image, you must use this modifier or the view will use the image bitmap size
                //                                                //                            .placeholder(UIImage(systemName: "basicbackimage")) // Placeholder Image
                //                                                                            // Supports ViewBuilder as well
                //
                //                                                                            .transition(.fade)
                //
                //
                //
                //                                                                            .clipped().brightness(-0.1)
                //                                                )
                //
                //                               }.background(Image("basicbackimage").resizable().frame(width:geometry.size
                //                                   .width,height:geometry.size.height)).brightness(-0.1)
                
                
                
                VStack {
                    VStack {
                        //                         Spacer()
                        //                                         .frame(height: geometry.size.height*0.05)
                        //                         HStack{
                        //
                        //
                        //
                        //
                        ////
                        ////                          Button(action : {
                        ////
                        ////                              self.showsAlert.toggle()
                        ////
                        ////                          //  self.Ufailed.toggle()
                        ////
                        ////
                        ////
                        ////                         //   print("cryftgbhjnlkm")
                        ////                           self.presentationMode.wrappedValue.dismiss()
                        ////
                        ////
                        ////                          }) {
                        ////                              Text("BACK")
                        ////                                .foregroundColor(.white)
                        ////                                .font(.custom("Regular_Font".localized(), size: 15)).hidden()
                        ////
                        ////
                        ////
                        ////                          }
                        ////
                        //
                        //
                        //
                        ////                          .alert(isPresented: self.$Pfailed){
                        ////                              Alert(title: Text("Failed"), message: Text("Please enter a password"), dismissButton: .default(Text("Got it!")))
                        ////                        }
                        //
                        //
                        //                          Spacer()
                        ////                            Button(action:{
                        ////
                        ////                                 let uuid = UIDevice.current.identifiexrForVendor?.uuidString
                        //////                                    print(uuid)
                        ////                                          self.ViewLoader = true
                        ////                                let user  =   Auth.auth().currentUser ;
                        ////                                 if(user?.email == nil && user?.uid != nil){
                        ////                                                        UserDefaults.standard.string(forKey: "Aid") ?? "0" = uuid ?? user?.uid as! String
                        ////                                    UserDefaults.standard.set(UserDefaults.standard.string(forKey: "Aid") ?? "0", forKey: "Aid")
                        ////
                        ////                                                                              self.OpenMenu=true
                        ////                                     self.ViewLoader = false
                        ////                                 }else{
                        ////
                        ////                                Auth.auth().signInAnonymously() { (authResult, error) in
                        ////                                  // ...
                        ////
                        ////
                        ////
                        ////                                    guard let user = authResult?.user else {
                        ////                                        print(error)
                        ////                                        return }
                        ////
                        ////
                        ////                                    let isAnonymous = user.isAnonymous  // true
                        ////                                    let uid = user.uid
                        ////
                        ////                                    UserDefaults.standard.string(forKey: "Aid") ?? "0" = uuid ?? uid
                        ////                                       UserDefaults.standard.set(UserDefaults.standard.string(forKey: "Aid") ?? "0", forKey: "Aid")
                        ////
                        ////                                      self.openMenu = true
                        ////                                    self.ViewLoader = false
                        ////
                        ////                                }
                        ////
                        ////                                }
                        ////
                        ////
                        ////                            })
                        ////                          {
                        ////                              Text("SKIP")
                        ////                                .foregroundColor(.white)
                        ////                                  .font(.custom("Regular_Font".localized(), size: 15))
                        ////
                        ////                          }
                        ////
                        ////
                        //
                        //
                        //
                        //
                        //                      }
                        //.padding([.top], 60)
                        
                        
                        
                        
                        //                    Spacer()
                        
                        
                        
                        
                        
                        
                        
                        VStack{
                            Spacer()
                            
                            VStack(spacing:0){
                                
                                Spacer().frame(height:200)
                                
                                HStack{
                                    
                                    Text("Enter the new password")
                                        
                                        .foregroundColor(Color("darkthemeletter"))
                                        //                                .padding( .vertical, 50.0)
                                        .font(.custom("Regular_Font".localized(), size: 20))
                                    Spacer()
                                    
                                }.padding(.vertical).padding(.top,50)
                                Spacer()
                                
                                PasswordPanel(password: self.$password, password2: self.$password2)
                                    .padding(.bottom, 30.0)
                                
                                if(self.ErrorStatus){
                                    
                                    Text(self.ErrorComments)
                                        .foregroundColor(.red) .font(.custom("Regular_Font".localized(), size: 13))
                                    
                                }
                                
                                Spacer()
                                
                                
                                Button(action: {
                                    
                                    if(self.password == ""){
                                        
                                        self.ErrorComments="Please enter a valid Password";                                    self.ErrorStatus=true
                                        
                                        
                                        
                                    }else if(self.password2 != self.password2){
                                        
                                        self.ErrorComments="Mismatch Password";                                    self.ErrorStatus=true
                                        
                                    }else{
                                        
                                        self.ViewLoader = true
                                        
                                        
                                        Auth.auth().currentUser?.updatePassword(to: self.password, completion: { (error) in
                                            if(error != nil){
                                                self.presentationMode.wrappedValue.dismiss()
                                            }
                                        })
                                        
                                        
                                        
                                        
                                        
                                        
                                    }
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                }
                                
                                
                                
                                )
                                {
                                    
                                    
                                    
                                    HStack{
                                        
                                        
                                        
                                        Spacer()
                                        
                                        Text("SAVE")   .font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color.white)
                                        Spacer()
                                        
                                        
                                        
                                    }.padding()
                                    //                            .border(Color("ManualSignInGray"), width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                                    
                                    //                                                               .frame(width: 300.0, height: 50)      .background(Color("su1"))
                                    //                                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color("su1"),lineWidth: 1))
                                    .background(Color("su1"))
                                    .cornerRadius(10)
                                    
                                    
                                    
                                }.alert(isPresented: self.$Ufailed) {
                                    Alert(title: Text("Sorry!"), message: Text(self.alert), dismissButton: .default(Text("Got it!")))
                                    
                                }
                                
                                
                                
                                
                                
                                //                            NavigationLink(destination: SignUpSelection()
                                ////                                .navigationBarTitle("").navigationBarHidden(true).edgesIgnoringSafeArea(.all)
                                //                                    ){
                                //
                                //
                                //
                                //
                                //
                                //
                                //                                HStack{
                                //                                    Spacer()
                                //                                    Text("Creat New Account?")
                                //                                        .font(.custom("Regular_Font".localized(), size: 13))
                                //                                      .foregroundColor(Color("ManBackColor"))
                                //
                                //                                    }
                                //
                                //                            }
                                //
                                
                                
                                
                                Spacer()
                                
                                
                                
                            }
                            
                            .padding(.horizontal, 25.0)
                            
                            
                            
                            Spacer()
                            
                            VStack{
                                Text("").padding(.vertical,50.0)
                            }
                            
                            
                            
                            
                        }
                        .frame(maxWidth:.infinity,maxHeight:geometry.size.height*0.7)
                        .background(RoundedCorners(color: .white, tl: 30, tr: 30, bl: 0, br: 0))
                        
                        
                        
                        
                    }
                    
                    
                    
                    
                    Spacer()
                    
                }
                
                
                
                if(self.ViewLoader){
                    
                    Loader()
                }
            }
            .frame(width:geometry.size.width,height: geometry.size.height)
            
            .navigationBarTitle("Sign in")
            .edgesIgnoringSafeArea(.all)
            
            //                .navigationBarTitle("").navigationBarHidden(true)
        }
        //          .navigationBarTitle("Login Page",displayMode: .inline)
        //        .navigationBarHidden(true)
        //            .navigationBarItems(
        //                           trailing:
        //                               Button("SKIP") {
        //                                let uuid = UIDevice.current.identifierForVendor?.uuidString
        //                                //                                    print(uuid)
        //                                                                          self.ViewLoader = true
        //                                                                let user  =   Auth.auth().currentUser ;
        //                                                                 if(user?.email == nil && user?.uid != nil){
        //                                                                                        UserDefaults.standard.string(forKey: "Aid") ?? "0" = uuid ?? user?.uid as! String
        //                                                                    UserDefaults.standard.set(UserDefaults.standard.string(forKey: "Aid") ?? "0", forKey: "Aid")
        //
        //                                                                                                              self.OpenMenu=true
        //                                                                     self.ViewLoader = false
        //                                                                 }else{
        //
        //                                                                Auth.auth().signInAnonymously() { (authResult, error) in
        //                                                                  // ...
        //
        //
        //
        //                                                                    guard let user = authResult?.user else {
        //                                                                        print(error)
        //                                                                        return }
        //
        //
        //                                                                    let isAnonymous = user.isAnonymous  // true
        //                                                                    let uid = user.uid
        //
        //                                                                    UserDefaults.standard.string(forKey: "Aid") ?? "0" = uuid ?? uid
        //                                                                       UserDefaults.standard.set(UserDefaults.standard.string(forKey: "Aid") ?? "0", forKey: "Aid")
        //
        //                                                                      self.OpenMenu = true
        //                                                                    self.ViewLoader = false
        //
        //                                                                }
        //
        //                                                                }
        //
        //                               }
        //                       )
        
        
        //          }
        //        ].background(Color.red)
        .edgesIgnoringSafeArea(.all)
        //        .navigationBarBackButtonHidden(true)
        //
        
        
        
        
        
    }
    
    struct PasswordPanel: View {
        
        @Binding var password : String
        @Binding var password2 : String
        @State var View = false
        
        var body: some View {
            VStack(){
                
                
                HStack{
                    
                    if(self.View){
                        
                        TextField("Password", text: $password)
                            
                            .foregroundColor(Color("darkthemeletter"))                .font(.custom("Regular_Font".localized(), size: 14))
                    }else{
                        SecureField("Password", text: $password)
                            
                            .foregroundColor(Color("darkthemeletter"))                .font(.custom("Regular_Font".localized(), size: 14))
                        
                    }
                    
                    Spacer()
                    Button(action:{
                        
                        self.View.toggle()
                    }){
                        Image("Group 395") .foregroundColor(Color.black) .font(.custom("Regular_Font".localized(), size: 14))
                    }
                }
                
                Divider()
                    
                    .frame(height: 1)
                
                
                
                
                
                //    Spacer()
                
                
                TextField("Re-Enter Password", text:$password2 )
                    .padding(.top, 20.0)
                    
                    
                    .foregroundColor(Color("darkthemeletter"))                .font(.custom("Regular_Font".localized(), size: 14))
                
                
                Divider()
                    .frame(height: 1)
                
                
                
            }
        }
    }
}

struct ChangePassword_Previews: PreviewProvider {
    static var previews: some View {
        ChangePassword()
    }
}
