//
//  Profile.swift
//  Twaddan
//
//  Created by Spine on 02/06/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI
import FirebaseDatabase
import FirebaseAuth

struct MyProfile: View {
    
    @State var Edit :EditMode = .inactive
    @State var Photo :String = ""
    @State var Name : String = ""
    
    
    @State var ALERT1 : Bool = false
    @State var ALERT2 : Bool = false
    @State var ALERT3 : Bool = false
    @State var VCode : String = ""
    @State var VERCODE : String = ""
    @State var showAlert : Bool = false
    
    @State var verificationID : String = ""
    @State var errorStatus : String = ""
    @State var ViewLoader : Bool = false
    
    
    @State var SecondName : String = ""
    @State var Email : String = ""
    @State var Phone : String = ""
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    let ref = Database.database().reference()
    @EnvironmentObject var settings : UserSettings
    
    var body: some View {
        
        
        
        
        GeometryReader { geometry in
            
            
            ZStack(){
                
                ZStack{
                    
                    
                    VStack{
                        Spacer().frame(height:geometry.size.height*0.1)
                            
                            
                            .padding(.vertical,15)
                        
                        AnimatedImage(url:URL(string:self.Photo)).resizable().frame(width:64,height:64)
                            .clipShape(Circle())
                            .padding(.bottom,10)
                        
                        Text(self.Name)  .font(.custom("Bold_Font".localized(), size: 14))  .foregroundColor(Color.white)
                        
                        
                        Spacer()
                        
                        
                        VStack{
                            
                            //                            Spacer().frame(height:100)
                            //           Rectangle()
                            //                                 .fill(Color(#colorLiteral(red: 0.6859999895, green: 0.6859999895, blue: 0.6859999895, alpha: 1)))
                            //
                            //                                 .frame(width:geometry.size.width,height: 260)
                            //                                 .cornerRadius(20)
                            VStack{
                                HStack{
                                    
                                    Image("user").resizable().frame(width:20,height: 20).padding(.horizontal,30).padding(.top,30)
                                    
                                    Text("First Name").font(.custom("Regular_Font".localized(), size: 14))  .padding(.top,30) .foregroundColor(Color("MainFontColor1"))
                                    Spacer()
                                }.padding(.leading,10)
                                
                                HStack{
                                    if(self.Edit != .inactive){
                                        
                                        
                                        TextField("First Name", text: self.$Name,onEditingChanged: {_ in
                                            self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("personal_details/name").setValue(self.Name)
                                            
                                        }).font(.custom("Regular_Font".localized(), size: 14))   .foregroundColor(Color("darkthemeletter"))
                                        .padding(.leading,97)
                                        .padding(.top,6)
                                    }else{
                                        
                                        Text(self.Name).font(.custom("Regular_Font".localized(), size: 14))   .foregroundColor(Color("darkthemeletter"))
                                            .padding(.leading,97)
                                            .padding(.top,6)
                                    }
                                    
                                    Spacer()
                                }
                                //  Spacer()
                            }
                            
                            
                            
                            VStack{
                                HStack{
                                    
                                    Image("").resizable().frame(width:20,height: 20).padding(.horizontal,30).padding(.top,30)
                                    
                                    Text("Last Name").font(.custom("Regular_Font".localized(), size: 14))  .padding(.top,30) .foregroundColor(Color("MainFontColor1"))
                                    Spacer()
                                }.padding(.leading,10)
                                
                                HStack{
                                    if(self.Edit != .inactive){
                                        
                                        
                                        TextField("Last Name", text: self.$SecondName,onEditingChanged: {_ in
                                            self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("personal_details/lastName").setValue(self.SecondName)
                                            
                                        }).font(.custom("Regular_Font".localized(), size: 14))   .foregroundColor(Color("darkthemeletter"))
                                        .padding(.leading,97)
                                        .padding(.top,6)
                                    }else{
                                        Text(self.SecondName).font(.custom("Regular_Font".localized(), size: 14))   .foregroundColor(Color("darkthemeletter"))
                                            .padding(.leading,97)
                                            .padding(.top,6)
                                    }
                                    Spacer()
                                }
                                //  Spacer()
                            }
                            
                            
                            VStack{
                                HStack{
                                    
                                    Image("call").renderingMode(.template).resizable().frame(width:20,height: 20).padding(.horizontal,30).padding(.top,30)
                                    
                                    Text("Phone Number").font(.custom("Regular_Font".localized(), size: 14))  .padding(.top,30) .foregroundColor(Color("MainFontColor1"))
                                    Spacer()
                                    
                                    
                                }.padding(.leading,10)
                                
                                HStack{
                                    
                                    if(self.Edit != .inactive){
                                        //                                                      Spacer()
                                        HStack{
                                            Text("+971").font(.custom("Regular_Font".localized(), size: 14))   .foregroundColor(Color("darkthemeletter"))
                                            
                                            TextField("Phone", text: self.$Phone,onCommit: {
                                                //                                                        self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("personal_details/phoneNumber").setValue(self.Phone)
                                                self.showAlert = false
                                                
                                                if(self.Phone.count >= 9){
                                                    self.Phone = String(self.Phone.suffix(9))
                                                    self.ALERT1 = true
                                                    
                                                }else{
                                                    self.ALERT3 = true
                                                }
                                                //
                                            }).font(.custom("Regular_Font".localized(), size: 14))   .foregroundColor(Color("darkthemeletter")).keyboardType(.namePhonePad)
                                            
                                        }.padding(.leading,97)
                                        .padding(.top,6)
                                        
                                        Spacer()
                                        Button(action:{
                                            self.showAlert = false
                                            if(self.Phone.count >= 9){
                                                self.Phone = String(self.Phone.suffix(9))
                                                self.ALERT1 = true
                                                
                                            }else{
                                                self.ALERT3 = true
                                            }
                                            
                                        }){
                                            Text("SAVE").font(.custom("Regular_Font".localized(), size: 14))  .padding(.trailing,30)
                                            
                                        }
                                        
                                    }else{
                                        //                                                    Spacer()
                                        
                                        HStack{
                                            Text("+971").font(.custom("Regular_Font".localized(), size: 14))   .foregroundColor(Color("darkthemeletter"))
                                            Text(self.Phone.suffix(9)).font(.custom("Regular_Font".localized(), size: 14))   .foregroundColor(Color("darkthemeletter"))
                                            
                                        }
                                        .padding(.leading,97)
                                        .padding(.top,6)
                                        Spacer()
                                    }
                                    
                                }
                                //     Spacer()
                            }
                            
                            
                            
                            VStack{
                                HStack{
                                    
                                    Image("super_suv").renderingMode(.template).resizable().frame(width:20,height: 20).padding(.horizontal,30).padding(.top,30)
                                    
                                    Text("Email_ID").font(.custom("Regular_Font".localized(), size: 14))  .padding(.top,30) .foregroundColor(Color("MainFontColor1"))
                                    Spacer()
                                }.padding(.leading,10)
                                
                                HStack{
                                    
                                    Text(self.Email).font(.custom("Regular_Font".localized(), size: 14))   .foregroundColor(Color("darkthemeletter"))
                                        .padding(.leading,97)
                                        .padding(.top,6)
                                    
                                    Spacer()
                                }
                                Divider()
                                Spacer()
                            }
                            
                            
                        }
                    }
                    
                }.blur(radius: self.ALERT1 || self.ALERT2 ? 2:0)
                
                
                
                if(self.ALERT1){
                    
                    ZStack{
                        
                        VStack{
                            
                            HStack{
                                Text("Verify mobile number").font(.custom("Bold_Font".localized(), size: 16))
                                    .foregroundColor(Color("darkthemeletter"))
                                Spacer()
                                
                            }
                            HStack{
                                
                                Image(systemName: "phone")
                                    .foregroundColor(Color("darkthemeletter"))
                                
                                
                                
                                Text("+971"+self.Phone.suffix(9)).font(.custom("Regular_Font".localized(), size: 16))    .foregroundColor(Color("darkthemeletter"))
                                Spacer()
                                Button(action: {
                                    
                                    self.ALERT1 = false
                                    
                                    //                                                                self.presentationMode.wrappedValue.dismiss()
                                    
                                    
                                }){
                                    
                                    Text("Change").font(.custom("Bold_Font".localized(), size: 14))    .foregroundColor(Color("darkthemeletter"))
                                }
                                
                                
                            }
                            
                            HStack{
                                Text("We will send you SMS with verfication code on this number above").font(.custom("Regular_Font".localized(), size: 14)).foregroundColor(Color(
                                                                                                                                                                                "MainFontColor1"))
                                
                                
                                Spacer()
                                
                            }
                            HStack{
                                
                                Spacer()
                                Button(action: {
                                    
                                    self.ALERT1 = false
                                    //                                                            self.BLUR = false
                                    
                                    
                                }){
                                    
                                    Text("Cancel").font(.custom("Regular_Font".localized(), size: 17)).foregroundColor(Color(
                                                                                                                        "MainFontColor1"))
                                }
                                
                                Button(action:
                                        
                                        {
                                            
                                            
                                            self.ViewLoader = true
                                            
                                            self.VERCODE = String(format:"%06d", Int.random(in: 0 ... 999999))
                                            
                                            if(self.Phone.count == 9 || self.Phone.count == 10){
                                                
                                                let S : String =  "https://smartsmsgateway.com/api/api_http.php?username=twaddan&password=NaJPCB2iSy&senderid=TWADDAN&to=971\(self.Phone)&text=\(self.VERCODE)%20is%20your%20verification%20code&type=text&datetime=2020-08-20%2008%3A01%3A26"
                                                
                                                let url = URL(string:S
                                                )
                                                //        else {
                                                //        let error = NSError(domain: "LocalDomain", code: 0, userInfo: [NSLocalizedDescriptionKey: "Failed to create object URL"])
                                                //        print("Error: \(error)")
                                                //        return
                                                //    }
                                                let config = URLSessionConfiguration.default
                                                let session = URLSession(configuration: config)
                                                
                                                
                                                let task = session.dataTask(with: url!, completionHandler:{_,_,_  in
                                                    
                                                    self.ALERT1 = false
                                                    self.showAlert = false
                                                    self.ALERT2 = true
                                                    self.ViewLoader = false
                                                    
                                                    
                                                } )
                                                task.resume()
                                                
                                                
                                                //                                                                                                                                           self.verificationID = VerificationID
                                                
                                                
                                                
                                            }else{
                                                self.showAlert = true
                                                self.ViewLoader = false
                                                self.errorStatus = "Invalid Phone Number"
                                            }
                                            
                                            //                                        Auth.auth().settings?.isAppVerificationDisabledForTesting = true
                                            //                                        PhoneAuthProvider.provider().verifyPhoneNumber("+91\(self.MOBILE)", uiDelegate: nil) { (VerificationID, error) in
                                            //
                                            //                                            if(error != nil){
                                            //
                                            //                                                let  e : Error = error!
                                            //
                                            //                                                let errorAuthStatus = AuthErrorCode.init(rawValue: e._code)!
                                            //
                                            //                                                self.showAlert = true
                                            //
                                            //                                                     self.ViewLoader = false
                                            //
                                            //                                                switch errorAuthStatus {
                                            //
                                            //
                                            //                                                case .invalidPhoneNumber:
                                            //                                                    self.errorStatus = "Invalid Phone Number"
                                            //
                                            //                                                    print("Invalid Phone Number")
                                            //
                                            //                                                case .invalidVerificationCode:
                                            //                                                    self.errorStatus = "Invalid Verification Code"
                                            //                                                    print("Invalid Verification Code")
                                            //                                                case .operationNotAllowed:
                                            //                                                    self.errorStatus = "operationNotAllowed"
                                            //                                                    print("operationNotAllowed")
                                            //                                                case .userDisabled:
                                            //                                                    self.errorStatus = "userDisabled"
                                            //                                                    print("userDisabled")
                                            //                                                case .userNotFound:
                                            //                                                    self.errorStatus = "userNotFound"
                                            //                                                    print("userNotFound")
                                            //                                                //            self.register(auth: Auth.auth())
                                            //                                                case .tooManyRequests:
                                            //                                                    self.errorStatus = "tooManyRequests, oooops"
                                            //                                                    print("tooManyRequests, oooops")
                                            //                                                default: print(e)
                                            //                                                }
                                            //
                                            //
                                            //
                                            //
                                            //                                                return
                                            //                                            }
                                            //                                            else{
                                            //
                                            //                                                self.verificationID = VerificationID
                                            //                                                self.ALERT1 = false
                                            //                                                self.ALERT2 = true
                                            //                                                     self.ViewLoader = false
                                            //                                            }
                                            //                                        }
                                            
                                            
                                            
                                            
                                            
                                        }
                                       
                                       //                                                            {
                                       //
                                       //
                                       //                                                                 self.ViewLoader = true
                                       //
                                       ////                                                            self.VERCODE = String(format:"%06d", Int.random(in: 0 ... 999999))
                                       //
                                       ////                                                            if(self.MOBILE.count == 9){
                                       ////                                                            let S : String =  "https://smartsmsgateway.com/api/api_http.php?username=twaddan&password=NaJPCB2iSy&senderid=TWADDAN&to=971\(self.Phone)&text=\(self.VERCODE)%20is%20your%20verification%20code&type=text&datetime=2020-08-20%2008%3A01%3A26"
                                       ////
                                       ////                                                                                                           let url = URL(string:S
                                       ////                                                                                                           )
                                       ////                                                                                                           //        else {
                                       ////                                                                                                           //        let error = NSError(domain: "LocalDomain", code: 0, userInfo: [NSLocalizedDescriptionKey: "Failed to create object URL"])
                                       ////                                                                                                           //        print("Error: \(error)")
                                       ////                                                                                                           //        return
                                       ////                                                                                                           //    }
                                       ////                                                                                                               let config = URLSessionConfiguration.default
                                       ////                                                                                                               let session = URLSession(configuration: config)
                                       ////                                                                                                               let task = session.dataTask(with: url!, completionHandler: {
                                       ////                                                                                                                   (data, response, error) in
                                       ////
                                       ////
                                       ////
                                       ////                    //                                                                                                                                           self.verificationID = VerificationID
                                       ////                                                                                                                                                               self.ALERT1 = false
                                       ////                                                                                                                                  self.showAlert = false
                                       ////                                                                                                                self.ALERT2 = true
                                       ////                                                                                                                                                                    self.ViewLoader = false
                                       ////
                                       ////                                                                                                               })
                                       ////
                                       ////                                                            }else{
                                       ////                                                               self.showAlert = true
                                       ////                                                                  self.ViewLoader = false
                                       ////                                                                self.errorStatus = "Invalid Phone Number"
                                       ////                                                            }
                                       //
                                       //                                                            Auth.auth().settings?.isAppVerificationDisabledForTesting = false
                                       //                                                            PhoneAuthProvider.provider().verifyPhoneNumber("+971\(self.Phone.suffix(9))", uiDelegate: nil) { (VerificationID, error) in
                                       //
                                       //                                                                if(error != nil){
                                       //
                                       //                                                                    let  e : Error = error!
                                       //
                                       //                                                                    let errorAuthStatus = AuthErrorCode.init(rawValue: e._code)!
                                       //
                                       //                                                                    self.showAlert = true
                                       //
                                       //                                                                         self.ViewLoader = false
                                       //
                                       //                                                                    switch errorAuthStatus {
                                       //
                                       //
                                       //                                                                    case .invalidPhoneNumber:
                                       //                                                                        self.errorStatus = "Invalid Phone Number"
                                       //
                                       //                                                                        print("Invalid Phone Number")
                                       //
                                       //                                                                    case .invalidVerificationCode:
                                       //                                                                        self.errorStatus = "Invalid Verification Code"
                                       //                                                                        print("Invalid Verification Code")
                                       //                                                                    case .operationNotAllowed:
                                       //                                                                        self.errorStatus = "operationNotAllowed"
                                       //                                                                        print("operationNotAllowed")
                                       //                                                                    case .userDisabled:
                                       //                                                                        self.errorStatus = "userDisabled"
                                       //                                                                        print("userDisabled")
                                       //                                                                    case .userNotFound:
                                       //                                                                        self.errorStatus = "userNotFound"
                                       //                                                                        print("userNotFound")
                                       //                                                                    //            self.register(auth: Auth.auth())
                                       //                                                                    case .tooManyRequests:
                                       //                                                                        self.errorStatus = "tooManyRequests, oooops"
                                       //                                                                        print("tooManyRequests, oooops")
                                       //                                                                    default: print(e)
                                       //                                                                    }
                                       //
                                       //
                                       //
                                       //
                                       //                                                                    return
                                       //                                                                }
                                       //                                                                else{
                                       //
                                       //                                                                    self.verificationID = VerificationID!
                                       //                                                                    self.ALERT1 = false
                                       //                                                                    self.ALERT2 = true
                                       //                                                                         self.ViewLoader = false
                                       //                                                                }
                                       //                                                            }
                                       //
                                       //
                                       //
                                       //
                                       //
                                       //                                                        }
                                       
                                       
                                ){
                                    
                                    Text("GetCode").font(.custom("ExtraBold_Font".localized(), size: 17))
                                }
                                
                            }
                            
                            if(self.showAlert){
                                
                                
                                Text(self.errorStatus).font(.custom("Regular_Font".localized(), size: 14)).foregroundColor(Color.red).padding()
                                
                            }
                        }.frame(width :320 , height: 150)
                        
                        .padding()
                        
                    } .background(Color.white)
                    
                    .cornerRadius(5)
                }
                
                
                
                if(self.ALERT2){
                    ZStack{
                        VStack{
                            
                            HStack{
                                Text("Verify mobile number").font(.custom("Bold_Font".localized(), size: 16))
                                    .foregroundColor(Color("darkthemeletter"))
                                Spacer()
                                
                            }
                            HStack{
                                
                                Image(systemName: "phone")
                                    .foregroundColor(Color("darkthemeletter"))
                                
                                Text(self.Phone).font(.custom("Regular_Font".localized(), size: 16))    .foregroundColor(Color("darkthemeletter"))
                                Spacer()
                                Button(action: {
                                    
                                    self.ALERT2 = false
                                    //                                                            self.BLUR = false
                                    self.presentationMode.wrappedValue.dismiss()
                                    
                                    
                                }){
                                    
                                    Text("Change").font(.custom("Bold_Font".localized(), size: 14))    .foregroundColor(Color("darkthemeletter"))
                                }
                                
                                
                            }
                            
                            
                            
                            HStack{
                                Text("Please enter the code sent to your number").font(.custom("Regular_Font".localized(), size: 14)).foregroundColor(Color(
                                                                                                                                                        "MainFontColor1"))
                                
                                
                                Spacer()
                                
                            }
                            
                            
                            TextField("CODE",text: self.$VCode )
                            {
                                
                                
                            }.frame(height : 60)   .multilineTextAlignment(.center)
                            .font(.custom("ExtraBold_Font".localized(), size: 30))  .keyboardType(.numberPad)
                            Divider()
                            
                            //
                            if(self.showAlert){
                                
                                
                                Text(self.errorStatus).font(.custom("Regular_Font".localized(), size: 14)).foregroundColor(Color.red).padding()
                                
                            }
                            
                            
                            
                            Button(action:
                                    
                                    {
                                        
                                        self.showAlert = false
                                        self.ViewLoader = true
                                        
                                        if(self.VERCODE == self.VCode ){ print("Success2")
                                            self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("personal_details/phoneNumber").setValue(self.Phone)
                                            // User re-authenticated.
                                            self.ViewLoader = false
                                            self.ALERT2 = false
                                            self.ALERT1 = false}else{
                                                self.ViewLoader = false
                                                self.showAlert = true
                                                self.errorStatus = "Invalid Code"
                                                
                                            }
                                        
                                        
                                        
                                        //                                    if(self.verificationID != nil){
                                        //
                                        //                                        print("Verification Start\(self.verificationID)")
                                        //
                                        //                                        let credential = PhoneAuthProvider.provider().credential(withVerificationID: self.verificationID!, verificationCode: self.VCode)
                                        //
                                        //
                                        //
                                        //                                        Auth.auth().currentUser?.updatePhoneNumber(credential, completion: { (error) in
                                        //                                            if let error = error {
                                        //                                                // An error happened.
                                        //
                                        //
                                        //                                                let  e : Error = error
                                        //
                                        //                                                let errorAuthStatus = AuthErrorCode.init(rawValue: e._code)!
                                        //
                                        //                                                self.showAlert = true
                                        //
                                        //                                                switch errorAuthStatus {
                                        //
                                        //
                                        //                                                case .invalidPhoneNumber:
                                        //                                                    self.errorStatus = "Invalid Phone Number"
                                        //
                                        //                                                    print("Invalid Phone Number")
                                        //
                                        //                                                case .invalidVerificationCode:
                                        //                                                    self.errorStatus = "Invalid Verification Code"
                                        //                                                    print("Invalid Verification Code")
                                        //                                                case .operationNotAllowed:
                                        //                                                    self.errorStatus = "operationNotAllowed"
                                        //                                                    print("operationNotAllowed")
                                        //                                                case .userDisabled:
                                        //                                                    self.errorStatus = "userDisabled"
                                        //                                                    print("userDisabled")
                                        //                                                case .userNotFound:
                                        //                                                    self.errorStatus = "userNotFound"
                                        //                                                    print("userNotFound")
                                        //                                                //            self.register(auth: Auth.auth())
                                        //                                                case .tooManyRequests:
                                        //                                                    self.errorStatus = "tooManyRequests, oooops"
                                        //                                                    print("tooManyRequests, oooops")
                                        //
                                        //                                                    self.errorStatus = "tooManyRequests, oooops"
                                        //                                                    print("tooManyRequests, oooops")
                                        //                                                default: self.errorStatus = "Number Already Linked to other account"
                                        //                                                    print("Verification Failed")
                                        //                                                }
                                        //
                                        //
                                        //
                                        //                                                     self.ViewLoader = false
                                        //
                                        //                                                print(error)
                                        //                                            } else {
                                        //                                                print("Success2")
                                        //                                                self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("personal_details/phoneNumber").setValue(self.MOBILE)
                                        //                                                // User re-authenticated.
                                        //                                                self.ALERT2 = false
                                        //                                                self.ALERT1 = false
                                        //                                                self.BLUR = false
                                        //
                                        //
                                        //
                                        //                                                var total_time : Int = 0
                                        //
                                        //
                                        //
                                        //                                                //  print("SUCCESS")
                                        //
                                        //                                                var NumberOfServices : Int = 0
                                        //                                                // var ServiceProviders : Int = 0
                                        //
                                        //                                                for cart in self.carts{
                                        //
                                        //
                                        //
                                        //
                                        //
                                        //
                                        //                                                    for Service in cart.services {
                                        //                                                        let S  = (Service.ServiceTime as NSString).doubleValue
                                        //                                                         NumberOfServices += cart.services.count
                                        //
                                        //                                                        total_time = total_time + Int(S)
                                        //
                                        //                                                    }
                                        //
                                        //                                                   // total_time = total_time/60
                                        //                                                  //  total_time = total_time+1
                                        //
                                        //                                                    for addons in cart.addons {
                                        //
                                        //
                                        //
                                        //                                                    }
                                        //
                                        //
                                        //
                                        //                                                }
                                        //
                                        //
                                        //
                                        //
                                        //                                                var pMode =  0
                                        //                                                                                             if(self.PaymentType > 0){
                                        //                                                                                                 pMode = 1
                                        //                                                                                             }
                                        //
                                        //
                                        //
                                        //
                                        //                                                let node = String((Date().timeIntervalSince1970)).replacingOccurrences(of: ".", with: "") + " & " + UserDefaults.standard.string(forKey: "Aid")!
                                        //
                                        //
                                        //
                                        //
                                        //                                               let O = self.findOffers.VehicleDriverLink.filter {
                                        //                                                                                                                                                    $0.DriverID.contains(self.selectedServiceProvider.DriverID)
                                        //                                                                                                                      }
                                        //                                                                                                                      var VNumber = ""
                                        //                                                                                                                      if(O.count>0){
                                        //                                                                                                                          VNumber = O[0].VehicleNumber
                                        //                                                                                                                      }
                                        //
                                        //
                                        //
                                        //                                                print("QQQQQQQQ",self.findOffers.VehicleDriverLink)
                                        //                                                 print("QQQQQQQQ",self.selectedServiceProvider.DriverID)
                                        //
                                        //                                                let Order : [String : Any] = [
                                        //
                                        //
                                        //                                                    "customer_gmail_id" : self.settings.Email,
                                        //                                                    "customer_id" : UserDefaults.standard.string(forKey: "Aid") ?? "0",
                                        //                                                    "customer_latitude": Double(UserDefaults.standard.double(forKey: "latitude")),
                                        //                                                    "customer_longitude" :Double(UserDefaults.standard.double(forKey: "longitude")),
                                        //                                                   "customer_mobile_number":UInt64("91\(self.MOBILE)") ?? "91000000000",
                                        //                                                    "customer_name" : self.NAME+self.LNAME,
                                        //                                                    "customer_raw_address": self.ADDRESS,
                                        //                                                    "driver_id" : self.selectedServiceProvider.DriverID,
                                        //                                                    "number_of_services" :NumberOfServices,
                                        //                                                    "order_id_number":node,
                                        //                                                    "payment_mode" : pMode,
                                        //                                                    "rating": 0,
                                        //                                                    "review" :"",
                                        //                                                    "sp_id":self.selectedServiceProvider.ServiceProvider_id,
                                        //                                                    "sp_name":self.selectedServiceProvider.ServiceProvider,
                                        //                                                    "status":0,
                                        //                                                    "time_order_placed" : [".sv":"timestamp"],
                                        //                                                    "total_discount":self.Discount,
                                        //                                                    "total_due": self.getSubTotal()+self.ServiceCharge-self.Discount,
                                        //                                                    "total_number_of_vehicles": self.carts.count,
                                        //                                                    "total_price_of_order" :self.getSubTotal()+self.ServiceCharge-self.Discount,
                                        //                                                    "total_time":total_time,
                                        //                                                    "order_place_from":"iOS",
                                        //                                                    "twaddanId" :String((Date().timeIntervalSince1970)).replacingOccurrences(of: ".", with: ""),
                                        //                                                    "vehicle_number":   VNumber,
                                        //                                                                                                                                                                              "eta":self.selectedServiceProvider.TimeToArrive,
                                        //
                                        //
                                        //                                                ]
                                        //
                                        //
                                        //
                                        //
                                        //                                              EnterOrderToDB(Node : node,Order: Order,VehicleList:self.VehicleList)
                                        //                                                self.OrderID = node
                                        //                                                self.BLUR = true
                                        //                                                self.ALERT3 = true
                                        //                                                     self.ViewLoader = false
                                        //
                                        //
                                        //
                                        //
                                        //                                            }
                                        //                                        })
                                        //                                        //
                                        //                                        //                                            Auth.auth().currentUser?.link(with: credential, completion: { (authResult, error) in
                                        //                                        //
                                        //                                        //                                                if(error != nil){
                                        //                                        //                                                    print(error!)
                                        //                                        //
                                        //                                        //
                                        //                                        ////                                                     Auth.auth().currentUser?.reauthenticate(with: credential) { error in
                                        //                                        ////                                                     if let error = error {
                                        //                                        ////                                                        // An error happened.
                                        //                                        ////                                                      } else {
                                        //                                        ////
                                        //                                        ////                                                        // User re-authenticated.
                                        //                                        ////                                                      }
                                        //                                        ////                                                    }
                                        //                                        //                                                return
                                        //                                        //                                                }else{
                                        //                                        //                                                    print("Success")
                                        //                                        //                                                    self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("personal_information/phoneNumber").setValue(self.MOBILE)
                                        //                                        //
                                        //                                        //                                                }
                                        //                                        //                                            })
                                        //
                                        //
                                        //
                                        //                                    }else{
                                        //
                                        //                                        print("Verification id is nil")
                                        //                                    }
                                        
                                        
                                        
                                        
                                    }
                                   //                                                        {
                                   //                                                             self.ViewLoader = true
                                   //
                                   ////                                                        if(self.VERCODE == self.VCode ){
                                   ////
                                   ////                                                              self.showAlert = false
                                   ////
                                   ////                                                                                                                                    print("Success2")
                                   ////                                                                                //                                                    self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("personal_details/phoneNumber").setValue(self.MOBILE)
                                   ////                                                                                                                                    // User re-authenticated.
                                   ////
                                   ////
                                   ////                                                                                                                                    self.ALERT2 = false
                                   ////                                                                                                                                    self.ALERT1 = false
                                   ////                                                            //                                                                        self.BLUR = false
                                   ////
                                   ////                                                        }else{
                                   ////                                                            self.showAlert = true
                                   ////                                                            self.errorStatus = "Invalid Phone Number"
                                   ////                                                        }
                                   //
                                   //
                                   //
                                   //                                                        if(self.verificationID != nil){
                                   //
                                   //                                                            print("Verification Start\(self.verificationID)")
                                   //
                                   //                                                            let credential = PhoneAuthProvider.provider().credential(withVerificationID: self.verificationID, verificationCode: self.VCode)
                                   //
                                   //
                                   //
                                   //                                                            Auth.auth().currentUser?.updatePhoneNumber(credential, completion: { (error) in
                                   //                                                                if let error = error {
                                   //                                                                    // An error happened.
                                   //
                                   //
                                   //                                                                    let  e : Error = error
                                   //
                                   //                                                                    let errorAuthStatus = AuthErrorCode.init(rawValue: e._code)!
                                   //
                                   //                                                                    self.showAlert = true
                                   //
                                   //                                                                    switch errorAuthStatus {
                                   //
                                   //
                                   //                                                                    case .invalidPhoneNumber:
                                   //                                                                        self.errorStatus = "Invalid Phone Number"
                                   //
                                   //                                                                        print("Invalid Phone Number")
                                   //
                                   //                                                                    case .invalidVerificationCode:
                                   //                                                                        self.errorStatus = "Invalid Verification Code"
                                   //                                                                        print("Invalid Verification Code")
                                   //                                                                    case .operationNotAllowed:
                                   //                                                                        self.errorStatus = "operationNotAllowed"
                                   //                                                                        print("operationNotAllowed")
                                   //                                                                    case .userDisabled:
                                   //                                                                        self.errorStatus = "userDisabled"
                                   //                                                                        print("userDisabled")
                                   //                                                                    case .userNotFound:
                                   //                                                                        self.errorStatus = "userNotFound"
                                   //                                                                        print("userNotFound")
                                   //                                                                    //            self.register(auth: Auth.auth())
                                   //                                                                    case .tooManyRequests:
                                   //                                                                        self.errorStatus = "tooManyRequests, oooops"
                                   //                                                                        print("tooManyRequests, oooops")
                                   //
                                   //                                                                        self.errorStatus = "tooManyRequests, oooops"
                                   //                                                                        print("tooManyRequests, oooops")
                                   //                                                                    default: self.errorStatus = "Number Already Linked to other account"
                                   //                                                                        print("Verification Failed")
                                   //                                                                    }
                                   //
                                   //
                                   //
                                   //                                                                         self.ViewLoader = false
                                   //
                                   //                                                                    print(error)
                                   //                                                                } else { print("Success2")
                                   //                                                                                                                                       self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("personal_details/phoneNumber").setValue(self.Phone)
                                   //                                                                                                                                       // User re-authenticated.
                                   //                                                                  self.ViewLoader = false
                                   //                                                                                                                                       self.ALERT2 = false
                                   //                                                                                                                                       self.ALERT1 = false}
                                   //                                                            })
                                   //                                                            //
                                   //                                                            //                                            Auth.auth().currentUser?.link(with: credential, completion: { (authResult, error) in
                                   //                                                            //
                                   //                                                            //                                                if(error != nil){
                                   //                                                            //                                                    print(error!)
                                   //                                                            //
                                   //                                                            //
                                   //                                                            ////                                                     Auth.auth().currentUser?.reauthenticate(with: credential) { error in
                                   //                                                            ////                                                     if let error = error {
                                   //                                                            ////                                                        // An error happened.
                                   //                                                            ////                                                      } else {
                                   //                                                            ////
                                   //                                                            ////                                                        // User re-authenticated.
                                   //                                                            ////                                                      }
                                   //                                                            ////                                                    }
                                   //                                                            //                                                return
                                   //                                                            //                                                }else{
                                   //                                                            //                                                    print("Success")
                                   //                                                            //                                                    self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("personal_information/phoneNumber").setValue(self.MOBILE)
                                   //                                                            //
                                   //                                                            //                                                }
                                   //                                                            //                                            })
                                   //
                                   //
                                   //
                                   //                                                        }else{
                                   //
                                   //                                                            print("Verification id is nil")
                                   //                                                        }
                                   //
                                   //
                                   //
                                   //
                                   //                                                    }
                            ){
                                
                                
                                if(!self.showAlert && self.errorStatus != "Invalid Code"){
                                    
                                    HStack{
                                        
                                        
                                        
                                        Spacer()
                                        
                                        Text("Verify")   .font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color.white)
                                        Spacer()
                                        
                                        
                                        
                                    }.padding().frame(width: 200)
                                    //                            .border(Color("ManualSignInGray"), width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                                    
                                    //                                                               .frame(width: 300.0, height: 50)      .background(Color("su1"))
                                    //                                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color("su1"),lineWidth: 1))
                                    .background(Color("su1"))
                                    .cornerRadius(10)
                                    
                                    
                                    //
                                    //
                                    //                                    RoundedPanel()
                                    //
                                    //                                        .overlay(Text("Verify")
                                    //                                            .font(.custom("ExtraBold_Font".localized(), size: 20))
                                    //
                                    //                                            .foregroundColor(Color.white))
                                    //                                        .background(Color(red: 0.24, green: 0.56, blue: 0.87, opacity: 1.0))
                                    //                                        .cornerRadius(5)
                                    //                                    }   .frame(width: 200).padding()
                                    //
                                    //
                                }
                            }
                            HStack{
                                
                                Spacer()
                                //
                                
                                Button(action: {
                                    
                                    self.ALERT2 = false
                                    self.ALERT1 = false
                                    //                                                            self.BLUR = false
                                }){
                                    
                                    Text("Cancel").font(.custom("ExtraBold_Font".localized(), size: 17))
                                }.padding()
                                
                            }
                        }.padding(20)
                        
                        
                    }.background(Color.white).clipped().shadow(radius: 1)
                    .padding(40)
                    .cornerRadius(5)
                    
                }
                
                
                if(self.ALERT3){
                    ZStack{
                        
                        VStack{
                            
                            HStack{
                                Text("Wrong Number Format").font(.custom("Bold_Font".localized(), size: 17))
                                    .foregroundColor(Color("darkthemeletter"))
                                
                                
                            }
                            
                            
                            
                            
                            HStack{
                                
                                Text("Please check Phone number").font(.custom("Regular_Font".localized(), size: 16)).padding()
                                    .foregroundColor(Color("darkthemeletter"))
                                
                                
                            }
                            
                            
                            //                                                                    HStack{
                            //                                                                        Text(getTime(timeIntervel: self.selectedServiceProvider.TimeToArrive)).font(.custom("ExtraBold_Font".localized(), size: 30))
                            //                                                                                                                                               .foregroundColor(Color("darkthemeletter"))
                            //
                            //
                            //                                                                                                                                                          }
                            
                            
                            
                            
                            HStack{
                                
                                Spacer()
                                
                                
                                Button(action: {
                                    
                                    self.ALERT3 = false
                                }){
                                    
                                    Text("OK").font(.custom("ExtraBold_Font".localized(), size: 17))
                                        .padding(.trailing).padding(.top,20)
                                }
                                
                            }
                            
                            //                                            if(self.showAlert){
                            //
                            //
                            //                                                                          Text(self.errorStatus).font(.custom("Regular_Font".localized(), size: 14)).foregroundColor(Color.red).padding()
                            //
                            //                                                                                                    }
                        }.frame(width :320 , height: 150)
                        
                        .padding()
                        
                    }.background(Color.white).clipped().shadow(radius: 2)
                }
                
                if(self.ViewLoader){
                    Loader()
                }
                
                
            }.onAppear{
                
                self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").observe(DataEventType.value) { (dataSnapshot) in
                    if(self.Phone == ""){
                        self.Phone = ""
                        
                        if(dataSnapshot.childSnapshot(forPath:"personal_details/phoneNumber").exists()){
                            self.Phone = String(describing :  (dataSnapshot.childSnapshot(forPath:"personal_details/phoneNumber").value) ?? "")
                        }
                    }
                    if(dataSnapshot.childSnapshot(forPath:"personal_details/name").value != nil){
                        self.Name = String(describing :  (dataSnapshot.childSnapshot(forPath:"personal_details/name").value) ?? "")
                        self.Photo = String(describing :  (dataSnapshot.childSnapshot(forPath:"personal_details/dp").value) ?? "")
                        self.Email = String(describing :  (dataSnapshot.childSnapshot(forPath:"personal_details/email").value) ?? "")
                        self.SecondName = String(describing :  (dataSnapshot.childSnapshot(forPath:"personal_details/lastName").value) ?? "")
                    }
                }
            }.onDisappear{
                self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("personal_details/name").setValue(self.Name)
                self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("personal_details/lastName").setValue(self.SecondName)
                //                     self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("personal_details/phoneNumber").setValue(self.Phone)
            }
            //            }
            
        }    .navigationBarTitle("Account Info",displayMode: .inline)
        .navigationBarItems(trailing: EditButton())
        .environment(\.editMode, $Edit)
        .navigationBarHidden(false)
        .edgesIgnoringSafeArea(.all)
        
    }
}


//
//struct MyProfile_Previews: PreviewProvider {
//    static var previews: some View {
//       MyProfile()
//    }
//}
