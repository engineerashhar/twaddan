//
//  MyOrders.swift
//  Twaddan
//
//  Created by Spine on 15/08/20.
//  Copyright © 2020 spine. All rights reserved.
//
import Firebase
import SwiftUI

import FirebaseDatabase
import SDWebImageSwiftUI
import MapKit
import FirebaseAuth


struct MyOrdersO: View {
    
    @State var Snap:DataSnapshot = DataSnapshot()
    
    @ObservedObject private var locationManager = LocationManager()
    @State var SELECT : Int = 1
    @State private var offset = CGSize.zero
    
    @EnvironmentObject var settings : UserSettings
    
    @State var ActiveOrders :[Bookings] = [Bookings]()
    @State var PastOrders :[Bookings] = [Bookings]()
    @State var AllOrders :[Bookings] = [Bookings]()
    //        @State var PastOrderSets :[pastOrderSet] = [pastOrderSet]()
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    let ref = Database.database().reference()
    @State var selectedOptions : Int = 0
    @State var Output : Bookings = Bookings()
    
    
    @State var OpenPartialSheet:Bool = false
    @State var goToMap:Bool = false
    //       @State var LastAddress:[String:String] = [String:String]()
    
    @State var item:Bookings = Bookings()
    
    @State var OpenCart : Bool = false
    
    
    
    @State var Driver:String = ""
    @State var ETA:Double = 0.0
    @State var SuccessETA :String = "0"
    
    @State var goToSplash:Bool = false
    @State var Ano:Bool = Auth.auth().currentUser!.isAnonymous
    
    
    
    var body: some View {
        
        
        GeometryReader { geometry in
            
            VStack{
                
                NavigationLink(destination: SignUpSelection(FromDA :.constant(false))
                               //                                    .navigationBarTitle(,width"").navigationBarHidden(true)
                               , isActive: self.$goToSplash){
                    Text("").frame(height:0)
                }
                
                if(!self.Ano){
                    
                    
                    ZStack{
                        
                        
                        
                        NavigationLink(destination: Cart(GoToBookDetailsOld: .constant(false))
                                        
                                        .environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                                        )), isActive: self.$OpenCart){
                            
                            Text("").frame(height:0)
                        }
                        
                        VStack  {
                            
                            //                                        if(self.length){
                            //                                        Spacer().frame(height:geometry.size.height*0.1)
                            //                                        }
                            
                            
                            //                           .frame(height:150)
                            
                            
                            
                            //                    Picker(selection: $selectedOptions, label: Text("")) {
                            //                        Text("ALL").font(.custom("Bold_Font".localized(), size: 16)).foregroundColor(Color("MainFontColor1")).tag(0)
                            //                        Text("ONGOING").font(.custom("Bold_Font".localized(), size: 16)).foregroundColor(Color("MainFontColor1")).tag(1)
                            //                        Text("COMPLETED").font(.custom("Bold_Font".localized(), size: 16)).foregroundColor(Color("MainFontColor1")).tag(2)
                            //                    }
                            //
                            //                    .pickerStyle(SegmentedPickerStyle()).font(.custom("Bold_Font".localized(), size: 16)).foregroundColor(Color("MainFontColor1")).background(Color("backcolor2"))
                            //
                            
                            
                            //                                        if((self.PastOrders).count != 0){
                            
                            DisplayNow(OpenPartialSheet: self.$OpenPartialSheet, item: self.$item, Data: self.$PastOrders,Driver:self.$Driver,SuccessETA: self.$SuccessETA,ETA:self.$ETA,OpenCart: self.$OpenCart, Snap: self.$Snap)
                            
                            Spacer()
                            //                                        }else
                            
                            if((self.PastOrders).count == 0){
                                
                                
                                
                                
                                
                                
                                VStack(alignment:.center){
                                    
                                    Spacer().frame(height:geometry.size.height*0.2)
                                    VStack{
                                        Image("cart").renderingMode(.template).resizable().frame(width:77, height:77).foregroundColor(Color(#colorLiteral(red: 0, green: 0.2269999981, blue: 0.5329999924, alpha: 1)))
                                        Text("You don't have any order yet").font(.custom("Regular_Font".localized(), size: 14)).foregroundColor(Color(#colorLiteral(red: 0.5059999824, green: 0.5099999905, blue: 0.5139999986, alpha: 1))).padding()
                                        
                                        Button(action: {
                                            self.goToSplash = true
                                        }){
                                            HStack {
                                                
                                                
                                                
                                                Spacer()
                                                
                                                Text("Login")   .font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color.white)
                                                Spacer()
                                                
                                                
                                                
                                            }.padding()
                                            //                            .border(Color("ManualSignInGray"), width: 1)
                                            
                                            //                                                               .frame(width: 300.0, height: 50)      .background(Color("su1"))
                                            //                                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color("su1"),lineWidth: 1))
                                            .background(Color("su1"))
                                            .cornerRadius(10)
                                        }.padding().hidden()
                                    }
                                    Spacer()
                                }.frame(height:geometry.size.height)
                                
                                
                                
                                
                                
                                
                                
                                
                            }
                            
                        }
                        //                .navigationBarTitle("Your Orders",displayMode: .inline)
                        VStack{
                            Text("")
                        }.frame(width:geometry.size.width,height:geometry.size.height)  .opacity(self.OpenPartialSheet ? 1:0)  .brightness(self.OpenPartialSheet ? -0.2:0)
                        VStack{
                            LocForReorder(OpenPartialSheet: self.$OpenPartialSheet, goToMap: self.$goToMap, PastOrders: self.PastOrders, bookings: self.$item, Driver: self.$Driver, ETA: self.$ETA, SuccessETA: self.$SuccessETA,Output: self.$Output,LocationName: .constant(self.locationManager.locationName ?? ""),Location:.constant(self.locationManager.location ?? CLLocation(latitude: 0.0, longitude: 0.0) ))
                            
                            
                        }
                        
                    }
                    
                    //             }
                    
                    .background(Color("backcolor2"))
                }else{
                    
                    VStack(alignment:.center){
                        
                        Spacer().frame(height:geometry.size.height*0.2)
                        VStack{
                            Image("cart").renderingMode(.template).resizable().frame(width:77, height:77).foregroundColor(Color(#colorLiteral(red: 0, green: 0.2269999981, blue: 0.5329999924, alpha: 1)))
                            Text("Login to see your orders").font(.custom("Regular_Font".localized(), size: 14)).foregroundColor(Color(#colorLiteral(red: 0.5059999824, green: 0.5099999905, blue: 0.5139999986, alpha: 1))).padding()
                            
                            Button(action: {
                                self.goToSplash = true
                            }){
                                HStack {
                                    
                                    
                                    
                                    Spacer()
                                    
                                    Text("Login")   .font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color.white)
                                    Spacer()
                                    
                                    
                                    
                                }.padding()
                                //                            .border(Color("ManualSignInGray"), width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                                
                                //                                                               .frame(width: 300.0, height: 50)      .background(Color("su1"))
                                //                                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color("su1"),lineWidth: 1))
                                .background(Color("su1"))
                                .cornerRadius(10)
                            }.padding()
                        }
                        Spacer()
                    }.frame(height:geometry.size.height)
                    
                    
                }
                
            }.background(Color.white).onAppear{
                self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").observe(DataEventType.value) { (usersnap) in
                    
                    //                                self.ActiveOrders.removeAll()
                    
                    //                                for AIDs in usersnap.childSnapshot(forPath: "active_orders/order_ids").children{
                    //                                    let AIDsSnap : DataSnapshot = AIDs as! DataSnapshot
                    //
                    //                                    self.ref.child("orders").child("all_orders").child(AIDsSnap.key).observeSingleEvent(of: DataEventType.value) { (ordersnap) in
                    //
                    //                                        self.ref.child("service_providers").child(String(describing :  (ordersnap.childSnapshot(forPath:"sp_id").value)!)).observeSingleEvent(of: DataEventType.value) { (spsnap) in
                    //
                    //                                            var allservice: String = ""
                    //
                    //                                            for S in ordersnap.childSnapshot(forPath: "services").children{
                    //                                                let Ssnap : DataSnapshot = S as! DataSnapshot
                    //
                    //
                    //                                                for Se in Ssnap.childSnapshot(forPath: "services").children{
                    //                                                    let SeSnap : DataSnapshot = Se as! DataSnapshot
                    //
                    //                                                    allservice = allservice + " , " + SeSnap.key
                    //
                    //                                                }
                    //
                    //                                            }
                    //                                            let ratings:Double = -1
                    //                                                                  let time:Double = 0
                    //
                    //                                            let bookings : Bookings = Bookings(Amount: String(describing :  (ordersnap.childSnapshot(forPath:"total_price_of_order").value)!), Status: "Booked", ServiceProvider: String(describing :  (spsnap.childSnapshot(forPath: "personal_information/name".localized()).value)!), Services: allservice, ServiceLocation: String(describing :  (ordersnap.childSnapshot(forPath:"customer_raw_address").value)!), Image: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/image").value)!),PhoneNumber: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/phone_number").value)!),OrderID: AIDsSnap.key,ServiceProviderID:spsnap.key ,DriverID:String(describing :  (ordersnap.childSnapshot(forPath:"driver_id").value)!) ,ServiceSnap: spsnap, OrderSnap: ordersnap,latitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_latitude").value)!),longitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_longitude").value)!),rating: ratings,time: time)
                    //
                    //                                            self.ActiveOrders.append(bookings)
                    //                                        }
                    //                                    }
                    //                                }
                    
                    //                                self.PastOrders.removeAll()
                    //                        self.PastOrderSets.removeAll()
                    var PastOrdersN :[Bookings] = [Bookings]()
                    for PIDs in usersnap.childSnapshot(forPath: "past_orders/order_ids").children{
                        let PIDsSnap : DataSnapshot = PIDs as! DataSnapshot
                        
                        self.ref.child("orders").child("all_orders").child(PIDsSnap.key).observeSingleEvent(of: DataEventType.value) { (ordersnap) in
                            
                            self.ref.child("service_providers").child(String(describing :  (ordersnap.childSnapshot(forPath:"sp_id").value)!)).observeSingleEvent(of: DataEventType.value) { (spsnap) in
                                
                                
                                
                                self.ref.child("service_providers_ratings").child(String(describing :  (ordersnap.childSnapshot(forPath:"sp_id").value)!)).child(ordersnap.key).observeSingleEvent(of: DataEventType.value) { (rateSnap) in
                                    let rateSnap : DataSnapshot = rateSnap as! DataSnapshot
                                    
                                    
                                    var allservice: String = ""
                                    
                                    for S in ordersnap.childSnapshot(forPath: "services").children{
                                        let Ssnap : DataSnapshot = S as! DataSnapshot
                                        
                                        
                                        for Se in Ssnap.childSnapshot(forPath: "services").children{
                                            let SeSnap : DataSnapshot = Se as! DataSnapshot
                                            
                                            allservice = allservice + " , " + SeSnap.key
                                            
                                        }
                                        
                                    }
                                    
                                    //
                                    //                                    self.LastAddress = ["Address":String(describing :  (ordersnap.childSnapshot(forPath:"customer_raw_address").value)!)
                                    //                                                  , "latitude": String(describing :  (ordersnap.childSnapshot(forPath:"customer_latitude").value)!),"longitude":String(describing :  (ordersnap.childSnapshot(forPath:"customer_longitude").value)!)]
                                    
                                    let milisecond:Int = Int(String(describing :  (ordersnap.childSnapshot(forPath:"time_order_placed").value)!)) ?? 0;
                                    //                                                                                           print("KKKKKK",milisecond)
                                    let dateVar = Date.init(timeIntervalSince1970: TimeInterval(milisecond)/1000)
                                    var dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "dd-MM-yyyy"
                                    print(dateFormatter.string(from: dateVar))
                                    
                                    dateFormatter.locale = Locale(identifier: UserDefaults.standard.string(forKey: "LANSEL") ?? "en")
                                    
                                    let pO : pastOrderSet = pastOrderSet(id: spsnap.key, driverID: String(describing :  (ordersnap.childSnapshot(forPath:"driver_id").value)!), SpName: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/name".localized()).value)!), Date: dateFormatter.string(from: dateVar), Image: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/image").value)!),snap: spsnap,Address:String(describing :  (ordersnap.childSnapshot(forPath:"customer_raw_address").value)!),latitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_latitude").value)!),longitude:String(describing :  (ordersnap.childSnapshot(forPath:"customer_longitude").value)!), OrderSnap: ordersnap )
                                    
                                    //                                    self.PastOrderSets.append(pO)
                                    var ratings:Double = 0
                                    
                                    if(!rateSnap.exists()){
                                        
                                        ratings = -1
                                    }else{
                                        ratings =   Double(String(describing:rateSnap.childSnapshot(forPath:"rating").value!)) ?? 0.0
                                    }
                                    
                                    print("000000",ratings)
                                    let time:Double = Double(String(describing:ordersnap.childSnapshot(forPath:"time_order_placed").value!)) ?? 0.0
                                    let bookings : Bookings = Bookings(Amount: String(describing :  (ordersnap.childSnapshot(forPath:"total_price_of_order").value)!), Status: "Completed", ServiceProvider: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/name".localized()).value)!), Services: allservice, ServiceLocation: String(describing :  (ordersnap.childSnapshot(forPath:"customer_raw_address").value)!), Image: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/image").value)!),PhoneNumber: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/phone_number").value)!),OrderID: PIDsSnap.key,ServiceProviderID:spsnap.key,DriverID:String(describing :  (ordersnap.childSnapshot(forPath:"driver_id").value)!) ,ServiceSnap: spsnap, OrderSnap: ordersnap ,latitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_latitude").value)!),longitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_longitude").value)!),rating: ratings,time:time)
                                    
                                    PastOrdersN.append(bookings)
                                    self.PastOrders.append(contentsOf: PastOrdersN)
                                    
                                }
                            }
                        }
                    }
                    
                    
                    
                }
                
            }
            //                .navigationBarTitle("Your Orders",displayMode: .inline)
            
        }
        //             .navigationBarTitle("Your Orders",displayMode: .inline)
        //            .navigationBarHidden(true)
        .edgesIgnoringSafeArea(.all)
    }
    
    func getAppendedData(Data1:[Bookings],Data2:[Bookings]) -> [Bookings] {
        
        let Data3 = Data1+Data2
        return Data3
    }
    
    
    
    
    
}

struct MyOrders_Previews: PreviewProvider {
    static var previews: some View {
        MyOrdersO()
    }
}



struct DisplayNow: View {
    
    
    
    @Binding var OpenPartialSheet:Bool
    //        @Binding var goToMap:Bool
    //       @Binding var LastAddress:[String:String]
    
    @State var length:Bool = false
    
    @Binding var item:Bookings
    
    
    
    
    
    @EnvironmentObject var settings : UserSettings
    @Binding var Data : [Bookings]
    @Binding var Driver:String
    @Binding var SuccessETA :String
    
    @Binding var ETA:Double
    
    @Binding var OpenCart : Bool
    @State var goToTrack : Bool = false
    @State var SOrderID : String = ""
    
    @State var Stage : Int = 0
    
    @State var ViewLoader:Bool = false
    
    
    
    @Binding var Snap:DataSnapshot
    @State var  GoToBookDetails:Bool = false
    
    //        @State var Reorder :String = "0"
    let ref = Database.database().reference()
    var body: some View {
        ZStack{
            
            
            NavigationLink(destination: Cart(GoToBookDetailsOld: .constant(false))
                            
                            .environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                            )), isActive: self.$OpenCart){
                EmptyView()
                
            }
            
            VStack{
                //                if(self.length){
                Spacer().frame(height:UIScreen.main.bounds.height*0.1)
                //                }
                List{
                    
                    //                Spacer().frame(height:UIScreen.main.bounds.height*0.05)
                    ForEach(Data.reversed(),id: \.self){ bookings in
                        
                        PastOrderView(item: bookings, ETA: self.$ETA, Output: self.$item, SuccessETA: self.$SuccessETA, OpenCart: self.$OpenCart, selectedOrderID: self.$SOrderID, Stage: self.$Stage, selecteditem: self.$item, OpenReorderSheet: self.$OpenPartialSheet, Driver: self.$Driver)
                            .frame(height:111).background(Color.white).cornerRadius(10).clipped().shadow(radius: 5).padding(.horizontal,10).padding(.top,10).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                            ))
                        
                        
                    }.onDelete(perform: self.removeRows)
                    .listRowInsets(EdgeInsets())
                    .listRowBackground(Color("h1"))
                    .buttonStyle(BorderlessButtonStyle())
                    .sheet(isPresented:self.$goToTrack ){
                        TrackServiceProvider(openTrack:self.$goToTrack,OrderID :self.SOrderID, goToHome: .constant(false)).environment(\.colorScheme, .light)
                    }
                    
                    
                    
                    
                    
                    
                }
                .environment(\.locale,.init(identifier:"en"
                ))
                
            }
            
            //            .onTapGesture {
            //                self.OpenCart = true
            //            }
            //        .sheet(isPresented: self.$GoToBookDetails){
            //
            //
            //
            //            BookingDetails(OpenCart: self.$OpenCart,GoToBookDetails:self.$GoToBookDetails ,serviceSnap: self.Snap, SelectedDriver:
            //                self.Driver,ETA: self.ETA,Reorder:self.Reorder).environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
            //        ))
            //
            //
            //
            //        }
            
        }.navigationBarTitle("Your Orders",displayMode: .inline)
        
        
    }
    func removeRows(at offsets: IndexSet) {
        //  numbers.remove(atOffsets: offsets)
        guard let index : Int = Array(offsets).first else { return }
        self.length = true
        let ID =  self.Data[index].OrderID
        //        self.Data.remove(at: index)
        print(ID)
        self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("past_orders/order_ids").child(ID).removeValue()
    }
    
    func ReOrderToCart(booking:Bookings,DriverID:String,ETA:Double) {
        
        
        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").removeValue()
        
        let node = String((Date().timeIntervalSince1970)).replacingOccurrences(of: ".", with: "")
        
        for service in booking.OrderSnap.childSnapshot(forPath: "services").children{
            let serviceSnap = service as! DataSnapshot
            let Vehicle = serviceSnap.key.components(separatedBy: ["&"])[0].trimmingCharacters(in: .whitespacesAndNewlines)
            
            
            self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("services").setValue(serviceSnap.childSnapshot(forPath: "services").value)
            self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("addons").setValue(serviceSnap.childSnapshot(forPath: "add_on_services").value)
            
            self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("Vehicle").setValue(Vehicle)
            
        }
        
        
        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider").setValue(booking.ServiceProvider)
        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("DriverID").setValue(DriverID)
        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ETA").setValue(ETA)
        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider_id").setValue(booking.ServiceSnap.key)
        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider_image").setValue(booking.ServiceSnap.childSnapshot(forPath:"personal_information/image").value!)
        
        
        
    }
}



struct PastOrderView:View{
    
    
    var item : Bookings
    
    @Binding var ETA:Double
    @Binding var Output : Bookings
    @Binding var SuccessETA:String
    
    @Binding var OpenCart : Bool
    @Binding var selectedOrderID : String
    @Binding var Stage : Int
    @Binding var selecteditem:Bookings
    
    @Binding var OpenReorderSheet : Bool
    
    @ObservedObject private var locationManager = LocationManager()
    @Binding var Driver:String
    //      @State var ETA:Double = 0.0
    //      @State var SuccessETA :String = "0"
    var body: some View {
        
        VStack  {
            
            
            
            HStack(alignment:.top){
                
                HStack{
                    AnimatedImage(url : URL (string: item.Image)).resizable().frame(width:44,height: 44).padding(10)
                    
                    
                    VStack(alignment:.leading){
                        Text(item.ServiceProvider).font(.custom("Regular_Font".localized(), size: 14))
                            .foregroundColor(Color("h5"))
                        Text(getDate(timeinmilli:  item.time))
                            .font(.custom("Regular_Font".localized(), size: 12))
                            .foregroundColor(Color("h6"))
                        
                        if(self.Output.OrderID == item.OrderID && self.SuccessETA == "1"){
                            
                            Text(getTime(timeIntervel: self.ETA))
                                .foregroundColor(Color("h5"))                                              .font(.custom("Regular_Font".localized(), size: 12))
                            //                                                                                                  Button(action:{
                            //
                            //                                                                                                                                                                                              self.ReOrderToCart(booking:item,DriverID:self.Driver,ETA:self.ETA)
                            //
                            //                                                                                                                                                                                              self.OpenCart = true
                            //
                            //
                            //                                                                                                                                                                                          })
                            //                                                                                              {
                            //
                            //                                                                                                                                        Text("BOOK AGAIN").font(.custom("Regular_Font".localized(), size: 12))
                            //                                                                                                                                            .foregroundColor(Color.black)
                            //                                                                                                                                                                                                                                                                                                       .padding(.horizontal).padding(.vertical,3)
                            //
                            //
                            //
                            //                                                                                                                                                                                                                                                                   .cornerRadius(13)
                            //                                                                                                                                                                                                                                                                   .overlay(
                            //                                                                                                                                                                                                                                                               RoundedRectangle(cornerRadius: 30).stroke(Color("c1"),lineWidth: 3)
                            //
                            //
                            //                                                                                                                                                                                                                                                               )
                            //                                                                                                                                        }
                            
                            HStack{
                                
                                Button(action:{
                                    
                                    ReOrderToCart(booking:self.item,DriverID:self.Driver,ETA:self.ETA)
                                    
                                    self.OpenCart = true
                                    
                                    
                                }){
                                    
                                    Text("ORDER NOW").font(.custom("Regular_Font".localized(), size: 12))
                                        .foregroundColor(Color.black)
                                        .padding(.horizontal).padding(.vertical,2)
                                        
                                        
                                        
                                        .cornerRadius(13)
                                        .overlay(
                                            RoundedRectangle(cornerRadius: 30).stroke(Color("c1"),lineWidth: 3)
                                            
                                            
                                        )
                                }
                                if (false) {
                                    if(item.rating == -1){
                                        Button(action:{
                                            
                                            self.selectedOrderID = self.item.OrderID
                                            self.Stage = 1
                                            
                                        }){
                                            
                                            HStack{
                                                Image("Component 36 – 3.pdf").resizable().frame(width:12,height:12)
                                                Text("RATE").font(.custom("Regular_Font".localized(), size: 12))
                                            }
                                            .foregroundColor(Color.black)
                                            .padding(.horizontal).padding(.vertical,3)
                                            
                                            
                                            
                                            .cornerRadius(13)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 30).stroke(Color("c1"),lineWidth: 3)
                                                
                                                
                                            )
                                        }
                                    }                                                                                                                                                         }
                            }.padding(.vertical,5)
                            
                        }
                        else{
                            
                            RatingView(item:.constant(item)).padding(.top,-5)
                            
                            Spacer()
                            
                            if(self.SuccessETA == "2" && self.Output.OrderID == item.OrderID){
                                Text("Unavailable")
                                    .font(.custom("Regular_Font".localized(), size: 12))
                            }else{
                                
                                //                                                                                                                                            Button(action: {
                                //                                                                                                                                                                                                                                                                              self.item = item
                                //                                                                                                                                                                                                                                                                              self.OpenReorderSheet = true
                                //
                                //                                                                                                                                                                                                                                                                          })
                                //                                                                                                                                            {
                                //                                                                                                                                        Text("BOOK AGAIN").font(.custom("Regular_Font".localized(), size: 12))
                                //                                                                                                                                                                                                                                                                   .foregroundColor(Color("h2"))
                                //                                                                                                                                                                                                                                                                .padding(.horizontal).padding(.vertical,3)
                                //
                                //                                                                                                                                                                                                                                                                .background(Color.white)
                                //
                                //                                                                                                                                                                                                                                                                   .cornerRadius(13)
                                //                                                                                                                                                                                                                                                                   .overlay(
                                //                                                                                                                                                                                                                                                                    RoundedRectangle(cornerRadius: 30).stroke(Color("t4"),lineWidth: 1)
                                //
                                //                                                                                                                                                                                                                                                               )
                                //                                                                                                                                        }
                                
                                
                                HStack{
                                    
                                    Button(action:{
                                        
                                        self.selecteditem = self.item
                                        self.OpenReorderSheet = true
                                        
                                        //                                                                                                                            self.locationManager.getLocationName()
                                        
                                    }){
                                        
                                        Text("BOOK AGAIN").font(.custom("Regular_Font".localized(), size: 12))
                                            .foregroundColor(Color(#colorLiteral(red: 0, green: 0.2269999981, blue: 0.5329999924, alpha: 1)))
                                            .padding(.horizontal,10).padding(.vertical,3)
                                            
                                            
                                            
                                            .cornerRadius(13)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 30).stroke(Color(#colorLiteral(red: 0.6859999895, green: 0.6859999895, blue: 0.6859999895, alpha: 1)),lineWidth: 1)
                                                
                                                
                                            )
                                    }
                                    if (item.rating == -1 && !(Auth.auth().currentUser?.isAnonymous ?? true) ) {
                                        
                                        Button(action:{
                                            
                                            self.selectedOrderID = self.item.OrderID
                                            self.Stage = 1
                                            
                                        }){
                                            
                                            HStack{
                                                Image("Component 36 – 3").resizable().frame(width:12,height:12)
                                                Text("RATE").font(.custom("Regular_Font".localized(), size: 12))
                                            }
                                            .foregroundColor(Color(#colorLiteral(red: 0, green: 0.2269999981, blue: 0.5329999924, alpha: 1)))
                                            .padding(.horizontal,10).padding(.vertical,3)
                                            
                                            
                                            
                                            .cornerRadius(13)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 30).stroke(Color("s1"),lineWidth: 1)
                                                
                                                
                                            )
                                        }
                                    }
                                }
                                
                            }
                            
                        }
                    }
                    
                }
                Spacer()
            }.padding(10)
            
            
            //                                                                                                                                        if(self.Output.OrderID == item.OrderID && self.SuccessETA == "1"){
            //                                                                                                                                            Text(getTime(timeIntervel: self.ETA)).font(.custom("Regular_Font".localized(), size: 10)).multilineTextAlignment(.center)
            //                                                                                                                                                .frame(width: 90, height: 90)
            //                                                                                                                                                .background(Color.white).clipped() .clipShape(Circle()).shadow(radius: 10)
            //                                                                                                                                        }else{
            //                                                                                                                                            AnimatedImage(url : URL (string: item.Image))
            //                                                                                                                                                .frame(width: 80, height: 56)
            //                                                                                                                                                .clipShape(Circle())
            //                                                                                                                                        }
            //                                 .overlay(
            //                                     Circle().stroke(Color.white, lineWidth: 1))
            //                                 .shadow(radius: 1)
            
            //                                                                                                                                        Text(item.ServiceProvider).font(.custom("ExtraBold_Font".localized(), size: 12)).frame(width:80 ,height: 20)
            //                                                                                                                                        if(self.Output.OrderID == item.OrderID){
            //                                                                                                                                            if(self.SuccessETA == "1"){
            //                                                                                                                                                Button(action:{
            //
            //                                                                                                                                                    self.ReOrderToCart(booking:item,DriverID:self.Driver,ETA:self.ETA)
            //
            //                                                                                                                                                    self.OpenCart = true
            //
            //
            //                                                                                                                                                }){
            //
            //                                                                                                                                                    Text("Book").font(.custom("Regular_Font".localized(), size: 14))
            //                                                                                                                                                        .foregroundColor(Color.white)
            //
            //                                                                                                                                                        // .padding(.horizontal,40)
            //                                                                                                                                                        .frame(width : 100, height: 24)
            //
            //                                                                                                                                                        .background(Color("ManBackColor"))
            //
            //                                                                                                                                                        .cornerRadius(13)
            //
            //                                                                                                                                                }
            //
            //                                                                                                                                            }else if(self.SuccessETA == "2"){
            //                                                                                                                                                Text("Unavailable")
            //                                                                                                                                                    .font(.custom("Regular_Font".localized(), size: 12))
            //                                                                                                                                            }
            
        }
        
        
        
        
        
        
        
    }
    
}
