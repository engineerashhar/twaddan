//
//  GetHelp.swift
//  Twaddan
//
//  Created by Spine on 15/08/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI

struct GetHelp: View {
    var body: some View {
        
        VStack{
            
            Spacer().frame(height:UIScreen.main.bounds.height * 0.1)
            
            Button(action:{
                
                let email = "Support@twaddan.com"
                if let url = URL(string: "mailto:\(email)") {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
                
            }){
                
                RoundedCorners(color: Color.white, tl: 10, tr: 10, bl: 10, br: 10).frame(width:UIScreen.main.bounds.width*0.9,height:111)
                    .padding(.horizontal).padding(.vertical,1).overlay( HStack {
                        
                        Spacer().frame(width:25)
                        Group{
                            
                            
                            ZStack{
                                
                                
                                Image("Path 60").renderingMode(.template).resizable().foregroundColor(Color.white).frame(width:30,height: 20).padding(20).clipShape(Circle()).background(LinearGradient(gradient: Gradient(colors: [Color("h3"),Color("h2")]), startPoint: .leading, endPoint: .trailing)).clipShape(Circle())
                                
                            }
                            
                        }
                        VStack(alignment:.leading){
                            
                            VStack(alignment:.leading){
                                Text("Write Us").foregroundColor(Color.black).font(.custom("ExtraBold_Font".localized(), size: 14))
                                
                                
                                Text("For assistance please contact is").font(.custom("Regular_Font".localized(), size: 14)).padding(.vertical,5).foregroundColor(Color("s1"))
                            }
                            
                            Text("Support@twaddan.com").foregroundColor(Color.black).font(.custom("Regular_Font".localized(), size: 14)).padding(.trailing)
                            //                                Spacer()
                            
                        }.padding(.trailing)
                        Spacer()
                    }).clipped().shadow(radius: 10)
                
                
            }
            
            Button(action:{
                
                let telephone = "tel://"
                let formattedString = telephone + "+97167680088"
                guard let url = URL(string: formattedString) else { return }
                UIApplication.shared.open(url)
                
                //                    if let url = NSURL(string: "tel://+971543203020"), UIApplication.shared.canOpenURL(url as URL) {
                //                        UIApplication.shared.openURL(url as URL)
                //                                  }
                
            }){
                RoundedCorners(color: Color.white, tl: 10, tr: 10, bl: 10, br: 10).frame(width:UIScreen.main.bounds.width*0.9,height:111)
                    .padding(.horizontal).padding(.vertical,1).overlay( HStack {
                        
                        Spacer().frame(width:25)
                        Group{
                            
                            
                            
                            Image("Path 26").renderingMode(.template).resizable().foregroundColor(Color.white).frame(width:30,height: 20).padding(20).clipShape(Circle()).background(LinearGradient(gradient: Gradient(colors: [Color("h3"),Color("h2")]), startPoint: .leading, endPoint: .trailing)).clipShape(Circle())
                            
                        }
                        VStack(alignment:.leading){
                            
                            VStack(alignment:.leading){
                                Text("Call Us").foregroundColor(Color.black).font(.custom("ExtraBold_Font".localized(), size: 14)).padding(.trailing)
                                
                                
                                Text("Get help with our team please,").font(.custom("Regular_Font".localized(), size: 14)).padding(.vertical,5).foregroundColor(Color("s1"))
                            }
                            
                            Text("+971 6768 00 88").foregroundColor(Color.black).font(.custom("Regular_Font".localized(), size: 14)).padding(.trailing)
                            //                                Spacer()
                            
                        }.padding(.trailing)
                        Spacer()
                    }).clipped().shadow(radius: 10)
            }
            
            Spacer()
            
        }.navigationBarTitle("Help Center")
        
    }
}

struct GetHelp_Previews: PreviewProvider {
    static var previews: some View {
        GetHelp()
    }
}

