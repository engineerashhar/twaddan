//
//  SavedCards.swift
//  Twaddan
//
//  Created by Spine on 17/08/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI

struct SavedCards: View {
    
    @State var Selected : Bool = false
    @State var SelectedCard : CARD = CARD(cardToken: "N", cardholderName: "", expiry: "", maskedPan: "", scheme: "")
    
    var body: some View {
        VStack{
            CardList(Selected:self.$Selected, SelectedCard: self.$SelectedCard)
        }.navigationBarTitle("Saved Cards")
    }
}

struct SavedCards_Previews: PreviewProvider {
    static var previews: some View {
        SavedCards()
    }
}
