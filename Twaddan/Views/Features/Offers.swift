//
//  Offers.swift
//  Twaddan
//
//  Created by Spine on 13/07/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI
import FirebaseDatabase

struct Offers: View {
    
    
    //    @ObservedObject var findOffers  = FindOffer()
    let ref = Database.database().reference()
    @State var Offers:[Offer] = [Offer]()
    
    var body: some View {
        
        GeometryReader{ geometry in
            
            VStack{
                
                Spacer().frame(height:geometry.size.height*0.1)
                
                
                
                
                List{
                    
                    
                    VStack(alignment: .leading){
                        
                        
                        Text("Offers & Promotions").font(.custom("Regular_Font".localized(), size: 20))   .foregroundColor(Color("darkthemeletter"))
                        Text("Save money on your orders").font(.custom("Regular_Font".localized(), size: 14))   .foregroundColor(Color("darkthemeletter"))
                    }
                    
                    ForEach(
                        //                1...10
                        self.Offers
                        ,id: \.self){ Offer in
                        
                        VStack(alignment:.center){
                            AnimatedImage(url:URL(string: Offer.OfferImage)).resizable().padding(.vertical).frame(width: geometry.size.width > 30 ? geometry.size.width-30 :geometry.size.width,height: 200)
                            
                            HStack{
                                Text(Offer.OfferCode)
                                    .font(.custom("Regular_Font".localized(), size: 16))   .foregroundColor(Color("darkthemeletter"))
                                Spacer()
                            }.padding(.horizontal,15)
                            
                            HStack{
                                Text(Offer.Desc).font(.custom("Regular_Font".localized(), size: 14))   .foregroundColor(Color("darkthemeletter"))
                                Spacer()
                            }.padding(.horizontal,15)
                            
                            
                            HStack{
                                Text(Offer.TC).font(.custom("Regular_Font".localized(), size: 13))
                                    .foregroundColor(Color("darkthemeletter"))
                                Spacer()
                            }.padding(.horizontal,15)
                            
                        }.clipped().shadow(radius: 10)
                        .onTapGesture {
                            let pasteboard = UIPasteboard.general
                            pasteboard.string = Offer.OfferCode
                        }       .listRowInsets(EdgeInsets())
                    }
                }
            }
        }.onAppear{
            self.ref.child("service_providers").observeSingleEvent(of: DataEventType.value, with: {
                
                (A) in
                
                
                //                        self.VehicleDriverLink.removeAll()
                self.Offers.removeAll()
                
                
                for B in A.children {
                    let snapshot = B as! DataSnapshot
                    
                    for Offers in snapshot.childSnapshot(forPath: "offers").children {
                        let snap = Offers as! DataSnapshot
                        let O : Offer = Offer(id: Date().timeIntervalSince1970 , OfferImage: String(describing :  (snap.childSnapshot(forPath:"image").value)!), ServiceProviderID: snapshot.key,OfferCode:String(describing :  (snap.childSnapshot(forPath:"promo_code").value)!),Desc:String(describing :  (snap.childSnapshot(forPath:"description".localized()).value)!),TC:String(describing :  (snap.childSnapshot(forPath:"terms_and_conditions".localized()).value )!)
                                              ,Percentage:String(describing :  (snap.childSnapshot(forPath:"percentage").value )!),OfferName: snap.key, show: false)
                        self.Offers.append(O)
                        
                        
                        
                    }
                    
                    //                         for V in snapshot.childSnapshot(forPath: "vehicles").children {
                    //                              let Ve = V as! DataSnapshot
                    //                             let Vehicle = VehicleDriver(VehicleNumber: Ve.key, DriverID: String(describing :  (Ve.childSnapshot(forPath:"driver").value ?? "" )))
                    //                             
                    //                             self.VehicleDriverLink.append(Vehicle)
                    //                         }
                    //                     
                    
                    
                }
                
                
                
                
                
            })
        }
        //           .background(LinearGradient(gradient: Gradient(colors: [Color("h3"),Color("h2")]), startPoint: .leading, endPoint: .trailing))
        .navigationBarHidden(false)
        .navigationBarTitle("Feauterd Offers",displayMode: .inline)
        .edgesIgnoringSafeArea(.all)
    }
}

struct Offers_Previews: PreviewProvider {
    static var previews: some View {
        Offers()
        //            .environment(\.locale,.init(identifier:"en"))
    }
}
