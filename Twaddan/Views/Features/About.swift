//
//  About.swift
//  Twaddan
//
//  Created by Spine on 15/08/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI

struct About: View {
    var body: some View {
        Text("About Us")
    }
}

struct About_Previews: PreviewProvider {
    static var previews: some View {
        About()
    }
}
