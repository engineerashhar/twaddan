//
//  UpdatePage.swift
//  Twaddan
//
//  Created by Spine on 01/10/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI

struct UpdatePage: View {
    var body: some View {
        GeometryReader{ geometry in
            
            ZStack(alignment: .center){
                
                
                VStack{
                    Spacer()
                    
                    Image("Group 788")
                    //                    .background(AnimatedImage(url : URL (string: self.backImagelinker.url))
                    //
                    //
                    //                                                               .resizable().frame(width:geometry.size.width,height:geometry.size.height) // Resizable like SwiftUI.Image, you must use this modifier or the view will use the image bitmap size
                    //                                   //                            .placeholder(UIImage(systemName: "basicbackimage")) // Placeholder Image
                    //                                                               // Supports ViewBuilder as well
                    //
                    //                                                               .transition(.fade)
                    //
                    //
                    //
                    //                                                               .clipped()
                    //                                                               )
                    
                    
                    
                    //                .background(Image("basicbackimage").resizable().frame(width:geometry.size.width,height:geometry.size.height))
                    //                .brightness(-0.2)
                    //
                    //                HStack{
                    //                    Image("Twaddan Logo").resizable().frame(width : 40*5.4,height:40).padding()
                    //
                    //                                    }
                    Spacer().frame(height:100)
                    
                    
                    VStack{
                        
                        
                        
                        
                        Text("Please update to new version").font(.custom("Bold_Font".localized(), size: 17)) .font(Font.body.bold()) .padding(.top,30) .foregroundColor(Color("c1"))
                        
                        
                        HStack{
                            
                            
                            
                            Button(action:{
                                if let url = URL(string: "https://apps.apple.com/us/app/id1525130644"),
                                   UIApplication.shared.canOpenURL(url) {
                                    UIApplication.shared.open(url, options: [:])
                                }
                                
                            }){
                                
                                Text("Update").font(.custom("Regular_Font".localized(), size: 16))
                                    .foregroundColor(Color("c2"))
                                    
                                    // .padding(.horizontal,40)
                                    .frame(width : 100, height: 50)
                                
                                
                                
                                
                                
                            }
                            
                        }
                        
                        
                        
                        
                    }
                    
                    Spacer()
                }
                //                .background(Color.red)
                
                
                
            }  .frame(width :geometry.size.width)
        }.navigationBarTitle("")
        .navigationBarBackButtonHidden(true)
        
        .navigationBarHidden(true)
        .edgesIgnoringSafeArea(.all)
        
    }
}

struct UpdatePage_Previews: PreviewProvider {
    static var previews: some View {
        UpdatePage()
    }
}
