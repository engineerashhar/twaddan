//
//  RateAndReviews.swift
//  Twaddan
//
//  Created by Spine on 11/08/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import FirebaseDatabase

struct RateAndReviews: View {
    @Binding var OrderID : String
    //    @Binding var SP : String
    
    
    @State var VS = 0 ;
    @State var AT = 0 ;
    @State var QS = 0 ;
    @State var feed = "";
    //    @Binding var Stage : Int ;
    
    @Binding var Stage : Int ;
    //    @Binding var customerName : String ;
    //        @Binding var Driver : String ;
    
    var body: some View {
        
        GeometryReader{ geometry in
            ZStack(alignment: .bottom){
                Rectangle()                         // Shapes are resizable by default
                    .foregroundColor(.clear)        // Making rectangle transparent
                    //                                                          .background((LinearGradient(gradient: Gradient(colors: [.clear, .black]), startPoint: .top, endPoint: .bottom)))
                    .background(Color.black).opacity(0.6)
                
                if(self.Stage == 1){
                    
                    ValueOfService( VS: self.$VS,Stage: self.$Stage)
                }else if(self.Stage == 2){
                    ArrivalTiming( AT: self.$AT,Stage: self.$Stage)
                }
                else if(self.Stage == 3){
                    QualityOfService( QS: self.$QS,Stage: self.$Stage)
                }
                else if(self.Stage == 4){
                    FeedBack(OrderID: self.$OrderID, VS: self.$VS, AT: self.$AT, QS: self.$QS, Stage: self.$Stage)
                }
                else
                if(self.Stage == 5)
                {
                    ThankYou(Stage: self.$Stage)
                }
                
            }.frame(width: geometry.size.width, height: geometry.size.height)
        }.environment(\.layoutDirection, .leftToRight)
    }
}

struct RateAndReviews_Previews: PreviewProvider {
    static var previews: some View {
        RateAndReviews(OrderID: .constant("0"), Stage: .constant(1))
    }
}

struct ValueOfService: View {
    
    
    //    @Binding var OrderID : String
    @Binding var VS :Int ;
    //    @Binding var AT : Int ;
    //    @Binding var QS : Int ;
    @Binding var Stage : Int ;
    //      @Binding var feed :String;
    //     @Binding var SP : String
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @State var timer: Timer.TimerPublisher = Timer.publish (every: 1, on: .main, in: .common)
    
    var body: some View {
        GeometryReader{geometry in
            VStack{
                Spacer()
                RoundedCorners(color: Color.white, tl: 50, tr: 0, bl: 0, br: 0) .frame(height: geometry.size.height*0.4).overlay(
                    
                    VStack{
                        
                        HStack{
                            Spacer()
                            Button(action:{
                                self.Stage = 0
                            }){
                                Image("close_2").resizable().frame(width:15,height:15).foregroundColor(Color.black).padding().padding(.leading,5).padding(.top,5)
                            }
                        }
                        Spacer()
                        Text("Value of Service").font(.custom("ExtraBold_Font".localized(), size: 19)).padding(.bottom)
                        Spacer()
                        HStack{
                            Spacer()
                            
                            Group{
                                //            HStack{
                                //                Text("1").foregroundColor(Color(
                                //                    "MainFontColor1")).font(.custom("ExtraBold_Font".localized(), size: 16))
                                Image("Component 32 – 3").resizable().renderingMode(.template) .foregroundColor(self.VS == 1 ? Color("green") : Color("lightgray")).frame(width:30,height:30)
                                    
                                    //            }
                                    //            .padding(.all,8).overlay(
                                    //                RoundedRectangle(cornerRadius: 10)
                                    //                    .stroke(Color("ManBackColor"), lineWidth: 1)
                                    //            )
                                    .onTapGesture {
                                        self.VS = 1
                                        self.timer.connect()
                                    }
                                Spacer()
                                //            HStack{
                                //                Text("2").foregroundColor(Color(
                                //                    "MainFontColor1")).font(.custom("ExtraBold_Font".localized(), size: 16))
                                Image("Component 33 – 3").resizable().renderingMode(.template).foregroundColor(self.VS == 2 ? Color("green") : Color("lightgray")).frame(width:30,height:30)
                                    
                                    //            }
                                    //            .padding(.all,8).overlay(
                                    //                RoundedRectangle(cornerRadius: 10)
                                    //
                                    //                    .stroke(Color("ManBackColor"), lineWidth: 1)
                                    //            )
                                    .onTapGesture {
                                        self.VS = 2
                                        self.timer.connect()
                                    }
                                Spacer()
                                
                                //            HStack{
                                //                Text("3").foregroundColor(Color(
                                //                    "MainFontColor1")).font(.custom("ExtraBold_Font".localized(), size: 16))
                                Image("Component 34 – 3").resizable().renderingMode(.template).foregroundColor(self.VS == 3 ? Color("green") : Color("lightgray")).frame(width:30,height:30)
                                    
                                    //            }
                                    //            .padding(.all,8).overlay(
                                    //                RoundedRectangle(cornerRadius: 10)
                                    //                    .stroke(Color("ManBackColor"), lineWidth: 1)
                                    //            )
                                    .onTapGesture {
                                        self.VS = 3
                                        self.timer.connect()
                                    }
                            }
                            
                            Group{
                                Spacer()
                                
                                //            HStack{
                                //                Text("4").foregroundColor(Color(
                                //                    "MainFontColor1")).font(.custom("ExtraBold_Font".localized(), size: 16))
                                Image("Component 36 – 3").resizable().renderingMode(.template).foregroundColor(self.VS == 4 ? Color("green") : Color("lightgray")).frame(width:30,height:30)
                                    
                                    //            }
                                    //            .padding(.all,8).overlay(
                                    //                RoundedRectangle(cornerRadius: 10)
                                    //                    .stroke(Color("ManBackColor"), lineWidth: 1)
                                    //            )
                                    .onTapGesture {
                                        self.VS = 4
                                        self.timer.connect()
                                    }
                                
                                Spacer()
                                
                                //            HStack{
                                //                Text("5").foregroundColor(Color(
                                //                    "MainFontColor1")).font(.custom("ExtraBold_Font".localized(), size: 16))
                                Image("Component 37 – 3").resizable().renderingMode(.template).foregroundColor(self.VS == 5 ? Color("green") : Color("lightgray")).frame(width:30,height:30)
                                    
                                    //            }
                                    //            .padding(.all,8).overlay(
                                    //                RoundedRectangle(cornerRadius: 10)
                                    //                    .stroke(Color("ManBackColor"), lineWidth: 1)
                                    //            )
                                    .onTapGesture {
                                        self.VS = 5
                                        
                                        
                                        self.timer.connect()
                                    }
                                
                                Spacer()
                            }
                        } .environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                        ))
                        
                        
                        
                        Spacer()
                        
                        HStack{
                            Button(action:{
                                self.Stage -= 1
                            }){
                                Image(systemName: "arrow.left").frame(width:20,height: 20).padding(35)
                            }
                            Spacer()
                            //                            Button(action:{
                            //                                                          self.Stage += 1
                            //                                                      }){
                            //                            RoundedPanel().overlay(
                            //                                Text("NEXT").font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color.white)
                            //                            ).frame(width:geometry.size.width/2)
                            //                            }
                        }
                    }.onReceive(self.timer){_ in
                        
                        self.Stage += 1
                    }
                    
                )
                
            }.frame(height:geometry.size.height)
            
        }
    }
}




struct ArrivalTiming: View {
    
    
    //    @Binding var OrderID : String
    //    @Binding var VS :Int ;
    @Binding var AT : Int ;
    //    @Binding var QS : Int ;
    @Binding var Stage : Int ;
    // /     @Binding var feed :String;
    //     @Binding var SP : String
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var timer: Timer.TimerPublisher = Timer.publish (every: 1, on: .main, in: .common)
    
    var body: some View {
        GeometryReader{geometry in
            VStack{
                Spacer()
                RoundedCorners(color: Color.white, tl: 50, tr: 0, bl: 0, br: 0) .frame(height: geometry.size.height*0.4).overlay(
                    
                    VStack{
                        
                        HStack{
                            Spacer()
                            
                            Image("close_2").resizable().frame(width:15,height:15).foregroundColor(Color.black).padding().padding(.leading,5).padding(.top,5)
                        }
                        Spacer()
                        Text("Arrival timing").font(.custom("ExtraBold_Font".localized(), size: 19)).padding(.bottom)
                        Spacer()
                        HStack{
                            Spacer()
                            
                            Group{
                                //            HStack{
                                //                Text("1").foregroundColor(Color(
                                //                    "MainFontColor1")).font(.custom("ExtraBold_Font".localized(), size: 16))
                                Image("Component 32 – 3").resizable().renderingMode(.template) .foregroundColor(self.AT == 1 ? Color("green") : Color("lightgray")).frame(width:30,height:30)
                                    
                                    //            }
                                    //            .padding(.all,8).overlay(
                                    //                RoundedRectangle(cornerRadius: 10)
                                    //                    .stroke(Color("ManBackColor"), lineWidth: 1)
                                    //            )
                                    .onTapGesture {
                                        self.AT = 1
                                        self.timer.connect()
                                    }
                                Spacer()
                                //            HStack{
                                //                Text("2").foregroundColor(Color(
                                //                    "MainFontColor1")).font(.custom("ExtraBold_Font".localized(), size: 16))
                                Image("Component 33 – 3").resizable().renderingMode(.template).foregroundColor(self.AT == 2 ? Color("green") : Color("lightgray")).frame(width:30,height:30)
                                    
                                    //            }
                                    //            .padding(.all,8).overlay(
                                    //                RoundedRectangle(cornerRadius: 10)
                                    //
                                    //                    .stroke(Color("ManBackColor"), lineWidth: 1)
                                    //            )
                                    .onTapGesture {
                                        self.AT = 2
                                        self.timer.connect()
                                    }
                                Spacer()
                                
                                //            HStack{
                                //                Text("3").foregroundColor(Color(
                                //                    "MainFontColor1")).font(.custom("ExtraBold_Font".localized(), size: 16))
                                Image("Component 34 – 3").resizable().renderingMode(.template).foregroundColor(self.AT == 3 ? Color("green") : Color("lightgray")).frame(width:30,height:30)
                                    
                                    //            }
                                    //            .padding(.all,8).overlay(
                                    //                RoundedRectangle(cornerRadius: 10)
                                    //                    .stroke(Color("ManBackColor"), lineWidth: 1)
                                    //            )
                                    .onTapGesture {
                                        self.AT = 3
                                        self.timer.connect()
                                    }
                            }
                            
                            Group{
                                Spacer()
                                
                                //            HStack{
                                //                Text("4").foregroundColor(Color(
                                //                    "MainFontColor1")).font(.custom("ExtraBold_Font".localized(), size: 16))
                                Image("Component 36 – 3").resizable().renderingMode(.template).foregroundColor(self.AT == 4 ? Color("green") : Color("lightgray")).frame(width:30,height:30)
                                    
                                    //            }
                                    //            .padding(.all,8).overlay(
                                    //                RoundedRectangle(cornerRadius: 10)
                                    //                    .stroke(Color("ManBackColor"), lineWidth: 1)
                                    //            )
                                    .onTapGesture {
                                        self.AT = 4
                                        self.timer.connect()
                                    }
                                
                                Spacer()
                                
                                //            HStack{
                                //                Text("5").foregroundColor(Color(
                                //                    "MainFontColor1")).font(.custom("ExtraBold_Font".localized(), size: 16))
                                Image("Component 37 – 3").resizable().renderingMode(.template).foregroundColor(self.AT == 5 ? Color("green") : Color("lightgray")).frame(width:30,height:30)
                                    
                                    //            }
                                    //            .padding(.all,8).overlay(
                                    //                RoundedRectangle(cornerRadius: 10)
                                    //                    .stroke(Color("ManBackColor"), lineWidth: 1)
                                    //            )
                                    .onTapGesture {
                                        self.AT = 5
                                        self.timer.connect()
                                    }
                                Spacer()
                            }
                        } .environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                        ))
                        
                        Spacer()
                        
                        HStack{
                            Button(action:{
                                self.Stage -= 1
                            }){
                                Image(systemName: "arrow.left").frame(width:20,height: 20).padding(35)
                            }
                            Spacer()
                            //                            Button(action:{
                            //                                                           self.Stage += 1
                            //                                                       }){
                            //                            RoundedPanel().overlay(
                            //                                Text("NEXT").font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color.white)
                            //
                            //                            ).frame(width:geometry.size.width/2)
                            //                            }
                            
                        }
                    }.onReceive(self.timer){_ in
                        
                        self.Stage += 1
                    }
                    
                )
                
            }.frame(height:geometry.size.height)
            
        }
    }
}


struct QualityOfService: View {
    
    //    @Binding var OrderID : String
    //    @Binding var VS :Int ;
    //    @Binding var AT : Int ;
    @Binding var QS : Int ;
    @Binding var Stage : Int ;
    //      @Binding var feed :String;
    //     @Binding var SP : String
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var timer: Timer.TimerPublisher = Timer.publish (every: 1, on: .main, in: .common)
    
    var body: some View {
        GeometryReader{geometry in
            VStack{
                Spacer()
                RoundedCorners(color: Color.white, tl: 50, tr: 0, bl: 0, br: 0) .frame(height: geometry.size.height*0.4).overlay(
                    
                    VStack{
                        
                        HStack{
                            Spacer()
                            Button(action:{
                                self.Stage = 0
                            }){
                                Image("close_2").resizable().frame(width:15,height:15).foregroundColor(Color.black).padding()
                            }
                        }
                        Spacer()
                        Text("Quality of Service").font(.custom("ExtraBold_Font".localized(), size: 19)).padding(.bottom)
                        Spacer()
                        HStack{
                            Spacer()
                            
                            Group{
                                //            HStack{
                                //                Text("1").foregroundColor(Color(
                                //                    "MainFontColor1")).font(.custom("ExtraBold_Font".localized(), size: 16))
                                Image("Component 32 – 3").resizable().renderingMode(.template) .foregroundColor(self.QS == 1 ? Color("green") : Color("lightgray")).frame(width:30,height:30)
                                    
                                    //            }
                                    //            .padding(.all,8).overlay(
                                    //                RoundedRectangle(cornerRadius: 10)
                                    //                    .stroke(Color("ManBackColor"), lineWidth: 1)
                                    //            )
                                    .onTapGesture {
                                        self.QS = 1
                                        self.timer.connect()
                                    }
                                Spacer()
                                //            HStack{
                                //                Text("2").foregroundColor(Color(
                                //                    "MainFontColor1")).font(.custom("ExtraBold_Font".localized(), size: 16))
                                Image("Component 33 – 3").resizable().renderingMode(.template).foregroundColor(self.QS == 2 ? Color("green") : Color("lightgray")).frame(width:30,height:30)
                                    
                                    //            }
                                    //            .padding(.all,8).overlay(
                                    //                RoundedRectangle(cornerRadius: 10)
                                    //
                                    //                    .stroke(Color("ManBackColor"), lineWidth: 1)
                                    //            )
                                    .onTapGesture {
                                        self.QS = 2
                                        withAnimation{
                                            self.Stage += 1
                                        }
                                    }
                                Spacer()
                                
                                //            HStack{
                                //                Text("3").foregroundColor(Color(
                                //                    "MainFontColor1")).font(.custom("ExtraBold_Font".localized(), size: 16))
                                Image("Component 34 – 3").resizable().renderingMode(.template).foregroundColor(self.QS == 3 ? Color("green") : Color("lightgray")).frame(width:30,height:30)
                                    
                                    //            }
                                    //            .padding(.all,8).overlay(
                                    //                RoundedRectangle(cornerRadius: 10)
                                    //                    .stroke(Color("ManBackColor"), lineWidth: 1)
                                    //            )
                                    .onTapGesture {
                                        self.QS = 3
                                        self.timer.connect()
                                    }
                            }
                            
                            Group{
                                Spacer()
                                
                                //            HStack{
                                //                Text("4").foregroundColor(Color(
                                //                    "MainFontColor1")).font(.custom("ExtraBold_Font".localized(), size: 16))
                                Image("Component 36 – 3").resizable().renderingMode(.template).foregroundColor(self.QS == 4 ? Color("green") : Color("lightgray")).frame(width:30,height:30)
                                    
                                    //            }
                                    //            .padding(.all,8).overlay(
                                    //                RoundedRectangle(cornerRadius: 10)
                                    //                    .stroke(Color("ManBackColor"), lineWidth: 1)
                                    //            )
                                    .onTapGesture {
                                        self.QS = 4
                                        self.timer.connect()
                                    }
                                
                                Spacer()
                                
                                //            HStack{
                                //                Text("5").foregroundColor(Color(
                                //                    "MainFontColor1")).font(.custom("ExtraBold_Font".localized(), size: 16))
                                Image("Component 37 – 3").resizable().renderingMode(.template).foregroundColor(self.QS == 5 ? Color("green") : Color("lightgray")).frame(width:30,height:30)
                                    
                                    //            }
                                    //            .padding(.all,8).overlay(
                                    //                RoundedRectangle(cornerRadius: 10)
                                    //                    .stroke(Color("ManBackColor"), lineWidth: 1)
                                    //            )
                                    .onTapGesture {
                                        self.QS = 5
                                        
                                        self.timer.connect()
                                    }
                                Spacer()
                            }
                        } .environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                        ))
                        
                        Spacer()
                        
                        HStack{
                            
                            Button(action:{
                                self.Stage -= 1
                            }){
                                Image(systemName: "arrow.left").frame(width:20,height: 20).padding(35)
                            }
                            Spacer()
                            
                            //                            Button(action:{
                            //                                self.Stage += 1
                            //                            }){
                            //                            RoundedPanel().overlay(
                            //                                Text("NEXT").font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color.white)
                            //
                            //                            ).frame(width:geometry.size.width/2)
                            //                            }
                        }
                    }.onReceive(self.timer){_ in
                        
                        self.Stage += 1
                    }
                    
                )
                
            }.frame(height:geometry.size.height)
            
        }
    }
}




struct FeedBack: View {
    
    @Binding var OrderID : String
    @Binding var VS :Int ;
    @Binding var AT : Int ;
    @Binding var QS : Int ;
    @Binding var Stage : Int ;
    @State var feed :String = "";
    //  @Binding var SP : String ;
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var Rate = 3 ;
    
    //     @Binding var customerName : String ;
    //     @Binding var Driver : String ;
    
    @State var textHeight: CGFloat = 133
    var body: some View {
        GeometryReader{geometry in
            VStack{
                Spacer()
                RoundedCorners(color: Color.white, tl: 50, tr: 0, bl: 0, br: 0) .frame(height: geometry.size.height*0.5).overlay(
                    
                    VStack{
                        
                        HStack{
                            Spacer()
                            
                            Button(action:{
                                self.Stage = 0
                            }){
                                Image("close_2").resizable().frame(width:15,height:15).foregroundColor(Color.black).padding().padding(.leading,5).padding(.top,5)
                            }
                        }
                        HStack{
                            Text("Write your feedback").font(.custom("ExtraBold_Font".localized(), size: 19)).padding(.bottom)
                            Text("(Optional)").font(.custom("ExtraBold_Font".localized(), size: 19)).padding(.bottom).foregroundColor(Color.gray)
                        }
                        
                        MultilineTextField("Share your thoughts with other customers".localized(), text: self.$feed).padding(5)
                            .overlay(RoundedRectangle(cornerRadius: 4).stroke(Color.gray)).padding(10)
                        
                        //                        TextView(placeholder: "Share your thoughts with other customers", text: self.$feed, minHeight: self.textHeight, calculatedHeight: self.$textHeight).padding()
                        //                                   .frame(minHeight: self.textHeight, maxHeight: self.textHeight)
                        //                        TextField("Share your thoughts with other customers", text: self.$feed) .multilineTextAlignment(.leading).lineLimit(nil).padding().frame(height:133).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color("h6"),lineWidth: 1)).padding()
                        
                        Spacer()
                        
                        HStack{
                            Button(action:{
                                self.Stage -= 1
                            }){
                                Image(systemName: "arrow.left").frame(width:20,height: 20).padding(.leading,35)
                            }
                            Spacer()
                            
                            Button(action:{
                                self.Stage += 1
                                
                                
                                let ref = Database.database().reference()
                                
                                self.Rate = (self.VS+self.QS+self.AT)/3
                                
                                ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("personal_details").observeSingleEvent(of: DataEventType.value) { (datasnapshot) in
                                    ref.child("orders").child("all_orders").child(self.OrderID).observeSingleEvent(of: DataEventType.value) { (orderSnap) in
                                        
                                        
                                        
                                        let data : [String:Any] = ["arrivalTimeRating" : self.AT,
                                                                   "customerName":"\(String(describing: datasnapshot.childSnapshot(forPath: "name").value!)) \(String(describing: datasnapshot.childSnapshot(forPath: "lastName").value!))",
                                                                   "driver_id":String(describing: orderSnap.childSnapshot(forPath: "driver_id").value!)
                                                                   ,
                                                                   "qualityOfServiceRating":self.QS,
                                                                   "rating":self.Rate,
                                                                   "review":self.feed,
                                                                   "timestamp":[".sv":"timestamp"],"valueOfServiceRating":self.VS]
                                        
                                        //                                ref.child("")
                                        ref.child("service_providers_ratings").child(String(describing: orderSnap.childSnapshot(forPath: "sp_id").value!)).child(self.OrderID).setValue(data)
                                        
                                        ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("past_orders").child("order_ids").child(self.OrderID).removeValue()
                                        ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("past_orders").child("order_ids").child(self.OrderID).setValue(self.OrderID)
                                        
                                        
                                        
                                        
                                        
                                    }
                                }
                                
                                
                            }){
                                RoundedPanel().overlay(
                                    Text("SUBMIT").font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color.white)
                                ).frame(width:geometry.size.width/2)
                            }
                        }.environment(\.layoutDirection,.leftToRight)
                    }.environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                    ))
                    
                )
                
            }.frame(height:geometry.size.height)
            
        }
    }
}


struct ThankYou: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @Binding var Stage : Int ;
    
    
    var body: some View {
        GeometryReader{geometry in
            VStack{
                Spacer()
                RoundedCorners(color: Color.white, tl: 50, tr: 0, bl: 0, br: 0) .frame(height: geometry.size.height*0.6).overlay(
                    
                    VStack{
                        
                        HStack{
                            Spacer()
                            Button(action:{
                                self.Stage = 0
                            }){
                                Image("close_2").resizable().frame(width:15,height:15).foregroundColor(Color.black).padding().padding(.leading,5).padding(.top,5)
                            }
                        }
                        
                        Image("Group 943")
                        
                        Image("Group 940")
                        
                        Text("You just helped us make even better!").font(.custom("Regular_Font".localized(), size: 15)).foregroundColor(Color("h6"))
                        
                        Spacer()
                        
                        HStack{
                            
                            Button(action:{
                                self.Stage = 0
                            }){
                                RoundedPanel().overlay(
                                    Text("Back to home page").font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color.white)
                                )
                            }
                            
                        }
                    }
                    
                )
                
            }.frame(height:geometry.size.height)
            
        }
    }
}


struct TextView: UIViewRepresentable {
    var placeholder: String
    @Binding var text: String
    
    var minHeight: CGFloat
    @Binding var calculatedHeight: CGFloat
    
    init(placeholder: String, text: Binding<String>, minHeight: CGFloat, calculatedHeight: Binding<CGFloat>) {
        self.placeholder = placeholder
        self._text = text
        self.minHeight = minHeight
        self._calculatedHeight = calculatedHeight
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    func makeUIView(context: Context) -> UITextView {
        let textView = UITextView()
        textView.delegate = context.coordinator
        textView.textColor = UIColor.red
        textView.text = "placeholder"
        textView.textColor = UIColor.red
        // Decrease priority of content resistance, so content would not push external layout set in SwiftUI
        textView.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        
        textView.isScrollEnabled = false
        textView.isEditable = true
        textView.isUserInteractionEnabled = true
        textView.backgroundColor = UIColor(white: 0.0, alpha: 0.1)
        
        // Set the placeholder
        
        
        return textView
    }
    
    func updateUIView(_ textView: UITextView, context: Context) {
        textView.text = self.text
        
        recalculateHeight(view: textView)
    }
    
    func recalculateHeight(view: UIView) {
        let newSize = view.sizeThatFits(CGSize(width: view.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        if minHeight < newSize.height && $calculatedHeight.wrappedValue != newSize.height {
            DispatchQueue.main.async {
                self.$calculatedHeight.wrappedValue = newSize.height // !! must be called asynchronously
            }
        } else if minHeight >= newSize.height && $calculatedHeight.wrappedValue != minHeight {
            DispatchQueue.main.async {
                self.$calculatedHeight.wrappedValue = self.minHeight // !! must be called asynchronously
            }
        }
    }
    
    class Coordinator : NSObject, UITextViewDelegate {
        
        var parent: TextView
        
        init(_ uiTextView: TextView) {
            self.parent = uiTextView
        }
        
        func textViewDidChange(_ textView: UITextView) {
            // This is needed for multistage text input (eg. Chinese, Japanese)
            if textView.markedTextRange == nil {
                parent.text = textView.text ?? String()
                parent.recalculateHeight(view: textView)
            }
        }
        
        func textViewDidBeginEditing(_ textView: UITextView) {
            if textView.textColor == UIColor.lightGray {
                textView.text = nil
                textView.textColor = UIColor.black
            }
        }
        
        func textViewDidEndEditing(_ textView: UITextView) {
            if textView.text.isEmpty {
                textView.text = parent.placeholder
                textView.textColor = UIColor.lightGray
            }
        }
    }
}
fileprivate struct UITextViewWrapper: UIViewRepresentable {
    typealias UIViewType = UITextView
    
    @Binding var text: String
    @Binding var calculatedHeight: CGFloat
    var onDone: (() -> Void)?
    
    func makeUIView(context: UIViewRepresentableContext<UITextViewWrapper>) -> UITextView {
        let textField = UITextView()
        textField.delegate = context.coordinator
        
        textField.isEditable = true
        textField.font = UIFont.preferredFont(forTextStyle: .body)
        textField.isSelectable = true
        textField.isUserInteractionEnabled = true
        textField.isScrollEnabled = false
        textField.backgroundColor = UIColor.clear
        if nil != onDone {
            textField.returnKeyType = .done
        }
        
        textField.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        return textField
    }
    
    func updateUIView(_ uiView: UITextView, context: UIViewRepresentableContext<UITextViewWrapper>) {
        if uiView.text != self.text {
            uiView.text = self.text
        }
        if uiView.window != nil, !uiView.isFirstResponder {
            uiView.becomeFirstResponder()
        }
        UITextViewWrapper.recalculateHeight(view: uiView, result: $calculatedHeight)
    }
    
    fileprivate static func recalculateHeight(view: UIView, result: Binding<CGFloat>) {
        let newSize = view.sizeThatFits(CGSize(width: view.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        if result.wrappedValue != newSize.height {
            DispatchQueue.main.async {
                result.wrappedValue = newSize.height // !! must be called asynchronously
            }
        }
    }
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(text: $text, height: $calculatedHeight, onDone: onDone)
    }
    
    final class Coordinator: NSObject, UITextViewDelegate {
        var text: Binding<String>
        var calculatedHeight: Binding<CGFloat>
        var onDone: (() -> Void)?
        
        init(text: Binding<String>, height: Binding<CGFloat>, onDone: (() -> Void)? = nil) {
            self.text = text
            self.calculatedHeight = height
            self.onDone = onDone
        }
        
        func textViewDidChange(_ uiView: UITextView) {
            text.wrappedValue = uiView.text
            UITextViewWrapper.recalculateHeight(view: uiView, result: calculatedHeight)
        }
        
        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            if let onDone = self.onDone, text == "\n" {
                textView.resignFirstResponder()
                onDone()
                return false
            }
            return true
        }
    }
    
}

struct MultilineTextField: View {
    
    private var placeholder: String
    private var onCommit: (() -> Void)?
    
    @Binding private var text: String
    private var internalText: Binding<String> {
        Binding<String>(get: { self.text } ) {
            self.text = $0
            self.showingPlaceholder = $0.isEmpty
        }
    }
    
    @State private var dynamicHeight: CGFloat = 100
    @State private var showingPlaceholder = false
    
    init (_ placeholder: String = "", text: Binding<String>, onCommit: (() -> Void)? = nil) {
        self.placeholder = placeholder
        self.onCommit = onCommit
        self._text = text
        self._showingPlaceholder = State<Bool>(initialValue: self.text.isEmpty)
    }
    
    var body: some View {
        UITextViewWrapper(text: self.internalText, calculatedHeight: $dynamicHeight, onDone: onCommit)
            .frame(minHeight: dynamicHeight, maxHeight: dynamicHeight)
            .background(placeholderView, alignment: .topLeading)
    }
    
    var placeholderView: some View {
        Group {
            if showingPlaceholder {
                Text(placeholder).foregroundColor(.gray)
                    .padding(.leading, 4)
                    .padding(.top, 8)
            }
        }
    }
}
