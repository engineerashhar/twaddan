//
//  CardList.swift
//  Twaddan
//
//  Created by Spine on 18/08/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import FirebaseDatabase

struct CardList: View {
    
    let ref = Database.database().reference()
    
    @Binding var Selected : Bool
    @Binding var SelectedCard : CARD
    @State var CardLists : [CARD] = [CARD]()
    
    var body: some View {
        
        
        VStack(alignment:.leading){
            
            ForEach(self.CardLists,id: \.self) { (V) in
                
                Button(action:{
                    
                    self.SelectedCard = V
                    self.Selected = true
                }){
                    
                    
                    RoundedCorners(color: Color.white, tl: 10, tr: 10, bl: 10, br: 10).frame(width:UIScreen.main.bounds.width*0.9,height:111)
                        .padding(.horizontal).padding(.vertical,1).overlay( HStack {
                            
                            Spacer().frame(width:50)
                            Group{
                                
                                
                                
                                Image(self.getImage(T: V.scheme)).renderingMode(.original).resizable().frame(width:40,height: 23).padding()
                                
                            }
                            VStack(alignment:.leading){
                                
                                HStack{
                                    Text(V.scheme + "CARD").foregroundColor(Color.black).font(.custom("ExtraBold_Font".localized(), size: 14)).padding(.trailing)
                                    
                                    
                                    Text(V.maskedPan).foregroundColor(Color.black).font(.custom("ExtraBold_Font".localized(), size: 14))
                                }
                                
                                Text("Expires In" + V.expiry).foregroundColor(Color.black).font(.custom("Regular_Font".localized(), size: 14)).padding(.trailing)
                                //                                Spacer()
                                
                            }.padding(.trailing)
                            Spacer()
                        }).clipped().shadow(radius: 10)
                    
                }
                
                
            }
            //            HStack{
            //            Image("Image 8").resizable().padding(10)
            //                          .overlay(Circle().stroke(Color("h8"),lineWidth: 2)).frame(width:44,height:44)
            //
            //                      Text("saloon").font(.custom("Bold_Font".localized(), size: 13)).foregroundColor(Color("h2")
            //                )
            //                    Spacer()
            //            }.padding(.horizontal).padding(.vertical,7).frame(width:260).background(Color.white).cornerRadius(20).clipped()
            Spacer()
            
        }.padding().background(Color.clear)
        .onAppear{
            self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("personal_details").child("saved_cards").observeSingleEvent(of: DataEventType.value) { (dataSnapshot) in
                
                self.CardLists.removeAll()
                for V in dataSnapshot.children{
                    let V = V as! DataSnapshot
                    let S = CARD(cardToken: String(describing:(V.childSnapshot(forPath:"cardToken").value)!), cardholderName: String(describing:(V.childSnapshot(forPath:"cardholderName").value)!), expiry: String(describing:(V.childSnapshot(forPath:"expiry").value)!), maskedPan: String(describing:(V.childSnapshot(forPath:"maskedPan").value)!), scheme: String(describing:(V.childSnapshot(forPath:"scheme").value)!))
                    //                             let S: CARD = try JSONDecoder().decode(CARD.self, from: data)
                    
                    
                    self.CardLists.append(S)
                }
                
            }
        }
        
    }
    func getImage(T:String) -> String {
        switch T {
        case "MASTER":
            return  "Group 959"
            break
            
        case "VISA":
            return        "Image 22"
            break
        default:
            return  "Image 21"
        }
    }
}

struct CardList_Previews: PreviewProvider {
    static var previews: some View {
        CardList(Selected: .constant(false), SelectedCard: .constant(CARD(cardToken: "", cardholderName: "", expiry: "", maskedPan: "", scheme: "")))
    }
}

struct CARD :Hashable{
    var cardToken : String
    var cardholderName : String
    var expiry:String
    var maskedPan : String
    var scheme : String
}
