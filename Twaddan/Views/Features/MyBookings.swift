//
//  MyBookings.swift
//  Twaddan
//
//  Created by Spine on 02/06/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import FirebaseDatabase
import SDWebImageSwiftUI
import MapKit


struct MyBookings: View {
    @ObservedObject private var locationManager = LocationManager()
    @State var SELECT : Int = 1
    @State private var offset = CGSize.zero
    
    @EnvironmentObject var settings : UserSettings
    
    @State var ActiveOrders :[Bookings] = [Bookings]()
    @State var PastOrders :[Bookings] = [Bookings]()
    @State var AllOrders :[Bookings] = [Bookings]()
    //        @State var PastOrderSets :[pastOrderSet] = [pastOrderSet]()
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    let ref = Database.database().reference()
    @State var selectedOptions : Int = 0
    @State var Output : Bookings = Bookings()
    
    
    @State var OpenPartialSheet:Bool = false
    @State var goToMap:Bool = false
    //       @State var LastAddress:[String:String] = [String:String]()
    
    @State var item:Bookings = Bookings()
    
    @State var OpenCart : Bool = false
    
    
    
    @State var Driver:String = ""
    @State var ETA:Double = 0.0
    @State var SuccessETA :String = "0"
    
    
    
    
    
    
    var body: some View {
        
        
        GeometryReader { geometry in
            ZStack{
                //                
                //               
                //                NavigationLink(destination: Cart(GoToBookDetailsOld: .constant(false))
                //                    
                //                                                  .environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                //        )), isActive: self.$OpenCart){
                //                                                      
                //                                                      Text("").frame(height:0)
                //                                              }
                
                VStack  {
                    
                    
                    VStack{
                        
                        //                                                    Spacer().frame(height:50)
                        Spacer().frame(height:geometry.size.height*0.05)
                        
                        HStack{
                            
                            Button(action : {
                                
                                
                                
                                //  self.cartFetch.readCart(settings: self.settings)
                                
                                
                                self.presentationMode.wrappedValue.dismiss()
                                
                                
                            }) {
                                Image("back").frame(width:24,height:19 )
                                
                                
                                
                            }
                            .padding(.leading, 25.0)
                            Spacer().frame(width: 40)
                            
                            Text("Bookings").font(.custom("Regular_Font".localized(), size: 16))
                            
                            
                            
                            Spacer()
                            
                            
                            
                            
                        }
                        .foregroundColor(Color("ManBackColor"))
                        .background(Color("backcolor2"))
                        .padding(.vertical,15)
                        
                        
                        
                        
                        HStack{
                            Spacer()
                            Button(action:{
                                withAnimation{
                                    self.SELECT = 1
                                }
                            }){
                                VStack{
                                    Text("ALL").font(.custom("Bold_Font".localized(), size: 16)).foregroundColor(self.SELECT==1 ?  Color("darkthemeletter") : Color("MainFontColor1"))
                                    
                                    if(self.SELECT==1){
                                        Rectangle()
                                            .fill(Color("darkthemeletter"))
                                            .frame(height: 2)
                                    }
                                    
                                }.frame(width:geometry.size.width/3)
                                
                            }
                            // Spacer()
                            Button(action:{
                                withAnimation{
                                    self.SELECT = 2
                                }
                            }){
                                VStack{
                                    Text("ONGOING").font(.custom("Bold_Font".localized(), size: 16)).foregroundColor(self.SELECT==2 ?  Color("darkthemeletter") : Color("MainFontColor1"))
                                    
                                    if(self.SELECT==2){
                                        Rectangle()
                                            .fill(Color("darkthemeletter"))
                                            .frame(height: 2)
                                    }
                                    
                                }.frame(width:geometry.size.width/3)
                                
                            }
                            //  Spacer()
                            Button(action:{
                                withAnimation{
                                    self.SELECT = 3
                                }
                            }){
                                VStack{
                                    Text("COMPLETED").font(.custom("Bold_Font".localized(), size: 16)).foregroundColor(self.SELECT==3 ?  Color("darkthemeletter") : Color("MainFontColor1"))
                                    
                                    if(self.SELECT==3){
                                        Rectangle()
                                            .fill(Color("darkthemeletter"))
                                            .frame(height: 2)
                                    }
                                    
                                }.frame(width:geometry.size.width/4)
                                
                            }
                            Spacer()
                        }
                        
                        
                        
                        //.padding(.top, 50)
                        //    Spacer()
                    }       .background(Color("backcolor2"))
                    //                           .frame(height:150)
                    
                    
                    
                    //                    Picker(selection: $selectedOptions, label: Text("")) {
                    //                        Text("ALL").font(.custom("Bold_Font".localized(), size: 16)).foregroundColor(Color("MainFontColor1")).tag(0)
                    //                        Text("ONGOING").font(.custom("Bold_Font".localized(), size: 16)).foregroundColor(Color("MainFontColor1")).tag(1)
                    //                        Text("COMPLETED").font(.custom("Bold_Font".localized(), size: 16)).foregroundColor(Color("MainFontColor1")).tag(2)
                    //                    }
                    //
                    //                    .pickerStyle(SegmentedPickerStyle()).font(.custom("Bold_Font".localized(), size: 16)).foregroundColor(Color("MainFontColor1")).background(Color("backcolor2"))
                    //
                    
                    if(self.SELECT == 1){
                        Display(OpenPartialSheet: self.$OpenPartialSheet,item: self.$item, Data: self.ActiveOrders+self.PastOrders,Driver:self.$Driver,SuccessETA: self.$SuccessETA,ETA:self.$ETA,OpenCart: self.$OpenCart)
                            .gesture(
                                DragGesture()
                                    .onChanged { gesture in
                                        self.offset = gesture.translation
                                    }
                                    
                                    .onEnded { _ in
                                        //                                    if self.offset.width > 100 {
                                        //                                        // remove the card
                                        //                                                 self.SELECT -= 1
                                        //
                                        //                                    } else
                                        if self.offset.width < -50 {
                                            self.SELECT += 1
                                        }else {
                                            self.offset = .zero
                                        }
                                    }
                            )
                    }else    if(self.SELECT == 2){
                        
                        Display(OpenPartialSheet: self.$OpenPartialSheet,item: self.$item, Data: self.ActiveOrders,Driver:self.$Driver,SuccessETA: self.$SuccessETA,ETA:self.$ETA,OpenCart: self.$OpenCart).gesture(
                            DragGesture()
                                .onChanged { gesture in
                                    self.offset = gesture.translation
                                }
                                
                                .onEnded { _ in
                                    if self.offset.width > 50 {
                                        // remove the card
                                        self.SELECT -= 1
                                        
                                    } else if self.offset.width < -50 {
                                        self.SELECT += 1
                                    }else {
                                        self.offset = .zero
                                    }
                                }
                        )
                    }else    {
                        
                        Display(OpenPartialSheet: self.$OpenPartialSheet, item: self.$item, Data: self.PastOrders,Driver:self.$Driver,SuccessETA: self.$SuccessETA,ETA:self.$ETA,OpenCart: self.$OpenCart).gesture(
                            DragGesture()
                                .onChanged { gesture in
                                    self.offset = gesture.translation
                                }
                                
                                .onEnded { _ in
                                    if self.offset.width > 50 {
                                        // remove the card
                                        self.SELECT -= 1
                                        
                                    }
                                    //                                    else if self.offset.width < -100 {
                                    //                                  self.SELECT += 1
                                    //                                    }
                                    else {
                                        self.offset = .zero
                                    }
                                }
                        )
                    }
                    Spacer()
                    
                }.onAppear{
                    self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").observe(DataEventType.value) { (usersnap) in
                        
                        self.ActiveOrders.removeAll()
                        
                        for AIDs in usersnap.childSnapshot(forPath: "active_orders/order_ids").children{
                            let AIDsSnap : DataSnapshot = AIDs as! DataSnapshot
                            
                            self.ref.child("orders").child("all_orders").child(AIDsSnap.key).observeSingleEvent(of: DataEventType.value) { (ordersnap) in
                                
                                self.ref.child("service_providers").child(String(describing :  (ordersnap.childSnapshot(forPath:"sp_id").value)!)).observeSingleEvent(of: DataEventType.value) { (spsnap) in
                                    
                                    var allservice: String = ""
                                    
                                    for S in ordersnap.childSnapshot(forPath: "services").children{
                                        let Ssnap : DataSnapshot = S as! DataSnapshot
                                        
                                        
                                        for Se in Ssnap.childSnapshot(forPath: "services").children{
                                            let SeSnap : DataSnapshot = Se as! DataSnapshot
                                            
                                            allservice = allservice + " , " + SeSnap.key
                                            
                                        }
                                        
                                    }
                                    let ratings:Double = -1
                                    let time:Double = 0
                                    
                                    //                                    let bookings : Bookings = Bookings(Amount: String(describing :  (ordersnap.childSnapshot(forPath:"total_price_of_order").value)!), Status: "Booked", ServiceProvider: String(describing :  (spsnap.childSnapshot(forPath:Localize(key: "personal_information/name")).value)!), Services: allservice, ServiceLocation: String(describing :  (ordersnap.childSnapshot(forPath:"customer_raw_address").value)!), Image: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/image").value)!),PhoneNumber: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/phone_number").value)!),OrderID: AIDsSnap.key,ServiceProviderID:spsnap.key ,DriverID:String(describing :  (ordersnap.childSnapshot(forPath:"driver_id").value)!) ,ServiceSnap: spsnap, OrderSnap: ordersnap,latitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_latitude").value)!),longitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_longitude").value)!),rating: ratings,time: time)
                                    //
                                    //                                    self.ActiveOrders.append(bookings)
                                }
                            }
                        }
                        
                        self.PastOrders.removeAll()
                        //                        self.PastOrderSets.removeAll()
                        
                        for PIDs in usersnap.childSnapshot(forPath: "past_orders/order_ids").children{
                            let PIDsSnap : DataSnapshot = PIDs as! DataSnapshot
                            
                            self.ref.child("orders").child("all_orders").child(PIDsSnap.key).observeSingleEvent(of: DataEventType.value) { (ordersnap) in
                                
                                self.ref.child("service_providers").child(String(describing :  (ordersnap.childSnapshot(forPath:"sp_id").value)!)).observeSingleEvent(of: DataEventType.value) { (spsnap) in
                                    
                                    
                                    
                                    self.ref.child("service_providers_ratings").child(String(describing :  (ordersnap.childSnapshot(forPath:"sp_id").value)!)).child(ordersnap.key).observeSingleEvent(of: DataEventType.value) { (rateSnap) in
                                        let rateSnap : DataSnapshot = rateSnap as! DataSnapshot
                                        
                                        
                                        var allservice: String = ""
                                        
                                        for S in ordersnap.childSnapshot(forPath: "services").children{
                                            let Ssnap : DataSnapshot = S as! DataSnapshot
                                            
                                            
                                            for Se in Ssnap.childSnapshot(forPath: "services").children{
                                                let SeSnap : DataSnapshot = Se as! DataSnapshot
                                                
                                                allservice = allservice + " , " + SeSnap.key
                                                
                                            }
                                            
                                        }
                                        
                                        //
                                        //                                    self.LastAddress = ["Address":String(describing :  (ordersnap.childSnapshot(forPath:"customer_raw_address").value)!)
                                        //                                                  , "latitude": String(describing :  (ordersnap.childSnapshot(forPath:"customer_latitude").value)!),"longitude":String(describing :  (ordersnap.childSnapshot(forPath:"customer_longitude").value)!)]
                                        
                                        let milisecond:Int = Int(String(describing :  (ordersnap.childSnapshot(forPath:"time_order_placed").value)!)) ?? 0;
                                        //                                                           print("KKKKKK",milisecond)
                                        let dateVar = Date.init(timeIntervalSince1970: TimeInterval(milisecond)/1000)
                                        var dateFormatter = DateFormatter()
                                        dateFormatter.dateFormat = "dd-MM-yyyy"
                                        print(dateFormatter.string(from: dateVar))
                                        
                                        dateFormatter.locale = Locale(identifier: UserDefaults.standard.string(forKey: "LANSEL") ?? "en")
                                        
                                        //                                    let pO : pastOrderSet = pastOrderSet(id: spsnap.key, driverID: String(describing :  (ordersnap.childSnapshot(forPath:"driver_id").value)!), SpName: String(describing :  (spsnap.childSnapshot(forPath:Localize(key:"personal_information/name")).value)!), Date: dateFormatter.string(from: dateVar), Image: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/image").value)!),snap: spsnap,Address:String(describing :  (ordersnap.childSnapshot(forPath:"customer_raw_address").value)!),latitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_latitude").value)!),longitude:String(describing :  (ordersnap.childSnapshot(forPath:"customer_longitude").value)!), OrderSnap: ordersnap )
                                        
                                        //                                    self.PastOrderSets.append(pO)
                                        var ratings:Double = 0
                                        
                                        if(!rateSnap.exists()){
                                            
                                            ratings = -1
                                        }else{
                                            ratings =   Double(String(describing:rateSnap.childSnapshot(forPath:"rating").value)) ?? 0.0
                                        }
                                        let time:Double = Double(String(describing:ordersnap.childSnapshot(forPath:"time_order_placed").value!)) ?? 0.0
                                        //                                    let bookings : Bookings = Bookings(Amount: String(describing :  (ordersnap.childSnapshot(forPath:"total_price_of_order").value)!), Status: "Completed", ServiceProvider: String(describing :  (spsnap.childSnapshot(forPath:Localize(key:"personal_information/name")).value)!), Services: allservice, ServiceLocation: String(describing :  (ordersnap.childSnapshot(forPath:"customer_raw_address").value)!), Image: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/image").value)!),PhoneNumber: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/phone_number").value)!),OrderID: PIDsSnap.key,ServiceProviderID:spsnap.key,DriverID:String(describing :  (ordersnap.childSnapshot(forPath:"driver_id").value)!) ,ServiceSnap: spsnap, OrderSnap: ordersnap ,latitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_latitude").value)!),longitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_longitude").value)!),rating: ratings,time:time)
                                        //
                                        //                                    self.PastOrders.append(bookings)
                                    }
                                }
                            }
                        }
                        
                        
                    }
                    
                }
                VStack{
                    Text("")
                }.frame(width:geometry.size.width,height:geometry.size.height)  .opacity(self.OpenPartialSheet ? 1:0)  .brightness(self.OpenPartialSheet ? -0.2:0)
                VStack{
                    LocForReorder(OpenPartialSheet: self.$OpenPartialSheet, goToMap: self.$goToMap, PastOrders: self.PastOrders, bookings: self.$item, Driver: self.$Driver, ETA: self.$ETA, SuccessETA: self.$SuccessETA,Output: self.$Output,LocationName: .constant(self.locationManager.locationName ?? ""),Location:.constant(self.locationManager.location ?? CLLocation(latitude: 0.0, longitude: 0.0) ))
                    
                    
                }
                
            }
            //             }
            
            .background(Color("backcolor2"))
            
        }    .navigationBarTitle("Bookings",displayMode: .inline)
        .navigationBarHidden(true)
        .edgesIgnoringSafeArea(.all)
    }
    
    func getAppendedData(Data1:[Bookings],Data2:[Bookings]) -> [Bookings] {
        
        let Data3 = Data1+Data2
        return Data3
    }
    
    
    
    
}


struct MyBookings_Previews: PreviewProvider {
    static var previews: some View {
        MyBookings().environmentObject(UserSettings())
    }
}

struct Display: View {
    
    
    
    @Binding var OpenPartialSheet:Bool
    //        @Binding var goToMap:Bool
    //       @Binding var LastAddress:[String:String]
    
    
    
    @Binding var item:Bookings
    
    
    
    
    
    @EnvironmentObject var settings : UserSettings
    var Data : [Bookings]
    @Binding var Driver:String
    @Binding var SuccessETA :String
    
    @Binding var ETA:Double
    
    @Binding var OpenCart : Bool
    @State var goToTrack : Bool = false
    @State var SOrderID : String = ""
    
    
    @State var ViewLoader:Bool = false
    
    
    
    @State var Snap:DataSnapshot = DataSnapshot()
    @State var  GoToBookDetails:Bool = false
    
    //        @State var Reorder :String = "0"
    let ref = Database.database().reference()
    var body: some View {
        ZStack{
            
            
            //            NavigationLink(destination: Cart(GoToBookDetailsOld: .constant(false))
            //                
            //                .environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
            //        )), isActive: self.$OpenCart){
            //                    Text("")
            //                    
            //            }
            List{
                ForEach(Data.reversed(),id: \.self){ bookings in
                    
                    VStack{
                        
                        //   Spacer().frame(width:20)
                        VStack{
                            
                            HStack{
                                VStack{
                                    //    Image("basicbackimage")
                                    AnimatedImage(url : URL (string: bookings.Image))
                                        .resizable()
                                        .frame(width: 50, height: 50)
                                        
                                        .cornerRadius(5)
                                        .padding(.all, 8.0)
                                    Spacer()
                                }
                                
                                Spacer()
                                
                                
                                VStack(alignment: .leading){
                                    
                                    Group{
                                        
                                        HStack{
                                            Text(bookings.ServiceProvider)
                                                .foregroundColor(Color("darkthemeletter"))
                                                .font(.custom("ExtraBold_Font".localized(), size: 14)).padding(.top,20)
                                            
                                            
                                            
                                            Text("-  \(bookings.Status)").padding(.top,20) .font(.custom("Bold_Font".localized(), size: 14)).foregroundColor(bookings.Status=="Booked" ? Color.orange : Color.green )
                                            Spacer()
                                            Text("\(bookings.Amount)" + " AED")
                                                
                                                //   Text("100 AED")
                                                .foregroundColor(Color("darkthemeletter"))
                                                .font(.custom("Bold_Font".localized(), size: 14)).padding(.top,20)
                                            
                                        }
                                        
                                        
                                        
                                    }
                                }
                            }.padding(.horizontal)
                            
                            Top(bookings: bookings).padding(.horizontal)
                            
                            
                            
                            if(bookings.OrderID == self.item.OrderID){
                                
                                
                                Group{
                                    
                                    if(self.SuccessETA == "1"){
                                        
                                        HStack{
                                            
                                            Text("Will arrive within in \(getTime(timeIntervel: self.ETA))").font(.custom("Regular_Font".localized(), size: 14)).padding()
                                            
                                            Button(action:{
                                                
                                                self.ReOrderToCart(booking:bookings,DriverID:self.Driver,ETA:self.ETA)
                                                
                                                self.OpenCart = true
                                                print("VVVVVVV")
                                            }){
                                                RoundedPanel()
                                                    .frame(width:100)
                                                    .overlay(Text("Book")
                                                                .font(.custom("Regular_Font".localized(), size: 14))
                                                                
                                                                
                                                                .foregroundColor(Color.white))
                                                    .background(Color("ManBackColor"))
                                                    .cornerRadius(5)
                                                
                                            }.padding()
                                            
                                        }
                                    }else if(self.SuccessETA == "2"){
                                        Text("Sorry Every Drivers are buzy").font(.custom("Regular_Font".localized(), size: 14)).padding()
                                        
                                    }else{
                                        
                                        //                                                      Loader()
                                        ButtonPanel(Status: bookings.Status, Phone: bookings.PhoneNumber, OrderID: bookings.OrderID, ServiceProviderID: bookings.ServiceProviderID, SOrderID:self.$SOrderID,goToTrack: self.$goToTrack,ViewLoader: self.$ViewLoader, booking:bookings, OpenPartialSheet: self.$OpenPartialSheet, item: self.$item)
                                    }
                                    
                                    
                                }
                                
                                
                            }else{
                                
                                ButtonPanel(Status: bookings.Status, Phone: bookings.PhoneNumber, OrderID: bookings.OrderID, ServiceProviderID: bookings.ServiceProviderID, SOrderID:self.$SOrderID,goToTrack: self.$goToTrack,ViewLoader: self.$ViewLoader, booking:bookings, OpenPartialSheet: self.$OpenPartialSheet, item: self.$item)
                            }
                            //                                        ButtonPanel(Status: bookings.Status, Phone: bookings.PhoneNumber, OrderID: bookings.OrderID, ServiceProviderID: bookings.ServiceProviderID,SOrderID:self.$SOrderID,goToTrack: self.$goToTrack,ViewLoader: self.$ViewLoader,SelectedDriver: self.$Driver,ETA: self.$ETA, Reorder: self.$Reorder,Snap: self.$Snap,GoToBookDetails: self.$GoToBookDetails, booking:bookings, OpenPartialSheet: self.$OpenPartialSheet, item: self.$item)
                            
                            Spacer()
                            
                            Spacer()
                            
                            
                            
                            
                            // Spacer().frame(width:10)
                        }.clipped()
                        .background(Color.white)
                        .cornerRadius(5)
                        .shadow(radius: 2)
                    }.onAppear(){
                        print(self.item.OrderID,bookings.OrderID)
                    }
                }.buttonStyle(BorderlessButtonStyle())
                .sheet(isPresented:self.$goToTrack ){
                    TrackServiceProvider(openTrack:self.$goToTrack,OrderID :self.SOrderID, goToHome: .constant(false)).environment(\.colorScheme, .light)
                }
                
                
                if(self.ViewLoader){
                    Loader()
                }
                
                
                
            }
            //        .sheet(isPresented: self.$GoToBookDetails){
            //
            //
            //
            //            BookingDetails(OpenCart: self.$OpenCart,GoToBookDetails:self.$GoToBookDetails ,serviceSnap: self.Snap, SelectedDriver:
            //                self.Driver,ETA: self.ETA,Reorder:self.Reorder).environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
            //        ))
            //                
            //                
            //
            //        }
            
        }
    }
    
    func ReOrderToCart(booking:Bookings,DriverID:String,ETA:Double) {
        
        
        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").removeValue()
        
        let node = String((Date().timeIntervalSince1970)).replacingOccurrences(of: ".", with: "")
        
        for service in booking.OrderSnap.childSnapshot(forPath: "services").children{
            let serviceSnap = service as! DataSnapshot
            let Vehicle = serviceSnap.key.components(separatedBy: ["&"])[0].trimmingCharacters(in: .whitespacesAndNewlines)
            
            
            self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("services").setValue(serviceSnap.childSnapshot(forPath: "services").value)
            self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("addons").setValue(serviceSnap.childSnapshot(forPath: "add_on_services").value)
            
            self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("Vehicle").setValue(Vehicle)
            
        }
        
        
        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider").setValue(booking.ServiceProvider)
        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("DriverID").setValue(DriverID)
        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ETA").setValue(ETA)
        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider_id").setValue(booking.ServiceSnap.key)
        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider_image").setValue(booking.ServiceSnap.childSnapshot(forPath:"personal_information/image").value!)
        
        
        
    }
}

struct Bookings:Hashable{
    
    var Amount : String = ""
    var Status :String = ""
    var ServiceProvider : String = ""
    var Services :String = ""
    var ServiceLocation:String = ""
    var Image: String = ""
    var PhoneNumber : String = ""
    var OrderID :String = ""
    var ServiceProviderID :String = ""
    var DriverID:String = ""
    var ServiceSnap: DataSnapshot = DataSnapshot()
    var OrderSnap : DataSnapshot = DataSnapshot()
    var latitude : String = ""
    var longitude : String = ""
    var rating : Double = 0
    var time : Double = 0
    
}

struct Top :View{
    var bookings:Bookings
    var body:some View{
        
        VStack{
            
            HStack{
                Image("prefix__maintenance-1")
                    .resizable().renderingMode(.template) .foregroundColor(Color("MainFontColor1"))
                    .frame(width: 11, height: 14)
                    .padding(.all,5)
                
                
                Text(self.bookings.Services)
                    .font(.custom("Regular_Font".localized(), size: 12))
                    .foregroundColor(Color("darkthemeletter"))
                Spacer()
                
            }
            
            
            
            Divider()
            HStack{
                
                
                
                Image("prefix__placeholder")
                    .resizable().renderingMode(.template) .foregroundColor(Color("MainFontColor1"))
                    .frame(width: 11, height: 14)
                    .padding(.all,5)
                
                
                Text(self.bookings.ServiceLocation)
                    .font(.custom("Regular_Font".localized(), size: 12))
                    .foregroundColor(Color("darkthemeletter"))
                
                Spacer()
                
                
                
                
                
                
                
                
            }
            Divider()
            
            //                HStack{
            //
            //
            //
            //                    Image("prefix__start_1")
            //                        .resizable()
            //                        .renderingMode(.template) .foregroundColor(Color("MainFontColor1"))                                              .frame(width: 12, height: 12)
            //                        .padding(.all,5)
            //
            //
            //                    Text(UserDefaults.standard.string(forKey: "PLACE") != nil ? String(UserDefaults.standard.string(forKey: "PLACE")!) : "Location")
            //                        .font(.custom("Regular_Font".localized(), size: 12))
            //                        .foregroundColor(Color("darkthemeletter"))
            //
            //                    Spacer()
            //
            //
            //
            //
            //
            //
            //
            //
            //                }
            //                Divider()
        }
        
    }
}

struct ButtonPanel: View {
    var Status:String
    var Phone:String
    var OrderID :String
    var ServiceProviderID :String
    @Binding var SOrderID: String
    @Binding  var goToTrack : Bool
    @Binding var ViewLoader :Bool
    
    //    @Binding var SelectedDriver :String
    //    @Binding  var ETA :Double
    //    @Binding var Reorder :String
    //    @Binding var Snap:DataSnapshot
    //    @Binding var GoToBookDetails:Bool
    
    
    let ref = Database.database().reference()
    var booking :Bookings
    
    
    @Binding var OpenPartialSheet : Bool
    //    @Binding var LastAddress:[String:String]
    @Binding var item:Bookings
    
    
    @EnvironmentObject var settings : UserSettings
    
    @State var ALERT2 : Bool = false
    @State var AlertMessege2 = ""
    
    var body: some View {
        
        ZStack{
            
            //            if(self.ViewLoader){
            //                Loader()
            //            }
            VStack{
                
                if(Status == "Completed"){
                    HStack(alignment:.center){
                        
                        Spacer()
                        Button(action:{
                            
                            self.item = self.booking
                            self.OpenPartialSheet = true
                            
                            
                            
                            
                        }){
                            Image("prefix__start_1")
                                .resizable()
                                .renderingMode(.template)
                                .foregroundColor(Color.white)                                          .frame(width: 12, height: 12)
                                .padding(.all,5)
                                .background(Color("ManBackColor")).clipShape(Circle())
                            
                        }
                        
                        
                        
                        
                        
                        Spacer()
                        
                        
                        
                        
                    }
                    
                    if(self.ALERT2){
                        
                        Text(self.AlertMessege2)
                        
                    }
                    
                }
                
                else{
                    
                    
                    HStack(alignment:.center){
                        
                        Spacer()
                        Button(action:{
                            let telephone = "tel://"
                            let formattedString = telephone + "+971" + self.Phone
                            guard let url = URL(string: formattedString) else { return }
                            UIApplication.shared.open(url)
                            
                            print("call")
                        }){
                            Image("call")
                                .resizable()
                                .foregroundColor(Color.white)                                          .frame(width: 12, height: 12)
                                .padding(.all,5)
                                .background(Color("ManBackColor")).clipShape(Circle())
                            
                        }
                        Button(action:{
                            print("track")
                            self.SOrderID = self.OrderID
                            self.goToTrack = true
                            
                            
                        }){
                            Image("placeholder_blue")
                                .resizable()
                                .foregroundColor(Color.white)                                          .frame(width: 11, height: 14)
                                .padding(.all,5)
                                .background(Color("ManBackColor")).clipShape(Circle())
                            
                        }.padding(.horizontal,30)
                        
                        
                        
                        Spacer()
                        
                        
                        
                        
                        
                    }
                    
                    
                }
            }
        }
    }
    
    struct ETAAndDriver{
        
        var ETA:Double
        var DriverID :String
    }
    
    struct pastOrderSet: Hashable{
        
        var driverID:String
        var SpName:String
        var Date:String
        var Image:String
        var snap : DataSnapshot
        var Address: String
        var latitude :String
        var longitude : String
        var OrderSnap : DataSnapshot
    }
    
    
    
}


//OLD REORDER CODE

//{
//     self.ViewLoader = true
//
//    print("Entered")
//    self.Snap = self.booking.ServiceSnap
//   // print(item.SpName)
//
//
//
//
//    //                                                self.ref.child("drivers").child(item.driverID).observeSingleEvent(of: DataEventType.value) { (driversnap) in
//    //
//
//    //                                                    if(String(describing :  (driversnap.childSnapshot(forPath:"status").value)!) == "1"){
//    //
//    //
//    //                                                               //        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5)) {
//    //
//    //                                                                    let driverlat =
//    //                                                                    //10.797765
//    //                                                                       Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/latitude").value)!))
//    //
//    //                                                                    let driverlon =
//    //                                                                    //76.635908
//    //                                                                        Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/longitude").value)!))
//    //
//    //                                                                    var driverloc : CLLocation = CLLocation(latitude: driverlat!, longitude: driverlon!)
//    //                                                                    var driver2D : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: driverlat!, longitude: driverlon!)
//    //
//    //                                                                    let userlat =
//    //                                                //                    10.874858
//    //                                                                        UserDefaults.standard.double(forKey: "latitude")
//    //                                                                    let userlon =
//    //                                                //                    76.449728
//    //                                                                        UserDefaults.standard.double(forKey: "longitude")
//    //
//    //
//    //                                                               var userloc : CLLocation = CLLocation(latitude: userlat, longitude: userlon)
//    //                                                                    var user2D  : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: userlat, longitude: userlon)
//    //
//    //
//    //                                                                //    var distance : CLLocationDistance = userloc.distance(from: driverloc)
//    //                                                                   // var timeOfArrival :
//    //
//    //                                                                    let request = MKDirections.Request()
//    //                                                             request.source = MKMapItem(placemark:  MKPlacemark(coordinate: driver2D))
//    //                                                             request.destination = MKMapItem(placemark:  MKPlacemark(coordinate: user2D))
//    //
//    //                                                             request.requestsAlternateRoutes = false
//    //
//    //                                                             let directions = MKDirections(request: request)
//    //                                                //                    directions.calculateETA { (responds, error) in
//    //                                                //                         if error != nil {
//    //                                                //                                            print("Error getting directions")
//    //                                                //                                        } else {
//    //                                                //                            var ETA  =    responds?.expectedTravelTime
//    //                                                //
//    //                                                //
//    //                                                //
//    //                                                //                        }
//    //                                                //                    }
//    //                                                                    directions.calculateETA{ response, error in
//    //
//    //
//    //
//    //
//    //                                                               //     directions.calculate { response, error in
//    //                                                                            var eta:Double = 10000000
//    //                                                                        if(error != nil){
//    //                                                                            print(error.debugDescription)
//    //                                                                        }else{
//    //
//    //                                                                                eta = response?.expectedTravelTime as! Double
//    //
//    //
//    //                                                //                        let routes = response?.routes
//    //
//    //
//    //
//    //
//    //                                                //                        if(routes != nil){
//    //                                                //                        let selectedRoute = routes![0]
//    //                                                //                        let distance = selectedRoute.distance
//    //                                                //                         eta = selectedRoute.expectedTravelTime
//    //                                                //
//    //                                                //
//    //                                                //                        }
//    //                                                                            print(eta)
//    //
//    //                                                                        }
//    //
//    //                                                                         self.ViewLoader = false
//    //
//    //                                                self.SelectedDriver = item.driverID
//    //                                                                        self.ETA = eta
//    //
//    //                                                self.Snap = item.snap
//    //
//    //                                                                       self.ReOrderToCart(item: item,DriverID: item.driverID,ETA: eta)
//    //
//    //                                                                        self.GoToBookDetails = true
//    //
//    //
//    //
//    //                                                    }
//    //                                                    }else{
//
//  //  self.ViewLoader = false
//
//
//    var selectedETAD : ETAAndDriver = ETAAndDriver(ETA:1000000.0 , DriverID: "")
//
//    self.ref.child("drivers").observeSingleEvent(of: DataEventType.value) { (allDriverSnapshot)
//        in
//
//
//        let     allDriver :DataSnapshot = allDriverSnapshot as! DataSnapshot
//
//        var x = self.booking.ServiceSnap.childSnapshot(forPath: "drivers_info/active_drivers_id").childrenCount
//
//
//        for driver  in  self.booking.ServiceSnap.childSnapshot(forPath: "drivers_info/active_drivers_id").children{
//            let  driverSnap = driver as! DataSnapshot
//               print("UUUUUUUU",driverSnap.key,(String(describing :  (allDriver.value)!)))
//
//            print( (String(describing :  (allDriver.childSnapshot(forPath:"\(driverSnap.key)/live_location/latitude").value)!), (String(describing :  (allDriver.childSnapshot(forPath:"\(driverSnap.key)/live_location/longitude").value)!))))
//
//
//            let driverlat =
//                //10.797765
//                Double(String(describing :  (allDriver.childSnapshot(forPath:"\(driverSnap.key)/live_location/latitude").value)!))
//
//            let driverlon =
//                //76.635908
//                Double(String(describing :  (allDriver.childSnapshot(forPath:"\(driverSnap.key)/live_location/longitude").value)!))
//
//
//            var driverloc : CLLocation = CLLocation(latitude: driverlat!, longitude: driverlon!)
//            var driver2D : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: driverlat!, longitude: driverlon!)
//
//
//
//
//            let userlat =
//                //                    10.874858
//                UserDefaults.standard.double(forKey: "latitude")
//            let userlon =
//                //                    76.449728
//                UserDefaults.standard.double(forKey: "longitude")
//
//
//            var userloc : CLLocation = CLLocation(latitude: userlat, longitude: userlon)
//            var user2D  : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: userlat, longitude: userlon)
//
//
//            //    var distance : CLLocationDistance = userloc.distance(from: driverloc)
//            // var timeOfArrival :
//
//            //                                                                                            let request = MKDirections.Request()
//            //                                                                                     request.source = MKMapItem(placemark:  MKPlacemark(coordinate: driver2D))
//            //                                                                                     request.destination = MKMapItem(placemark:  MKPlacemark(coordinate: user2D))
//            //
//            //                                                                                     request.requestsAlternateRoutes = false
//            //
//            //                                                                                     let directions = MKDirections(request: request)
//            //                    directions.calculateETA { (responds, error) in
//            //                         if error != nil {
//            //                                            print("Error getting directions")
//            //                                        } else {
//            //                            var ETA  =    responds?.expectedTravelTime
//            //
//            //
//            //
//            //                        }
//            //                    }
//            //                                                                                            directions.calculateETA{ response, error in
//            //
//            //
//            //
//            //
//            //                                                                                       //     directions.calculate { response, error in
//            //                                                                                                    var eta:Double = 10000000
//            //                                                                                                if(error != nil){
//            //                                                                                                    print(error.debugDescription)
//            //                                                                                                }else{
//
//
//            var eta:Double = 10000000
//
//            let origin = "\(driver2D.latitude),\(driver2D.longitude)"
//            let destination = "\(user2D.latitude),\(user2D.longitude)"
//
//
//            let S : String =  "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=\(origin)&destinations=\(destination)&mode=driving&key=AIzaSyCYQeItZ7SVEQ24Grjzc3zA1B_2DU_GfPU"
//
//            let url = URL(string:S
//            )
//            //        else {
//            //        let error = NSError(domain: "LocalDomain", code: 0, userInfo: [NSLocalizedDescriptionKey: "Failed to create object URL"])
//            //        print("Error: \(error)")
//            //        return
//            //    }
//            let config = URLSessionConfiguration.default
//            let session = URLSession(configuration: config)
//            let task = session.dataTask(with: url!, completionHandler: {
//                (data, response, error) in
//
//                //                                                                                                                                                   print("11111111",driversnap.key)
//                x = x-1
//                if error != nil {
//                    print(error!.localizedDescription)
//                }
//                else {
//
//                    //  print(response)
//                    do {
//                        if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
//
//                            guard let routes = json["rows"] as? NSArray else {
//                                DispatchQueue.main.async {
//                                }
//                                return
//                            }
//                            if (routes.count > 0) {
//                                let overview_polyline = routes[0] as? NSDictionary
//                                //   let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
//                                //  let points = dictPolyline?.object(forKey: "points") as? String
//
//                                DispatchQueue.main.async {
//                                    //
//
//
//                                    let legs = overview_polyline?["elements"] as! Array<Dictionary<String, AnyObject>>
//
//                                    let distance = legs[0]["distance"] as? NSDictionary
//                                    let distanceValue = distance?["value"] as? Int ?? 0
//                                    let distanceDouleValue = distance?["value"] as? Double ?? 0.0
//                                    let duration = legs[0]["duration"] as? NSDictionary
//                                    let totalDurationInSeconds = duration?["value"] as? Int ?? 0
//                                    let durationDouleValue = duration?["value"] as? Double ?? 0.0
//
//
//
//                                    eta = durationDouleValue+600 ;
//
//
//                                    if(eta < 601){
//                                        eta = 10000000
//                                    }
//
//
//
//
//
//
//
//
//
//                                    if (eta < selectedETAD.ETA){
//                                        selectedETAD  = ETAAndDriver(ETA:eta, DriverID: driverSnap.key)
//
//                                    }
//
//                                    if(x==0){
//                                        print("OOOOOO",x) ;
//
//                                        self.ViewLoader = false
//
//                                        if(selectedETAD.DriverID != ""){
//                                            self.SelectedDriver = selectedETAD.DriverID
//                                            self.ETA = selectedETAD.ETA
//
//
//                                            self.ReOrderToCart(booking: self.booking,DriverID:selectedETAD.DriverID, ETA: selectedETAD.ETA )
//                                            self.Reorder = self.booking.OrderSnap.key
//                                            self.GoToBookDetails = true
//
//                                        }else{
//                                            self.AlertMessege2 = "Soory , No Free drivers Available"
//                                            self.ALERT2 = true
//                                        }
//
//                                    }
//                                    print (S,eta)
//
//
//                                }
//                            }
//                            else {
//                                print(json)
//                                DispatchQueue.main.async {
//                                    //   SVProgressHUD.dismiss()
//                                }
//                            }
//                        }
//                    }
//                    catch {
//                        print("error in JSONSerialization")
//                        DispatchQueue.main.async {
//                            //  SVProgressHUD.dismiss()
//                        }
//                    }
//                }
//            })
//            task.resume()
//
//
//
//
//
//            //                                                                    }
//            //
//            //
//            //
//            //                                                                }
//
//
//
//
//
//
//
//            print("TTTTTTT",x)
//
//
//
//
//
//
//
//        }
//
//
//
//
//
//    }
//
//
//
//
//
//
//
//
//
//
//    //                                                    }
//
//    //                                        }
//    //                                                        }
//
//
//}
