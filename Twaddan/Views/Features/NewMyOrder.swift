//
//  NewMyOrder.swift
//  Twaddan
//
//  Created by Spine on 21/09/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI

import Firebase
import SwiftUI

import FirebaseDatabase
import SDWebImageSwiftUI

import FirebaseAuth
import MapKit


struct NewMyOrder: View {
    @State var goToMap2:Bool = false
    
    
    //      @State var selectedOrderID:String = "0"
    @State var Stage:Int = 0
    
    @State var Snap:DataSnapshot = DataSnapshot()
    
    @ObservedObject private var locationManager = LocationManager()
    @State var SELECT : Int = 1
    @State private var offset = CGSize.zero
    
    @EnvironmentObject var settings : UserSettings
    
    @State var ActiveOrders :[Bookings] = [Bookings]()
    @State var PastOrders :[Bookings] = [Bookings]()
    @State var AllOrders :[Bookings] = [Bookings]()
    //        @State var PastOrderSets :[pastOrderSet] = [pastOrderSet]()
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    let ref = Database.database().reference()
    @State var selectedOptions : Int = 0
    @State var Output : Bookings = Bookings()
    
    
    @State var OpenPartialSheet:Bool = false
    @State var goToMap:Bool = false
    //       @State var LastAddress:[String:String] = [String:String]()
    
    @State var item:Bookings = Bookings()
    
    @State var OpenCart : Bool = false
    
    
    
    @State var Driver:String = ""
    @State var ETA:Double = 0.0
    @State var SuccessETA :String = "0"
    
    @State var goToSplash:Bool = false
    @State var Ano:Bool = Auth.auth().currentUser?.isAnonymous ?? true
    
    
    @State var displayNoOrder:Bool = false
    
    
    
    
    //        @Binding var goToMap:Bool
    //       @Binding var LastAddress:[String:String]
    
    @State var length:Bool = false
    
    
    
    
    
    
    //          @Binding var Data : [Bookings]
    
    
    
    
    
    @State var goToTrack : Bool = false
    @State var SOrderID : String = ""
    
    //            @State var Stage : Int = 0
    
    @State var ViewLoader:Bool = false
    
    
    
    
    @State var  GoToBookDetails:Bool = false
    
    //        @State var Reorder :String = "0"
    
    
    var body: some View {
        
        
        
        GeometryReader{ geometry in
            ZStack{
                VStack{
                    
                    
                    NavigationLink(destination: SignUpSelection(FromDA :.constant(false))
                                   //                                    .navigationBarTitle(,width"").navigationBarHidden(true)
                                   , isActive: self.$goToSplash){
                        EmptyView()
                    }
                    
                    
                    NavigationLink(destination: Cart(GoToBookDetailsOld: .constant(false))
                                    
                                    .environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                                    )), isActive: self.$OpenCart){
                        EmptyView()
                        
                    }
                    
                    
                    NavigationLink(destination: Cart(GoToBookDetailsOld: .constant(false))
                                    
                                    .environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                                    )), isActive: self.$OpenCart){
                        
                        Text("").frame(height:0)
                    }
                    
                    //                if(self.length){
                    //                        Spacer().frame(height:UIScreen.main.bounds.height*0.1)
                    //                }
                    
                    
                    if(!self.Ano){
                        
                        ZStack{
                            VStack{
                                Spacer().frame(height:UIScreen.main.bounds.height*0.05)
                                List{
                                    
                                    
                                    ForEach(self.PastOrders.reversed(),id: \.self){ bookings in
                                        
                                        PastOrderView(item: bookings, ETA: self.$ETA, Output: self.$item, SuccessETA: self.$SuccessETA, OpenCart: self.$OpenCart, selectedOrderID: self.$SOrderID, Stage: self.$Stage, selecteditem: self.$item, OpenReorderSheet: self.$OpenPartialSheet, Driver: self.$Driver)
                                            .frame(height:111).background(Color.white).cornerRadius(10).clipped().shadow(radius: 5).padding(.horizontal,10).padding(.top,10).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                                            )).frame(height:120)
                                        
                                        
                                        
                                    }.onDelete(perform: self.removeRows)
                                    .listRowInsets(EdgeInsets())
                                    .listRowBackground(Color("h1"))
                                    .buttonStyle(BorderlessButtonStyle())
                                    
                                    
                                    
                                    
                                    
                                    
                                    //                         Spacer().frame(height:UIScreen.main.bounds.height*0.02)
                                }
                                //                    .sheet(isPresented:self.$goToTrack ){
                                //                                TrackServiceProvider(OrderID :self.SOrderID, goToHome: .constant(false)).environment(\.colorScheme, .light)
                                //                        }
                                .environment(\.locale,.init(identifier:"en"
                                ))
                                Spacer().frame(height:UIScreen.main.bounds.height*0.015)
                            }
                            
                            
                            if(self.displayNoOrder){
                                
                                
                                
                                
                                
                                
                                VStack(alignment:.center){
                                    
                                    Spacer().frame(height:geometry.size.height*0.2)
                                    VStack{
                                        Image("cart").renderingMode(.template).resizable().frame(width:77, height:77).foregroundColor(Color(#colorLiteral(red: 0, green: 0.2269999981, blue: 0.5329999924, alpha: 1)))
                                        Text("You don't have any order yet").font(.custom("Regular_Font".localized(), size: 14)).foregroundColor(Color(#colorLiteral(red: 0.5059999824, green: 0.5099999905, blue: 0.5139999986, alpha: 1))).padding()
                                        
                                        Button(action: {
                                            self.goToSplash = true
                                        }){
                                            HStack {
                                                
                                                
                                                
                                                Spacer()
                                                
                                                Text("Login")   .font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color.white)
                                                Spacer()
                                                
                                                
                                                
                                            }.padding()
                                            //                            .border(Color("ManualSignInGray"), width: 1)
                                            
                                            //                                                               .frame(width: 300.0, height: 50)      .background(Color("su1"))
                                            //                                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color("su1"),lineWidth: 1))
                                            .background(Color("su1"))
                                            .cornerRadius(10)
                                        }.padding().hidden()
                                    }
                                    Spacer()
                                }.frame(height:geometry.size.height)
                                
                                
                                
                                
                                
                                
                                
                                
                            }
                            
                        }
                    }else{
                        
                        VStack(alignment:.center){
                            
                            Spacer().frame(height:geometry.size.height*0.2)
                            VStack{
                                Image("cart").renderingMode(.template).resizable().frame(width:77, height:77).foregroundColor(Color(#colorLiteral(red: 0, green: 0.2269999981, blue: 0.5329999924, alpha: 1)))
                                Text("Login to see your orders").font(.custom("Regular_Font".localized(), size: 14)).foregroundColor(Color(#colorLiteral(red: 0.5059999824, green: 0.5099999905, blue: 0.5139999986, alpha: 1))).padding()
                                
                                Button(action: {
                                    self.goToSplash = true
                                }){
                                    HStack {
                                        
                                        
                                        
                                        Spacer()
                                        
                                        Text("Login")   .font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color.white)
                                        Spacer()
                                        
                                        
                                        
                                    }.padding()
                                    //                            .border(Color("ManualSignInGray"), width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                                    
                                    //                                                               .frame(width: 300.0, height: 50)      .background(Color("su1"))
                                    //                                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color("su1"),lineWidth: 1))
                                    .background(Color("su1"))
                                    .cornerRadius(10)
                                }.padding()
                            }
                            Spacer()
                        }.frame(height:geometry.size.height)
                        
                        
                    }
                    
                    
                    
                    
                }.frame(width:geometry.size.width,height:geometry.size.height)  .blur(radius: self.OpenPartialSheet ? 15:0)  .brightness(self.OpenPartialSheet ? -0.1:0)
                
                //                VStack{
                //                                                     EmptyView()
                //                }.frame(width:geometry.size.width,height:geometry.size.height)  .blur(radius: self.OpenPartialSheet ? 15:0)  .brightness(self.OpenPartialSheet ? -1:0)
                
                VStack{
                    
                    
                    LocForReorder(OpenPartialSheet: self.$OpenPartialSheet, goToMap: self.$goToMap2, PastOrders: self.PastOrders, bookings: self.$item, Driver: self.$Driver, ETA: self.$ETA, SuccessETA: self.$SuccessETA,Output:self.$Output,LocationName: .constant(self.locationManager.locationName ?? ""),Location:.constant(self.locationManager.location ?? CLLocation(latitude: 0.0, longitude: 0.0) ))
                    
                    
                }
                
                if(self.Stage != 0){
                    RateAndReviews(OrderID: self.$SOrderID,  Stage: self.$Stage).frame(width: geometry.size.width, height: geometry.size.height)
                }
            }
        }.edgesIgnoringSafeArea(.top).onAppear{
            self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").observe(DataEventType.value) { (usersnap) in
                
                //                                self.ActiveOrders.removeAll()
                
                //                                for AIDs in usersnap.childSnapshot(forPath: "active_orders/order_ids").children{
                //                                    let AIDsSnap : DataSnapshot = AIDs as! DataSnapshot
                //
                //                                    self.ref.child("orders").child("all_orders").child(AIDsSnap.key).observeSingleEvent(of: DataEventType.value) { (ordersnap) in
                //
                //                                        self.ref.child("service_providers").child(String(describing :  (ordersnap.childSnapshot(forPath:"sp_id").value)!)).observeSingleEvent(of: DataEventType.value) { (spsnap) in
                //
                //                                            var allservice: String = ""
                //
                //                                            for S in ordersnap.childSnapshot(forPath: "services").children{
                //                                                let Ssnap : DataSnapshot = S as! DataSnapshot
                //
                //
                //                                                for Se in Ssnap.childSnapshot(forPath: "services").children{
                //                                                    let SeSnap : DataSnapshot = Se as! DataSnapshot
                //
                //                                                    allservice = allservice + " , " + SeSnap.key
                //
                //                                                }
                //
                //                                            }
                //                                            let ratings:Double = -1
                //                                                                  let time:Double = 0
                //
                //                                            let bookings : Bookings = Bookings(Amount: String(describing :  (ordersnap.childSnapshot(forPath:"total_price_of_order").value)!), Status: "Booked", ServiceProvider: String(describing :  (spsnap.childSnapshot(forPath: "personal_information/name".localized()).value)!), Services: allservice, ServiceLocation: String(describing :  (ordersnap.childSnapshot(forPath:"customer_raw_address").value)!), Image: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/image").value)!),PhoneNumber: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/phone_number").value)!),OrderID: AIDsSnap.key,ServiceProviderID:spsnap.key ,DriverID:String(describing :  (ordersnap.childSnapshot(forPath:"driver_id").value)!) ,ServiceSnap: spsnap, OrderSnap: ordersnap,latitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_latitude").value)!),longitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_longitude").value)!),rating: ratings,time: time)
                //
                //                                            self.ActiveOrders.append(bookings)
                //                                        }
                //                                    }
                //                                }
                
                //                                self.PastOrders.removeAll()
                //                        self.PastOrderSets.removeAll()
                
                if(usersnap.childSnapshot(forPath: "past_orders/order_ids").childrenCount == 0){
                    self.displayNoOrder = true
                }
                
                self.PastOrders.removeAll()
                //                                            var PastOrdersN :[Bookings] = [Bookings]()
                for PIDs in usersnap.childSnapshot(forPath: "past_orders/order_ids").children{
                    let PIDsSnap : DataSnapshot = PIDs as! DataSnapshot
                    
                    self.ref.child("orders").child("all_orders").child(PIDsSnap.key).observeSingleEvent(of: DataEventType.value) { (ordersnap) in
                        
                        self.ref.child("service_providers").child(String(describing :  (ordersnap.childSnapshot(forPath:"sp_id").value)!)).observeSingleEvent(of: DataEventType.value) { (spsnap) in
                            
                            
                            
                            self.ref.child("service_providers_ratings").child(String(describing :  (ordersnap.childSnapshot(forPath:"sp_id").value)!)).child(ordersnap.key).observeSingleEvent(of: DataEventType.value) { (rateSnap) in
                                let rateSnap : DataSnapshot = rateSnap as! DataSnapshot
                                
                                
                                var allservice: String = ""
                                
                                for S in ordersnap.childSnapshot(forPath: "services").children{
                                    let Ssnap : DataSnapshot = S as! DataSnapshot
                                    
                                    
                                    for Se in Ssnap.childSnapshot(forPath: "services").children{
                                        let SeSnap : DataSnapshot = Se as! DataSnapshot
                                        
                                        allservice = allservice + " , " + SeSnap.key
                                        
                                    }
                                    
                                }
                                
                                //
                                //                                    self.LastAddress = ["Address":String(describing :  (ordersnap.childSnapshot(forPath:"customer_raw_address").value)!)
                                //                                                  , "latitude": String(describing :  (ordersnap.childSnapshot(forPath:"customer_latitude").value)!),"longitude":String(describing :  (ordersnap.childSnapshot(forPath:"customer_longitude").value)!)]
                                
                                let milisecond:Int = Int(String(describing :  (ordersnap.childSnapshot(forPath:"time_order_placed").value)!)) ?? 0;
                                //                                                                                           print("KKKKKK",milisecond)
                                let dateVar = Date.init(timeIntervalSince1970: TimeInterval(milisecond)/1000)
                                var dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "dd-MM-yyyy"
                                print(dateFormatter.string(from: dateVar))
                                
                                dateFormatter.locale = Locale(identifier: UserDefaults.standard.string(forKey: "LANSEL") ?? "en")
                                
                                let pO : pastOrderSet = pastOrderSet(id: spsnap.key, driverID: String(describing :  (ordersnap.childSnapshot(forPath:"driver_id").value)!), SpName: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/name".localized()).value)!), Date: dateFormatter.string(from: dateVar), Image: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/image").value)!),snap: spsnap,Address:String(describing :  (ordersnap.childSnapshot(forPath:"customer_raw_address").value)!),latitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_latitude").value)!),longitude:String(describing :  (ordersnap.childSnapshot(forPath:"customer_longitude").value)!), OrderSnap: ordersnap )
                                
                                //                                    self.PastOrderSets.append(pO)
                                var ratings:Double = 0
                                
                                if(!rateSnap.exists()){
                                    
                                    ratings = -1
                                }else{
                                    ratings =   Double(String(describing:rateSnap.childSnapshot(forPath:"rating").value!)) ?? 0.0
                                }
                                
                                print("000000",ratings)
                                let time:Double = Double(String(describing:ordersnap.childSnapshot(forPath:"time_order_placed").value!)) ?? 0.0
                                let bookings : Bookings = Bookings(Amount: String(describing :  (ordersnap.childSnapshot(forPath:"total_price_of_order").value)!), Status: "Completed", ServiceProvider: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/name".localized()).value)!), Services: allservice, ServiceLocation: String(describing :  (ordersnap.childSnapshot(forPath:"customer_raw_address").value)!), Image: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/image").value)!),PhoneNumber: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/phone_number").value)!),OrderID: PIDsSnap.key,ServiceProviderID:spsnap.key,DriverID:String(describing :  (ordersnap.childSnapshot(forPath:"driver_id").value)!) ,ServiceSnap: spsnap, OrderSnap: ordersnap ,latitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_latitude").value)!),longitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_longitude").value)!),rating: ratings,time:time)
                                
                                self.PastOrders.append(bookings)
                                //                                                                                            self.PastOrders.append(contentsOf: PastOrdersN)
                                
                            }
                        }
                    }
                }
                
                
                
            }
            
        }
    }
    
    
    func removeRows(at offsets: IndexSet) {
        //  numbers.remove(atOffsets: offsets)
        guard let index : Int = Array(offsets).first else { return }
        self.length = true
        let ID =  self.PastOrders[index].OrderID
        //        self.Data.remove(at: index)
        print(ID)
        self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("past_orders/order_ids").child(ID).removeValue()
    }
    
    
}

struct NewMyOrder_Previews: PreviewProvider {
    static var previews: some View {
        NewMyOrder()
    }
}
