//
//  SavedAddress.swift
//  Twaddan
//
//  Created by Spine on 17/08/20.
//  Copyright © 2020 spine. All rights reserved.
//
import Firebase
import SwiftUI
import FirebaseDatabase


struct SavedAddress: View {
    let ref = Database.database().reference()
    //     @State var PastOrders :[Bookings] = [Bookings]()
    @State var addressDatas :[AddressDataO] = [AddressDataO]()
    @Environment(\.presentationMode) var presentationMode
    var body: some View {
        GeometryReader{ geometry in
            
            VStack(spacing:5){
                List{
                    Spacer().frame(height:10).foregroundColor(Color("s1"))
                    
                    ForEach(self.addressDatas.reversed(),id: \.self){ address in
                        
                        
                        Button(action:{
                            
                            //                                                UserDefaults.standard.set(Double(order.latitude), forKey: "latitude")
                            //
                            //
                            //                                                UserDefaults.standard.set(Double(order.longitude), forKey: "longitude")
                            //
                            //
                            //
                            //
                            //
                            //
                            //
                            //                                                UserDefaults.standard.set("", forKey: "HOME")
                            //
                            //                                                UserDefaults.standard.set("", forKey: "STREET")
                            //
                            //                                                UserDefaults.standard.set("", forKey: "BUILDING")
                            //                    self.setLocations(bookings: order)
                            //
                            //
                            
                            
                            
                            //                                              UserDefaults.standard.set(nil, forKey: "PLACE")
                            
                            //                                                                           DispatchQueue.global(qos: .background).async {
                            DispatchQueue.main.async {
                                
                                //                                                                                                                                                         let geocoder = GMSGeocoder()
                                //
                                //                                                                                                                       geocoder.reverseGeocodeCoordinate(self.locationManager.location!.coordinate) { response, error in
                                //                                                                                                                                                           //
                                //                                                                                                                                                         if error != nil {
                                //                                                                                                                                                             print("reverse geodcode fail: \(error)")
                                //                                                                                                                                                                     } else {
                                //                                                                                                                                                                         if let places = response?.results()  else {
                                //                                                                                                                                                                             print("GEOCODE: nil in places")
                                //                                                                                                                                                                         }
                                //                                                                                                                                                                     }
                                //                                                                                                                                                         }
                                //                                        //
                                //                                                                                     let georeader = CLGeocoder()
                                //                                                                               georeader.reverseGeocodeLocation(self.locationManager.location!){ (places,err) in
                                //
                                //                                                                                         if err != nil{
                                //
                                //                                                                                             print((err?.localizedDescription)!)
                                //                                                                                             return
                                //                                                                                         }
                                //
                                //
                                //                                               //                                            self.parent.centerLocation = places?.first?.subLocality as! String
                                //                                                                                       //      return places?.first?.locality
                                //                                                                                        //   print(places?.first?.subLocality!)
                                //
                                //
                                //
                                //
                                //
                                //
                                //
                                //                                                                                   UserDefaults.standard.set(Double((self.locationManager.location?.coordinate.latitude)!), forKey: "latitude")
                                //
                                //
                                //                                                                                   UserDefaults.standard.set(Double((self.locationManager.location?.coordinate.longitude)!), forKey: "longitude")
                                //
                                //                                                                                           let str1 = String((places?.first?.name) ?? "")
                                //
                                //
                                //
                                //                                                                                           let str2 = String( (places?.first?.subThoroughfare) ?? ""
                                //                                                                                               )
                                //
                                //                                                                                           let str3 = String( (places?.first?.thoroughfare) ?? ""
                                //                                                                                                                                          )
                                //
                                //
                                //
                                //                                                                                           let str4 = String( (places?.first?.postalCode) ?? ""
                                //                                                                                                                                          )
                                //
                                //
                                //
                                //
                                //                                                                                           let str5 = String( (places?.first?.subLocality) ?? ""
                                //                                                                                                                                          )
                                //
                                //
                                //
                                //
                                //                                                                                           let str6 = String( (places?.first?.locality) ?? ""
                                //                                                                                                                                          )
                                //
                                //
                                //
                                //
                                //                                                                                           let str7 = String( (places?.first?.country) ?? "")
                                //
                                //
                                //                                                                                           UserDefaults.standard.set(str2, forKey: "HOME")
                                //
                                //                                                                                           UserDefaults.standard.set(str5+" , "+str6+" , "+str4+" , "+str7, forKey: "STREET")
                                //
                                //                                                                                           UserDefaults.standard.set(str3, forKey: "BUILDING")
                                //
                                //                                                                                           UserDefaults.standard.set(str5+" , "+str6+" , "+str2+" , "+str3+" , "+str4+" , "+str7, forKey: "PLACE")
                                //
                                //                                                                                                            UserDefaults.standard.set("",forKey: "AHOME")
                                //                                                                                                                                                     UserDefaults.standard.set(str5+" , "+str6+" , "+str4+" , "+str7,forKey: "ASTREET")
                                //                                                                                                                                                      UserDefaults.standard.set("",forKey: "AFLOOR")
                                //                                                                                                                                                   UserDefaults.standard.set("",forKey: "AOFFICE")
                                //                                                                                                                                                   UserDefaults.standard.set("",forKey: "ADNOTE")
                                //                                                                                                 UserDefaults.standard.set("",forKey: "ANICK")
                                //
                                //                                                                                     }
                                
                                
                                //                                            self.placedisplay.toggle()
                                //                                            self.placedisplay.toggle()
                                
                                
                                
                                
                                
                                
                                
                                UserDefaults.standard.set(address.lat, forKey: "latitude")
                                
                                
                                UserDefaults.standard.set(address.lng, forKey: "longitude")
                                
                                
                                
                                
                                //                                                    let str4 = String( (places.first?.postalCode) ?? ""
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                UserDefaults.standard.set(address.house, forKey: "HOME")
                                
                                UserDefaults.standard.set(address.streetName, forKey: "STREET")
                                
                                UserDefaults.standard.set(address.buildingName, forKey: "BUILDING")
                                
                                UserDefaults.standard.set(address.address, forKey: "PLACE")
                                
                                UserDefaults.standard.set(address.house,forKey: "AHOME")
                                UserDefaults.standard.set(address.streetName,forKey: "ASTREET")
                                UserDefaults.standard.set(address.floor,forKey: "AFLOOR")
                                UserDefaults.standard.set(address.office,forKey: "AOFFICE")
                                UserDefaults.standard.set(address.notes,forKey: "ADNOTE")
                                UserDefaults.standard.set(address.addressNickName,forKey: "ANICK")
                                self.presentationMode.wrappedValue.dismiss()
                                //                                                                                                                    self.OpenPartialSheet = false
                                
                                
                                //
                                //                                                    if let place = places.first {
                                //
                                //
                                //                                                        if let lines = place.lines {
                                //                                                            print("GEOCODE: Formatted Address: \(lines)")
                                //
                                //
                                //                                                        }
                                //
                                //
                                //
                                //                                                    } else {
                                //                                                        print("GEOCODE: nil first in places")
                                //                                                    }
                                
                                
                                
                                
                                
                                
                            }
                            
                            
                            
                            
                        }){
                            HStack {
                                
                                //   Spacer().frame(width:20)
                                VStack(alignment: .leading){
                                    
                                    
                                    
                                    HStack{
                                        
                                        
                                        //                                                                                                                                             Text(String((self.PastOrders.firstIndex(of: order) ?? 0 )+1)).font(.custom("Regular_Font".localized(), size: 14)).foregroundColor(Color.white).padding(10).background(Color(#colorLiteral(red: 0, green: 0.2509999871, blue: 0.4979999959, alpha: 1))).clipShape(Circle()).padding(.trailing)
                                        //
                                        //                                                                                                                                                                                                    Image("basicbackimage")
                                        
                                        
                                        
                                        Text(address.addressType)
                                            .font(.custom("Bold_Font".localized(), size: 13)).foregroundColor(Color("h2"))
                                        
                                        
                                        //                                                                                                                                      Text("JJJJJJJ")
                                        
                                        
                                        //                                                                .background(Color.red)
                                        Spacer()
                                        
                                        //                                                                                                                    + " AED".localized())
                                        
                                        //                                                                                                                                                                                                        Text("150 AED")
                                        
                                        //   Text("100 AED")
                                        
                                        
                                    }.padding(10).padding(.vertical,10).background(Color.white)
                                    
                                    
                                    Spacer()
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    //                                                            .background(Color.red)
                                    
                                    
                                    //                                                        Divider()
                                    
                                    
                                    Text( self.getAdressData(address: address))
                                        .font(.custom("Bold_Font".localized(), size: 13))
                                        .foregroundColor(Color("h8")).padding()
                                    
                                    
                                    //            if(address.addressType == "Work"){
                                    //
                                    //                                                                                                    Text("\(self.getAdressData(bookings: order).NickName),\(self.getAdressData(bookings: order).Office ), \(self.getAdressData(bookings: order).Floor ),  \(self.getAdressData(bookings: order).Home ),  \(self.getAdressData(bookings: order).Street), \(self.getAdressData(bookings: order).AdNote)")
                                    //                                                                                                        .font(.custom("Bold_Font".localized(), size: 13))
                                    //                                                                                                        .foregroundColor(Color("h8")).padding()
                                    //
                                    ////                                                                                                    Spacer()
                                    //
                                    //
                                    //            }
                                    //            else{
                                    //
                                    //                Text("\(self.getAdressData(bookings: order).NickName), \(self.getAdressData(bookings: order).Home ),  \(self.getAdressData(bookings: order).Street), \(self.getAdressData(bookings: order).AdNote)")
                                    //                                                                                                                      .font(.custom("Bold_Font".localized(), size: 13))
                                    //                                                                                                                      .foregroundColor(Color("h8")).padding()
                                    //
                                    //            }
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    Spacer()
                                    
                                }
                                
                                .background(Color("ca1"))
                                .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color("ca2"),lineWidth: 2)
                                )                                  .cornerRadius(5)
                                .shadow(radius: 2)
                                
                                // Spacer().frame(width:10)
                                
                            }.padding(10).frame(height:118)
                        }
                    }.onDelete(perform: self.removeRows).listRowInsets(EdgeInsets())
                    .listRowBackground(Color("h1"))
                    
                    Spacer()
                }
                //         .onAppear{
                //                            self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").observe(DataEventType.value) { (usersnap) in
                //
                //                                self.PastOrders.removeAll()
                //
                //                                for AIDs in usersnap.childSnapshot(forPath: "active_orders/order_ids").children{
                //                                    let AIDsSnap : DataSnapshot = AIDs as! DataSnapshot
                //
                //                                    self.ref.child("orders").child("all_orders").child(AIDsSnap.key).observeSingleEvent(of: DataEventType.value) { (ordersnap) in
                //
                //                                        self.ref.child("service_providers").child(String(describing :  (ordersnap.childSnapshot(forPath:"sp_id").value)!)).observeSingleEvent(of: DataEventType.value) { (spsnap) in
                //
                //                                            var allservice: String = ""
                //
                //                                            for S in ordersnap.childSnapshot(forPath: "services").children{
                //                                                let Ssnap : DataSnapshot = S as! DataSnapshot
                //
                //
                //                                                for Se in Ssnap.childSnapshot(forPath: "services").children{
                //                                                    let SeSnap : DataSnapshot = Se as! DataSnapshot
                //
                //                                                    allservice = allservice + " , " + SeSnap.key
                //
                //                                                }
                //
                //                                            }
                //                                            let ratings:Double = -1
                //                                                                  let time:Double = 0
                //
                //                                            let bookings : Bookings = Bookings(Amount: String(describing :  (ordersnap.childSnapshot(forPath:"total_price_of_order").value)!), Status: "Booked", ServiceProvider: String(describing :  (spsnap.childSnapshot(forPath: "personal_information/name".localized()).value)!), Services: allservice, ServiceLocation: String(describing :  (ordersnap.childSnapshot(forPath:"customer_raw_address").value)!), Image: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/image").value)!),PhoneNumber: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/phone_number").value)!),OrderID: AIDsSnap.key,ServiceProviderID:spsnap.key ,DriverID:String(describing :  (ordersnap.childSnapshot(forPath:"driver_id").value)!) ,ServiceSnap: spsnap, OrderSnap: ordersnap,latitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_latitude").value)!),longitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_longitude").value)!),rating: ratings,time: time)
                //
                //                                            self.PastOrders.append(bookings)
                //                                        }
                //                                    }
                //                                }
                //
                //
                //        //                        self.PastOrderSets.removeAll()
                //
                //                                for PIDs in usersnap.childSnapshot(forPath: "past_orders/order_ids").children{
                //                                    let PIDsSnap : DataSnapshot = PIDs as! DataSnapshot
                //
                //                                    self.ref.child("orders").child("all_orders").child(PIDsSnap.key).observeSingleEvent(of: DataEventType.value) { (ordersnap) in
                //
                //                                        self.ref.child("service_providers").child(String(describing :  (ordersnap.childSnapshot(forPath:"sp_id").value)!)).observeSingleEvent(of: DataEventType.value) { (spsnap) in
                //
                //
                //
                //                                                                            self.ref.child("service_providers_ratings").child(String(describing :  (ordersnap.childSnapshot(forPath:"sp_id").value)!)).child(ordersnap.key).observeSingleEvent(of: DataEventType.value) { (rateSnap) in
                //                                                                              let rateSnap : DataSnapshot = rateSnap as! DataSnapshot
                //
                //
                //                                                                            var allservice: String = ""
                //
                //                                                                            for S in ordersnap.childSnapshot(forPath: "services").children{
                //                                                                                let Ssnap : DataSnapshot = S as! DataSnapshot
                //
                //
                //                                                                                for Se in Ssnap.childSnapshot(forPath: "services").children{
                //                                                                                    let SeSnap : DataSnapshot = Se as! DataSnapshot
                //
                //                                                                                    allservice = allservice + " , " + SeSnap.key
                //
                //                                                                                }
                //
                //                                                                            }
                //
                //                                        //
                //                                        //                                    self.LastAddress = ["Address":String(describing :  (ordersnap.childSnapshot(forPath:"customer_raw_address").value)!)
                //                                        //                                                  , "latitude": String(describing :  (ordersnap.childSnapshot(forPath:"customer_latitude").value)!),"longitude":String(describing :  (ordersnap.childSnapshot(forPath:"customer_longitude").value)!)]
                //
                //                                                                            let milisecond:Int = Int(String(describing :  (ordersnap.childSnapshot(forPath:"time_order_placed").value)!)) ?? 0;
                ////                                                                                                   print("KKKKKK",milisecond)
                //                                                                                                   let dateVar = Date.init(timeIntervalSince1970: TimeInterval(milisecond)/1000)
                //                                                                                                   var dateFormatter = DateFormatter()
                //                                                                                                   dateFormatter.dateFormat = "dd-MM-yyyy"
                //                                                                                                   print(dateFormatter.string(from: dateVar))
                //
                //                                                                             dateFormatter.locale = Locale(identifier: UserDefaults.standard.string(forKey: "LANSEL") ?? "en")
                //
                //                                                                            let pO : pastOrderSet = pastOrderSet(id: spsnap.key, driverID: String(describing :  (ordersnap.childSnapshot(forPath:"driver_id").value)!), SpName: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/name").value)!), Date: dateFormatter.string(from: dateVar), Image: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/image").value)!),snap: spsnap,Address:String(describing :  (ordersnap.childSnapshot(forPath:"customer_raw_address").value)!),latitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_latitude").value)!),longitude:String(describing :  (ordersnap.childSnapshot(forPath:"customer_longitude").value)!), OrderSnap: ordersnap )
                //
                //                                        //                                    self.PastOrderSets.append(pO)
                //                                                                            var ratings:Double = 0
                //
                //                                                                                if(!rateSnap.exists()){
                //
                //                                                                                    ratings = -1
                //                                                                                }else{
                //                                                                                 ratings =   Double(String(describing:rateSnap.childSnapshot(forPath:"rating").value)) ?? 0.0
                //                                                                                }
                //                                                                                                  let time:Double = Double(String(describing:ordersnap.childSnapshot(forPath:"time_order_placed").value!)) ?? 0.0
                //                                                                            let bookings : Bookings = Bookings(Amount: String(describing :  (ordersnap.childSnapshot(forPath:"total_price_of_order").value)!), Status: "Completed", ServiceProvider: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/name").value)!), Services: allservice, ServiceLocation: String(describing :  (ordersnap.childSnapshot(forPath:"customer_raw_address").value)!), Image: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/image").value)!),PhoneNumber: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/phone_number").value)!),OrderID: PIDsSnap.key,ServiceProviderID:spsnap.key,DriverID:String(describing :  (ordersnap.childSnapshot(forPath:"driver_id").value)!) ,ServiceSnap: spsnap, OrderSnap: ordersnap ,latitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_latitude").value)!),longitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_longitude").value)!),rating: ratings,time:time)
                //
                //                                                                            self.PastOrders.append(bookings)
                //                                                                        }
                //                                                                    }
                //                                    }
                //                                }
                //
                //
                //
                //
                //
                //
                //
                //
                //
                //
                //
                //                            }
                //
                //        }
            }.frame(width:UIScreen.main.bounds.width,height:UIScreen.main.bounds.height)
        }.background(Color("s1"))
        
        .onAppear{
            self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("saved_address").observeSingleEvent(of: DataEventType.value, with: { (datasnapshot) in
                self.addressDatas.removeAll()
                for ordersnap in datasnapshot.children{
                    let ordersnap : DataSnapshot = ordersnap as! DataSnapshot
                    //                    var addressD = AddressDataO(address: String(describing :  (ordersnap.childSnapshot(forPath:"address").value)!), addressId: "", addressNickName: "", addressType: "", buildingName: "", floor: "", house: "", lat: 0.0, lng: 0.0, notes: "", office: "", streetName: "")
                    let     addressD = AddressDataO(address: String(describing :  (ordersnap.childSnapshot(forPath:"address").value!)), addressId: String(describing :  (ordersnap.childSnapshot(forPath:"addressId").value!)), addressNickName: String(describing :  (ordersnap.childSnapshot(forPath:"addressNickName").value!)), addressType: String(describing :  (ordersnap.childSnapshot(forPath:"addressType").value!)), buildingName: String(describing :  (ordersnap.childSnapshot(forPath:"buildingName").value!)), floor: String(describing :  (ordersnap.childSnapshot(forPath:"floor").value!)), house: String(describing :  (ordersnap.childSnapshot(forPath:"house").value!)), lat: Double(String(describing:ordersnap.childSnapshot(forPath:"lat").value!)) ?? 0.0, lng: Double(String(describing:ordersnap.childSnapshot(forPath:"lng").value!)) ?? 0.0, notes: String(describing :  (ordersnap.childSnapshot(forPath:"notes").value!)), office: String(describing :  (ordersnap.childSnapshot(forPath:"office").value!)), streetName: String(describing :  (ordersnap.childSnapshot(forPath:"streetName").value!)))
                    
                    self.addressDatas.append(addressD)
                }
                
            })
            
        }
        
    }
    func removeRows(at offsets: IndexSet) {
        //  numbers.remove(atOffsets: offsets)
        guard let index : Int = Array(offsets).first else { return }
        
        let ID =  self.addressDatas[index].addressId
        //        self.Data.remove(at: index)
        
        self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("saved_address").child(ID).removeValue()
    }
    //    func setLocations(bookings:Bookings)  {
    //          let items = bookings.ServiceLocation.components(separatedBy: "\n")
    //
    //        print("AAAAAAA1",items)
    //        let part1 =  items[0].components(separatedBy: " ")
    //        print("AAAAAAA",part1)
    //
    //        var NickName = ""
    //        let Type = part1[0]
    //        print("AAAAAAA",Type)
    //
    //
    //        if(part1.count>1){
    //            NickName = part1[1].replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "")
    //        }
    //        var Home = ""
    //         var Floor = ""
    //         var Office = ""
    //         var Street = ""
    //
    //        var Adnote = ""
    //        var Address = ""
    //
    //        var part2 = [""]
    //
    //        if(items.count>1){
    //          part2 = items[1].components(separatedBy: ",")
    //
    //        }
    //        print("AAAAAAA",part2)
    //
    //        if(Type == ("Work")){
    //
    //
    //            Home = part2[0]
    //
    //            if(part2.count>1){
    //            Floor = part2[1]
    //            }
    //             if(part2.count>2){
    //            Office = part2[2]
    //            }
    //
    //                 if(part2.count>3){
    //            Street = part2[3]
    //                }
    //
    //            print("AAAAAAA",Home)
    //            print("AAAAAAA",Office)
    //
    //
    //
    //        }else{
    //
    //
    //            Home = part2[0]
    //                      if(part2.count>1){
    //                       Street = part2[1]
    //            }
    //             print("AAAAAAAHOM",Home)
    //        }
    //
    //        if(items.count>2){
    //        Adnote = items[2].replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "")
    //        }
    //        if(items.count>3){
    //            Address = items [3].components(separatedBy: ":")[1]
    //        }
    //
    //
    //            print("AAAAAAA",Home,"H",Street,"S",Floor,"F",Office,"O")
    //
    //                             UserDefaults.standard.set(Home,forKey: "AHOME")
    //                                                                               UserDefaults.standard.set(Street,forKey: "ASTREET")
    //                                                                                UserDefaults.standard.set(Floor,forKey: "AFLOOR")
    //                                                                             UserDefaults.standard.set(Office,forKey: "AOFFICE")
    //           UserDefaults.standard.set(NickName,forKey: "ANICK")
    //
    //
    //                                                                             UserDefaults.standard.set(Adnote,forKey: "ADNOTE")
    //                                   UserDefaults.standard.set(Address,forKey: "PLACE")
    //    }
    
    func getAdressData(address:AddressDataO) -> String {
        
        return "\(address.addressType) - \(address.addressNickName) - \(address.house) ,\(address.streetName) "
    }
    
    //    func getAdressData(bookings:Bookings) -> AddressData {
    //          let items = bookings.ServiceLocation.components(separatedBy: "\n")
    //
    //        print("AAAAAAA1",items)
    //        let part1 =  items[0].components(separatedBy: " ")
    //        print("AAAAAAA",part1)
    //
    //        var NickName = ""
    //        let Type = part1[0]
    //        print("AAAAAAA",Type)
    //
    //
    //        if(part1.count>1){
    //            NickName = part1[1].replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "")
    //        }
    //        var Home = ""
    //         var Floor = ""
    //         var Office = ""
    //         var Street = ""
    //
    //        var Adnote = ""
    //        var Address = ""
    //
    //        var part2 = [""]
    //
    //        if(items.count>1){
    //          part2 = items[1].components(separatedBy: ",")
    //
    //        }
    //        print("AAAAAAA",part2)
    //
    //        if(Type == ("Work")){
    //
    //
    //            Home = part2[0]
    //
    //            if(part2.count>1){
    //            Floor = part2[1]
    //            }
    //             if(part2.count>2){
    //            Office = part2[2]
    //            }
    //
    //                 if(part2.count>3){
    //            Street = part2[3]
    //                }
    //
    //            print("AAAAAAA",Home)
    //            print("AAAAAAA",Office)
    //
    //
    //
    //        }else{
    //
    //
    //            Home = part2[0]
    //                      if(part2.count>1){
    //                       Street = part2[1]
    //            }
    //             print("AAAAAAAHOM",Home)
    //        }
    //
    //        if(items.count>2){
    //        Adnote = items[2].replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "")
    //        }
    //        if(items.count>3){
    //            Address = items [3].components(separatedBy: ":")[1]
    //        }
    //
    //
    //            print("AAAAAAA",Home,"H",Street,"S",Floor,"F",Office,"O")
    //
    //
    //        let R:AddressDataw = AddressData(TYPE: Type, NickName: NickName, Home: Home, Raw: bookings.ServiceLocation, Street: Street, Floor: Floor, Office: Office, AdNote: Adnote)
    //
    //        return R
    //    }
    
    //    struct AddressDataw {
    //        var TYPE : String
    //        var NickName : String
    //        var Home : String
    //        var Raw : String
    //        var Street : String
    //        var Floor : String
    //        var Office : String
    //        var AdNote : String
    //    }
}



struct SavedAddress_Previews: PreviewProvider {
    static var previews: some View {
        SavedAddress()
    }
}
struct AddressDataO : Hashable{
    var address : String = ""
    var addressId : String = ""
    var addressNickName : String = ""
    var addressType : String = ""
    var buildingName : String = ""
    var floor : String = ""
    var house : String = ""
    var lat : Double = 0.0
    var lng : Double = 0.0
    var notes : String = ""
    var office : String = ""
    var streetName : String = ""
}
