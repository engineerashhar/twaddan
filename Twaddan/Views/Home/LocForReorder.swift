//
//  LocForReorder.swift
//  Twaddan
//
//  Created by Spine on 22/08/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Firebase
import SwiftUI
import MapKit
import FirebaseDatabase
import GoogleMaps

struct LocForReorder: View {
    
    @Binding var OpenPartialSheet:Bool
    @Binding var goToMap:Bool
    
    var PastOrders : [Bookings]
    
    
    @Binding var bookings:Bookings
    @Binding var Driver:String
    @Binding var ETA:Double
    @Binding var SuccessETA :String
    
    @Binding var Output:Bookings
    let ref = Database.database().reference()
    
    @ObservedObject private var locationManager = LocationManager()
    
    @State var CalculateETA : Bool = false
    //    @State var myLocation : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 11.11, longitude: 24.11)
    
    @Binding var LocationName:String
    @Binding var Location:CLLocation
    
    @State var addressDatas :[AddressDataO] = [AddressDataO]()
    var body: some View {
        
        GeometryReader{ geomenty in
            
            
            
            
            VStack{
                NavigationLink(destination : FindLocation(CalculateETA: self.$CalculateETA).navigationBarTitle("Map",displayMode: .inline) ,isActive: self.$goToMap ){
                    
                    Text("").frame(height:0)
                }
                
                Spacer()
                
                
                VStack{
                    
                    HStack{
                        Spacer()
                        Button(action:{
                            withAnimation{
                                self.OpenPartialSheet = false
                            }
                        }){
                            Text("Close").font(.custom("Regular_Font".localized(), size: 14)).padding()
                        }
                    }
                    
                    //                    HStack{
                    //                        Text("Location used for this booking").font(.custom("ExtraBold_Font".localized(), size: 16)).foregroundColor(Color("darkthemeletter"))
                    //                        Spacer()
                    //                    }.padding(.horizontal)
                    
                    //                    Button(action: {
                    //
                    //
                    //                        UserDefaults.standard.set(Double(self.bookings.latitude), forKey: "latitude")
                    //
                    //
                    //                        UserDefaults.standard.set(Double(self.bookings.longitude), forKey: "longitude")
                    //
                    //
                    //
                    //
                    //
                    //
                    //
                    //                        UserDefaults.standard.set("", forKey: "HOME")
                    //
                    //                        UserDefaults.standard.set("", forKey: "STREET")
                    //
                    //                        UserDefaults.standard.set("", forKey: "BUILDING")
                    //
                    ////                        UserDefaults.standard.set(self.bookings.ServiceLocation, forKey: "PLACE")
                    //
                    //
                    //                        self.setLocations()
                    //
                    //
                    //
                    //
                    //
                    //
                    //
                    //                        withAnimation{
                    //                            self.FindDriver()
                    //                            self.OpenPartialSheet = false
                    //                        }
                    //
                    //                    }
                    //                    ){
                    //
                    //                        HStack{
                    //                            Image("placeholder").resizable().frame(width:20,height: 20).padding().padding(.leading,10).foregroundColor(Color.blue)
                    //
                    //                            Text(String(self.bookings.ServiceLocation ?? "Twaddan")).font(.custom("Regular_Font".localized(), size: 14))
                    //                                .foregroundColor(Color("MainFontColor1")).padding(.trailing,20).lineLimit(1)
                    //                            Spacer()
                    //
                    //                        }
                    //
                    //                         .frame(width:UIScreen.main.bounds.size.width-30).background(Color.white)
                    //
                    //                    }
                    //
                    
                    HStack{
                        Text("Recent Locations").font(.custom("ExtraBold_Font".localized(), size: 16)).foregroundColor(Color("darkthemeletter")).padding(.horizontal)
                        Spacer()
                    }
                    
                    
                    //                    List{
                    ForEach(self.addressDatas.reversed().prefix(3),id: \.self){ order in
                        
                        VStack{
                            Button(action:{
                                
                                //
                                //                                UserDefaults.standard.set(Double(order.latitude), forKey: "latitude")
                                //
                                //
                                //                                UserDefaults.standard.set(Double(order.longitude), forKey: "longitude")
                                //
                                //
                                //
                                //
                                //
                                //
                                //
                                //                                UserDefaults.standard.set("", forKey: "HOME")
                                //
                                //                                UserDefaults.standard.set("", forKey: "STREET")
                                //
                                //                                UserDefaults.standard.set("", forKey: "BUILDING")
                                //
                                //                                self.setLocations()
                                //                                UserDefaults.standard.set("",forKey: "AHOME")
                                //                                                                                  UserDefaults.standard.set("",forKey: "ASTREET")
                                //                                                                                   UserDefaults.standard.set("",forKey: "AFLOOR")
                                //                                                                                UserDefaults.standard.set("",forKey: "AOFFICE")
                                //                                                                                UserDefaults.standard.set("",forKey: "ADNOTE")
                                //
                                //                                UserDefaults.standard.set(order.location, forKey: "PLACE")
                                
                                
                                
                                UserDefaults.standard.set(order.lat, forKey: "latitude")
                                
                                
                                UserDefaults.standard.set(order.lng, forKey: "longitude")
                                
                                
                                
                                
                                //                                                    let str4 = String( (places.first?.postalCode) ?? ""
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                UserDefaults.standard.set(order.house, forKey: "HOME")
                                
                                UserDefaults.standard.set(order.streetName, forKey: "STREET")
                                
                                UserDefaults.standard.set(order.buildingName, forKey: "BUILDING")
                                
                                UserDefaults.standard.set(order.address, forKey: "PLACE")
                                
                                UserDefaults.standard.set(order.house,forKey: "AHOME")
                                UserDefaults.standard.set(order.streetName,forKey: "ASTREET")
                                UserDefaults.standard.set(order.floor,forKey: "AFLOOR")
                                UserDefaults.standard.set(order.office,forKey: "AOFFICE")
                                UserDefaults.standard.set(order.notes,forKey: "ADNOTE")
                                UserDefaults.standard.set(order.addressNickName,forKey: "ANICK")
                                
                                
                                withAnimation{
                                    self.FindDriver()
                                    self.OpenPartialSheet = false
                                }
                                
                            }){
                                HStack{
                                    Image("placeholder").resizable().frame(width:20,height: 20).padding().padding(.leading,10).foregroundColor(Color.black)
                                    
                                    Text( "\(order.addressType) - \(order.addressNickName) ").font(.custom("Regular_Font".localized(), size: 14))
                                        .foregroundColor(Color("MainFontColor1")).padding(.trailing,20).lineLimit(1)
                                    Spacer()
                                    
                                }
                                
                                .frame(width:UIScreen.main.bounds.size.width-30).background(Color.white)
                            }
                            
                            Divider()
                        }
                    }
                    //                    }.listRowInsets(EdgeInsets()).frame(height: geomenty.size.height*0.2).padding(.leading,-4)
                    //                    Divider()
                    Group{
                        Button(action:{
                            
                            if(true){
                                
                                
                                let geocoder = GMSGeocoder()
                                
                                geocoder.reverseGeocodeCoordinate(self.locationManager.location!.coordinate) { response, error in
                                    //
                                    if error != nil {
                                        print("reverse geodcode fail: \(error)")
                                    } else {
                                        if let places = response?.results() {
                                            let places : [GMSAddress] = places
                                            
                                            
                                            print("ooooooooo","3")
                                            UserDefaults.standard.set(Double(UserDefaults.standard.double(forKey: "CLLOCATION_LAT") ), forKey: "latitude")
                                            
                                            
                                            UserDefaults.standard.set(Double(UserDefaults.standard.double(forKey: "CLLOCATION_LAN") ), forKey: "longitude")
                                            let str1 = String((places.first?.thoroughfare) ?? "")
                                            
                                            print("str1",str1)
                                            
                                            let str2 = String( (places.first?.subLocality) ?? ""
                                                               
                                                               
                                            )
                                            print("str2",places.first?.lines)
                                            let str3 = String( (places.first?.locality) ?? ""
                                            )
                                            
                                            print("str3",str3)
                                            
                                            //                                                    let str4 = String( (places.first?.postalCode) ?? ""
                                            
                                            
                                            
                                            
                                            
                                            let str5 = String( (places.first?.administrativeArea) ?? ""
                                            )
                                            
                                            print("str5",str5)
                                            
                                            
                                            //                                                    let str6 = String( (places.first?.locality) ?? ""
                                            
                                            
                                            
                                            
                                            
                                            let str7 = String( (places.first?.country) ?? "")
                                            print("str7",str7)
                                            
                                            UserDefaults.standard.set(str1, forKey: "HOME")
                                            
                                            UserDefaults.standard.set(str1+" , "+str2+" , "+str3, forKey: "STREET")
                                            
                                            UserDefaults.standard.set(str1, forKey: "BUILDING")
                                            
                                            UserDefaults.standard.set(places.first?.lines![0], forKey: "PLACE")
                                            
                                            UserDefaults.standard.set("",forKey: "AHOME")
                                            UserDefaults.standard.set(str1+" , "+str2+" , "+str3,forKey: "ASTREET")
                                            UserDefaults.standard.set("",forKey: "AFLOOR")
                                            UserDefaults.standard.set("",forKey: "AOFFICE")
                                            UserDefaults.standard.set("",forKey: "ADNOTE")
                                            UserDefaults.standard.set("",forKey: "ANICK")
                                            
                                            withAnimation{
                                                self.FindDriver()
                                                self.OpenPartialSheet = false
                                            }
                                            //
                                            //                                                    if let place = places.first {
                                            //
                                            //
                                            //                                                        if let lines = place.lines {
                                            //                                                            print("GEOCODE: Formatted Address: \(lines)")
                                            //
                                            //
                                            //                                                        }
                                            //
                                            //
                                            //
                                            //                                                    } else {
                                            //                                                        print("GEOCODE: nil first in places")
                                            //                                                    }
                                        } else {
                                            print("GEOCODE: nil in places")
                                        }
                                    }
                                }
                                
                                //                                                                                                    let georeader = CLGeocoder()
                                //                                                                                              georeader.reverseGeocodeLocation(self.locationManager.location!){ (places,err) in
                                //
                                //                                                                                                        if err != nil{
                                //
                                //                                                                                                            print((err?.localizedDescription)!)
                                //                                                                                                            return
                                //                                                                                                        }
                                //
                                //
                                //                                                              //                                            self.parent.centerLocation = places?.first?.subLocality as! String
                                //                                                                                                      //      return places?.first?.locality
                                //                                                                                                       //   print(places?.first?.subLocality!)
                                //
                                //
                                //
                                //
                                //
                                //
                                //
                                //                                                                                                UserDefaults.standard.set(Double((self.Location.coordinate.latitude)), forKey: "latitude")
                                //
                                //
                                //                                                                                                UserDefaults.standard.set(Double((self.Location.coordinate.longitude)), forKey: "longitude")
                                //
                                //                                                                                                          let str1 = String((places?.first?.name) ?? "")
                                //
                                //
                                //
                                //                                                                                                          let str2 = String( (places?.first?.subThoroughfare) ?? ""
                                //                                                                                                              )
                                //
                                //                                                                                                          let str3 = String( (places?.first?.thoroughfare) ?? ""
                                //                                                                                                                                                         )
                                //
                                //
                                //
                                //                                                                                                          let str4 = String( (places?.first?.postalCode) ?? ""
                                //                                                                                                                                                         )
                                //
                                //
                                //
                                //
                                //                                                                                                          let str5 = String( (places?.first?.subLocality) ?? ""
                                //                                                                                                                                                         )
                                //
                                //
                                //
                                //
                                //                                                                                                          let str6 = String( (places?.first?.locality) ?? ""
                                //                                                                                                                                                         )
                                //
                                //
                                //
                                //
                                //                                                                                                          let str7 = String( (places?.first?.country) ?? "")
                                //
                                //
                                //                                                                                                          UserDefaults.standard.set(str2, forKey: "HOME")
                                //
                                //                                                                                                          UserDefaults.standard.set(str5+" , "+str6+" , "+str4+" , "+str7, forKey: "STREET")
                                //
                                //                                                                                                          UserDefaults.standard.set(str3, forKey: "BUILDING")
                                //
                                //                                                                                                          UserDefaults.standard.set(str5+" , "+str6+" , "+str2+" , "+str3+" , "+str4+" , "+str7, forKey: "PLACE")
                                //
                                //                                                                                                                           UserDefaults.standard.set("",forKey: "AHOME")
                                //                                                                                                                                                                    UserDefaults.standard.set(str5+" , "+str6+" , "+str4+" , "+str7,forKey: "ASTREET")
                                //                                                                                                                                                                     UserDefaults.standard.set("",forKey: "AFLOOR")
                                //                                                                                                                                                                  UserDefaults.standard.set("",forKey: "AOFFICE")
                                //                                                                                                                                                                  UserDefaults.standard.set("",forKey: "ADNOTE")
                                //                                                                                                                UserDefaults.standard.set("",forKey: "ANICK")
                                //
                                //                                                                                                    }
                                
                            }
                            //                                                           self.placedisplay.toggle()
                            //                                                           self.placedisplay.toggle()
                            
                        }){
                            HStack{
                                Image("select").resizable().frame(width:20,height: 20).padding().padding(.leading,25).foregroundColor(Color.black)
                                VStack(alignment:.leading){
                                    Text("Deliver to current location").font(.custom("ExtraBold_Font".localized(), size: 14)).foregroundColor(Color("darkthemeletter"))
                                    Text(UserDefaults.standard.string(forKey: "CLLOCATION_NAME") ?? "").font(.custom("Regular_Font".localized(), size: 13)).foregroundColor(Color("darkthemeletter"))
                                    
                                }
                                Spacer()
                            }
                        }
                        
                        //                                                          Divider()
                        
                        
                        Divider()
                        
                        
                        
                        
                        Button(action:{
                            self.goToMap = true
                            //                    self.OpenPartialSheet = false
                        }){
                            HStack{
                                Image("pin_new").resizable().frame(width:10,height: 25).padding().padding(.leading,30).foregroundColor(Color.black)
                                VStack{
                                    Text("Deliver to different location").font(.custom("ExtraBold_Font".localized(), size: 14)).foregroundColor(Color("darkthemeletter"))
                                    Text("Choose location on the map").font(.custom("Regular_Font".localized(), size: 13)).foregroundColor(Color("darkthemeletter"))
                                    
                                }
                                Spacer()
                            }
                        }
                        
                        Divider()
                    }
                    Spacer().frame(height:50)
                    
                }.background(RoundedCorners(color: .white, tl: 30, tr: 30, bl: 0, br: 0))
                
                
            }.onAppear{
                
                
                
                self.locationManager.getLocationName()
                print("AAAAAAAA",
                      "a",  self.locationManager.getLocationName())
                if(self.CalculateETA){
                    self.FindDriver()
                    self.SuccessETA = "1"
                    self.Output = self.bookings
                    self.OpenPartialSheet = false
                    print(String(self.OpenPartialSheet))
                    
                }
                
                
                
                self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("saved_address").observeSingleEvent(of: DataEventType.value, with: { (datasnapshot) in
                    self.addressDatas.removeAll()
                    for ordersnap in datasnapshot.children{
                        let ordersnap : DataSnapshot = ordersnap as! DataSnapshot
                        //                    var addressD = AddressDataO(address: String(describing :  (ordersnap.childSnapshot(forPath:"address").value)!), addressId: "", addressNickName: "", addressType: "", buildingName: "", floor: "", house: "", lat: 0.0, lng: 0.0, notes: "", office: "", streetName: "")
                        let     addressD = AddressDataO(address: String(describing :  (ordersnap.childSnapshot(forPath:"address").value!)), addressId: String(describing :  (ordersnap.childSnapshot(forPath:"addressId").value!)), addressNickName: String(describing :  (ordersnap.childSnapshot(forPath:"addressNickName").value!)), addressType: String(describing :  (ordersnap.childSnapshot(forPath:"addressType").value!)), buildingName: String(describing :  (ordersnap.childSnapshot(forPath:"buildingName").value!)), floor: String(describing :  (ordersnap.childSnapshot(forPath:"floor").value!)), house: String(describing :  (ordersnap.childSnapshot(forPath:"house").value!)), lat: Double(String(describing:ordersnap.childSnapshot(forPath:"lat").value!)) ?? 0.0, lng: Double(String(describing:ordersnap.childSnapshot(forPath:"lng").value!)) ?? 0.0, notes: String(describing :  (ordersnap.childSnapshot(forPath:"notes").value!)), office: String(describing :  (ordersnap.childSnapshot(forPath:"office").value!)), streetName: String(describing :  (ordersnap.childSnapshot(forPath:"streetName").value!)))
                        
                        self.addressDatas.append(addressD)
                    }
                    
                })
                
                
                
                
                
            }.clipped().shadow(radius: 2) .offset(y: self.OpenPartialSheet ? 0 : 1350)
            .gesture(
                DragGesture(minimumDistance: 50)
                    .onEnded { _ in
                        withAnimation{
                            self.OpenPartialSheet.toggle()
                            
                        }
                    }
            )    .navigationBarTitle("Booking",displayMode: .inline)
            //                .navigationBarHidden(true)
            
        }
    }
    
    func setLocations()  {
        let items = self.bookings.ServiceLocation.components(separatedBy: "\n")
        
        print("AAAAAAA1",items)
        let part1 =  items[0].components(separatedBy: " ")
        print("AAAAAAA",part1)
        
        var NickName = ""
        let Type = part1[0]
        print("AAAAAAA",Type)
        
        
        if(part1.count>1){
            NickName = part1[1].replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "")
        }
        var Home = ""
        var Floor = ""
        var Office = ""
        var Street = ""
        
        var Adnote = ""
        var Address = ""
        
        var part2 = [""]
        
        if(items.count>1){
            part2 = items[1].components(separatedBy: ",")
            
        }
        print("AAAAAAA",part2)
        
        if(Type == ("Work")){
            
            
            Home = part2[0]
            
            if(part2.count>1){
                Floor = part2[1]
            }
            if(part2.count>2){
                Office = part2[2]
            }
            
            if(part2.count>3){
                Street = part2[3]
            }
            
            print("AAAAAAA",Home)
            print("AAAAAAA",Office)
            
            
            
        }else{
            
            
            Home = part2[0]
            if(part2.count>1){
                Street = part2[1]
            }
            print("AAAAAAAHOM",Home)
        }
        
        if(items.count>2){
            Adnote = items[2].replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "")
        }
        if(items.count>3){
            Address = items [3].components(separatedBy: ":")[1]
        }
        
        
        print("AAAAAAA",Home,"H",Street,"S",Floor,"F",Office,"O")
        
        UserDefaults.standard.set(Home,forKey: "AHOME")
        UserDefaults.standard.set(Street,forKey: "ASTREET")
        UserDefaults.standard.set(Floor,forKey: "AFLOOR")
        UserDefaults.standard.set(Office,forKey: "AOFFICE")
        UserDefaults.standard.set(NickName,forKey: "ANICK")
        
        
        UserDefaults.standard.set(Adnote,forKey: "ADNOTE")
        UserDefaults.standard.set(Address,forKey: "PLACE")
    }
    
    func FindDriver (){
        
        //                    self.Snap = self.booking.ServiceSnap
        
        var selectedETAD : ETAAndDriver = ETAAndDriver(ETA:10000000.0 , DriverID: "")
        
        
        
        //            let     allDriver :DataSnapshot = allDriverSnapshot as! DataSnapshot
        
        var x = self.bookings.ServiceSnap.childSnapshot(forPath: "drivers_info/active_drivers_id").childrenCount
        
        
        for driver  in  self.bookings.ServiceSnap.childSnapshot(forPath: "drivers_info/active_drivers_id").children{
            let  d = driver as! DataSnapshot
            
            //                                   print("UUUUUUUU",driverSnap.key,(String(describing :  (allDriver.value)!)))
            
            self.ref.child("drivers").child(d.key).observeSingleEvent(of: DataEventType.value) { (driverSnap)
                in
                
                
                //                print( (String(describing :  (allDriver.childSnapshot(forPath:"\(driverSnap.key)/live_location/latitude").value)!), (String(describing :  (allDriver.childSnapshot(forPath:"\(driverSnap.key)/live_location/longitude").value)!))))
                //
                
                
                var driverlat =
                    25.0
                
                var driverlon = 55.635908
                var lastETA : Double = 0
                
                
                if(String(describing:  driverSnap.childSnapshot(forPath: "live_location/time_driver_engaged").value!) != "0"){
                    
                    lastETA = Double(String(describing :  (driverSnap.childSnapshot(forPath:"live_location/time_driver_engaged").value)!)) ?? 0
                    
                    
                    
                    
                    
                    
                    driverlat =
                        Double(String(describing :  (driverSnap.childSnapshot(forPath:"live_location/lc_lat").value)!)) ?? 25.0
                    
                    //  }
                    
                    
                    
                    //   if(driversnap.childSnapshot(forPath:"live_location/longitude").exists()){
                    driverlon =
                        Double(String(describing :  (driverSnap.childSnapshot(forPath:"live_location/lc_lng").value)!)) ?? 55
                    
                    
                }else{
                    
                    print("QQQQQQQQQQ2",driverSnap.key)
                    driverlat =
                        Double(String(describing :  (driverSnap.childSnapshot(forPath:"live_location/latitude").value)!)) ?? 25.0
                    
                    //  }
                    
                    
                    
                    //   if(driversnap.childSnapshot(forPath:"live_location/longitude").exists()){
                    driverlon =
                        Double(String(describing :  (driverSnap.childSnapshot(forPath:"live_location/longitude").value)!)) ?? 55
                    
                }
                
                var driverloc : CLLocation = CLLocation(latitude: driverlat, longitude: driverlon)
                var driver2D : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: driverlat, longitude: driverlon)
                
                
                
                
                let userlat =
                    //                    10.874858
                    UserDefaults.standard.double(forKey: "latitude")
                let userlon =
                    //                    76.449728
                    UserDefaults.standard.double(forKey: "longitude")
                
                
                var userloc : CLLocation = CLLocation(latitude: userlat, longitude: userlon)
                var user2D  : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: userlat, longitude: userlon)
                
                print("RRRRRRRRR",user2D.latitude,user2D.longitude,driver2D.latitude,driver2D.longitude)
                
                var eta:Double = 10000000
                
                let origin = "\(driver2D.latitude),\(driver2D.longitude)"
                let destination = "\(user2D.latitude),\(user2D.longitude)"
                
                
                let S : String =  "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=\(origin)&destinations=\(destination)&mode=driving&key=AIzaSyCYQeItZ7SVEQ24Grjzc3zA1B_2DU_GfPU"
                
                let url = URL(string:S
                )
                //        else {
                //        let error = NSError(domain: "LocalDomain", code: 0, userInfo: [NSLocalizedDescriptionKey: "Failed to create object URL"])
                //        print("Error: \(error)")
                //        return
                //    }
                let config = URLSessionConfiguration.default
                let session = URLSession(configuration: config)
                let task = session.dataTask(with: url!, completionHandler: {
                    (data, response, error) in
                    
                    //                                                                                                                                                   print("11111111",driversnap.key)
                    x = x-1
                    if error != nil {
                        print(error!.localizedDescription)
                    }
                    else {
                        
                        //  print(response)
                        do {
                            if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                                
                                guard let routes = json["rows"] as? NSArray else {
                                    DispatchQueue.main.async {
                                    }
                                    return
                                }
                                if (routes.count > 0) {
                                    let overview_polyline = routes[0] as? NSDictionary
                                    //   let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                                    //  let points = dictPolyline?.object(forKey: "points") as? String
                                    
                                    DispatchQueue.main.async {
                                        //
                                        
                                        
                                        let legs = overview_polyline?["elements"] as! Array<Dictionary<String, AnyObject>>
                                        
                                        let distance = legs[0]["distance"] as? NSDictionary
                                        let distanceValue = distance?["value"] as? Int ?? 0
                                        let distanceDouleValue = distance?["value"] as? Double ?? 0.0
                                        let duration = legs[0]["duration"] as? NSDictionary
                                        let totalDurationInSeconds = duration?["value"] as? Int ?? 0
                                        let durationDouleValue = duration?["value"] as? Double ?? 0.0
                                        
                                        
                                        
                                        
                                        eta = durationDouleValue
                                        print("QQQQQQQQQQ1",driverSnap.key ,eta,lastETA,durationDouleValue)
                                        if((lastETA-(Date().timeIntervalSince1970)*1000) > 0){
                                            eta = eta+(((lastETA/1000)-(Date().timeIntervalSince1970)))
                                            print("WWWWW",(((lastETA/1000)-(Date().timeIntervalSince1970))/60))
                                            
                                            
                                        }
                                        
                                        
                                        
                                        eta = eta+300 ;
                                        
                                        if(eta < 301){
                                            eta = 10000000
                                        }
                                        
                                        print("QQQQQQQQQQ1",driverSnap.key ,eta,lastETA,durationDouleValue)
                                        
                                        print("WWWWW",lastETA,Date().timeIntervalSince1970*1000,eta+lastETA-(Date().timeIntervalSince1970)*1000,eta)
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        if (eta < selectedETAD.ETA){
                                            selectedETAD  = ETAAndDriver(ETA:eta, DriverID: driverSnap.key)
                                            
                                        }
                                        
                                        if(x==0){
                                            print("OOOOOO",x) ;
                                            //                                                                                                               self.ViewLoader = false
                                            if(selectedETAD.DriverID != ""){
                                                self.Driver = selectedETAD.DriverID
                                                self.ETA = selectedETAD.ETA
                                                
                                                self.SuccessETA = "1"
                                                self.Output = self.bookings
                                                //                                                                                                                self.ReOrderToCart(item: item,DriverID:selectedETAD.DriverID, ETA: selectedETAD.ETA )
                                                
                                            }else{
                                                self.SuccessETA = "2"
                                                self.Output = self.bookings
                                            }
                                            
                                        }
                                        print (S,eta)
                                        
                                        
                                    }
                                }
                                else {
                                    print(json)
                                    DispatchQueue.main.async {
                                        self.SuccessETA = "2"
                                        self.Output = self.bookings
                                        //   SVProgressHUD.dismiss()
                                    }
                                }
                            }
                        }
                        catch {
                            print("error in JSONSerialization")
                            DispatchQueue.main.async {
                                self.SuccessETA = "2"
                                self.Output = self.bookings
                                //  SVProgressHUD.dismiss()
                            }
                        }
                    }
                })
                task.resume()
                
                
                
            }
            
            //                                                                    }
            //
            //
            //
            //                                                                }
            
            
            
            
            
            
            
            print("TTTTTTT",x)
            
            
            
            
            
            
            
        }
        
        if(self.bookings.ServiceSnap.childSnapshot(forPath: "drivers_info/active_drivers_id").childrenCount == 0){
            self.SuccessETA = "2"
            self.Output = self.bookings
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        //                                                    }
        
        //                                        }
        //                                                        }
        
        
    }
}
