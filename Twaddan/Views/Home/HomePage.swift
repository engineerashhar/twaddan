//
//  HomePageUS.swift
//  Twaddan
//
//  Created by Spine on 01/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI

import FirebaseAuth
import FirebaseDatabase
import SDWebImageSwiftUI
//import CodeScanner
import MapKit
import LocalAuthentication
import Foundation
import SwiftUIPager
import GoogleMaps

struct HomePage: View {
    
    
    
    
    @State var goToUpdate : Bool = false
    @State var VEHICLES : [VEHICLETYPES] = [VEHICLETYPES]()
    
    //       @State var timer: Timer.TimerPublisher = Timer.publish (every: 0.01, on: .main, in: .common)
    @State var V = false
    
    @State var YouHave1 : Bool = false
    @State var YouHave2 : Bool = false
    @State var Vibrate : Int = 0
    @ObservedObject private var locationManager = LocationManager()
    @State var page: Int = 0
    @State var DisplayActive : Bool = true
    //    @State var navigationController:UINavigationController = UINavigationController()
    @State var RecentAddress : [OrderAddress] = [OrderAddress]()
    @State var ALERT2 : Bool = false
    @State var AlertMessege2 = ""
    @State var OpenLoginPage = false
    @State var goToSplash : Bool = false
    //    @State var FID: Bool = UserDefaults.standard.bool(forKey: "FaceID") ?? false
    @State var OProfile : Bool = false
    @State var ONotification : Bool = false
    
    @State var OBookings : Bool = false
    
    @State var openSPD : Bool = false
    @State var OHelpCenter : Bool = false
    @State var OAboutUS : Bool = false
    @State var OOffers : Bool = false
    @State var goToTrack : Bool = false
    @State var OpenPartialSheet : Bool = false
    
    @State var openVA: Bool = true
    @State var ViewLoader: Bool = false
    @State var PastOrders : [Bookings] = [Bookings]()
    @EnvironmentObject var vehicleItems : VehicleItems
    
    @State var GoToBookDetails : Bool = false
    @State var GTB : Bool = false
    //    @State var SelectedDriver : String = ""
    @State var ETA : Double = 0.0
    @State var OpenCart : Bool = false
    @State var Snap : DataSnapshot = DataSnapshot()
    
    let ref = Database.database().reference()
    
    @State private var isShowingScanner = false
    //       @State var Reorder : String = "0"
    
    @State var openSP : Bool = false
    @State var openSPL : Bool = false
    @State var ALERT : Bool = false
    @State var activeImageIndex = 0
    @State var Stage : Int = 0
    @State var selectedImageIndex = 0
    
    
    @State var timer: Timer.TimerPublisher = Timer.publish (every: 4, on: .main, in: .common)
    @State var timerO: Timer.TimerPublisher = Timer.publish (every: 1, on: .main, in: .common)
    
    
    @ObservedObject var cartFetch = CartFetch()
    @ObservedObject var serviceProviderFetch = FindOffer()
    @State var StreetAddress : String = ""
    @State var menuOpen : Bool = false
    @State var goToMap : Bool = false
    
    @EnvironmentObject var settings : UserSettings
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    
    @State var OLanguageSelPage : Bool = false
    //    @State var localauthsuccess : Bool = false
    //   @State var enablelocalauthentication : Bool = true
    @State var ActiveOrderID:[String] = [String]();
    
    
    
    @State var OpenReorderSheet:Bool = false
    @State var goToMap2:Bool = false
    //       @State var LastAddress:[String:String] = [String:String]()
    
    @State var item:Bookings = Bookings()
    @State var Output:Bookings = Bookings()
    
    
    @State var selectedOrderID:String = "0"
    @State var Driver:String = ""
    
    @State var SuccessETA :String = "0"
    
    @State private var offset = CGSize.zero
    
    
    @State var x : CGFloat = 0
    @State var count : CGFloat = 0
    @State var screen = UIScreen.main.bounds.width - 30
    @State var op : CGFloat = 0
    @State var myLocation : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 11.11, longitude: 24.11)
    
    @State var placedisplay : Bool = false
    //      @State var placedisplay : Bool = true
    
    @State var cartsNumber:Int = 0
    
    
    
    @State var addressDatas :[AddressDataO] = [AddressDataO]()
    
    
    init() {
        
        
        
        //        print("Home Page")
        //         UIView.appearance().semanticContentAttribute = .forceLeftToRight
        
        let appearance = UINavigationBarAppearance()
        //        appearance.configureWithTransparentBackground()
        appearance.backgroundColor = UIColor.white
        //        appearance.shadowColor = .clear
        
        
        let font = UIFont(name: "Regular_Font".localized(), size: 17)
        
        if let font = font {
            appearance.titleTextAttributes = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.black]
        }
        
        
        UINavigationBar.appearance().standardAppearance = appearance
        
        //        let navigationBar = navigationController?.navigationBar
        //        let navigationBarAppearence = UINavigationBarAppearance()
        //        navigationBarAppearence.shadowColor = .clear
        //        navigationBar?.scrollEdgeAppearance = navigationBarAppearence
        //        /        appearance.configureWithTransparentBackground()
        
        //               let attrs: [NSAttributedString.Key: Any] = [
        //                    .foregroundColor: UIColor.black,
        //                    .font: UIFont.monospacedSystemFont(ofSize: 10, weight: .black)]
        //                             appearance.titleTextAttributes = attrs
        
        
        
        
        
        //        let attrs: [NSAttributedString.Key: Any] = [
        //            .foregroundColor: UIColor.black,
        //            .font: UIFont.monospacedSystemFont(ofSize: 10, weight: .black)]
        
        //            appearance.titleTextAttributes = attrs
        
        //        UINavigationBar.appearance().standardAppearance = appearance
        //          UINavigationBar.appearance().titleTextAttributes = [.font: UIFont.init(name: "ExtraBold_Font".localized(), size: 100)]
        
        //
        //        let appearance = UINavigationBarAppearance()
        //        appearance.configureWithOpaqueBackground()
        //        appearance.backgroundColor = .red
        //
        //        let attrs: [NSAttributedString.Key: Any] = [
        //            .foregroundColor: UIColor.white,
        //            .font: UIFont.monospacedSystemFont(ofSize: 36, weight: .black)
        //        ]
        //
        //        appearance.largeTitleTextAttributes = attrs
        //
        //        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        //        UINavigationBar.appearance().backgroundColor = UIColor(named: "ManBackColor")
        //        UINavigationBar.appearance().titleTextAttributes = [.font: UIFont.init(name: "ExtraBold_Font".localized(), size: 20)]
    }
    
    var body: some View {
        
        
        GeometryReader { geometry in
            
            
            
            
            
            
            ZStack{
                
                
                
                
                Group{
                    //                MapView(FromSelection: .constant(false), centerCoordinates: self.$myLocation).hidden()
                    
                    NavigationLink(destination: Settings()
                                   //                                    .navigationBarTitle("").navigationBarHidden(true)
                                   , isActive: self.$menuOpen){
                        EmptyView()
                    }
                    NavigationLink(destination: UpdatePage()
                                   //                                    .navigationBarTitle("").navigationBarHidden(true)
                                   , isActive: self.$goToUpdate){
                        EmptyView()
                    }.onReceive(self.timerO) { _ in
                        self.goToUpdate = true
                    }
                    
                    
                    
                }
                Group{
                    
                    //                    NavigationLink(destination:ServiceProviders().environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                    //                    )).environmentObject(self.settings).environmentObject(self.vehicleItems).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                    //                        )),isActive: self.$openSPL){
                    //
                    //                                                         EmptyView()
                    //                                             }
                    
                    
                    NavigationLink(destination: SignUpSelection(FromDA :.constant(false))
                                   //                                    .navigationBarTitle(,width"").navigationBarHidden(true)
                                   , isActive: self.$goToSplash){
                        Text("").frame(height:0)
                    }
                    
                    
                    
                    NavigationLink(destination: LangSelection()
                                   //                                    .navigationBarTitle("").navigationBarHidden(true)
                                   , isActive: self.$OLanguageSelPage){
                        Text("").frame(height:0)
                    }
                    
                    NavigationLink(destination: SignUpSelection(FromDA: .constant(false))
                                   //                                    .navigationBarTitle("").navigationBarHidden(true)
                                   , isActive: self.$OpenLoginPage){
                        Text("").frame(height:0)
                    }
                    NavigationLink(destination: QRScanner(isShowingScanner: self.$isShowingScanner,ALERT2:self.$ALERT2,AlertMessege2:self.$AlertMessege2, GoToBookDetails: self.$GoToBookDetails, SelectedDriver: self.$Driver, ETA: self.$ETA, OpenCart: self.$OpenCart, Snap: self.$Snap)
                                    //
                                    .environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                                    ))
                                   //
                                   , isActive: self.$isShowingScanner)
                    {
                        
                        Text("").frame(height:0)
                    }
                    
                    
                    
                    NavigationLink(destination: Cart(GoToBookDetailsOld: .constant(false))
                                    //                                    .navigationBarTitle("").navigationBarHidden(true)
                                    .environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                                    )), isActive: self.$OpenCart){
                        
                        Text("").frame(height:0)
                    }
                    
                    
                    
                    NavigationLink(destination: MyBookings()
                                    //                                    .navigationBarTitle("").navigationBarHidden(true)
                                    .environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                                    ))
                                   , isActive: self.$OBookings){
                        
                        Text("").frame(height:0)
                    }
                    
                    
                    NavigationLink(destination: Offers()
                                    //                                    .navigationBarTitle("").navigationBarHidden(true)
                                    .environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                                    ))
                                   , isActive: self.$OOffers
                    ){
                        
                        Text("").frame(height:0)
                    }
                    
                    
                    NavigationLink(destination: MyProfile()
                                    //                                    .navigationBarTitle("").navigationBarHidden(true)
                                    .environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                                    ))
                                   , isActive: self.$OProfile){
                        
                        Text("").frame(height:0)
                    }
                    //  Spacer()
                }
                
                
                
                TabView{
                    
                    
                    
                    MainPage(VEHICLES:self.$VEHICLES,YouHave1:self.$YouHave1,YouHave2:self.$YouHave2,
                             Vibrate:self.$Vibrate,
                             page:self.$page,
                             DisplayActive:self.$DisplayActive,
                             RecentAddress:self.$RecentAddress,
                             ALERT2:self.$ALERT2,
                             AlertMessege2:self.$AlertMessege2,
                             OpenLoginPage:self.$OpenLoginPage,
                             goToSplash:self.$goToSplash,
                             OProfile:self.$OProfile,
                             ONotification:self.$ONotification,
                             OBookings:self.$OBookings,
                             openSPD:self.$openSPD,
                             OHelpCenter:self.$OHelpCenter,
                             OAboutUS:self.$OAboutUS,
                             OOffers:self.$OOffers,
                             goToTrack:self.$goToTrack,
                             OpenPartialSheet:self.$OpenPartialSheet,
                             openVA:self.$openVA,
                             ViewLoader:self.$ViewLoader,
                             PastOrders:self.$PastOrders,
                             GoToBookDetails:self.$GoToBookDetails,
                             GTB:self.$GTB,
                             ETA:self.$ETA,
                             OpenCart:self.$OpenCart,
                             Snap:self.$Snap,
                             isShowingScanner:self.$isShowingScanner,
                             openSP:self.$openSP,
                             openSPL:self.$openSPL,
                             ALERT:self.$ALERT,
                             StreetAddress:self.$StreetAddress,
                             menuOpen:self.$menuOpen,
                             goToMap:self.$goToMap,
                             OLanguageSelPage:self.$OLanguageSelPage,
                             
                             ActiveOrderID:self.$ActiveOrderID,
                             OpenReorderSheet:self.$OpenReorderSheet,
                             goToMap2:self.$goToMap2,
                             item:self.$item,
                             Output:self.$Output,
                             selectedOrderID:self.$selectedOrderID,
                             Driver:self.$Driver,
                             SuccessETA:self.$SuccessETA,
                             x:self.$x,
                             count:self.$count,
                             screen:self.$screen,
                             op:self.$op,
                             placedisplay:self.$placedisplay,
                             cartsNumber:self.$cartsNumber,
                             addressDatas:self.$addressDatas)
                        
                        
                        .tabItem ({
                            Image("Group 776").renderingMode(.template)
                            Text("Home")
                        }).tag(0)
                    
                    
                    QRScanner(isShowingScanner: self.$isShowingScanner,ALERT2:self.$ALERT2,AlertMessege2:self.$AlertMessege2, GoToBookDetails: self.$GoToBookDetails, SelectedDriver: self.$Driver, ETA: self.$ETA, OpenCart: self.$OpenCart, Snap: self.$Snap)
                        .tabItem ({
                            
                            Image("qr-code2").renderingMode(.template).resizable().frame(width:30,height:30)
                            
                            Text("Scan QR Code")
                        }).tag(1)
                    
                    
                    Offers()
                        
                        .tabItem ({
                            Image("Group 800").renderingMode(.template)
                            Text("Offers")
                        }).tag(2)
                    
                    Settings()
                        
                        .tabItem ({
                            Image("settings").renderingMode(.template)
                            Text("Settings")
                        }).tag(3)
                    
                }   .accentColor(Color("c2"))  .frame(width:geometry.size.width, height:geometry.size.height).blur(radius: self.openSP ? 10:0).brightness(self.openSP ?  -0.5:0)
                //                    .edgesIgnoringSafeArea(.all)
                
                
                
                //                HStack{
                //                                    //                    if(self.menuOpen){
                //                                    SideMenu(goToSplash: self.$goToSplash, OProfile: self.$OProfile, ONotification: self.$ONotification, OpenCart: self.$OpenCart, OBookings: self.$OBookings, isShowingScanner: self.$isShowingScanner, OHelpCenter: self.$OHelpCenter, OAboutUS: self.$OAboutUS,OOffers : self.$OOffers, OLanguageSelPage: self.$OLanguageSelPage).onTapGesture{
                //                                        withAnimation{
                //                                            self.menuOpen.toggle()
                //
                //                                        }
                //                                    }
                //                                        //                            .frame(width:
                //                                        //    self.menuOpen ? geometry.size.width*0.8 : 0
                //                                        //                        ,
                //                                        //                           height: geometry.size.height)
                //                                        //
                //                                        .offset(x: self.menuOpen ? 0 : -geometry.size.width).frame(width: geometry.size.width*0.9)
                //                                    //                        .animation(.default)
                //                                    //  .frame(maxWidth:geometry.size.width*0.8)
                //                                    //   .transition(.move(edge: .leading))
                //                                    //  .offset(x:  0)
                //                                    //.disabled(self.menuOpen ? true : false)
                //                                    ////
                //
                //
                //
                //                                    //                                .gesture(
                //                                    //                                    DragGesture(minimumDistance: 3)
                //                                    //                                        .onEnded { _ in
                //                                    //                                            withAnimation{
                //                                    //                                                self.menuOpen.toggle()
                //                                    //
                //                                    //                                            }
                //                                    //                                    }
                //                                    //                            )
                //                                    //                }
                //                                    Spacer()
                //
                //                                }
                
                
                
                
                if(self.ViewLoader){
                    Loader()
                }
                
                
                
                if(self.ALERT2){
                    
                    
                    ZStack{
                        
                        VStack{
                            
                            //                                                                                      HStack{
                            //                                                                                          Text("Your Location Changed !").font(.custom("Bold_Font".localized(), size: 17))
                            //                                                                                              .foregroundColor(Color("darkthemeletter"))
                            //
                            //
                            //                                                                                      }
                            
                            
                            HStack{
                                Text("Unknown QR Code").font(.custom("Regular_Font".localized(), size: 16)).padding()
                                    .foregroundColor(Color("darkthemeletter"))
                                
                                
                            }
                            
                            //                                                                                      HStack{
                            //                                                                                          Text(getTime(timeIntervel: self.selectedServiceProvider.TimeToArrive)).font(.custom("ExtraBold_Font".localized(), size: 16))
                            //                                                                                                                                                                 .foregroundColor(Color("darkthemeletter"))
                            //
                            //
                            //                                                                                                                                                                            }
                            
                            
                            
                            
                            HStack{
                                
                                Spacer()
                                
                                
                                Button(action: {
                                    
                                    self.ALERT2 = false
                                }){
                                    
                                    Text("OK").font(.custom("ExtraBold_Font".localized(), size: 17))
                                        .padding(.trailing).padding(.top,20)
                                }
                                
                            }
                            
                            //                                            if(self.showAlert){
                            //
                            //
                            //                                                                          Text(self.errorStatus).font(.custom("Regular_Font".localized(), size: 14)).foregroundColor(Color.red).padding()
                            //
                            //                                                                                                    }
                        }.frame(width :320 , height: 150)
                        
                        .padding()
                        
                    }.background(Color.white).clipped().shadow(radius: 2)
                    
                    //
                    //                    HStack(alignment:.center){
                    //                        VStack(alignment:.center){
                    //
                    //
                    //
                    //                            Text(self.AlertMessege2).foregroundColor(Color("darkthemeletter")).font(.custom("Regular_Font".localized(), size: 17)).multilineTextAlignment(.center).padding()
                    //
                    //
                    //
                    //
                    //
                    //                        }.frame(width:280,height:250).padding().background(Color("backcolor2"))
                    //                        VStack{
                    //
                    //                            Button(action:{
                    //
                    //
                    //                                self.ALERT2 = false
                    //
                    //                            }){
                    //                                Image("close_1").resizable().frame().frame(width: 20, height: 20)
                    //
                    //                            }
                    //                            Spacer()
                    //                        }
                    //
                    //
                    //                    }.onTapGesture {
                    //
                    //                        self.ALERT2 = false
                    //
                    //                    }.frame(width:300,height:300)
                }
                
                
                VStack{
                    EmptyView()
                }.sheet(isPresented:self.$goToTrack ){
                    TrackServiceProvider(openTrack:self.$goToTrack,OrderID :self.selectedOrderID,goToHome:.constant(false)).environment(\.colorScheme, .light)
                }
                
                VStack{
                    EmptyView()
                }.sheet(isPresented: self.$GoToBookDetails){
                    
                    
                    
                    BookingDetails(OpenCart: self.$OpenCart,GoToBookDetails:self.$GoToBookDetails ,serviceSnap: self.Snap, SelectedDriver:
                                    self.Driver,ETA: self.ETA, openVA: self.$openVA ).environmentObject(self.settings).environmentObject(self.vehicleItems).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                                    ))
                    
                    
                    
                }
                
                VStack{
                    
                    Spacer()
                    VStack{
                        //                        Spacer()
                        HStack{
                            Spacer()
                            Button(action:{
                                withAnimation{
                                    self.OpenPartialSheet = false
                                }
                            }){
                                Text("Close").font(.custom("Regular_Font".localized(), size: 14)).padding()
                            }
                        }
                        //                         Spacer().frame(height:50)
                        VStack{
                            HStack{
                                Text("Recent Address").font(.custom("ExtraBold_Font".localized(), size: 16)).foregroundColor(Color("darkthemeletter")).padding()
                                Spacer()
                            }.padding(.top,30)
                            //                        List{
                            
                            //                         Divider()
                            ForEach(self.addressDatas.reversed().prefix(3),id: \.self){ order in
                                
                                VStack{
                                    Button(action:{
                                        
                                        
                                        //                                    UserDefaults.standard.set(Double(order.latitude), forKey: "latitude")
                                        //
                                        //
                                        //                                    UserDefaults.standard.set(Double(order.longitude), forKey: "longitude")
                                        //
                                        //
                                        //
                                        //
                                        //
                                        //
                                        //
                                        //                                    UserDefaults.standard.set("", forKey: "HOME")
                                        //
                                        //                                    UserDefaults.standard.set("", forKey: "STEET")
                                        //
                                        //                                    UserDefaults.standard.set("", forKey: "BUILDING")
                                        //
                                        //                                    UserDefaults.standard.set(order.ServiceLocation, forKey: "PLACE")
                                        
                                        
                                        
                                        UserDefaults.standard.set(order.lat, forKey: "latitude")
                                        
                                        
                                        UserDefaults.standard.set(order.lng, forKey: "longitude")
                                        
                                        
                                        
                                        
                                        //                                                    let str4 = String( (places.first?.postalCode) ?? ""
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        UserDefaults.standard.set(order.house, forKey: "HOME")
                                        
                                        UserDefaults.standard.set(order.streetName, forKey: "STREET")
                                        
                                        UserDefaults.standard.set(order.buildingName, forKey: "BUILDING")
                                        
                                        UserDefaults.standard.set(order.address, forKey: "PLACE")
                                        
                                        UserDefaults.standard.set(order.house,forKey: "AHOME")
                                        UserDefaults.standard.set(order.streetName,forKey: "ASTREET")
                                        UserDefaults.standard.set(order.floor,forKey: "AFLOOR")
                                        UserDefaults.standard.set(order.office,forKey: "AOFFICE")
                                        UserDefaults.standard.set(order.notes,forKey: "ADNOTE")
                                        UserDefaults.standard.set(order.addressNickName,forKey: "ANICK")
                                        
                                        
                                        
                                        
                                        //                                    withAnimation{
                                        self.OpenPartialSheet = false
                                        //                                    }
                                        
                                    }){
                                        HStack{
                                            Image("placeholder").renderingMode(.template).resizable().frame(width:20,height: 20).padding().padding(.leading,25).foregroundColor(Color.black)
                                            
                                            Text( "\(order.addressType) - \(order.addressNickName) ").font(.custom("Regular_Font".localized(), size: 14))
                                                .foregroundColor(Color("MainFontColor1")).padding(.trailing,20).lineLimit(1)
                                            Spacer()
                                            
                                        }
                                    }
                                    Divider()
                                    
                                }
                            }
                            
                            //                        }.listRowInsets(EdgeInsets())
                            //                        Divider()
                            
                            
                            
                            
                            
                            Group{
                                Button(action:{
                                    //                                              UserDefaults.standard.set(nil, forKey: "PLACE")
                                    UserDefaults.standard.set(Double(UserDefaults.standard.double(forKey: "CLLOCATION_LAT") ), forKey: "latitude")
                                    
                                    
                                    UserDefaults.standard.set(Double(UserDefaults.standard.double(forKey: "CLLOCATION_LON") ), forKey: "longitude")
                                    
                                    
                                    UserDefaults.standard.set(UserDefaults.standard.string(forKey: "CLLOCATION_NAME"), forKey: "PLACE")
                                    self.OpenPartialSheet = false
                                    
                                    //                                                                           DispatchQueue.global(qos: .background).async {
                                    DispatchQueue.main.async {
                                        
                                        
                                        
                                        let geocoder = GMSGeocoder()
                                        
                                        geocoder.reverseGeocodeCoordinate(self.locationManager.location?.coordinate ?? CLLocationCoordinate2D(latitude: 0, longitude: 0)) { response, error in
                                            //
                                            if error != nil {
                                                print("reverse geodcode fail: \(error)")
                                            } else {
                                                if let places = response?.results() {
                                                    let places : [GMSAddress] = places
                                                    
                                                    
                                                    print("ooooooooo","3")
                                                    
                                                    let str1 = String((places.first?.thoroughfare) ?? "")
                                                    
                                                    print("str1",str1)
                                                    
                                                    let str2 = String( (places.first?.subLocality) ?? ""
                                                                       
                                                                       
                                                    )
                                                    print("str2",places.first?.lines)
                                                    let str3 = String( (places.first?.locality) ?? ""
                                                    )
                                                    
                                                    print("str3",str3)
                                                    
                                                    //                                                    let str4 = String( (places.first?.postalCode) ?? ""
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    let str5 = String( (places.first?.administrativeArea) ?? ""
                                                    )
                                                    
                                                    print("str5",str5)
                                                    
                                                    
                                                    //                                                    let str6 = String( (places.first?.locality) ?? ""
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    let str7 = String( (places.first?.country) ?? "")
                                                    print("str7",str7)
                                                    
                                                    UserDefaults.standard.set(str1, forKey: "HOME")
                                                    
                                                    UserDefaults.standard.set(str1+" , "+str2+" , "+str3, forKey: "STREET")
                                                    
                                                    UserDefaults.standard.set(str1, forKey: "BUILDING")
                                                    
                                                    
                                                    UserDefaults.standard.set("",forKey: "AHOME")
                                                    UserDefaults.standard.set(str1+" , "+str2+" , "+str3,forKey: "ASTREET")
                                                    UserDefaults.standard.set("",forKey: "AFLOOR")
                                                    UserDefaults.standard.set("",forKey: "AOFFICE")
                                                    UserDefaults.standard.set("",forKey: "ADNOTE")
                                                    UserDefaults.standard.set("",forKey: "ANICK")
                                                    
                                                    
                                                    
                                                    //
                                                    //                                                    if let place = places.first {
                                                    //
                                                    //
                                                    //                                                        if let lines = place.lines {
                                                    //                                                            print("GEOCODE: Formatted Address: \(lines)")
                                                    //
                                                    //
                                                    //                                                        }
                                                    //
                                                    //
                                                    //
                                                    //                                                    } else {
                                                    //                                                        print("GEOCODE: nil first in places")
                                                    //                                                    }
                                                } else {
                                                    print("GEOCODE: nil in places")
                                                }
                                            }
                                        }
                                        //
                                        //                                                                                     let georeader = CLGeocoder()
                                        //                                                                               georeader.reverseGeocodeLocation(self.locationManager.location!){ (places,err) in
                                        //
                                        //                                                                                         if err != nil{
                                        //
                                        //                                                                                             print((err?.localizedDescription)!)
                                        //                                                                                             return
                                        //                                                                                         }
                                        //
                                        //
                                        //                                               //                                            self.parent.centerLocation = places?.first?.subLocality as! String
                                        //                                                                                       //      return places?.first?.locality
                                        //                                                                                        //   print(places?.first?.subLocality!)
                                        //
                                        //
                                        //
                                        //
                                        //
                                        //
                                        //
                                        //                                                                                   UserDefaults.standard.set(Double((self.locationManager.location?.coordinate.latitude)!), forKey: "latitude")
                                        //
                                        //
                                        //                                                                                   UserDefaults.standard.set(Double((self.locationManager.location?.coordinate.longitude)!), forKey: "longitude")
                                        //
                                        //                                                                                           let str1 = String((places?.first?.name) ?? "")
                                        //
                                        //
                                        //
                                        //                                                                                           let str2 = String( (places?.first?.subThoroughfare) ?? ""
                                        //                                                                                               )
                                        //
                                        //                                                                                           let str3 = String( (places?.first?.thoroughfare) ?? ""
                                        //                                                                                                                                          )
                                        //
                                        //
                                        //
                                        //                                                                                           let str4 = String( (places?.first?.postalCode) ?? ""
                                        //                                                                                                                                          )
                                        //
                                        //
                                        //
                                        //
                                        //                                                                                           let str5 = String( (places?.first?.subLocality) ?? ""
                                        //                                                                                                                                          )
                                        //
                                        //
                                        //
                                        //
                                        //                                                                                           let str6 = String( (places?.first?.locality) ?? ""
                                        //                                                                                                                                          )
                                        //
                                        //
                                        //
                                        //
                                        //                                                                                           let str7 = String( (places?.first?.country) ?? "")
                                        //
                                        //
                                        //                                                                                           UserDefaults.standard.set(str2, forKey: "HOME")
                                        //
                                        //                                                                                           UserDefaults.standard.set(str5+" , "+str6+" , "+str4+" , "+str7, forKey: "STREET")
                                        //
                                        //                                                                                           UserDefaults.standard.set(str3, forKey: "BUILDING")
                                        //
                                        //                                                                                           UserDefaults.standard.set(str5+" , "+str6+" , "+str2+" , "+str3+" , "+str4+" , "+str7, forKey: "PLACE")
                                        //
                                        //                                                                                                            UserDefaults.standard.set("",forKey: "AHOME")
                                        //                                                                                                                                                     UserDefaults.standard.set(str5+" , "+str6+" , "+str4+" , "+str7,forKey: "ASTREET")
                                        //                                                                                                                                                      UserDefaults.standard.set("",forKey: "AFLOOR")
                                        //                                                                                                                                                   UserDefaults.standard.set("",forKey: "AOFFICE")
                                        //                                                                                                                                                   UserDefaults.standard.set("",forKey: "ADNOTE")
                                        //                                                                                                 UserDefaults.standard.set("",forKey: "ANICK")
                                        //
                                        //                                                                                     }
                                        
                                        
                                        //                                            self.placedisplay.toggle()
                                        //                                            self.placedisplay.toggle()
                                        
                                        
                                        
                                        
                                    }
                                }){
                                    HStack{
                                        Image("select").renderingMode(.template).resizable().frame(width:20,height: 20).padding().padding(.leading,25).foregroundColor(Color.black)
                                        VStack(alignment:.leading){
                                            Text("Deliver to current location").font(.custom("ExtraBold_Font".localized(), size: 14)).foregroundColor(Color("darkthemeletter"))
                                            Text((UserDefaults.standard.string(forKey: "CLLOCATION_NAME") ?? "")).font(.custom("Regular_Font".localized(), size: 13)).foregroundColor(Color("darkthemeletter"))
                                            
                                        }
                                        Spacer()
                                    }
                                }
                                
                                Divider()
                                
                                
                                
                                
                                Button(action:{
                                    self.goToMap = true
                                    self.OpenPartialSheet = false
                                }){
                                    HStack{
                                        Image("pin_new").renderingMode(.template).resizable().frame(width:10,height: 25).padding().padding(.leading,30).foregroundColor(Color.black)
                                        VStack{
                                            Text("Deliver to different location").font(.custom("ExtraBold_Font".localized(), size: 14)).foregroundColor(Color("darkthemeletter"))
                                            Text("Choose location on the map").font(.custom("Regular_Font".localized(), size: 13)).foregroundColor(Color("darkthemeletter"))
                                            
                                        }
                                        Spacer()
                                    }
                                }
                                
                                Divider()
                            }
                        }
                        Spacer()
                        Spacer().frame(height:50)
                        Spacer()
                    }.frame(width:geometry.size.width,height:600).background(RoundedCorners(color: .white, tl: 30, tr: 30, bl: 0, br: 0))
                    
                    
                } .offset(y: self.OpenPartialSheet ? 0 : geometry.size.height+350)
                .gesture(
                    DragGesture(minimumDistance: 50)
                        .onEnded { _ in
                            withAnimation{
                                self.OpenPartialSheet.toggle()
                                
                            }
                        }
                ).onAppear{
                    self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("saved_address").observeSingleEvent(of: DataEventType.value, with: { (datasnapshot) in
                        self.addressDatas.removeAll()
                        for ordersnap in datasnapshot.children{
                            let ordersnap : DataSnapshot = ordersnap as! DataSnapshot
                            //                    var addressD = AddressDataO(address: String(describing :  (ordersnap.childSnapshot(forPath:"address").value)!), addressId: "", addressNickName: "", addressType: "", buildingName: "", floor: "", house: "", lat: 0.0, lng: 0.0, notes: "", office: "", streetName: "")
                            let     addressD = AddressDataO(address: String(describing :  (ordersnap.childSnapshot(forPath:"address").value!)), addressId: String(describing :  (ordersnap.childSnapshot(forPath:"addressId").value!)), addressNickName: String(describing :  (ordersnap.childSnapshot(forPath:"addressNickName").value!)), addressType: String(describing :  (ordersnap.childSnapshot(forPath:"addressType").value!)), buildingName: String(describing :  (ordersnap.childSnapshot(forPath:"buildingName").value!)), floor: String(describing :  (ordersnap.childSnapshot(forPath:"floor").value!)), house: String(describing :  (ordersnap.childSnapshot(forPath:"house").value!)), lat: Double(String(describing:ordersnap.childSnapshot(forPath:"lat").value!)) ?? 0.0, lng: Double(String(describing:ordersnap.childSnapshot(forPath:"lng").value!)) ?? 0.0, notes: String(describing :  (ordersnap.childSnapshot(forPath:"notes").value!)), office: String(describing :  (ordersnap.childSnapshot(forPath:"office").value!)), streetName: String(describing :  (ordersnap.childSnapshot(forPath:"streetName").value!)))
                            
                            self.addressDatas.append(addressD)
                        }
                        
                    })
                    
                }
                
                
                Group{
                    
                    if(self.OpenReorderSheet){
                        VStack{
                            LocForReorder(OpenPartialSheet: self.$OpenReorderSheet, goToMap: self.$goToMap2, PastOrders: self.PastOrders, bookings: self.$item, Driver: self.$Driver, ETA: self.$ETA, SuccessETA: self.$SuccessETA,Output:self.$Output,LocationName: .constant(self.locationManager.locationName ?? ""),Location:.constant(self.locationManager.location ?? CLLocation(latitude: 0.0, longitude: 0.0) ))
                            
                            
                        }
                        
                    }
                    
                    if(self.Stage != 0){
                        RateAndReviews(OrderID: self.$selectedOrderID,  Stage: self.$Stage).frame(width: geometry.size.width, height: geometry.size.height)
                    }
                    
                }
                
                if(self.openSP){
                    
                    
                    
                    
                    VehicleSelection(openSPL: self.$openSPL, openSP: self.$openSP)
                    
                    //                            ZStack{
                    //                                ScrollView{
                    //                    //            NavigationLink(destination:ServiceProviders().environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                    //                    //                                                   )).environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                    //                    //                                                       )),isActive: self.$openSP){
                    //                    //
                    //                    //                                                           EmptyView()
                    //                    //                                               }
                    //
                    //                                    VStack{
                    //
                    ////                                        Spacer().frame(height:100)
                    //                            HStack{
                    //                                Spacer()
                    //                            VStack{
                    //                                  Spacer()
                    //                                HStack{
                    //                                    Spacer()
                    //
                    //                                    Button(action: {
                    //                                        self.openSP = false
                    //                                    }){
                    //                                    Image(systemName: "x.circle.fill").resizable().frame(width:25,height: 25).padding(.trailing,10).foregroundColor(Color.white)
                    //                                    }
                    //                                }
                    //
                    //                                Text("Good!, You’re Almost there Now Select your vehicle type").font(.custom("Bold_Font".localized(), size: 14)).foregroundColor(Color.white).multilineTextAlignment(.center).padding(.horizontal,70)
                    //
                    //                                Vehicles(VEHICLES: self.$VEHICLES).modifier(Shakes(animatableData: CGFloat(self.Vibrate)))
                    //
                    //
                    //                                    .animation(self.V ? Animation.default.repeatCount(3):nil)
                    //
                    //                                Button(action:{
                    //                                    if(self.VEHICLES.count>0){
                    //
                    //                    //                    UserDefaults.standard.set(self.VEHICLES, forKey: "VT")
                    //                    //                     var VArray : [[String:String]] =  [[String:String]]()
                    //                    //
                    //                    //                                        for V in self.VEHICLES{
                    //                    //                    //                        let Vname = ["name":V.name]
                    //                    //                    //                        let Vimage = ["image":V.image]
                    //                    //                                            let Vitem = ["name":V.name,"image":V.image]
                    //                    //                                            VArray.append(Vitem)
                    //                    //                                        }
                    //
                    //                    //                    var VData : [VehicleItems] = [VehicleItems]()
                    //                    //
                    //                    //
                    //                    //                    UserDefaults.standard.set(VData, forKey: "VT")
                    //                    //
                    //
                    //                    //                    self.vehicleItems.vehicleItem =  self.VEHICLES
                    //
                    //                                                self.openSPL = true
                    //                                                                                             self.openSP = false
                    //
                    //                    //                       self.timer.connect()
                    //                                          print("ppppp")
                    //
                    //                                        self.V = false
                    //
                    //
                    //                                    }else{
                    //                    //                      self.openSPL = true
                    //                                        self.V = true
                    //                                        self.Vibrate += 1
                    //                                    }
                    //                                }){
                    //                                HStack{
                    //                                    Text("View Service Providers").font(.custom("Bold_Font".localized(), size: 13)).foregroundColor(Color.white)
                    //
                    //                                    Image(systemName: "arrow.right".localized())
                    //                                              .foregroundColor(Color.white)
                    //                                       }.padding(.horizontal).padding(.vertical,20).frame(width:300).background(LinearGradient(gradient: Gradient(colors: [Color("g1"),Color("g2")]), startPoint: .top, endPoint: .bottom)).cornerRadius(20).clipped()
                    //
                    //
                    //
                    //                                }
                    ////                                .onReceive(self.timer) { _ in
                    ////                                                   print("ppppp")
                    ////                                                    self.openSPL = true
                    ////                                                                    self.openSP.toggle()
                    ////                                               }
                    //
                    //
                    //
                    //                                  Spacer()
                    //                                }
                    //                                  Spacer()
                    //                            }
                    //                                }
                    //                                }
                    //                            }.background(Color.clear).onAppear{
                    //                    //            self.openSPL = true
                    //                                self.VEHICLES.removeAll()
                    //                                  UserDefaults.standard.set(self.VEHICLES, forKey: "VTYPE")
                    //                            }.onDisappear{
                    ////                                self.vehicleItems.vehicleItem = AddVehicleData(VEHICLES: self.VEHICLES)
                    //
                    //                    }
                }
                
            }.frame(width: geometry.size.width, height: geometry.size.height)
            
            
            
            .onAppear(perform: {
                
                
                UserDefaults.standard.set(nil,forKey: "PLACE")
                
                
                //                self.instantiateTimer()
                print("Home Page3")
                //                   self.timer.connect()
                
                
                //
                //
                //                self.op = ((self.screen + 15) * CGFloat(self.serviceProviderFetch.Offers.count / 2)) - (self.serviceProviderFetch.Offers.count % 2 == 0 ? ((self.screen + 15) / 2) : 0)
                //                if(self.serviceProviderFetch.Offers.count>0){
                //
                //                    self.serviceProviderFetch.Offers[0].show = true
                //                }
                //
                //                //                       print("WWwwwwww",Auth.auth().currentUser?.email)
                //                //                    UserDefaults.standard.set("saloon", forKey: "Vtype")
                //                self.instantiateTimer() // You could also consider an optional self.timer variable.
                //                self.timer.connect() // This allows you to manually connect where you want with greater efficiency, if you don't always want to autostart.
                //
                //                //                    self.FetchPast()
                //                //                    self.FetchActiveOrders()
                //
                
            }).onDisappear(perform: {
                
                // So, I am just assuming you want to stop the timer when you navigate out.
                // But, you can also easily remove this closure.
                // Also, this may not run as you would intuit.
                //                self.cancelTimer()
            })
            //                .frame(width:geometry.size.width,height:geometry.size.height)
            //                    .navigationBarTitle("Heloo")
            //                                   .navigationBarHidden(true)
            
            
            
            
            //        }
            
            
        }
        .onAppear{
            
            
            
            self.ref.child("twaddan_admin").child("iOS_Update").observe(DataEventType.value, with: {
                (Msnapshot) in
                
                
                if(!(String(describing :  (Msnapshot.childSnapshot(forPath:"Update_Version").value)!) == Constants.appVersion)){
                    self.timerO.connect()
                    
                }
                
            })
            
            
            self.timer.connect()
            self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").observe(DataEventType.value, with: {
                
                (Msnapshot) in
                
                self.cartsNumber = 0
                
                
                
                
                
                for ID in Msnapshot.childSnapshot(forPath:"Vehicles").children {
                    let id = ID as! DataSnapshot
                    
                    
                    self.cartsNumber += 1
                    
                }
            })
            
            
            
            
            //            appearance.backgroundColor = .red
            print("QQQQQQ",  UserDefaults.standard.string( forKey: "Aid") ?? "")
            
            if(Auth.auth().currentUser == nil){
                self.ViewLoader = true
                let uuid = UIDevice.current.identifierForVendor?.uuidString
                Auth.auth().signInAnonymously() { (authResult, error) in
                    // ...
                    
                    self.ViewLoader = false
                    
                    guard let user = authResult?.user else {
                        print(error)
                        return }
                    
                    
                    let isAnonymous = user.isAnonymous  // true
                    let uid = user.uid
                    
                    
                    UserDefaults.standard.set( uuid ?? uid, forKey: "Aid")
                    self.FetchPast()
                    self.FetchActiveOrders()
                    //                                                                                                 UserDefaults.standard.string(forKey: "Aid") ?? "0" = uuid ?? uid
                    //                                                                                                    UserDefaults.standard.set(UserDefaults.standard.string(forKey: "Aid") ?? "0", forKey: "Aid")
                    
                    
                    self.ViewLoader = false
                    
                    //                                if(error == nil){
                    //                                    self.OM.toggle()
                    //                                }
                    //
                    
                }
            }else{
                self.FetchPast()
                self.FetchActiveOrders()
                
            }
            
            
            
            
        }
        //            .gesture(
        //                                        DragGesture(minimumDistance: 20)
        //                                            .onEnded { _ in
        //                                                withAnimation{
        //                                                    self.menuOpen.toggle()
        //
        //                                                }
        //                                        }
        //                         k4       )
        //            .edgesIgnoringSafeArea(.all)
        
        .navigationBarTitle("Home",displayMode: .inline)
        
        
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading:
                                //                Button(action:{
                                //                withAnimation{
                                //                    self.menuOpen.toggle()
                                //                }
                                //              }){
                                
                                Button(action:{
                                    self.menuOpen.toggle()
                                }){
                                    HStack{ Image("Group 795").renderingMode(.template).foregroundColor(.black)
                                        
                                        Spacer()
                                        
                                        
                                    }.frame(width:50,height:50)
                                }
                            
                            //              }
                            ,trailing:
                                //                Button(action:{
                                //                           withAnimation{
                                //                                 self.OpenCart = true
                                //                               recalculateETA()
                                //                           }
                                //                         }){
                                
                                ZStack{
                                    Image("cart").resizable().renderingMode(.template).foregroundColor(.black).frame(width:25,height:25)
                                    //
                                    if(self.cartsNumber>0){
                                        Text(String(self.cartsNumber)).font(.custom("Regular_Font".localized(), size: 10))
                                            .frame(width: 15.0, height: 15.0)
                                            .background(Circle().fill(Color.red))
                                            .foregroundColor(.white)
                                            .padding([.trailing, .bottom])
                                    }
                                    //                            }
                                    
                                    
                                }.onTapGesture {
                                    withAnimation{
                                        
                                        recalculateETA()
                                        self.OpenCart = true
                                    }
                                }
                            
        )
        //            .edgesIgnoringSafeArea(.top)
        
        //
        //        GeometryReader { geometry in
        //        VStack{
        //            Text("Hello")
        //
        //            NavigationView{
        //                NavigationLink(destination: LoginPage(), isActive: self.$openSP) {
        //                    Text("Hello")
        //                }.background(Color.blue)
        //            }
        //        }.frame(width:geometry.size.width, height:geometry.size.height).background(Color.red)
        //
        //        }.edgesIgnoringSafeArea(.all)
    }
    struct Shakes: GeometryEffect {
        var amount: CGFloat = 10
        var shakesPerUnit = 3
        var animatableData: CGFloat
        
        func effectValue(size: CGSize) -> ProjectionTransform {
            ProjectionTransform(CGAffineTransform(translationX:
                                                    amount * sin(animatableData * .pi * CGFloat(shakesPerUnit)),
                                                  y: 0))
        }
    }
    
    //    func updateHeight(value : Int){
    //
    //
    //        for i in 0..<self.serviceProviderFetch.Offers.count{
    //
    //            self.serviceProviderFetch.Offers[i].show = false
    //           }
    //
    //        self.serviceProviderFetch.Offers[value].show = true
    //       }
    
    
    
    //    struct PageView: View {
    //
    //        @ObservedObject var serviceProviderFetch = FindOffer()
    //        var body: some View {
    //
    //
    //
    //            TabView {
    ////                ForEach(self.serviceProviderFetch.Offers,id:\.self){ i in
    //                  ForEach(0..<30) { i in
    //                               ZStack {
    //                                   Color.black
    //                                   Text("Row: \(i)").foregroundColor(.white)
    //                               }.clipShape(RoundedRectangle(cornerRadius: 10.0, style: .continuous))
    //                           }
    //                           .padding(.all, 10)
    //
    //
    //
    //
    //        } .frame(width: UIScreen.main.bounds.width, height: 200)
    ////             .tabViewStyle(PageTabViewStyle())
    //    }
    //    }
    
    
    //    func ReOrderToCart(item:pastOrderSet,DriverID:String,ETA:Double) {
    //
    //
    //        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").removeValue()
    //
    //        let node = String((Date().timeIntervalSince1970)).replacingOccurrences(of: ".", with: "")
    //
    //        for service in item.OrderSnap.childSnapshot(forPath: "services").children{
    //            let serviceSnap = service as! DataSnapshot
    //            let Vehicle = serviceSnap.key.components(separatedBy: ["&"])[0].trimmingCharacters(in: .whitespacesAndNewlines)
    //
    //
    //            self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("services").setValue(serviceSnap.childSnapshot(forPath: "services").value)
    //            self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("addons").setValue(serviceSnap.childSnapshot(forPath: "add_on_services").value)
    //
    //            self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("Vehicle").setValue(Vehicle)
    //
    //        }
    //
    //
    //        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider").setValue(item.SpName)
    //        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("DriverID").setValue(DriverID)
    //        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ETA").setValue(ETA)
    //        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider_id").setValue(item.snap.key)
    //        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider_image").setValue(item.snap.childSnapshot(forPath:"personal_information/image").value!)
    //
    //
    //
    //    }
    struct ETAAndDriver{
        
        var ETA:Double
        var DriverID :String
    }
    
    
    
    func FetchActiveOrders(){
        
        self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("active_orders").child("order_ids").observe(DataEventType.value, with: { Snaps in
            
            
            if(Snaps.childrenCount == 0){
                self.YouHave2 = true
            }
            self.ActiveOrderID.removeAll();
            
            for OrderID in Snaps.children {
                let orderids = OrderID as! DataSnapshot
                
                
                self.ActiveOrderID.append(String(describing :  (orderids.key )))
                
                if(self.selectedOrderID == "0"){
                    self.selectedOrderID = orderids.key
                }
            }
            
            
            
        })
        
    }
    
    func FetchPast()  {
        self.ref.child("Users").child(
            
            UserDefaults.standard.string(forKey: "Aid") ?? "0"
        ).child("past_orders").child("order_ids").queryLimited(toLast: 5).observe(DataEventType.value) { (datasnapshot) in
            
            self.PastOrders.removeAll()
            
            if(datasnapshot.childrenCount == 0){
                self.YouHave1 = true
            }
            
            
            
            for P in datasnapshot.children {
                let pastsnap = P as! DataSnapshot
                
                self.ref.child("orders").child("all_orders").child(pastsnap.key).observeSingleEvent(of: DataEventType.value) { (ordersnap) in
                    
                    self.ref.child("service_providers").child(String(describing :  (ordersnap.childSnapshot(forPath:"sp_id").value)!)).observeSingleEvent(of: DataEventType.value) { (spsnap) in
                        
                        
                        
                        self.ref.child("service_providers_ratings").child(String(describing :  (ordersnap.childSnapshot(forPath:"sp_id").value)!)).child(ordersnap.key).observeSingleEvent(of: DataEventType.value) { (rateSnap) in
                            let rateSnap : DataSnapshot = rateSnap as! DataSnapshot
                            
                            
                            var allservice: String = ""
                            
                            for S in ordersnap.childSnapshot(forPath: "services").children{
                                let Ssnap : DataSnapshot = S as! DataSnapshot
                                
                                
                                for Se in Ssnap.childSnapshot(forPath: "services").children{
                                    let SeSnap : DataSnapshot = Se as! DataSnapshot
                                    
                                    allservice = allservice + " , " + SeSnap.key
                                    
                                }
                                
                            }
                            
                            //
                            //                                    self.LastAddress = ["Address":String(describing :  (ordersnap.childSnapshot(forPath:"customer_raw_address").value)!)
                            //                                                  , "latitude": String(describing :  (ordersnap.childSnapshot(forPath:"customer_latitude").value)!),"longitude":String(describing :  (ordersnap.childSnapshot(forPath:"customer_longitude").value)!)]
                            
                            let milisecond:Int = Int(String(describing :  (ordersnap.childSnapshot(forPath:"time_order_placed").value)!)) ?? 0;
                            //                                                                               print("KKKKKK",milisecond)
                            let dateVar = Date.init(timeIntervalSince1970: TimeInterval(milisecond)/1000)
                            var dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "dd-MM-yyyy"
                            dateFormatter.locale = Locale(identifier: UserDefaults.standard.string(forKey: "LANSEL") ?? "en")
                            print(dateFormatter.string(from: dateVar))
                            
                            
                            
                            let pO : pastOrderSet = pastOrderSet(id: spsnap.key, driverID: String(describing :  (ordersnap.childSnapshot(forPath:"driver_id").value)!), SpName: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/name".localized()).value)!), Date: dateFormatter.string(from: dateVar), Image: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/image").value)!),snap: spsnap,Address:String(describing :  (ordersnap.childSnapshot(forPath:"customer_raw_address").value)!),latitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_latitude").value)!),longitude:String(describing :  (ordersnap.childSnapshot(forPath:"customer_longitude").value)!), OrderSnap: ordersnap )
                            
                            //                                    self.PastOrderSets.append(pO)
                            var ratings:Double = 0
                            
                            if(!rateSnap.exists()){
                                
                                ratings = -1
                            }else{
                                ratings =   Double(String(describing:rateSnap.childSnapshot(forPath:"rating").value!)) ?? 0.0
                                
                                
                            }
                            
                            let time:Double = Double(String(describing:ordersnap.childSnapshot(forPath:"time_order_placed").value!)) ?? 0.0
                            //                                                            print("QQQQQQQQQQQ",)
                            let bookings : Bookings = Bookings(Amount: String(describing :  (ordersnap.childSnapshot(forPath:"total_price_of_order").value)!), Status: "Completed", ServiceProvider: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/name".localized()).value)!), Services: allservice, ServiceLocation: String(describing :  (ordersnap.childSnapshot(forPath:"customer_raw_address").value)!), Image: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/image").value)!),PhoneNumber: String(describing :  (spsnap.childSnapshot(forPath:"personal_information/phone_number").value)!),OrderID: ordersnap.key,ServiceProviderID:spsnap.key,DriverID:String(describing :  (ordersnap.childSnapshot(forPath:"driver_id").value)!) ,ServiceSnap: spsnap, OrderSnap: ordersnap ,latitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_latitude").value)!),longitude: String(describing :  (ordersnap.childSnapshot(forPath:"customer_longitude").value)!),rating: ratings,time:time)
                            
                            self.PastOrders.append(bookings)
                            
                        }
                    }
                }
                
            }
            
        }
    }
    
    func handleScan(result: Result<String, CodeScan.ScanError>) {
        
        
        
        
        
        self.isShowingScanner = false
        self.ViewLoader = true
        // more code to come
        print(result)
        
        switch result {
        case .success(let code):
            //   let details = code.components(separatedBy: "\n")
            // guard details.count == 2 else { return }
            print("QQQQQQQQQQ")
            print(code)
            
            
            if(code != ""){
                self.ref.child("drivers").child(code).observeSingleEvent(of: DataEventType.value) { (driversnap) in
                    
                    self.ViewLoader = false
                    
                    if(driversnap.exists()){
                        
                        //        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5)) {
                        
                        let driverlat =
                            //10.797765
                            Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/latitude").value)!))
                        
                        let driverlon =
                            //76.635908
                            Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/longitude").value)!))
                        
                        var driverloc : CLLocation = CLLocation(latitude: driverlat!, longitude: driverlon!)
                        var driver2D : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: driverlat!, longitude: driverlon!)
                        
                        let userlat =
                            //                    10.874858
                            UserDefaults.standard.double(forKey: "latitude")
                        let userlon =
                            //                    76.449728
                            UserDefaults.standard.double(forKey: "longitude")
                        
                        
                        var userloc : CLLocation = CLLocation(latitude: userlat, longitude: userlon)
                        var user2D  : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: userlat, longitude: userlon)
                        
                        
                        //    var distance : CLLocationDistance = userloc.distance(from: driverloc)
                        // var timeOfArrival :
                        
                        //                        let request = MKDirections.Request()
                        //                 request.source = MKMapItem(placemark:  MKPlacemark(coordinate: driver2D))
                        //                 request.destination = MKMapItem(placemark:  MKPlacemark(coordinate: user2D))
                        //
                        //                 request.requestsAlternateRoutes = false
                        //
                        //                 let directions = MKDirections(request: request)
                        //    //                    directions.calculateETA { (responds, error) in
                        //    //                         if error != nil {
                        //    //                                            print("Error getting directions")
                        //    //                                        } else {
                        //    //                            var ETA  =    responds?.expectedTravelTime
                        //    //
                        //    //
                        //    //
                        //    //                        }
                        //    //                    }
                        //                        directions.calculateETA{ response, error in
                        //
                        //
                        //
                        //
                        //                   //     directions.calculate { response, error in
                        //                                var eta:Double = 10000000
                        //                            if(error != nil){
                        //                                print(error.debugDescription)
                        //                            }else{
                        //
                        //                                    eta = response?.expectedTravelTime as! Double
                        //
                        //
                        //    //                        let routes = response?.routes
                        //
                        //
                        //
                        //
                        //    //                        if(routes != nil){
                        //    //                        let selectedRoute = routes![0]
                        //    //                        let distance = selectedRoute.distance
                        //    //                         eta = selectedRoute.expectedTravelTime
                        //    //
                        //    //
                        //    //                        }
                        //                                print(eta)
                        //
                        //                            }
                        //
                        
                        
                        self.GoToBookDetails = true
                        var eta:Double = 10000000
                        
                        let origin = "\(driver2D.latitude),\(driver2D.longitude)"
                        let destination = "\(user2D.latitude),\(user2D.longitude)"
                        
                        
                        let S : String =  "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=\(origin)&destinations=\(destination)&mode=driving&key=AIzaSyCYQeItZ7SVEQ24Grjzc3zA1B_2DU_GfPU"
                        
                        let url = URL(string:S
                        )
                        //        else {
                        //        let error = NSError(domain: "LocalDomain", code: 0, userInfo: [NSLocalizedDescriptionKey: "Failed to create object URL"])
                        //        print("Error: \(error)")
                        //        return
                        //    }
                        let config = URLSessionConfiguration.default
                        let session = URLSession(configuration: config)
                        let task = session.dataTask(with: url!, completionHandler: {
                            (data, response, error) in
                            
                            print("11111111",driversnap.key)
                            
                            if error != nil {
                                print(error!.localizedDescription)
                            }
                            else {
                                
                                //  print(response)
                                do {
                                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                                        
                                        guard let routes = json["rows"] as? NSArray else {
                                            DispatchQueue.main.async {
                                            }
                                            return
                                        }
                                        if (routes.count > 0) {
                                            let overview_polyline = routes[0] as? NSDictionary
                                            //   let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                                            //  let points = dictPolyline?.object(forKey: "points") as? String
                                            
                                            DispatchQueue.main.async {
                                                //
                                                let legs = overview_polyline?["elements"] as! Array<Dictionary<String, AnyObject>>
                                                
                                                let distance = legs[0]["distance"] as? NSDictionary
                                                let distanceValue = distance?["value"] as? Int ?? 0
                                                let distanceDouleValue = distance?["value"] as? Double ?? 0.0
                                                let duration = legs[0]["duration"] as? NSDictionary
                                                let totalDurationInSeconds = duration?["value"] as? Int ?? 0
                                                let durationDouleValue = duration?["value"] as? Double ?? 0.0
                                                
                                                print (S)
                                                
                                                eta = durationDouleValue+300 ;
                                                
                                                
                                                if(eta < 301){
                                                    eta = 10000000
                                                }
                                                
                                                
                                                
                                                
                                                
                                                print("QQQQQQQQQQQQQQQQQ",eta)
                                                
                                                
                                                self.ref.child("service_providers").child(String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)).observeSingleEvent(of: DataEventType.value) { (snapshot) in
                                                    
                                                    
                                                    
                                                    
                                                    self.Driver = code
                                                    self.ETA = eta
                                                    
                                                    self.Snap = snapshot
                                                    
                                                    
                                                    self.GTB = true
                                                    
                                                    //                                    self.OProfile = true
                                                    print("QQQQQQQQQQQQQQQQQ",self.GoToBookDetails)
                                                    
                                                    //                            }
                                                }
                                                
                                                
                                                
                                                
                                            }
                                        }
                                        else {
                                            print(json)
                                            DispatchQueue.main.async {
                                                //   SVProgressHUD.dismiss()
                                            }
                                        }
                                    }
                                }
                                catch {
                                    print("error in JSONSerialization")
                                    DispatchQueue.main.async {
                                        //  SVProgressHUD.dismiss()
                                    }
                                }
                            }
                        })
                        task.resume()
                        
                    } else{
                        self.AlertMessege2 = "Unknown QR Code"
                        
                        self.ALERT2 = true
                        
                        
                        
                    }
                    
                    
                }
                
                
            }
            
            
            
        //            let person = Prospect()
        //            person.name = details[0]
        //            person.emailAddress = details[1]
        
        //            self.prospects.people.append(person)
        case .failure(let error):
            print("Scanning failed")
        }
        
        
        
    }
    
    
    
    //    func instantiateTimer() {
    //        self.timer = Timer.publish (every: 4, on: .main, in: .common)
    //        return
    //    }
    
    func cancelTimer() {
        self.timer.connect().cancel()
        return
    }
    
    
    
}



struct HomePage_Previews: PreviewProvider {
    static var previews: some View {
        HomePage()  .environmentObject(UserSettings())
    }
}



struct OrderAddress {
    
    var Address : String
    var latitude : Double
    var longitude : Double
}
struct TrackServiceStatus: View {
    
    @State var VS : Int = 1
    @State var AT : Int = 1
    @State var QS : Int = 1
    @State var EB : Int = 1
    @State var PA : Int = 1
    @Binding var goToTrack : Bool
    
    let ref = Database.database().reference()
    @EnvironmentObject var settings : UserSettings
    
    @State var Status : String = "0"
    @State var Works : String = ""
    @State var ServiceProviderName : String = ""
    @State var ServiceProviderAddress : String = ""
    @State var ServiceProviderImage : String = ""
    @State var ETA : Double = 0.0
    var OrderID : String
    var ViewTrackButton : Bool
    @State var Review: String = ""
    
    //    @State var reavelStroke : Bool = false
    //    @State var revealStroke : Bool = false
    
    @State var A : Bool = false
    @State var B : Bool = false
    @State var C : Bool = false
    @State var D : Bool = false
    @State var E : Bool = false
    
    @State var timer2 = Timer.publish(every: 4, on: .main, in: .common).autoconnect()
    
    @State var solidlength : CGFloat = 0.0
    @State var dottedlength : CGFloat = ((UIScreen.main.bounds.width-60)/4)
    @Binding var SOrderID : String
    @State var tick: Bool = false
    @State var tick2: Bool = false
    @State var tick3: Bool = false
    @State var tick4: Bool = false
    
    
    var body: some View {
        ZStack{
            VStack{
                
                
                ZStack(alignment:.top){
                    
                    HStack(alignment:.top){
                        VStack{
                            HStack{
                                
                                if(self.Status != "1" && self.Status != "0"){
                                    Rectangle().frame(width:(CGFloat(self.getInt(Status: self.Status)-2)*(UIScreen.main.bounds.width-75)/4),height:1).foregroundColor(Color("t2")).padding(.top,23)
                                    
                                    
                                    Rectangle().frame(width:(CGFloat(1)*(UIScreen.main.bounds.width-75)/4),height:1).foregroundColor(Color.clear).overlay(Line()
                                                                                                                                                            .stroke(style: StrokeStyle(lineWidth: 1, dash: [3,5]))
                                                                                                                                                            .frame(height: 1).foregroundColor(Color("t2"))).padding(.top,23)
                                }
                                
                            }
                            Spacer()
                        }
                        Spacer()
                    }.frame(height: 68).padding(.leading, 35)
                    
                    
                    HStack(alignment:.top){
                        
                        
                        ZStack{
                            VStack{
                                
                                ZStack{
                                    
                                    VStack{
                                        
                                        
                                        
                                        
                                        RotationAnimation(image:"tick")
                                            .padding(.all,10)
                                            .foregroundColor(Color("t2"))
                                            .background(Color("t1"))    .clipShape(Circle())
                                            .overlay(HStack{ if(self.Status == "1"){ Circle().stroke (style: StrokeStyle(lineWidth: 1, lineCap: .square, lineJoin: .round,  dash:  [3,5], dashPhase: 1)) }else{ Circle().stroke(lineWidth: 1)}}.foregroundColor(self.getInt(Status: self.Status) < 1 ? Color.clear:Color("t2")))
                                        
                                    }
                                    if(self.Status != "1"){
                                        Image("tick")  .renderingMode(.template).resizable().frame(width:15,height:15)
                                            
                                            
                                            .padding(.all,10)
                                            .foregroundColor(Color("t2"))
                                            .background(Color("t1"))    .clipShape(Circle())
                                            .overlay(HStack{ if(self.Status == "1"){ Circle().stroke (style: StrokeStyle(lineWidth: 1, lineCap: .square, lineJoin: .round,  dash:  [3,5], dashPhase: 1)) }else{ Circle().stroke(lineWidth: 1)}}.foregroundColor(self.getInt(Status: self.Status) < 1 ? Color.clear:Color("t2")))
                                    }
                                    
                                    
                                }
                                
                                //                                                   .overlay(
                                //
                                //
                                //            //
                                //
                                //
                                //
                                //                                                    Circle().trim(from:self.A ? 0:self.getDashPhase(status: self.Status, number: "1"),to :1)
                                //
                                //                                                        .stroke(style: StrokeStyle(lineWidth: self.getLineWidth(status: self.Status, number: "1"), lineCap: .square, lineJoin: .round,  dash: self.getDashPhase(status: self.Status, number: "1") == 0 ? [1,1] :  [3,5], dashPhase: 1))
                                //                                                                       //                           .frame(width: 300, height: 300)
                                //                                                        .rotationEffect(.degrees(90))
                                //                                                        .rotation3DEffect(.degrees(180), axis: (x: 1, y: 0, z: 0))
                                //                                                                                                      .animation(Animation.easeOut(duration:8).delay(0).repeatForever(autoreverses: false)
                                //                                                                                   )
                                //
                                //                                                                               .foregroundColor(Color("t2"))
                                //                                                                                  .onAppear
                                //                                                                                                      {
                                //                                                                                                          self.A.toggle()
                                //                                                                                                  }
                                //            //                                                .stroke(Color("h2"), lineWidth:    self.Status=="2"||self.Status=="3"||self.Status=="4"||self.Status=="5" ? 1 : 0 )
                                //
                                //
                                //
                                //                                                    )
                                Text("Confirm") .font(.custom("Regular_Font".localized(), size: 9))
                                    .foregroundColor(Color("t3"))
                                
                            }
                            
                        }
                        .padding(.leading,10)
                        
                        
                        Spacer()
                        VStack{
                            
                            ZStack{
                                
                                if(true){
                                    
                                    RotationAnimation(image:"surface_1")
                                        .padding(.all,10)
                                        .foregroundColor(Color("t2"))
                                        .background(Color("t1"))
                                        
                                        
                                        .clipShape(Circle())
                                        .overlay(HStack{ if(self.Status == "2"){ Circle().stroke (style: StrokeStyle(lineWidth: 1, lineCap: .square, lineJoin: .round,  dash:  [3,5], dashPhase: 1)) }else{ Circle().stroke(lineWidth: 1)}}.foregroundColor(self.getInt(Status: self.Status) < 2 ? Color.clear:Color("t2")))
                                }
                                if(self.Status != "2"){
                                    Image("surface_1")
                                        //                        .overlay(Image("surface_1").frame(width:13,height:10))
                                        .renderingMode(.template)
                                        .resizable()   .frame(width:15,height:8).padding(.all,15)
                                        .foregroundColor(Color("t2"))
                                        .background(Color("t1"))
                                        
                                        
                                        .clipShape(Circle())
                                        .overlay(HStack{ if(self.Status == "2"){ Circle().stroke (style: StrokeStyle(lineWidth: 1, lineCap: .square, lineJoin: .round,  dash:  [3,5], dashPhase: 1)) }else{ Circle().stroke(lineWidth: 1)}}.foregroundColor(self.getInt(Status: self.Status) < 2 ? Color.clear:Color("t2")))
                                }
                            }
                            //                                                                                                 .overlay(
                            //
                            //
                            //                                                          //
                            //
                            //
                            //
                            //                                                                                                       Circle().trim(from:self.B ? 0:self.getDashPhase(status: self.Status, number: "2"),to :1)
                            //
                            //                                                                                                                                                 .stroke(style: StrokeStyle(lineWidth: self.getLineWidth(status: self.Status, number: "2"), lineCap: .square, lineJoin: .round,  dash: self.getDashPhase(status: self.Status, number: "2") == 0 ? [1,1] :  [3,5], dashPhase: 1))
                            //                                                                                                                     //                           .frame(width: 300, height: 300)
                            //                                                                                                      .rotationEffect(.degrees(90))
                            //                                                                                                      .rotation3DEffect(.degrees(180), axis: (x: 1, y: 0, z: 0))
                            //                                                                                                                                                    .animation(Animation.easeOut(duration:8).delay(0).repeatForever(autoreverses: false)
                            //                                                                                                                                 )
                            //
                            //                                                                                                                             .foregroundColor(Color("h2"))
                            //                                                                                                                                .onAppear
                            //                                                                                                                                                    {
                            //                                                                                                                                                        self.B.toggle()
                            //                                                                                                                                                }
                            //                                                          //                                                .stroke(Color("h2"), lineWidth:    self.Status=="2"||self.Status=="3"||self.Status=="4"||self.Status=="5" ? 1 : 0 )
                            //
                            //
                            //
                            //                                                                                                  )
                            Text("Heading") .font(.custom("Regular_Font".localized(), size: 9))
                                .foregroundColor(Color("t3"))
                            
                        }
                        
                        Spacer()
                        
                        VStack{
                            
                            ZStack{
                                RotationAnimation(image:"Path")
                                    
                                    .padding(.all,10)
                                    
                                    .foregroundColor(Color("t2"))
                                    .background(Color("t1"))
                                    
                                    
                                    .clipShape(Circle())
                                    .overlay(HStack{ if(self.Status == "3"){ Circle().stroke (style: StrokeStyle(lineWidth: 1, lineCap: .square, lineJoin: .round,  dash:  [3,5], dashPhase: 1)) }else{ Circle().stroke(lineWidth: 1)}}.foregroundColor(self.getInt(Status: self.Status) < 3 ? Color.clear:Color("t2")))
                                
                                if(self.Status != "3"){
                                    Image("Path")  .renderingMode(.template).resizable()   .frame(width:15,height:15).padding(.all,10)
                                        
                                        .foregroundColor(Color("t2"))
                                        .background(Color("t1"))
                                        
                                        
                                        .clipShape(Circle())
                                        .overlay(HStack{ if(self.Status == "3"){ Circle().stroke (style: StrokeStyle(lineWidth: 1, lineCap: .square, lineJoin: .round,  dash:  [3,5], dashPhase: 1)) }else{ Circle().stroke(lineWidth: 1)}}.foregroundColor(self.getInt(Status: self.Status) < 3 ? Color.clear:Color("t2")))
                                }
                            }
                            //                                                                                                 .overlay(
                            //
                            //
                            //                                                          //
                            //
                            //
                            //
                            //                                                                                                       Circle().trim(from:self.C ? 0:self.getDashPhase(status: self.Status, number: "3"),to :1)
                            //
                            //                                                                                                                                                 .stroke(style: StrokeStyle(lineWidth: self.getLineWidth(status: self.Status, number: "3"), lineCap: .square, lineJoin: .round,  dash: self.getDashPhase(status: self.Status, number: "3") == 0 ? [1,1] :  [3,5], dashPhase: 1))                                                   //                           .frame(width: 300, height: 300)
                            //                                                                                                      .rotationEffect(.degrees(90))
                            //                                                                                                      .rotation3DEffect(.degrees(180), axis: (x: 1, y: 0, z: 0))
                            //                                                                                                                                                    .animation(Animation.easeOut(duration:8).delay(0).repeatForever(autoreverses: false)
                            //                                                                                                                                 )
                            //
                            //                                                                                                                             .foregroundColor(Color("h2"))
                            //                                                                                                                                .onAppear
                            //                                                                                                                                                    {
                            //                                                                                                                                                        self.C.toggle()
                            //                                                                                                                                                }
                            //                                                          //                                                .stroke(Color("h2"), lineWidth:    self.Status=="2"||self.Status=="3"||self.Status=="4"||self.Status=="5" ? 1 : 0 )
                            //
                            //
                            //
                            //                                                                                                  )
                            Text("Reached") .font(.custom("Regular_Font".localized(), size: 9))
                                .foregroundColor(Color("t3"))
                            
                        }
                        
                        Spacer()
                        
                        VStack{
                            
                            
                            ZStack{
                                
                                RotationAnimation(image:"prefix__start_1")
                                    .padding(.all,10)
                                    .foregroundColor(Color("t2"))
                                    .background(Color("t1"))
                                    
                                    
                                    .clipShape(Circle())
                                    .overlay(HStack{ if(self.Status == "4"){ Circle().stroke (style: StrokeStyle(lineWidth: 1, lineCap: .square, lineJoin: .round,  dash:  [3,5], dashPhase: 1)) }else{ Circle().stroke(lineWidth: 1)}}.foregroundColor(self.getInt(Status: self.Status) < 4 ? Color.clear:Color("t2")))
                                
                                if(self.Status != "4"){
                                    Image("prefix__start_1")  .renderingMode(.template).resizable()   .frame(width:15,height:15).padding(.all,10)
                                        .foregroundColor(Color("t2"))
                                        .background(Color("t1"))
                                        
                                        
                                        .clipShape(Circle())
                                        .overlay(HStack{ if(self.Status == "4"){ Circle().stroke (style: StrokeStyle(lineWidth: 1, lineCap: .square, lineJoin: .round,  dash:  [3,5], dashPhase: 1)) }else{ Circle().stroke(lineWidth: 1)}}.foregroundColor(self.getInt(Status: self.Status) < 4 ? Color.clear:Color("t2")))
                                    
                                }
                            }
                            //                                                                                                 .overlay(
                            //
                            //
                            //                                                          //
                            //
                            //
                            //
                            //                                                      Circle().trim(from:self.D ? 0:self.getDashPhase(status: self.Status, number: "4"),to :1)
                            //
                            //                                                                                                                                                 .stroke(style: StrokeStyle(lineWidth: self.getLineWidth(status: self.Status, number: "4"), lineCap: .square, lineJoin: .round,  dash: self.getDashPhase(status: self.Status, number: "4") == 0 ? [1,1] :  [3,5], dashPhase: 1))                                                //                           .frame(width: 300, height: 300)
                            //                                                                                                      .rotationEffect(.degrees(90))
                            //                                                                                                      .rotation3DEffect(.degrees(180), axis: (x: 1, y: 0, z: 0))
                            //                                                                                                                                                    .animation(Animation.easeOut(duration:8).delay(0).repeatForever(autoreverses: false)
                            //                                                                                                                                 )
                            //
                            //                                                                                                                             .foregroundColor(Color("h2"))
                            //                                                                                                                                .onAppear
                            //                                                                                                                                                    {
                            //                                                                                                                                                        self.D.toggle()
                            //                                                                                                                                                }
                            //                                                          //                                                .stroke(Color("h2"), lineWidth:    self.Status=="2"||self.Status=="3"||self.Status=="4"||self.Status=="5" ? 1 : 0 )
                            //
                            //
                            //
                            //                                                                                                  )
                            Text("Started") .font(.custom("Regular_Font".localized(), size: 9))
                                .foregroundColor(Color("t3"))
                            
                        }
                        
                        Spacer()
                        VStack{
                            
                            ZStack{
                                Image("flag")  .renderingMode(.template).resizable()   .rotationEffect(.degrees(0))
                                    
                                    .frame(width:15,height:15).padding(.all,10)
                                    .foregroundColor(Color("t2"))
                                    .background(Color("t1"))
                                    
                                    
                                    .clipShape(Circle())
                                    .overlay(HStack{ if(self.Status == "5"){ Circle().stroke (style: StrokeStyle(lineWidth: 1, lineCap: .square, lineJoin: .round,  dash:  [3,5], dashPhase: 1)) }else{ Circle().stroke(lineWidth: 1)}}.foregroundColor(self.getInt(Status: self.Status) < 5 ? Color.clear:Color("t2")))
                                //                                                                                                     .overlay(
                                //
                                //
                                //                                                              //
                                //
                                //
                                //
                                //                                                      Circle().trim(from:self.E ? 0:self.getDashPhase(status: self.Status, number: "5"),to :1)
                                //
                                //                                                                                                                                                     .stroke(style: StrokeStyle(lineWidth: self.getLineWidth(status: self.Status, number: "5"), lineCap: .square, lineJoin: .round,  dash: self.getDashPhase(status: self.Status, number: "5") == 0 ? [1,1] :  [3,5], dashPhase: 1))
                                //                                                                                                                         //                           .frame(width: 300, height: 300)
                                //                                                                                                          .rotationEffect(.degrees(90))
                                //                                                                                                          .rotation3DEffect(.degrees(180), axis: (x: 1, y: 0, z: 0))
                                //                                                                                                                                                        .animation(Animation.easeOut(duration:8).delay(0).repeatForever(autoreverses: false)
                                //                                                                                                                                     )
                                //
                                //                                                                                                                                 .foregroundColor(Color("h2"))
                                //                                                                                                                                    .onAppear
                                //                                                                                                                                                        {
                                //                                                                                                                                                            self.E.toggle()
                                //                                                                                                                                                    }
                                //                                                              //                                                .stroke(Color("h2"), lineWidth:    self.Status=="2"||self.Status=="3"||self.Status=="4"||self.Status=="5" ? 1 : 0 )
                                //
                                //
                                //
                                //                                                                                                      )
                                
                                
                                if(self.Status=="5"){
                                    
                                    AnimatedImage(
                                        //                                name:"tick_animation"
                                        url:
                                            URL(string: "https://firebasestorage.googleapis.com/v0/b/twaddan-87437.appspot.com/o/Assets%2Ftick_animation.gif?alt=media&token=7f9d6103-5323-4c5f-b57a-53bb299e638b")
                                    ).resizable().frame(width:45,height:45).padding(.top,-6).onAppear{
                                        self.timer2 = Timer.publish (every: 5, on: .current, in:
                                                                        .common).autoconnect()
                                    }
                                    .onReceive(self.timer2) { input in
                                        
                                        self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("active_orders").child("order_ids").child(self.OrderID).removeValue();
                                        self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("past_orders").child("order_ids").child(self.OrderID).setValue(self.OrderID);
                                        
                                        self.timer2.upstream.connect().cancel()
                                    }
                                }
                                
                            }
                            
                            
                            Text("Finished") .font(.custom("Regular_Font".localized(), size: 9))
                                .foregroundColor(Color("t3")).padding(.top,self.Status=="5" ? -5 : 0)
                            
                            
                        }
                        .padding(.trailing,10)
                        
                        
                        
                        
                    }.frame(height: 68)
                    
                    
                }
                .background(Color.white).padding(.top,20)
                Divider().frame(height:1).background(Color("h4"))
                
                HStack{
                    
                    //                Spacer()
                    //                            Image("basicbackimage")
                    AnimatedImage(url : URL (string: self.ServiceProviderImage))
                        .resizable()
                        .frame(width: 51, height: 51) .cornerRadius(5)
                        .padding(.horizontal,10)
                    
                    Spacer().frame(width:5)
                    VStack(alignment:.leading){
                        
                        Text(self.getStatusWord(status: self.Status)).font(.custom("ExtraBold_Font".localized(), size: 11))
                            .multilineTextAlignment(.leading)
                            .foregroundColor(Color("darkthemeletter")).fixedSize()
                        
                        //                                      Text("self.Works").font(.custom("Regular_Font".localized(), size: 12))
                        //                                          .foregroundColor(Color("darkthemeletter"))
                        HStack{
                            Text(self.ServiceProviderName).font(.custom("Regular_Font".localized(), size: 11)).multilineTextAlignment(.leading).padding(.top,5)
                                
                                .foregroundColor(Color("t3"))
                            
                            Spacer()
                        }.frame(width:UIScreen.main.bounds.width*0.45,height:20)
                        //                                      Text("self.ServiceProviderAddress").font(.custom("Regular_Font".localized(), size: 12))
                        //                                          .foregroundColor(Color("darkthemeletter"))
                        
                        //                                                  HStack{ Text("Expected Time of Arrival").font(.custom("Regular_Font".localized(), size: 12))
                        //                                                      .foregroundColor(Color("darkthemeletter"))
                        HStack{
                            Text((self.ETA-(Date().timeIntervalSince1970)*1000) > 180000 ? getTime(timeIntervel: (self.ETA/1000)-(Date().timeIntervalSince1970)) : getTime(timeIntervel: 180) ).font(.custom("Regular_Font".localized(), size: 11))
                                .foregroundColor(Color("darkthemeletter"))
                        }
                        
                        //
                    }.frame(width:UIScreen.main.bounds.width*0.50)
                    
                    Spacer()
                    
                    
                    if(self.ViewTrackButton){
                        Button(action:{
                            self.SOrderID = self.OrderID
                            
                            
                            self.goToTrack = true
                            
                            
                            
                        }
                        ){
                            
                            Text("TRACK").font(.custom("Regular_Font".localized(), size: 12))
                                .foregroundColor(Color("h2"))
                                .padding(.horizontal).padding(.vertical,3)
                                // .padding(.horizontal,40)
                                //                                                                                   .frame(width : 100, height: 24)
                                
                                .background(Color.white)
                                
                                .cornerRadius(13)
                                .overlay(
                                    RoundedRectangle(cornerRadius: 30).stroke(Color("t4"),lineWidth: 1)
                                    
                                )
                            
                        }
                        
                    }
                    
                    Spacer()
                }
                
                
                
                //                              Divider()
                
                
                // OP
                
                
                
                
                //                if(self.Status == "5"){
                //
                //
                //                                              //Value of Service
                //                                              VStack{
                //
                //                                                  VStack{
                //                                                      HStack{
                //                                                          Text("Value of Service").font(.custom("Bold_Font".localized(), size: 16)).padding()
                //                                                          Spacer()
                //                                                      }
                //
                //                                                      Rating(Rate:self.$VS)
                //
                //
                //                                                      HStack{
                //                                                          Text("Arrival Time").font(.custom("Bold_Font".localized(), size: 16)).padding()
                //                                                          Spacer()
                //                                                      }
                //
                //                                                      Rating(Rate:self.$AT)
                //
                //
                //                                                      HStack{
                //                                                          Text("Quality of Service").font(.custom("Bold_Font".localized(), size: 16)).padding()
                //                                                          Spacer()
                //                                                      }
                //
                //                                                      Rating(Rate:self.$QS)
                //
                //
                //                                                      //                    HStack{
                //                                                      //                                           Text("Employee Behaviour").font(.custom("ExtraBold_Font".localized(), size: 16)).padding()
                //                                                      //                                           Spacer()
                //                                                      //                                       }
                //                                                      //
                //                                                      //                                       Rating(Rate:self.$EB)
                //
                //
                //
                //                                                      //                    HStack{
                //                                                      //                                           Text("Professional Apprearance").font(.custom("ExtraBold_Font".localized(), size: 16)).padding()
                //                                                      //                                           Spacer()
                //                                                      //                                       }
                //                                                      //
                //                                                      //                                       Rating(Rate:self.$PA)
                //
                //                                                  }
                //                                                  Spacer().frame(height: 20)
                //
                //                                                  HStack{
                //                                                      Text("We would like to know more about your experiance").font(.custom("Regular_Font".localized(), size: 12))
                //                                                          .foregroundColor(Color("darkthemeletter")).padding(.horizontal).padding(.top,10)
                //                                                      Spacer()
                //                                                  }
                //
                //                                                  HStack{
                //
                //                                                      TextField("Review Here ..", text: self.$Review)
                //                                                          //                        .lineLimit(4)
                //                                                          //                    .multilineTextAlignment(.leading)
                //                                                          //                    .frame(minWidth: 100, maxWidth: 200, minHeight: 100, maxHeight: .infinity, alignment: .topLeading)
                //
                //                                                          .padding()
                //                                                          .overlay(
                //                                                              RoundedRectangle(cornerRadius: 10)
                //                                                                  .stroke(Color("backcolor2"), lineWidth: 1)
                //                                                      )
                //                                                          .font(.custom("Regular_Font".localized(), size: 12)).padding(.horizontal)
                //
                //
                //
                //                                                  }
                //
                //                                                  HStack{
                //
                //                                                      Button(action:{
                //
                //                                                          self.ref.child("orders").child("all_orders").child(self.OrderID).child("rating").setValue(self.VS)
                //
                //                                                          self.ref.child("orders").child("all_orders").child(self.OrderID).child("review").setValue(self.Review)
                //
                //                                                          self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("active_orders").child("order_ids").child(self.OrderID).removeValue()
                //
                //                                                          self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("past_orders").child("order_ids").child(self.OrderID).setValue(self.OrderID)
                //
                //                                                      }){
                //                                                                                             Text("Submit").font(.custom("Regular_Font".localized(), size: 12))
                //                                                                                                                                             .foregroundColor(Color("h2"))
                //                                                                                                                                          .padding(.horizontal).padding(.vertical,3)
                //                                                                                                                                             // .padding(.horizontal,40)
                //                                                          //                                                                                   .frame(width : 100, height: 24)
                //
                //                                                                                                                                          .background(Color.white)
                //
                //                                                                                                                                             .cornerRadius(13)
                //                                                                                                                                             .overlay(
                //                                                                                                                                              RoundedRectangle(cornerRadius: 30).stroke(Color("t4"),lineWidth: 1)
                //
                //                                                                                                                                         )
                //                                                      }.padding()
                //
                //                                                      Spacer()
                //
                //                                                      Button(action:{
                //
                //                                                          //                        self.ref.child("orders").child("all_orders").child(self.OrderID).child("rating").setValue(self.Rate)
                //                                                          //
                //                                                          //                                                 self.ref.child("orders").child("all_orders").child(self.OrderID).child("review").setValue(self.Review)
                //
                //                                                          self.ref.child("Users").child("active_orders").child("order_ids").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child(self.OrderID).removeValue()
                //
                //                                                          self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("past_orders").child("order_ids").child(self.OrderID).setValue(self.OrderID)
                //
                //
                //                                                      }){
                //                                                          Text("Skip").font(.custom("Regular_Font".localized(), size: 14))
                //                                                             .foregroundColor(Color("h2"))
                //
                //                                                              // .padding(.horizontal,40)
                //                                                              .frame(width : 100, height: 24)
                //
                //                                                              //    .background(Color("ManBackColor"))
                //
                //                                                            .cornerRadius(13)
                //                                                      }
                //                                                  }
                //                                              }
                //
                //
                //                                          }else{
                
                
                Spacer().frame(height:10)
                
                
                
                //                                          }
                
                
            }.background(Color.white).clipped().cornerRadius(10).shadow(radius: 5).padding(10)
            
            
            //            TrackServiceProvider(OrderID :self.OrderID)
            
        }
        
        
        
        
        .onAppear{
            print("Home Page5")
            self.ref.child("orders").child("all_orders").child(self.OrderID).observe(DataEventType.value) { (datasnapshot) in
                
                self.Works = ""
                //                    self.ref.child("drivers").child(String(describing :  (datasnapshot.childSnapshot(forPath: "driver_id").value!))).child("live_location/time_driver_engaged").observeSingleEvent(of: DataEventType.value) { (drive) in
                
                //                        let lastETA = Double(String(describing :  (drive.value ?? "0"))) ?? 0.0
                //                        print("WWWWWWWWWW",drive.value)
                print("WWWWWWWWWW",(String(describing :  (datasnapshot.childSnapshot(forPath: "driver_id").value!))))
                var total_time = 0.0
                if(datasnapshot.childSnapshot(forPath: "total_time").exists()){
                    total_time = Double(String(describing :  (datasnapshot.childSnapshot(forPath: "total_time").value ?? "0"))) ?? 0.0
                }
                
                //                        self.ETA = lastETA-total_time
                self.ETA = Double(String(describing :  (datasnapshot.childSnapshot(forPath: "eta").value ?? "0"))) ?? 0.0
                
                
                
                //                    }
                
                
                for service in datasnapshot.childSnapshot(forPath:"services").children {
                    let servicesnap = service as! DataSnapshot
                    
                    
                    
                    for servicename in servicesnap.childSnapshot(forPath:"services").children {
                        let servicenamesnap = servicename as! DataSnapshot
                        
                        self.Works = self.Works  + String(describing :  (servicenamesnap.key)) + ","
                        
                        
                        
                        
                    }
                    
                    
                    
                    
                }
                
                self.Status = "0"
                if(datasnapshot.childSnapshot(forPath:"status").exists()){
                    self.Status = String(describing :  (datasnapshot.childSnapshot(forPath:"status").value)!)
                }
                
                self.getSolidLength()
                
                // self.Status = String(describing :  (datasnapshot.childSnapshot(forPath:"status").value ?? "0")! )
                //                    print("KKKKKKK \(self.Status)")
                
                self.ref.child("service_providers").child(String(describing :  (datasnapshot.childSnapshot(forPath:"sp_id").value)!)).observeSingleEvent(of: DataEventType.value) { (Spsnaps) in
                    
                    
                    self.ServiceProviderName = String(describing :  (Spsnaps.childSnapshot(forPath:"personal_information/name".localized()).value)!)
                    
                    self.ServiceProviderAddress = String(describing :  (Spsnaps.childSnapshot(forPath:"personal_information/Address".localized()).value)!)
                    
                    self.ServiceProviderImage = String(describing :  (Spsnaps.childSnapshot(forPath:"personal_information/image").value)!)
                    
                    
                }
            }
            
        }
        
        
    }
    
    
    func getInt(Status:String) -> Int {
        
        let V : Int = Int(Status) ?? 0
        return V
    }
    func getLineWidth(status:String,number:String) -> CGFloat {
        switch status {
        case "1":
            
            switch number {
            case "1":
                return 1
                break;
                
            case "2":
                return 0
                break;
            case "3":
                return 0
                break;
            case "4":
                return 0
                break;
            case "5":
                return 0
                break;
                
            default:
                return 0
            }
            
            
            break;
        case "2":
            
            switch number {
            case "1":
                return 1
                break;
                
            case "2":
                return 1
                break;
            case "3":
                return 0
                break;
            case "4":
                return 0
                break;
            case "5":
                return 0
                break;
                
            default:
                return 0
            }
            
            break;
        case "3":
            
            switch number {
            case "1":
                return 1
                break;
                
            case "2":
                return 1
                break;
            case "3":
                return 1
                break;
            case "4":
                return 0
                break;
            case "5":
                return 0
                break;
                
            default:
                return 0
            }
            
            break;
        case "4":
            switch number {
            case "1":
                return 1
                break;
                
            case "2":
                return 1
                break;
            case "3":
                return 1
                break;
            case "4":
                return 1
                break;
            case "5":
                return 0
                break;
                
            default:
                return 0
            }
            break;
        case "5":
            
            
            switch number {
            case "1":
                return 1
                break;
                
            case "2":
                return 1
                break;
            case "3":
                return 1
                break;
            case "4":
                return 1
                break;
            case "5":
                return 1
                break;
                
            default:
                return 0
            }
            
            break;
            
            
        default:
            switch number {
            case "1":
                return 1
                break;
                
            case "2":
                return 0
                break;
            case "3":
                return 0
                break;
            case "4":
                return 0
                break;
            case "5":
                return 0
                break;
                
            default:
                return 0
            }
        }
    }
    func getDashPhase(status:String,number:String) -> CGFloat {
        switch status {
        case "1":
            
            switch number {
            case "1":
                return 1
                break;
                
            case "2":
                return 0
                break;
            case "3":
                return 0
                break;
            case "4":
                return 0
                break;
            case "5":
                return 0
                break;
                
            default:
                return 0
            }
            
            
            break;
        case "2":
            
            switch number {
            case "1":
                return 0
                break;
                
            case "2":
                return 1
                break;
            case "3":
                return 0
                break;
            case "4":
                return 0
                break;
            case "5":
                return 0
                break;
                
            default:
                return 0
            }
            
            break;
        case "3":
            
            switch number {
            case "1":
                return 0
                break;
                
            case "2":
                return 0
                break;
            case "3":
                return 1
                break;
            case "4":
                return 0
                break;
            case "5":
                return 0
                break;
                
            default:
                return 0
            }
            
            break;
        case "4":
            switch number {
            case "1":
                return 0
                break;
                
            case "2":
                return 0
                break;
            case "3":
                return 0
                break;
            case "4":
                return 1
                break;
            case "5":
                return 0
                break;
                
            default:
                return 0
            }
            break;
        case "5":
            
            
            switch number {
            case "1":
                return 0
                break;
                
            case "2":
                return 0
                break;
            case "3":
                return 0
                break;
            case "4":
                return 0
                break;
            case "5":
                return 1
                break;
                
            default:
                return 0
            }
            
            break;
            
            
        default:
            switch number {
            case "1":
                return 1
                break;
                
            case "2":
                return 0
                break;
            case "3":
                return 0
                break;
            case "4":
                return 0
                break;
            case "5":
                return 0
                break;
                
            default:
                return 0
            }
        }
        
    }
    
    func getLineLength() -> CGFloat {
        
        switch self.Status {
        case "1":
            return 0.0
        case "3":
            return    (UIScreen.main.bounds.width-60)*(1/4)
            
        case "4":
            return  (UIScreen.main.bounds.width-60)*(3/4)
        case "5":
            return   (UIScreen.main.bounds.width-60)*(4/4)
        case "2":
            return   0.0
        default:
            return 0.0
        }
    }
    
    
    
    func getSolidLength()  {
        
        switch self.Status {
        case "1":
            self.solidlength =  0.0
        case "3":
            self.solidlength =    (UIScreen.main.bounds.width-60)*(1/4)
            
        case "4":
            self.solidlength =  (UIScreen.main.bounds.width-60)*(3/4)
        case "5":
            self.solidlength =   (UIScreen.main.bounds.width-60)*(4/4)
        case "2":
            self.solidlength =   0.0
        default:
            self.solidlength = 0.0
        }
        
        
    }
    
    
    
    func getStatusWord(status : String) -> String {
        switch status {
        case "1":
            return "Your Booking is Confirmed".localized()
        case "2":
            return    "Service Provider approching you".localized()
            
        case "3":
            return   "Service Provider reached your location".localized()
        case "4":
            return   "Stared the job".localized()
        case "5":
            return   "Succesfully finished".localized()
        default:
            return "Waiting for accepting by service provider".localized()
        }
    }
    
}

struct SideMenu: View {
    
    
    let ref = Database.database().reference()
    @EnvironmentObject var settings : UserSettings
    @Binding var goToSplash : Bool
    
    @State var FID: Bool = UserDefaults.standard.bool(forKey: "FaceID") ?? false
    @Binding var OProfile : Bool
    @Binding var ONotification : Bool
    @Binding var OpenCart : Bool
    @Binding var OBookings : Bool
    @Binding var isShowingScanner : Bool
    @Binding var OHelpCenter : Bool
    @Binding var OAboutUS : Bool
    @Binding var OOffers : Bool
    @Binding var OLanguageSelPage : Bool
    
    @State var Name : String = ""
    @State var Email : String = ""
    @State var Photo : String = ""
    var body: some View {
        
        
        VStack(alignment:.leading){
            //            ScrollView{
            
            ZStack(alignment:.top   ) {
                
                
                //                Spacer()
                //                    .frame(height: 200)
                
                //                VStack{
                //                    HStack{
                //                        Image("buildings").resizable()
                //                            .frame(height: 400)
                //                        Spacer()
                //                    }.padding(.top,40)
                //                    Spacer()
                //
                //
                //                }
                
                VStack{
                    
                    
                    HStack{
                        
                        if(
                            !(Auth.auth().currentUser?.isAnonymous ??
                                true
                            )
                        ){
                            
                            AnimatedImage(url:URL (string:self.Photo)).resizable().frame(width:16,height:16)
                                .foregroundColor(Color("c1")).padding(12)
                                .clipShape(Circle())
                                .overlay(Circle().stroke(Color.black,lineWidth: 1))
                        }else{
                            Image(systemName: "person").resizable().frame(width:16,height:16)
                                .foregroundColor(Color("c1")).padding(12)
                                .clipShape(Circle())
                                .overlay(Circle().stroke(Color.black,lineWidth: 1)
                                         
                                )
                        }
                        VStack(alignment:.leading){
                            
                            //                            Spacer().frame(height: 50)
                            if(
                                !(Auth.auth().currentUser?.isAnonymous ??
                                    true
                                )
                            ){
                                
                                
                                Text(self.Name)  .font(.custom("Bold_Font".localized(), size: 14))  .foregroundColor(Color("darkthemeletter"))
                                
                                //                                Text(self.Email)  .font(.custom("Regular_Font".localized(), size: 14))
                                //                                    .foregroundColor(Color("darkthemeletter"))
                            }else{
                                
                                Text("Guest")  .font(.custom("Bold_Font".localized(), size: 14))  .foregroundColor(Color("darkthemeletter"))
                            }
                            
                            
                            
                        }.padding(.leading)
                        Spacer()
                    }.padding().padding(.top,40).padding(.leading,20)
                    
                    Divider()
                    
                    VStack{
                        
                        VStack(alignment:.leading, spacing:30){
                            
                            if(
                                !(Auth.auth().currentUser?.isAnonymous ??
                                    true
                                )
                            ){
                                Button(action:{
                                    self.OProfile = true
                                    
                                }){
                                    
                                    HStack{
                                        
                                        
                                        
                                        Image("Iconly-Light-Profile") .renderingMode(.template).resizable().frame(width:20,height:20)
                                            .foregroundColor(Color("darkthemeletter"))
                                        
                                        Text("Profile").font(.custom("Bold_Font".localized(), size: 14))
                                            .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
                                        
                                        
                                    }
                                    
                                }
                                Button(action:{
                                    self.ONotification = true
                                }){
                                    HStack{
                                        
                                        Image("Iconly-Light-Notification").renderingMode(.template).resizable().frame(width:20,height:20).foregroundColor(Color("darkthemeletter"))
                                        
                                        Text("Notification").font(.custom("Bold_Font".localized(), size: 14))
                                            .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
                                        
                                        
                                        
                                        
                                    }
                                }
                                
                            }else{
                                
                                
                                
                                Spacer().frame(height:60)
                                
                            }
                            
                            Button(action:{
                                self.OpenCart = true
                                recalculateETA()
                                
                            }){
                                HStack{
                                    
                                    Image("Iconly-Light-Wallet").renderingMode(.template).resizable().frame(width:20,height:20).foregroundColor(Color("darkthemeletter"))
                                    
                                    Text("Cart").font(.custom("Bold_Font".localized(), size: 14))
                                        .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
                                    Spacer()
                                    
                                }
                            }
                            
                            
                            if(
                                !(Auth.auth().currentUser?.isAnonymous ??
                                    true
                                )
                            ){
                                Button(action:{
                                    self.OBookings = true
                                }){
                                    HStack{
                                        
                                        Image("Iconly-Light-Document").renderingMode(.template).resizable().frame(width:20,height:20).foregroundColor(Color("darkthemeletter"))
                                        
                                        Text("My Bookings").font(.custom("Bold_Font".localized(), size: 14))
                                            .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
                                        
                                        
                                    }
                                }
                            }
                            
                            Group{
                                Button(action:{
                                    
                                    self.OOffers = true
                                }){
                                    HStack{
                                        
                                        Image("Group 800").renderingMode(.template).resizable().frame(width:20,height:20).foregroundColor(Color("darkthemeletter"))
                                        
                                        Text("Offers").font(.custom("Bold_Font".localized(), size: 14))
                                            .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
                                        //                                        Spacer()
                                        
                                    }
                                }
                                //                                    Button(action:{
                                //
                                //                                        self.isShowingScanner = true
                                //                                    }){
                                //                                        HStack{
                                //
                                //                                            Image("qr-code2").renderingMode(.template).resizable().frame(width:20,height:20).foregroundColor(Color("darkthemeletter"))
                                //
                                //                                            Text("Scan QR Code").font(.custom("Bold_Font".localized(), size: 14))
                                //                                                .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
                                //                                            Spacer()
                                //
                                //                                        }
                                //
                                //                                    }
                                
                                if(
                                    //                                    !(Auth.auth().currentUser?.isAnonymous ??
                                    true
                                    //                                    )
                                ){
                                    Button(action:{
                                        
                                        
                                        let b : Bool =   UserDefaults.standard.bool(forKey: "FaceID")
                                        print(b)
                                        
                                        UserDefaults.standard.set(!b, forKey: "FaceID")
                                        
                                        
                                        self.FID = UserDefaults.standard.bool(forKey: "FaceID")
                                        
                                    }){
                                        HStack{
                                            
                                            Image("face").renderingMode(.template).resizable().frame(width:20,height:20).foregroundColor(Color("darkthemeletter"))
                                            
                                            Text(self.FID ? "Disable Face ID" : "Enable Face ID" ).font(.custom("Bold_Font".localized(), size: 14))
                                                .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
                                            
                                            
                                        }
                                        
                                        
                                        
                                    }
                                    
                                }
                                Button(action:{
                                    
                                }){
                                    HStack{
                                        
                                        Image("share").renderingMode(.template).resizable().frame(width:20,height:20).foregroundColor(Color("darkthemeletter"))
                                        
                                        Text("Share").font(.custom("Bold_Font".localized(), size: 14))
                                            .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
                                        
                                        
                                    }
                                }
                                Button(action:{
                                    
                                    self.OHelpCenter = true
                                }){
                                    HStack{
                                        
                                        Image("Group 772").renderingMode(.template).resizable().frame(width:20,height:20).foregroundColor(Color("darkthemeletter"))
                                        
                                        Text("Help Center").font(.custom("Bold_Font".localized(), size: 14))
                                            .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
                                        
                                        
                                    }
                                }
                                Button(action:{
                                    
                                    self.OAboutUS = true
                                }){
                                    HStack{
                                        
                                        Image("Group 826").renderingMode(.template).resizable().frame(width:20,height:20).foregroundColor(Color("darkthemeletter"))
                                        
                                        Text("About US").font(.custom("Bold_Font".localized(), size: 14))
                                            .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
                                        
                                        
                                    }
                                }
                                
                                
                                Button(action:{
                                    
                                    self.OLanguageSelPage = true
                                }){
                                    HStack{
                                        
                                        Image("lansel").renderingMode(.template).resizable().frame(width:20,height:20).foregroundColor(Color("darkthemeletter"))
                                        
                                        Text("Change Language").font(.custom("Bold_Font".localized(), size: 14))
                                            .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
                                        
                                        
                                    }
                                }
                                
                                
                                
                                if(
                                    !(Auth.auth().currentUser?.isAnonymous ??
                                        true
                                    )
                                ){
                                    Button(action:{
                                        
                                        let firebaseAuth = Auth.auth()
                                        do {
                                            try firebaseAuth.signOut()
                                            
                                            
                                            
                                            UserDefaults.standard.set(false,forKey:"SOCIAL")
                                            self.goToSplash = true
                                            
                                            
                                            
                                        } catch let signOutError as NSError {
                                            print ("Error signing out: %@", signOutError)
                                        }
                                        
                                        //
                                        
                                        
                                    }){
                                        
                                        
                                        
                                        HStack{
                                            
                                            Image("logout").renderingMode(.template).resizable().frame(width:20,height:20).foregroundColor(Color("darkthemeletter"))
                                            
                                            Text("Sign Out").font(.custom("Bold_Font".localized(), size: 14))
                                                .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
                                            
                                            
                                        }
                                        
                                        
                                    }
                                }
                            }
                            
                        }.padding(.top,35).padding(.leading,10)
                        .padding(.horizontal, 25.0)
                        Spacer()
                        
                        
                        
                        
                        
                        
                    }
                    //.frame(maxWidth:.infinity ,maxHeight: geometry.size.height*0.8)
                    //                        .background(RoundedCorners(color: .white, tl: 30, tr: 30, bl: 0, br: 0))
                    
                }
                
                
            }.background(Color.white).frame( maxWidth:.infinity,alignment: .leading).frame(height:UIScreen.main.bounds.height)
            //                .background(LinearGradient(gradient: Gradient(colors: [Color("G1"),Color("G4"),Color("G5")]), startPoint: .top, endPoint: .bottom))
            
            
            //        }
            
            
            .navigationBarItems(trailing:Text(""))
            
            
            
            
            
            
            
            
        }.onAppear{
            
            
            
            //
            self.ref.child("Users").child("\(UserDefaults.standard.string(forKey: "Aid") ?? "0")/personal_details").observeSingleEvent(of: DataEventType.value) { (dataSnapshot) in
                
                if(dataSnapshot.childSnapshot(forPath:"name").value != nil){
                    self.Name = String(describing :  (dataSnapshot.childSnapshot(forPath:"name").value  ?? ""))
                    self.Photo = String(describing :  (dataSnapshot.childSnapshot(forPath:"dp").value ?? "") )
                    self.Email = String(describing :  (dataSnapshot.childSnapshot(forPath:"pemail").value ?? "") )
                }
            }
        }
        //   .padding()
        //  .frame(maxWidth: .infinity, alignment: .leading)
        //  .background(Color(red: 32/255, green: 32/255, blue: 32/255))
        
        
    }
    
}
//struct TextView: UIViewRepresentable {
//    @Binding var text: String
//    
//    func makeCoordinator() -> Coordinator {
//        Coordinator(self)
//    }
//    
//    func makeUIView(context: Context) -> UITextView {
//        
//        let myTextView = UITextView()
//        myTextView.delegate = context.coordinator
//        
//        myTextView.font = UIFont(name: "HelveticaNeue", size: 15)
//        myTextView.isScrollEnabled = true
//        myTextView.isEditable = true
//        myTextView.isUserInteractionEnabled = true
//        myTextView.backgroundColor = UIColor(white: 0.0, alpha: 0.05)
//        
//        return myTextView
//    }
//    
//    func updateUIView(_ uiView: UITextView, context: Context) {
//        uiView.text = text
//    }
//    
//    class Coordinator : NSObject, UITextViewDelegate {
//        
//        var parent: TextView
//        
//        init(_ uiTextView: TextView) {
//            self.parent = uiTextView
//        }
//        
//        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//            return true
//        }
//        
//        func textViewDidChange(_ textView: UITextView) {
//            print("text now: \(String(describing: textView.text!))")
//            self.parent.text = textView.text
//        }
//    }
//}

//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ContentView()
//    }
//}
struct pastOrderSet: Hashable ,Identifiable{
    
    var id: String
    var driverID:String
    var SpName:String
    var Date:String
    var Image:String
    var snap : DataSnapshot
    var Address: String
    var latitude :String
    var longitude : String
    var OrderSnap : DataSnapshot
    
}



//OLD REORDER
//{
//                                                        print("Entered")
//                                                        self.Snap = item.snap
//                                                        print(item.SpName)
//
//                                                                                                        self.ViewLoader = true
//
//
//                                                        //                                                self.ref.child("drivers").child(item.driverID).observeSingleEvent(of: DataEventType.value) { (driversnap) in
//                                                        //
//
//                                                        //                                                    if(String(describing :  (driversnap.childSnapshot(forPath:"status").value)!) == "1"){
//                                                        //
//                                                        //
//                                                        //                                                               //        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5)) {
//                                                        //
//                                                        //                                                                    let driverlat =
//                                                        //                                                                    //10.797765
//                                                        //                                                                       Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/latitude").value)!))
//                                                        //
//                                                        //                                                                    let driverlon =
//                                                        //                                                                    //76.635908
//                                                        //                                                                        Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/longitude").value)!))
//                                                        //
//                                                        //                                                                    var driverloc : CLLocation = CLLocation(latitude: driverlat!, longitude: driverlon!)
//                                                        //                                                                    var driver2D : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: driverlat!, longitude: driverlon!)
//                                                        //
//                                                        //                                                                    let userlat =
//                                                        //                                                //                    10.874858
//                                                        //                                                                        UserDefaults.standard.double(forKey: "latitude")
//                                                        //                                                                    let userlon =
//                                                        //                                                //                    76.449728
//                                                        //                                                                        UserDefaults.standard.double(forKey: "longitude")
//                                                        //
//                                                        //
//                                                        //                                                               var userloc : CLLocation = CLLocation(latitude: userlat, longitude: userlon)
//                                                        //                                                                    var user2D  : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: userlat, longitude: userlon)
//                                                        //
//                                                        //
//                                                        //                                                                //    var distance : CLLocationDistance = userloc.distance(from: driverloc)
//                                                        //                                                                   // var timeOfArrival :
//                                                        //
//                                                        //                                                                    let request = MKDirections.Request()
//                                                        //                                                             request.source = MKMapItem(placemark:  MKPlacemark(coordinate: driver2D))
//                                                        //                                                             request.destination = MKMapItem(placemark:  MKPlacemark(coordinate: user2D))
//                                                        //
//                                                        //                                                             request.requestsAlternateRoutes = false
//                                                        //
//                                                        //                                                             let directions = MKDirections(request: request)
//                                                        //                                                //                    directions.calculateETA { (responds, error) in
//                                                        //                                                //                         if error != nil {
//                                                        //                                                //                                            print("Error getting directions")
//                                                        //                                                //                                        } else {
//                                                        //                                                //                            var ETA  =    responds?.expectedTravelTime
//                                                        //                                                //
//                                                        //                                                //
//                                                        //                                                //
//                                                        //                                                //                        }
//                                                        //                                                //                    }
//                                                        //                                                                    directions.calculateETA{ response, error in
//                                                        //
//                                                        //
//                                                        //
//                                                        //
//                                                        //                                                               //     directions.calculate { response, error in
//                                                        //                                                                            var eta:Double = 10000000
//                                                        //                                                                        if(error != nil){
//                                                        //                                                                            print(error.debugDescription)
//                                                        //                                                                        }else{
//                                                        //
//                                                        //                                                                                eta = response?.expectedTravelTime as! Double
//                                                        //
//                                                        //
//                                                        //                                                //                        let routes = response?.routes
//                                                        //
//                                                        //
//                                                        //
//                                                        //
//                                                        //                                                //                        if(routes != nil){
//                                                        //                                                //                        let selectedRoute = routes![0]
//                                                        //                                                //                        let distance = selectedRoute.distance
//                                                        //                                                //                         eta = selectedRoute.expectedTravelTime
//                                                        //                                                //
//                                                        //                                                //
//                                                        //                                                //                        }
//                                                        //                                                                            print(eta)
//                                                        //
//                                                        //                                                                        }
//                                                        //
//                                                        //                                                                         self.ViewLoader = false
//                                                        //
//                                                        //                                                self.SelectedDriver = item.driverID
//                                                        //                                                                        self.ETA = eta
//                                                        //
//                                                        //                                                self.Snap = item.snap
//                                                        //
//                                                        //                                                                       self.ReOrderToCart(item: item,DriverID: item.driverID,ETA: eta)
//                                                        //
//                                                        //                                                                        self.GoToBookDetails = true
//                                                        //
//                                                        //
//                                                        //
//                                                        //                                                    }
//                                                        //                                                    }else{
//
////                                                        self.ViewLoader = false
//
//
//                                                        var selectedETAD : ETAAndDriver = ETAAndDriver(ETA:1000000.0 , DriverID: "")
//
//                                                        self.ref.child("drivers").observeSingleEvent(of: DataEventType.value) { (allDriverSnapshot)
//                                                            in
//
//
//                                                            let     allDriver :DataSnapshot = allDriverSnapshot as! DataSnapshot
//
//                                                            var x = item.snap.childSnapshot(forPath: "drivers_info/active_drivers_id").childrenCount
//
//
//                                                            for driver  in  item.snap.childSnapshot(forPath: "drivers_info/active_drivers_id").children{
//                                                                let  driverSnap = driver as! DataSnapshot
////                                                                   print("UUUUUUUU",driverSnap.key,(String(describing :  (allDriver.value)!)))
//
//                                                                print( (String(describing :  (allDriver.childSnapshot(forPath:"\(driverSnap.key)/live_location/latitude").value)!), (String(describing :  (allDriver.childSnapshot(forPath:"\(driverSnap.key)/live_location/longitude").value)!))))
//
//
//                                                                let driverlat =
//                                                                    //10.797765
//                                                                    Double(String(describing :  (allDriver.childSnapshot(forPath:"\(driverSnap.key)/live_location/latitude").value)!))
//
//                                                                let driverlon =
//                                                                    //76.635908
//                                                                    Double(String(describing :  (allDriver.childSnapshot(forPath:"\(driverSnap.key)/live_location/longitude").value)!))
//
//
//                                                                var driverloc : CLLocation = CLLocation(latitude: driverlat!, longitude: driverlon!)
//                                                                var driver2D : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: driverlat!, longitude: driverlon!)
//
//
//
//
//                                                                let userlat =
//                                                                    //                    10.874858
//                                                                    UserDefaults.standard.double(forKey: "latitude")
//                                                                let userlon =
//                                                                    //                    76.449728
//                                                                    UserDefaults.standard.double(forKey: "longitude")
//
//
//                                                                var userloc : CLLocation = CLLocation(latitude: userlat, longitude: userlon)
//                                                                var user2D  : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: userlat, longitude: userlon)
//
//
//                                                                //    var distance : CLLocationDistance = userloc.distance(from: driverloc)
//                                                                // var timeOfArrival :
//
//                                                                //                                                                                            let request = MKDirections.Request()
//                                                                //                                                                                     request.source = MKMapItem(placemark:  MKPlacemark(coordinate: driver2D))
//                                                                //                                                                                     request.destination = MKMapItem(placemark:  MKPlacemark(coordinate: user2D))
//                                                                //
//                                                                //                                                                                     request.requestsAlternateRoutes = false
//                                                                //
//                                                                //                                                                                     let directions = MKDirections(request: request)
//                                                                //                    directions.calculateETA { (responds, error) in
//                                                                //                         if error != nil {
//                                                                //                                            print("Error getting directions")
//                                                                //                                        } else {
//                                                                //                            var ETA  =    responds?.expectedTravelTime
//                                                                //
//                                                                //
//                                                                //
//                                                                //                        }
//                                                                //                    }
//                                                                //                                                                                            directions.calculateETA{ response, error in
//                                                                //
//                                                                //
//                                                                //
//                                                                //
//                                                                //                                                                                       //     directions.calculate { response, error in
//                                                                //                                                                                                    var eta:Double = 10000000
//                                                                //                                                                                                if(error != nil){
//                                                                //                                                                                                    print(error.debugDescription)
//                                                                //                                                                                                }else{
//
//
//                                                                var eta:Double = 10000000
//
//                                                                let origin = "\(driver2D.latitude),\(driver2D.longitude)"
//                                                                let destination = "\(user2D.latitude),\(user2D.longitude)"
//
//
//                                                                let S : String =  "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=\(origin)&destinations=\(destination)&mode=driving&key=AIzaSyCYQeItZ7SVEQ24Grjzc3zA1B_2DU_GfPU"
//
//                                                                let url = URL(string:S
//                                                                )
//                                                                //        else {
//                                                                //        let error = NSError(domain: "LocalDomain", code: 0, userInfo: [NSLocalizedDescriptionKey: "Failed to create object URL"])
//                                                                //        print("Error: \(error)")
//                                                                //        return
//                                                                //    }
//                                                                let config = URLSessionConfiguration.default
//                                                                let session = URLSession(configuration: config)
//                                                                let task = session.dataTask(with: url!, completionHandler: {
//                                                                    (data, response, error) in
//
//                                                                    //                                                                                                                                                   print("11111111",driversnap.key)
//                                                                    x = x-1
//                                                                    if error != nil {
//                                                                        print(error!.localizedDescription)
//                                                                    }
//                                                                    else {
//
//                                                                        //  print(response)
//                                                                        do {
//                                                                            if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
//
//                                                                                guard let routes = json["rows"] as? NSArray else {
//                                                                                    DispatchQueue.main.async {
//                                                                                    }
//                                                                                    return
//                                                                                }
//                                                                                if (routes.count > 0) {
//                                                                                    let overview_polyline = routes[0] as? NSDictionary
//                                                                                    //   let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
//                                                                                    //  let points = dictPolyline?.object(forKey: "points") as? String
//
//                                                                                    DispatchQueue.main.async {
//                                                                                        //
//
//
//                                                                                        let legs = overview_polyline?["elements"] as! Array<Dictionary<String, AnyObject>>
//
//                                                                                        let distance = legs[0]["distance"] as? NSDictionary
//                                                                                        let distanceValue = distance?["value"] as? Int ?? 0
//                                                                                        let distanceDouleValue = distance?["value"] as? Double ?? 0.0
//                                                                                        let duration = legs[0]["duration"] as? NSDictionary
//                                                                                        let totalDurationInSeconds = duration?["value"] as? Int ?? 0
//                                                                                        let durationDouleValue = duration?["value"] as? Double ?? 0.0
//
//
//
//                                                                                        eta = durationDouleValue+600 ;
//
//
//                                                                                        if(eta < 601){
//                                                                                            eta = 10000000
//                                                                                        }
//
//
//
//
//
//
//
//
//
//                                                                                        if (eta < selectedETAD.ETA){
//                                                                                            selectedETAD  = ETAAndDriver(ETA:eta, DriverID: driverSnap.key)
//
//                                                                                        }
//
//                                                                                        if(x==0){
//                                                                                            print("OOOOOO",x) ;
//                                                                                               self.ViewLoader = false
//                                                                                            if(selectedETAD.DriverID != ""){
//                                                                                                self.SelectedDriver = selectedETAD.DriverID
//                                                                                                self.ETA = selectedETAD.ETA
//
//
//                                                                                                self.ReOrderToCart(item: item,DriverID:selectedETAD.DriverID, ETA: selectedETAD.ETA )
//                                                                                                self.Reorder = item.OrderSnap.key
//                                                                                                self.GoToBookDetails = true
//
//                                                                                            }else{
//                                                                                                self.AlertMessege2 = "Soory , No Free drivers Available"
//                                                                                                self.ALERT2 = true
//                                                                                            }
//
//                                                                                        }
//                                                                                        print (S,eta)
//
//
//                                                                                    }
//                                                                                }
//                                                                                else {
//                                                                                    print(json)
//                                                                                    DispatchQueue.main.async {
//                                                                                        //   SVProgressHUD.dismiss()
//                                                                                    }
//                                                                                }
//                                                                            }
//                                                                        }
//                                                                        catch {
//                                                                            print("error in JSONSerialization")
//                                                                            DispatchQueue.main.async {
//                                                                                //  SVProgressHUD.dismiss()
//                                                                            }
//                                                                        }
//                                                                    }
//                                                                })
//                                                                task.resume()
//
//
//
//
//
//                                                                //                                                                    }
//                                                                //
//                                                                //
//                                                                //
//                                                                //                                                                }
//
//
//
//
//
//
//
//                                                                print("TTTTTTT",x)
//
//
//
//
//
//
//
//                                                            }
//
//
//
//
//
//                                                        }
//
//
//
//
//
//
//
//
//
//
//                                                        //                                                    }
//
//                                                        //                                        }
//                                                        //                                                        }
//
//
//                                                    }



struct Rating: View {
    
    
    @Binding var Rate : Int
    
    var body: some View {
        
        HStack{
            Spacer()
            
            Group{
                //            HStack{
                //                Text("1").foregroundColor(Color(
                //                    "MainFontColor1")).font(.custom("ExtraBold_Font".localized(), size: 16))
                Image(systemName: "star.fill").resizable().foregroundColor(Color("c1")).frame(width:30,height:30)
                    
                    //            }
                    //            .padding(.all,8).overlay(
                    //                RoundedRectangle(cornerRadius: 10)
                    //                    .stroke(Color("ManBackColor"), lineWidth: 1)
                    //            )
                    .onTapGesture {
                        self.Rate = 1
                    }
                Spacer()
                //            HStack{
                //                Text("2").foregroundColor(Color(
                //                    "MainFontColor1")).font(.custom("ExtraBold_Font".localized(), size: 16))
                Image(systemName: "star.fill").resizable().foregroundColor(self.Rate>=2 ? Color("c1") : Color("lightgray")).frame(width:30,height:30)
                    
                    //            }
                    //            .padding(.all,8).overlay(
                    //                RoundedRectangle(cornerRadius: 10)
                    //
                    //                    .stroke(Color("ManBackColor"), lineWidth: 1)
                    //            )
                    .onTapGesture {
                        self.Rate = 2
                    }
                Spacer()
                
                //            HStack{
                //                Text("3").foregroundColor(Color(
                //                    "MainFontColor1")).font(.custom("ExtraBold_Font".localized(), size: 16))
                Image(systemName: "star.fill").resizable().foregroundColor(self.Rate>=3 ? Color("c1") : Color("lightgray")).frame(width:30,height:30)
                    
                    //            }
                    //            .padding(.all,8).overlay(
                    //                RoundedRectangle(cornerRadius: 10)
                    //                    .stroke(Color("ManBackColor"), lineWidth: 1)
                    //            )
                    .onTapGesture {
                        self.Rate = 3
                    }
            }
            
            Group{
                Spacer()
                
                //            HStack{
                //                Text("4").foregroundColor(Color(
                //                    "MainFontColor1")).font(.custom("ExtraBold_Font".localized(), size: 16))
                Image(systemName: "star.fill").resizable().foregroundColor(self.Rate>=4 ? Color("c1") : Color("lightgray")).frame(width:30,height:30)
                    
                    //            }
                    //            .padding(.all,8).overlay(
                    //                RoundedRectangle(cornerRadius: 10)
                    //                    .stroke(Color("ManBackColor"), lineWidth: 1)
                    //            )
                    .onTapGesture {
                        self.Rate = 4
                    }
                
                Spacer()
                
                //            HStack{
                //                Text("5").foregroundColor(Color(
                //                    "MainFontColor1")).font(.custom("ExtraBold_Font".localized(), size: 16))
                Image(systemName: "star.fill").resizable().foregroundColor(self.Rate>=5 ? Color("c1") : Color("lightgray")).frame(width:30,height:30)
                    
                    //            }
                    //            .padding(.all,8).overlay(
                    //                RoundedRectangle(cornerRadius: 10)
                    //                    .stroke(Color("ManBackColor"), lineWidth: 1)
                    //            )
                    .onTapGesture {
                        self.Rate = 5
                    }
                Spacer()
            }
        }
    }
}
func recalculateETA() {
    let ref = Database.database().reference()
    
    ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").observeSingleEvent(of: DataEventType.value) { (cartSnap) in
        if(cartSnap.childSnapshot(forPath:"ServiceProvider_id").value != nil){
            print("333333","1")
            
            ref.child("service_providers").child(String(describing:cartSnap.childSnapshot(forPath:"ServiceProvider_id").value!)).observeSingleEvent(of: DataEventType.value) { (snapshot) in
                
                
                print("333333","2")
                //                    self.Snap = self.booking.ServiceSnap
                
                var selectedETAD : ETAAndDriver = ETAAndDriver(ETA:10000000.0 , DriverID: "")
                
                ref.child("drivers").observeSingleEvent(of: DataEventType.value) { (allDriverSnapshot)
                    in
                    
                    
                    let     allDriver :DataSnapshot = allDriverSnapshot as! DataSnapshot
                    
                    var x = snapshot.childSnapshot(forPath: "drivers_info/active_drivers_id").childrenCount
                    
                    print("333333","3")
                    
                    for driver  in  snapshot.childSnapshot(forPath: "drivers_info/active_drivers_id").children{
                        let  driverSnap = driver as! DataSnapshot
                        
                        
                        print("333333","4")
                        //                                   print("UUUUUUUU",driverSnap.key,(String(describing :  (allDriver.value)!)))
                        
                        print( (String(describing :  (allDriver.childSnapshot(forPath:"\(driverSnap.key)/live_location/latitude").value)!), (String(describing :  (allDriver.childSnapshot(forPath:"\(driverSnap.key)/live_location/longitude").value)!))))
                        
                        
                        let driverlat =
                            //10.797765
                            Double(String(describing :  (allDriver.childSnapshot(forPath:"\(driverSnap.key)/live_location/latitude").value)!))
                        
                        let driverlon =
                            //76.635908
                            Double(String(describing :  (allDriver.childSnapshot(forPath:"\(driverSnap.key)/live_location/longitude").value)!))
                        
                        
                        var driverloc : CLLocation = CLLocation(latitude: driverlat!, longitude: driverlon!)
                        var driver2D : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: driverlat!, longitude: driverlon!)
                        
                        
                        
                        
                        let userlat =
                            //                    10.874858
                            UserDefaults.standard.double(forKey: "latitude")
                        let userlon =
                            //                    76.449728
                            UserDefaults.standard.double(forKey: "longitude")
                        
                        
                        var userloc : CLLocation = CLLocation(latitude: userlat, longitude: userlon)
                        var user2D  : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: userlat, longitude: userlon)
                        
                        
                        
                        var eta:Double = 10000000
                        
                        let origin = "\(driver2D.latitude),\(driver2D.longitude)"
                        let destination = "\(user2D.latitude),\(user2D.longitude)"
                        
                        
                        let S : String =  "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=\(origin)&destinations=\(destination)&mode=driving&key=AIzaSyCYQeItZ7SVEQ24Grjzc3zA1B_2DU_GfPU"
                        
                        let url = URL(string:S
                        )
                        //        else {
                        //        let error = NSError(domain: "LocalDomain", code: 0, userInfo: [NSLocalizedDescriptionKey: "Failed to create object URL"])
                        //        print("Error: \(error)")
                        //        return
                        //    }
                        let config = URLSessionConfiguration.default
                        let session = URLSession(configuration: config)
                        let task = session.dataTask(with: url!, completionHandler: {
                            (data, response, error) in
                            
                            //                                                                                                                                                   print("11111111",driversnap.key)
                            x = x-1
                            if error != nil {
                                print(error!.localizedDescription)
                            }
                            else {
                                
                                //  print(response)
                                do {
                                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                                        
                                        guard let routes = json["rows"] as? NSArray else {
                                            DispatchQueue.main.async {
                                            }
                                            return
                                        }
                                        if (routes.count > 0) {
                                            let overview_polyline = routes[0] as? NSDictionary
                                            //   let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                                            //  let points = dictPolyline?.object(forKey: "points") as? String
                                            
                                            DispatchQueue.main.async {
                                                //
                                                
                                                print("333333","9")
                                                let legs = overview_polyline?["elements"] as! Array<Dictionary<String, AnyObject>>
                                                
                                                let distance = legs[0]["distance"] as? NSDictionary
                                                let distanceValue = distance?["value"] as? Int ?? 0
                                                let distanceDouleValue = distance?["value"] as? Double ?? 0.0
                                                let duration = legs[0]["duration"] as? NSDictionary
                                                let totalDurationInSeconds = duration?["value"] as? Int ?? 0
                                                let durationDouleValue = duration?["value"] as? Double ?? 0.0
                                                
                                                
                                                
                                                eta = durationDouleValue+300 ;
                                                
                                                
                                                if(eta < 301){
                                                    eta = 10000000
                                                }
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                if (eta < selectedETAD.ETA){
                                                    selectedETAD  = ETAAndDriver(ETA:eta, DriverID: driverSnap.key)
                                                    
                                                }
                                                
                                                if(x==0){
                                                    print("333333","10")
                                                    if(selectedETAD.DriverID != ""){
                                                        
                                                        cartSnap.ref.child("DriverID").setValue(selectedETAD.DriverID)
                                                        cartSnap.ref.child("ETA").setValue(selectedETAD.ETA)
                                                        
                                                    }else{
                                                        
                                                    }
                                                    
                                                }
                                                print (S,eta)
                                                
                                                
                                            }
                                        }
                                        else {
                                            print(json)
                                            DispatchQueue.main.async {
                                                
                                                //   SVProgressHUD.dismiss()
                                            }
                                        }
                                    }
                                }
                                catch {
                                    print("error in JSONSerialization")
                                    DispatchQueue.main.async {
                                        
                                        //  SVProgressHUD.dismiss()
                                    }
                                }
                            }
                        })
                        task.resume()
                        
                        
                        
                        
                        
                        //                                                                    }
                        //
                        //
                        //
                        //                                                                }
                        
                        
                        
                        
                        
                        
                        
                        print("TTTTTTT",x)
                        
                        
                        
                        
                        
                        
                        
                    }
                    
                    //                            if(self.bookings.ServiceSnap.childSnapshot(forPath: "drivers_info/active_drivers_id").childrenCount == 0){
                    //                                self.SuccessETA = "2"
                    //                                self.Output = self.bookings
                }
                
                
                
            }
            
            
            
            
            
            
            
            
            
            
            //                                                    }
            
            //                                        }
            //                                                        }
            
            
            
            
            
            
        }
        
        
        
    }
    
    
}
struct Line: Shape {
    func path(in rect: CGRect) -> Path {
        var path = Path()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: rect.width, y: 0))
        return path
    }
}
struct RatingView : View {
    
    @Binding var item:Bookings
    
    var body : some View{
        HStack{
            //    Text(String(item.rating))
            Image(systemName: item.rating > 0.0 ?  "star.fill" :"star").resizable().frame(width:10,height: 10).foregroundColor(item.rating > 0.0 ? Color.yellow:Color.gray)
            Image(systemName: item.rating > 1.0 ?  "star.fill" :"star").resizable().frame(width:10,height: 10).foregroundColor(item.rating > 1.0 ? Color.yellow:Color.gray)
            Image(systemName: item.rating > 2.0 ?  "star.fill" :"star").resizable().frame(width:10,height: 10).foregroundColor(item.rating > 2.0 ? Color.yellow:Color.gray)
            Image(systemName: item.rating > 3.0 ?  "star.fill" :"star").resizable().frame(width:10,height: 10).foregroundColor(item.rating > 3.0 ? Color.yellow:Color.gray)
            Image(systemName: item.rating > 4.0 ?  "star.fill" :"star").resizable().frame(width:10,height: 10).foregroundColor(item.rating > 4.0 ? Color.yellow:Color.gray)
        }
    }
}
func getDate(timeinmilli:Double) -> String {
    let milisecond = timeinmilli
    //                                                             print("KKKKKK",milisecond)
    let dateVar = Date.init(timeIntervalSince1970: TimeInterval(milisecond)/1000)
    var dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd-MMMM-yyyy hh:mm a"
    dateFormatter.locale = Locale(identifier: UserDefaults.standard.string(forKey: "LANSEL") ?? "en")
    //     dateFormatter.locale = Locale(identifier: UserDefaults.standard.string(forKey: "LANSEL") ?? "en")
    //                                                             print(dateFormatter.string(from: dateVar))
    return UnLocNumbers(key: dateFormatter.string(from: dateVar))
}



func ReOrderToCart(booking:Bookings,DriverID:String,ETA:Double) {
    
    let ref = Database.database().reference()
    
    ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").removeValue()
    
    let node = String((Date().timeIntervalSince1970)).replacingOccurrences(of: ".", with: "")
    
    for service in booking.OrderSnap.childSnapshot(forPath: "services").children{
        let serviceSnap = service as! DataSnapshot
        let Vehicle = serviceSnap.key.components(separatedBy: ["&"])[0].trimmingCharacters(in: .whitespacesAndNewlines)
        
        
        ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("services").setValue(serviceSnap.childSnapshot(forPath: "services").value)
        ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("addons").setValue(serviceSnap.childSnapshot(forPath: "add_on_services").value)
        
        ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("Vehicle").setValue(Vehicle)
        
        ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("VehicleName").setValue(Vehicle)
        
    }
    
    
    ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider").setValue(booking.ServiceProvider)
    ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("DriverID").setValue(DriverID)
    ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ETA").setValue(ETA)
    ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider_id").setValue(booking.ServiceSnap.key)
    ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider_image").setValue(booking.ServiceSnap.childSnapshot(forPath:"personal_information/image").value!)
    
    
    
}

//func getLocationName(location:CLLocation) -> String {
//   let k = ""
//                                                                                        let georeader = CLGeocoder()
//                                                                                  georeader.reverseGeocodeLocation(location){ (places,err) in
//                                                                                    
//                                                                                return places?.first?.name as! String
//                                                                                    
//    }
//    
//}
func UnLocNumbers(key:String) -> String {
    //    print((UserDefaults.standard.string(forKey: "LANSEL") ?? ""))
    
    
    
    
    
    
    
    
    
    //        let K :NSString = key as NSString
    
    
    
    
    
    
    let A = Array(key)
    //        var chars = [Character](key)
    //        print("wwwwwwwwwwwwww",String(K.prefix(1)).localized())
    
    var value = ""
    for k in A {
        
        
        
        let formatter: NumberFormatter = NumberFormatter()
        formatter.locale = NSLocale(localeIdentifier: "EN") as Locale?
        
        
        let final = formatter.number(from: String(k))
        
        if(final == nil){
            
            value += String(k)
        }else{
            value +=  String(Int(final!))
        }
        
        
        
        
        
        
        
        
        
        
        //        print("00000000",String(k).localized())
    }
    //        for k in A {
    //                   var V=""
    //
    //
    //
    //
    //
    //
    //            switch k {
    //                   case "۰":
    //                       V =  "0" ;
    //                       break;
    //
    //
    //            case "۱":
    //                      V = "1" ;
    //                              break;
    //                       case "۲":
    //                       V = "2" ;
    //                              break;
    //                       case "۳":
    //                           V = "3" ;
    //                              break;
    //
    //                       case "۴":
    //                           V = "4" ;
    //                              break;
    //                       case "۵":
    //                           V = "5" ;
    //                              break;
    //
    //                       case "۶":
    //                      V = "6" ;
    //                              break;
    //
    //                       case "۷":
    //                      V = "7" ;
    //                              break;
    //
    //                       case "۸":
    //                      V = "8" ;
    //                              break;
    //                       case "۹":
    //                                  V = "9" ;
    //                              break;
    //
    //
    //
    //            default:
    //                V = "5"
    //            }
    //
    //                   print("wwwwwwwwww",V)
    //
    //
    //
    //                   value += String(V)
    //
    //
    //
    //
    //
    //
    //           //        print("00000000",String(k).localized())
    //               }
    return value
}

struct MainPage: View {
    
    
    
    //    @State var Vibrate : Int = 0
    @Binding  var VEHICLES : [VEHICLETYPES]
    
    //       @State var timer: Timer.TimerPublisher = Timer.publish (every: 0.01, on: .main, in: .common)
    
    
    @Binding var YouHave1 : Bool
    @Binding var YouHave2 : Bool
    @Binding var Vibrate : Int
    @ObservedObject private var locationManager = LocationManager()
    @Binding var page: Int
    @Binding var DisplayActive : Bool
    //    @State var navigationController:UINavigationController = UINavigationController()
    @Binding var RecentAddress : [OrderAddress]
    @Binding var ALERT2 : Bool
    @Binding var AlertMessege2:String
    @Binding var OpenLoginPage:Bool
    @Binding var goToSplash : Bool
    //    @State var FID: Bool = UserDefaults.standard.bool(forKey: "FaceID") ?? false
    @Binding var OProfile : Bool
    @Binding var ONotification : Bool
    
    @Binding var OBookings : Bool
    
    @Binding var openSPD : Bool
    @Binding var OHelpCenter : Bool
    @Binding var OAboutUS : Bool
    @Binding var OOffers : Bool
    @Binding var goToTrack : Bool
    @Binding var OpenPartialSheet : Bool
    
    @Binding var openVA: Bool
    @Binding var ViewLoader: Bool
    @Binding var PastOrders : [Bookings]
    @EnvironmentObject var vehicleItems : VehicleItems
    
    @Binding var GoToBookDetails : Bool
    @Binding var GTB : Bool
    //    @State var SelectedDriver : String = ""
    @Binding var ETA : Double
    @Binding var OpenCart : Bool
    @Binding var Snap : DataSnapshot
    
    let ref = Database.database().reference()
    
    @Binding  var isShowingScanner :Bool
    //       @State var Reorder : String = "0"
    
    @Binding var openSP : Bool
    @Binding var openSPL : Bool
    @Binding var ALERT : Bool
    @State var activeImageIndex = 0
    @State var Stage : Int = 0
    @State var selectedImageIndex = 0
    @State var timer: Timer.TimerPublisher = Timer.publish (every: 4, on: .main, in: .common)
    
    @ObservedObject var cartFetch = CartFetch()
    @ObservedObject var serviceProviderFetch = FindOffer()
    @Binding var StreetAddress : String
    @Binding var menuOpen : Bool
    @Binding var goToMap : Bool
    
    @EnvironmentObject var settings : UserSettings
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    
    @Binding var OLanguageSelPage : Bool
    //    @State var localauthsuccess : Bool = false
    //   @State var enablelocalauthentication : Bool = true
    @Binding var ActiveOrderID:[String]
    
    
    
    @Binding var OpenReorderSheet:Bool
    @Binding var goToMap2:Bool
    //       @State var LastAddress:[String:String] = [String:String]()
    
    @Binding var item:Bookings
    @Binding var Output:Bookings
    
    
    @Binding var selectedOrderID:String
    @Binding var Driver:String
    
    @Binding var SuccessETA :String
    
    @State private var offset = CGSize.zero
    
    
    @Binding var x : CGFloat
    @Binding var count : CGFloat
    @Binding var screen : CGFloat
    @Binding var op : CGFloat
    @State var myLocation : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 11.11, longitude: 24.11)
    
    @Binding var placedisplay : Bool
    //      @State var placedisplay : Bool = true
    
    @Binding var cartsNumber:Int
    
    
    
    @Binding var addressDatas :[AddressDataO]
    
    @State var Offers :[Offer] = [Offer]()
    
    var body: some View {
        
        
        GeometryReader { geometry in
            
            VStack{
                
                //
                ZStack{
                    Group{
                        
                        ScrollView{
                            Spacer().frame(width:geometry.size.width,height: geometry.size.height*0.13)
                            Group{
                                VStack{
                                    
                                    
                                    Group{
                                        VStack(){
                                            
                                            Spacer()
                                                .frame(height: geometry.size.height*0.05)
                                            
                                            
                                            
                                            Text("Keep your vehicles clean and shiny.").lineLimit(nil) .foregroundColor(.white)
                                                .font(.custom("Regular_Font".localized(), size: 34))
                                                .frame(height:150)
                                                .padding(.trailing,100).padding(.leading,30)
                                            
                                            
                                            HStack{
                                                
                                                Text("Select Your Location")
                                                    //                                            Text(self.item)
                                                    
                                                    .padding(.leading, 35)
                                                    //.padding(.top, 1.0)
                                                    
                                                    .foregroundColor(Color("darkthemeletter"))
                                                    .font(.custom("Bold_Font".localized(), size: 15))
                                                
                                                
                                                Spacer()
                                            }.hidden()
                                            
                                            
                                            HStack{
                                                Spacer().frame(width:15)
                                                
                                                
                                                NavigationLink(destination : FindLocation(CalculateETA: .constant(false)) ,isActive: self.$goToMap ){
                                                    
                                                    EmptyView()
                                                }
                                                
                                                
                                                
                                                HStack{
                                                    
                                                    
                                                    HStack{
                                                        
                                                        
                                                        HStack{
                                                            Image("placeholder")
                                                                .resizable().renderingMode(.template).foregroundColor(Color(#colorLiteral(red: 0, green: 0.2509999871, blue: 0.4979999959, alpha: 1)))
                                                                .frame(width: 18, height: 18)
                                                            Text(UserDefaults.standard.string(forKey: "PLACE") ?? "Please select your location".localized() )
                                                                .foregroundColor(UserDefaults.standard.string(forKey: "PLACE") == nil ? self.placedisplay ? Color.red : Color.gray:  Color("darkthemeletter"))
                                                                
                                                                .font(.custom("Regular_Font".localized(), size: 11))
                                                                .padding(.leading,5)
                                                                .padding(.trailing,10)
                                                            
                                                            Spacer()
                                                            
                                                            
                                                            
                                                            
                                                        }
                                                        .padding(.leading, 10)
                                                        .padding(.vertical,10.0)
                                                        
                                                        .frame(height:40)
                                                        
                                                        .background(Color.white)
                                                        //                                                            }
                                                        
                                                        
                                                    }
                                                    .onTapGesture {
                                                        self.locationManager.getLocationName()
                                                        withAnimation{
                                                            
                                                            self.OpenPartialSheet = true
                                                            
                                                            
                                                            
                                                        }
                                                    }
                                                    HStack{
                                                        
                                                        Button(action:{
                                                            self.placedisplay = false
                                                            //                                                                self.cancelTimer()
                                                            if( UserDefaults.standard.string(forKey: "PLACE") != nil){
                                                                self.openSP = true
                                                                print(UserDefaults.standard.string(forKey: "Aid") ?? "0")
                                                                UserDefaults.standard.set("Choose Vehicle", forKey: "Vtype" )
                                                            }else{
                                                                self.placedisplay = true
                                                            }
                                                            
                                                            
                                                            
                                                            
                                                        }){
                                                            
                                                            
                                                            Text("ORDER NOW").padding(10).padding(.horizontal,10).frame(height:40 ).background(Color("h4")) .font(.custom("Regular_Font".localized(), size: 14))
                                                                .foregroundColor(Color.white)
                                                        }
                                                    }
                                                    
                                                }.cornerRadius(30)
                                                
                                                Spacer().frame(width:15)
                                            }.frame(height:35)
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                        }
                                        
                                        .frame(width:geometry.size.width)
                                        
                                        Spacer().frame(height:25)
                                        
                                        VStack{
                                            
                                            
                                            
                                            
                                            if(self.YouHave1 && self.YouHave2){
                                                Spacer().frame(height:30)
                                                ZStack{
                                                    
                                                    
                                                    HStack{
                                                        Text("You have not booked any service yet. please Selected service to get started").font(.custom("Regular_Font".localized(), size: 13)).multilineTextAlignment(.leading).foregroundColor(Color("darkthemeletter")).padding()
                                                        
                                                        Image("Image 3")
                                                        
                                                        Spacer()
                                                    }
                                                    
                                                } .frame(width: geometry.size.width-20,height: 100)
                                                
                                                .background(Color.white)
                                                
                                                .cornerRadius(10)
                                                .clipped().shadow(radius: 5)
                                                
                                            }else{
                                                VStack(spacing:0){
                                                    HStack{
                                                        if(self.ActiveOrderID.count>0){
                                                            Button(action:{
                                                                withAnimation{
                                                                    self.DisplayActive = true
                                                                }
                                                            }){
                                                                VStack(alignment: .leading, spacing:2){
                                                                    Text("Active Orders") .font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(self.DisplayActive ? Color.black:Color.gray)
                                                                    
                                                                    
                                                                }    .padding(.horizontal,15).padding(.top,15)
                                                                
                                                            }
                                                            
                                                            Spacer()
                                                        }
                                                        if(self.PastOrders.count>0){
                                                            Button(action: {
                                                                withAnimation{
                                                                    self.DisplayActive = false
                                                                }
                                                            }){
                                                                VStack(alignment: .trailing, spacing:2){
                                                                    Text("Past Orders") .font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(self.DisplayActive && self.ActiveOrderID.count>0 ? Color.gray:Color.black)
                                                                    
                                                                }.padding(.horizontal,15).padding(.top,15)
                                                                
                                                            }
                                                        }
                                                        
                                                        if(self.ActiveOrderID.count == 0){
                                                            Spacer()
                                                        }
                                                        
                                                    }
                                                    
                                                    HStack{
                                                        
                                                        if(!self.DisplayActive && self.ActiveOrderID.count > 0){
                                                            Spacer()
                                                        }
                                                        RoundedRectangle(cornerRadius: 10).frame(width:30,height:2 ).background(Color("c1"))
                                                        
                                                        if(self.DisplayActive || self.ActiveOrderID.count == 0){
                                                            Spacer()
                                                        }
                                                        
                                                    }.padding(.horizontal,20)
                                                    
                                                }
                                                
                                                
                                                if(self.DisplayActive && self.ActiveOrderID.count>0){
                                                    
                                                    
                                                    VStack{
                                                        
                                                        ForEach(self.ActiveOrderID.reversed(),id: \.self){ orderid in
                                                            TrackServiceStatus(goToTrack: self.$goToTrack,OrderID:orderid,ViewTrackButton:true, SOrderID: self.$selectedOrderID).environment(\.colorScheme, .light)
                                                            
                                                        }
                                                    }
                                                    
                                                }
                                                else{
                                                    
                                                    
                                                    
                                                    ScrollView(.horizontal ,showsIndicators: false){
                                                        HStack(alignment:.center,spacing: 0){
                                                            
                                                            ForEach(self.PastOrders.reversed().prefix(5),id: \.self){ item in
                                                                
                                                                PastOrderView(item: item, ETA: self.$ETA, Output: self.$Output, SuccessETA: self.$SuccessETA, OpenCart: self.$OpenCart, selectedOrderID: self.$selectedOrderID, Stage: self.$Stage, selecteditem: self.$item, OpenReorderSheet: self.$OpenReorderSheet, Driver: self.$Driver).frame(width:300,height:111).background(Color.white).cornerRadius(10).clipped().shadow(radius: 5).padding(.vertical,15).padding(.horizontal,10)
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    .frame(width:300,height:111).background(Color.white).cornerRadius(10).clipped().shadow(radius: 5).padding(.vertical,15).padding(.horizontal,10)
                                                                
                                                                
                                                                
                                                            }
                                                            
                                                        }.environment(\.layoutDirection, .leftToRight )
                                                    }.padding(.horizontal,2)
                                                    
                                                    
                                                    
                                                }
                                                
                                            }
                                            
                                            
                                            
                                            HStack{
                                                
                                                VStack(alignment: .leading, spacing:2){
                                                    Text("Offers") .font(.custom("Regular_Font".localized(), size: 16)).foregroundColor( Color.black)
                                                    
                                                    RoundedRectangle(cornerRadius: 10).frame(width:30,height:2 ).background(Color("c1")).padding(.leading,5)
                                                    
                                                    
                                                    
                                                }
                                                
                                                
                                                Spacer()
                                            }.padding(.horizontal,15)
                                            
                                            //
                                            //                                        VStack{
                                            //                                            ImageCarouselView(numberOfImages: self.Offers.count) {
                                            //                                                ForEach(self.Offers,id: \.self){ i in
                                            //                                                    CardView(Offer: i)
                                            //
                                            //                                                        .frame(width: geometry.size.width)
                                            //                                                        .clipped()
                                            //                                                }
                                            //                                            }
                                            //                                        }.frame(height:(UIScreen.main.bounds.width-30)/2 + 50 )
                                            
                                            ViewPager(data:self.$Offers)
                                                .frame(height:230)
                                            //                                                                          VStack{
                                            //                                                                          Pager(page: self.$page,
                                            //                                                                               data: self.Offers,
                                            //                                                                               id: \.self,
                                            //                                                                               content: { i in
                                            //                                                                                  CardView(Offer: i).frame(width: UIScreen.main.bounds.width-30,height:(UIScreen.main.bounds.width-30)/2 ).onAppear{
                                            //                                                                                      self.timer.connect()
                                            //                                                                                  }
                                            //                                                                                   // create a page based on the data passed
                                            //                                      //                                             Text("Page: \(index)")
                                            //
                                            //                                                                          })
                                            //
                                            //                                      //                                    .horizontal(.leftToRight)
                                            //                                                                              .itemSpacing(0)
                                            //                                                                              .interactive(0.8)
                                            //                                                                              .preferredItemSize(CGSize(width: UIScreen.main.bounds.width-30,height:(UIScreen.main.bounds.width-30)/2 ))
                                            //                                                                          .loopPages()
                                            //                                                                              .pagingPriority(.high)
                                            //                                                                              .frame(height:30+(UIScreen.main.bounds.width-30)/2)
                                            //
                                            //                                                                          .simultaneousGesture(
                                            //                                                                                                                                              DragGesture()
                                            //                                                                                                                                                  .onChanged { gesture in
                                            //                                                                                                                                                      self.offset = gesture.translation
                                            //                                                                                                                                                  }
                                            //
                                            //                                                                                                                                                  .onEnded { _ in
                                            //                                                                                                                  //                                    if self.offset.width > 100 {
                                            //                                                                                                                  //                                        // remove the card
                                            //                                                                                                                  //                                                 self.SELECT -= 1
                                            //                                                                                                                  //
                                            //                                                                                                                  //                                    } else
                                            //                                                                                                                                                          if self.offset.width < -1 {
                                            //                                                                                                                                                             withAnimation{
                                            //                                                                                                                                                                                                                                                                                            self.page += 1
                                            //
                                            //
                                            //                                                                                                                                                                                                                                                                                                                                       if(self.page == self.serviceProviderFetch.Offers.count ){
                                            //                                                                                                                                                                                                                                                                                                                                           self.page = 0
                                            //                                                                                                                                                                                                                                                                                                                                       }
                                            //                                                                                                                                                                                                                                                                                            }
                                            //
                                            //                                                                                                                                                      }else if(self.offset.width > 1) {
                                            //
                                            //
                                            //
                                            //
                                            //                                                                                                                                                             withAnimation{
                                            //                                                                                                                                                                                                                                                                                          self.page -= 1
                                            //
                                            //
                                            //                                                                                                                                                                                                                                                                                                                                     if(self.page == 0 ){
                                            //                                                                                                                                                                                                                                                                                                                                         self.page = self.serviceProviderFetch.Offers.count
                                            //                                                                                                                                                                                                                                                                                                                                     }
                                            //                                                                                                                                                                                                                                                                                          }
                                            //                                                                                                                                                                  print(self.page)
                                            //                                                                                                                                                      }
                                            //                                                                                                                                                  }
                                            //                                                                                                                                          )
                                            //                                                                                     .highPriorityGesture(
                                            //                                                                                                          DragGesture()
                                            //                                                                                                              .onChanged { gesture in
                                            //                                                                                                                  self.offset = gesture.translation
                                            //                                                                                                              }
                                            //
                                            //                                                                                                              .onEnded { _ in
                                            //                                                                              //                                    if self.offset.width > 100 {
                                            //                                                                              //                                        // remove the card
                                            //                                                                              //                                                 self.SELECT -= 1
                                            //                                                                              //
                                            //                                                                              //                                    } else
                                            //                                                                                                                      if self.offset.width < -1 {
                                            //                                                                                                                         withAnimation{
                                            //                                                                                                                                                                                                                                                        self.page += 1
                                            //
                                            //
                                            //                                                                                                                                                                                                                                                                                                   if(self.page == self.serviceProviderFetch.Offers.count ){
                                            //                                                                                                                                                                                                                                                                                                       self.page = 0
                                            //                                                                                                                                                                                                                                                                                                   }
                                            //                                                                                                                                                                                                                                                        }
                                            //
                                            //                                                                                                                  }else if(self.offset.width > 1) {
                                            //
                                            //
                                            //
                                            //
                                            //                                                                                                                         withAnimation{
                                            //                                                                                                                                                                                                                                                      self.page -= 1
                                            //
                                            //
                                            //                                                                                                                                                                                                                                                                                                 if(self.page == 0 ){
                                            //                                                                                                                                                                                                                                                                                                     self.page = self.serviceProviderFetch.Offers.count
                                            //                                                                                                                                                                                                                                                                                                 }
                                            //                                                                                                                                                                                                                                                      }
                                            //                                                                                                                              print(self.page)
                                            //                                                                                                                  }
                                            //                                                                                                              }
                                            //                                                                                                      )
                                            //                                                                                 .onReceive(self.timer) { _ in
                                            //
                                            //
                                            //                                                                                                                                                                      withAnimation{
                                            //                                                                                                                                                                      self.page += 1
                                            //
                                            //
                                            //                                                                                                                                                                                                                 if(self.page == self.serviceProviderFetch.Offers.count ){
                                            //                                                                                                                                                                                                                     self.page = 0                                                                                                                                                                           }
                                            //                                                                                                                                                                      }
                                            //
                                            //
                                            //                                                                                                                                                                      // Go to the next image. If this is the last image, go
                                            //                                                                                                                                                                      // back to the image #0
                                            //
                                            //
                                            //
                                            //                                                                              //                                                                                        if(self.count >= CGFloat(CFloat(self.serviceProviderFetch.Offers.count/2))){
                                            //                                                                              //                                                                                            self.count =  -1*self.count
                                            //                                                                              //                                                                                        }else{                                                                                self.count += 1
                                            //                                                                              //                                                                                        }
                                            //                                                                              //
                                            //                                                                              //
                                            //                                                                              //
                                            //                                                                              //                                                                                        print(self.count,CGFloat(self.serviceProviderFetch.Offers.count))
                                            //                                                                              //                                                                                        self.x = -((self.screen + 15) * self.count)
                                            //                                                                                                                                                                      //self.activeImageIndex =  self.serviceProviderFetch.Offers.count-1
                                            //
                                            //                                                                                                                                                                      ////
                                            //                                                                                                                                                                      //                                        self.activeImageIndex =  Int.random(in: 0..<self.serviceProviderFetch.Offers.count)
                                            //                                                                                                                                                                  }
                                            //
                                            //
                                            //
                                            //                                                                          }.frame(height:(UIScreen.main.bounds.width-30)/2 )
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            //                                                                          ScrollView(.horizontal, showsIndicators: false){
                                            //                                                                                                                      HStack(spacing: 15){
                                            //
                                            //
                                            //
                                            //                                                                                                                          ForEach(self.serviceProviderFetch.Offers.prefix(1),id:\.self){i in
                                            //
                                            //                                                                                                                              CardView(Offer: i)
                                            //                                      //                                                                                            .offset(x:CGFloat (CGFloat(self.x) * -285))
                                            //                                                                                                                                  .contextMenu{
                                            //                                                                                                                                  //                                            ////
                                            //                                                                                                                                                                                                                              Button(action:{
                                            //                                                                                                                                                                                                                                                                                let pasteboard = UIPasteboard.general
                                            //                                                                                                                                                                                                                                                                                     pasteboard.string = i.OfferCode
                                            //                                                                                                                                                                                                                              }){
                                            //                                                                                                                                                                                                                           Text("Copy To Clipboard")
                                            //                                                                                                                                                                                                                   }
                                            //                                                                                                                              }
                                            //                                                                                                                                  //                                            ////                                                                                      }
                                            //                                      //                                                                                            .highPriorityGesture(DragGesture()
                                            //                                      //
                                            //                                      //                                                                                                .onChanged({ (value) in
                                            //                                      //
                                            //                                      //                                                                                                    if value.translation.width > 0{
                                            //                                      //
                                            //                                      //                                                                                                        self.x = value.location.x
                                            //                                      //                                                                                                    }
                                            //                                      //                                                                                                    else{
                                            //                                      //
                                            //                                      //                                                                                                        self.x = value.location.x - self.screen
                                            //                                      //                                                                                                    }
                                            //                                      //
                                            //                                      //                                                                                                })
                                            //                                      //                                                                                                .onEnded({ (value) in
                                            //                                      //
                                            //                                      //                                                                                                    if value.translation.width > 0{
                                            //                                      //
                                            //                                      //
                                            //                                      //                                                                                                        //                                                                          if value.translation.width > ((self.screen - 80) / 2) && Int(self.count) != 0{
                                            //                                      //
                                            //                                      //                                                                                                        if(self.count >= -1*CGFloat(CFloat(self.serviceProviderFetch.Offers.count/2))){
                                            //                                      //                                                                                                            self.count =  -1*self.count
                                            //                                      //                                                                                                        }else{                                                                                self.count -= 1
                                            //                                      //                                                                                                        }
                                            //                                      //
                                            //                                      //
                                            //                                      //
                                            //                                      //                                                                                                        //                                                                              self.updateHeight(value: Int(self.count))
                                            //                                      //
                                            //                                      //                                                                                                        self.x = -((self.screen + 15) * self.count)
                                            //                                      //                                                                                                        //                                                                          }
                                            //                                      //                                                                                                        //                                                                          else{
                                            //                                      //                                                                                                        //
                                            //                                      //                                                                                                        //                                                                              self.x = -((self.screen + 15) * self.count)
                                            //                                      //                                                                                                        //                                                                          }
                                            //                                      //                                                                                                    }
                                            //                                      //                                                                                                    else{
                                            //                                      //
                                            //                                      //
                                            //                                      //                                                                                                        //                                                                          if -value.translation.width > ((self.screen - 80) / 2) && Int(self.count) !=  (self.serviceProviderFetch.Offers.count - 1){
                                            //                                      //
                                            //                                      //
                                            //                                      //
                                            //                                      //                                                                                                        if(self.count >= CGFloat(CFloat(self.serviceProviderFetch.Offers.count/2))){
                                            //                                      //                                                                                                            self.count =  -1*self.count
                                            //                                      //                                                                                                        }else{                                                                                self.count += 1
                                            //                                      //                                                                                                        }
                                            //                                      //
                                            //                                      //
                                            //                                      //                                                                                                        //                                                                              self.updateHeight(value: Int(self.count))
                                            //                                      //                                                                                                        self.x = -((self.screen + 15) * self.count)
                                            //                                      //                                                                                                        //                                                                          }
                                            //                                      //                                                                                                        //                                                                          else{
                                            //                                      //                                                                                                        //
                                            //                                      //                                                                                                        //                                                                              self.x = -((self.screen + 15) * self.count)
                                            //                                      //                                                                                                        //                                                                          }
                                            //                                      //                                                                                                    }
                                            //                                      //                                                                                                })
                                            //                                      //                                                                                        )
                                            //                                                                                                                          }                                                .onReceive(self.timer) { _ in
                                            //
                                            //                                                                                                                              withAnimation{
                                            //                                                                                                                              self.x += 1
                                            //
                                            //                                                                                                                              if(self.x == CGFloat( self.serviceProviderFetch.Offers.count-1)){
                                            //                                                                                                                                  self.x = 0
                                            //                                                                                                                                  }
                                            //                                                                                                                              }
                                            //
                                            //                                                                                                                              // Go to the next image. If this is the last image, go
                                            //                                                                                                                              // back to the image #0
                                            //
                                            //
                                            //
                                            //                                      //                                                                                        if(self.count >= CGFloat(CFloat(self.serviceProviderFetch.Offers.count/2))){
                                            //                                      //                                                                                            self.count =  -1*self.count
                                            //                                      //                                                                                        }else{                                                                                self.count += 1
                                            //                                      //                                                                                        }
                                            //                                      //
                                            //                                      //
                                            //                                      //
                                            //                                      //                                                                                        print(self.count,CGFloat(self.serviceProviderFetch.Offers.count))
                                            //                                      //                                                                                        self.x = -((self.screen + 15) * self.count)
                                            //                                                                                                                              //self.activeImageIndex =  self.serviceProviderFetch.Offers.count-1
                                            //
                                            //                                                                                                                              ////
                                            //                                                                                                                              //                                        self.activeImageIndex =  Int.random(in: 0..<self.serviceProviderFetch.Offers.count)
                                            //                                                                                                                          }
                                            //
                                            //                                                                                                                      }
                                            //                                                                                                                      .frame(width: UIScreen.main.bounds.width)
                                            //                                                                                                                          //                                                      .offset(x: self.op)
                                            //                                                                                                                          .padding(.horizontal,100)
                                            //                                                                                                                          .padding(.vertical,20)
                                            //
                                            //                                                                          }    .frame(height:135 )
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            if(Bool(Auth.auth().currentUser?.isAnonymous ?? false) ){
                                                HStack{
                                                    Spacer().frame(width:15)
                                                    
                                                    Button(action:{
                                                        self.OpenLoginPage = true
                                                        
                                                    }){
                                                        VStack{
                                                            HStack{
                                                                Spacer()
                                                                Image(systemName: "person").resizable().frame(width:16,height:20)
                                                                    .foregroundColor(Color.white).padding(12).background(Color("c1")
                                                                    ).clipShape(Circle())
                                                                
                                                                
                                                                Text("Login or Create an account to get your service faster and better offers.").frame(height:40)
                                                                    .lineLimit(3).font(.custom("Regular_Font".localized(), size: 13.5))
                                                                    .padding()
                                                            }.background(Color("h7"))
                                                            Text("Login").font(.custom("Regular_Font".localized(), size: 17)).padding(5)
                                                                .padding(.bottom,10)
                                                        }
                                                        
                                                    }.background(Color.white).clipped().cornerRadius(10).shadow(radius: 5)
                                                    Spacer().frame(width:15)
                                                }.padding(.vertical,15)
                                                
                                            }
                                            
                                            
                                            
                                            
                                            Spacer().frame(width:50)
                                        }.background(RoundedCorners(color: Color("h1"), tl: 30, tr: 0, bl: 0, br: 30))
                                        
                                        
                                    }
                                    
                                }
                                
                            }
                            
                            .blur(radius: self.ALERT2 ? 1 : 0)
                            
                            
                            . onTapGesture {
                                
                                
                                
                                self.menuOpen = false
                                
                            }
                        }.background(LinearGradient(gradient: Gradient(colors: [Color("h3"),Color("h2")]), startPoint: .leading, endPoint: .trailing))
                        
                        
                        
                        
                    }
                    .blur(radius: self.OpenPartialSheet || self.OpenReorderSheet ? 2 : 0)
                    .brightness(self.OpenPartialSheet ? -0.2 : 0)
                    .disabled(self.OpenPartialSheet || self.OpenReorderSheet)
                    
                    .brightness(self.ALERT2 ? -0.5 : 0)
                    .gesture(
                        DragGesture(minimumDistance: 3)
                            .onEnded { _ in
                                if(self.OpenPartialSheet){
                                    self.OpenPartialSheet = false
                                }
                                
                            }
                    )
                    
                    
                }
            }.onAppear{
                self.ref.child("service_providers").observeSingleEvent(of: DataEventType.value, with: {
                    
                    (A) in
                    
                    
                    
                    self.Offers.removeAll()
                    
                    
                    for B in A.children {
                        let snapshot = B as! DataSnapshot
                        
                        for Offers in snapshot.childSnapshot(forPath: "offers").children {
                            let snap = Offers as! DataSnapshot
                            let O : Offer = Offer(id: Date().timeIntervalSince1970 , OfferImage: String(describing :  (snap.childSnapshot(forPath:"image").value)!), ServiceProviderID: snapshot.key,OfferCode:String(describing :  (snap.childSnapshot(forPath:"promo_code").value)!),Desc:String(describing :  (snap.childSnapshot(forPath:"description".localized()).value)!),TC:String(describing :  (snap.childSnapshot(forPath:"terms_and_conditions".localized()).value )!)
                                                  ,Percentage:String(describing :  (snap.childSnapshot(forPath:"percentage").value )!),OfferName: snap.key, show: false)
                            self.Offers.append(O)
                            
                            
                            
                        }
                        
                        
                        
                        
                    }
                    
                    
                    
                    
                    
                })
            }
            //                    .edgesIgnoringSafeArea(.top)
            
        }
        
    }
}

struct CardView : View {
    
    var Offer : Offer
    
    var body : some View{
        
        //            VStack(alignment: .leading, spacing: 0){
        
        
        
        
        AnimatedImage(url:URL(string:Offer.OfferImage))
            .resizable()
            
            //                Text(Offer.OfferCode)
            //                    .fontWeight(.bold)
            //                    .padding(.vertical, 13)
            //                    .padding(.leading)
            
            //            }
            .frame(width: UIScreen.main.bounds.width-30,height:(UIScreen.main.bounds.width-30)/2 )
            .background(Color.white)
            .cornerRadius(10)
            .clipped()
            .shadow(radius: 10)
        
    }
}
func getInt(Status:String) -> Int {
    
    let V : Int = Int(Status) ?? 0
    return V
}

struct RotationAnimation: View {
    
    var image:String
    @State var timer3: Timer.TimerPublisher = Timer.publish (every: 0.5, on: .main, in: .common)
    @State var rotate: Bool = false
    
    var body: some View {
        VStack{
            Image(self.image)  .renderingMode(.template).resizable()
                
                .rotationEffect(.degrees(self.rotate ? -20 : +20))
                
                //                                                        .animation( Animation.linear(duration: 1))
                //                .rotationEffect(.degrees(self.tick ? -45 : +45))
                .animation( Animation.linear(duration: 1).repeatForever())
                .onAppear{
                    //                    self.timer3.connect()
                    self.rotate = true
                }
                .frame(width:15,height:15)
            
            
            
        }
        //        .onReceive(self.timer3) { _ in
        //            withAnimation() {
        //                self.rotate.toggle()
        //            }
        //
        //        }
        
    }
}

struct ViewPager: View {
    
    
    @State var page: Int = 0
    @State var pageOffset: Int = 0
    @Binding var data : [Offer]
    
    @State var t = Timer.publish (every: 3, on: .main, in: .common).autoconnect()
    
    var body: some View {
        Pager(page: self.$page,
              data: self.data,
              id: \.self,
              content: { Offer in
                
                AnimatedImage(url:URL(string:Offer.OfferImage))
                    .resizable()
                    
                    
                    //            }
                    .frame(width: UIScreen.main.bounds.width-30,height:(UIScreen.main.bounds.width-30)/2 )
                    .background(Color.white)
                    .cornerRadius(10)
                    .clipped()
                    .shadow(radius: 10).frame(width: UIScreen.main.bounds.width-30,height:(UIScreen.main.bounds.width-30)/2 )
                
                // create a page based on the data passed
                //                                             Text("Page: \(index)")
                
              }).loopPages() .horizontal(.leftToRight)
            .itemSpacing(0)
            .interactive(0.8)
            
            .onReceive(self.t) { _ in
                //        self.page += 1
                print("JJJJJJ")
                //        print(self.t.interval)
                //  self.page += 1
                if(self.page >= self.data.count){
                    self.page = 0
                    
                }else{
                    self.page += 1
                }
            }
    }
}
