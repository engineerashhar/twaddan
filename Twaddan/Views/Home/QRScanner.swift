//
//  QRScanner.swift
//  Twaddan
//
//  Created by Spine on 23/06/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import FirebaseDatabase
//import CodeScanner
import MapKit

struct QRScanner: View {
    
    
    @Binding  var isShowingScanner : Bool
    @State var ViewLoader: Bool = false
    
    @Binding var ALERT2 : Bool
    @Binding var AlertMessege2 :String
    @Binding var GoToBookDetails : Bool
    @Binding var SelectedDriver : String
    @Binding var ETA : Double
    @Binding var OpenCart : Bool
    @Binding var Snap : DataSnapshot
    
    let ref = Database.database().reference()
    
    @ObservedObject var findOffers  = FindOffer()
    @EnvironmentObject var settings : UserSettings
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    
    
    
    
    
    var body: some View {
        
        GeometryReader{ geometry in
            
            
            
            VStack{
                
                Spacer().frame(height:geometry.size.height*0.05)
                
                //                HStack{
                //
                //                    Button(action : {
                //
                //
                //
                //
                //                        self.presentationMode.wrappedValue.dismiss()
                //
                //
                //                    }) {
                //                        Image("back").frame(width:25,height:20 )
                //
                //
                //
                //                    }
                //                    .padding(.leading, 25.0)
                //                    Spacer()
                //                    Text("QR Scanner").font(.custom("Regular_Font".localized(), size: 15))
                //                    Spacer()
                //
                //                    Button(action: {}){
                //                        ZStack{ Image("bell").resizable().frame(width: 25, height: 30)
                //
                //                            Text("8")
                //                                .frame(width: 20.0, height: 20.0)
                //                                .background(Circle().fill(Color.red))
                //                                .foregroundColor(.white)
                //                                .padding([.leading, .bottom])
                //
                //
                //
                //
                //                        }.hidden()
                //
                //                    }
                //                    .padding(.trailing, 15.0)
                //
                //                    Button(action: {
                //
                //
                //                    }){
                //                        Image("sortandfilterpng")
                //                            .resizable().frame(width: 25, height: 25).hidden()
                //
                //                    }
                //                    .padding(.trailing, 20)
                //
                //
                //                }
                
                
                CodeScan(codeTypes: [.qr]) { result in
                    //                    self.handleScan(result: result)
                    //                    result.success(let code);
                    //                    self.publishETA(code:code)
                    //                
                    if(result != ""){
                        print(self.findOffers.VehicleDriverLink)
                        print(result)
                        let items = result.components(separatedBy: "-")
                        if(items.count>2){
                            
                            var O =  self.findOffers.VehicleDriverLink.filter {
                                $0.VehicleNumber.contains(items[1])
                            }
                            
                            
                            //                        print(O,O.count)
                            
                            if(O.count<1){
                                self.AlertMessege2 = "Unknown QR Code"
                                
                                self.ALERT2 = true
                                //                              self.presentationMode.wrappedValue.dismiss()
                                
                            }else{
                                self.ALERT2 = false
                                self.publishETA(code:O[0].DriverID)
                                //                                                          self.presentationMode.wrappedValue.dismiss()
                            }
                        }else{
                            self.AlertMessege2 = "Unknown QR Code"
                            
                            self.ALERT2 = true
                            
                        }
                        
                        
                    }else{
                        self.AlertMessege2 = "Unknown QR Code"
                        
                        self.ALERT2 = true
                    }
                    
                }
                
                
                
                
                //.padding(.top, 50)
                Spacer()
            }
            
            .foregroundColor(Color("ManBackColor"))
            
            
            //            Text("")
            
            //            CodeScan(codeTypes: [.qr], simulatedData: "", completion: self.handleScan)
            //            CodeScan(codeTypes: [.qr], completion: self.handleScan)
            
            
            //
            //            NavigationLink(destination: Cart(GoToBookDetailsOld: .constant(false))
            //
            //                                                        .environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
            //        )), isActive: self.$OpenCart){
            //
            //                                                                                           Text("").frame(height:0)
            //                                                                   }
            
            
        }
        .navigationBarTitle("QR Scanner",displayMode: .inline)
        .navigationBarHidden(true)
        .edgesIgnoringSafeArea(.all)
        //        }
        //        .sheet(isPresented: self.$GoToBookDetails){
        //
        //
        //
        //            BookingDetails(OpenCart: self.$OpenCart,GoToBookDetails:self.$GoToBookDetails ,serviceSnap: self.Snap, SelectedDriver:
        //                self.SelectedDriver,ETA: self.ETA).environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
        //        ))
        //            
        //            
        //
        //        }
    }
    
    
    //
    //        func sp_data_func(sp_data:DataSnapshot) {
    //
    //
    //
    //                                        if(sp_data.childSnapshot(forPath: "drivers_info/active_drivers_id").childrenCount == 0){
    //
    //
    //
    //                                                                                                    var HighPercentage = 0.0
    //                                                                                                    for O in sp_data.childSnapshot(forPath: "offers").children {
    //                                                                                                        let Offers = O as! DataSnapshot
    //                                                                                                        print("WWWWWW",Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value!))) ?? 0.0)
    //                                                                                                        if(Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value!))) ?? 0.0 > HighPercentage){
    //
    //                                                                                                            HighPercentage = Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value! ))) ?? 0.0
    //                                                                                                        }
    //
    //                                                                                                    }
    //
    //                                                                                                    let S = sp_data.childSnapshot(forPath: "personal_information/name".localized()).value
    //
    //                                            //                                                        print("personal_information/name"))
    //
    //                                                                                                    let service :ServiceProvider
    //
    //                                                                                                        = ServiceProvider(id:sp_data.key,
    //                                                                                                                          serviceSnap:sp_data ,
    //                                                                                                                          serviceName: String(describing :  (sp_data.childSnapshot(forPath:"personal_information/name".localized()).value) ?? " "),
    //                                                                                                                          serviceDesc: String(describing :  (sp_data.childSnapshot(forPath:"personal_information/name".localized()).value ) ?? " "),
    //                                                                                                                          Servicerating: String(String(describing :  (sp_data.childSnapshot(forPath:"personal_information/ratings").value)!).prefix(3)),
    //                                                                                                                          Servicetime: 9999999,
    //                                                                                                                          ServiceImage:  String(describing :  (sp_data.childSnapshot(forPath:"personal_information/image").value) ?? " "), DriverID: "driversnap.key", Offer: HighPercentage)
    //
    //
    //
    //                                                                                                                               self.services.append(service)
    //                                                                                                                                   self.services.sort {
    //
    //
    //                                                                                                                                       $0.Servicetime < $1.Servicetime
    //                                                                                                                                   }
    //                                            let reduce = self.services.reduce(into:[:],{ $0[$1, default:0] += 1})
    //                                                 let sorted = reduce.sorted(by: {$0.value > $1.value})
    //                                                 let map = sorted.map({$0.key})
    //                                                 self.services = map
    //                                            self.services.sort {
    //
    //
    //                                                $0.Servicetime < $1.Servicetime
    //                                            }
    //
    //
    //
    //                                        }
    //
    //                                        for dd in sp_data.childSnapshot(forPath: "drivers_info/active_drivers_id").children {
    //                                                            let dd = dd as! DataSnapshot
    //
    //
    //
    //                                            self.ref.child("drivers").child(dd.key).observeSingleEvent(of: DataEventType.value) { (driversnap) in
    //
    //
    //
    //                                                            var driverlat =
    //                                                                                   25.0
    //
    //                                                             var driverlon = 55.635908
    //                                                            var lastETA : Double = 0
    //
    //                                                            if(String(describing:  driversnap.childSnapshot(forPath: "live_location/time_driver_engaged").value!) != "0"){
    //
    //                                                                lastETA = Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/time_driver_engaged").value)!)) ?? 0
    //
    //
    //
    //
    //
    //
    //                                                                driverlat =
    //                                                                                        Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/lc_lat").value)!)) ?? 25.0
    //
    //                                                                                    //  }
    //
    //
    //
    //                                                                                    //   if(driversnap.childSnapshot(forPath:"live_location/longitude").exists()){
    //                                                                                    driverlon =
    //                                                                                        Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/lc_lng").value)!)) ?? 55
    //
    //
    //                                                            }else{
    //
    //                                                                  print("QQQQQQQQQQ2",driversnap.key)
    //                                                                driverlat =
    //                                                                                        Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/latitude").value)!)) ?? 25.0
    //
    //                                                                                    //  }
    //
    //
    //
    //                                                                                    //   if(driversnap.childSnapshot(forPath:"live_location/longitude").exists()){
    //                                                                                    driverlon =
    //                                                                                        Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/longitude").value)!)) ?? 55
    //
    //                                                            }
    //
    //                                                            print("cccccc","3")
    //                                                              print("QQQQQQQQQQ","3")
    //
    //                                                            if(String(describing :  (driversnap.childSnapshot(forPath:"status").value)!) == "1"){
    //            //                                                      print("QQQQQQQQQQ4",driversnap.key)
    //                                                                //                            print("PPPPPPPPPP",driversnap.key)
    //                                                                //        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5)) {
    //
    //
    //
    //                                                                //  if(driversnap.childSnapshot(forPath:"live_location/latitude") != nil){
    //                                        //
    //
    //                                                                //  }
    //                                                                var driverloc : CLLocation = CLLocation(latitude: driverlat, longitude: driverlon)
    //                                                                var driver2D : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: driverlat, longitude: driverlon)
    //
    //                                                                let userlat = UserDefaults.standard.double(forKey: "latitude")
    //                                                                let userlon = UserDefaults.standard.double(forKey: "longitude")
    //
    //
    //                                                                var userloc : CLLocation = CLLocation(latitude: userlat, longitude: userlon)
    //                                                                var user2D  : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: userlat, longitude: userlon)
    //
    //
    //                                                                //ETA USING APPLE MAP
    //                                                                //
    //                                                                //                        let request = MKDirections.Request()
    //                                                                //                 request.source = MKMapItem(placemark:  MKPlacemark(coordinate: driver2D))
    //                                                                //                 request.destination = MKMapItem(placemark:  MKPlacemark(coordinate: user2D))
    //                                                                //
    //                                                                //                 request.requestsAlternateRoutes = false
    //
    //                                                                //   let directions = MKDirections(request: request)
    //                                                                //                    directions.calculateETA { (responds, error) in
    //                                                                //                         if error != nil {
    //                                                                //
    //                                                                //                                        } else {
    //                                                                //                            var ETA  =    responds?.expectedTravelTime
    //                                                                //
    //                                                                //
    //                                                                //
    //                                                                //                        }
    //                                                                //                    }
    //                                                                //                        directions.calculateETA{ response, error in
    //                                                                //
    //                                                                //
    //                                                                //
    //                                                                //
    //                                                                //                   //     directions.calculate { response, error in
    //                                                                //                                var eta:Double = 10000000
    //                                                                //                            if(error != nil){
    //                                                                //                        //                            }else{
    //                                                                //
    //                                                                //                                    eta = response?.expectedTravelTime as! Double
    //
    //
    //                                                                var eta:Double = 10000000
    //                                                                //   eta = getETA(from: user2D, to: driver2D)
    //
    //                                                                //                        let routes = response?.routes
    //
    //
    //
    //
    //                                                                //                        if(routes != nil){
    //                                                                //                        let selectedRoute = routes![0]
    //                                                                //                        let distance = selectedRoute.distance
    //                                                                //                         eta = selectedRoute.expectedTravelTime
    //                                                                //
    //                                                                //
    //                                                                //                        }
    //
    //
    //
    //                                                                let origin = "\(driver2D.latitude),\(driver2D.longitude)"
    //                                                                let destination = "\(user2D.latitude),\(user2D.longitude)"
    //
    //
    //
    //                                                                //guard
    //
    //                                                                let S : String =  "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=\(origin)&destinations=\(destination)&mode=driving&key=AIzaSyCYQeItZ7SVEQ24Grjzc3zA1B_2DU_GfPU"
    //
    //                                                                let url = URL(string:S
    //                                                                )
    //                                                                //        else {
    //                                                                //        let error = NSError(domain: "LocalDomain", code: 0, userInfo: [NSLocalizedDescriptionKey: "Failed to create object URL"])
    //                                                                //
    //                                                                //        return
    //                                                                //    }
    //                                                                let config = URLSessionConfiguration.default
    //                                                                let session = URLSession(configuration: config)
    //                                                                let task = session.dataTask(with: url!, completionHandler: {
    //                                                                    (data, response, error) in
    //
    //                                                                    print("11111111",driversnap.key)
    //
    //                                                                    if error != nil {
    //                                                                        print("cccccc","5")
    //                                                                        print(error!.localizedDescription)
    //                                                                    }
    //                                                                    else {
    //                                                                        print("cccccc","6")
    //                                                                        //  print(response)
    //                                                                        do {
    //                                                                            if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
    //
    //                                                                                guard let routes = json["rows"] as? NSArray else {
    //                                                                                    DispatchQueue.main.async {
    //                                                                                    }
    //                                                                                    return
    //                                                                                }
    //                                                                                if (routes.count > 0) {
    //                                                                                    let overview_polyline = routes[0] as? NSDictionary
    //                                                                                    //   let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
    //                                                                                    //  let points = dictPolyline?.object(forKey: "points") as? String
    //
    //                                                                                    DispatchQueue.main.async {
    //
    //                                                                                        print("cccccc","7")
    //                                                                                        //
    //                                                                                        let legs = overview_polyline?["elements"] as! Array<Dictionary<String, AnyObject>>
    //
    //                                                                                        let distance = legs[0]["distance"] as? NSDictionary
    //                                                                                        let distanceValue = distance?["value"] as? Int ?? 0
    //                                                                                        let distanceDouleValue = distance?["value"] as? Double ?? 0.0
    //                                                                                        let duration = legs[0]["duration"] as? NSDictionary
    //                                                                                        let totalDurationInSeconds = duration?["value"] as? Int ?? 0
    //                                                                                        let durationDouleValue = duration?["value"] as? Double ?? 0.0
    //
    //                                                                                        print (S)
    //
    //                                                                                      eta = durationDouleValue
    //                                                                                         print("QQQQQQQQQQ1",driversnap.key ,eta,lastETA,durationDouleValue)
    //                                                                                        if((lastETA-(Date().timeIntervalSince1970)*1000) > 0){
    //                                                                                                                                           eta = eta+(((lastETA/1000)-(Date().timeIntervalSince1970)))
    //                                                                                                                                           print("WWWWW",(((lastETA/1000)-(Date().timeIntervalSince1970))/60))
    //
    //
    //                                                                                        }
    //
    //
    //
    //                                                                                                                                              eta = eta+600 ;
    //
    //                                                                                                                                        if(eta < 601){
    //                                                                                                                                            eta = 10000000
    //                                                                                                                                        }
    //
    //                                                                                          print("QQQQQQQQQQ1",driversnap.key ,eta,lastETA,durationDouleValue)
    //
    //                                                                                        print("WWWWW",lastETA,Date().timeIntervalSince1970*1000,eta+lastETA-(Date().timeIntervalSince1970)*1000,eta)
    //
    //                                        //                                                if(Msnapshot.childSnapshot(forPath: "newsp1_gmail_com" + "/services/"+"saloon").exists()){
    //                                        //                                                eta += lastETA
    //                                        //
    //                                        //                                                }
    //                                        //                                                print("cccccc","8")
    //                                        //
    //                                        //
    //                                        //                                                print("QQQQQQQQQQQQQQQQQ",eta)
    //                                        //
    //                                        //                                                print("PPPPPPPPPP",      String(describing:Msnapshot.childSnapshot(forPath: "newsp1_gmail_com" + "/services/"+"saloon").exists()))
    //                                        //
    //                                        //                                                print(getTime(timeIntervel: durationDouleValue) + " HHHHH " + String(describing:  driversnap.childSnapshot(forPath:"sp_related").value),driversnap.key)
    //
    //                                                                                        //  print(eta)
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //                                                                                                //   print("UUUUUUU","New",(driversnap.childSnapshot(forPath:"sp_related").value)!,eta)
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //                                                                                                var HighPercentage = 0.0
    //                                                                                                for O in sp_data.childSnapshot(forPath: "offers").children {
    //                                                                                                    let Offers = O as! DataSnapshot
    //                                                                                                    print("WWWWWW",Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value!))) ?? 0.0)
    //                                                                                                    if(Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value!))) ?? 0.0 > HighPercentage){
    //
    //                                                                                                        HighPercentage = Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value! ))) ?? 0.0
    //                                                                                                    }
    //
    //                                                                                                }
    //
    //                                                                                                let S = sp_data.childSnapshot(forPath: "personal_information/name".localized()).value
    //
    //                                        //                                                        print("personal_information/name"))
    //
    //                                                                                                let service :ServiceProvider
    //
    //                                                                                                    = ServiceProvider(id:sp_data.key,
    //                                                                                                                      serviceSnap:sp_data ,
    //                                                                                                                      serviceName: String(describing :  (sp_data.childSnapshot(forPath:"personal_information/name".localized()).value) ?? " "),
    //                                                                                                                      serviceDesc: String(describing :  (sp_data.childSnapshot(forPath:"personal_information/description".localized()).value ) ?? " "),
    //                                                                                                                      Servicerating: String(String(describing :  (sp_data.childSnapshot(forPath:"personal_information/ratings").value)!).prefix(3)),
    //                                                                                                                      Servicetime: eta,
    //                                                                                                                      ServiceImage:  String(describing :  (sp_data.childSnapshot(forPath:"personal_information/image").value) ?? " "), DriverID: driversnap.key, Offer: HighPercentage)
    //
    //
    //                                                                                               print("WWWWWWWWWWW",getTime(timeIntervel: eta))
    //
    //
    //
    //
    //
    //
    //
    //                                                                                      let TempService =     self.services.filter { (Sp) -> Bool in
    //                                                                                            Sp.id == service.id
    //                                                                                        }
    //
    //                                                                                        if(TempService.count>0){
    //                                                                                            if(TempService[0].Servicetime>service.Servicetime){
    //                                                                                                self.services.remove(at: self.services.firstIndex(of: TempService[0])!)
    //                                                                                                 self.services.append(service)
    //
    //                                                                                            }
    //                                                                                        }else{
    //                                                                                                self.services.append(service)
    //                                                                                        }
    //
    //
    //
    //    //                                                                                        self.services.sort {
    //    //
    //    //
    //    //                                                                                            $0.Servicetime < $1.Servicetime
    //    //                                                                                        }
    //                                                                                        let reduce = self.services.reduce(into:[:],{ $0[$1, default:0] += 1})
    //                                                                                                                                 let sorted = reduce.sorted(by: {$0.value > $1.value})
    //                                                                                                                                 let map = sorted.map({$0.key})
    //                                                                                                                                 self.services = map
    //
    //                                                                                        //                        }
    //
    //
    //                                                                                        //                        }
    //
    //                                                                                        self.services.sort {
    //
    //
    //                                                                                            $0.Servicetime < $1.Servicetime
    //                                                                                        }
    //
    //                                                                                    }
    //                                                                                }
    //                                                                                else {
    //                                                                                    print(json)
    //                                                                                    DispatchQueue.main.async {
    //                                                                                        //   SVProgressHUD.dismiss()
    //                                                                                    }
    //                                                                                }
    //                                                                            }
    //                                                                        }
    //                                                                        catch {
    //                                                                            print("error in JSONSerialization")
    //                                                                            DispatchQueue.main.async {
    //                                                                                //  SVProgressHUD.dismiss()
    //                                                                            }
    //                                                                        }
    //                                                                    }
    //                                                                })
    //                                                                task.resume()
    //
    //
    //                                                            }
    //                                        }
    //                                                        }
    //        }
    func publishETA(code:String) {
        let c = code.replacingOccurrences(of: ".", with: "").replacingOccurrences(of: "#", with: "").replacingOccurrences(of: "$", with: "")
            .replacingOccurrences(of: "[", with: "").replacingOccurrences(of: "]", with: "").replacingOccurrences(of: "/", with: "")
        self.ref.child("drivers").child(c).observeSingleEvent(of: DataEventType.value) { (driversnap) in
            
            self.ViewLoader = false
            
            if(driversnap.exists()){
                print("CVBN",code)
                
                //        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5)) {
                
                
                var driverlat =
                    25.0
                
                var driverlon = 55.635908
                var lastETA : Double = 0
                
                if(String(describing:  driversnap.childSnapshot(forPath: "live_location/time_driver_engaged").value!) != "0"){
                    
                    lastETA = Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/time_driver_engaged").value)!)) ?? 0
                    
                    
                    
                    
                    
                    
                    driverlat =
                        Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/lc_lat").value)!)) ?? 25.0
                    
                    //  }
                    
                    
                    
                    //   if(driversnap.childSnapshot(forPath:"live_location/longitude").exists()){
                    driverlon =
                        Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/lc_lng").value)!)) ?? 55
                    
                    
                }else{
                    
                    print("QQQQQQQQQQ2",driversnap.key)
                    driverlat =
                        Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/latitude").value)!)) ?? 25.0
                    
                    //  }
                    
                    
                    
                    //   if(driversnap.childSnapshot(forPath:"live_location/longitude").exists()){
                    driverlon =
                        Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/longitude").value)!)) ?? 55
                    
                }
                var driverloc : CLLocation = CLLocation(latitude: driverlat, longitude: driverlon)
                var driver2D : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: driverlat, longitude: driverlon)
                
                let userlat =
                    //                    10.874858
                    UserDefaults.standard.double(forKey: "latitude")
                let userlon =
                    //                    76.449728
                    UserDefaults.standard.double(forKey: "longitude")
                
                
                var userloc : CLLocation = CLLocation(latitude: userlat, longitude: userlon)
                var user2D  : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: userlat, longitude: userlon)
                
                
                //    var distance : CLLocationDistance = userloc.distance(from: driverloc)
                // var timeOfArrival :
                
                //                        let request = MKDirections.Request()
                //                 request.source = MKMapItem(placemark:  MKPlacemark(coordinate: driver2D))
                //                 request.destination = MKMapItem(placemark:  MKPlacemark(coordinate: user2D))
                //
                //                 request.requestsAlternateRoutes = false
                //
                //                 let directions = MKDirections(request: request)
                //    //                    directions.calculateETA { (responds, error) in
                //    //                         if error != nil {
                //    //                                            print("Error getting directions")
                //    //                                        } else {
                //    //                            var ETA  =    responds?.expectedTravelTime
                //    //
                //    //
                //    //
                //    //                        }
                //    //                    }
                //                        directions.calculateETA{ response, error in
                //
                //
                //
                //
                //                   //     directions.calculate { response, error in
                //                                var eta:Double = 10000000
                //                            if(error != nil){
                //                                print(error.debugDescription)
                //                            }else{
                //
                //                                    eta = response?.expectedTravelTime as! Double
                //
                //
                //    //                        let routes = response?.routes
                //
                //
                //
                //
                //    //                        if(routes != nil){
                //    //                        let selectedRoute = routes![0]
                //    //                        let distance = selectedRoute.distance
                //    //                         eta = selectedRoute.expectedTravelTime
                //    //
                //    //
                //    //                        }
                //                                print(eta)
                //
                //                            }
                //
                
                
                self.GoToBookDetails = true
                var eta:Double = 10000000
                
                let origin = "\(driver2D.latitude),\(driver2D.longitude)"
                let destination = "\(user2D.latitude),\(user2D.longitude)"
                
                
                let S : String =  "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=\(origin)&destinations=\(destination)&mode=driving&key=AIzaSyCYQeItZ7SVEQ24Grjzc3zA1B_2DU_GfPU"
                
                let url = URL(string:S
                )
                //        else {
                //        let error = NSError(domain: "LocalDomain", code: 0, userInfo: [NSLocalizedDescriptionKey: "Failed to create object URL"])
                //        print("Error: \(error)")
                //        return
                //    }
                let config = URLSessionConfiguration.default
                let session = URLSession(configuration: config)
                let task = session.dataTask(with: url!, completionHandler: {
                    (data, response, error) in
                    
                    print("11111111",driversnap.key)
                    
                    if error != nil {
                        print(error!.localizedDescription)
                    }
                    else {
                        
                        //  print(response)
                        do {
                            if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                                
                                guard let routes = json["rows"] as? NSArray else {
                                    DispatchQueue.main.async {
                                    }
                                    return
                                }
                                if (routes.count > 0) {
                                    let overview_polyline = routes[0] as? NSDictionary
                                    //   let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                                    //  let points = dictPolyline?.object(forKey: "points") as? String
                                    
                                    DispatchQueue.main.async {
                                        //
                                        let legs = overview_polyline?["elements"] as! Array<Dictionary<String, AnyObject>>
                                        
                                        let distance = legs[0]["distance"] as? NSDictionary
                                        let distanceValue = distance?["value"] as? Int ?? 0
                                        let distanceDouleValue = distance?["value"] as? Double ?? 0.0
                                        let duration = legs[0]["duration"] as? NSDictionary
                                        let totalDurationInSeconds = duration?["value"] as? Int ?? 0
                                        let durationDouleValue = duration?["value"] as? Double ?? 0.0
                                        
                                        print (S)
                                        
                                        eta = durationDouleValue
                                        print("QQQQQQQQQQ1",driversnap.key ,eta,lastETA,durationDouleValue)
                                        if((lastETA-(Date().timeIntervalSince1970)*1000) > 0){
                                            eta = eta+(((lastETA/1000)-(Date().timeIntervalSince1970)))
                                            print("WWWWW",(((lastETA/1000)-(Date().timeIntervalSince1970))/60))
                                            
                                            
                                        }
                                        
                                        
                                        
                                        eta = eta+300 ;
                                        
                                        if(eta < 301){
                                            eta = 10000000
                                        }
                                        
                                        print("QQQQQQQQQQ1",driversnap.key ,eta,lastETA,durationDouleValue)
                                        
                                        print("WWWWW",lastETA,Date().timeIntervalSince1970*1000,eta+lastETA-(Date().timeIntervalSince1970)*1000,eta)
                                        
                                        
                                        
                                        
                                        
                                        print("QQQQQQQQQQQQQQQQQ",eta)
                                        
                                        
                                        self.ref.child("service_providers").child(String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)).observeSingleEvent(of: DataEventType.value) { (snapshot) in
                                            
                                            
                                            
                                            
                                            self.SelectedDriver = code
                                            self.ETA = eta
                                            
                                            self.Snap = snapshot
                                            
                                            self.GoToBookDetails = true
                                            
                                            
                                            //                                    self.OProfile = true
                                            print("QQQQQQQQQQQQQQQQQ",self.GoToBookDetails)
                                            
                                            //                            }
                                        }
                                        
                                        
                                        
                                        
                                    }
                                }
                                else {
                                    print(json)
                                    DispatchQueue.main.async {
                                        //   SVProgressHUD.dismiss()
                                    }
                                }
                            }
                        }
                        catch {
                            print("error in JSONSerialization")
                            DispatchQueue.main.async {
                                //  SVProgressHUD.dismiss()
                            }
                        }
                    }
                })
                task.resume()
                
            }
            else{
                print(c)
                self.AlertMessege2 = "Unknown QR Code"
                
                self.ALERT2 = true
                
                
                
            }
            
            
        }
        
        
        
    }
    func handleScan(result: Result<String, CodeScan.ScanError>) {
        
        
        
        
        
        self.isShowingScanner = false
        self.ViewLoader = true
        // more code to come
        print(result)
        
        switch result {
        case .success(let code):
            //   let details = code.components(separatedBy: "\n")
            // guard details.count == 2 else { return }
            print("QQQQQQQQQQ")
            print(code)
            
            
            if(code != ""){
                publishETA(code:code)
            }
            
            
            
        //            let person = Prospect()
        //            person.name = details[0]
        //            person.emailAddress = details[1]
        
        //            self.prospects.people.append(person)
        case .failure(let error):
            print("Scanning failed")
        }
        
        
        
    }
}

