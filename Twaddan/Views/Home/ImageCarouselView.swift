//
//  ImageCarouselView.swift
//  Twaddan
//
//  Created by Spine on 12/09/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import Combine

// 2
struct ImageCarouselView<Content: View>: View {
    // 3
    private var numberOfImages: Int
    private var content: Content
    
    // 4
    @State private var currentIndex: Int = 0
    @State private var offset: CGSize = CGSize.zero
    
    // 5
    private let timer = Timer.publish(every: 3, on: .main, in: .common).autoconnect()
    
    // 6
    init(numberOfImages: Int, @ViewBuilder content: () -> Content) {
        self.numberOfImages = numberOfImages
        self.content = content()
    }
    
    var body: some View {
        GeometryReader { geometry in
            // 2
            HStack(spacing: 0) {
                // 3
                self.content
            }.cornerRadius(10)
            .frame(width: geometry.size.width, height: geometry.size.height, alignment: .leading) // 4
            .offset(x: CGFloat(self.currentIndex) * -geometry.size.width, y: 0) // 5
            .animation(.spring()) // 6
            .onReceive(self.timer) { _ in
                // 7
                self.currentIndex = (self.currentIndex + 1) % self.numberOfImages
            }.gesture(
                DragGesture()
                    .onChanged { gesture in
                        self.offset = gesture.translation
                    }
                    
                    .onEnded { _ in
                        //                                    if self.offset.width > 100 {
                        //                                        // remove the card
                        //                                                 self.SELECT -= 1
                        //
                        //                                    } else
                        if self.offset.width < -1 {
                            self.currentIndex = (self.currentIndex + 1) % self.numberOfImages
                        }else if(self.offset.width > 1) {
                            self.currentIndex = (self.currentIndex - 1) % self.numberOfImages
                        }
                    }
            )
        }    }
}

struct ImageCarouselView_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReader { geometry in
            ImageCarouselView(numberOfImages: 5) {
                
                ForEach(0...10,id: \.self){ _ in
                    Image("home")
                        .resizable()
                        .scaledToFill()
                        .frame(width: geometry.size.width, height: geometry.size.height)
                        .clipped()
                }
                //                  Image("home2")
                //                      .resizable()
                //                      .scaledToFill()
                //                      .frame(width: geometry.size.width, height: geometry.size.height)
                //                      .clipped()
                //                  Image("group")
                //                      .resizable()
                //                      .scaledToFill()
                //                      .frame(width: geometry.size.width, height: geometry.size.height)
                //                      .clipped()
            }
        }.frame(width: UIScreen.main.bounds.width, height: 300, alignment: .center)
        // 7
        //        ImageCarouselView(numberOfImages: 3) {
        //            Text("Hello World")
        //              Text("Hello World2")
        //              Text("Hello World3")
        //        }
    }
}
