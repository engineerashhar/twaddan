//
//  SideMenu.swift
//  Twaddan
//
//  Created by Spine on 29/07/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import FirebaseDatabase
import FirebaseAuth
import SDWebImageSwiftUI
import Firebase

//struct SideMenu: View {
//    
//    
//    let ref = Database.database().reference()
//    @EnvironmentObject var settings : UserSettings
//    @Binding var goToSplash : Bool
//    
//    @State var FID: Bool = UserDefaults.standard.bool(forKey: "FaceID") ?? false
//    @Binding var OProfile : Bool
//    @Binding var ONotification : Bool
//    @Binding var OpenCart : Bool
//    @Binding var OBookings : Bool
//    @Binding var isShowingScanner : Bool
//    @Binding var OHelpCenter : Bool
//    @Binding var OAboutUS : Bool
//    @Binding var OOffers : Bool
//    @Binding var OLanguageSelPage : Bool
//    
//    @State var Name : String = ""
//    @State var Email : String = ""
//    @State var Photo : String = ""
//    var body: some View {
//        
//        
//        VStack(alignment:.leading){
//            ScrollView{
//            
//            ZStack(alignment:.top   ) {
//                
//                
////                Spacer()
////                    .frame(height: 200)
//                
////                VStack{
////                    HStack{
////                        Image("buildings").resizable()
////                            .frame(height: 400)
////                        Spacer()
////                    }.padding(.top,40)
////                    Spacer()
////
////
////                }
//                
//                VStack{
//                    
//                    
//                    HStack{
//                        
//if(
//                                !(Auth.auth().currentUser?.isAnonymous ??
//                                    true
//                                )
//                            ){
//                                                        
//                                                        AnimatedImage(url:URL (string:self.Photo)).resizable().frame(width:16,height:16)
//                                                                             .foregroundColor(Color("c1")).padding(12)
//                                                                        .clipShape(Circle())
//                        .overlay(Circle().stroke(Color.black,lineWidth: 1))
//}else{
//                                                        Image(systemName: "person").resizable().frame(width:16,height:16)
//                                                                                                                 .foregroundColor(Color("c1")).padding(12)
//                                                                                                            .clipShape(Circle())
//                                                            .overlay(Circle().stroke(Color.black,lineWidth: 1)
//                       
//                        )
//                         }
//                        VStack(alignment:.leading){
//                            
////                            Spacer().frame(height: 50)
//                            if(
//                                !(Auth.auth().currentUser?.isAnonymous ??
//                                    true
//                                )
//                            ){
//                                                        
//                                
//                                Text(self.Name)  .font(.custom("Bold_Font".localized(), size: 14))  .foregroundColor(Color("darkthemeletter"))
//                                
////                                Text(self.Email)  .font(.custom("Regular_Font".localized(), size: 14))
////                                    .foregroundColor(Color("darkthemeletter"))
//                            }else{
//                                
//                                Text("Guest")  .font(.custom("Bold_Font".localized(), size: 14))  .foregroundColor(Color("darkthemeletter"))
//                            }
//                            
//                            
//                            
//                        }.padding(.leading)
//                        Spacer()
//                    }.padding().padding(.top,40).padding(.leading,20)
//                    
//                    Divider()
//                    
//                    VStack{
//                        
//                        VStack(alignment:.leading, spacing:30){
//                            
//                            if(
//                                !(Auth.auth().currentUser?.isAnonymous ??
//                                    true
//                                )
//                                ){
//                                Button(action:{
//                                    self.OProfile = true
//                                    
//                                }){
//                                    
//                                    HStack{
//                                        
//                                        
//                                        
//                                        Image("Iconly-Light-Profile") .renderingMode(.template).resizable().frame(width:20,height:20)
//                                            .foregroundColor(Color("darkthemeletter"))
//                                        
//                                        Text("Profile").font(.custom("Bold_Font".localized(), size: 14))
//                                            .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
//                                       
//                                        
//                                    }
//                                    
//                                }
//                                Button(action:{
//                                    self.ONotification = true
//                                }){
//                                    HStack{
//                                        
//                                        Image("Iconly-Light-Notification").renderingMode(.template).resizable().frame(width:20,height:20).foregroundColor(Color("darkthemeletter"))
//                                        
//                                        Text("Notification").font(.custom("Bold_Font".localized(), size: 14))
//                                            .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
//                                     
//                                        
//                                        
//                                        
//                                    }
//                                }
//                                
//                            }else{
//                                
//                                
//                                
//                                Spacer().frame(height:60)
//                                
//                            }
//                            
//                            Button(action:{
//                                self.OpenCart = true
//                                recalculateETA()
//                                
//                            }){
//                                HStack{
//                                    
//                                    Image("Iconly-Light-Wallet").renderingMode(.template).resizable().frame(width:20,height:20).foregroundColor(Color("darkthemeletter"))
//                                    
//                                    Text("Cart").font(.custom("Bold_Font".localized(), size: 14))
//                                        .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
//                                    Spacer()
//                                    
//                                }
//                            }
//                            
//                            
//                            if(
//                                !(Auth.auth().currentUser?.isAnonymous ??
//                                true
//                                )
//                                ){
//                                Button(action:{
//                                    self.OBookings = true
//                                }){
//                                    HStack{
//                                        
//                                        Image("Iconly-Light-Document").renderingMode(.template).resizable().frame(width:20,height:20).foregroundColor(Color("darkthemeletter"))
//                                        
//                                        Text("My Bookings").font(.custom("Bold_Font".localized(), size: 14))
//                                            .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
//                                        
//                                        
//                                    }
//                                }
//                            }
//                            
//                            Group{
//                                Button(action:{
//                                    
//                                    self.OOffers = true
//                                }){
//                                    HStack{
//                                        
//                                        Image("Group 800").renderingMode(.template).resizable().frame(width:20,height:20).foregroundColor(Color("darkthemeletter"))
//                                        
//                                        Text("Offers").font(.custom("Bold_Font".localized(), size: 14))
//                                            .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
////                                        Spacer()
//                                        
//                                    }
//                                }
//                                //                                    Button(action:{
//                                //
//                                //                                        self.isShowingScanner = true
//                                //                                    }){
//                                //                                        HStack{
//                                //
//                                //                                            Image("qr-code2").renderingMode(.template).resizable().frame(width:20,height:20).foregroundColor(Color("darkthemeletter"))
//                                //
//                                //                                            Text("Scan QR Code").font(.custom("Bold_Font".localized(), size: 14))
//                                //                                                .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
//                                //                                            Spacer()
//                                //
//                                //                                        }
//                                //
//                                //                                    }
//                                
//                                if(
////                                    !(Auth.auth().currentUser?.isAnonymous ??
//                                        true
////                                    )
//                                    ){
//                                    Button(action:{
//                                        
//                                        
//                                        let b : Bool =   UserDefaults.standard.bool(forKey: "FaceID")
//                                        print(b)
//                                        
//                                        UserDefaults.standard.set(!b, forKey: "FaceID")
//                                        
//                                        
//                                        self.FID = UserDefaults.standard.bool(forKey: "FaceID")
//                                        
//                                    }){
//                                        HStack{
//                                            
//                                            Image("face").renderingMode(.template).resizable().frame(width:20,height:20).foregroundColor(Color("darkthemeletter"))
//                                            
//                                            Text(self.FID ? "Disable Face ID" : "Enable Face ID" ).font(.custom("Bold_Font".localized(), size: 14))
//                                                .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
//                                         
//                                            
//                                        }
//                                        
//                                        
//                                        
//                                    }
//                                    
//                                }
//                                Button(action:{
//                                    
//                                }){
//                                    HStack{
//                                        
//                                        Image("share").renderingMode(.template).resizable().frame(width:20,height:20).foregroundColor(Color("darkthemeletter"))
//                                        
//                                        Text("Share").font(.custom("Bold_Font".localized(), size: 14))
//                                            .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
//                                      
//                                        
//                                    }
//                                }
//                                Button(action:{
//                                    
//                                    self.OHelpCenter = true
//                                }){
//                                    HStack{
//                                        
//                                        Image("Group 772").renderingMode(.template).resizable().frame(width:20,height:20).foregroundColor(Color("darkthemeletter"))
//                                        
//                                        Text("Help Center").font(.custom("Bold_Font".localized(), size: 14))
//                                            .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
//                                        
//                                        
//                                    }
//                                }
//                                Button(action:{
//                                    
//                                    self.OAboutUS = true
//                                }){
//                                    HStack{
//                                        
//                                        Image("Group 826").renderingMode(.template).resizable().frame(width:20,height:20).foregroundColor(Color("darkthemeletter"))
//                                        
//                                        Text("About US").font(.custom("Bold_Font".localized(), size: 14))
//                                            .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
//                                       
//                                        
//                                    }
//                                }
//                                
//                                
//                                Button(action:{
//                                    
//                                    self.OLanguageSelPage = true
//                                }){
//                                    HStack{
//                                        
//                                        Image("lansel").renderingMode(.template).resizable().frame(width:20,height:20).foregroundColor(Color("darkthemeletter"))
//                                        
//                                        Text("Change Language").font(.custom("Bold_Font".localized(), size: 14))
//                                            .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
//                                       
//                                        
//                                    }
//                                }
//                                
//                                
//                                
//                                if(
//                                    !(Auth.auth().currentUser?.isAnonymous ??
//                                        true
//                                    )
//                                    ){
//                                    Button(action:{
//                                        
//                                        let firebaseAuth = Auth.auth()
//                                        do {
//                                            try firebaseAuth.signOut()
//
//
//
//                                            UserDefaults.standard.set(false,forKey:"SOCIAL")
//                                            self.goToSplash = true
//
//
//
//                                        } catch let signOutError as NSError {
//                                            print ("Error signing out: %@", signOutError)
//                                        }
//
////
//                                        
//                                        
//                                    }){
//                                        
//                                        
//                                        
//                                        HStack{
//                                            
//                                            Image("logout").renderingMode(.template).resizable().frame(width:20,height:20).foregroundColor(Color("darkthemeletter"))
//                                            
//                                            Text("Sign Out").font(.custom("Bold_Font".localized(), size: 14))
//                                                .foregroundColor(Color("darkthemeletter")).padding(.horizontal)
//                                          
//                                            
//                                        }
//                                        
//                                        
//                                    }
//                                }
//                            }
//                            
//                        }.padding(.top,35).padding(.leading,10)
//                            .padding(.horizontal, 25.0)
//                        Spacer()
//                        
//                        
//                        
//                        
//                        
//                        
//                    }
//                        //.frame(maxWidth:.infinity ,maxHeight: geometry.size.height*0.8)
////                        .background(RoundedCorners(color: .white, tl: 30, tr: 30, bl: 0, br: 0))
//                    
//                }
//                
//                
//            }.background(Color.white).frame( maxWidth:.infinity,alignment: .leading).frame(height:UIScreen.main.bounds.height)
////                .background(LinearGradient(gradient: Gradient(colors: [Color("G1"),Color("G4"),Color("G5")]), startPoint: .top, endPoint: .bottom))
//                
//                
//        }
//                
//                
//                .navigationBarItems(trailing:Text(""))
//            
//            
//            
//            
//            
//            
//            
//            
//        }.onAppear{
//            
//            
//            
//            
//            self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").observe(DataEventType.value) { (dataSnapshot) in
//                
//                if(dataSnapshot.childSnapshot(forPath:"personal_details/name").value != nil){
//                    self.Name = String(describing :  (dataSnapshot.childSnapshot(forPath:"personal_details/name").value  ?? ""))
//                    self.Photo = String(describing :  (dataSnapshot.childSnapshot(forPath:"personal_details/dp").value ?? "") )
//                    self.Email = String(describing :  (dataSnapshot.childSnapshot(forPath:"personal_details/email").value ?? "") )
//                }
//            }
//        }
//        //   .padding()
//        //  .frame(maxWidth: .infinity, alignment: .leading)
//        //  .background(Color(red: 32/255, green: 32/255, blue: 32/255))
//        
//        
//    }
//    
//}

struct SideMenu_Previews: PreviewProvider {
    static var previews: some View {
        SideMenu(goToSplash: .constant(false), OProfile: .constant(false), ONotification: .constant(false), OpenCart: .constant(false), OBookings: .constant(false), isShowingScanner: .constant(false), OHelpCenter: .constant(false), OAboutUS: .constant(false), OOffers: .constant(false), OLanguageSelPage: .constant(false))
    }
}
