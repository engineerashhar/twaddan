//
//  Settings.swift
//  Twaddan
//
//  Created by Spine on 05/08/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import FirebaseAuth
import SwitchLanguage
import FirebaseDatabase
import SDWebImageSwiftUI

struct Settings: View {
    
    
    @EnvironmentObject var vehicleItems : VehicleItems
    @State var Photo :String = ""
    @State var Name : String = ""
    @State var Email : String = ""
    @State var Phone : String = ""
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    let ref = Database.database().reference()
    
    
    @State private var previewIndex = UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? 0 : 1
    @State private var previewIndex2 =     UserDefaults.standard.bool(forKey: "FaceID") ?? false  ? 0 : 1
    var previewOptions = ["English","اللغة العربية"]
    var previewOptions2 = ["Enable".localized(), "Disable".localized()]
    @State var goToSplash = false
    
    
    @State var EnableNotification = true ;
    @State var EnableFaceID = UserDefaults.standard.bool(forKey: "FaceID") ?? false
    
    var body: some View {
        
        GeometryReader{ geometry in
            
            
            //            NavigationView{
            
            //            ScrollView{
            ZStack{
                
                NavigationLink(destination: SignUpSelection(FromDA :.constant(false))
                               //                                    .navigationBarTitle("").navigationBarHidden(true)
                               , isActive: self.$goToSplash){
                    Text("").frame(height:0)
                }
                
                
                
                VStack{
                    
                    VStack{
                        //                                                    Spacer().frame(width:geometry.size.width,height: geometry.size.height*0.20)
                        //                        }.background(Color.red)
                    }
                    //         .background(LinearGradient(gradient: Gradient(colors: [Color("h3"),Color("h2")]), startPoint: .leading, endPoint: .trailing))
                    
                    
                    
                    Form{
                        
                        
                        if(!(Auth.auth().currentUser?.isAnonymous ?? true)){
                            
                            HStack{
                                Spacer()
                                
                                VStack{
                                    AnimatedImage(url: URL(string: self.Photo)).resizable().frame(width:50,height:50).clipShape(Circle())
                                        .foregroundColor(Color("c1")).padding(12)
                                    
                                    //                                                                            .overlay(Circle().stroke(Color.black,lineWidth: 1)
                                    //
                                    //
                                    //                                )
                                    
                                    
                                    Text(self.Name)  .font(.custom("Bold_Font".localized(), size: 14))  .foregroundColor(Color("darkthemeletter"))
                                    
                                }
                                
                                Spacer()
                                
                            }.padding()
                            
                            
                            
                            Section{
                                
                                
                                NavigationLink(destination: MyProfile()){
                                    
                                    Text("Account Info").font(.custom("Regular_Font".localized(), size: 16))
                                }
                                
                                
                                
                                Toggle("Face ID", isOn: self.$EnableFaceID ).font(.custom("Regular_Font".localized(), size: 16)).onTapGesture {
                                    //                                   UserDefaults.standard.set(self.EnableNotification , forKey: "PUSH_NOT")
                                    //
                                    //                                   if(self.EnableNotification){
                                    //                                       UIApplication.shared.registerForRemoteNotifications()
                                    //                                          }else{
                                    //                                              UIApplication.shared.unregisterForRemoteNotifications()
                                    //                                   }
                                    
                                    if(!self.EnableFaceID ){
                                        
                                        //                             let b : Bool =   UserDefaults.standard.bool(forKey: "FaceID")
                                        //                                                                    print(b)
                                        
                                        UserDefaults.standard.set(true, forKey: "FaceID")
                                        print("Enable")
                                        
                                        //                                                                    self.FID = UserDefaults.standard.bool(forKey: "FaceID")
                                        
                                        
                                    }else{
                                        UserDefaults.standard.set(false, forKey: "FaceID")
                                        print("Disable")
                                    }
                                }
                                
                                
                                //                            Picker(selection: self.$previewIndex2, label: Text("Face ID").font(.custom("Regular_Font".localized(), size: 16))
                                //               ) {
                                //                   ForEach(0 ..< self.previewOptions2.count) {
                                //                       Text(self.previewOptions2[$0]).font(.custom("Regular_Font".localized(), size: 16)).onAppear() {
                                //                        if(self.previewOptions2[self.previewIndex2] == "Enable" ){
                                //
                                ////                             let b : Bool =   UserDefaults.standard.bool(forKey: "FaceID")
                                ////                                                                    print(b)
                                //
                                //                                                                    UserDefaults.standard.set(true, forKey: "FaceID")
                                //
                                //
                                ////                                                                    self.FID = UserDefaults.standard.bool(forKey: "FaceID")
                                //
                                //
                                //                        }else{
                                //                                UserDefaults.standard.set(false, forKey: "FaceID")
                                //                        }
                                //                       }
                                //
                                //                                      }
                                //                               }
                                
                                NavigationLink(destination: SavedAddress()){
                                    
                                    Text("Saved Address").font(.custom("Regular_Font".localized(), size: 16))
                                }
                                NavigationLink(destination: SavedCards()){
                                    
                                    Text("Saved Cards").font(.custom("Regular_Font".localized(), size: 16))
                                }
                                
                                
                                NavigationLink(destination: ChangePassword()){
                                    
                                    Text("Change Password").font(.custom("Regular_Font".localized(), size: 16))
                                }
                                
                            }
                        }else{
                            
                            HStack{
                                Spacer()
                                
                                VStack{
                                    Image(systemName: "person").resizable().frame(width:16,height:16)
                                        .foregroundColor(Color("c1")).padding(12)
                                        .clipShape(Circle())
                                        .overlay(Circle().stroke(Color.black,lineWidth: 1)
                                                 
                                        )
                                    
                                    
                                    Text("Guest")  .font(.custom("Bold_Font".localized(), size: 14))  .foregroundColor(Color("darkthemeletter"))
                                    
                                }
                                
                                Spacer()
                                
                            }.padding()
                            
                        }
                        
                        Section{
                            
                            NavigationLink(destination: NewMyOrder()){
                                
                                Text("My Orders").font(.custom("Regular_Font".localized(), size: 16))
                            }
                            
                            NavigationLink(destination: Notifications()){
                                
                                Text("Notifications").font(.custom("Regular_Font".localized(), size: 16))
                            }
                        }
                        
                        Section{
                            
                            
                            
                            
                            
                            Toggle("Notification", isOn: self.$EnableNotification ).font(.custom("Regular_Font".localized(), size: 16)).onTapGesture {
                                UserDefaults.standard.set(self.EnableNotification , forKey: "PUSH_NOT")
                                
                                if(!self.EnableNotification){
                                    print("Enable")
                                    UIApplication.shared.registerForRemoteNotifications()
                                }else{
                                    print("Disable")
                                    UIApplication.shared.unregisterForRemoteNotifications()
                                }
                            }
                            
                            
                            
                            Picker(selection: self.$previewIndex, label: Text("Language").font(.custom("Regular_Font".localized(), size: 16))
                            ) {
                                ForEach(0 ..< self.previewOptions.count) {
                                    Text(self.previewOptions[$0]).font(.custom("Regular_Font".localized(), size: 16)).onAppear() {
                                        if(self.previewOptions[self.previewIndex] == "English" ){
                                            
                                            
                                            let Old = UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                                            
                                            UserDefaults.standard.set("en", forKey: "LANSEL")
                                            Language.setCurrentLanguage("en")
                                            UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
                                            UserDefaults.standard.synchronize()
                                            
                                            if(Old == "en"){
                                                //                     self.ViewLoader = true
                                                //                 self.Click = true
                                                
                                                
                                                
                                                //            self.OM = true
                                            }else{
                                                let appDelegate = AppDelegate()
                                                
                                                appDelegate.resetApp()
                                                
                                            }
                                            
                                            
                                        }else{
                                            
                                            let Old = UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                                            
                                            
                                            UserDefaults.standard.set("ar", forKey: "LANSEL")
                                            
                                            Language.setCurrentLanguage("ar")
                                            UserDefaults.standard.set(["ar"], forKey: "AppleLanguages")
                                            UserDefaults.standard.synchronize()
                                            
                                            if(Old == "ar"){
                                                //                 self.ViewLoader = true
                                                //                                           self.OM = true
                                            }else{
                                                let appDelegate = AppDelegate()
                                                appDelegate.resetApp()
                                            }
                                            
                                        }
                                    }
                                    
                                }
                            }
                        }
                        
                        
                        Section{
                            
                            NavigationLink(destination: GetHelp()){
                                
                                Text("Get Help").font(.custom("Regular_Font".localized(), size: 16))
                            }
                            
                            Button(action: {
                                
                                if let url = URL(string: "https://www.twaddan.com"),
                                   UIApplication.shared.canOpenURL(url) {
                                    UIApplication.shared.open(url, options: [:])
                                }
                            }){
                                
                                Text("About").font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color.black)
                            }
                            
                            if(!(Auth.auth().currentUser?.isAnonymous ?? true)){
                                Button(action:{
                                    
                                    let firebaseAuth = Auth.auth()
                                    do {
                                        try firebaseAuth.signOut()
                                        
                                        
                                        
                                        UserDefaults.standard.set(false,forKey:"SOCIAL")
                                        self.goToSplash = true
                                        
                                        
                                        
                                    } catch let signOutError as NSError {
                                        print ("Error signing out: %@", signOutError)
                                    }
                                    
                                    //
                                    
                                    
                                    
                                    
                                }){
                                    Text("Logout").font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color.black)
                                }
                            }else{
                                
                                NavigationLink(destination: SignUpSelection(FromDA: .constant(false))){
                                    
                                    Text("Sign in").font(.custom("Regular_Font".localized(), size: 16))
                                }
                            }
                            Spacer().frame(height:geometry.size.height*0.01)
                        }
                        
                    }.frame(width:geometry.size.width,height: geometry.size.height)
                }.onAppear{
                    
                    self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").observe(DataEventType.value) { (dataSnapshot) in
                        
                        if(dataSnapshot.childSnapshot(forPath:"personal_details/name").value != nil){
                            self.Name = String(describing :  (dataSnapshot.childSnapshot(forPath:"personal_details/name").value) ?? "")
                            self.Photo = String(describing :  (dataSnapshot.childSnapshot(forPath:"personal_details/dp").value) ?? "")
                            self.Email = String(describing :  (dataSnapshot.childSnapshot(forPath:"personal_details/email").value) ?? "")
                        }
                    }
                }
                
            }
            //            }
            //    }
            
        }.navigationBarTitle("Settings",displayMode: .inline)
        .navigationBarHidden(false)
        .navigationBarBackButtonHidden(false)
        .edgesIgnoringSafeArea(.top)
    }
}

struct Settings_Previews: PreviewProvider {
    static var previews: some View {
        Settings()
    }
}
