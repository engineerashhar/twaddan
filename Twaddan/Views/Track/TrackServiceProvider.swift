//
//  TrackServiceProvider.swift
//  Twaddan
//
//  Created by Spine on 29/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import MapKit
import FirebaseDatabase

struct TrackServiceProvider: View {
    @Binding var openTrack : Bool
    var OrderID : String
    
    @Binding var goToHome : Bool
    
    @State var timer: Timer.TimerPublisher = Timer.publish (every: 1, on: .main, in: .common)
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    
    let ref = Database.database().reference()
    // @ObservedObject var liveLocationFetch = LiveLocationFetch()
    
    @State var driver2D : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 23.0000, longitude: 51.80)
    
    @State              var user2D  : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude:  26, longitude: 57)
    
    
    
    var body: some View {
        
        
        
        GeometryReader { geometry in
            ZStack{
                VStack{
                    
                    
                    TrackMapView(user2D:self.$user2D,driver2D:self.$driver2D).environment(\.colorScheme, .light)
                    //                    .frame(height:geometry.size.height*0.6)
                    
                    
                    
                    
                    
                    
                    
                    TrackServiceStatus(goToTrack:.constant(false) ,OrderID: self.OrderID,ViewTrackButton:false, SOrderID: .constant("")).padding()
                    
                    Spacer()
                    
                    
                }
                
                VStack{
                    Button(action:{
                        
                        
                        self.goToHome = true
                        
                        self.timer.connect()
                        //                                                    self.presentationMode.wrappedValue.dismiss()
                        
                    }){
                        HStack{
                            
                            Image("b").renderingMode(.template)
                            Text("Go to Home".localized()).font(.custom("Bold_Font".localized(), size: 16))
                            Spacer()
                            
                        }.padding(.all,10).frame(width:geometry.size.width,height: 50).foregroundColor(Color(.white)).background(Color(#colorLiteral(red: 0, green: 0.2509999871, blue: 0.4979999959, alpha: 1)))
                    }.onReceive(self.timer) { _ in
                        self.openTrack = false
                    }
                    Spacer()
                }
                
                
                //                        }
            }.environment(\.colorScheme, .light).background(Color.white).onAppear
            //                        {
            //                        self.liveLocationFetch.loadData(OrderID: self.OrderID)
            //            }
            {
                
                self.ref.child("orders").child("all_orders").child(self.OrderID).observe(DataEventType.value) { (datasnapshot) in
                    
                    
                    if(datasnapshot.childSnapshot(forPath: "driver_id").exists()){
                        self.user2D = CLLocationCoordinate2D(latitude:      Double(String(describing :  (datasnapshot.childSnapshot(forPath:"customer_latitude").value)!)) as! CLLocationDegrees
                                                             , longitude:      Double(String(describing :  (datasnapshot.childSnapshot(forPath:"customer_longitude").value)!)) as! CLLocationDegrees
                        )
                        
                        
                        self.ref.child("drivers").child(
                            //"8700815211"
                            String(describing :  (datasnapshot.childSnapshot(forPath:"driver_id").value)!)
                        ).child("live_location").observe(DataEventType.value) { (locsnap) in
                            
                            
                            self.driver2D = CLLocationCoordinate2D(latitude:      Double(String(describing :  (locsnap.childSnapshot(forPath:"latitude").value)!))!
                                                                   , longitude:      Double(String(describing :  (locsnap.childSnapshot(forPath:"longitude").value)!)) as! CLLocationDegrees
                            )
                        }
                        //
                        //                             //
                        //
                        //
                    }
                    else if(!datasnapshot.childSnapshot(forPath: "driver_id").exists()){
                        
                        self.driver2D  = CLLocationCoordinate2D(latitude: 23.0000, longitude: 51.80)
                        
                        self.user2D   = CLLocationCoordinate2D(latitude:  26, longitude: 57)
                    }
                }
                
            }
            
            
            //
            
        }     .navigationBarTitle("Live Tracking",displayMode: .inline)
        .navigationBarHidden(true)
        .edgesIgnoringSafeArea(.all)
        
        
        
        
    }
}

//struct TrackServiceProvider_Previews: PreviewProvider {
//    static var previews: some View {
//        TrackServiceProvider(OrderID:"orderid", goToHome: .constant(false), openTrack: .con)
//    }
//}



