//
//  MapView.swift
//  Twaddan
//
//  Created by Spine on 18/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation
import SwiftUI
import MapKit
import CoreLocation


struct FixedMapView : UIViewRepresentable {
    
    var centerCoordinates : CLLocationCoordinate2D
    //     @Binding var centerLocation : String
    // let Selectedlandmark : Landmark
    // let landmarks : [Landmark]
    func makeUIView(context: Context) ->  MKMapView {
        let map = MKMapView()
        map.showsUserLocation = true
        map.delegate = context.coordinator
        return map
        // mapView.delegate = mapView return mapView
    }
    
    func makeCoordinator() -> FixedCoordinator {
        FixedCoordinator(self,centerCoordinates: centerCoordinates)
    }
    
    func updateUIView(_ uiView:  MKMapView, context: UIViewRepresentableContext<FixedMapView>) {
        
        uiView.setCenter(centerCoordinates, animated: true)
        
        // updateAnnotations(from: uiView)
        //        if(self.Selectedlandmark.name != "" ){
        //        uiView.setCenter(Selectedlandmark.coordinate, animated: true)
        //        }
    }
    
    private func updateAnnotations(from mapView:MKMapView){
        
        mapView.removeAnnotations(mapView.annotations)
        // let annotations = self.landmarks.map(LandmarkAnnotation.init)
        //   mapView.addAnnotations(annotations)
    }
    
}

class FixedCoordinator : NSObject ,MKMapViewDelegate {
    
    var centerCoordinates : CLLocationCoordinate2D
    var parent : FixedMapView
    
    init(_ parent :FixedMapView,centerCoordinates : CLLocationCoordinate2D){
        
        
        self.parent = parent
        self.centerCoordinates = centerCoordinates
    }
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        
        if let annotationView = views.first {
            if let annotation = annotationView.annotation{
                if annotation is MKUserLocation {
                    
                    let region = MKCoordinateRegion(center: centerCoordinates, latitudinalMeters: 1000, longitudinalMeters:  1000)
                    mapView.setRegion(region, animated: false)
                    mapView.isUserInteractionEnabled = false
                    
                    //                    mapView.setCenter(centerCoordinates, animated: true)
                }
                
                
            }
            
        }
        
    }
    
    func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
        parent.centerCoordinates = mapView.centerCoordinate
        print(mapView.centerCoordinate)
        
        //      let georeader = CLGeocoder()
        //        georeader.reverseGeocodeLocation(CLLocation(latitude:parent.centerCoordinates.latitude,longitude:parent.centerCoordinates.longitude)){ (places,err) in
        //
        //          if err != nil{
        //
        //              print((err?.localizedDescription)!)
        //              return
        //          }
        //
        //
        //            self.parent.centerLocation = places?.first?.subLocality as! String
        //        //      return places?.first?.locality
        //          print(places?.first?.subLocality as! String)
        //
        //      }
        
        
        
    }
    
}
