//
//  PlaceListView.swift
//  Twaddan
//
//  Created by Spine on 19/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import MapKit

struct PlaceListView: View {
    @Binding var FromSelection: Bool
    @Binding var centerCoodinates: CLLocationCoordinate2D
    let landmarks : [Landmark]
    
    var onTap:() -> ()
    var body: some View {
        
        
        
        
        VStack(alignment : .leading){
            
            HStack{
                EmptyView()
                
            }.frame(width : UIScreen.main.bounds.size.width,height: 60)
            .background(Color.white)
            .gesture(TapGesture().onEnded(self.onTap))
            
            List{
                
                ForEach(self.landmarks,id: \.id){ landmark in
                    VStack(alignment :.leading){
                        
                        Button(action:{
                            //  print(landmark.coordinate)
                            self.centerCoodinates = landmark.coordinate
                            self.FromSelection = true
                        }){
                            HStack{
                                Text(landmark.name).font(.custom("ExtraBold_Font".localized(), size: 15)).foregroundColor(Color("darkthemeletter"))
                                
                                
                                
                                
                                
                                
                            }
                            
                            Text(landmark.title).font(.custom("Regular_Font".localized(), size: 13)).foregroundColor(Color("MainFontColor1"))
                            
                        }
                    }
                }.buttonStyle(BorderlessButtonStyle())
            }
            
        }.cornerRadius(10)
        .navigationBarTitle("Select")
        
        
        
    }
}

//struct PlaceListView_Previews: PreviewProvider {
//    static var previews: some View {
//        PlaceListView(centerCoodinates: <#Binding<CLLocationCoordinate2D>#>, landmarks: [Landmark(placemark: MKPlacemark()
//        )], onTap: {})
//    }
//}
