//
//  MapView.swift
//  Twaddan
//
//  Created by Spine on 18/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation
import SwiftUI
import MapKit
import CoreLocation


struct MapView : UIViewRepresentable {
    
    @Binding var FromSelection: Bool
    
    @Binding var centerCoordinates : CLLocationCoordinate2D
    var currentLoc  : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 11.11, longitude: 24.11)
    //     @Binding var centerLocation : String
    // let Selectedlandmark : Landmark
    // let landmarks : [Landmark]
    func makeUIView(context: Context) ->  MKMapView {
        let map = MKMapView()
        map.showsUserLocation = true
        map.delegate = context.coordinator
        return map
        // mapView.delegate = mapView return mapView
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    func updateUIView(_ uiView:  MKMapView, context: UIViewRepresentableContext<MapView>) {
        
        // updateAnnotations(from: uiView)
        //        if(self.Selectedlandmark.name != "" ){
        //        uiView.setCenter(centerCoordinates, animated: true)
        if(FromSelection){
            
            //            print("setting center")
            let region = MKCoordinateRegion(center: centerCoordinates, latitudinalMeters: 1000, longitudinalMeters:  1000)
            uiView.setRegion(region, animated: true)
        }
        //        }
    }
    
    private func updateAnnotations(from mapView:MKMapView){
        
        mapView.removeAnnotations(mapView.annotations)
        // let annotations = self.landmarks.map(LandmarkAnnotation.init)
        //   mapView.addAnnotations(annotations)
    }
    
}

class Coordinator : NSObject ,MKMapViewDelegate {
    
    var parent : MapView
    
    init(_ parent :MapView){
        
        
        self.parent = parent
    }
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        
        if let annotationView = views.first {
            if let annotation = annotationView.annotation{
                if annotation is MKUserLocation {
                    
                    let region = MKCoordinateRegion(center: annotation.coordinate, latitudinalMeters: 1000, longitudinalMeters:  1000)
                    mapView.setRegion(region, animated: true)
                    
                    
                }
                
                
            }
            
        }
        
    }
    
    func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
        parent.centerCoordinates = mapView.centerCoordinate
        print(mapView.centerCoordinate)
        parent.FromSelection = false
        //      let georeader = CLGeocoder()
        //        georeader.reverseGeocodeLocation(CLLocation(latitude:parent.centerCoordinates.latitude,longitude:parent.centerCoordinates.longitude)){ (places,err) in
        //
        //          if err != nil{
        //
        //              print((err?.localizedDescription)!)
        //              return
        //          }
        //
        //
        //            self.parent.centerLocation = places?.first?.subLocality as! String
        //        //      return places?.first?.locality
        //          print(places?.first?.subLocality as! String)
        //
        //      }
        
        
        
    }
    
}

