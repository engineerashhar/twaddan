 //
 //  Landmark.swift
 //  Twaddan
 //
 //  Created by Spine on 19/05/20.
 //  Copyright © 2020 spine. All rights reserved.
 //
 
 import Foundation
 import MapKit
 
 struct Landmark : Hashable {
    let placemark:MKPlacemark
    
    var id : UUID { return UUID() }
    var name : String {self.placemark.name ?? ""}
    var title : String {self.placemark.title ?? ""}
    var coordinate : CLLocationCoordinate2D {self.placemark.coordinate}
    
    
 }
