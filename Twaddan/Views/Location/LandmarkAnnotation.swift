//
//  LandmarkAnnotation.swift
//  Twaddan
//
//  Created by Spine on 19/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation
import MapKit

final class LandmarkAnnotation : NSObject , MKAnnotation {
    
    let title: String?
    let coordinate: CLLocationCoordinate2D
    
    init(landmark: Landmark){
        self.title = landmark.name
        self.coordinate = landmark.coordinate
        
    }
    
    
    
    
}
