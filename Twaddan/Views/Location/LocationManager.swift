//
//  LocationManager.swift
//  Twaddan
//
//  Created by Spine on 18/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation
import GoogleMaps

class LocationManager : NSObject, ObservableObject{
    
    
    @Published var locationManager = CLLocationManager()
    @Published var location : CLLocation? = nil
    @Published var locationName : String? = nil
    override init()
    {
        super.init()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        self.location =  self.locationManager.location
        //        print(self.location)
        
    }
    func getLocationName() {
        let georeader = CLGeocoder()
        
        
        if(self.location != nil){
            
            
            let geocoder = GMSGeocoder()
            
            geocoder.reverseGeocodeCoordinate(self.location!.coordinate) { response, error in
                //
                if error != nil {
                    print("reverse geodcode fail: \(error)")
                } else {
                    if let places = response?.results() {
                        let places : [GMSAddress] = places
                        
                        
                        self.locationName =  places.first?.lines![0]
                        UserDefaults.standard.set(self.location?.coordinate.latitude, forKey: "CLLOCATION_LAT");
                        UserDefaults.standard.set(self.location?.coordinate.longitude, forKey: "CLLOCATION_LON");
                        UserDefaults.standard.set(self.locationName, forKey: "CLLOCATION_NAME");
                    }
                }
                
                
                
                
                //        georeader.reverseGeocodeLocation(self.location!){ (places,err) in
                //
                //
                //                                                                                                let str1 = String((places?.first?.name) ?? "")
                //
                //
                //
                //                                                                                                                                                                                          let str2 = String( (places?.first?.subThoroughfare) ?? ""
                //                                                                                                                                                                                              )
                //
                //                                                                                                                                                                                          let str3 = String( (places?.first?.thoroughfare) ?? ""
                //                                                                                                                                                                                                                                         )
                //
                //
                //
                //                                                                                                                                                                                          let str4 = String( (places?.first?.postalCode) ?? ""
                //                                                                                                                                                                                                                                         )
                //
                //
                //
                //
                //                                                                                                                                                                                          let str5 = String( (places?.first?.subLocality) ?? ""
                //                                                                                                                                                                                                                                         )
                //
                //
                //
                //
                //                                                                                                                                                                                          let str6 = String( (places?.first?.locality) ?? ""
                //                                                                                                                                                                                                                                         )
                //
                //
                //
                //
                //                                                                                                                                                                                          let str7 = String( (places?.first?.country) ?? "")
                //            self.locationName =  "\(str5), \(str6),  \(str2), \(str3), \(str4), \(str7)"
                //
            }
        }
    }
    
}
extension LocationManager : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        //     print(status)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else{
            
            return
        }
        
        self.location = location
        
        
        
        if(UserDefaults.standard.string(forKey: "PLACE") == nil && false){
            //                                              UserDefaults.standard.set(nil, forKey: "PLACE")
            
            //                                                                           DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                
                let geocoder = GMSGeocoder()
                
                geocoder.reverseGeocodeCoordinate(self.locationManager.location!.coordinate) { response, error in
                    //
                    if error != nil {
                        print("reverse geodcode fail: \(error)")
                    } else {
                        if let places = response?.results() {
                            let places : [GMSAddress] = places
                            
                            
                            print("ooooooooo","3")
                            UserDefaults.standard.set(Double((self.locationManager.location?.coordinate.latitude)!), forKey: "latitude")
                            
                            
                            UserDefaults.standard.set(Double((self.locationManager.location?.coordinate.longitude)!), forKey: "longitude")
                            
                            let str1 = String((places.first?.thoroughfare) ?? "")
                            
                            print("str1",str1)
                            
                            let str2 = String( (places.first?.subLocality) ?? ""
                                               
                                               
                            )
                            print("str2",places.first?.lines)
                            let str3 = String( (places.first?.locality) ?? ""
                            )
                            
                            print("str3",str3)
                            
                            //                                                    let str4 = String( (places.first?.postalCode) ?? ""
                            
                            
                            
                            
                            
                            let str5 = String( (places.first?.administrativeArea) ?? ""
                            )
                            
                            print("str5",str5)
                            
                            
                            //                                                    let str6 = String( (places.first?.locality) ?? ""
                            
                            
                            
                            
                            
                            let str7 = String( (places.first?.country) ?? "")
                            print("str7",str7)
                            
                            UserDefaults.standard.set(str1, forKey: "HOME")
                            
                            UserDefaults.standard.set(str1+" , "+str2+" , "+str3, forKey: "STREET")
                            
                            UserDefaults.standard.set(str1, forKey: "BUILDING")
                            
                            UserDefaults.standard.set(places.first?.lines![0], forKey: "PLACE")
                            
                            UserDefaults.standard.set("",forKey: "AHOME")
                            UserDefaults.standard.set(str1+" , "+str2+" , "+str3,forKey: "ASTREET")
                            UserDefaults.standard.set("",forKey: "AFLOOR")
                            UserDefaults.standard.set("",forKey: "AOFFICE")
                            UserDefaults.standard.set("",forKey: "ADNOTE")
                            UserDefaults.standard.set("",forKey: "ANICK")
                            //                                                                                                                          self.OpenPartialSheet = false
                            
                            
                            //
                            //                                                    if let place = places.first {
                            //
                            //
                            //                                                        if let lines = place.lines {
                            //                                                            print("GEOCODE: Formatted Address: \(lines)")
                            //
                            //
                            //                                                        }
                            //
                            //
                            //
                            //                                                    } else {
                            //                                                        print("GEOCODE: nil first in places")
                            //                                                    }
                        } else {
                            print("GEOCODE: nil in places")
                        }
                    }
                }
                //
                //                                                                                     let georeader = CLGeocoder()
                //                                                                               georeader.reverseGeocodeLocation(self.locationManager.location!){ (places,err) in
                //
                //                                                                                         if err != nil{
                //
                //                                                                                             print((err?.localizedDescription)!)
                //                                                                                             return
                //                                                                                         }
                //
                //
                //                                               //                                            self.parent.centerLocation = places?.first?.subLocality as! String
                //                                                                                       //      return places?.first?.locality
                //                                                                                        //   print(places?.first?.subLocality!)
                //
                //
                //
                //
                //
                //
                //
                //                                                                                   UserDefaults.standard.set(Double((self.locationManager.location?.coordinate.latitude)!), forKey: "latitude")
                //
                //
                //                                                                                   UserDefaults.standard.set(Double((self.locationManager.location?.coordinate.longitude)!), forKey: "longitude")
                //
                //                                                                                           let str1 = String((places?.first?.name) ?? "")
                //
                //
                //
                //                                                                                           let str2 = String( (places?.first?.subThoroughfare) ?? ""
                //                                                                                               )
                //
                //                                                                                           let str3 = String( (places?.first?.thoroughfare) ?? ""
                //                                                                                                                                          )
                //
                //
                //
                //                                                                                           let str4 = String( (places?.first?.postalCode) ?? ""
                //                                                                                                                                          )
                //
                //
                //
                //
                //                                                                                           let str5 = String( (places?.first?.subLocality) ?? ""
                //                                                                                                                                          )
                //
                //
                //
                //
                //                                                                                           let str6 = String( (places?.first?.locality) ?? ""
                //                                                                                                                                          )
                //
                //
                //
                //
                //                                                                                           let str7 = String( (places?.first?.country) ?? "")
                //
                //
                //                                                                                           UserDefaults.standard.set(str2, forKey: "HOME")
                //
                //                                                                                           UserDefaults.standard.set(str5+" , "+str6+" , "+str4+" , "+str7, forKey: "STREET")
                //
                //                                                                                           UserDefaults.standard.set(str3, forKey: "BUILDING")
                //
                //                                                                                           UserDefaults.standard.set(str5+" , "+str6+" , "+str2+" , "+str3+" , "+str4+" , "+str7, forKey: "PLACE")
                //
                //                                                                                                            UserDefaults.standard.set("",forKey: "AHOME")
                //                                                                                                                                                     UserDefaults.standard.set(str5+" , "+str6+" , "+str4+" , "+str7,forKey: "ASTREET")
                //                                                                                                                                                      UserDefaults.standard.set("",forKey: "AFLOOR")
                //                                                                                                                                                   UserDefaults.standard.set("",forKey: "AOFFICE")
                //                                                                                                                                                   UserDefaults.standard.set("",forKey: "ADNOTE")
                //                                                                                                 UserDefaults.standard.set("",forKey: "ANICK")
                //
                //                                                                                     }
                
                
                //                                            self.placedisplay.toggle()
                //                                            self.placedisplay.toggle()
                
                
                
                
            }
        }
        
        
        
        //        let georeader = CLGeocoder()
        //                                                                                       georeader.reverseGeocodeLocation(location){ (places,err) in
        //
        //
        //                                                                                        let str1 = String((places?.first?.name) ?? "")
        //
        //
        //
        //                                                                                                                                                                                  let str2 = String( (places?.first?.subThoroughfare) ?? ""
        //                                                                                                                                                                                      )
        //
        //                                                                                                                                                                                  let str3 = String( (places?.first?.thoroughfare) ?? ""
        //                                                                                                                                                                                                                                 )
        //
        //
        //
        //                                                                                                                                                                                  let str4 = String( (places?.first?.postalCode) ?? ""
        //                                                                                                                                                                                                                                 )
        //
        //
        //
        //
        //                                                                                                                                                                                  let str5 = String( (places?.first?.subLocality) ?? ""
        //                                                                                                                                                                                                                                 )
        //
        //
        //
        //
        //                                                                                                                                                                                  let str6 = String( (places?.first?.locality) ?? ""
        //                                                                                                                                                                                                                                 )
        //
        //
        //
        //
        //                                                                                                                                                                                  let str7 = String( (places?.first?.country) ?? "")
        //
        //                                                                                        self.locationName = str5+" , "+str6+" , "+str2+" , "+str3+" , "+str4+" , "+str7
        //
        //         }
    }
    
}
