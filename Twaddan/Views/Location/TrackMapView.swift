//
//  TrackMapVieew.swift
//  Twaddan
//
//  Created by Spine on 29/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation

import SwiftUI
import MapKit
import CoreLocation


struct TrackMapView : UIViewRepresentable {
    
    @Binding var user2D : CLLocationCoordinate2D
    @Binding var driver2D : CLLocationCoordinate2D
    
    //@Binding var centerCoordinates : CLLocationCoordinate2D
    //     @Binding var centerLocation : String
    // let Selectedlandmark : Landmark
    // let landmarks : [Landmark]
    func makeUIView(context: Context) ->  MKMapView {
        let map = MKMapView()
        map.showsUserLocation = true
        map.delegate = context.coordinator
        return map
        // mapView.delegate = mapView return mapView
    }
    
    func makeCoordinator() -> Coordinator2 {
        Coordinator2(self ,user2D:user2D,driver2D:driver2D)
    }
    
    func updateUIView(_ uiView:  MKMapView, context: UIViewRepresentableContext<TrackMapView>) {
        // if(!(self.user2D.latitude == 26 && self.user2D.longitude == 57)){
        updateAnnotations(from: uiView)
        
        updateDirection(from : uiView)
        //        }
        //        if(self.Selectedlandmark.name != "" ){
        //        uiView.setCenter(Selectedlandmark.coordinate, animated: true)
        //        }
    }
    func updateDirection(from mapView:MKMapView)  {
        
        
        //
        
        
        
        let request = MKDirections.Request()
        request.source = MKMapItem(placemark:  MKPlacemark(coordinate: driver2D))
        request.destination = MKMapItem(placemark:  MKPlacemark(coordinate: user2D))
        request.requestsAlternateRoutes = false
        
        
        let directions = MKDirections(request: request)
        directions.calculate { response, error in
            
            
            if(error != nil){
                print( error?.localizedDescription)
            }
            let routes = response?.routes
            
            
            //   var eta:Double = 10000000
            
            if(routes != nil){
                let selectedRoute = routes![0]
                let distance = selectedRoute.distance
                print("HHHHHHHHH \(distance)")
                // eta = selectedRoute.expectedTravelTime
                let route = selectedRoute
                
                for overlay in mapView.overlays {
                    
                    mapView.removeOverlay(overlay)
                    
                }
                //                            if(!(self.user2D.latitude == 26 && self.user2D.longitude == 57)){
                
                mapView.addOverlay(route.polyline, level: .aboveRoads)
                //                            }
                
                let rect = route.polyline.boundingMapRect
                //                                        mapView.setRegion(MKCoordinateRegion(rect), animated: true)
                mapView.setVisibleMapRect(rect, edgePadding: UIEdgeInsets(top: 50, left: 50, bottom: 50, right: 50), animated: true)
                
                
                
            }else{
                //  mapView.showAnnotations(mapView.annotations, animated: true)
                //                            if(!(self.user2D.latitude == 26 && self.user2D.longitude == 57)){
                
                
                var zoomRect = MKMapRect.null
                for annotation in mapView.annotations {
                    
                    if(annotation.title == "Service Provider" || annotation.title == "Destination" || annotation.title == "Destination1" || annotation.title == "Service Provider1" ){
                        let annotationPoint = MKMapPoint(annotation.coordinate)
                        
                        let pointRect = MKMapRect(x: annotationPoint.x, y: annotationPoint.y, width: 0, height: 0)
                        
                        if (zoomRect.isNull) {
                            zoomRect = pointRect
                        } else {
                            zoomRect = zoomRect.union(pointRect)
                        }
                        
                    }
                    //                                   }
                    
                    mapView.setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets(top: 50, left: 50, bottom: 50, right: 50), animated: true)
                }
                
            }
            
            
        }
    }
    
    private func updateAnnotations(from mapView:MKMapView){
        var sourcePin = customPin(pinTitle: "Service Provider", pinSubTitle: "", location: driver2D)
        var destinationPin = customPin(pinTitle: "Destination", pinSubTitle: "", location: user2D)
        if((self.user2D.latitude == 26 && self.user2D.longitude == 57)){
            sourcePin = customPin(pinTitle: "Service Provider1", pinSubTitle: "", location: driver2D)
            destinationPin = customPin(pinTitle: "Destination1", pinSubTitle: "", location: user2D)
            
        }
        
        //        if(!(self.user2D.latitude == 26 && self.user2D.longitude == 57)){
        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotation(sourcePin)
        mapView.addAnnotation(destinationPin)
        
        //                    if((self.user2D.latitude == 26 && self.user2D.longitude == 57)){
        //                        mapView.removeAnnotations(mapView.annotations)
        //        }
        
        //        }
        // let annotations = self.landmarks.map(LandmarkAnnotation.init)
        //   mapView.addAnnotations(annotations)
        //                    }
        //                    else{
        //
        //                        let sourcePin = customPin(pinTitle: "0", pinSubTitle: "", location: driver2D)
        //                                                  let destinationPin = customPin(pinTitle: "1", pinSubTitle: "", location: user2D)
        //
        //
        //
        //                           //        if(!(self.user2D.latitude == 26 && self.user2D.longitude == 57)){
        // mapView.removeAnnotations(mapView.annotations)
        //                                   mapView.addAnnotation(sourcePin)
        //                                                            mapView.addAnnotation(destinationPin)
        //        }
        
    }
    
}

class Coordinator2 : NSObject ,MKMapViewDelegate {
    
    var parent : TrackMapView
    var user2D : CLLocationCoordinate2D
    var driver2D : CLLocationCoordinate2D
    
    init(_ parent :TrackMapView,user2D:CLLocationCoordinate2D,driver2D:CLLocationCoordinate2D){
        
        
        self.parent = parent
        self.user2D = user2D
        self.driver2D = driver2D
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        
        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "TESTING NOTE")
        annotationView.canShowCallout = true
        
        //        if(!(self.user2D.latitude == 26 && self.user2D.longitude == 57)){
        if(annotation.title == "Service Provider"){
            annotationView.image =  resizeImage(image: UIImage(named:"van")!, targetSize: CGSize.init(width: 100, height: 50))
        }else if(annotation.title == "Destination") {
            annotationView.image = resizeImage(image: UIImage(named:"pin_new")!, targetSize: CGSize.init(width: 20, height: 50))
        }else  {
            annotationView.image = resizeImage(image: UIImage(named:"van")!, targetSize: CGSize.init(width: 1, height: 1))
            
        }
        
        //        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        
        
        
        //        let sourceLocation = CLLocationCoordinate2D(latitude:11.050976 , longitude: 76.071098)
        //        let destinationLocation = CLLocationCoordinate2D(latitude:11.050980 , longitude: 76.071098)
        //
        //        let sourcePin = customPin(pinTitle: "Kansas City", pinSubTitle: "", location: sourceLocation)
        //        let destinationPin = customPin(pinTitle: "St. Louis", pinSubTitle: "", location: destinationLocation)
        //        mapView.addAnnotation(sourcePin)
        //        mapView.addAnnotation(destinationPin)
        //
        //        let sourcePlaceMark = MKPlacemark(coordinate: sourceLocation)
        //        let destinationPlaceMark = MKPlacemark(coordinate: destinationLocation)
        
        
        
        
        //        var driver2D : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 11.050976, longitude: 76.071098)
        //
        //                       var user2D  : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 11.050980, longitude: 76.101098)
        
        //
        //                let sourcePin = customPin(pinTitle: "Service Provider", pinSubTitle: "", location: driver2D)
        //                let destinationPin = customPin(pinTitle: "Destination", pinSubTitle: "", location: user2D)
        //
        //
        //
        //             let request = MKDirections.Request()
        //                request.source = MKMapItem(placemark:  MKPlacemark(coordinate: driver2D))
        //                request.destination = MKMapItem(placemark:  MKPlacemark(coordinate: user2D))
        //                request.requestsAlternateRoutes = false
        //
        //
        //        let directions = MKDirections(request: request)
        //            directions.calculate { response, error in
        //
        //
        //                if(error != nil){
        //                    print( error?.localizedDescription)
        //                }
        //                        let routes = response?.routes
        //
        //
        //                       //   var eta:Double = 10000000
        //
        //                        if(routes != nil){
        //                        let selectedRoute = routes![0]
        //                        let distance = selectedRoute.distance
        //                            print("HHHHHHHHH \(distance)")
        //                        // eta = selectedRoute.expectedTravelTime
        //                            let route = selectedRoute
        //                            mapView.addOverlay(route.polyline, level: .aboveRoads)
        //
        //                                       let rect = route.polyline.boundingMapRect
        //                                       mapView.setRegion(MKCoordinateRegion(rect), animated: true)
        //
        //
        //
        //
        //                        }
        //
        //        }
        //
        //
        
        //        if let annotationView = views.first {
        //            if let annotation = annotationView.annotation{
        //                if annotation is MKUserLocation {
        //
        //                    let region = MKCoordinateRegion(center: annotation.coordinate, latitudinalMeters: 1000, longitudinalMeters:  1000)
        //                    mapView.setRegion(region, animated: true)
        //
        //                }
        //
        //
        //            }
        //
        //        }
        //
    }
    
    func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
        // parent.centerCoordinates = mapView.centerCoordinate
        // print(mapView.centerCoordinate)
        
        //      let georeader = CLGeocoder()
        //        georeader.reverseGeocodeLocation(CLLocation(latitude:parent.centerCoordinates.latitude,longitude:parent.centerCoordinates.longitude)){ (places,err) in
        //
        //          if err != nil{
        //
        //              print((err?.localizedDescription)!)
        //              return
        //          }
        //
        //
        //            self.parent.centerLocation = places?.first?.subLocality as! String
        //        //      return places?.first?.locality
        //          print(places?.first?.subLocality as! String)
        //
        //      }
        
        
        
    }
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 4.0
        return renderer
    }
}
class customPin: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(pinTitle:String, pinSubTitle:String, location:CLLocationCoordinate2D) {
        self.title = pinTitle
        self.subtitle = pinSubTitle
        self.coordinate = location
    }
    
}
