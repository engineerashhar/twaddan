//
//  FindLocation.swift
//  Twaddan
//
//  Created by Spine on 18/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import MapKit
import GoogleMaps

struct FindLocation: View {
    
    @Binding var CalculateETA : Bool
    
    @State var FromSelection : Bool = false
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    //  @ObservedObject var locationManager = LocationManager()
    @State private var tapped :Bool = false
    @State var centerCoordinates : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 11.11, longitude: 24.11)
    //   @State var centerLocation : String = ""
    @State var ViewBottomPage = false
    
    @State var StreetAddress : String = ""
    @ObservedObject var locationManager = LocationManager()
    @State private var landmarks : [Landmark] = [Landmark]()
    private func getNearByLandmarks(){
        
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = StreetAddress
        
        let StreetAddress = MKLocalSearch(request : request)
        
        StreetAddress.start { (response,error) in
            
            
            if let response = response {
                
                let mapItems = response.mapItems
                
                self.landmarks = mapItems.map{
                    Landmark(placemark : $0.placemark)
                }
                
            }
            
            
        }
        
    }
    
    
    var body: some View {
        
        
        
        GeometryReader { geometry in
            
            
            ZStack{
                
                
                
                
                //First Layer
                
                VStack{
                    Rectangle()
                        .fill(Color("backcolor2"))
                        
                        .frame(width:geometry.size.width,height: 200)
                        .cornerRadius(20)
                    
                    
                    //         RoundedCorners(color: Color("backcolor2"), tl: 0, tr: 0, bl: 60, br: 60)
                    //       .frame(width:geometry.size.width,height: geometry.size.height*0.25)
                    //        .background(Color("backcolor2"))
                    //  .foregroundColor(Color("ManBackColor"))
                    
                    
                    
                    
                    Spacer()
                    
                    
                    ZStack{
                        MapView(FromSelection: self.$FromSelection, centerCoordinates: self.$centerCoordinates)
                        
                        //                        Circle()
                        //                            .fill(Color.blue)
                        //                            .opacity(0.3)
                        //                            .frame(width:20 ,height :20)
                        Image("pin_new").resizable().frame(width:20, height:50)
                            .padding(.bottom,80)
                    }
                    
                    
                    
                    
                    
                    
                    
                }
                
                
                VStack(alignment: .leading){
                    
                    
                    Spacer().frame(height:geometry.size.height*0.1)
                    
                    HStack(alignment: .top, spacing:10){
                        
                        
                        
                        VStack{
                            Spacer().frame(height:15)
                            
                            HStack{
                                
                                Text("Search Location").font(.custom("Bold_Font".localized(), size: 16))
                                
                                Spacer()
                            }
                            
                            HStack{
                                
                                TextField("Address", text: self.$StreetAddress,onCommit:{
                                    self.tapped = false
                                    
                                    self.getNearByLandmarks()
                                    
                                    withAnimation{
                                        self.ViewBottomPage = true
                                    }
                                    
                                } )
                                .foregroundColor(Color(red: 0.38, green: 0.51, blue: 0.64, opacity: 1.0))
                                .font(.custom("Regular_Font".localized(), size: 15))
                                .padding(10)
                                
                                
                                
                                
                                
                                
                                
                                
                                // .padding(.trailing, 15.0)
                                
                                
                                
                                
                            } .frame(height:40).background(Color.white)
                            .cornerRadius(4)
                            .shadow(radius: 1)
                            
                            
                        }
                        
                        
                        
                        
                        
                        
                        
                        //
                        //                            VStack{
                        //
                        //
                        //                                HStack{
                        //
                        //
                        //                                    Spacer().frame(height:29)
                        //
                        //                                }
                        //
                        //                                HStack{
                        //
                        //
                        //
                        //                                    Button(action: {}){
                        //                                        Image("lence")
                        //                                            .resizable()
                        //                                            .frame(width: 20, height: 20)
                        //
                        //                                    }
                        //                                    .padding(.all, 15.0)
                        //
                        //
                        //
                        //                                    // .padding(.trailing, 15.0)
                        //
                        //
                        //
                        //
                        //                                } .frame(height:40).background(Color.white)
                        //                                    .cornerRadius(4)
                        //                                    .shadow(radius: 1)
                        //
                        //
                        //                            }
                        //
                        //                            Spacer().frame(width:15)
                        //
                        
                        
                        
                    }.padding()
                    
                    
                    
                    Spacer()
                    
                    
                    
                    
                    
                    
                    
                }.foregroundColor(Color("ManBackColor"))
                
                .cornerRadius(20)
                
                
                
                //Tird Layer
                
                
                //                    VStack{
                //
                //                        Spacer().frame(height:geometry.size.height*0.05)
                //
                //                        HStack{
                //
                //                            Button(action : {
                //
                //
                //
                //
                //                                self.presentationMode.wrappedValue.dismiss()
                //
                //
                //                            }) {
                //                                Image("back").frame(width:25,height:20 )
                //
                //
                //
                //                            }
                //                            .padding(.leading, 25.0)
                //                            Spacer()
                //                            Text("Select Location").font(.custom("Regular_Font".localized(), size: 15))
                //                            Spacer()
                //
                //                            Button(action: {}){
                //                                ZStack{ Image("bell").resizable().frame(width: 25, height: 30)
                //
                //                                    Text("8")
                //                                        .frame(width: 20.0, height: 20.0)
                //                                        .background(Circle().fill(Color.red))
                //                                        .foregroundColor(.white)
                //                                        .padding([.leading, .bottom])
                //
                //
                //
                //
                //                                }.hidden()
                //
                //                            }
                //                            .padding(.trailing, 15.0)
                //
                //                            Button(action: {
                //
                //
                //                            }){
                //                                Image("sortandfilterpng")
                //                                    .resizable().frame(width: 25, height: 25).hidden()
                //
                //                            }
                //                            .padding(.trailing, 20)
                //
                //
                //                        }
                //
                //
                //
                //
                //
                //
                //                        //.padding(.top, 50)
                //                        Spacer()
                //                    }.foregroundColor(Color("ManBackColor"))
                
                .cornerRadius(20)
                
                Spacer()
                
                
                
                
                
                VStack{
                    
                    Spacer()
                    
                    
                    
                    
                    
                    
                    //                        PlaceListView(FromSelection: self.$FromSelection, centerCoodinates: self.$centerCoordinates, landmarks:self.landmarks)
                    //                                                            {
                    //                                                                self.tapped.toggle()
                    //                                                            }.animation(.spring())
                    //                                                                .offset(y : self.calculateoffset())
                    //                                        //                    .frame(height:200)
                    
                    Button(action:{
                        print("ooooooooo","1")
                        
                        if(self.centerCoordinates.latitude != 0){
                            
                            print("ooooooooo","2")
                            
                            
                            let geocoder = GMSGeocoder()
                            
                            geocoder.reverseGeocodeCoordinate(CLLocationCoordinate2D(latitude: self.centerCoordinates.latitude, longitude: self.centerCoordinates.longitude)) { response, error in
                                //
                                if error != nil {
                                    print("reverse geodcode fail: \(error)")
                                } else {
                                    if let places = response?.results() {
                                        let places : [GMSAddress] = places
                                        
                                        
                                        print("ooooooooo","3")
                                        UserDefaults.standard.set(Double(self.centerCoordinates.latitude), forKey: "latitude")
                                        
                                        
                                        UserDefaults.standard.set(Double(self.centerCoordinates.longitude), forKey: "longitude")
                                        
                                        
                                        UserDefaults.standard.set(places.first?.administrativeArea?.localized(), forKey: "EMIRATES")
                                        print("uuuu",places.first?.administrativeArea)
                                        let str1 = String((places.first?.thoroughfare) ?? "")
                                        
                                        print("str1",str1)
                                        
                                        let str2 = String( (places.first?.subLocality) ?? ""
                                                           
                                                           
                                        )
                                        print("str2",places.first?.lines)
                                        let str3 = String( (places.first?.locality) ?? ""
                                        )
                                        
                                        print("str3",str3)
                                        
                                        //                                                    let str4 = String( (places.first?.postalCode) ?? ""
                                        
                                        
                                        
                                        
                                        
                                        let str5 = String( (places.first?.administrativeArea) ?? ""
                                        )
                                        
                                        print("str5",str5)
                                        
                                        
                                        //                                                    let str6 = String( (places.first?.locality) ?? ""
                                        
                                        
                                        
                                        
                                        
                                        let str7 = String( (places.first?.country) ?? "")
                                        print("str7",str7)
                                        
                                        UserDefaults.standard.set(str1, forKey: "HOME")
                                        
                                        UserDefaults.standard.set(str1+" , "+str2+" , "+str3, forKey: "STREET")
                                        
                                        UserDefaults.standard.set(str1, forKey: "BUILDING")
                                        
                                        UserDefaults.standard.set(places.first?.lines![0], forKey: "PLACE")
                                        
                                        UserDefaults.standard.set("",forKey: "AHOME")
                                        UserDefaults.standard.set(str1+" , "+str2+" , "+str3,forKey: "ASTREET")
                                        UserDefaults.standard.set("",forKey: "AFLOOR")
                                        UserDefaults.standard.set("",forKey: "AOFFICE")
                                        UserDefaults.standard.set("",forKey: "ADNOTE")
                                        UserDefaults.standard.set("",forKey: "ANICK")
                                        
                                        
                                        //
                                        //                                                    if let place = places.first {
                                        //
                                        //
                                        //                                                        if let lines = place.lines {
                                        //                                                            print("GEOCODE: Formatted Address: \(lines)")
                                        //
                                        //
                                        //                                                        }
                                        //
                                        //
                                        //
                                        //                                                    } else {
                                        //                                                        print("GEOCODE: nil first in places")
                                        //                                                    }
                                    } else {
                                        print("GEOCODE: nil in places")
                                    }
                                }
                            }
                            
                            
                            
                            
                            //
                            //                                      let georeader = CLGeocoder()
                            //                                        georeader.reverseGeocodeLocation(CLLocation(latitude:self.centerCoordinates.latitude,longitude:self.centerCoordinates.longitude)){ (places,err) in
                            //
                            //                                          if err != nil{
                            //
                            //                                              print("oooooooooooo",(err?.localizedDescription)!)
                            //                                              return
                            //                                          }
                            //
                            //
                            ////                                            self.parent.centerLocation = places?.first?.subLocality as! String
                            //                                        //      return places?.first?.locality
                            //                                         //   print(places?.first?.subLocality!)
                            //
                            //
                            //
                            //
                            //
                            //
                            //                                              print("ooooooooo","3")
                            //                                            UserDefaults.standard.set(Double(self.centerCoordinates.latitude), forKey: "latitude")
                            //
                            //
                            //                                            UserDefaults.standard.set(Double(self.centerCoordinates.longitude), forKey: "longitude")
                            //
                            //                                            let str1 = String((places?.first?.name) ?? "")
                            //
                            //
                            //
                            //                                            let str2 = String( (places?.first?.subThoroughfare) ?? ""
                            //                                                )
                            //
                            //                                            let str3 = String( (places?.first?.thoroughfare) ?? ""
                            //                                                                                           )
                            //
                            //
                            //
                            //                                            let str4 = String( (places?.first?.postalCode) ?? ""
                            //                                                                                           )
                            //
                            //
                            //
                            //
                            //                                            let str5 = String( (places?.first?.subLocality) ?? ""
                            //                                                                                           )
                            //
                            //
                            //
                            //
                            //                                            let str6 = String( (places?.first?.locality) ?? ""
                            //                                                                                           )
                            //
                            //
                            //
                            //
                            //                                            let str7 = String( (places?.first?.country) ?? "")
                            //
                            //
                            //                                            UserDefaults.standard.set(str2, forKey: "HOME")
                            //
                            //                                            UserDefaults.standard.set(str5+" , "+str6+" , "+str4+" , "+str7, forKey: "STREET")
                            //
                            //                                            UserDefaults.standard.set(str3, forKey: "BUILDING")
                            //
                            //                                            UserDefaults.standard.set(str5+" , "+str6+" , "+str2+" , "+str3+" , "+str4+" , "+str7, forKey: "PLACE")
                            //
                            //                                                             UserDefaults.standard.set("",forKey: "AHOME")
                            //                                                                                                      UserDefaults.standard.set(str5+" , "+str6+" , "+str4+" , "+str7,forKey: "ASTREET")
                            //                                                                                                       UserDefaults.standard.set("",forKey: "AFLOOR")
                            //                                                                                                    UserDefaults.standard.set("",forKey: "AOFFICE")
                            //                                                                                                    UserDefaults.standard.set("",forKey: "ADNOTE")
                            //                                                  UserDefaults.standard.set("",forKey: "ANICK")
                            //
                            //                                      }
                            
                            
                            
                            self.CalculateETA = true
                            
                            self.presentationMode.wrappedValue.dismiss()
                            
                        }
                        
                        
                        
                        
                    }){
                        
                        RoundedPanel()
                            
                            
                            
                            
                            .overlay(Text("Select Location").font(.custom("ExtraBold_Font".localized(), size: 16))
                                        .foregroundColor(.white))
                        
                        //                            .padding(.horizontal,30)
                        
                        
                        
                        
                    }
                    
                }
                
                VStack{
                    Spacer()
                    
                    VStack(alignment : .leading){
                        
                        HStack{
                            EmptyView()
                            
                        }.frame(width : UIScreen.main.bounds.size.width,height: 30)
                        .background(Color.white).onTapGesture {
                            withAnimation{
                                self.ViewBottomPage = false
                            }
                        }
                        
                        
                        List{
                            
                            ForEach(self.landmarks,id: \.self){ landmark in
                                VStack(alignment :.leading){
                                    
                                    Button(action:{
                                        //  print(landmark.coordinate)
                                        //                                                                    DispatchQueue.main.async {
                                        self.centerCoordinates = landmark.coordinate
                                        self.FromSelection = true
                                        //                                                                    withAnimation{
                                        self.ViewBottomPage = false
                                        //                                                                    }
                                        //                                                                    }
                                    }){
                                        VStack(alignment:.leading){
                                            
                                            HStack{
                                                Text(landmark.name).font(.custom("ExtraBold_Font".localized(), size: 15)).foregroundColor(Color("darkthemeletter")).padding(.all,15)
                                                
                                                
                                                Spacer()
                                            }
                                            
                                            
                                            HStack{
                                                
                                                Text(landmark.title).font(.custom("Regular_Font".localized(), size: 13)).foregroundColor(Color("MainFontColor1")) .padding(.all,15)
                                                
                                                
                                                Spacer()
                                            }
                                        }.border(Color.black, width: 2)
                                        
                                        .frame(width:UIScreen.main.bounds.size.width-30).background(Color.white)
                                        
                                    }
                                }
                            }.buttonStyle(BorderlessButtonStyle())
                        }
                        
                    }.cornerRadius(10).frame(height:geometry.size.height*0.5).offset(y:self.ViewBottomPage ? 0:geometry.size.height)
                    
                }
                
                
            }.navigationBarTitle("Map",displayMode: .inline)
            //                .navigationBarHidden(true)
            .edgesIgnoringSafeArea(.top)
            //            }
            
            
            
            
            
        }
        
        
        
    }
    
    
    func calculateoffset()
    -> CGFloat {
        
        if self.landmarks.count > 0 && !self.tapped{
            
            return UIScreen.main.bounds.size.height - UIScreen.main.bounds.size.height/2
        }
        else if self.tapped{
            return UIScreen.main.bounds.size.height
        }else{
            
            return UIScreen.main.bounds.size.height
        }
        
    }
    //
    //    func getlocation(centerCoordinates:CLLocationCoordinate2D) -> String {
    //
    //
    //
    ////
    ////        let georeaderg = CLGeocoder()
    ////        georeader.reverseGeocodeLocation(CLLocation(latitude:centerCoordinates.latitude,longitude:centerCoordinates.longitude)){ (places,err) in
    ////
    ////            if err != nil{
    ////
    ////                print((err?.localizedDescription)!)
    ////                return
    ////            }
    ////
    ////
    ////
    ////                return places?.first?.locality
    ////
    ////
    ////        }
    //
    //
    //    }
    //  func getDouble -> Double(_ d: Double) { return d }
}

struct FindLocation_Previews: PreviewProvider {
    static var previews: some View {
        FindLocation(CalculateETA: .constant(false))//
    }
}



