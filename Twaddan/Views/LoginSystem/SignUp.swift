//
//  SignUp.swift
//  Twaddan
//
//  Created by Spine on 01/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI

import FirebaseDatabase
import FirebaseAuth

struct SignUp: View {
    
    @Binding var FromDA : Bool
    @State var goToLogin : Bool = false
    let ref = Database.database().reference()
    @ObservedObject var backImagelinker = BackImageLinker()
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var ALERT:Bool = false
    @State var alert:String = ""
    @State var ErrorComments:String = "false"
    @State var ErrorStatus:Bool = false
    @ObservedObject var signLink = SignLink()
    @EnvironmentObject var settings: UserSettings
    
    //     @State var firstName : String = ""
    //     @State var lastName : String = ""
    //     @State var mobileNumber : String = ""
    //     @State var email : String = ""
    //     @State var password : String = ""
    @State var profile : Profile = Profile(Name:"",Email:"",Password:"",Phone:"",LastName:"", RePassword: "")
    
    @State var Terms:Bool = true
    @State var showTerms : Bool = false
    @State var ViewLoader :Bool = false
    @State var openMenu : Bool = false
    
    @State var openDAP : Bool =  false
    
    var body: some View {
        GeometryReader { geometry in
            
            
            
            
            ZStack{
                
                
                
                
                VStack {
                    VStack {
                        //                        Spacer()
                        //                                                .frame(height: geometry.size.height*0.05)
                        HStack{
                            
                            NavigationLink(destination:HomePage()
                                           //                                .navigationBarTitle("").navigationBarHidden(true)
                                           ,isActive: self.$openMenu){
                                Text("")
                                
                                
                            }
                            NavigationLink(destination:DetailAddressPage(),isActive: self.$openDAP){
                                EmptyView()
                                
                                
                            }
                            
                            //                            Button(action : {
                            //                                
                            //                                self.presentationMode.wrappedValue.dismiss()
                            //                                
                            //                                
                            //                            }) {
                            //                                Text("BACK")
                            //                                    .foregroundColor(Color.white)
                            //                                    .font(.custom("Regular_Font".localized(), size: 15))
                            //                                
                            //                            }
                            
                            
                            //                            Spacer()
                            //                            Button(action:{
                            //                                                            
                            //                                                             let uuid = UIDevice.current.identifierForVendor?.uuidString
                            //                            //                                    print(uuid)
                            //                                                                      self.ViewLoader = true
                            //                                                            let user  =   Auth.auth().currentUser ;
                            //                                                             if(user?.email == nil && user?.uid != nil){
                            //                                                                                    self.settings.Aid = uuid ?? user?.uid as! String
                            //                                                                UserDefaults.standard.set(self.settings.Aid, forKey: "Aid")
                            //                                                            
                            //                                                                                                          self.openMenu=true
                            //                                                                 self.ViewLoader = false
                            //                                                             }else{
                            //                                                  
                            //                                                            Auth.auth().signInAnonymously() { (authResult, error) in
                            //                                                              // ...
                            //                                                              
                            //                                                                                                 
                            //                                                                
                            //                                                                guard let user = authResult?.user else {
                            //                                                                    print(error)
                            //                                                                    return }
                            //                                                              
                            //                                                                
                            //                                                                let isAnonymous = user.isAnonymous  // true
                            //                                                                let uid = user.uid
                            //                                                                
                            //                                                                self.settings.Aid = uuid ?? uid
                            //                                                                   UserDefaults.standard.set(self.settings.Aid, forKey: "Aid")
                            //                                                                
                            //                                                                  self.openMenu = true
                            //                                                                self.ViewLoader = false
                            //                                                                
                            //                                                            }
                            //                                                               
                            //                                                            }
                            //                                                            
                            //                                                            
                            //                                                        })
                            //                            {
                            //                                Text("SKIP")
                            //                                    .foregroundColor(Color.white)
                            //                                    .font(.custom("Regular_Font".localized(), size: 15))
                            //                                
                            //                            }
                            //                            
                            
                            
                            
                            
                            
                        }
                        NavigationLink(destination:LoginPage(FromDA: self.$FromDA)
                                       //                            .navigationBarTitle("").navigationBarHidden(true)
                                       ,isActive: self.$goToLogin){
                            Text("")
                        }
                        VStack{
                            Spacer()
                            VStack(spacing:20){
                                
                                HStack{
                                    
                                    Text("Create your account")
                                        
                                        .foregroundColor(Color("darkthemeletter"))
                                        //                                .padding( .vertical, 50.0)
                                        .font(.custom("Regular_Font".localized(), size: 20))
                                    Spacer()
                                    
                                }.padding(.vertical)
                                //                                                                .padding(.top,50)
                                Spacer()
                                
                                
                                RegistrationDetails(profile:self.$profile)
                                
                                
                                if(self.ErrorStatus){
                                    
                                    Text(self.ErrorComments)
                                        .foregroundColor(.red) .font(.custom("Regular_Font".localized(), size: 14))
                                    
                                }
                                
                                Spacer()
                                
                                HStack{
                                    
                                    
                                    Image(systemName: self.Terms ? "checkmark.square.fill" : "square" ).resizable().frame(width:22,height: 22).onTapGesture {
                                        self.Terms.toggle()
                                    }.foregroundColor(Color("c1"))
                                    
                                    
                                    Text("I agree with terms and conditions")
                                        .foregroundColor(Color(red: 0.15, green: 0.22, blue: 0.44, opacity: 1.0))
                                        .font(.custom("Regular_Font".localized(), size: 12))
                                        .onTapGesture {
                                            self.showTerms = true
                                        }  .sheet(isPresented: self.$showTerms) {
                                            TermsAndConditions()
                                        }
                                    
                                    
                                    Spacer()
                                }
                                
                                
                                Button(action: {
                                    
                                    self.ErrorStatus=true;
                                    
                                    
                                    if(self.profile.Name == "" ){
                                        self.ErrorComments="Please Enter A Valid Name"
                                        self.ErrorStatus=true;
                                        
                                        
                                    }else
                                    
                                    if(self.profile.Phone == "" ){
                                        self.ErrorComments="Please Enter A Valid Mobile Number"
                                        self.ErrorStatus=true;
                                        
                                        
                                    }
                                    else
                                    
                                    if(!self.profile.Email.contains("@")){
                                        self.ErrorComments="Please Enter A Valid Email"
                                        self.ErrorStatus=true;
                                        
                                        
                                    }
                                    else
                                    
                                    if(self.profile.Password == "" ){
                                        self.ErrorComments="Please Enter A Valid Password"
                                        self.ErrorStatus=true;
                                        
                                        
                                    }
                                    
                                    else
                                    
                                    if(self.profile.Password != self.profile.RePassword ){
                                        self.ErrorComments="Passwords must be matched"
                                        self.ErrorStatus=true;
                                        
                                        
                                    }
                                    else
                                    
                                    if(!self.Terms){
                                        self.ErrorComments="You must agree terms and conditions"
                                        self.ErrorStatus=true;
                                        
                                        
                                    }
                                    
                                    else
                                    
                                    {
                                        self.ErrorStatus=false;
                                        
                                        self.ViewLoader = true
                                        
                                        let firebaseAuth = Auth.auth()
                                        do {
                                            try firebaseAuth.signOut()
                                            
                                            
                                        } catch let signOutError as NSError {
                                            print ("Error signing out: %@", signOutError)
                                        }
                                        
                                        if(self.profile.Phone.count > 9){
                                            self.profile.Phone.suffix(9)
                                        }
                                        
                                        Auth.auth().fetchProviders(forEmail: self.profile.Email, completion: {
                                            (providers, error) in
                                            
                                            if let error = error {
                                                self.ErrorComments = handleError(error:error)
                                                self.ErrorStatus = true
                                                //
                                                print(error.localizedDescription)
                                            } else if let providers = providers {
                                                print(providers)
                                                self.ErrorComments = "Email already exist"
                                                self.ErrorStatus = true
                                            }else{
                                                self.ErrorStatus=false;
                                                
                                                
                                                
                                                Auth.auth().createUser(withEmail: self.profile.Email, password: self.profile.Password) { (result, error) in
                                                    
                                                    Auth.auth().currentUser?.sendEmailVerification { (error) in
                                                        // ...
                                                    }
                                                    
                                                    
                                                    //                                                if(error == nil){
                                                    //
                                                    //
                                                    //                                                }else if((result?.user) != nil){
                                                    //
                                                    //
                                                    //                                                }else{
                                                    //
                                                    //
                                                    //
                                                    //                                                }
                                                    
                                                    
                                                    guard error == nil else {
                                                        self.ErrorComments = handleError(error:error!)
                                                        self.ErrorStatus = true
                                                        fatalError(handleError(error:error!))
                                                        self.ViewLoader = false
                                                    }
                                                    
                                                    guard (result?.user) != nil else {
                                                        fatalError("Do not know why this would happen")
                                                        self.ErrorComments = "Fatel Error"
                                                        self.ErrorStatus = true
                                                        self.ViewLoader = false
                                                    }
                                                    
                                                    
                                                    let P : [String : Any]=[
                                                        
                                                        "email" : self.profile.Email,
                                                        "lastName": self.profile.LastName,
                                                        "name" :self.profile.Name,
                                                        "phoneNumber":self.profile.Phone,
                                                        "RT" : [".sv": "timestamp"]
                                                    ]
                                                    
                                                    
                                                    //                                                                        self.settings.Uid = result?.user.uid as! String
                                                    //                                                                                self.settings.Email = result?.user.email as! String
                                                    //       self.settings.Name = user?.displayName as! String
                                                    
                                                    
                                                    let c=result?.user.email as! String
                                                    let A = c.replacingOccurrences(of: "@", with: "_")
                                                    
                                                    let B=A.replacingOccurrences(of: ".", with: "_")
                                                    
                                                    //                                                                                                   UserDefaults.standard.string(forKey: "Aid") ?? "0" = B
                                                    
                                                    
                                                    
                                                    UserDefaults.standard.set(B, forKey: "Aid")
                                                    
                                                    
                                                    self.ref.child("Users").child(B).child("personal_details").setValue(P)
                                                    
                                                    self.alert="Thank you, Registered Successfully , kindly verify your email by clicking the mail recieved to you and login"
                                                    self.ALERT=true
                                                    
                                                    
                                                    
                                                    //   print("registered user: \(user.email)")
                                                    
                                                }
                                                
                                                
                                                
                                                //
                                            }
                                        })
                                        
                                        //
                                        //                                                if(self.signLink.isEmailExist){
                                        //
                                        //                                                    self.alert="Email Already exist"
                                        //                                                                                                     self.ALERT=true
                                        
                                        //
                                        //                                                }else{
                                        //
                                        //                                                     self.signLink.register(profile:self.profile)
                                        //
                                        //                                                }
                                        //
                                        
                                        
                                        
                                    }
                                    
                                    
                                }
                                
                                
                                
                                
                                ) {
                                    
                                    
                                    
                                    HStack{
                                        
                                        
                                        
                                        Spacer()
                                        
                                        Text("Create your account")   .font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color.white)
                                        Spacer()
                                        
                                        
                                        
                                    }.padding()
                                    //                            .border(Color("ManualSignInGray"), width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                                    
                                    //                                                               .frame(width: 300.0, height: 50)      .background(Color("su1"))
                                    //                                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color("su1"),lineWidth: 1))
                                    .background(Color("su1"))
                                    .cornerRadius(10)
                                    
                                    
                                    
                                }.alert(isPresented: self.$ALERT) {
                                    Alert(title: Text(""), message: Text(self.alert), dismissButton: .default(Text("OK"),action:{
                                        self.goToLogin = true
                                    }))
                                    
                                }
                                
                                
                                
                                
                                NavigationLink(destination: LoginPage(FromDA: self.$FromDA)
                                               
                                ){
                                    
                                    
                                    
                                    
                                    
                                    
                                    HStack{
                                        Spacer()
                                        Text("I have already an account")
                                            .font(.custom("Regular_Font".localized(), size: 12))
                                            .foregroundColor(Color(red: 0.49, green: 0.58, blue: 0.67, opacity: 1.0))
                                        
                                    }.hidden()
                                    
                                }
                                
                                
                                
                                Spacer()
                                
                                
                                
                            }
                            
                            .padding(.horizontal, 25.0)
                            Spacer()
                            
                            
                            
                            
                            
                            
                        }
                        .frame(maxWidth:.infinity,maxHeight:geometry.size.height*0.8)
                        .background(RoundedCorners(color: .white, tl: 30, tr: 30, bl: 0, br: 0))
                        
                        
                        
                    }
                    
                    
                    
                    
                    
                }
                
                
                if(self.ViewLoader){
                    Loader()
                }
            }.frame(width:geometry.size.width,height: geometry.size.height)
            .navigationBarTitle("Welcome to Twaddan")
            //            .navigationBarHidden(true)
            
            //                }
            
        }
        .edgesIgnoringSafeArea(.all)
        
        
    }
}

struct SignUp_Previews: PreviewProvider {
    static var previews: some View {
        SignUp(FromDA: .constant(false))
    }
}

struct RegistrationDetails: View {
    
    @Binding var profile : Profile
    @State var View = false
    var body: some View {
        VStack(spacing:15){
            
            HStack{
                
                VStack{
                    TextField("First Name", text: $profile.Name)
                        
                        .foregroundColor(Color("darkthemeletter"))
                        .font(.custom("Regular_Font".localized(), size: 14))
                    Divider().frame(height: 1)
                }
                
                VStack{
                    TextField("Last Name", text: $profile.LastName)
                        
                        .foregroundColor(Color(red: 0.38, green: 0.51, blue: 0.64, opacity: 1.0))
                        .font(.custom("Regular_Font".localized(), size: 14))
                    Divider().frame(height: 1)
                }
            }
            
            TextField("Mobile Number", text: $profile.Phone)
                
                .foregroundColor(Color("darkthemeletter"))                .font(.custom("Regular_Font".localized(), size: 14))
                .keyboardType(.numberPad)
            Divider().frame(height: 1)
            
            
            TextField("Email", text: $profile.Email)
                
                .foregroundColor(Color("darkthemeletter"))                .font(.custom("Regular_Font".localized(), size: 14))
            Divider().frame(height: 1)
            HStack{
                
                if(self.View){
                    
                    TextField("Password", text: $profile.Password)
                        
                        .foregroundColor(Color("darkthemeletter"))                .font(.custom("Regular_Font".localized(), size: 14))
                }else{
                    SecureField("Password", text: $profile.Password)
                        
                        .foregroundColor(Color("darkthemeletter"))                .font(.custom("Regular_Font".localized(), size: 14))
                    
                }
                
                Spacer()
                Button(action:{
                    
                    self.View.toggle()
                }){
                    Image("Group 395") .foregroundColor(Color.black) .font(.custom("Regular_Font".localized(), size: 14))
                }
            }
            Divider().frame(height: 1)
            
            HStack{
                TextField("Confirm Password", text: $profile.RePassword)
                    
                    .foregroundColor(Color("darkthemeletter"))                .font(.custom("Regular_Font".localized(), size: 14))
                
                Spacer()
                //                Button(action:{}){
                //                Text("View") .font(.custom("Regular_Font".localized(), size: 14))
                //                }
            }
            Divider().frame(height: 1)
            
            
        }
    }
}

//struct Profile{
//
//    var Name:String
//    var Email:String
//    var Password:String
//    var Phone :String
//    var LastName:String
//    var RePassword:String
//
//}
func handleError(error: Error) -> String {
    
    
    
    /// the user is not registered
    /// user not found
    
    var errorStatus : String=""
    let errorAuthStatus = AuthErrorCode.init(rawValue: error._code)!
    
    
    switch errorAuthStatus {
    case .wrongPassword:
        errorStatus = "wrongPassword"
        print("wrongPassword")
        
    case .invalidEmail:
        errorStatus = "invalidEmail"
        print("invalidEmail")
    case .operationNotAllowed:
        errorStatus = "operationNotAllowed"
        print("operationNotAllowed")
    case .userDisabled:
        errorStatus = "userDisabled"
        print("userDisabled")
    case .userNotFound:
        errorStatus = "userNotFound"
        print("userNotFound")
    //            self.register(auth: Auth.auth())
    case .tooManyRequests:
        errorStatus = "tooManyRequests, oooops"
        print("tooManyRequests, oooops")
    default: print("error not supported here")
        errorStatus = "tooManyRequests, oooops"
    }
    
    return errorStatus
    
}
struct TermsAndConditions: View {
    var body: some View {
        
        ScrollView{
            VStack(alignment:.center){
                Text("Terms and Conditions").font(.custom("ExtraBold_Font".localized(), size: 30)).padding()
                Divider()
                
                Text("TERMS").font(.custom("Regular_Font".localized(), size: 19))
                    .padding()
            }
        }
    }
}


