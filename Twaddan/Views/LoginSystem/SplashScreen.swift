//
//  ContentView.swift
//  Twaddan
//
//  Created by user170583 on 4/22/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import AVKit
import UIKit
import FirebaseAuth





struct SplashScreen: View {
    
    @EnvironmentObject var settings: UserSettings
    
    @State var second:Int = 0 ;
    
    //    static var shouldAnimate = true
    //
    //    static var showSplash = true
    
    @State private var openMenu:Bool=false
    @State private var openReg:Bool=false
    @EnvironmentObject var vehicleItems : VehicleItems
    //  @ObservedObject var signLink = SignLink()
    
    @State var timer = Timer.publish(every: 5, on: .main, in: .common).autoconnect()
    
    
    var body: some View {
        
        NavigationView{
            
            
            GeometryReader{ geometry in
                
                ZStack{
                    
                    PlayerView()   .scaledToFill()
                    
                    
                    
                    ZStack {
                        
                        
                        
                        
                        //                if(naviagate){
                        //                    if(signLink.loginStatus){
                        
                        if(UserDefaults.standard.bool(forKey: "FaceID") ?? false){
                            NavigationLink(destination:FaceID().environmentObject(self.vehicleItems).environmentObject(self.settings),isActive: self.$openMenu){
                                Text("")
                                
                                
                            }
                            
                        }else{
                            NavigationLink(destination:HomePage().environmentObject(self.vehicleItems).environmentObject(self.settings),isActive: self.$openMenu){
                                Text("")
                                
                                
                            }
                        }
                        //
                        //                }else{
                        NavigationLink(
                            destination:LangSelection()
                            //                    .navigationBarTitle("").navigationBarHidden(true).edgesIgnoringSafeArea(.all)
                            ,isActive:self.$openReg) {
                            Text("")
                            //                }
                            
                            //                }
                        }
                        //                Color("su1")
                        
                        
                        //                VStack(alignment: .center){
                        //
                        //                    //  Text("\(second)")
                        //
                        //                    PlayerView()
                        //                        .frame(width: geometry.size.width)
                        //
                        //                    //   player()
                        //
                        //
                        //
                        ////                    HStack{
                        ////                        Image("Group 780")
                        ////                        Spacer()
                        ////                    }
                        ////
                        ////                    Spacer()
                        ////
                        ////                    Image("Group 782")
                        ////
                        ////                    Spacer()
                        ////
                        ////                                  Image("Group 783")
                        ////
                        ////                                  Spacer()
                        //
                        //                }
                        
                        
                        
                    }
                    //            .onReceive(timer){_ in
                    //
                    //                if self.settings.loggedIn {
                    //                                                      self.openMenu=true                         } else {
                    //                                                        self.openReg=true
                    //
                    //                                                         }
                    //            }
                    .onAppear{
                        self.timer = Timer.publish (every: 5, on: .current, in:
                                                        .common).autoconnect()
                        //                    print("reached here")
                        
                    }
                    .onReceive(self.timer) { input in
                        self.timer.upstream.connect().cancel()
                        //   self.timer.upstream.autoconnect()
                        
                        
                        
                        
                        
                        print(input)
                        let user = Auth.auth().currentUser
                        //                
                        //                let firebaseAuth = Auth.auth()
                        //                do {
                        //                  try firebaseAuth.signOut()
                        //                } catch let signOutError as NSError {
                        //                  print ("Error signing out: %@", signOutError)
                        //                }
                        
                        if(UserDefaults.standard.string(forKey: "LANSEL") == nil){
                            self.openReg = true
                            
                        }else if(user?.uid == nil){
                            
                            self.openMenu = true
                            
                        }else if (user!.isAnonymous){
                            let uuid = UIDevice.current.identifierForVendor?.uuidString
                            
                            let uid = user!.uid
                            
                            //                                                                         UserDefaults.standard.string(forKey: "Aid") ?? "0" = uuid ?? user?.uid as! String
                            //                                                     UserDefaults.standard.set(UserDefaults.standard.string(forKey: "Aid") ?? "0", forKey: "Aid")
                            UserDefaults.standard.set( uuid ?? uid, forKey: "Aid")
                            self.openMenu = true
                            
                            
                            
                        }else{
                            
                            
                            if(user?.email != nil){
                                self.settings.Uid = user?.uid as! String
                                self.settings.Email = user?.email as! String
                                //       self.settings.Name = user?.displayName as! String
                                
                                
                                let c=user?.email as! String
                                let A = c.replacingOccurrences(of: "@", with: "_")
                                
                                let B=A.replacingOccurrences(of: ".", with: "_")
                                
                                //                                     UserDefaults.standard.string(forKey: "Aid") ?? "0" = B
                                UserDefaults.standard.set(B, forKey: "Aid")
                                
                                
                                
                                self.openMenu=true
                            }else{
                                
                                if(user?.providerData[0].email != nil){
                                    //                             self.settings.Uid = user?.uid as! String
                                    //                                                                 self.settings.Email = user?.providerData[0].email as! String
                                    //       self.settings.Name = user?.displayName as! String
                                    
                                    
                                    let c=user?.providerData[0].email as! String
                                    let A = c.replacingOccurrences(of: "@", with: "_")
                                    
                                    let B=A.replacingOccurrences(of: ".", with: "_")
                                    
                                    //                                     UserDefaults.standard.string(forKey: "Aid") ?? "0" = B
                                    UserDefaults.standard.set(B, forKey: "Aid")
                                    
                                    
                                    
                                    self.openMenu=true
                                    Auth.auth().currentUser?.updateEmail(to: (Auth.auth().currentUser?.providerData[0].email)!, completion: { (error) in
                                        
                                    })
                                    
                                    
                                    
                                    
                                }else{
                                    
                                    let firebaseAuth = Auth.auth()
                                    do {
                                        try firebaseAuth.signOut()
                                    } catch let signOutError as NSError {
                                        print ("Error signing out: %@", signOutError)
                                    }
                                    self.openReg=true
                                    
                                }
                            }
                            
                            
                        }
                        
                        //                if((user?.uid == nil || !(user?.isEmailVerified ?? false))){
                        //
                        //                    //print(user?.uid)
                        ////                    if(user?.email == nil && user?.uid != nil){
                        ////                        UserDefaults.standard.string(forKey: "Aid") ?? "0" = user?.uid as! String
                        ////                                               UserDefaults.standard.set(user?.uid as! String, forKey: "Aid")
                        ////
                        ////                                              self.openMenu=true
                        ////                    }
                        //
                        //                    print("Login","Email Verification",user?.isEmailVerified)
                        //
                        //
                        //                    if(user?.uid != nil && UserDefaults.standard.bool(forKey: "SOCIAL") ){
                        ////                         self.settings.Uid = user?.uid as! String
                        ////                                           self.settings.Email = user?.email as! String
                        ////                                    //       self.settings.Name = user?.displayName as! String
                        ////
                        ////
                        ////                                           let c=user?.email as! String
                        ////                                           let A = c.replacingOccurrences(of: "@", with: "_")
                        ////
                        ////                                           let B=A.replacingOccurrences(of: ".", with: "_")
                        ////
                        ////                                           UserDefaults.standard.string(forKey: "Aid") ?? "0" = B
                        ////                                               UserDefaults.standard.set(B, forKey: "Aid")
                        ////
                        ////                                              self.openMenu=true
                        //                        var EMAIL:String = ""
                        //
                        //                        if(user?.email == nil){
                        //                              EMAIL = user?.providerData[0].email as! String
                        //                        }else{
                        //                            EMAIL = user?.email as! String
                        //                        }
                        //
                        //
                        //
                        //
                        //
                        //                        self.settings.Uid = user?.uid as! String
                        //                                                                                                                                          self.settings.Email = EMAIL
                        //
                        //                                                                                   //       self.settings.Name = user?.displayName as! String
                        //
                        //
                        //                                                                                                                                          let c=EMAIL as! String
                        //                                                                                                                                                            let A = c.replacingOccurrences(of: "@", with: "_")
                        //
                        //                                                                                                                                                            let B=A.replacingOccurrences(of: ".", with: "_")
                        //
                        //                                                                                                                                                            UserDefaults.standard.string(forKey: "Aid") ?? "0" = B
                        //
                        //                                                                                   UserDefaults.standard.set(B, forKey: "Aid") ;                                                                   self.openMenu=true
                        //
                        //
                        //
                        //                    }else{
                        //                          self.openReg=true
                        //                    }
                        //
                        //                }else{
                        //
                        //                //    self.settings.loggedIn = true;
                        //
                        //                    self.settings.Uid = user?.uid as! String
                        //                    self.settings.Email = user?.email as! String
                        //             //       self.settings.Name = user?.displayName as! String
                        //
                        //
                        //                    let c=user?.email as! String
                        //                    let A = c.replacingOccurrences(of: "@", with: "_")
                        //
                        //                    let B=A.replacingOccurrences(of: ".", with: "_")
                        //
                        //                    UserDefaults.standard.string(forKey: "Aid") ?? "0" = B
                        //                        UserDefaults.standard.set(B, forKey: "Aid")
                        //
                        //                       self.openMenu=true
                        //                }
                        //
                        
                        
                    }
                    
                    
                    
                    
                    
                }
                //            .background(Color.red)
                .frame(width:geometry.size.width, height:geometry.size.height)
                
                
            }
            .navigationBarTitle("")
            //        .navigationBarHidden(true)
            .edgesIgnoringSafeArea(.all)
            
        }
        //        .edgesIgnoringSafeArea(.all)
        //            .background(Color.red)
        
        
    }
    
    
    struct ContentView_Previews: PreviewProvider {
        static var previews: some View {
            SplashScreen()
        }
    }
    struct PlayerView: UIViewRepresentable {
        
        
        func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<PlayerView>) {
        }
        
        
        func makeUIView(context: Context) -> UIView {
            print("player 1")
            return PlayerUIView(frame: .zero)
        }
    }
    
    
    
    
    //struct player : UIViewControllerRepresentable{
    //
    //
    //
    //
    //
    //    func makeUIViewController(context: UIViewControllerRepresentableContext<player>) -> AVPlayerViewController {
    //
    //        let controller=AVPlayerViewController()
    //        let path=Bundle.main.path(forResource: "twaddan2", ofType: "mp4") ?? "default value"
    //
    //        let url = URL(fileURLWithPath: path)
    //
    //        let player1=AVPlayer(url:url)
    //        controller.player=player1
    //        return controller
    //
    //
    //
    //
    //
    //
    //    }
    //    func updateUIViewController(_ uiViewController: AVPlayerViewController, context:  UIViewControllerRepresentableContext<player>) {
    //
    //    }
    //
    //
    //}
    
}
