//
//  SignInWithApple.swift
//  Twaddan
//
//  Created by Spine on 09/08/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import AuthenticationServices

import CryptoKit
import FirebaseAuth
// 1
struct SignInWithApple: UIViewRepresentable {
    // 2
    var name : String = ""
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(self)
    }
    
    func makeUIView(context: Context) -> ASAuthorizationAppleIDButton {
        let button = ASAuthorizationAppleIDButton(authorizationButtonType: .continue, authorizationButtonStyle: .whiteOutline)
        
        button.addTarget(context.coordinator, action:  #selector(Coordinator.didTapButton), for: .touchUpInside)
        return button
    }
    
    func updateUIView(_ uiView: ASAuthorizationAppleIDButton, context: Context) {
        
    }
    
    
    
    
    
    class Coordinator: NSObject, ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
        var parent: SignInWithApple?
        fileprivate var currentNonce: String?
        init(_ parent: SignInWithApple) {
            self.parent = parent
            super.init()
            
        }
        
        @objc func didTapButton() {
            print("Tapped")
            
            
            //            let appleIDProvider = ASAuthorizationAppleIDProvider()
            //            let request = appleIDProvider.createRequest()
            //            request.requestedScopes = [.fullName, .email]
            //
            //            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            //            authorizationController.presentationContextProvider = self
            //            authorizationController.delegate = self
            //            authorizationController.performRequests()
            
            let nonce = randomNonceString()
            currentNonce = nonce
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            request.nonce = sha256(nonce)
            
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        }
        
        @available(iOS 13, *)
        private func sha256(_ input: String) -> String {
            let inputData = Data(input.utf8)
            let hashedData = SHA256.hash(data: inputData)
            let hashString = hashedData.compactMap {
                return String(format: "%02x", $0)
            }.joined()
            
            return hashString
        }
        
        private func randomNonceString(length: Int = 32) -> String {
            precondition(length > 0)
            let charset: Array<Character> =
                Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
            var result = ""
            var remainingLength = length
            
            while remainingLength > 0 {
                let randoms: [UInt8] = (0 ..< 16).map { _ in
                    var random: UInt8 = 0
                    let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
                    if errorCode != errSecSuccess {
                        fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
                    }
                    return random
                }
                
                randoms.forEach { random in
                    if remainingLength == 0 {
                        return
                    }
                    
                    if random < charset.count {
                        result.append(charset[Int(random)])
                        remainingLength -= 1
                    }
                }
            }
            
            return result
        }
        
        func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
            let vc = UIApplication.shared.windows.last?.rootViewController
            return (vc?.view.window!)!
        }
        
        func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
            if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
                
                
                print("OOOOOOOOOOOOO","0001")
                guard let nonce = currentNonce else {
                    fatalError("Invalid state: A login callback was received, but no login request was sent.")
                }
                guard let appleIDToken = appleIDCredential.identityToken else {
                    print("Unable to fetch identity token")
                    return
                }
                guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                    print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                    return
                }
                print("OOOOOOOOOOOOO","000")
                
                let firebaseAuth = Auth.auth()
                do {
                    try firebaseAuth.signOut()
                    
                    
                    
                    
                    
                    
                    
                    
                } catch let signOutError as NSError {
                    print ("Error signing out: %@", signOutError)
                }
                
                // Initialize a Firebase credential.
                let credential = OAuthProvider.credential(withProviderID: "apple.com",
                                                          idToken: idTokenString,
                                                          rawNonce: nonce)
                // Sign in with Firebase.
                Auth.auth().signIn(with: credential) { (authResult, error) in
                    
                    print("OOOOOOOOOOOOOOOO",error?.localizedDescription)
                    if (error != nil) {
                        // Error. If error.code == .MissingOrInvalidNonce, make sure
                        // you're sending the SHA256-hashed nonce as a hex string with
                        // your request to Apple.
                        print(error?.localizedDescription)
                        
                        print("OOOOOOOOOOOOO","111")
                        UserDefaults.standard.set(true,forKey:"SOCIAL")
                        if(authResult?.user.email == nil){
                            print("OOOOOOOOOOOOO","112")
                            let userSettings = UserSettings()
                            authResult?.user.updateEmail(to: (authResult?.user.providerData[0].email)!, completion: { (error) in
                                LoginMark(user: authResult!.user)
                                print("OOOOOOOOOOOOO","113")
                            })
                            
                        }else{
                            print("OOOOOOOOOOOOO","114")
                            LoginMark(user: authResult!.user)
                        }
                        
                        //                  return
                    }else{
                        let appDelegate = AppDelegate()
                        
                        appDelegate.resetApp()
                        print("OOOOOOOOOOOOOOOO","115",authResult?.user.displayName)
                        //                    LoginMark(user: authResult!.user)
                    }
                    // User is signed in to Firebase with Apple.
                    // ...
                }
            }
        }
        
        func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
            // Handle error.
            print("Sign in with Apple errored: \(error)")
        }
        
        
        //        func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        //            guard let credentials = authorization.credential as? ASAuthorizationAppleIDCredential else {
        //                print("credentials not found....")
        //                return
        //            }
        //
        //            let defaults = UserDefaults.standard
        //            defaults.set(credentials.user, forKey: "userId")
        //            parent?.name = "\(credentials.fullName?.givenName ?? "")"
        //        }
        //
        //        func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        //        }
    }
}
