//
//  EOTP.swift
//  Twaddan
//
//  Created by Spine on 02/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI

struct EOTP: View {
    
    @ObservedObject var backImagelinker = BackImageLinker()
    
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    
    var body: some View {
        
        
        
        GeometryReader { geometry in
            VStack {
                VStack {
                    
                    
                    HStack{
                        
                        
                        Button(action : {
                            
                            self.presentationMode.wrappedValue.dismiss()
                            
                            
                        }) {
                            Text("BACK")
                                .foregroundColor(Color.white)
                                .font(.custom("Regular_Font".localized(), size: 19))
                            
                        }
                        
                        //.hidden()
                        
                        
                        Spacer()
                        NavigationLink(destination:HomePageUS())
                        {
                            Text("SKIP")
                                .foregroundColor(Color.white)
                                .font(.custom("Regular_Font".localized(), size: 19))
                            
                        }
                        
                        
                        
                        
                        
                        
                    } .padding([.top], 60)
                    .padding([.leading, .trailing], 25)
                    
                    
                    
                    Spacer()
                    
                    HStack{
                        Image("Twaddan Logo").padding()
                        Spacer()
                    }
                    .padding(.bottom, 30.0)
                    
                    
                    
                    VStack{
                        
                        VStack(spacing:20){
                            
                            Spacer()
                            
                            
                            
                            
                            Text("Verification Code")
                                .foregroundColor(Color("MainFontColor1"))
                                .font(.custom("Regular_Font".localized(), size: 29))
                            
                            Text("Please type verification code sent to your Email")
                                .foregroundColor(Color("MainFontColor1"))
                                .font(.custom("Regular_Font".localized(), size: 19))
                                .multilineTextAlignment(.center)
                                .padding()
                            
                            Spacer()
                            
                            TextField("", text: /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Value@*/.constant("")/*@END_MENU_TOKEN@*/)
                                
                                .foregroundColor(Color.gray)
                                .font(.custom("ExtraBold_Font".localized(), size: 60))
                                .multilineTextAlignment(.center)
                            
                            Divider().background(Color.black)
                                .frame(height: 1)
                            
                            Spacer()
                            
                            
                            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/) {
                                
                                
                                RoundedPanel()
                                    
                                    .overlay(Text("Submit")
                                                .font(.custom("ExtraBold_Font".localized(), size: 20))
                                                
                                                .foregroundColor(Color.white))
                                    .background(Color(red: 0.24, green: 0.56, blue: 0.87, opacity: 1.0))
                                    .cornerRadius(5)
                                
                                
                                
                            }
                            
                            
                            
                            
                            
                            
                            Spacer()
                            
                            
                            
                        }
                        
                        .padding(.horizontal, 25.0)
                        Spacer()
                        
                        
                        
                        
                        
                        
                    }
                    .frame(maxWidth:.infinity,maxHeight:geometry.size.height*0.8)
                    .background(RoundedCorners(color: .white, tl: 30, tr: 30, bl: 0, br: 0))
                    
                    
                    
                    
                }
                .background(Image("basicbackimage")
                                .resizable()
                                
                                .clipped()
                                .scaledToFill())
                .edgesIgnoringSafeArea(.top)
                
                
                .navigationBarItems(trailing:Text(""))
                
            } .background(Image("basicbackimage").resizable())
            
            
            
            
            
            
        }
    }
}

struct EOTP_Previews: PreviewProvider {
    static var previews: some View {
        EOTP()
    }
}
