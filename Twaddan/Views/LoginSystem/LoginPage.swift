//
//  LoginPage.swift
//  Twaddan
//
//  Created by Spine on 29/04/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI
import FirebaseAuth
import FirebaseDatabase


struct LoginPage: View {
    
    @Binding var FromDA : Bool
    
    @State var ViewLoader = false
    
    @EnvironmentObject var settings: UserSettings
    
    @State var CNA : Bool = false
    
    @State var username : String = ""
    @State var password : String = ""
    @State var Pfailed:Bool = false
    @State var Ufailed:Bool = false
    @State var showsAlert = false
    @State var alert :String = ""
    @State var ErrorStatus:Bool=false
    @State var OpenMenu:Bool=false
    @State var openMenu:Bool=false
    @State var ErrorComments:String=""
    @State var Resend : Bool = false
    @State var openSignUp : Bool = false
    
    @ObservedObject var backImagelinker = BackImageLinker()
    @ObservedObject var signLink = SignLink()
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    let ref = Database.database().reference()
    
    @State var openDAP : Bool =  false
    
    
    var body: some View {
        
        
        //     username=settings.Email
        
        
        
        
        
        
        
        GeometryReader { geometry in
            
            
            ZStack{
                
                //
                //                    VStack{
                //                               VStack{
                //                                  EmptyView()
                //                               }  .background(AnimatedImage(url : URL (string: self.backImagelinker.url))
                //
                //
                //                                                                            .resizable().frame(width:geometry.size.width,height:geometry.size.height) // Resizable like SwiftUI.Image, you must use this modifier or the view will use the image bitmap size
                //                                                //                            .placeholder(UIImage(systemName: "basicbackimage")) // Placeholder Image
                //                                                                            // Supports ViewBuilder as well
                //
                //                                                                            .transition(.fade)
                //
                //
                //
                //                                                                            .clipped().brightness(-0.1)
                //                                                )
                //
                //                               }.background(Image("basicbackimage").resizable().frame(width:geometry.size
                //                                   .width,height:geometry.size.height)).brightness(-0.1)
                
                
                
                VStack {
                    VStack {
                        //                         Spacer()
                        //                                         .frame(height: geometry.size.height*0.05)
                        //                         HStack{
                        //
                        //
                        //
                        //
                        ////
                        ////                          Button(action : {
                        ////
                        ////                              self.showsAlert.toggle()
                        ////
                        ////                          //  self.Ufailed.toggle()
                        ////
                        ////
                        ////
                        ////                         //   print("cryftgbhjnlkm")
                        ////                           self.presentationMode.wrappedValue.dismiss()
                        ////
                        ////
                        ////                          }) {
                        ////                              Text("BACK")
                        ////                                .foregroundColor(.white)
                        ////                                .font(.custom("Regular_Font".localized(), size: 15)).hidden()
                        ////
                        ////
                        ////
                        ////                          }
                        ////
                        //
                        //
                        //
                        ////                          .alert(isPresented: self.$Pfailed){
                        ////                              Alert(title: Text("Failed"), message: Text("Please enter a password"), dismissButton: .default(Text("Got it!")))
                        ////                        }
                        //
                        //
                        //                          Spacer()
                        ////                            Button(action:{
                        ////
                        ////                                 let uuid = UIDevice.current.identifiexrForVendor?.uuidString
                        //////                                    print(uuid)
                        ////                                          self.ViewLoader = true
                        ////                                let user  =   Auth.auth().currentUser ;
                        ////                                 if(user?.email == nil && user?.uid != nil){
                        ////                                                        UserDefaults.standard.string(forKey: "Aid") ?? "0" = uuid ?? user?.uid as! String
                        ////                                    UserDefaults.standard.set(UserDefaults.standard.string(forKey: "Aid") ?? "0", forKey: "Aid")
                        ////
                        ////                                                                              self.OpenMenu=true
                        ////                                     self.ViewLoader = false
                        ////                                 }else{
                        ////
                        ////                                Auth.auth().signInAnonymously() { (authResult, error) in
                        ////                                  // ...
                        ////
                        ////
                        ////
                        ////                                    guard let user = authResult?.user else {
                        ////                                        print(error)
                        ////                                        return }
                        ////
                        ////
                        ////                                    let isAnonymous = user.isAnonymous  // true
                        ////                                    let uid = user.uid
                        ////
                        ////                                    UserDefaults.standard.string(forKey: "Aid") ?? "0" = uuid ?? uid
                        ////                                       UserDefaults.standard.set(UserDefaults.standard.string(forKey: "Aid") ?? "0", forKey: "Aid")
                        ////
                        ////                                      self.openMenu = true
                        ////                                    self.ViewLoader = false
                        ////
                        ////                                }
                        ////
                        ////                                }
                        ////
                        ////
                        ////                            })
                        ////                          {
                        ////                              Text("SKIP")
                        ////                                .foregroundColor(.white)
                        ////                                  .font(.custom("Regular_Font".localized(), size: 15))
                        ////
                        ////                          }
                        ////
                        ////
                        //
                        //
                        //
                        //
                        //                      }
                        //.padding([.top], 60)
                        
                        
                        
                        
                        //                    Spacer()
                        
                        
                        NavigationLink(destination:HomePage(),isActive: self.$OpenMenu){
                            EmptyView()
                            
                            
                        }
                        
                        NavigationLink(destination:SignUp(FromDA: self.$FromDA),isActive: self.$openSignUp){
                            EmptyView()
                            
                            
                        }
                        
                        NavigationLink(destination:DetailAddressPage(),isActive: self.$openDAP){
                            EmptyView()
                            
                            
                        }
                        
                        NavigationLink(destination:HomePage(),isActive: self.$openMenu){
                            Text("")
                            
                            
                        }
                        
                        
                        
                        
                        
                        VStack{
                            Spacer()
                            
                            VStack(spacing:0){
                                
                                Spacer().frame(height:200)
                                
                                HStack{
                                    
                                    Text("Continue with email")
                                        
                                        .foregroundColor(Color("darkthemeletter"))
                                        //                                .padding( .vertical, 50.0)
                                        .font(.custom("Regular_Font".localized(), size: 20))
                                    Spacer()
                                    
                                }.padding(.vertical).padding(.top,50)
                                Spacer()
                                
                                LoginPanel(username: self.$username, password: self.$password)
                                    .padding(.bottom, 30.0)
                                
                                if(self.ErrorStatus){
                                    
                                    Text(self.ErrorComments)
                                        .foregroundColor(.red) .font(.custom("Regular_Font".localized(), size: 13))
                                    
                                }
                                
                                Spacer()
                                HStack{
                                    Spacer()
                                    Button(action:{
                                        
                                        
                                        self.openSignUp = true
                                    }){
                                        Text("Create account")
                                            .foregroundColor(Color("s2"))
                                            .font(.custom("Regular_Font".localized(), size: 13))
                                    }
                                }.padding(.vertical,10)
                                
                                Button(action: {
                                    
                                    if(!self.username.contains("@")){
                                        
                                        self.ErrorComments="Please enter a valid username (email)";                                    self.ErrorStatus=true
                                        
                                        
                                        
                                    }else if(self.password == ""){
                                        
                                        self.ErrorComments="Please enter a valid password";                                    self.ErrorStatus=true
                                        
                                    }else{
                                        
                                        self.ViewLoader = true
                                        
                                        Auth.auth().signIn(withEmail: self.username, password: self.password) { (result, error) in
                                            
                                            self.ViewLoader = false
                                            
                                            guard error == nil else {
                                                
                                                self.ErrorComments=handleError(error: error!)
                                                self.ErrorStatus=true
                                                
                                                //    fatalError(handleError(error: error!))
                                                self.ViewLoader = false
                                                return
                                            }
                                            
                                            guard let user = result?.user else{
                                                self.ErrorComments="Fatel Error"
                                                self.ErrorStatus=true
                                                //   fatalError("Not user do not know what went wrong")
                                                
                                                return
                                            }
                                            
                                            guard   user.isEmailVerified  else{
                                                self.Resend = true
                                                self.ErrorComments="Please Verify Email Address"
                                                self.ErrorStatus=true
                                                //        fatalError("Email not verified")
                                                return
                                                
                                            }
                                            
                                            self.Resend = false
                                            //      print("Signed in user: \(user.email)")
                                            
                                            print(user.email!+" "+self.username)
                                            
                                            self.settings.Uid = user.uid as! String
                                            self.settings.Email = user.email as! String
                                            //       self.settings.Name = user?.displayName as! String
                                            
                                            
                                            let c=user.email as! String
                                            let A = c.replacingOccurrences(of: "@", with: "_")
                                            
                                            let B=A.replacingOccurrences(of: ".", with: "_")
                                            
                                            //                                                           UserDefaults.standard.string(forKey: "Aid") ?? "0" = B
                                            
                                            UserDefaults.standard.set(B, forKey: "Aid")
                                            self.ref.child("Users").child(B).child("LLT").setValue([".sv": "timestamp"])
                                            
                                            if(!self.FromDA){
                                                self.OpenMenu=true
                                            }else{
                                                self.FromDA = false
                                            }
                                            
                                            
                                        }
                                        
                                    }
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                }
                                
                                
                                
                                )
                                {
                                    
                                    
                                    
                                    HStack{
                                        
                                        
                                        
                                        Spacer()
                                        
                                        Text("LOGIN")   .font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color.white)
                                        Spacer()
                                        
                                        
                                        
                                    }.padding()
                                    //                            .border(Color("ManualSignInGray"), width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                                    
                                    //                                                               .frame(width: 300.0, height: 50)      .background(Color("su1"))
                                    //                                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color("su1"),lineWidth: 1))
                                    .background(Color("su1"))
                                    .cornerRadius(10)
                                    
                                    
                                    
                                }.alert(isPresented: self.$Ufailed) {
                                    Alert(title: Text("Sorry!"), message: Text(self.alert), dismissButton: .default(Text("Got it!")))
                                    
                                }
                                
                                ExtraOption(Email: self.$username,ErroComment:self.$ErrorComments, ErrorStatus:self.$ErrorStatus,Loader:self.$ViewLoader, Resend: self.$Resend).padding(.top,10)
                                
                                
                                
                                //                            NavigationLink(destination: SignUpSelection()
                                ////                                .navigationBarTitle("").navigationBarHidden(true).edgesIgnoringSafeArea(.all)
                                //                                    ){
                                //
                                //
                                //
                                //
                                //
                                //
                                //                                HStack{
                                //                                    Spacer()
                                //                                    Text("Creat New Account?")
                                //                                        .font(.custom("Regular_Font".localized(), size: 13))
                                //                                      .foregroundColor(Color("ManBackColor"))
                                //
                                //                                    }
                                //
                                //                            }
                                //
                                
                                
                                
                                Spacer()
                                
                                
                                
                            }
                            
                            .padding(.horizontal, 25.0)
                            
                            
                            
                            Spacer()
                            
                            VStack{
                                Text("").padding(.vertical,50.0)
                            }
                            
                            
                            
                            
                        }
                        .frame(maxWidth:.infinity,maxHeight:geometry.size.height*0.7)
                        .background(RoundedCorners(color: .white, tl: 30, tr: 30, bl: 0, br: 0))
                        
                        
                        
                        
                    }
                    
                    
                    
                    
                    Spacer()
                    
                }
                
                
                
                if(self.ViewLoader){
                    
                    Loader()
                }
            }
            .frame(width:geometry.size.width,height: geometry.size.height)
            
            .navigationBarTitle("Sign in")
            .edgesIgnoringSafeArea(.all)
            
            //                .navigationBarTitle("").navigationBarHidden(true)
        }
        //          .navigationBarTitle("Login Page",displayMode: .inline)
        //        .navigationBarHidden(true)
        //            .navigationBarItems(
        //                           trailing:
        //                               Button("SKIP") {
        //                                let uuid = UIDevice.current.identifierForVendor?.uuidString
        //                                //                                    print(uuid)
        //                                                                          self.ViewLoader = true
        //                                                                let user  =   Auth.auth().currentUser ;
        //                                                                 if(user?.email == nil && user?.uid != nil){
        //                                                                                        UserDefaults.standard.string(forKey: "Aid") ?? "0" = uuid ?? user?.uid as! String
        //                                                                    UserDefaults.standard.set(UserDefaults.standard.string(forKey: "Aid") ?? "0", forKey: "Aid")
        //
        //                                                                                                              self.OpenMenu=true
        //                                                                     self.ViewLoader = false
        //                                                                 }else{
        //
        //                                                                Auth.auth().signInAnonymously() { (authResult, error) in
        //                                                                  // ...
        //
        //
        //
        //                                                                    guard let user = authResult?.user else {
        //                                                                        print(error)
        //                                                                        return }
        //
        //
        //                                                                    let isAnonymous = user.isAnonymous  // true
        //                                                                    let uid = user.uid
        //
        //                                                                    UserDefaults.standard.string(forKey: "Aid") ?? "0" = uuid ?? uid
        //                                                                       UserDefaults.standard.set(UserDefaults.standard.string(forKey: "Aid") ?? "0", forKey: "Aid")
        //
        //                                                                      self.OpenMenu = true
        //                                                                    self.ViewLoader = false
        //
        //                                                                }
        //
        //                                                                }
        //
        //                               }
        //                       )
        
        
        //          }
        //        ].background(Color.red)
        .edgesIgnoringSafeArea(.all)
        //        .navigationBarBackButtonHidden(true)
        //
        
        
        
        
        
    }
}

struct LoginPage_Previews: PreviewProvider {
    static var previews: some View {
        LoginPage(FromDA: .constant(false))
        //.environment(\.colorScheme, .dark)
        
        //            .environment(\.locale,Locale(identifier: "ar")).environment(\.layoutDirection, .rightToLeft)
    }
}

struct RoundedCorners: View {
    var color: Color = .blue
    var tl: CGFloat = 0.0
    var tr: CGFloat = 0.0
    var bl: CGFloat = 0.0
    var br: CGFloat = 0.0
    
    var body: some View {
        GeometryReader { geometry in
            Path { path in
                
                let w = geometry.size.width
                let h = geometry.size.height
                
                // Make sure we do not exceed the size of the rectangle
                let tr = min(min(self.tr, h/2), w/2)
                let tl = min(min(self.tl, h/2), w/2)
                let bl = min(min(self.bl, h/2), w/2)
                let br = min(min(self.br, h/2), w/2)
                
                path.move(to: CGPoint(x: w / 2.0, y: 0))
                path.addLine(to: CGPoint(x: w - tr, y: 0))
                path.addArc(center: CGPoint(x: w - tr, y: tr), radius: tr, startAngle: Angle(degrees: -90), endAngle: Angle(degrees: 0), clockwise: false)
                path.addLine(to: CGPoint(x: w, y: h - br))
                path.addArc(center: CGPoint(x: w - br, y: h - br), radius: br, startAngle: Angle(degrees: 0), endAngle: Angle(degrees: 90), clockwise: false)
                path.addLine(to: CGPoint(x: bl, y: h))
                path.addArc(center: CGPoint(x: bl, y: h - bl), radius: bl, startAngle: Angle(degrees: 90), endAngle: Angle(degrees: 180), clockwise: false)
                path.addLine(to: CGPoint(x: 0, y: tl))
                path.addArc(center: CGPoint(x: tl, y: tl), radius: tl, startAngle: Angle(degrees: 180), endAngle: Angle(degrees: 270), clockwise: false)
            }
            .fill(self.color)
        }
    }
}


struct RoundedCornersStroke: View {
    var color: Color = .blue
    var tl: CGFloat = 0.0
    var tr: CGFloat = 0.0
    var bl: CGFloat = 0.0
    var br: CGFloat = 0.0
    var lineWidth: CGFloat = 0.0
    
    
    var body: some View {
        GeometryReader { geometry in
            Path { path in
                
                let w = geometry.size.width
                let h = geometry.size.height
                
                // Make sure we do not exceed the size of the rectangle
                let tr = min(min(self.tr, h/2), w/2)
                let tl = min(min(self.tl, h/2), w/2)
                let bl = min(min(self.bl, h/2), w/2)
                let br = min(min(self.br, h/2), w/2)
                
                var starter = 0.0
                
                if(self.tl != 0){
                    starter = Double(w / 2.0)
                }
                
                path.move(to: CGPoint(x: starter, y: 0))
                path.addLine(to: CGPoint(x: w - tr, y: 0))
                path.addArc(center: CGPoint(x: w - tr, y: tr), radius: tr, startAngle: Angle(degrees: -90), endAngle: Angle(degrees: 0), clockwise: false)
                path.addLine(to: CGPoint(x: w, y: h - br))
                path.addArc(center: CGPoint(x: w - br, y: h - br), radius: br, startAngle: Angle(degrees: 0), endAngle: Angle(degrees: 90), clockwise: false)
                path.addLine(to: CGPoint(x: bl, y: h))
                path.addArc(center: CGPoint(x: bl, y: h - bl), radius: bl, startAngle: Angle(degrees: 90), endAngle: Angle(degrees: 180), clockwise: false)
                path.addLine(to: CGPoint(x: 0, y: tl))
                path.addArc(center: CGPoint(x: tl, y: tl), radius: tl, startAngle: Angle(degrees: 180), endAngle: Angle(degrees: 270), clockwise: false)
            }
            .stroke(self.color,lineWidth: self.lineWidth)
        }
    }
}




struct LoginPanel: View {
    
    @Binding var username : String
    @Binding var password : String
    @State var View = false
    
    var body: some View {
        VStack(){
            
            
            TextField("Email", text: $username)
                
                
                .foregroundColor(Color("darkthemeletter"))
                .font(.custom("Regular_Font".localized(), size: 14))
            Divider()
                
                .frame(height: 1)
            
            
            
            
            
            //    Spacer()
            
            HStack{
                
                if(self.View){
                    
                    TextField("Password", text: $password)
                        
                        .foregroundColor(Color("darkthemeletter"))                .font(.custom("Regular_Font".localized(), size: 14))
                }else{
                    SecureField("Password", text: $password)
                        
                        .foregroundColor(Color("darkthemeletter"))                .font(.custom("Regular_Font".localized(), size: 14))
                    
                }
                
                Spacer()
                Button(action:{
                    
                    self.View.toggle()
                }){
                    Image("Group 395") .foregroundColor(Color.black).font(.custom("Regular_Font".localized(), size: 14))
                }
            }
            
            
            
            Divider()
                .frame(height: 1)
            
            
            
        }
    }
}

struct ExtraOption: View {
    @EnvironmentObject var settings: UserSettings
    @Binding var Email : String
    @Binding var ErroComment: String
    @Binding var ErrorStatus: Bool
    @Binding var Loader : Bool
    @Binding var Resend : Bool
    var body: some View {
        HStack{
            
            HStack{
                
                //  Image(systemName:"square").hidden()
                if(Resend){
                    
                    Button(action:{
                        Auth.auth().currentUser?.sendEmailVerification { (error) in
                            
                            if(error == nil){
                                self.ErroComment = "Verification mail send again"
                                self.ErrorStatus = true
                            }else{
                                
                                
                            }
                            
                        }
                        
                    }){
                        
                        
                        Text("Resend Verificaion Mail")
                            .foregroundColor(Color("su1"))
                            .font(.custom("Regular_Font".localized(), size: 13))
                    }
                    
                }
                
            }
            
            Spacer()
            Button(action:{
                
                if(self.Email.contains("@") && self.Email.contains(".")){
                    
                    self.Loader = true
                    Auth.auth().sendPasswordReset(withEmail: self.Email) { error in
                        
                        self.Loader = false
                        
                        if(error != nil )
                        
                        {
                            self.ErroComment = error?.localizedDescription as! String
                            
                        }else{
                            self.ErroComment = "Reset Password is sent to email address"
                        }
                        self.ErrorStatus = true
                        
                    }
                }else{
                    self.ErroComment = "Inavalid username"
                    self.ErrorStatus = true
                }
                
            }){
                Text("Forgot password ?")
                    .foregroundColor(Color("su1"))
                    .font(.custom("Regular_Font".localized(), size: 13))
            }
            
            
        }
        
    }
}


struct RoundedPanel: View {
    var body: some View {
        
        ZStack{
            HStack{
                Spacer().frame(width:0)
                RoundedCorners(color: Color("c2"), tl: 200, tr: 0, bl: 0, br: 0) .frame(height: 80)
                
            }
            
            
        }
        
        
        
        
        
        
        
        
    }
}

//
//extension View {
//    func toast<Content>(isPresented: Binding<Bool>, content: @escaping () -> Content) -> some View where Content: View {
//        Toast(
//            isPresented: isPresented,
//            presenter: { self },
//            content: content
//        )
//    }
//}
//struct Toast<Presenting, Content>: View where Presenting: View, Content: View {
//    @Binding var isPresented: Bool
//    let presenter: () -> Presenting
//    let content: () -> Content
//    let delay: TimeInterval = 2
//
//    var body: some View {
//        if self.isPresented {
//            DispatchQueue.main.asyncAfter(deadline: .now() + self.delay) {
//                withAnimation {
//                    self.isPresented = false
//                }
//            }
//        }
//
//        return GeometryReader { geometry in
//            ZStack(alignment: .bottom) {
//                self.presenter()
//
//                ZStack {
//                    Capsule()
//                        .fill(Color.gray)
//
//                    self.content()
//                } //ZStack (inner)
//                .frame(width: geometry.size.width / 1.25, height: geometry.size.height / 10)
//                .opacity(self.isPresented ? 1 : 0)
//            } //ZStack (outer)
//            .padding(.bottom)
//        } //GeometryReader
//    } //body
//} //Toast
//func handleError(error: Error) -> String {
//    
//    /// the user is not registered
//    /// user not found
//    
//    var errorStatus : String=""
//    let errorAuthStatus = AuthErrorCode.init(rawValue: error._code)!
//    
//    
//    switch errorAuthStatus {
//    case .wrongPassword:
//        errorStatus = "wrongPassword"
//        print("wrongPassword")
//        
//    case .invalidEmail:
//        errorStatus = "invalidEmail"
//        print("invalidEmail")
//    case .operationNotAllowed:
//        errorStatus = "operationNotAllowed"
//        print("operationNotAllowed")
//    case .userDisabled:
//        errorStatus = "userDisabled"
//        print("userDisabled")
//    case .userNotFound:
//        errorStatus = "userNotFound"
//        print("userNotFound")
//    //            self.register(auth: Auth.auth())
//    case .tooManyRequests:
//        errorStatus = "tooManyRequests, oooops"
//        print("tooManyRequests, oooops")
//    default: fatalError("error not supported here")
//    }
//    
//    return errorStatus
//    
//}
