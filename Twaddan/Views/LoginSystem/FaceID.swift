//
//  FaceID.swift
//  Twaddan
//
//  Created by Spine on 11/09/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import LocalAuthentication


struct FaceID: View {
    
    @State var goHome : Bool = false
    @EnvironmentObject var settings : UserSettings
    
    @EnvironmentObject var vehicleItems : VehicleItems
    
    
    var body: some View {
        GeometryReader{ geometry in
            
            ZStack{
                //        if(!self.localauthsuccess){
                
                //
                
                
                
                VStack(alignment: .center){
                    
                    //  Text("\(second)")
                    
                    //                                PlayerView()
                    //                                    .frame(width: geometry.size.width)
                    
                    //   player()
                    
                    
                    
                    HStack{
                        Image("Group 780")
                        Spacer()
                    }
                    
                    Spacer()
                    
                    //                                Image("Group 782")
                    //
                    //                                Spacer()
                    
                    //                                              Image("Group 783")
                    //
                    //                                              Spacer()
                    
                }
                
                
                NavigationLink(destination:HomePage().environmentObject(self.vehicleItems).environmentObject(self.settings),isActive: self.$goHome){
                    Text("")
                    
                    
                }
                
                
                HStack(alignment:.center){
                    Spacer()
                    VStack(alignment:.center){
                        Spacer()
                        
                        Image("pngwave").resizable().frame(width: 150,height: 150).padding().foregroundColor(Color.white).background(Color("lightgray")).clipped().shadow(radius: 5).onTapGesture {
                            
                            let context = LAContext()
                            var error: NSError?
                            print(1)
                            // check whether biometric authentication is possible
                            if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
                                // it's possible, so go ahead and use it
                                let reason = "We need to use the app"
                                print(1)
                                context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, authenticationError in
                                    // authentication has now completed
                                    print(1)
                                    DispatchQueue.main.async {
                                        if success {
                                            // authenticated successfully
                                            //                                                  print(4)
                                            self.goHome = true
                                            
                                        }
                                    }
                                }
                            }else{
                                self.goHome = true
                                
                                //
                                //                                    let ac = UIAlertController(title: "Biometry unavailable", message: "Your device is not configured for biometric authentication.", preferredStyle: .alert)
                                //                                    ac.addAction(UIAlertAction(title: "OK", style: .default))
                                //                                    self.present(ac, animated: true)
                                
                                
                            }
                        }
                        
                        Text("You can't use application without Proper authentication").foregroundColor(Color("darkthemeletter")).font(.custom("Regular_Font".localized(), size: 17)).multilineTextAlignment(.center).padding()
                        Spacer()
                    }
                    Spacer()
                    
                }.onAppear{
                    
                    
                    
                    
                    let context = LAContext()
                    var error: NSError?
                    print(1)
                    // check whether biometric authentication is possible
                    if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
                        // it's possible, so go ahead and use it
                        let reason = "We need to use the app"
                        print(1)
                        context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, authenticationError in
                            // authentication has now completed
                            print(1)
                            DispatchQueue.main.async {
                                if success {
                                    // authenticated successfully
                                    //                                    print(4)
                                    self.goHome = true
                                    
                                }
                            }
                        }
                    }
                    
                    
                    
                }
                
                //        }
            }.navigationBarTitle("").navigationBarHidden(true).navigationBarBackButtonHidden(true).edgesIgnoringSafeArea(.all)
            
        }
    }
}

struct FaceID_Previews: PreviewProvider {
    static var previews: some View {
        FaceID()
    }
}
