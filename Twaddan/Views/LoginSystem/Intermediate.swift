//
//  Intermediate.swift
//  Twaddan
//
//  Created by Spine on 28/08/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import FirebaseDatabase


struct Intermediate: View {
    
    @State var openCart : Bool = false
    @State var gotToBookDetails : Bool = false
    @State var openVA : Bool = true
    @EnvironmentObject var vehicleItems : VehicleItems
    @EnvironmentObject var settings : UserSettings
    var spid : String
    
    var body: some View {
        BookingDetails(OpenCart:self.$openCart , GoToBookDetails: self.$gotToBookDetails, serviceSnap: DataSnapshot.init(), SelectedDriver: self.spid, ETA: -1,openVA: self.$openVA).environmentObject(self.settings).environmentObject(self.vehicleItems)
        
    }
}

struct Intermediate_Previews: PreviewProvider {
    static var previews: some View {
        Intermediate(spid: "")
    }
}
