//
//  LangSelection.swift
//  Twaddan
//
//  Created by Spine on 10/07/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI
import FirebaseAuth
import SwitchLanguage

struct LangSelection: View {
    
    @State var OM = false
    @State var ViewLoader = false
    @State var Click = false
    
    
    @EnvironmentObject var settings: UserSettings
    @ObservedObject var backImagelinker = BackImageLinker()
    
    var body: some View {
        GeometryReader{ geometry in
            
            ZStack{
                
                VStack{
                    
                    NavigationLink(destination:HomePage()   .environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                    )),isActive: self.$OM){
                        Text("")
                        
                        
                    }
                }
                VStack{
                    Spacer()
                    
                    Image("Group 788")
                    //                    .background(AnimatedImage(url : URL (string: self.backImagelinker.url))
                    //
                    //
                    //                                                               .resizable().frame(width:geometry.size.width,height:geometry.size.height) // Resizable like SwiftUI.Image, you must use this modifier or the view will use the image bitmap size
                    //                                   //                            .placeholder(UIImage(systemName: "basicbackimage")) // Placeholder Image
                    //                                                               // Supports ViewBuilder as well
                    //
                    //                                                               .transition(.fade)
                    //
                    //
                    //
                    //                                                               .clipped()
                    //                                                               )
                    
                    
                    
                    //                .background(Image("basicbackimage").resizable().frame(width:geometry.size.width,height:geometry.size.height))
                    //                .brightness(-0.2)
                    //
                    //                HStack{
                    //                    Image("Twaddan Logo").resizable().frame(width : 40*5.4,height:40).padding()
                    //
                    //                                    }
                    Spacer().frame(height:100)
                    
                    
                    VStack{
                        
                        
                        
                        
                        Text("Select your language").font(.custom("Bold_Font".localized(), size: 17)) .font(Font.body.bold()) .padding(.top,30) .foregroundColor(Color("c1"))
                        
                        
                        HStack{
                            
                            Spacer()
                            
                            Button(action:{
                                UserDefaults.standard.set("en", forKey: "LANSEL")
                                Language.setCurrentLanguage("en")
                                UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
                                UserDefaults.standard.synchronize()
                                let appDelegate = AppDelegate()
                                appDelegate.resetApp()
                                
                                
                                //                            let Old = UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                                //
                                //                              UserDefaults.standard.set("en", forKey: "LANSEL")
                                //                            Language.setCurrentLanguage("en")
                                //                                                     UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
                                //                                                     UserDefaults.standard.synchronize()
                                //
                                //                            if(Old == "en"){
                                //                                self.ViewLoader = true
                                //                            self.Click = true
                                //
                                //
                                //
                                //                       self.OM = true
                                //                            }else{
                                //                            let appDelegate = AppDelegate()
                                //
                                //                                                 appDelegate.resetApp()
                                //                            }
                                //
                                
                            }){
                                
                                Text("Engish").font(.custom("Regular_Font".localized(), size: 16))
                                    .foregroundColor(Color("c2"))
                                    
                                    // .padding(.horizontal,40)
                                    .frame(width : 100, height: 50)
                                
                                
                                
                                
                                
                            }
                            
                            Button(action:{
                                
                                UserDefaults.standard.set("ar", forKey: "LANSEL")
                                Language.setCurrentLanguage("ar")
                                UserDefaults.standard.set(["ar"], forKey: "AppleLanguages")
                                UserDefaults.standard.synchronize()
                                let appDelegate = AppDelegate()
                                appDelegate.resetApp()
                                //
                                //                              let Old = UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                                //
                                //
                                //                              UserDefaults.standard.set("ar", forKey: "LANSEL")
                                //
                                //                            Language.setCurrentLanguage("ar")
                                //                            UserDefaults.standard.set(["ar"], forKey: "AppleLanguages")
                                //                            UserDefaults.standard.synchronize()
                                //
                                //                             if(Old == "ar"){
                                //                            self.ViewLoader = true
                                //                                                      self.OM = true
                                //                             }else{
                                //                            let appDelegate = AppDelegate()
                                //                            appDelegate.resetApp()
                                //                            }
                                
                            }){
                                Text("اللغة العربية").font(.custom("Regular_Font".localized(), size: 16))
                                    .foregroundColor(Color("c2"))
                                    
                                    // .padding(.horizontal,40)
                                    .frame(width : 100, height: 50)
                                
                                
                                
                            }
                            Spacer()
                        }
                        
                        
                        
                        
                    }
                    
                    Spacer()
                }
                
                if(self.ViewLoader){
                    Loader()
                }
                
            }
        }.navigationBarTitle("")
        .navigationBarBackButtonHidden(true)
        
        .navigationBarHidden(true)
        .edgesIgnoringSafeArea(.all)
        
    }
}

struct LangSelection_Previews: PreviewProvider {
    static var previews: some View {
        LangSelection()
    }
}
