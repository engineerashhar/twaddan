//
//  SignUpSelection.swift
//  Twaddan
//
//  Created by Spine on 01/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import GoogleSignIn
import SDWebImageSwiftUI
import GoogleSignIn
import FirebaseDatabase
import FirebaseAuth
import SwiftUI
import FBSDKLoginKit
import Firebase
import AuthenticationServices
import CryptoKit

struct SignUpSelection: View {
    
    
    var GOOGLE =  googlesign()
    
    @Binding var FromDA : Bool
    
    @State var ViewLoader : Bool = false
    @ObservedObject var backImagelinker = BackImageLinker()
    // @ObservedObject var loginListener = LoginListner()
    
    @EnvironmentObject var settings : UserSettings
    @State private var openMenu:Bool=false
    @State var openSignUp = false
    @State var openLogin = false
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let ref = Database.database().reference()
    @State var openDAP : Bool =  false
    @State var GuestButtonKey : String =  ""
    
    
    var body: some View {
        
        
        
        GeometryReader { geometry in
            
            
            
            ZStack{
                
                
                
                
                
                NavigationLink(destination:DetailAddressPage(),isActive: self.$openDAP){
                    EmptyView()
                    
                    
                }
                
                NavigationLink(destination: LoginPage(FromDA: self.$FromDA)
                               //                                                                     .navigationBarTitle("").navigationBarHidden(true).edgesIgnoringSafeArea(.all)
                               ,isActive: self.$openSignUp){
                    EmptyView().frame(height:0)
                }
                
                
                NavigationLink(destination: LoginPage(FromDA: self.$FromDA)
                               //                                                                     .navigationBarTitle("").navigationBarHidden(true).edgesIgnoringSafeArea(.all)
                               ,isActive: self.$openLogin){
                    EmptyView().frame(height:0)
                }
                VStack {
                    VStack {
                        //                        Spacer()
                        //                                                .frame(height: geometry.size.height*0.05)
                        
                        Text("Login").hidden()
                            
                            .foregroundColor(Color.black)
                            
                            .font(.custom("Regular_Font".localized(), size: 16))
                            .padding(.bottom,40)
                        
                        Spacer()
                        
                        Image("Group 782")
                        
                        HStack{
                            
                            NavigationLink(destination:HomePage()
                                           
                                           ,isActive: self.$openMenu){
                                EmptyView()
                                
                                
                            }
                            
                            //
                            //                        Button(action : {
                            //
                            //                            self.presentationMode.wrappedValue.dismiss()
                            //
                            //
                            //                        }) {
                            //                            Text("BACK")
                            //                                .foregroundColor(Color.white)
                            //                                .font(.custom("Regular_Font".localized(), size: 15))
                            //
                            //                        }
                            ////
                            //
                            //                        Spacer()
                            //                     Button(action:{
                            //
                            //                                                      let uuid = UIDevice.current.identifierForVendor?.uuidString
                            //                     //                                    print(uuid)
                            //                                                               self.ViewLoader = true
                            //                                                     let user  =   Auth.auth().currentUser ;
                            //                                                      if(user?.email == nil && user?.uid != nil){
                            //                                                                             self.settings.Aid = uuid ?? user?.uid as! String
                            //                                                         UserDefaults.standard.set(self.settings.Aid, forKey: "Aid")
                            //
                            //                                                                                                   self.openMenu=true
                            //                                                          self.ViewLoader = false
                            //                                                      }else{
                            //
                            //                                                     Auth.auth().signInAnonymously() { (authResult, error) in
                            //                                                       // ...
                            //
                            //
                            //
                            //                                                         guard let user = authResult?.user else {
                            //                                                             print(error)
                            //                                                             return }
                            //
                            //
                            //                                                         let isAnonymous = user.isAnonymous  // true
                            //                                                         let uid = user.uid
                            //
                            //                                                         self.settings.Aid = uuid ?? uid
                            //                                                            UserDefaults.standard.set(self.settings.Aid, forKey: "Aid")
                            //
                            //                                                           self.openMenu = true
                            //                                                         self.ViewLoader = false
                            //
                            //                                                     }
                            //
                            //                                                     }
                            //
                            //
                            //                                                 })
                            //                        {
                            //                            Text("SKIP")
                            //                                .foregroundColor(Color.white)
                            //                                .font(.custom("Regular_Font".localized(), size: 15))
                            //
                            //                        }
                            //
                            
                            
                            
                            
                            
                        }
                        
                        
                        
                        
                        VStack{
                            
                            VStack(spacing:10){
                                
                                //  Spacer()
                                
                                Spacer()
                                Text("Login or Create an account")
                                    
                                    .foregroundColor(Color.black)
                                    
                                    .font(.custom("Regular_Font".localized(), size: 20)).frame(width:280)
                                
                                Text("Login or create an account to receive rewards and save your details for a faster service")
                                    
                                    .foregroundColor(Color.black)
                                    
                                    .font(.custom("Regular_Font".localized(), size: 14))
                                    .frame(width:280).multilineTextAlignment(.center)
                                    
                                    .padding(.bottom,30)
                                
                                //  Spacer()
                                
                                //                            google()
                                //                                .frame(width: 307, height:50)
                                //                                .shadow(radius: 1)
                                //
                                //
                                //                            FaceBookLoginView()
                                //                                .frame(width: 300, height: 50)
                                //                                .shadow(radius: 3)
                                //                                .onTapGesture {
                                //                                    self.presentationMode.wrappedValue.dismiss()
                                //                            }
                                
                                
                                
                                
                                // Text(self.settings.loggedIn ? "IN" : "OUT" )
                                //                            NavigationLink(destination:SplashScreen()
                                //
                                //                            ,isActive:self.$settings.loggedIn ) {
                                //                                Text("")
                                //                            }
                                //                            google()
                                
                                Button(action: {
                                    
                                    //                                let firebaseAuth = Auth.auth()
                                    //                                                              do {
                                    //                                                                try firebaseAuth.signOut()
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //                                                              } catch let signOutError as NSError {
                                    //                                                                print ("Error signing out: %@", signOutError)
                                    //                                                              }
                                    
                                    
                                    
                                    
                                    //                                SocialLogin().attemptLoginGoogle()
                                    print("QQQQQQQQQQQQQ","1")
                                    //                                self.GOOGLE.signIn()
                                    Social().attemptLoginGoogle()
                                    
                                    self.ViewLoader = true
                                    
                                    
                                    //
                                    //                                               Auth.auth().addStateDidChangeListener { (auth, user) in
                                    //                                print("LISTNER CALLED")
                                    //                                                if Auth.auth().currentUser != nil {
                                    //                                                    // User is signed in.
                                    //                                                    // ...
                                    //
                                    //
                                    //                                                    print("LISTNER CALLED")
                                    //
                                    //                                                    let user = Auth.auth().currentUser
                                    //
                                    //
                                    //                                                    if let user = user {
                                    //
                                    //                                                        var EMAIL:String = ""
                                    //                                                        if(user.email != nil && user.email != ""){
                                    //                                                            EMAIL = user.email!
                                    //
                                    //                                                        }else{
                                    //
                                    //                                                            EMAIL = Auth.auth().currentUser?.providerData[0].email as! String
                                    //
                                    //                                                        }
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //                                                        self.settings.Uid = user.uid as! String
                                    //                                                        self.settings.Email = EMAIL
                                    //                                                        //       self.settings.Name = user?.displayName as! String
                                    //
                                    //
                                    //                                                        let c=EMAIL as! String
                                    //                                                        let A = c.replacingOccurrences(of: "@", with: "_")
                                    //
                                    //                                                        let B=A.replacingOccurrences(of: ".", with: "_")
                                    //
                                    //
                                    //                                                        UserDefaults.standard.set(B, forKey: "Aid")
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //                                                        if(!self.FromDA){
                                    //                                                            //                                                                                                                                        self.FromDA = false
                                    //                                                            self.openMenu=true
                                    //                                                        }else{
                                    //                                                            self.FromDA = false
                                    //                                                        }
                                    //                                                        print("WERT CALLED")
                                    //                                                        var multiFactorString = "MultiFactor: "
                                    //                                                        for info in user.multiFactor.enrolledFactors {
                                    //                                                            multiFactorString += info.displayName ?? "[DispayName]"
                                    //                                                            multiFactorString += " "
                                    //
                                    //                                                        }
                                    //
                                    //                                                        var FirstName :String = ""
                                    //
                                    //                                                        var SecondName : String = ""
                                    //
                                    //                                                        let items = user.displayName!.components(separatedBy: " ")
                                    //
                                    //                                                        FirstName = items[0]
                                    //
                                    //
                                    //                                                        var i:Int = 1
                                    //                                                        while i<items.count {
                                    //
                                    //                                                            SecondName += items[i]
                                    //                                                            i+=x1
                                    //                                                        }
                                    //
                                    //                                                        self.ref.child("Users").child(B).observeSingleEvent(of: DataEventType.value, with: {
                                    //                                                            (datansnapshot) in
                                    //
                                    //                                                            if(datansnapshot.exists()){
                                    //
                                    //                                                                let P : [String : Any]=[
                                    //
                                    //                                                                    "email" : EMAIL,
                                    //                                                                    "lastName": SecondName,
                                    //                                                                    "name" : FirstName ,
                                    //                                                                    "phoneNumber":"",
                                    //                                                                    "dp":String(describing :(user.photoURL!)),
                                    //                                                                    "RT" : [".sv": "timestamp"]
                                    //                                                                ]
                                    //
                                    //                                                                self.ref.child("Users").child(B).child("personal_details").updateChildValues(P)
                                    //                                                            }
                                    //                                                            else{
                                    //
                                    //                                                                let P : [String : Any]=[
                                    //
                                    //                                                                    "email" : EMAIL,
                                    //                                                                    "lastName": SecondName,
                                    //                                                                    "name" : FirstName,
                                    //
                                    //                                                                    "dp":String(describing :(user.photoURL!)),
                                    //
                                    //                                                                ]
                                    //                                                                self.ref.child("Users").child(B).child("personal_details").updateChildValues(P)
                                    //
                                    //
                                    //                                                            }
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //                                                            let email = user.email
                                    //                                                            let photoURL = user.photoURL
                                    //
                                    //                                                        }
                                    //                                                            // ...So
                                    //                                                        )
                                    //
                                    //                                                    }
                                    //
                                    //                                                } else {
                                    //                                                     // No user is signed in.
                                    //                                                      print("LISTNER ERGHSDF")
                                    ////                                                    self.ViewLoader = false
                                    //                                                     // ...
                                    //                                                   }
                                    //                                               }
                                    
                                    
                                    
                                    
                                }){
                                    
                                    HStack{
                                        
                                        HStack{
                                            Image("google")
                                                .renderingMode(.original)
                                                .resizable()
                                                .frame(width: 15, height: 15)
                                                
                                                // .foregroundColor(Color.white)
                                                
                                                .padding(.all, 10.0)
                                            
                                        }.background(Color.white)
                                        
                                        .cornerRadius(10)
                                        
                                        .padding(.leading, 5.0)
                                        
                                        
                                        Spacer().frame(width: 50)
                                        
                                        Text("Continue with Google")   .font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color.white)
                                        Spacer()
                                        
                                        
                                        
                                    }.padding()
                                    //                            .border(Color("ManualSignInGray"), width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                                    
                                    //                                                               .frame(width: 300.0, height: 50)      .background(Color("su1"))
                                    //                                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color("su1"),lineWidth: 1))
                                    .background(Color("su1"))
                                    .cornerRadius(10)
                                    
                                    //                                                           .clipped()
                                    //                                                               .shadow(radius: 2)
                                    
                                }
                                
                                
                                
                                Button(action: {
                                    
                                    //                                let firebaseAuth = Auth.auth()
                                    //                                 do {
                                    //                                   try firebaseAuth.signOut()
                                    //
                                    //
                                    //                                 } catch let signOutError as NSError {
                                    //                                   print ("Error signing out: %@", signOutError)
                                    //                                 }
                                    
                                    SocialLogin().attemptLoginFb(completion: { (result, error) in
                                        
                                        
                                        
                                        
                                        self.ViewLoader = true
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                    })
                                    
                                    
                                    
                                    
                                    
                                    
                                }){
                                    
                                    HStack{
                                        
                                        HStack{
                                            Image("f2")
                                                .renderingMode(.original)
                                                .resizable()
                                                .frame(width: 15, height: 22)
                                                // .foregroundColor(Color.white)
                                                
                                                .padding(.all, 7)
                                            
                                        }
                                        //                                        .background(Color("facebookcolor"))
                                        .cornerRadius(10)
                                        
                                        .padding(.leading, 5.0)
                                        
                                        
                                        Spacer().frame(width: 50)
                                        
                                        Text("Continue with Facebook") .font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color("darkthemeletter"))
                                        Spacer()
                                        
                                        
                                        
                                    }.padding()
                                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color("s1"),lineWidth: 1))
                                }
                                
                                
                                
                                
                                
                                
                                Button(action:{
                                    
                                    self.openSignUp = true
                                })
                                
                                {
                                    
                                    HStack{
                                        
                                        HStack{
                                            Image("emailicon")
                                                .resizable()
                                                .frame(width: 20, height: 20)
                                                .foregroundColor(Color.black)
                                                
                                                .padding(.all, 10.0)
                                            
                                        }
                                        .cornerRadius(10)
                                        
                                        .padding(.leading, 5.0)
                                        
                                        
                                        Spacer().frame(width: 50)
                                        
                                        Text("Continue with email") .font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color("darkthemeletter"))
                                        Spacer()
                                        
                                        
                                        
                                    }
                                    .padding()
                                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color("s1"),lineWidth: 1))
                                }
                                
                                //
                                //                            Button(action:{
                                //
                                //
                                //
                                //
                                //                                SocialLogin().makeCoordinator().didTapButton()
                                //
                                //                                                       })
                                ////
                                //                                                       {
                                //
                                //                                                               HStack{
                                //
                                //                                                                   HStack{
                                //                                                                       Image("apple")
                                //                                                                           .resizable()
                                //                                                                           .frame(width: 17, height: 20)
                                //                                                                           .foregroundColor(Color.black)
                                //
                                //                                                                           .padding(.all, 10.0)
                                //
                                //                                                                   }
                                //                                                                       .cornerRadius(10)
                                //
                                //                                                                       .padding(.leading, 5.0)
                                //
                                //
                                //                                                                  Spacer().frame(width: 50)
                                //
                                //                                                                   Text("Continue with Apple") .font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color("darkthemeletter"))
                                //                                                                   Spacer()
                                //
                                //
                                //
                                //                                                                }
                                //                                                                   .padding()
                                //                                                                   .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color("s1"),lineWidth: 1))
                                //                                                       }
                                
                                SignInWithApple().frame(height:70).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                                ))
                                //                            Button(action:{
                                //                                                      self.openLogin = true
                                //
                                //                                                  }){
                                //                                                 HStack{
                                ////                                                                                    Spacer()
                                //                                                                                    Text("Already have an account?  Sign in")
                                //                                                                                        .font(.custom("Regular_Font".localized(), size: 13))
                                //                                                                                      .foregroundColor(Color("ManBackColor"))
                                //
                                //                                                                                    }
                                //                                                  }
                                if(true){
                                    HStack{
                                        //                                                     Spacer()
                                        Button(action:{
                                            if(self.FromDA){
                                                self.FromDA = false
                                            }else{
                                                //                                                            self.presentationMode.wrappedValue.dismiss()
                                                self.openMenu = true
                                            }
                                            
                                        }){
                                            Text("Login as Guest")
                                                
                                                
                                                
                                                .font(.custom("Regular_Font".localized(), size: 16))
                                        }
                                    }.padding(.top).padding(.horizontal)
                                }
                                
                                Spacer()
                                
                                
                                
                            }
                            
                            .padding(.horizontal, 25.0)
                            Spacer()
                            
                            
                            
                            
                        }.frame(maxWidth:.infinity ,maxHeight: geometry.size.height*0.7)
                        .background(RoundedCorners(color: .white, tl: 30, tr: 30, bl: 0, br: 0))
                        
                        
                        
                        
                    }
                    
                    
                    
                    
                    
                    
                }
                
                
                
                
                if(self.ViewLoader){
                    Loader()
                }
            }.onDisappear{
                
                self.ViewLoader = false
            }.onAppear{
                if(self.FromDA){
                    self.GuestButtonKey = "Guest"
                }
                
                //
                //                    let firebaseAuth = Auth.auth()
                //                                                                                 do {
                //                                                                                   try firebaseAuth.signOut()
                //
                //
                //
                //
                //
                //
                //
                //
                //                                                                                 } catch let signOutError as NSError {
                //                                                                                   print ("Error signing out: %@", signOutError)
                //                                                                                 }
                
                
                
                Auth.auth().addStateDidChangeListener { (auth, user) in
                    print("LISTNER CALLED")
                    if user != nil {
                        
                        if !user!.isAnonymous{
                            // User is signed in.
                            // ...
                            
                            print("LISTNER CALLED")
                            
                            //                                                           let user = Auth.auth().currentUser
                            //                                                                            print(user?.displayName)
                            //                                                                                LoginMark(user: user!)
                            
                            if(!self.FromDA){
                                
                                self.openMenu=true
                            }else{
                                self.FromDA = false
                                //                                                                                                      self.openDAP = true
                            }
                        }
                    } else {
                        // No user is signed in.
                        print("LISTNER ERGHSDF")
                        //                                                        self.ViewLoader = false
                        // ...
                    }
                }
                
                
                
            }
            .frame(width:geometry.size.width,height: geometry.size.height)
            .background(Color.white)
            //            .navigationBarTitle("Sign Up")
            
            //                    .navigationBarHidden(true)
            
            //                .navigationBarHidden(true)
            //                    .navigationBarBackButtonHidden(true)
            
        }
        //            .statusBar(hidden: true)
        //                .navigationViewStyle(StackNavigationViewStyle())
        
        //        }
        .navigationBarTitle("Login")
        //                .navigationBarItems(trailing:Button(self.GuestButtonKey){self.GuestButtonKey = "";self.FromDA = false }
        //                   )
        .edgesIgnoringSafeArea(.all)
        //        .navigationBarBackButtonHidden(true)
        //
        //
        
    }
    
    
    struct SignUpSelection_Previews: PreviewProvider {
        static var previews: some View {
            SignUpSelection(FromDA: .constant(false))
        }
    }
    
    struct google : UIViewRepresentable {
        
        
        func makeUIView(context: UIViewRepresentableContext<google>) -> GIDSignInButton {
            
            let button = GIDSignInButton()
            button.colorScheme = .light
            button.style = .wide
            
            GIDSignIn.sharedInstance()?.presentingViewController = UIApplication.shared.windows.last?.rootViewController
            return button
        }
        func updateUIView(_ uiView: GIDSignInButton, context: UIViewRepresentableContext<google>) {
            
        }
    }
    
    
    
    
    
    struct FaceBookLoginView: UIViewRepresentable {
        
        func makeCoordinator() -> FaceBookLoginView.Coordinator {
            return FaceBookLoginView.Coordinator()
        }
        
        class Coordinator: NSObject, LoginButtonDelegate {
            func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                
                let credential = FacebookAuthProvider.credential(withAccessToken: AccessToken.current!.tokenString)
                Auth.auth().signIn(with: credential) { (authResult, error) in
                    if let error = error {
                        print(error.localizedDescription)
                        return
                    }
                    //                print("Facebook Sign In")
                }
            }
            
            func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
                try! Auth.auth().signOut()
            }
        }
        
        func makeUIView(context: UIViewRepresentableContext<FaceBookLoginView>) -> FBLoginButton {
            let view = FBLoginButton()
            view.permissions = ["email"]
            view.delegate = context.coordinator
            return view
        }
        
        func updateUIView(_ uiView: FBLoginButton, context: UIViewRepresentableContext<FaceBookLoginView>) { }
    }
    
    struct Social:UIViewControllerRepresentable {
        func makeUIViewController(context: UIViewControllerRepresentableContext<Social>) ->  UIViewController {
            return UIViewController()
        }
        func updateUIViewController(_ uiViewController:  UIViewController, context: UIViewControllerRepresentableContext<Social> ) {
            
        }
        
        func attemptLoginGoogle() {
            //        if(GIDSignIn.sharedInstance()?.hasPreviousSignIn()){
            GIDSignIn.sharedInstance()?.disconnect()
            //        GIDSignIn.sharedInstance()?.currentUser
            
            GIDSignIn.sharedInstance()?.presentingViewController = UIApplication.shared.windows.last?.rootViewController
            
            
            GIDSignIn.sharedInstance()?.signIn()
            
            print("QQQQQQQQQQQQQ","2")
            //        print("CCCCCCCCC",Auth.auth().currentUser?.email)
            
            
        }
    }
    
    struct SocialLogin: UIViewRepresentable {
        func makeCoordinator() -> Coordinator {
            return Coordinator(self)
        }
        
        @EnvironmentObject var settings : UserSettings
        func makeUIView(context: UIViewRepresentableContext<SocialLogin>) -> UIView {
            return UIView()
        }
        
        func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<SocialLogin>) {
        }
        
        func attemptLoginGoogle() {
            //        if(GIDSignIn.sharedInstance()?.hasPreviousSignIn()){
            GIDSignIn.sharedInstance()?.disconnect()
            //        GIDSignIn.sharedInstance()?.currentUser
            
            GIDSignIn.sharedInstance()?.presentingViewController = UIApplication.shared.windows.last?.rootViewController
            
            
            GIDSignIn.sharedInstance()?.signIn()
            
            print("QQQQQQQQQQQQQ","2")
            //        print("CCCCCCCCC",Auth.auth().currentUser?.email)
            
            
        }
        
        func attemptLoginFb(completion: @escaping (_ result: LoginManagerLoginResult?, _ error: Error?) -> Void) {
            let fbLoginManager: LoginManager = LoginManager()
            fbLoginManager.logOut()
            fbLoginManager.logIn(permissions: ["email"], from: UIApplication.shared.windows.last?.rootViewController) { (result, error) -> Void in
                completion(result, error)
                
                
                let credential = FacebookAuthProvider.credential(withAccessToken: result?.token?.tokenString ?? "")
                Auth.auth().signIn(with: credential) { (authResult, error) in
                    
                    print("CCCCCCC",authResult?.user.email)
                    
                    if let error = error {
                        print(error.localizedDescription)
                        return
                    }
                    UserDefaults.standard.set(true,forKey:"SOCIAL")
                    if(authResult?.user.email == nil){
                        
                        authResult?.user.updateEmail(to: (authResult?.user.providerData[0].email)!, completion: { (error) in
                            LoginMark(user: authResult!.user)
                        })
                        
                    }else{
                        LoginMark(user: authResult!.user )
                    }
                    
                    
                }
            }
        }
        
        class Coordinator: NSObject, ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
            var parent: SocialLogin?
            fileprivate var currentNonce: String?
            init(_ parent: SocialLogin) {
                self.parent = parent
                super.init()
                
            }
            
            @objc func didTapButton() {
                print("Tapped")
                //            let appleIDProvider = ASAuthorizationAppleIDProvider()
                //            let request = appleIDProvider.createRequest()
                //            request.requestedScopes = [.fullName, .email]
                //
                //            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
                //            authorizationController.presentationContextProvider = self
                //            authorizationController.delegate = self
                //            authorizationController.performRequests()
                
                let nonce = randomNonceString()
                currentNonce = nonce
                let appleIDProvider = ASAuthorizationAppleIDProvider()
                let request = appleIDProvider.createRequest()
                request.requestedScopes = [.fullName, .email]
                request.nonce = sha256(nonce)
                
                let authorizationController = ASAuthorizationController(authorizationRequests: [request])
                authorizationController.delegate = self
                authorizationController.presentationContextProvider = self
                authorizationController.performRequests()
            }
            
            @available(iOS 13, *)
            private func sha256(_ input: String) -> String {
                let inputData = Data(input.utf8)
                let hashedData = SHA256.hash(data: inputData)
                let hashString = hashedData.compactMap {
                    return String(format: "%02x", $0)
                }.joined()
                
                return hashString
            }
            
            private func randomNonceString(length: Int = 32) -> String {
                precondition(length > 0)
                let charset: Array<Character> =
                    Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
                var result = ""
                var remainingLength = length
                
                while remainingLength > 0 {
                    let randoms: [UInt8] = (0 ..< 16).map { _ in
                        var random: UInt8 = 0
                        let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
                        if errorCode != errSecSuccess {
                            fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
                        }
                        return random
                    }
                    
                    randoms.forEach { random in
                        if remainingLength == 0 {
                            return
                        }
                        
                        if random < charset.count {
                            result.append(charset[Int(random)])
                            remainingLength -= 1
                        }
                    }
                }
                
                return result
            }
            
            func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
                let vc = UIApplication.shared.windows.last?.rootViewController
                return (vc?.view.window!)!
            }
            
            func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
                
                print("OOOOOOOOOOOOOOOOOO","1")
                if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
                    guard let nonce = currentNonce else {
                        fatalError("Invalid state: A login callback was received, but no login request was sent.")
                    }
                    guard let appleIDToken = appleIDCredential.identityToken else {
                        print("Unable to fetch identity token")
                        return
                    }
                    guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                        print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                        return
                    }
                    
                    print("OOOOOOOOOOOOOOOOOO","1")
                    // Initialize a Firebase credential.
                    let credential = OAuthProvider.credential(withProviderID: "apple.com",
                                                              idToken: idTokenString,
                                                              rawNonce: nonce)
                    // Sign in with Firebase.
                    Auth.auth().signIn(with: credential) { (authResult, error) in
                        if (error != nil) {
                            // Error. If error.code == .MissingOrInvalidNonce, make sure
                            // you're sending the SHA256-hashed nonce as a hex string with
                            // your request to Apple.
                            print(error?.localizedDescription)
                            UserDefaults.standard.set(true,forKey:"SOCIAL")
                            if(authResult?.user.email == nil){
                                
                                let userSettings = UserSettings()
                                authResult?.user.updateEmail(to: (authResult?.user.providerData[0].email)!, completion: { (error) in
                                    LoginMark(user: authResult!.user)
                                })
                                
                            }else{
                                
                                LoginMark(user: authResult!.user)
                            }
                            
                            return
                        }
                        // User is signed in to Firebase with Apple.
                        // ...
                    }
                }
            }
            
            func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
                // Handle error.
                print("Sign in with Apple errored: \(error)")
            }
            
            
            //        func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
            //            guard let credentials = authorization.credential as? ASAuthorizationAppleIDCredential else {
            //                print("credentials not found....")
            //                return
            //            }
            //
            //            let defaults = UserDefaults.standard
            //            defaults.set(credentials.user, forKey: "userId")
            //            parent?.name = "\(credentials.fullName?.givenName ?? "")"
            //        }
            //
            //        func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
            //        }
        }
        
    }
    //    private func showAppleLogin() {
    //      // 1
    //      let request = ASAuthorizationAppleIDProvider().createRequest()
    //
    //      // 2
    //      request.requestedScopes = [.fullName, .email]
    //
    //      // 3
    //      let controller = ASAuthorizationController(authorizationRequests: [request])
    //    }
    
    //func logginFb() {
    //       SocialLogin().attemptLoginFb(completion: { result, error in
    //
    //       })
    //   }
}
func LoginMark(user:User) {
    let ref = Database.database().reference()
    
    var EMAIL:String = ""
    if(user.email != nil && user.email != ""){
        
        print("OOOOOOOOOOOOOO","1")
        EMAIL = user.email!
        let c=EMAIL as! String
        let A = c.replacingOccurrences(of: "@", with: "_")
        let B=A.replacingOccurrences(of: ".", with: "_")
        UserDefaults.standard.set(B, forKey: "Aid") ;
        
    }else if (user.providerData.count > 0 && (user.providerData[0].email) != nil){
        
        print("OOOOOOOOOOOOOO","2")
        
        EMAIL = user.providerData[0].email as! String
        let c=EMAIL as! String
        let A = c.replacingOccurrences(of: "@", with: "_")
        let B=A.replacingOccurrences(of: ".", with: "_")
        UserDefaults.standard.set(B, forKey: "Aid") ;
        
    }else{
        
        UserDefaults.standard.set(user.uid, forKey: "Aid") ;
        
        
    }
    
    
    
    
    //       self.settings.Name = user?.displayName as! String
    
    //
    //                                                                       let c=EMAIL as! String
    //                                                                       let A = c.replacingOccurrences(of: "@", with: "_")
    //
    //                                                                       let B=A.replacingOccurrences(of: ".", with: "_")
    //
    //                                                                       //                                                                                                                                     UserDefaults.standard.string(forKey: "Aid") ?? "0" = B
    //                                                                       //                                                            UserDefaults.standard.set(B, forKey: "Aid")
    //
    //                                                                       UserDefaults.standard.set(B, forKey: "Aid") ;
    
    print("WERT CALLED")
    
    
    
    
    
    let email = EMAIL
    let photoURL = user.photoURL
    var multiFactorString = "MultiFactor: "
    for info in user.multiFactor.enrolledFactors {
        multiFactorString += info.displayName ?? "[DispayName]"
        multiFactorString += " "
        
        
        
    }
    var FirstName :String = ""
    
    var SecondName : String = ""
    if(user.displayName != nil){
        let items = user.displayName!.components(separatedBy: " ")
        
        FirstName = items[0]
        
        
        var i:Int = 1
        while i<items.count {
            
            SecondName += items[i]
            i+=1
        }
    }
    // ...
    ref.child("Users").child( UserDefaults.standard.string( forKey: "Aid") ?? user.uid).observeSingleEvent(of: DataEventType.value, with: {
        (datansnapshot) in
        
        if(datansnapshot.exists()){
            
            let P : [String : Any]=[
                
                "email" : EMAIL,
                "lastName": SecondName,
                "name" : FirstName ,
                "phoneNumber":"",
                "dp":String(describing :(user.photoURL!)),
                "RT" : [".sv": "timestamp"]
            ]
            
            ref.child("Users").child(UserDefaults.standard.string( forKey: "Aid") ?? user.uid).child("personal_details").updateChildValues(P)
        }
        else{
            
            let P : [String : Any]=[
                
                "email" : EMAIL,
                "lastName": SecondName,
                "name" : FirstName,
                
                "dp":String(describing :(user.photoURL!)),
                
            ]
            ref.child("Users").child(UserDefaults.standard.string( forKey: "Aid") ?? user.uid).child("personal_details").updateChildValues(P)
            
            
        }
        
        
    })
    
    
}


class googlesign : NSObject{
    func signIn ()
    {
        GIDSignIn.sharedInstance()?.presentingViewController = UIApplication.shared.windows.last?.rootViewController
        
        
        GIDSignIn.sharedInstance()?.signIn()
    }
}
