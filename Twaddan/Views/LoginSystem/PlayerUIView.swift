//
//  PlayerUIView.swift
//  Twaddan
//
//  Created by Spine on 30/04/20.
//  Copyright © 2020 spine. All rights reserved.
//

import Foundation
import UIKit
import AVKit

class PlayerUIView: UIView {
    
    
    private let playerLayer = AVPlayerLayer()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        guard let path=Bundle.main.path(forResource: "twaddan2", ofType: "mp4")else {return}
        let url = URL(fileURLWithPath: path)
        
        
        let player = AVPlayer(url: url)
        player.play()
        
        playerLayer.player = player
        layer.addSublayer(playerLayer)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        playerLayer.frame = bounds
    }
}
