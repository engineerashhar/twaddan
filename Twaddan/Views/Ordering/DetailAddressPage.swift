//
//  DetailAddressPage.swift
//  Twaddan
//
//  Created by Spine on 16/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI

import FirebaseDatabase
import FirebaseAuth
import Combine
import IQKeyboardManagerSwift

struct DetailAddressPage: View {
    
    
    
    //    @Binding var Total : Double
    //    @Binding var ServiceCharge : Double
    //    @Binding var Discount : Double
    //       @State var Total : Double
    @State var goToMenu : Bool = false
    
    @State var VNumber : [String: String] = [String: String] ()
    
    @State    var Proceed : Bool = true
    
    @State    var RedAlert : Bool = false
    @State var verificationID : String? = ""
    
    //    var PaymentType : Int
    
    var colors = ["House", "Apartment", "Work"]
    @State private var selectedType =  UserDefaults.standard.integer(forKey: "ATYPE") ?? 0
    //    @State var NAME : String = ""
    //    @State var LNAME : String = ""
    //    @State var MOBILE : String = ""
    @State var NOTE : String = ""
    @State var ADDRESS : String = ""
    
    @State var errorStatus : String  = ""
    
    @State var NAME : String = ""
    @State var LNAME : String = ""
    @State var MOBILE : String = ""
    
    @State var ANICK :String = ""
    @State var AHOME: String = ""
    @State var ASTREET : String = ""
    @State var AFLOOR : String = ""
    @State var AOFFICE : String = ""
    @State var ADNOTE : String = ""
    
    @State var ANOTE : String = ""
    @State var AMOBILE : String = ""
    
    //    @State var VCode : String = ""
    
    //    @State var AllVehicles : [Vehicle] = [Vehicle]()
    
    @State var ExpandAdress :Bool = false
    @State var Login :Bool = true
    
    @ObservedObject var vehicleListVM = VehicleListViewModel()
    //     @ObservedObject var keyboardResponder = KeyboardResponder()
    //  @ObservedObject var userDetailVM = UserDetailViewModel()
    
    //    @ObservedObject var cartFetch = CartFetch()
    
    @State var selectedServiceProvider : SelectedServiceProvider = SelectedServiceProvider()
    @State var carts : [CartStruct] = [CartStruct]()
    @State var VehicleList : [VehicleSets] = [VehicleSets]()
    
    @State var ALERT1:Bool = false
    @State var BLUR:Bool = false
    @State var ALERT2:Bool = false
    @State var ALERT3:Bool = false
    @State var alert:String = ""
    @State var ErrorComments:String = ""
    @State var ErrorStatus:Bool = false
    
    @State var showAlert:Bool = false
    
    
    @State var VNumbers : [Vehicle] = [Vehicle]()
    //@State var VNumbers : [String: String] = [String: String] ()
    var onCommit:([Vehicle]) -> (Void) = {_ in}
    
    
    let ref = Database.database().reference()
    @ObservedObject var backImagelinker = BackImageLinker()
    
    @State var profile : Profile = Profile(Name:"",Email:"",Password:"",Phone:"",LastName:"", RePassword: "")
    
    @State var Terms:Bool = true
    @State var showTerms : Bool = false
    
    @ObservedObject var signLink = SignLink()
    @EnvironmentObject var settings: UserSettings
    
    @State var Number : String = ""
    
    @State var OpenCheckOut : Bool = false
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    
    
    var body: some View {
        
        
        
        
        GeometryReader { geometry in
            
            
            ZStack{
                Color("h1").edgesIgnoringSafeArea(.all)
                
                if(Auth.auth().currentUser?.isAnonymous ?? true  && self.Login){
                    
                    
                    SignUpSelection(FromDA: self.$Login).frame(width:geometry.size.width,height:geometry.size.height)
                }else{
                    
                    ZStack{
                        VStack{
                            
                            
                            ScrollView{
                                
                                Spacer().frame(height:100)
                                
                                
                                VStack {
                                    
                                    
                                    VStack {
                                        
                                        HStack{
                                            
                                            
                                            //                                Button(action : {
                                            //
                                            //                                    self.presentationMode.wrappedValue.dismiss()
                                            //
                                            //
                                            //                                }) {
                                            //                                    Text("BACK")
                                            //                                        .foregroundColor(Color.white)
                                            //                                        .font(.custom("Regular_Font".localized(), size: 15))
                                            //
                                            //                                }
                                            
                                            
                                            Spacer()
                                            NavigationLink(destination:HomePage(),isActive: self.$goToMenu)
                                            {
                                                Text("").frame(height:0)
                                                
                                            }
                                            NavigationLink(destination:CheckOut(NAME: self.$NAME, MOBILE: self.$MOBILE, LNAME: self.$LNAME, ADDRESS: self.getFullAdderss()),isActive: self.$OpenCheckOut)
                                            {
                                                Text("").frame(height:0)
                                                
                                            }
                                            
                                            
                                            
                                            
                                            
                                        }
                                        
                                        
                                        
                                        
                                        //                            HStack{
                                        //                                Image("Twaddan Logo").resizable().frame(width : 160,height:30).padding()
                                        //                                Spacer()
                                        //                            }.onTapGesture{
                                        //
                                        ////                                print(self.vehicleListVM.vehicleCellViewModel)
                                        //                            }
                                        
                                        //                            .padding(.bottom, 30.0)
                                        
                                        
                                        VStack{
                                            //
                                            VStack(spacing:8){
                                                //   ScrollView{
                                                
                                                Group{
                                                    
                                                    //                                        Spacer().frame(height:50)
                                                    Group{
                                                        HStack{
                                                            //                                                                                       Text("Contact Details") .font(.custom("Regular_Font".localized(), size: 16)).padding(.vertical,10)
                                                            Spacer()
                                                        }
                                                        
                                                        
                                                        DetailsFilling(NAME: self.$NAME, LNAME: self.$LNAME, MOBILE: self.$MOBILE, RedAlert: self.$RedAlert).padding(.top,3)
                                                        
                                                    }
                                                    HStack{
                                                        TextField("Area", text: self.$ADDRESS)
                                                            
                                                            
                                                            .foregroundColor(Color("darkthemeletter"))
                                                            .font(.custom("Regular_Font".localized(), size: 14))
                                                            .fixedSize(horizontal: false, vertical: true)
                                                            .lineLimit(5)
                                                        
                                                        //                                            Image("up_and_down_2")
                                                        //                                                .resizable()
                                                        //                                                .frame(width: 13, height: 10)
                                                        //                                                .onTapGesture {
                                                        //
                                                        //                                                    withAnimation{
                                                        //                                                        self.ExpandAdress .toggle()
                                                        //
                                                        //                                                    }
                                                        //                                            }
                                                        
                                                        
                                                    }
                                                    Divider().frame(height: 1)
                                                    
                                                    
                                                    VStack{
                                                        
                                                        //                                            HStack{
                                                        //                                                Text("Address") .font(.custom("Regular_Font".localized(), size: 16)).padding(.top,20).padding(.bottom,10)
                                                        //                                                                                                                                  Spacer()
                                                        //                                                                                                                              }
                                                        Spacer().frame(height:10)
                                                        Picker(selection: self.$selectedType, label: Text("Please choose a Address Type")) {
                                                            ForEach(0 ..< self.colors.count) {
                                                                Text(self.colors[$0].localized())
                                                                
                                                            }
                                                        }.pickerStyle(SegmentedPickerStyle())
                                                        
                                                        Spacer().frame(height:20)
                                                        //                                        if (self.ExpandAdress){
                                                        
                                                        
                                                        //                                            ExpandedAddress(ADDRESS: self.$ADDRESS, ANICK: self.$ANICK, AHOME: self.$AHOME, ASTREET: self.$ASTREET, AFLOOR: self.$AFLOOR, ANOTE: self.$ANOTE, MOBILE: self.$MOBILE)
                                                        
                                                        if(self.selectedType == 0){
                                                            
                                                            ExpandHomeAddress(ANICK: self.$ANICK, AHOME: self.$AHOME, ASTREET: self.$ASTREET, AFLOOR: self.$ASTREET, ADNOTE: self.$ADNOTE, AOFFICE: self.$AOFFICE, RedAlert: self.$RedAlert).onAppear{
                                                                UserDefaults.standard.set(0, forKey: "ATYPE")
                                                            }
                                                        }else if(self.selectedType == 1){
                                                            ExpandApartmentAddress(ANICK: self.$ANICK, AHOME: self.$AHOME, ASTREET: self.$ASTREET, AFLOOR: self.$AFLOOR, ADNOTE: self.$ADNOTE, AOFFICE: self.$AOFFICE, RedAlert: self.$RedAlert).onAppear{
                                                                UserDefaults.standard.set(1, forKey: "ATYPE")
                                                            }
                                                        }else{
                                                            ExpandOfficeAddress(ANICK: self.$ANICK, AHOME: self.$AHOME, ASTREET: self.$ASTREET, AFLOOR: self.$AFLOOR, ADNOTE: self.$ADNOTE, AOFFICE: self.$AOFFICE, RedAlert: self.$RedAlert).onAppear{
                                                                UserDefaults.standard.set(2, forKey: "ATYPE")
                                                            }
                                                        }
                                                        
                                                        //                                        }
                                                    }
                                                    
                                                    //                                        ScrollView(.vertical){
                                                    
                                                    VStack(spacing:20){
                                                        
                                                        //                                                HStack{
                                                        //                                                    Text("Vehicle Numbers") .font(.custom("Regular_Font".localized(), size: 16)).padding(.top,20).padding(.bottom,10)
                                                        //                                                                                           Spacer()
                                                        //                                                                                       }
                                                        //                                                  List{
                                                        ForEach(self.vehicleListVM.vehicleCellViewModel){ vehicleCellVM in
                                                            
                                                            
                                                            
                                                            VehicleCell(vehicleCellVM:vehicleCellVM, RedAlert: self.$RedAlert)
                                                            
                                                            
                                                        }
                                                        //                                            }.onAppear{
                                                        //                                            print("WWWWWW",self.vehicleListVM.vehicleCellViewModel)
                                                        //                                            }
                                                        
                                                    }
                                                    //                                        }
                                                    
                                                    
                                                    TextField("Note (Optional)", text: self.$NOTE)
                                                        
                                                        .foregroundColor(Color(red: 0.38, green: 0.51, blue: 0.64, opacity: 1.0))
                                                        .font(.custom("Regular_Font".localized(), size: 14))
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    Spacer()
                                                    
                                                    HStack{
                                                        
                                                        
                                                        Image(systemName: self.Terms ? "checkmark.square.fill" : "square" ).resizable().frame(width:17,height: 17).onTapGesture {
                                                            self.Terms.toggle()
                                                        }
                                                        
                                                        
                                                        Text("I agree with terms and conditions")
                                                            .foregroundColor(Color(red: 0.15, green: 0.22, blue: 0.44, opacity: 1.0))
                                                            .font(.custom("Regular_Font".localized(), size: 14))
                                                            .onTapGesture {
                                                                self.showTerms = true
                                                            }  .sheet(isPresented: self.$showTerms) {
                                                                TermsAndConditions()
                                                            }
                                                        
                                                        
                                                        Spacer()
                                                    }
                                                    
                                                }
                                                
                                                
                                                
                                                
                                                
                                                
                                                //                                    NavigationLink(destination: SignUpSelection(FromDA: .constant(false)
                                                //                                        )
                                                //                                         ){
                                                //
                                                //
                                                //
                                                //
                                                //
                                                //
                                                //                                            HStack{
                                                //                                                Spacer()
                                                //                                                Text("Sign in to experiance more features")
                                                //                                                    .font(.custom("Regular_Font".localized(), size: 13))
                                                //                                                    .foregroundColor(Color(red: 0.49, green: 0.58, blue: 0.67, opacity: 1.0))
                                                //
                                                //                                            }
                                                //
                                                //                                    }
                                                
                                                
                                                //                                    if(Auth.auth().currentUser!.isAnonymous){
                                                //                                    Button(action:{
                                                //                                        self.Login = true
                                                //                                    }){
                                                //                                        HStack{
                                                //                                                                                        Spacer()
                                                //                                                                                        Text("Sign in to experiance more features")
                                                //                                                                                            .font(.custom("Regular_Font".localized(), size: 13))
                                                //                                                                                            .foregroundColor(Color(red: 0.49, green: 0.58, blue: 0.67, opacity: 1.0))
                                                //
                                                //                                                                                    }
                                                //
                                                //                                        }
                                                //
                                                //                                    }
                                                
                                                
                                                
                                                Spacer().frame(height:100)
                                                
                                                
                                                
                                            }
                                            
                                            .padding(.horizontal, 25.0)
                                            Spacer()
                                            
                                            
                                            
                                            
                                            
                                            
                                        }
                                        .padding(.top,30)
                                        //                            .frame(maxWidth:.infinity,maxHeight:geometry.size.height*0.8)
                                        //                            .background(RoundedCorners(color: .white, tl: 30, tr: 30, bl: 0, br: 0))
                                        
                                        
                                        
                                    }
                                    
                                    
                                }
                                
                                .blur(radius: self.BLUR ? 10 : 0)
                                .alert(isPresented: self.$showAlert) {
                                    Alert(title: Text(self.errorStatus))
                                    
                                    
                                }
                                
                                
                                //Alert 1
                            }
                            
                        }
                        
                        
                        
                        
                        VStack{
                            Spacer()
                            
                            if(self.ErrorStatus && self.ErrorComments != ""){
                                HStack{
                                    Text(self.ErrorComments.localized())
                                        .foregroundColor(.red) .font(.custom("Regular_Font".localized(), size: 17))
                                }.padding().background(Color("h1"))
                                
                            }
                            
                            
                            Button(action: {
                                
                                self.MOBILE = UnLocNumbers(key:self.MOBILE )
                                //                                                            print("wwwwwwwwww",UnLocNumbers(key:self.MOBILE ))
                                
                                self.ErrorStatus=true;
                                
                                self.Proceed  = true
                                
                                for vehiclelistVM in self.vehicleListVM.vehicleCellViewModel {
                                    
                                    if(vehiclelistVM.vehicle.VehicleNumber == ""){
                                        print("QQQQQQQQ",self.Proceed)
                                        self.Proceed = false
                                    }
                                    
                                }
                                
                                self.RedAlert = true
                                
                                if(self.NAME == "" ){
                                    self.ErrorComments="Please Enter A Valid Name"
                                    self.ErrorStatus=true;
                                    
                                    
                                }else if(self.MOBILE.count < 9 ){
                                    self.ErrorComments="Please Enter A Valid Mobile Number"
                                    self.ErrorStatus=true;
                                    
                                    
                                }else if(self.ADDRESS == "" ){
                                    self.ErrorComments="Please Enter A Valid Address"
                                    self.ErrorStatus=true;
                                    
                                    
                                }
                                else if(!self.Terms){
                                    self.ErrorComments="You must agree terms and conditions"
                                    self.ErrorStatus=true;
                                    
                                    
                                }
                                //                                                            else if(self.ANICK == ""){
                                //                                                                    self.ErrorComments="Please Enter A Nick Name"
                                //                                                                    self.ErrorStatus=true;
                                //
                                //
                                //                                                                }
                                
                                else if(self.AOFFICE == "" && self.selectedType == 2){
                                    self.ErrorComments="Please Enter A Office Name"
                                    self.ErrorStatus=true;
                                    
                                    
                                }
                                else if(self.AFLOOR == "" && self.selectedType == 2){
                                    self.ErrorComments="Please Enter A Floor Name"
                                    self.ErrorStatus=true;
                                    
                                    
                                }
                                else if(self.AHOME == ""){
                                    self.ErrorComments="Please Enter A Valid Home , Apartment or Building Name"
                                    self.ErrorStatus=true;
                                    
                                    
                                }
                                
                                
                                else if(self.ASTREET == ""){
                                    self.ErrorComments="Please Enter Street Name"
                                    self.ErrorStatus=true;
                                    
                                    
                                }
                                //
                                else if(!self.Proceed){
                                    
                                    self.ErrorComments="Please Enter All Vehicle Number"
                                    self.ErrorStatus=true;
                                    
                                }
                                
                                else {
                                    
                                    self.RedAlert = false
                                    print("QQQQQQQQ",self.Proceed)
                                    
                                    if(self.ANICK == ""){
                                        self.ANICK = String(self.ADDRESS.prefix(5))
                                        
                                        
                                    }
                                    
                                    //                                                                if(self.MOBILE.count == 10 && self.MOBILE.prefix(1) == "0"){
                                    self.MOBILE = String(self.MOBILE.suffix(9))
                                    
                                    //                                                                }
                                    self.OpenCheckOut = true
                                    
                                    
                                }
                                
                                
                            }
                            
                            
                            
                            
                            ) {
                                
                                
                                RoundedPanel()
                                    
                                    .overlay(Text("CONTINUE")
                                                .font(.custom("ExtraBold_Font".localized(), size: 16))
                                                
                                                .foregroundColor(Color.white))
                                
                                
                                
                                
                                
                            }
                        }
                        
                    }
                    //                        .modifier(KeyboardAdaptive())
                    
                    .onAppear{
                        
                        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").observeSingleEvent(of: DataEventType.value) { (data) in
                            if(!data.exists()){
                                let uuid = UIDevice.current.identifierForVendor?.uuidString
                                
                                self.ref.child("Cart").child(uuid ?? "uuid").observeSingleEvent(of: DataEventType.value) { (uuidSnap) in
                                    if(uuidSnap.exists()){
                                        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").setValue(uuidSnap.value)
                                        self.ref.child("Cart").child(uuid ?? "uuid").removeValue()
                                    }
                                }
                                
                            }
                        }
                        
                        self.AHOME = UserDefaults.standard.string(forKey: "AHOME") ?? ""
                        self.ASTREET = UserDefaults.standard.string(forKey: "ASTREET") ?? ""
                        self.AFLOOR = UserDefaults.standard.string(forKey: "AFLOOR") ?? ""
                        self.ADNOTE = UserDefaults.standard.string(forKey: "ADNOTE") ?? ""
                        self.AOFFICE = UserDefaults.standard.string(forKey: "AOFFICE") ?? ""
                        self.ANICK = UserDefaults.standard.string(forKey: "ANICK") ?? ""
                        
                        self.ADDRESS =
                            //                    self.AHOME + self.AFLOOR + self.ASTREET +
                            UserDefaults.standard.string(forKey: "PLACE") ?? ""
                        
                        //                if(self.ADDRESS == ""){
                        //                     self.ADDRESS =
                        //                }
                        
                        //                self.readVehicles(settings: self.settings)
                        
                        self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").observeSingleEvent(of: DataEventType.value) { (datasnapshot) in
                            
                            
                            if(datasnapshot.exists()){
                                
                                if(self.NAME == "" && datasnapshot.childSnapshot(forPath: "personal_details/name").exists()){
                                    self.NAME = String(describing:(datasnapshot.childSnapshot(forPath: "personal_details/name").value)!)
                                    
                                    
                                    
                                }
                                
                                if(self.LNAME == "" && datasnapshot.childSnapshot(forPath: "personal_details/lastName").exists()){
                                    self.LNAME = String(describing:(datasnapshot.childSnapshot(forPath: "personal_details/lastName").value)!)
                                    
                                }
                                
                                print("//////",datasnapshot.childSnapshot(forPath: "personal_details/phoneNumber").exists(),self.MOBILE)
                                if(self.MOBILE == "" && ( datasnapshot.childSnapshot(forPath: "personal_details/phoneNumber").exists())){
                                    
                                    print("//////","Enter")
                                    self.MOBILE = String(String(describing:datasnapshot.childSnapshot(forPath: "personal_details/phoneNumber").value!).suffix(9))
                                    print("//////",self.MOBILE,String(describing:datasnapshot.childSnapshot(forPath: "personal_details/phoneNumber").value!).suffix(9),datasnapshot.childSnapshot(forPath: "personal_details").value)
                                    
                                    if(String(describing:(datasnapshot.childSnapshot(forPath: "personal_details/phoneNumber").value)!) == ""){
                                        print("//////","Enter2")
                                        self.MOBILE = String((UserDefaults.standard.string(forKey: "LAST_USED_PHONE") ?? "").suffix(9)) ;
                                    }
                                    //   NOTE : String = ""
                                    // ADDRESS : String = ""
                                }else if(self.MOBILE == "" && !datasnapshot.childSnapshot(forPath: "personal_details/phoneNumber").exists()){
                                    self.MOBILE = String((UserDefaults.standard.string(forKey: "LAST_USED_PHONE") ?? "").suffix(9))
                                    
                                }
                                
                            }
                            
                            
                        }
                        
                        
                        
                        
                    }
                }
                
            }
            .modifier(KeyboardAdaptive())
            .onAppear{
                
                
                
                
                
                
                //                    IQKeyboardManager.shared.enableAutoToolbar = false
                //                              IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
                //                              IQKeyboardManager.shared.shouldResignOnTouchOutside = true
                //                              IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
                print("i was called")
                //self.cartFetch.readCart(settings: self.settings)
                
                //            print("Started 2 \(UserDefaults.standard.string(forKey: "Aid") ?? "0")")
                
                
                self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").observe(DataEventType.value, with: {
                    
                    (Msnapshot) in
                    if(Msnapshot.exists()){
                        
                        self.ref.child("service_providers").child(String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider_id").value)!)).observeSingleEvent(of: DataEventType.value) { (snap) in
                            
                            
                            
                            
                            self.carts.removeAll()
                            
                            self.selectedServiceProvider = SelectedServiceProvider(ServiceProvider: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider").value)!), ServiceProvider_id: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider_id").value)!), ServiceProvider_image: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider_image").value)!), DriverID: String(describing :  (Msnapshot.childSnapshot(forPath:"DriverID").value)!), TimeToArrive: Msnapshot.childSnapshot(forPath:"ETA").value as? Double ?? 0.0 )
                            
                            //                                    SelectedServiceProvider(
                            //
                            //                                                                           ServiceProvider: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider").value)!),
                            //                                                                                                           ServiceProvider_id: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider_id").value)!),
                            //                                                                                                           ServiceProvider_image: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider_image").value)!),DriverID: String(describing :  (Msnapshot.childSnapshot(forPath:"DriverID").value)!),ETA :Msnapshot.childSnapshot(forPath:"ETA").value as? Double ?? 0.0 ,SPSnap: snap)
                            
                            
                            self.VehicleList.removeAll()
                            
                            for ID in Msnapshot.childSnapshot(forPath:"Vehicles").children {
                                let id = ID as! DataSnapshot
                                
                                
                                var addonslist = [Addons]()
                                var servicelist = [Services]()
                                
                                let vehiclesets:VehicleSets =  VehicleSets (VehicleType: String(describing :  (id.childSnapshot(forPath:"Vehicle").value)!), VehicleNumber: String(describing :  (id.childSnapshot(forPath:"Vehicle Number").value)!), AddonSnaps: id.childSnapshot(forPath:"addons"), ServiceSnap: id.childSnapshot(forPath:"services"))
                                
                                self.VehicleList.append(vehiclesets)
                                
                                for SERVICE in id.childSnapshot(forPath:"services").children {
                                    let serv_snap = SERVICE as! DataSnapshot
                                    
                                    let service : Services = Services(id: serv_snap.key,
                                                                      serviceName: String(describing :  (serv_snap.childSnapshot(forPath:"name".localized()).value)!),serviceNameO:String(describing :  (serv_snap.childSnapshot(forPath:"name_ar".localized()).value)!),
                                                                      serviceDesc: String(describing :  (serv_snap.childSnapshot(forPath:"description".localized()).value)!),serviceDescO: String(describing :  (serv_snap.childSnapshot(forPath:"description_ar".localized()).value)!),
                                                                      ServiceImage: String(describing :  (serv_snap.childSnapshot(forPath:"image_url").value)!),
                                                                      ServicePrice: String(describing :  (serv_snap.childSnapshot(forPath:"price").value)!),ServiceTime: String(describing :  (serv_snap.childSnapshot(forPath:"time_required").value)!))
                                    servicelist.append(service)
                                }
                                
                                for ADDONS in id.childSnapshot(forPath:"addons").children{
                                    
                                    
                                    let add_snap = ADDONS as! DataSnapshot
                                    
                                    
                                    let addon : Addons = Addons(id: add_snap.key,
                                                                addonName: String(describing :  (add_snap.childSnapshot(forPath:"name".localized()).value)!),addonNameO: String(describing :  (add_snap.childSnapshot(forPath:"name_ar".localized()).value)!), addonDesc:  String(describing :  (add_snap.childSnapshot(forPath:"description".localized()).value ?? "")),addonDescO:  String(describing :  (add_snap.childSnapshot(forPath:"description_ar".localized()).value ?? "")),
                                                                addonImage: String(describing :  (add_snap.childSnapshot(forPath:"image_url").value)!),
                                                                addonPrice: String(describing :  (add_snap.childSnapshot(forPath:"price").value)!))
                                    addonslist.append(addon)
                                    
                                }
                                
                                let cart : CartStruct = CartStruct(id:id.key , addons:addonslist , services: servicelist , vehicle: String(describing :  (id.childSnapshot(forPath:"Vehicle").value)!), vehicleName: String(describing :  (id.childSnapshot(forPath:"VehicleName").value)!)
                                                                   //                            ,ServiceProvider: String(describing :  (id.childSnapshot(forPath:"ServiceProvider").value)!),ServiceProvider_id: String(describing :  (id.childSnapshot(forPath:"ServiceProvider_id").value)!),ServiceProvider_image: String(describing :  (id.childSnapshot(forPath:"ServiceProvider_image").value)!)
                                                                   
                                )
                                
                                //                         print("Started 3 \(self.carts.count)")
                                self.carts.append(cart)
                                
                            }
                            
                        }
                    }
                })
                
                
                
            }
            
            
            
            
            
        }
        .background(Color("h1"))
        .navigationBarTitle("Fill Address Details")
        //            .navigationBarTitle("Address Page",displayMode: .large)
        //        .navigationBarHidden(true)
        //        .navigationBarTitle("Login").navigationBarItems(trailing:Button(""){}
        //        )
        .edgesIgnoringSafeArea(.top)
        
        
        
    }
    
    struct DetailAddressPage_Previews: PreviewProvider {
        // @State var  PaymentType : String = "Cash On Delivery"
        
        static var previews: some View {
            DetailAddressPage().environmentObject(UserSettings())
        }
    }
    
    func getFullAdderss() -> String {
        
        
        var A : String = ""
        if(self.selectedType<2){
            
            A = "\(colors[self.selectedType]) (\(self.ANICK))\n\(self.AHOME),\(self.ASTREET)\n(\(self.ADNOTE))\nAddress from Location:\(self.ADDRESS)"
            
        }else{
            
            A = "\(colors[self.selectedType]) (\(self.ANICK))\n\(self.AHOME),\(self.AFLOOR),\(self.AOFFICE),\(self.ASTREET)\n(\(self.ADNOTE))\nAddress from Location:\(self.ADDRESS)"
            
        }
        
        return A
    }
    
    struct VehicleSets {
        
        var VehicleType : String
        var VehicleNumber : String
        var AddonSnaps: DataSnapshot
        var ServiceSnap: DataSnapshot
    }
    
    
    //    func readVehicles(settings : UserSettings) {
    //
    //
    //        print(UserDefaults.standard.string(forKey: "Aid") ?? "0")
    //
    //
    //                self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").observeSingleEvent(of: DataEventType.value) { (datasnapshot) in
    //
    //
    ////                    self.AllVehicles.removeAll()
    //    //
    //                    for ID in datasnapshot.children {
    //                                     let id = ID as! DataSnapshot
    //    //
    //                        var Services : String = ""
    //
    //                        for SERVICE in id.childSnapshot(forPath:"services").children {
    //                                           let serv_snap = SERVICE as! DataSnapshot
    //                            Services.append(String(describing :  (serv_snap.childSnapshot(forPath:Localize(key:"name")).value)!)
    //                            )
    //                            Services.append(" ")
    //                        }
    //
    //                        let name : String = Services +
    //                        " for " + String(describing :  (id.childSnapshot(forPath:"Vehicle").value)!) + " with  " + String(describing :  (datasnapshot.childSnapshot(forPath:"ServiceProvider").value)!)
    //
    //                        var Number : String = ""
    //                        if(self.VNumber[id.key] != nil){
    //                            Number = self.VNumber[id.key]!
    //                        }
    //
    //                        let vehicle : Vehicle = Vehicle(id: id.key, name: name, VehicleNumber:Number )
    //
    ////                        self.AllVehicles.append(vehicle)
    //
    //                    }
    //
    //
    //                }
    //
    //
    //
    //
    //            }
    
    struct DetailsFilling: View {
        
        @Binding var NAME :String
        @Binding var LNAME : String
        @Binding var MOBILE : String
        @Binding var RedAlert : Bool
        
        var body: some View {
            VStack(spacing:20){
                
                //                HStack{
                
                //                    VStack{
                VStack{
                    TextField("First Name".localized() + "*", text: $NAME)
                        
                        .foregroundColor(Color("darkthemeletter"))
                        .font(.custom("Regular_Font".localized(), size: 14))
                    Divider().frame(height: 1).background(self.NAME == "" && self.RedAlert ? Color.red : Color(#colorLiteral(red: 0.9263015985, green: 0.9348484874, blue: 0.9562863708, alpha: 1)))
                }
                //                    }
                
                //                    VStack{
                VStack{
                    TextField("Last Name".localized() + "*", text: $LNAME)
                        
                        .foregroundColor(Color("darkthemeletter"))
                        .font(.custom("Regular_Font".localized(), size: 14))
                    Divider().frame(height: 1).background(self.LNAME == "" && self.RedAlert ? Color.red : Color(#colorLiteral(red: 0.9263015985, green: 0.9348484874, blue: 0.9562863708, alpha: 1)))
                    //                    }
                    //                }
                }
                
                VStack{
                    
                    HStack{
                        Text("+971".localized())
                            
                            .foregroundColor(Color("darkthemeletter"))
                            .font(.custom("Regular_Font".localized(), size: 14))
                            .keyboardType(.numberPad).padding(.top,-1)
                        TextField("Mobile Number".localized() + "*", text: $MOBILE)
                            
                            .foregroundColor(Color("darkthemeletter"))
                            
                            .font(.custom("Regular_Font".localized(), size: 14))
                            .keyboardType(.numberPad)
                            .environment(\.locale,.init(identifier:"en"))
                        
                    }
                    Divider().frame(height: 1).background(self.MOBILE == "" && self.RedAlert ? Color.red : Color(#colorLiteral(red: 0.9263015985, green: 0.9348484874, blue: 0.9562863708, alpha: 1)))
                    
                    
                }
                
                
                
                
                
            }
        }
    }
    
    struct ExpandHomeAddress: View {
        @Binding var ANICK :String
        @Binding var AHOME :String
        @Binding var ASTREET:String
        @Binding var AFLOOR:String
        @Binding var ADNOTE:String
        
        @Binding var AOFFICE:String
        @Binding var RedAlert:Bool
        
        var body: some View {
            VStack(spacing:20){
                
                //                               TextField("Office", text: $AOFFICE)
                //
                //
                //                                   .foregroundColor(Color("darkthemeletter"))
                //                                   .font(.custom("Regular_Font".localized(), size: 12))
                //
                //                               Divider().frame(height: 1)
                //
                //                               TextField("Floor", text: $AFLOOR)
                //
                //                                   .foregroundColor(Color("dartkthemeletter"))
                //                                   .font(.custom("Regular_Font".localized(), size: 12))
                //
                //                               Divider().frame(height: 1)
                
                VStack{
                    TextField("Nick Name".localized() ,text: $ANICK,onEditingChanged:  {_ in
                        
                        UserDefaults.standard.set(self.ANICK,forKey: "ANICK")
                        
                        
                        
                        
                    })
                    
                    .foregroundColor(Color.black)                    .font(.custom("Regular_Font".localized(), size: 14))
                    Divider()
                    //                                                .background(self.ANICK == "" && self.RedAlert ? Color.red : Color(#colorLiteral(red: 0.9263015985, green: 0.9348484874, blue: 0.9562863708, alpha: 1)))
                    //                                              Divider().frame(height: 1).background(self.ANICK == "" && self.RedAlert ? Color.red : Color(#colorLiteral(red: 0.9080269933, green: 0.9226157069, blue: 0.9321033359, alpha: 1)))
                    
                    
                }
                VStack{
                    TextField("House No".localized() + "*" ,text: $AHOME,onEditingChanged:  {_ in
                        
                        UserDefaults.standard.set(self.AHOME,forKey: "AHOME")
                        
                        
                        
                        
                    })
                    
                    .foregroundColor(Color.black)                    .font(.custom("Regular_Font".localized(), size: 14))
                    
                    Divider().frame(height: 1).background(self.AHOME == "" && self.RedAlert ? Color.red : Color(#colorLiteral(red: 0.9263015985, green: 0.9348484874, blue: 0.9562863708, alpha: 1)))
                    
                    
                }
                VStack{
                    
                    
                    TextField("Street".localized() + "*" ,text: $ASTREET,onEditingChanged:  { v in
                        print("Data Enter \(v)")
                        
                        UserDefaults.standard.set(self.ASTREET,forKey: "ASTREET")
                        
                        
                        
                    })
                    
                    .foregroundColor(Color.black)                    .font(.custom("Regular_Font".localized(), size: 14))
                    
                    Divider().frame(height: 1).background(self.ASTREET == "" && self.RedAlert ? Color.red : Color(#colorLiteral(red: 0.9263015985, green: 0.9348484874, blue: 0.9562863708, alpha: 1)))
                    
                    
                }
                VStack{
                    
                    TextField("Additional Directions (Optional)", text: $ADNOTE,onEditingChanged:  {_ in
                        
                        
                        
                        UserDefaults.standard.set(self.ADNOTE,forKey: "ADNOTE")
                        
                    })
                    
                    .foregroundColor(Color.black)                  .font(.custom("Regular_Font".localized(), size: 14))
                    
                    Divider().frame(height: 1)
                }
                //                   TextField("Mobile Number", text: $MOBILE)
                //
                //      .foregroundColor(Color("dartkthemeletter"))                    .font(.custom("Regular_Font".localized(), size: 12))
                //
                //                   Divider().frame(height: 1)
                
                
            }
        }
    }
    
    
    
    struct ExpandApartmentAddress: View {
        @Binding var ANICK :String
        @Binding var AHOME :String
        @Binding var ASTREET:String
        @Binding var AFLOOR:String
        @Binding var ADNOTE:String
        
        @Binding var AOFFICE:String
        @Binding var RedAlert:Bool
        
        var body: some View {
            VStack(spacing:20){
                
                //                               TextField("Office", text: $AOFFICE)
                //
                //
                //                                   .foregroundColor(Color("darkthemeletter"))
                //                                   .font(.custom("Regular_Font".localized(), size: 12))
                //
                //                               Divider().frame(height: 1)
                //
                //                               TextField("Floor", text: $AFLOOR)
                //
                //                                   .foregroundColor(Color("dartkthemeletter"))
                //                                   .font(.custom("Regular_Font".localized(), size: 12))
                //
                //                               Divider().frame(height: 1)
                
                VStack{
                    TextField("Nick Name".localized(),text: $ANICK,onEditingChanged:  {_ in
                        
                        UserDefaults.standard.set(self.ANICK,forKey: "ANICK")
                        
                        
                        
                        
                    })
                    
                    .foregroundColor(Color.black)                    .font(.custom("Regular_Font".localized(), size: 14))
                    
                    Divider()
                    //                                                                                            .frame(height: 1).background(self.ANICK == "" && self.RedAlert ? Color.red : Color(#colorLiteral(red: 0.9263015985, green: 0.9348484874, blue: 0.9562863708, alpha: 1)))
                }
                
                VStack{
                    
                    TextField("Apartment No.".localized() + "*" ,text: $AHOME,onEditingChanged:  {_ in
                        
                        UserDefaults.standard.set(self.AHOME,forKey: "AHOME")
                        
                        
                        
                        
                    })
                    
                    .foregroundColor(Color.black)                   .font(.custom("Regular_Font".localized(), size: 14))
                    
                    Divider().frame(height: 1).background(self.AHOME == "" && self.RedAlert ? Color.red : Color(#colorLiteral(red: 0.9263015985, green: 0.9348484874, blue: 0.9562863708, alpha: 1)))
                    
                    
                    
                }
                
                VStack{
                    
                    TextField("Street".localized() + "*" ,text: $ASTREET,onEditingChanged:  {_ in
                        
                        
                        UserDefaults.standard.set(self.ASTREET,forKey: "ASTREET")
                        
                        
                        
                    })
                    
                    .foregroundColor(Color.black)                      .font(.custom("Regular_Font".localized(), size: 14))
                    
                    Divider().frame(height: 1).background(self.ASTREET == "" && self.RedAlert ? Color.red : Color(#colorLiteral(red: 0.9263015985, green: 0.9348484874, blue: 0.9562863708, alpha: 1)))
                    
                    
                }
                
                VStack{
                    
                    TextField("Additional Notes (Optional)", text: $ADNOTE,onEditingChanged:  {_ in
                        
                        
                        
                        UserDefaults.standard.set(self.ADNOTE,forKey: "ADNOTE")
                        
                    })
                    
                    .foregroundColor(Color.black)                   .font(.custom("Regular_Font".localized(), size: 14))
                    
                    Divider().frame(height: 1)
                    
                }
                
                //                   TextField("Mobile Number", text: $MOBILE)
                //
                //      .foregroundColor(Color("dartkthemeletter"))                    .font(.custom("Regular_Font".localized(), size: 12))
                //
                //                   Divider().frame(height: 1)
                
                
            }
        }
    }
    
    
    
    
    struct ExpandOfficeAddress: View {
        
        @Binding var ANICK :String
        @Binding var AHOME :String
        @Binding var ASTREET:String
        @Binding var AFLOOR:String
        @Binding var ADNOTE:String
        
        @Binding var AOFFICE:String
        
        @Binding var RedAlert:Bool
        
        var body: some View {
            VStack{
                
                VStack(spacing:20){
                    
                    
                    VStack{
                        
                        TextField("Nick Name".localized() ,text: self.$ANICK,onEditingChanged:  {_ in
                            
                            UserDefaults.standard.set(self.ANICK,forKey: "ANICK")
                            
                            
                            
                            
                        })
                        
                        .foregroundColor(Color.black)                    .font(.custom("Regular_Font".localized(), size: 14))
                        
                        Divider()
                        //                                                                .frame(height: 1).background(self.ANICK == "" && self.RedAlert ? Color.red : Color(#colorLiteral(red: 0.9263015985, green: 0.9348484874, blue: 0.9562863708, alpha: 1)))
                    }
                    VStack{
                        
                        TextField("Office No".localized() + "*", text: self.$AOFFICE,onEditingChanged:  {_ in
                            
                            UserDefaults.standard.set(self.AOFFICE,forKey: "AOFFICE")
                        })
                        
                        
                        .foregroundColor(Color.black)
                        .font(.custom("Regular_Font".localized(), size: 14))
                        
                        Divider().frame(height: 1).background(self.AOFFICE == "" && self.RedAlert ? Color.red : Color(#colorLiteral(red: 0.9263015985, green: 0.9348484874, blue: 0.9562863708, alpha: 1)))
                        
                    }
                    
                    VStack{
                        
                        TextField("Floor".localized() + "*", text: $AFLOOR,onEditingChanged:  {_ in
                            
                            
                            UserDefaults.standard.set(self.AFLOOR,forKey: "AFLOOR")
                        })
                        
                        .foregroundColor(Color.black)
                        .font(.custom("Regular_Font".localized(), size: 14))
                        
                        Divider().frame(height: 1).background(self.AFLOOR == "" && self.RedAlert ? Color.red : Color(#colorLiteral(red: 0.9263015985, green: 0.9348484874, blue: 0.9562863708, alpha: 1)))
                        
                    }
                }
                
                VStack{
                    
                    TextField("Building".localized() + "*" ,text: self.$AHOME,onEditingChanged:  {_ in
                        
                        UserDefaults.standard.set(self.AHOME,forKey: "AHOME")
                        UserDefaults.standard.set(self.ASTREET,forKey: "ASTREET")
                    })
                    
                    .foregroundColor(Color.black)                     .font(.custom("Regular_Font".localized(), size: 14))
                    
                    Divider().frame(height: 1).background(self.AHOME == "" && self.RedAlert ? Color.red : Color(#colorLiteral(red: 0.9263015985, green: 0.9348484874, blue: 0.9562863708, alpha: 1)))
                    
                }
                VStack{
                    
                    TextField("Street".localized() + "*" ,text: self.$ASTREET,onEditingChanged:  {_ in
                        UserDefaults.standard.set(self.ASTREET,forKey: "ASTREET")
                        
                    })
                    
                    .foregroundColor(Color.black)                   .font(.custom("Regular_Font".localized(), size: 14))
                    
                    Divider().frame(height: 1).background(self.ASTREET == "" && self.RedAlert ? Color.red : Color(#colorLiteral(red: 0.9263015985, green: 0.9348484874, blue: 0.9562863708, alpha: 1)))
                    
                }
                
                VStack{
                    
                    TextField("Additional Notes (Optional)", text: $ADNOTE,onEditingChanged:  {_ in
                        UserDefaults.standard.set(self.ADNOTE,forKey: "ADNOTE")
                        
                    })
                    
                    .foregroundColor(Color.black)                     .font(.custom("Regular_Font".localized(), size: 14))
                    
                    Divider().frame(height: 1)
                    
                }
                
                //                   TextField("Mobile Number", text: $MOBILE)
                //
                //      .foregroundColor(Color("dartkthemeletter"))                    .font(.custom("Regular_Font".localized(), size: 12))
                //
                //                   Divider().frame(height: 1)
                
                
            }
        }
    }
    
    
    
    
    
    
    
    struct VehicleCell: View {
        
        
        @ObservedObject  var vehicleCellVM:VehicleCellViewModel
        var onCommit:(Vehicle) -> (Void) = {_ in}
        @Binding var RedAlert: Bool
        
        var body: some View {
            
            
            
            
            VStack(spacing:8){
                
                TextField("Enter Plate Number", text: $vehicleCellVM.vehicle.VehicleNumber ,onCommit: {
                    
                    
                    
                    self.onCommit(self.vehicleCellVM.vehicle)
                    
                }).keyboardType(.numberPad)
                
                
                .foregroundColor(Color("darkthemeletter"))
                .font(.custom("Regular_Font".localized(), size: 14))
                
                Divider().frame(height: 1).background(vehicleCellVM.vehicle.VehicleNumber == "" && self.RedAlert ? Color.red : Color(#colorLiteral(red: 0.9263015985, green: 0.9348484874, blue: 0.9562863708, alpha: 1)))
                
                HStack{
                    Text(vehicleCellVM.vehicle.name).font(.custom("Light_Font".localized(), size: 10))                            .foregroundColor(Color("h6"))
                    
                    Spacer()
                }
            }
        }
    }
    
    func EnterOrderToDB(Node :String ,Order : [String:Any]) {
        
        self.ref.child("orders").child("all_orders").child(Node).setValue(Order)
        self.ref.child("drivers").child(self.selectedServiceProvider.DriverID).child("new_orders").child(Node).setValue(Node)
        
        self.ref.child("Users").child(Order["customer_id"] as! String).child("active_orders").child("order_ids").child(Node).setValue(Node)
        
        for vehicleset in self.VehicleList {
            
            let V : [String : Any] = ["add_on_services":vehicleset.AddonSnaps.value,
                                      
                                      "services" :vehicleset.ServiceSnap.value,
            ]
            
            self.ref.child("orders").child("all_orders").child(Node).child("services").child("\(vehicleset.VehicleType) & \(vehicleset.VehicleNumber)").setValue(V)
            
        }
        
        self.ref.child("Cart").child(Order["customer_id"] as! String).removeValue()
        
        
        
        
    }
    
}
//}
extension Notification {
    var keyboardHeight: CGFloat {
        return (userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect)?.height ?? 0
    }
}
extension Publishers {
    // 1.
    static var keyboardHeight: AnyPublisher<CGFloat, Never> {
        // 2.
        let willShow = NotificationCenter.default.publisher(for: UIApplication.keyboardWillShowNotification)
            .map { $0.keyboardHeight }
        
        let willHide = NotificationCenter.default.publisher(for: UIApplication.keyboardWillHideNotification)
            .map { _ in CGFloat(0) }
        
        // 3.
        return MergeMany(willShow, willHide)
            .eraseToAnyPublisher()
    }
}

struct KeyboardAdaptive: ViewModifier {
    @State private var bottomPadding: CGFloat = 0
    
    func body(content: Content) -> some View {
        // 1.
        GeometryReader { geometry in
            content
                .padding(.bottom, self.bottomPadding)
                // 2.
                .onReceive(Publishers.keyboardHeight) { keyboardHeight in
                    // 3.
                    let keyboardTop = geometry.frame(in: .global).height - keyboardHeight
                    // 4.
                    let focusedTextInputBottom = UIResponder.currentFirstResponder?.globalFrame?.maxY ?? 0
                    // 5.
                    self.bottomPadding = max(0, focusedTextInputBottom - keyboardTop - geometry.safeAreaInsets.bottom)
                }
                // 6.
                .animation(.easeOut(duration: 0.16))
        }
    }
}
extension UIResponder {
    static var currentFirstResponder: UIResponder? {
        _currentFirstResponder = nil
        UIApplication.shared.sendAction(#selector(UIResponder.findFirstResponder(_:)), to: nil, from: nil, for: nil)
        return _currentFirstResponder
    }
    
    private static weak var _currentFirstResponder: UIResponder?
    
    @objc private func findFirstResponder(_ sender: Any) {
        UIResponder._currentFirstResponder = self
    }
    
    var globalFrame: CGRect? {
        guard let view = self as? UIView else { return nil }
        return view.superview?.convert(view.frame, to: nil)
    }
}
