//
//  URLScemeNavigator.swift
//  Twaddan
//
//  Created by Spine on 28/07/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI

struct URLScemeNavigator: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct URLScemeNavigator_Previews: PreviewProvider {
    static var previews: some View {
        URLScemeNavigator()
    }
}
