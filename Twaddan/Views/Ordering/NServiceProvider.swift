//
//  NServiceProvider.swift
//  Twaddan
//
//  Created by Spine on 08/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI

struct NServiceProvider: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var SF = ["Nearest", "Best Rating", "Lowest price", "By Offer"]
    @State var SortAndFilter:Bool = false
    @State var selectedSF : String = "";
    
    var body: some View {
        
        
        ZStack{
            VStack{
                
                
                //.frame(width:self.SortAndFilter ? geometry.size.width :0)
                //.frame(height:self.SortAndFilter ? geometry.size.height : 0)
                
                VStack{
                    
                    Spacer().frame(height:150)
                    
                    Form{
                        HStack{
                            
                            Button(action : {
                                
                                
                                
                                
                                self.presentationMode.wrappedValue.dismiss()
                                
                                
                            }) {
                                Image("back").frame(width:25,height:20 )
                                
                                
                                
                            }
                            .padding(.leading, 25.0)
                            Spacer()
                            Text("Service Providers").font(.custom("Regular_Font".localized(), size: 20))
                            Spacer()
                            
                            Button(action: {}){
                                ZStack{ Image("bell").resizable().frame(width: 25, height: 30)
                                    
                                    Text("8")
                                        .frame(width: 20.0, height: 20.0)
                                        .background(Circle().fill(Color.red))
                                        .foregroundColor(.white)
                                        .padding([.leading, .bottom])
                                    
                                    
                                    
                                    
                                }
                                
                            }
                            .padding(.trailing, 15.0)
                            
                            Button(action: {
                                withAnimation
                                {
                                    self.SortAndFilter.toggle()
                                }
                                
                            }){
                                Image("sortandfilterpng")
                                    .resizable().frame(width: 25, height: 25)
                                
                            }
                            .padding(.trailing, 20)
                            
                            
                        }
                        .frame( height:30)
                        .background(Color.blue)
                        
                    }
                    
                    
                    
                    
                    //.padding(.top, 50)
                    Spacer()
                }.foregroundColor(Color("ManBackColor"))
                
                .cornerRadius(20)
                
                
                
            }
            
            
            
            
            
            
            
            
            //.frame(maxWidth:.infinity,maxHeight: .infinity)
            .background(Color.red)
            .navigationBarTitle("ServiceProvider",displayMode: .inline)
            .navigationBarHidden(true)
            .edgesIgnoringSafeArea(.all)
            
            
            
            
            
            
            
            
        }
    }
}

struct NServiceProvider_Previews: PreviewProvider {
    static var previews: some View {
        NServiceProvider()
    }
}
