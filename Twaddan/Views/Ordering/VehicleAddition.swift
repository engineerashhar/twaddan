//
//  VehicleAddition.swift
//  Twaddan
//
//  Created by Spine on 30/07/20.
//  Copyright © 2020 spine. All rights reserved.
//




import SwiftUI

struct VehicleAddition: View {
    
    @Binding var openVA : Bool
    @EnvironmentObject var settings : UserSettings
    @State var openSP : Bool = false
    @State var Vibrate : Int = 0
    @State var VEHICLES : [VEHICLETYPES] = [VEHICLETYPES]()
    @EnvironmentObject var vehicleItems : VehicleItems
    
    @State var V = false
    
    
    
    var body: some View {
        
        
        ZStack{
            ScrollView{
                //            NavigationLink(destination:ServiceProviders(openSPQ: self.$openSPQ).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                //                                                   )).environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                //                                                       )),isActive: self.$openSPL){
                //                                                           
                //                                                           EmptyView()
                //                                               }
                
                VStack{
                    Spacer().frame(height:40)
                    HStack{
                        Button(action: {
                            
                            self.openVA.toggle()
                        }){
                            Text("Cancel").padding()
                        }
                        Spacer()
                    }
                    
                    Spacer().frame(height:70)
                    HStack{
                        Spacer()
                        VStack{
                            Spacer()
                            Text("Select Vehicle").font(.custom("Bold_Font".localized(), size: 14)).foregroundColor(Color.white).multilineTextAlignment(.center).padding(.horizontal,70)
                            
                            Vehicles(VEHICLES: self.$VEHICLES).modifier(Shakes(animatableData: CGFloat(self.Vibrate)))
                                
                                
                                .animation(self.V ? Animation.default.repeatCount(3):nil)
                            
                            Button(action:{
                                if(self.VEHICLES.count>0){
                                    //                    UserDefaults.standard.set(self.VEHICLES, forKey: "VT")
                                    //                     var VArray : [[String:String]] =  [[String:String]]()
                                    //
                                    //                                        for V in self.VEHICLES{
                                    //                    //                        let Vname = ["name":V.name]
                                    //                    //                        let Vimage = ["image":V.image]
                                    //                                            let Vitem = ["name":V.name,"image":V.image]
                                    //                                            VArray.append(Vitem)
                                    //                                        }
                                    
                                    //                    var VData : [VehicleItems] = [VehicleItems]()
                                    //
                                    //
                                    //                    UserDefaults.standard.set(VData, forKey: "VT")
                                    //
                                    
                                    var VEHICLES2 : [VEHICLETYPES] = [VEHICLETYPES]()
                                    
                                    for V in self.VEHICLES{
                                        let NewV = VEHICLETYPES(id:Int.random(in: 0 ... 10000000000), image: V.image, name: V.name, index: V.index, Vid: V.Vid)
                                        self.vehicleItems.vehicleItem.append(NewV)
                                    }
                                    
                                    //                    self.vehicleItems.vehicleItem.append(contentsOf: self.VEHICLES)
                                    
                                    self.openVA.toggle()
                                }else{
                                    self.V = true
                                    self.Vibrate += 1
                                }
                            }){
                                HStack{
                                    Text("ADD").font(.custom("Bold_Font".localized(), size: 13)).foregroundColor(Color.white)
                                    
                                    Image(systemName: "arrow.right".localized())
                                        .foregroundColor(Color.white)
                                }.padding(.horizontal).padding(.vertical,20).frame(width:300).background(LinearGradient(gradient: Gradient(colors: [Color("g1"),Color("g2")]), startPoint: .top, endPoint: .bottom)).cornerRadius(20).clipped()
                            }
                            
                            Spacer()
                        }
                        Spacer()
                    }
                }
            }
        }.background(Color.clear).onAppear{
            self.VEHICLES.removeAll()
            UserDefaults.standard.set(self.VEHICLES, forKey: "VTYPE")
        }
        
        .edgesIgnoringSafeArea(.all)
    }
    struct Shakes: GeometryEffect {
        var amount: CGFloat = 10
        var shakesPerUnit = 3
        var animatableData: CGFloat
        
        func effectValue(size: CGSize) -> ProjectionTransform {
            ProjectionTransform(CGAffineTransform(translationX:
                                                    amount * sin(animatableData * .pi * CGFloat(shakesPerUnit)),
                                                  y: 0))
        }
    }
    
}


//class VehicleItems {
//
//      var name : String = ""
//      var image : String = ""
//      var id : String = ""
//
//
//  }
