//
//  Cart.swift
//  Twaddan
//
//  Created by Spine on 12/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI
import FirebaseDatabase


struct Cart: View {
    
    @EnvironmentObject var vehicleItems : VehicleItems
    @Binding  var GoToBookDetailsOld : Bool
    @State var GoToBookDetails : Bool = false
    @State var OpenCart : Bool = false
    @State var openSP : Bool = false
    @State var openVA : Bool = true
    @State var openSPD : Bool = false
    //   @Binding  var OpenCart : Bool
    //   @Binding  var GoToBookDetails : Bool
    
    @State  var serviceSnap : DataSnapshot = DataSnapshot()
    //  var SelectedDriver : String
    
    @State  var ETA : Double = 0.0
    @State var Driver : String = ""
    
    @State var ALERT1 : Bool = false
    
    @State var ALERT1MESSAGE :String = ""
    
    @State var openEditCart : Bool = false
    @State var openActionSheet : Bool = false
    
    @State var OpenCheckOut:Bool = false
    @EnvironmentObject var settings : UserSettings
    // @ObservedObject var cartFetch = CartFetch()
    let ref = Database.database().reference()
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var test:String = ""
    @State var carts = [CartStruct]()
    @State var selectedCart : CartStruct = CartStruct(id: "", addons: [Addons](), services: [Services](), vehicle: "", vehicleName: "")
    
    @State var selectedServiceProvider = SelectedSP()
    
    var body: some View {
        
        ZStack{
            Color("Color").edgesIgnoringSafeArea(.all)
            GeometryReader { geometry in
                
                
                
                
                
                VStack{
                    
                    NavigationLink(destination:ServiceProviders().environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                    )),isActive: self.$openSP){
                        
                        EmptyView()
                    }
                    
                    
                    NavigationLink( destination: EditFromCart(Cart: self.$selectedCart, selectedServiceProvider: self.$selectedServiceProvider), isActive: self.$openEditCart){
                        
                        EmptyView()
                    }
                    
                    //                NavigationLink( destination: ServiceProviderPage(serviceSnap: self.serviceSnap, ETA:self.ETA,Driver:self.Driver, GoButton: .constant(true)).environmentObject(self.vehicleItems).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                    //                                                                                    )), isActive: self.$openEditCart){
                    //                
                    //                                                                    EmptyView()
                    //                                                                }
                    //            }
                    
                    VStack{
                        
                        //                                     Spacer().frame(height:geometry.size.height*0.1)
                        
                        
                        
                        //
                        
                        
                        
                        
                        
                        
                        //.padding(.top, 50)
                        Spacer()
                    }     
                    .frame(height:50)
                    
                    Spacer() .frame(height:50)
                    VStack{
                        //   ScrollView{
                        VStack(spacing:10) {
                            List{
                                ForEach(self.carts){ cart in
                                    
                                    HStack{
                                        
                                        //   Spacer().frame(width:20)
                                        VStack{
                                            
                                            
                                            
                                            HStack{
                                                
                                                
                                                Text(LocNumbers(key:  String((self.carts.firstIndex(of: cart) ?? 0 )+1))).font(.custom("Regular_Font".localized(), size: 14)).foregroundColor(Color.white).padding(10).background(Color(#colorLiteral(red: 0, green: 0.2509999871, blue: 0.4979999959, alpha: 1))).clipShape(Circle()).padding(.trailing)
                                                
                                                //                                                                                                                                                                                                    Image("basicbackimage")
                                                AnimatedImage(url : URL (string: self.selectedServiceProvider.ServiceProvider_image))
                                                    .resizable()
                                                    .clipShape(Circle())                                                                              .overlay(Circle().stroke(Color("h8"),lineWidth: 2)).frame(width:44,height:44)
                                                //
                                                VStack(alignment:.leading){
                                                    
                                                    Text(cart.vehicleName)
                                                        .font(.custom("Bold_Font".localized(), size: 13)).foregroundColor(Color("h2"))
                                                    
                                                    
                                                    //                                                                                                                                      Text("JJJJJJJ")
                                                    Text(self.selectedServiceProvider.ServiceProvider)
                                                        .font(.custom("Regular_Font".localized(), size: 12))
                                                        .foregroundColor(Color("h6")).frame(height:9)
                                                        .padding(.vertical,2)
                                                }.padding(.leading,5)
                                                //                                                                .background(Color.red)
                                                Spacer()
                                                Text("\(LocNumbers(key: sumOfAll(cartStruct: cart)))")
                                                    //                                                                                                                    + " AED".localized())
                                                    
                                                    //                                                                                                                                                                                                        Text("150 AED")
                                                    
                                                    //   Text("100 AED")
                                                    .foregroundColor(Color("darkthemeletter"))
                                                    .font(.custom("Bold_Font".localized(), size: 14)).padding(.trailing,5)
                                                Button(action:{
                                                    
                                                    self.openActionSheet = true
                                                    self.selectedCart = cart
                                                    
                                                }){
                                                    Image("menu").resizable().frame(width:14 ,height: 16)
                                                        
                                                        .foregroundColor(Color("h8"))
                                                    //                                                                    .padding(.top,10)
                                                }
                                            }.padding(10).background(Color.white)
                                            
                                            
                                            Spacer()
                                            
                                            
                                            VStack(alignment: .leading,spacing: 0){
                                                
                                                
                                                
                                                
                                                
                                                //                                                            .background(Color.red)
                                                
                                                
                                                //                                                        Divider()
                                                
                                                HStack{
                                                    
                                                    
                                                    
                                                    Image("maintenance").renderingMode(.template)
                                                        .resizable()
                                                        .frame(width: 12, height: 12)
                                                        .padding(.all,5) .foregroundColor(Color("h8"))
                                                    
                                                    Text(getServices(cartStruct: cart))
                                                        .font(.custom("Bold_Font".localized(), size: 13))
                                                        .foregroundColor(Color("h8")).frame(height:16)
                                                    
                                                    Spacer()
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                }.padding(.vertical,4)
                                                
                                                
                                                HStack{
                                                    
                                                    
                                                    
                                                    Image("placeholder").renderingMode(.template)
                                                        .resizable()
                                                        .frame(width: 8, height: 12)
                                                        .padding(.all,7)
                                                        .foregroundColor(Color("h8"))
                                                    Text(UserDefaults.standard.string(forKey: "PLACE") != nil ? String(UserDefaults.standard.string(forKey: "PLACE")!) : "Location")
                                                        .font(.custom("Bold_Font".localized(), size: 13))
                                                        .foregroundColor(Color("h8")).frame(height:16)
                                                    
                                                    Spacer()
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                }.padding(.vertical,2)
                                                
                                                
                                                HStack{
                                                    
                                                    
                                                    
                                                    Image("prefix__start_1").renderingMode(.template)
                                                        .resizable()
                                                        .frame(width: 12, height: 12)
                                                        .padding(.all,5)
                                                        .foregroundColor(Color("h8"))
                                                    
                                                    Text(getTime(timeIntervel: self.selectedServiceProvider.ETA)).frame(height:16)
                                                        .font(.custom("Bold_Font".localized(), size: 13))
                                                        .foregroundColor(Color("h8"))
                                                    
                                                    Spacer()
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                }.padding(.vertical,2)
                                                
                                                //                                            Spacer()
                                                
                                                //                                                        .padding(.bottom,5)
                                            }.padding(.leading,10)
                                            
                                            
                                            
                                            Spacer()
                                            
                                        }
                                        
                                        .background(Color("ca1"))
                                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color("ca2"),lineWidth: 2)
                                        )                                  .cornerRadius(5)
                                        .shadow(radius: 2)
                                        
                                        // Spacer().frame(width:10)
                                        
                                    }.padding(5).frame(height:200)
                                    
                                    //                                HStack{
                                    //
                                    //                                                                                //   Spacer().frame(width:20)
                                    //                                                HStack(alignment:.top){
                                    //
                                    ////                                                                                        Image("basicbackimage")
                                    //                                                                                    AnimatedImage(url : URL (string: self.selectedServiceProvider.ServiceProvider_image))
                                    //                                                                                        .resizable()
                                    //                                                                                        .frame(width: 110, height: 130)
                                    //
                                    //                                                                                        .cornerRadius(5)
                                    //                                //                                                        .padding(.all, 8.0)
                                    //
                                    //
                                    //
                                    //                                                                                    Spacer()
                                    //
                                    //
                                    //                                                                                    VStack(alignment: .leading,spacing: 0){
                                    //
                                    //
                                    //
                                    //                                                                                        HStack{
                                    //                                                                                            Text(cart.vehicleName)
                                    //                                                                                                .foregroundColor(Color("darkthemeletter"))
                                    //                                                                                                .font(.custom("Bold_Font".localized(), size: 14))
                                    //
                                    //                                //                                                                .background(Color.red)
                                    //                                                                                            Spacer()
                                    //                                                                                            Text("\(LocNumbers(key: sumOfAll(cartStruct: cart)))"
                                    //                                                                                                + " AED".localized())
                                    //
                                    ////                                                                                                Text("150 AED")
                                    //
                                    //                                                                                                //   Text("100 AED")
                                    //                                                                                                .foregroundColor(Color("darkthemeletter"))
                                    //                                                                                                .font(.custom("Bold_Font".localized(), size: 14))
                                    //                                                                                            Button(action:{
                                    //
                                    //                                                                                                self.openActionSheet = true
                                    //                                                                                                self.selectedCart = cart
                                    //
                                    //                                                                                            }){
                                    //                                                                                                Image("menu").resizable().frame(width:14 ,height: 14)
                                    //
                                    //                                                                                                    .foregroundColor(Color("ManBackColor"))
                                    //                                //                                                                    .padding(.top,10)
                                    //                                                                                            }
                                    //                                                                                        }
                                    //
                                    //                                                                                        Text(self.selectedServiceProvider.ServiceProvider)
                                    //                                                                                            .font(.custom("Regular_Font".localized(), size: 12))
                                    //                                                                                            .foregroundColor(Color("darkthemeletter")).frame(height:16)
                                    //                                                                                            .padding(.vertical,2)
                                    //                                //                                                            .background(Color.red)
                                    //
                                    //
                                    //                                //                                                        Divider()
                                    //
                                    //                                                                                        HStack{
                                    //
                                    //
                                    //
                                    //                                                                                            Image("maintenance").renderingMode(.template)
                                    //                                                                                                .resizable()
                                    //                                                                                                .frame(width: 12, height: 12)
                                    //                                                                                                .padding(.all,5) .foregroundColor(Color.white)
                                    //                                                                                                .background(Color("lightorange")).clipShape(Circle())
                                    //
                                    //                                                                                            Text(getServices(cartStruct: cart))
                                    //                                                                                                .font(.custom("Regular_Font".localized(), size: 12))
                                    //                                                                                                .foregroundColor(Color("darkthemeletter")).frame(height:16)
                                    //
                                    //                                                                                            Spacer()
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //                                                                                        }.padding(.vertical,4)
                                    //                                                                                        Divider().padding(.leading,30)
                                    //
                                    //                                                                                        HStack{
                                    //
                                    //
                                    //
                                    //                                                                                            Image("placeholder").renderingMode(.template)
                                    //                                                                                                .resizable()
                                    //                                                                                                .frame(width: 8, height: 12)
                                    //                                                                                                .padding(.all,7)
                                    //                                                                                                .foregroundColor(Color.white).background(Color("destbackgreen")).clipShape(Circle())
                                    //
                                    //                                                                                            Text(UserDefaults.standard.string(forKey: "PLACE") != nil ? String(UserDefaults.standard.string(forKey: "PLACE")!) : "Location")
                                    //                                                                                                .font(.custom("Regular_Font".localized(), size: 12))
                                    //                                                                                                .foregroundColor(Color("darkthemeletter")).frame(height:16)
                                    //
                                    //                                                                                            Spacer()
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //                                                                                        }.padding(.vertical,2)
                                    //                                                                                        Divider().padding(.leading,30)
                                    //
                                    //                                                                                        HStack{
                                    //
                                    //
                                    //
                                    //                                                                                            Image("prefix__start_1").renderingMode(.template)
                                    //                                                                                                .resizable()
                                    //                                                                                                .frame(width: 12, height: 12)
                                    //                                                                                                .padding(.all,5)
                                    //                                                                                                .foregroundColor(Color.white).background(Color("lightblue")).clipShape(Circle())
                                    //
                                    //                                                                                            Text(getTime(timeIntervel: self.selectedServiceProvider.ETA)).frame(height:16)
                                    //                                                                                                .font(.custom("Regular_Font".localized(), size: 12))
                                    //                                                                                                .foregroundColor(Color("darkthemeletter"))
                                    //
                                    //                                                                                            Spacer()
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //                                                                                        }.padding(.vertical,2)
                                    //
                                    //                                            //                                            Spacer()
                                    //                                                                                         Divider().padding(.leading,30)
                                    //                                //                                                        .padding(.bottom,5)
                                    //                                                                                    }.padding(.leading,5)
                                    //
                                    //                                                                                    Spacer()
                                    //
                                    //                                                }.padding(.vertical,10).padding(.leading,10).padding(.trailing,2)
                                    //
                                    //                                                                                .background(Color.white)
                                    //                                                                                .cornerRadius(5)
                                    //                                                                                .shadow(radius: 2)
                                    //
                                    //                                                                                // Spacer().frame(width:10)
                                    //
                                    //                                }.padding(5)
                                }.onDelete(perform: self.removeRows)
                                .listRowInsets(EdgeInsets())
                                .listRowBackground(Color("h1"))
                                
                            }
                        }
                        .actionSheet(isPresented: self.$openActionSheet, content: {
                            ActionSheet(title: Text("Action"), message: Text("Choose Action"), buttons: [
                                            .default(Text("Edit")) {
                                                self.openEditCart = true
                                            },
                                            .default(Text("Delete")) {
                                                self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(self.selectedCart.id).removeValue()                                                    },
                                            .cancel()])
                            
                        })
                        .frame(width:geometry.size.width, height:
                                //                            CGFloat( self.carts.count * 200) > geometry.size.height*0.75 ?
                                geometry.size.height*0.75
                               //                                : CGFloat( self.carts.count * 200)
                        )
                        
                        //}
                    }.frame(width:geometry.size.width,
                            height:
                                //                    CGFloat( self.carts.count * 200) >
                                geometry.size.height*0.75
                            //                            ? geometry.size.height*0.75
                            //                    : CGFloat( self.carts.count * 200)
                    )
                    
                    //.padding(.top,150)
                    
                    
                    
                    Spacer() .frame(height:180)
                }
                
                
                VStack{
                    Spacer()
                    
                    VStack{
                        
                        //                    HStack{
                        //                        Text("Sub Total").font(.custom("Bold_Font".localized(), size: 16))
                        //                        Spacer()
                        //                        Text("Price").font(.custom("Regular_Font".localized(), size: 16))
                        //                        Text("\(self.getSubTotal()) AED").font(.custom("Bold_Font".localized(), size: 16))
                        //
                        //                    }.padding()
                        //                        .padding(.vertical,15)
                        //                        .background(Color.white)
                        //
                        //                        .shadow(radius: 1)
                        
                        HStack{
                            Text("Total").font(.custom("Regular_Font".localized(), size: 16))
                            Spacer()
                            Text("\(LocNumbers(key: self.getSubTotal()))" + " AED".localized()).font(.custom("ExtraBold_Font".localized(), size: 16))
                        }.padding(.horizontal).environment(\.layoutDirection, .leftToRight).padding(.top,5)
                        
                        
                        HStack{
                            Button(action:{
                                
                                if(self.carts.count>0){
                                    self.serviceSnap = self.selectedServiceProvider.SPSnap
                                    
                                    
                                    self.ETA  = self.selectedServiceProvider.ETA
                                    self.Driver  = self.selectedServiceProvider.DriverID
                                    
                                    self.GoToBookDetails = true
                                }else{
                                    self.openSP = true
                                }
                                
                                
                            })  {
                                HStack(alignment:.center){
                                    
                                    //                                                                                    Spacer()
                                    Image(systemName:
                                            //                                        self.VEHICLES.contains(V) ?
                                            "plus.circle.fill"
                                          //                                            : "circle"
                                    )
                                    .resizable()
                                    
                                    .frame(width: 25, height: 24)
                                    
                                    .foregroundColor(Color("green")).padding(.trailing)
                                    
                                    
                                    Text("ADD VEHICLE").font(.custom("Bold_Font".localized(), size: 13)).foregroundColor(Color("h2"))
                                }.padding(.horizontal)
                                
                                
                            }.sheet(isPresented: self.$GoToBookDetails){
                                
                                
                                
                                BookingDetails(OpenCart: self.$OpenCart,GoToBookDetails:self.$GoToBookDetails ,serviceSnap: self.serviceSnap, SelectedDriver:
                                                self.Driver,ETA: self.ETA, openVA: self.$openVA ).environmentObject(self.settings).environmentObject(self.vehicleItems).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                                                ))
                                
                                
                                
                            }
                            
                            Button(action:{
                                
                                //   print("Started")
                                //  self.cartFetch.readCart(settings: self.settings)
                                //CheckOut()
                                if(self.carts.count>0){
                                    self.OpenCheckOut = true
                                }else{
                                    self.ALERT1MESSAGE = "Please add item to cart for check out".localized()
                                    self.ALERT1 = true
                                }
                                
                                
                            }){
                                RoundedPanel()
                                    
                                    .overlay(
                                        
                                        HStack(alignment: .center){
                                            //                                    Spacer()
                                            Text("CHECKOUT").font(.custom("ExtraBold_Font".localized(), size: 18))
                                                .foregroundColor(.white)
                                            //                                    Spacer()
                                            //
                                            //                                    Text(LocNumbers(key:String(self.carts.count))) .frame(width:35,height:35)
                                            //                                        .background(Circle().stroke())
                                            //                                     .foregroundColor(.white)
                                            //
                                            //                                       Spacer()
                                            //                                  Text("TOTAL \(LocNumbers(key: self.getSubTotal())) AED").font(.custom("ExtraBold_Font".localized(), size: 16))
                                            //                                    .foregroundColor(.white)
                                            //                                    Spacer()
                                        }
                                    )
                                
                            }
                            // Spacer()
                            
                            .frame(maxWidth:.infinity)
                            .alert(isPresented: self.$ALERT1) {
                                Alert(title: Text( "Message"), message: Text(self.ALERT1MESSAGE), dismissButton: .default(Text("OK")))
                            }
                            
                        }.environment(\.layoutDirection, .leftToRight)
                        
                        
                    }.background(Color("h1"))
                    
                    
                }
                
                NavigationLink( destination: DetailAddressPage(), isActive: self.$OpenCheckOut){
                    
                    Text("").frame(height: 0)
                }
                
                //            NavigationLink( destination: EditFromCart(Cart: self.selectedCart, selectedServiceProvider: self.selectedServiceProvider), isActive: self.$OpenCheckOut){
                //
                //                           EmptyView()
                //                       }
                //            }
                
                
                
                
            }
            .background(Color("h1"))
            .onAppear{
                
                //            let appearance = UINavigationBarAppearance()
                //
                //                         appearance.backgroundColor = UIColor(named:"backcolor2")
                ////             appearance.backgroundColor = .red
                //                  appearance.shadowColor = .black
                //
                //
                //                         UINavigationBar.appearance().standardAppearance = appearance
                //
                
                print("i was called")
                //self.cartFetch.readCart(settings: self.settings)
                
                //            print("Started 2 \(UserDefaults.standard.string(forKey: "Aid") ?? "0")")
                self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").keepSynced(true)
                
                self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").observe(DataEventType.value, with: {
                    
                    (Msnapshot) in
                    
                    
                    print("read")
                    if(Msnapshot.exists()){
                        
                        self.ref.child("service_providers").child(String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider_id").value)!)).observeSingleEvent(of: DataEventType.value) { (snap) in
                            
                            
                            
                            
                            self.carts.removeAll()
                            
                            self.selectedServiceProvider =   SelectedSP(
                                
                                ServiceProvider: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider").value)!),
                                ServiceProvider_id: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider_id").value)!),
                                ServiceProvider_image: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider_image").value)!),DriverID: String(describing :  (Msnapshot.childSnapshot(forPath:"DriverID").value)!),ETA :Msnapshot.childSnapshot(forPath:"ETA").value as? Double ?? 0.0 ,SPSnap: snap)
                            
                            
                            
                            for ID in Msnapshot.childSnapshot(forPath:"Vehicles").children {
                                let id = ID as! DataSnapshot
                                
                                
                                var addonslist = [Addons]()
                                var servicelist = [Services]()
                                
                                
                                
                                for SERVICE in id.childSnapshot(forPath:"services").children {
                                    let serv_snap = SERVICE as! DataSnapshot
                                    
                                    let service : Services = Services(id: serv_snap.key,
                                                                      serviceName: String(describing :  (serv_snap.childSnapshot(forPath:"name".localized()).value)!),
                                                                      serviceNameO: String(describing :  (serv_snap.childSnapshot(forPath:"name_ar".localized()).value)!),
                                                                      serviceDesc: String(describing :  (serv_snap.childSnapshot(forPath:"description".localized()).value)!),serviceDescO:String(describing :  (serv_snap.childSnapshot(forPath:"description_ar".localized()).value)!) ,
                                                                      ServiceImage: String(describing :  (serv_snap.childSnapshot(forPath:"image_url").value)!),
                                                                      ServicePrice: String(describing :  (serv_snap.childSnapshot(forPath:"price").value)!),ServiceTime: String(describing :  (serv_snap.childSnapshot(forPath:"time_required").value)!))
                                    servicelist.append(service)
                                }
                                
                                for ADDONS in id.childSnapshot(forPath:"addons").children{
                                    
                                    
                                    let add_snap = ADDONS as! DataSnapshot
                                    
                                    
                                    let addon : Addons = Addons(id: add_snap.key,
                                                                addonName: String(describing :  (add_snap.childSnapshot(forPath:"name".localized()).value)!),addonNameO: String(describing :  (add_snap.childSnapshot(forPath:"name_ar".localized()).value)!),addonDesc:  String(describing :  (add_snap.childSnapshot(forPath:"description".localized()).value ?? "")),addonDescO: String(describing :  (add_snap.childSnapshot(forPath:"description_ar".localized()).value ?? "")) ,
                                                                addonImage: String(describing :  (add_snap.childSnapshot(forPath:"image_url").value)!),
                                                                addonPrice: String(describing :  (add_snap.childSnapshot(forPath:"price").value)!))
                                    addonslist.append(addon)
                                    
                                }
                                
                                let cart : CartStruct = CartStruct(id:id.key , addons:addonslist , services: servicelist , vehicle: String(describing :  (id.childSnapshot(forPath:"Vehicle").value)!), vehicleName: String(describing :( id.childSnapshot(forPath:"VehicleName").value)!)
                                                                   //                            ,ServiceProvider: String(describing :  (id.childSnapshot(forPath:"ServiceProvider").value)!),ServiceProvider_id: String(describing :  (id.childSnapshot(forPath:"ServiceProvider_id").value)!),ServiceProvider_image: String(describing :  (id.childSnapshot(forPath:"ServiceProvider_image").value)!)
                                                                   
                                )
                                
                                //                         print("Started 3 \(self.carts.count)")
                                self.carts.append(cart)
                                
                            }
                            
                            //                        if(Msnapshot.childSnapshot(forPath:"Vehicles").childrenCount==0){
                            //                                                 self.GoToBookDetails = true
                            //                                             }
                            
                        }
                    }
                })
                
                
                
            }    .navigationBarTitle("Cart",displayMode: .inline)
            //              .navigationBarItems(trailing: EditButton())
            //        .navigationBarItems(trailing: Button(action:{
            //
            //            if(self.carts.count>0){
            //                                       self.serviceSnap = self.selectedServiceProvider.SPSnap
            //
            //
            //                                       self.ETA  = self.selectedServiceProvider.ETA
            //                                       self.Driver  = self.selectedServiceProvider.DriverID
            //
            //                                         self.GoToBookDetails = true
            //            }else{
            //                self.openSP = true
            //            }
            //
            //
            //                                   })  {
            //                                              HStack(alignment:.center){
            //
            //                                                                        Spacer()
            //                                                                      Image(systemName:
            //                                                                          //                                        self.VEHICLES.contains(V) ?
            //                                                                                                                      "plus.circle.fill"
            //                                                                          //                                            : "circle"
            //                                                                                                              )
            //                                                                                                                                                                                                                                                            .resizable()
            //
            //                                                                                                                                                                                                                                                               .frame(width: 25, height: 24)
            //
            //                                                                                                                  .foregroundColor(Color("green")).padding(.trailing)
            //
            //
            //                                                                          Text("ADD VEHICLE").font(.custom("Bold_Font".localized(), size: 13)).foregroundColor(Color("h2"))
            //                                                                     }
            //
            //
            //                                          }.sheet(isPresented: self.$GoToBookDetails){
            //
            //
            //
            //                                              BookingDetails(OpenCart: self.$OpenCart,GoToBookDetails:self.$GoToBookDetails ,serviceSnap: self.serviceSnap, SelectedDriver:
            //                                                self.Driver,ETA: self.ETA ).environmentObject(self.settings).environmentObject(self.vehicleItems).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
            //        ))
            //
            //
            //
            //                                          } )
            //            .navigationBarHidden(true)
            .edgesIgnoringSafeArea(.top)
            
        }
    }
    
    
    
    func removeRows(at offsets: IndexSet) {
        //  numbers.remove(atOffsets: offsets)
        guard let index : Int = Array(offsets).first else { return }
        
        let ID =  self.carts[index].id
        
        self.ref.child("Cart").child(settings.Aid).child("Vehicles").child(ID).removeValue()
    }
    
    
    func getSubTotal() -> String {
        var SubTotal:Double = 0.0
        
        for cartStruct in ( self.carts ){
            
            //   var cartSum:Double = 0.0
            
            var sumOfServices:Double = 0.0
            
            for service in cartStruct.services {
                
                sumOfServices += Double(service.ServicePrice)!
            }
            
            var sumOfAddons:Double = 0.0
            
            for Addons in cartStruct.addons {
                
                sumOfAddons += Double(Addons.addonPrice)!
            }
            SubTotal += sumOfServices+sumOfAddons
        }
        
        return String(format: "%.1f",SubTotal )
        
    }
    
    
    
    
    
    
}

struct Cart_Previews: PreviewProvider {
    static var previews: some View {
        Cart(GoToBookDetailsOld: .constant(false)).environmentObject(UserSettings())
    }
}


struct SelectedSP {
    
    //var id : String = ""
    var ServiceProvider : String = ""
    var ServiceProvider_id : String = ""
    var ServiceProvider_image : String = ""
    var DriverID : String = ""
    var ETA : Double = 0.0
    var SPSnap :DataSnapshot = DataSnapshot()
    
}

func sumOfAll(cartStruct:CartStruct) -> String {
    
    var sumOfServices:Double = 0.0
    
    for service in cartStruct.services {
        
        sumOfServices += Double(service.ServicePrice)!
    }
    
    var sumOfAddons:Double = 0.0
    
    for Addons in cartStruct.addons {
        
        sumOfAddons += Double(Addons.addonPrice)!
    }
    
    return String(format: "%.1f", sumOfServices+sumOfAddons)
    
}
func getServices(cartStruct:CartStruct) -> String{
    
    var AllServices : String = ""
    for service in cartStruct.services {
        
        AllServices += service.serviceName+", "
    }
    
    for addons in cartStruct.addons {
        
        AllServices += addons.addonName+", "
    }
    
    return String(AllServices.prefix(AllServices.count-2))
    
}




//
//
//
//
////
////  Cart.swift
////  Twaddan
////
////  Created by Spine on 12/05/20.
////  Copyright © 2020 spine. All rights reserved.
////
//
//import SwiftUI
//import SDWebImageSwiftUI
//import FirebaseDatabase
//
//
//struct Cart: View {
//
//    @State var OpenCheckOut:Bool = false
//          @EnvironmentObject var settings : UserSettings
//   // @ObservedObject var cartFetch = CartFetch()
//      let ref = Database.database().reference()
//    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
//    @State var test:String = ""
//    @State var carts = [CartStruct]()
//
//    var body: some View {
//
//      
//
//        GeometryReader { geometry in
//
//
//
//
//
//            VStack{
//
//        VStack{
//
//                                     Spacer().frame(height:geometry.size.height*0.1)
//
//            HStack{
//
//                Button(action : {
//
//
//
//                  //  self.cartFetch.readCart(settings: self.settings)
//
//                    self.presentationMode.wrappedValue.dismiss()
//
//
//                }) {
//                    Image("back").frame(width:24,height:1[rr[r[r[[rrrrr ] )
//
//
//
//                    }
//                .padding(.leading, 25.0)
//                Spacer().frame(width: 40)
//
//                Text("Cart").font(.custom("Regular_Font".localized(), size: 16))
//
//
//
//                Spacer()
//
//
//
//
//            }
//           .foregroundColor(Color("ManBackColor"))
//            .background(Color("backcolor2"))
//             .padding(.vertical,15)
//
//
//
//
//
//
//
//
//            //.padding(.top, 50)
//          Spacer()
//        }       .background(Color("backcolor2"))
//            .frame(height:50)
//
//         Spacer() .frame(height:50)
//                VStack{
//             //   ScrollView{
//                    VStack(spacing:10) {
//                        List{
//                ForEach(self.carts){ cart in
//
//                                 HStack{
//
//                                  //   Spacer().frame(width:20)
//                                     HStack{
//
//                                     //    Image("basicbackimage")
//                                        AnimatedImage(url : URL (string: cart.ServiceProvider_image))
//                                            .resizable()
//                                             .frame(width: 100, height: 110)
//
//                                             .cornerRadius(5)
//                                             .padding(.all, 8.0)
//
//                                        Spacer()
//
//
//                                         VStack(alignment: .leading){
//
//
//
//                                            HStack{
//                                                Text(cart.vehicle)
//                                                 .foregroundColor(Color("darkthemeletter"))
//                                                 .font(.custom("Bold_Font".localized(), size: 14)).padding(.top,20)
//
//
//                                                Spacer()
//                                                Text("\(sumOfAll(cartStruct: cart)) AED")
//
//                                                  //   Text("100 AED")
//                                                .foregroundColor(Color("darkthemeletter"))
//                                                .font(.custom("Bold_Font".localized(), size: 14)).padding(.top,20)
//                                                Button(action:{}){
//                                                Image("menu").resizable().frame(width:14 ,height: 14)
//
//                                                    .foregroundColor(Color("ManBackColor"))
//                                                .padding(.top,20)
//                                                }
//                                            }
//
//                                            Text(cart.ServiceProvider)
//                                                                                               .font(.custom("Regular_Font".localized(), size: 12))
//                                                                                               .foregroundColor(Color("darkthemeletter"))
//
//
//
//                                            Divider()
//
//                                             HStack{
//
//
//
//                                                 Image("destination")
//                                                     .frame(width: 16, height: 16)
//                                                     .background(Color("lightorange")).clipShape(Circle())
//
//                                                 Text(getServices(cartStruct: cart))
//                                                     .font(.custom("Regular_Font".localized(), size: 12))
//                                                     .foregroundColor(Color("darkthemeletter"))
//
//                                                 Spacer()
//
//
//
//
//
//
//
//
//                                             }
//                                            Divider()
//
//                                                                                        HStack{
//
//
//
//                                                                                            Image("destination")
//                                                                                                .frame(width: 16, height: 16)
//                                                                                                .background(Color("destbackgreen")).clipShape(Circle())
//
//                                                                                            Text("location Address")
//                                                                                                .font(.custom("Regular_Font".localized(), size: 12))
//                                                                                                .foregroundColor(Color("darkthemeletter"))
//
//                                                                                            Spacer()
//
//
//
//
//
//
//
//
//                                                                                        }
//                                            Divider()
//
//                                                                                        HStack{
//
//
//
//                                                                                            Image("destination")
//                                                                                                .frame(width: 16, height: 16)
//                                                                                                .background(Color("lightblue")).clipShape(Circle())
//
//                                                                                            Text("Service at 10:00 PM")
//                                                                                                .font(.custom("Regular_Font".localized(), size: 12))
//                                                                                                .foregroundColor(Color("darkthemeletter"))
//
//                                                                                            Spacer()
//
//
//
//
//
//
//
//
//                                                                                        }
//                                             Spacer()
//
//                                         }
//
//                                         Spacer()
//
//                                     }
//
//                                     .background(Color.white)
//                                     .cornerRadius(5)
//                                     .shadow(radius: 2)
//
//                                    // Spacer().frame(width:10)
//
//                                 }
//                }.onDelete(perform: self.removeRows)
//
//                    }
//                    }.frame(width:geometry.size.width, height:geometry.size.height*0.8)
//
//            //}
//                }.frame(width:geometry.size.width, height:geometry.size.height*0.8)
//
//            //.padding(.top,150)
//                Spacer() .frame(height:180)
//            }
//
//
//            VStack{
//                Spacer()
//
//                VStack{
//
//                    HStack{
//                        Text("Sub Total").font(.custom("Bold_Font".localized(), size: 16))
//                        Spacer()
//                        Text("Price").font(.custom("Regular_Font".localized(), size: 16))
//                        Text("\(self.getSubTotal()) AED").font(.custom("Bold_Font".localized(), size: 16))
//
//                    }.padding()
//                        .padding(.vertical,15)
//                        .background(Color.white)
//
//                        .shadow(radius: 1)
//
//                    Button(action:{
//
//                       //   print("Started")
//                         //  self.cartFetch.readCart(settings: self.settings)
//                      //CheckOut()
//                        self.OpenCheckOut = true
//
//
//                    }){
//                        RoundedPanel()
//
//                            .frame(height:50)                            .overlay(
//
//                                HStack{
//
//                                    Text("Check Out").font(.custom("ExtraBold_Font".localized(), size: 18))
//                                        .foregroundColor(.white)
//                                    Spacer()
//
//                                    Text(String(self.carts.count)) .frame(width:35,height:35)
//                                        .background(Circle().stroke())
//                                     .foregroundColor(.white)
//
//                                       Spacer()
//                                  Text("Total \(self.getSubTotal()) AED").font(.custom("ExtraBold_Font".localized(), size: 16))
//                                    .foregroundColor(.white)
//                                }
//                        )
//                            .padding(.horizontal,30)
//                            .background(Color("ManBackColor"))
//                            .cornerRadius(5)
//                    }
//                        // Spacer()
//
//                        .frame(maxWidth:.infinity)
//
//
//                }
//
//
//            }
//
//            NavigationLink(destination: CheckOut(), isActive: self.$OpenCheckOut){
//
//                Text("")
//            }
//
//
//            }
//        
//        
//        
//
//        }
//        .onAppear{
//          print("i was called")
//          //self.cartFetch.readCart(settings: self.settings)
//
//            print("Started 2 \(UserDefaults.standard.string(forKey: "Aid") ?? "0")")
//
//
//            self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").observe(DataEventType.value, with: {
//
//                      (Msnapshot) in
//
//                      self.carts.removeAll()
//
//
//
//                     for ID in Msnapshot.children {
//                           let id = ID as! DataSnapshot
//
//
//                         var addonslist = [Addons]()
//                     var servicelist = [Services]()
//
//
//
//                         for SERVICE in id.childSnapshot(forPath:"services").children {
//                             let serv_snap = SERVICE as! DataSnapshot
//
//                             let service : Services = Services(id: serv_snap.key,
//                                                               serviceName: String(describing :  (serv_snap.childSnapshot(forPath:Localize(key:"name")).value)!),
//                                                               serviceDesc: String(describing :  (serv_snap.childSnapshot(forPath:Localize(key:"description")).value)!),
//                                                               ServiceImage: String(describing :  (serv_snap.childSnapshot(forPath:"image_url").value)!),
//                                                               ServicePrice: String(describing :  (serv_snap.childSnapshot(forPath:"price").value)!))
//                             servicelist.append(service)
//                         }
//
//                         for ADDONS in id.childSnapshot(forPath:"addons").children{
//
//
//                                          let add_snap = ADDONS as! DataSnapshot
//
//
//                             let addon : Addons = Addons(id: add_snap.key,
//                                                         addonName: String(describing :  (add_snap.childSnapshot(forPath:Localize(key:"name")).value)!),
//                                                         addonImage: String(describing :  (add_snap.childSnapshot(forPath:"image_url").value)!),
//                                                         addonPrice: String(describing :  (add_snap.childSnapshot(forPath:"price").value)!))
//                             addonslist.append(addon)
//
//                         }
//
//                         let cart : CartStruct = CartStruct(id:id.key , addons:addonslist , services: servicelist , vehicle: String(describing :  (id.childSnapshot(forPath:"Vehicle").value)!),ServiceProvider: String(describing :  (id.childSnapshot(forPath:"ServiceProvider").value)!),ServiceProvider_id: String(describing :  (id.childSnapshot(forPath:"ServiceProvider_id").value)!),ServiceProvider_image: String(describing :  (id.childSnapshot(forPath:"ServiceProvider_image").value)!))
//
//                         print("Started 3 \(self.carts.count)")
//                         self.carts.append(cart)
//
//                     }
//                 })
//
//
//
//        }
//
//
//    }
//
//    func removeRows(at offsets: IndexSet) {
//      //  numbers.remove(atOffsets: offsets)
//        guard let index : Int = Array(offsets).first else { return }
//
//     let ID =  self.carts[index].id
//
//        self.ref.child("Cart").child(settings.Aid).child(ID).removeValue()
//    }
//
//
//    func getSubTotal() -> String {
//         var SubTotal:Double = 0.0
//
//        for cartStruct in ( self.carts ){
//
//         //   var cartSum:Double = 0.0
//
//        var sumOfServices:Double = 0.0
//
//        for service in cartStruct.services {
//
//            sumOfServices += Double(service.ServicePrice)!
//        }
//
//        var sumOfAddons:Double = 0.0
//
//        for Addons in cartStruct.addons {
//
//            sumOfAddons += Double(Addons.addonPrice)!
//        }
//            SubTotal += sumOfServices+sumOfAddons
//        }
//
//        return String(format: "%.1f",SubTotal )
//
//    }
//}
//
//struct Cart_Previews: PreviewProvider {
//    static var previews: some View {
//        Cart().environmentObject(UserSettings())
//    }
//}
//
//
//
//
//func sumOfAll(cartStruct:CartStruct) -> String {
//
//    var sumOfServices:Double = 0.0
//
//    for service in cartStruct.services {
//
//        sumOfServices += Double(service.ServicePrice)!
//    }
//
//    var sumOfAddons:Double = 0.0
//
//    for Addons in cartStruct.addons {
//
//        sumOfAddons += Double(Addons.addonPrice)!
//    }
//
//    return String(format: "%.1f", sumOfServices+sumOfAddons)
//
//}
//func getServices(cartStruct:CartStruct) -> String{
//
//    var AllServices : String = ""
//    for service in cartStruct.services {
//
//       AllServices += service.serviceName+" "
//    }
//
//    return AllServices
//
//}
