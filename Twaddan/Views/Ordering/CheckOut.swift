//
//  CheckOut.swift
//  Twaddan
//
//  Created by Spine on 14/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import MapKit
import FirebaseDatabase
import FirebaseAuth
import Foundation
import PassKit
import Network

struct CheckOut: View {
    
    @State var timer = Timer.publish(every: 5, on: .main, in: .common).autoconnect()
    
    @ObservedObject var findOffers  = FindOffer()
    
    @Binding var NAME : String
    @Binding var MOBILE : String
    @Binding var LNAME : String
    var ADDRESS : String
    
    @State var O:[String:String] = [String:String]()
    @State var N:String = ""
    
    @State var VERCODE:String = ""
    
    @State var VehicleList : [VehicleSets] = [VehicleSets]()
    @State var showAlert:Bool = false
    @State var selectedServiceProvider : SelectedServiceProvider = SelectedServiceProvider()
    @State var errorStatus : String  = ""
    @State var verificationID : String? = ""
    @State var ALERT1:Bool = false
    @State var BLUR:Bool = false
    @State var ALERT2:Bool = false
    @State var ALERT3:Bool = false
    @State var openHome = false
    @State var VCode : String = ""
    @State var displayETA = false
    @State var calculateETA = false
    @State var goToMap = false
    
    @State var Connected = false
    @State var failure = false
    
    @State var billingAddress = Billing(firstName: "", lastName: "", address1: "", city: "", countryCode: "")
    @State var OR :[String:Any] = [String:Any]()
    @State var PStatus :String = "Fresh"
    @State var k : [Product] = [Product]()
    
    
    @State var ShowApay = false
    
    @ObservedObject var cartFetch :CartFetch = CartFetch()
    @State var ErrorComments : String = ""
    @State var storedPhone : String = ""
    @State var ErrorStatus :Bool = false
    @State var VoucherApplied : Bool = false
    @State var carts : [CartStruct] = [CartStruct]()
    
    let ref = Database.database().reference()
    //   @State var Total : Double
    @State var ServiceCharge : Double = 0.0
    @State var Discount : Double = 0.0
    //   @State var SubTotal : Double = 0.0
    
    @State var  PaymentType : Int = 0
    
    @Environment(\.presentationMode) var presentationMode:
        Binding<PresentationMode>
    @State var VoucherCode:String = ""
    
    @State var ApplePay : Bool = false
    @State var CreditCard : Bool = false
    @State var DebitCard : Bool = false
    @State var NetBanking : Bool = false
    @State var CashOnDelivery : Bool = true
    @EnvironmentObject var settings: UserSettings
    
    @State var openTracking : Bool = false
    @State var OrderID : String = ""
    @State var showPaymentPortal : Bool = false
    
    @State var SOffers:[Offer] = [Offer]()
    
    @State var ViewCodes:Bool = false
    @State var OfferCodes: [String] = [String]()
    @State var buttonArray: [ActionSheet.Button] = [ActionSheet.Button]()
    @State var ViewLoader : Bool = false
    
    let applePaymentHandler = ApplePaymentHandler()
    let paymentHandler = CPaymentHandler()
    
    
    var body: some View {
        
        
        
        GeometryReader { geometry in
            ZStack{
                
                Color("h1").edgesIgnoringSafeArea(.all)
                ZStack{
                    
                    //                     Color("h1").edgesIgnoringSafeArea(.all)
                    
                    NavigationLink(destination : FindLocation(CalculateETA: self.$calculateETA) ,isActive: self.$goToMap ){
                        
                        EmptyView()
                    }
                    
                    
                    VStack{
                        
                        //                                                  .foregroundColor(Color("ManBackColor"))
                        //                                                   .background(Color("backcolor2"))
                        
                        
                        
                        
                        
                        
                        //            HStack(alignment: .bottom){
                        //
                        //                   Button(action : {
                        //
                        //
                        //
                        //
                        //                       self.presentationMode.wrappedValue.dismiss()
                        //
                        //
                        //                   }) {
                        //                       Image("back").frame(width:24,height:19 )
                        //
                        //
                        //
                        //                   }
                        //                   .padding(.leading, 25.0)
                        //                   Spacer().frame(width: 40)
                        //                   Text("Check out").font(.custom("Regular_Font".localized(), size: 16))
                        //
                        //
                        //                   Spacer()
                        //
                        //
                        //
                        //
                        ////            }
                        //            .padding(.top)
                        //            .frame(height:100)
                        //              .foregroundColor(Color("ManBackColor"))
                        //               .background(Color("backcolor2"))
                        //
                        
                        
                        ScrollView{
                            
                            
                            
                            
                            HStack{
                                Spacer()
                                
                                
                                
                                
                            }.frame(width: geometry.size.width, height: geometry.size.height*0.1)
                            
                            //.padding(.top, 50)
                            
                            
                            
                            //            HStack{
                            //
                            //                Text("Address").font(.custom("Bold_Font".localized(), size: 16)).foregroundColor(Color("darkthemeletter"))
                            //                    .padding()
                            //                Spacer()
                            //                Text("Change").font(.custom("Regular_Font".localized(), size: 14))
                            //                    .padding().foregroundColor(Color("darkthemeletter")).hidden()
                            //
                            //
                            //
                            //            }
                            
                            
                            //            Image("basicbackimage")
                            //                .resizable()
                            
                            VStack{
                                
                                ZStack(alignment:.top){
                                    
                                    FixedMapView(centerCoordinates:CLLocationCoordinate2D(latitude: UserDefaults.standard.double(forKey: "latitude"), longitude: UserDefaults.standard.double(forKey: "longitude")) )
                                        .overlay(Image("pin_new") .resizable().frame(width:12, height:30).padding(.bottom,20))
                                        .frame( height: 150, alignment: .center)
                                        .cornerRadius(4)
                                    
                                    VStack{
                                        Text("")
                                    }.frame(height: 120)
                                    
                                    VStack{
                                        Spacer()
                                        
                                        //                            Button(action:{
                                        //                                self.goToMap = true
                                        //                            }){
                                        //                            HStack{
                                        //
                                        //                                                            Text("Refine Location")
                                        //                                                                .foregroundColor(Color("ManBackColor"))
                                        //                                                                .font(.custom("Regular_Font".localized(), size: 14))
                                        //                                                                .padding(.leading,10)
                                        //                                                                .padding(.trailing,10)
                                        //
                                        //
                                        //
                                        //
                                        //                                                        }
                                        //                                                                                              .padding(.vertical,10.0)
                                        //                                                                                                  .background(Color.white)
                                        //                                                        .cornerRadius(4)
                                        //                            .border(Color("ManBackColor"))
                                        //                                                        .shadow(radius: 1)
                                        //
                                        //                            }
                                        
                                    }.frame( height: 150, alignment: .center)
                                    
                                }
                                
                                
                                HStack(alignment:.top){
                                    
                                    Image("placeholder").resizable().renderingMode(.template)
                                        .foregroundColor(Color(#colorLiteral(red: 0, green: 0.2509999871, blue: 0.4979999959, alpha: 1))).frame(width:25,height:25)
                                    VStack(alignment:.leading){
                                        Text(String((UserDefaults.standard.string(forKey: "PLACE")  ?? "")!)).font(.custom("ExtraBold_Font".localized(), size:
                                                                                                                            16)).foregroundColor(Color.black).lineLimit(1)
                                        
                                        Text(self.ADDRESS).font(.custom("Regular_Font".localized(), size:
                                                                            16)).foregroundColor(Color(#colorLiteral(red: 0.5059999824, green: 0.5099999905, blue: 0.5139999986, alpha: 1))).lineLimit(3).padding(.top,5)
                                        
                                        Text("Mobile" + ": " + LocNumbers(key: "+971"+self.MOBILE)).font(.custom("Regular_Font".localized(), size:
                                                                                                                    16)).foregroundColor(Color(#colorLiteral(red: 0.5059999824, green: 0.5099999905, blue: 0.5139999986, alpha: 1))).lineLimit(1)
                                    }.padding(.horizontal, 5)
                                    
                                    
                                    Button(action:{
                                        self.goToMap = true
                                    }){
                                        
                                        
                                        Text("Change")
                                            .foregroundColor(Color(#colorLiteral(red: 0.200000003, green: 0.7139999866, blue: 0.9060000181, alpha: 1)))
                                            .font(.custom("ExtraBold_Font".localized(), size: 16))
                                            .padding(.leading,10)
                                            .padding(.trailing,10)
                                        
                                        
                                        
                                        
                                        
                                        
                                    }
                                    
                                }.padding(20)
                                
                            }.background(Color.white).cornerRadius(10).clipped().shadow(radius: 5).padding()
                            
                            //            Spacer().frame(height:30)
                            
                            //            HStack{
                            //
                            //                Spacer().frame(width:20)
                            //                HStack{
                            //
                            //                    Image("basicbackimage")
                            ////                    AnimatedImage(url : URL (string: String(describing: self.serviceSnap.childSnapshot(forPath:"personal_information/image").value!)))
                            //                        .resizable()
                            //                        .padding(.all, 5)                                                           .frame(width: 120, height: 130)
                            //                        .overlay(HStack{
                            //                            Spacer()
                            //                            VStack{
                            //
                            //                                Text("Rating"
                            ////                                    String(describing: self.serviceSnap.childSnapshot(forPath:"personal_information/ratings").value!))  .font(.custom("ExtraBold_Font".localized(), size: 12)
                            //
                            //                                )
                            //                                    .foregroundColor(.white)
                            //                                    .frame(width: 25.0, height: 25.0)
                            //                                    .background(Color("morethanthree"))
                            //                                    .cornerRadius(4)
                            //                                    .padding([.top, .trailing], 5.0)
                            //                                Spacer()
                            //                            }
                            //
                            //                        })
                            //                        .cornerRadius(5)
                            //                        .padding(.trailing, 15.0)
                            //
                            //
                            //                    VStack(alignment: .leading){
                            //
                            //
                            //
                            //
                            //                        Text("Name"
                            ////                            String(describing: self.serviceSnap.childSnapshot(forPath:Localize(key:"personal_information/name")).value!)
                            //                        )
                            //                            .foregroundColor(Color("darkthemeletter"))
                            //                            .font(.custom("Bold_Font".localized(), size: 17))
                            //                            .padding(.top,20)
                            //
                            //
                            //                        Text("Desc"
                            ////                            String(describing: self.serviceSnap.childSnapshot(forPath:Localize(key:"personal_information/name")).value!)
                            //                        )
                            //                            .foregroundColor(Color("MainFontColor1"))
                            //                            .font(.custom("Regular_Font".localized(), size: 17))
                            //
                            //
                            //
                            //                        Divider()
                            //
                            //                        HStack{
                            //
                            //
                            //
                            //                            Image("destination")
                            //                                .frame(width: 25, height: 25)
                            //                                .background(Color("destbackgreen")).clipShape(Circle())
                            //
                            //                            Text("Time"
                            ////                                String(describing: self.serviceSnap.childSnapshot(forPath:"personal_information/times_rated").value!)
                            //                            )
                            //                                .font(.custom("Regular_Font".localized(), size: 17))
                            //                                .foregroundColor(Color("darkthemeletter"))
                            //
                            //                            Spacer()
                            //
                            //
                            //
                            //
                            //
                            //                        }
                            //                        Spacer()
                            //
                            //                    }
                            //
                            //                    .padding(.trailing,15)
                            //
                            //
                            //
                            //                }
                            //
                            //                .background(Color.white)
                            //                .cornerRadius(5)
                            //                .shadow(radius: 2)
                            //
                            //                Spacer().frame(width:20)
                            //
                            //                }.frame(height: 0).hidden()
                            
                            
                            HStack{
                                
                                Image("movingvan").resizable().frame(width:30,height: 25)
                                    .padding(.horizontal)
                                
                                
                                
                                Text(getTime(timeIntervel: self.selectedServiceProvider.TimeToArrive).localized()).font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color(#colorLiteral(red: 0.5059999824, green: 0.5099999905, blue: 0.5139999986, alpha: 1)))
                                Spacer()
                                
                                
                                
                            }.frame(height:60 ).background(Color.white).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color(#colorLiteral(red: 0.6859999895, green: 0.6859999895, blue: 0.6859999895, alpha: 1)),lineWidth: 1)).padding(.horizontal,15)
                            
                            
                            Group{
                                
                                
                                HStack{
                                    Text("Discount Voucher").font(.custom("ExtraBold_Font".localized(), size: 20)).foregroundColor(Color(#colorLiteral(red: 0.3019999862, green: 0.5799999833, blue: 0.8000000119, alpha: 1)))
                                    Spacer()
                                }.padding(.top,17).padding(.bottom,13).padding(.horizontal,15)
                                HStack{
                                    //                    Spacer().frame(width: geometry.size.width*0.05)
                                    HStack{
                                        
                                        TextField("Code", text:self.$VoucherCode )
                                            .foregroundColor(Color(red: 0.38, green: 0.51, blue: 0.64, opacity: 1.0))
                                            .font(.custom("Regular_Font".localized(), size: 17))
                                            .textContentType(.oneTimeCode)
                                            //  .shadow(radius:5)
                                            .background(Color("backcolor2"))
                                            .frame(height:30)
                                            .padding()
                                            .disabled(self.VoucherApplied)
                                        //    Spacer()
                                        
                                        // .padding(.trailing, 15.0)
                                        
                                        
                                        
                                        
                                        
                                    }  .frame(height:30)
                                    .padding(.vertical,10.0)
                                    .background(Color("backcolor2"))
                                    
                                    .overlay(  RoundedRectangle(cornerRadius: 10)
                                                .stroke(Color("ManBackColor"), lineWidth: 1))
                                    .overlay(
                                        
                                        HStack{
                                            
                                            Spacer()
                                            //                            Button(action:{
                                            //
                                            //                                let pasteboard = UIPasteboard.general
                                            //                                                                              if let string = pasteboard.string {
                                            //                                                                                  // text was found and placed in the "string" constant
                                            //                                                                                  self.VoucherCode = string
                                            //                                                                              }
                                            //
                                            //                            }){
                                            //                                Text("Paste").foregroundColor(Color.black).padding(.vertical).padding(.leading)
                                            //
                                            //                            }
                                            //
                                            //                             Text("|")
                                            
                                            Button(action:{
                                                
                                                self.ViewCodes.toggle()
                                            }){
                                                Text("Get").foregroundColor(Color.black).padding(.vertical).padding(.trailing)
                                                
                                            }.actionSheet(isPresented: self.$ViewCodes) {
                                                
                                                
                                                
                                                ActionSheet(title: Text("Voucher Code"), message: Text("Select From Here"), buttons:self.buttonArray)
                                                
                                                
                                                
                                                
                                                
                                            }
                                        }
                                    )
                                    
                                    Button(action:{
                                        
                                        //                      self.SOffers = self.findOffers.Offers.filter {
                                        //                                     $0.ServiceProviderID.contains(self.cartFetch.selectedServiceProvider.ServiceProvider_id)
                                        //                                 }
                                        print(self.findOffers.Offers)
                                        
                                        var Offer = self.findOffers.Offers.filter {
                                            $0.OfferCode.contains(self.VoucherCode) &&  $0.ServiceProviderID.contains(self.cartFetch.selectedServiceProvider.ServiceProvider_id)
                                        }
                                        
                                        if(Offer.count == 0){
                                            self.ErrorStatus = true
                                            self.ErrorComments = "offer not Valid for this service"
                                            
                                        }else{
                                            
                                            var O = Offer[0]
                                            print(O)
                                            self.ErrorStatus = true
                                            self.ErrorComments = "Voucher Code Applied Successfully"
                                            
                                            let Percent = (O.Percentage as NSString).doubleValue
                                            
                                            self.Discount = self.getSubTotal()*Percent/100
                                            
                                            self.VoucherApplied = true
                                        }
                                        
                                        //                        self.ref.child("service_providers").child(self.cartFetch.selectedServiceProvider.ServiceProvider_id).child("offers").child(self.VoucherCode).observeSingleEvent(of: DataEventType.value) { (datasnapshot) in
                                        //
                                        //                            let D = datasnapshot
                                        //
                                        //
                                        //                            if(D.exists()){
                                        //                                   self.ErrorStatus = true
                                        //                                self.ErrorComments = "Voucher Code Applied Successfully"
                                        //
                                        //                                let Percent = D.childSnapshot(forPath:"percentage").value as! Double
                                        //
                                        //                                self.Discount = self.getSubTotal()*Percent/100
                                        //
                                        //                                self.VoucherApplied = true
                                        //
                                        //                            }else{
                                        //
                                        //                                self.ErrorStatus = true
                                        //                                self.ErrorComments = "offer not Valid for this service"
                                        //
                                        //                            }
                                        //
                                        //                        }
                                        
                                    }){
                                        Text("Apply").font(.custom("ExtraBold_Font".localized(), size: 16)).padding()
                                    }
                                    
                                    
                                    
                                }.padding(.horizontal,15)
                                
                                if(self.ErrorStatus){
                                    HStack{
                                        Text(self.ErrorComments)
                                            .foregroundColor(.red) .font(.custom("Regular_Font".localized(), size: 17))
                                    }.padding(10)
                                    Spacer()
                                    
                                }
                                
                                HStack{
                                    Text("Pay with").font(.custom("ExtraBold_Font".localized(), size: 20)).foregroundColor(Color(#colorLiteral(red: 0.3019999862, green: 0.5799999833, blue: 0.8000000119, alpha: 1)))
                                    Spacer()
                                }.padding(.top,17).padding(.horizontal,15)
                                //                    .sheet(isPresented: self.$ShowApay) {
                                //
                                //                        ApplePayController( isPresenting: self.$ShowApay, items: [PKPaymentSummaryItem(label: "Twaddan service Booking \(self.N)", amount: NSDecimalNumber(value: self.getSubTotal()))], paymentStatus: self.paymentStatus)
                                //
                                //                }
                                
                                VStack{
                                    
                                    
                                    
                                    HStack{
                                        
                                        Image(systemName: "circle").resizable().frame(width:21,height: 21).overlay(Image(systemName: self.ApplePay ? "circle.fill":"none").resizable().frame(width:12,height: 12).foregroundColor(Color(#colorLiteral(red: 0.200000003, green: 0.7139999866, blue: 0.9060000181, alpha: 1))))
                                            .padding(.horizontal)
                                        
                                        Image("applepay2").resizable().frame(width:30,height: 28).padding(.horizontal)
                                        
                                        Text("Apple pay").font(.custom("Bold_Font".localized(), size: 16)).foregroundColor(Color.black)
                                        Spacer()
                                        
                                        
                                        
                                    }.frame(height:60 ).background(Color.white).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color(#colorLiteral(red: 0.6859999895, green: 0.6859999895, blue: 0.6859999895, alpha: 1)),lineWidth: 1))
                                    
                                    
                                    
                                    .onTapGesture {
                                        //                        self.AllButtonFalse()
                                        //                        self.ApplePay=true
                                        //                        self.PaymentType = 3
                                        self.ErrorComments = "This payment method will be availabe shortly.";
                                        self.ErrorStatus = true
                                        
                                    }
                                    
                                    
                                    
                                    HStack{
                                        
                                        Image(systemName: "circle").resizable().frame(width:21,height: 21).overlay(Image(systemName: self.CreditCard ? "circle.fill":"none").resizable().frame(width:12,height: 12).foregroundColor(Color(#colorLiteral(red: 0.200000003, green: 0.7139999866, blue: 0.9060000181, alpha: 1))))
                                            .padding(.horizontal)
                                        
                                        Image("Image 21").resizable().frame(width:28,height: 28).padding(.horizontal)
                                        
                                        Text("Credit Card or Debit Card").font(.custom("Bold_Font".localized(), size: 16)).foregroundColor(Color.black)
                                        Spacer()
                                        
                                        
                                        
                                    }.frame(height:60 ).background(Color.white).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color(#colorLiteral(red: 0.6859999895, green: 0.6859999895, blue: 0.6859999895, alpha: 1)
                                                                                                                                        
                                    ),lineWidth: 1)).onTapGesture {
                                        self.ErrorComments = "This payment method will be availabe shortly";
                                        self.ErrorStatus = true
                                        
                                        //                                          self.AllButtonFalse()
                                        //                                          self.CreditCard=true
                                        //                                         self.PaymentType = 2
                                    }
                                    //                    HStack{
                                    //
                                    //                                          Image(systemName: self.NetBanking ? "checkmark.circle.fill": "circle")  .padding(.horizontal)
                                    //
                                    //                                         Text("Net Banking").font(.custom("Regular_Font".localized(), size: 14))
                                    //                         Spacer()
                                    //
                                    //
                                    //
                                    //                                      }.onTapGesture {
                                    //                                          self.AllButtonFalse()
                                    //                                          self.NetBanking=true
                                    //                                         self.PaymentType = 1
                                    //                                      }
                                    
                                    HStack{
                                        
                                        Image(systemName: "circle").resizable().frame(width:21,height: 21).overlay(Image(systemName: self.CashOnDelivery ? "circle.fill":"none").resizable().frame(width:12,height: 12).foregroundColor(Color(#colorLiteral(red: 0.200000003, green: 0.7139999866, blue: 0.9060000181, alpha: 1))))
                                            .padding(.horizontal)
                                        
                                        Image("Image 23").resizable().frame(width:28,height: 28).padding(.horizontal)
                                        
                                        Text("Cash").font(.custom("Bold_Font".localized(), size: 16)).foregroundColor(Color.black)
                                        Spacer()
                                        
                                        
                                        
                                    }.frame(height:60 ).background(Color.white).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color(#colorLiteral(red: 0.6859999895, green: 0.6859999895, blue: 0.6859999895, alpha: 1)),lineWidth: 1)).onTapGesture {
                                        self.AllButtonFalse()
                                        self.CashOnDelivery=true
                                        self.PaymentType = 0
                                    }
                                    
                                    
                                    
                                }.padding(15)
                                
                                
                            }
                            
                            
                            
                            
                            
                            VStack{
                                HStack{
                                    Text("Payment Summary").font(.custom("ExtraBold_Font".localized(), size: 20)).foregroundColor(Color(#colorLiteral(red: 0.3019999862, green: 0.5799999833, blue: 0.8000000119, alpha: 1)))
                                    Spacer()
                                }.padding(.top,17).padding(.bottom,13).padding(.horizontal,15)
                                
                                
                                HStack{
                                    
                                    Text("Total").font(.custom("Regular_Font".localized(), size: 16))
                                    Spacer()
                                    Text(String()).font(.custom("Regular_Font".localized(), size: 16))
                                    Text("\(LocNumbers(key:  String(format: "%.1f",self.getSubTotal())))  "+" AED".localized()).font(.custom("Regular_Font".localized(), size: 16))
                                    
                                }.padding(.horizontal)
                                .padding(.top,20)
                                .padding(.bottom,10)
                                
                                
                                //                          Divider()
                                
                                //                                           HStack{
                                //
                                //                                                                   Text("Service Charge").font(.custom("Regular_Font".localized(), size: 16))
                                //                                                                   Spacer()
                                //                                                                   Text(LocNumbers(key:String( self.ServiceCharge))).font(.custom("Regular_Font".localized(), size: 16))
                                //                                                                   Text(" AED").font(.custom("Regular_Font".localized(), size: 16))
                                //
                                //                                           }.padding(.horizontal)
                                //                                                   .padding(.bottom,10)
                                //                                    Divider()
                                
                                HStack{
                                    
                                    Text("Discount").font(.custom("Regular_Font".localized(), size: 16))
                                    Spacer()
                                    Text(LocNumbers(key:String(self.Discount))).font(.custom("Regular_Font".localized(), size: 16))
                                    Text(" AED").font(.custom("Regular_Font".localized(), size: 16))
                                    
                                }.padding(.horizontal)
                                
                                
                                Divider().padding(.horizontal,30)
                                
                                
                                HStack{
                                    
                                    Text("Sub Total").font(.custom("Bold_Font".localized(), size: 16))
                                    Spacer()
                                    Text(LocNumbers(key:String(format: "%.1f",self.getSubTotal()+self.ServiceCharge-self.Discount))).font(.custom("Bold_Font".localized(), size: 16))
                                    Text(" AED").font(.custom("Bold_Font".localized(), size: 16))
                                    
                                }.padding(.horizontal)
                                // .padding(.vertical,5)
                                //   .padding(.vertical,15) .background(Color.white)
                                .padding(.bottom,20)
                                
                                
                                
                            }
                        }
                        
                        
                        VStack{
                            // Spacer()
                            
                            VStack{
                                
                                
                                
                                
                                Button(action:{
                                    
                                    
                                    if(self.Connected){
                                        self.ViewLoader = true
                                        
                                        
                                        if(self.PaymentType == 0
                                            && self.storedPhone != self.MOBILE && UserDefaults.standard.string(forKey: "LAST_USED_PHONE") ?? "" != self.MOBILE )
                                        {
                                            
                                            self.ALERT1 = true
                                            self.BLUR = true
                                            self.ViewLoader = false
                                            
                                            
                                        }else{
                                            
                                            //  print("SUCCESS")
                                            
                                            var NumberOfServices : Int = 0
                                            // var ServiceProviders : Int = 0
                                            var total_time = 0
                                            for cart in self.carts{
                                                
                                                NumberOfServices += cart.services.count
                                                
                                                
                                                
                                                for Service in cart.services {
                                                    let S  = (Service.ServiceTime as NSString).doubleValue
                                                    
                                                    total_time = total_time + Int(S)
                                                    
                                                }
                                                
                                                // total_time = total_time/60
                                                //    total_time = total_time+1
                                                
                                            }
                                            
                                            var pMode =  0
                                            if(self.PaymentType > 0){
                                                pMode = 1
                                            }
                                            
                                            
                                            let node = String((Date().timeIntervalSince1970)).prefix(14).replacingOccurrences(of: ".", with: "") + " & " + UserDefaults.standard.string(forKey: "Aid")!
                                            
                                            
                                            print("QQQQQQQQ",self.findOffers.VehicleDriverLink)
                                            print("QQQQQQQQ",self.selectedServiceProvider.DriverID)
                                            let O = self.findOffers.VehicleDriverLink.filter {
                                                $0.DriverID.contains(self.selectedServiceProvider.DriverID)
                                            }
                                            var VNumber = ""
                                            if(O.count>0){
                                                VNumber = O[0].VehicleNumber
                                            }
                                            
                                            let Order : [String : Any] = [
                                                
                                                
                                                "customer_gmail_id" : self.settings.Email,
                                                "customer_id" : UserDefaults.standard.string(forKey: "Aid") ?? "0",
                                                "customer_latitude": Double(UserDefaults.standard.double(forKey: "latitude")),
                                                "customer_longitude" :Double(UserDefaults.standard.double(forKey: "longitude")),
                                                "customer_mobile_number":UInt64("971\(self.MOBILE)") ?? 971123456789,
                                                "customer_name" : self.NAME+" "+self.LNAME,
                                                "customer_raw_address": self.ADDRESS,
                                                //                                                                                                                           "driver_id" :  self.selectedServiceProvider.DriverID,
                                                "number_of_services" :NumberOfServices,
                                                "order_id_number":node,
                                                "payment_mode" : pMode,
                                                "rating": 0,
                                                "review" :"",
                                                "sp_id":self.selectedServiceProvider.ServiceProvider_id,
                                                "sp_name":self.selectedServiceProvider.ServiceProvider,
                                                "status":0,
                                                "time_order_placed" : [".sv":"timestamp"],
                                                "total_discount":self.Discount,
                                                "total_due": self.getSubTotal()+self.ServiceCharge-self.Discount,
                                                "total_number_of_vehicles": self.carts.count,
                                                "total_price_of_order" :self.getSubTotal()+self.ServiceCharge-self.Discount,
                                                "total_time":total_time,
                                                "order_place_from":"iOS",
                                                "twaddanId" :String((Date().timeIntervalSince1970)).replacingOccurrences(of: ".", with: ""),
                                                "vehicle_number":   VNumber,
                                                "eta":(self.selectedServiceProvider.TimeToArrive*1000)+Date().timeIntervalSince1970*1000,
                                                
                                            ]
                                            
                                            //
                                            //                                                let Order : [String : Any] = [
                                            //                                                    "customer_gmail_id" : self.settings.Email,
                                            //                                                    "customer_id" : UserDefaults.standard.string(forKey: "Aid") ?? "0",
                                            //                                                    "customer_latitude": Double(UserDefaults.standard.double(forKey: "latitude")),
                                            //                                                    "customer_longitude" :Double(UserDefaults.standard.double(forKey: "longitude")),
                                            //                                                    "customer_mobile_number":UInt64("91\(self.MOBILE)") ?? "91000000000",
                                            //                                                    "customer_name" : self.NAME+self.LNAME,
                                            //                                                    "customer_raw_address": self.ADDRESS,
                                            //                                                    "driver_id" : self.cartFetch.selectedServiceProvider.DriverID,
                                            //                                                    "number_of_services" :NumberOfServices,
                                            //                                                    "order_id_number":node,
                                            //                                                    "payment_mode" : self.PaymentType,
                                            //                                                    "rating": 0,
                                            //                                                    "review" :"",
                                            //                                                    "sp_id":self.cartFetch.selectedServiceProvider.ServiceProvider_id,
                                            //                                                    "sp_name":self.cartFetch.selectedServiceProvider.ServiceProvider,
                                            //
                                            //                                                    "time_order_placed" : [".sv":"timestamp"],
                                            //                                                    "total_discount":self.Discount,
                                            //                                                    "tatal_due": self.getSubTotal()+self.ServiceCharge-self.Discount,
                                            //                                                    "total_number_of_vehicles": self.cartFetch.carts.count,
                                            //                                                    "total_price_of_order" :self.getSubTotal()+self.ServiceCharge-self.Discount,
                                            //                                                    "total_time":total_time,
                                            //                                                     "order_place_from":"iOS",
                                            //
                                            //
                                            //                                                ]
                                            
                                            
                                            if(self.PaymentType == 3){
                                                
                                                //                                                                            self.ShowApay = true
                                                self.applePaymentHandler.startPayment(label:"Twaddan Booking Payment : OrderID: \(node)",amount: NSDecimalNumber(value: self.getSubTotal()+self.ServiceCharge-self.Discount)) { (success) in
                                                    if success {
                                                        print("Success")
                                                        EnterOrderToDB(Node : node,Order: Order,VehicleList:self.VehicleList)
                                                        
                                                        self.ViewLoader = true
                                                        
                                                        self.ref.child("orders").child("all_orders").child(node).child("driver_id").observe(DataEventType.value) { (dataSnapshot) in
                                                            if(dataSnapshot.exists()){
                                                                self.OrderID = node
                                                                
                                                                self.BLUR = true
                                                                self.ALERT3 = true
                                                                self.ViewLoader = false
                                                            }
                                                            
                                                        }
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                    } else {
                                                        print("Failed")
                                                        self.ViewLoader = false
                                                    }
                                                }
                                                
                                                
                                                
                                                
                                                
                                            }else if(self.PaymentType == 2){
                                                self.ViewLoader = true
                                                
                                                
                                                self.k  = [Product(name: node, amount: Int((self.getSubTotal()+self.ServiceCharge - self.Discount)*100))]
                                                
                                                self.OrderID = node
                                                self.OR = Order
                                                self.showPaymentPortal = true
                                                
                                                //                                                                            PaymentPage()
                                                //                                                                            let k : [Product] = [Product(name: "Test", amount: 100)]
                                                //
                                                ////                                                                            self.paymentHandler.startPayment()
                                                //                                                                            self.paymentHandler.createOrder(paymentAmount: 100, using: .Card, with: k) { (success) in
                                                //                                                                                                                                                                   if success {
                                                //                                                                                                                                                                       print("Success")
                                                //                                                                                                                                                                       self.EnterOrderToDB(Node : node,Order: Order)
                                                //
                                                //                                                                                                                                                                       self.BLUR = true
                                                //                                                                                                                                                                       self.ALERT3 = true
                                                //                                                                                                                                                                       self.ViewLoader = false
                                                //
                                                //                                                                                                                                                                   } else {
                                                //                                                                                                                                                                       print("Failed")
                                                //                                                                                                                                                                        self.ViewLoader = false
                                                //                                                                                                                                                                   }
                                                //                                                                                                                                                               }
                                                //
                                                //
                                                
                                            }else{
                                                
                                                
                                                let connectedRef = Database.database().reference(withPath: ".info/connected")
                                                connectedRef.observeSingleEvent(of: DataEventType.value, with: { snapshot in
                                                    if snapshot.value as? Bool ?? false {
                                                        print("oooooooo","Connected")
                                                        
                                                        EnterOrderToDB(Node : node,Order: Order,VehicleList:self.VehicleList)
                                                        self.ViewLoader = true
                                                        
                                                        
                                                        
                                                        self.OrderID = node
                                                        
                                                        self.BLUR = true
                                                        self.ALERT3 = true
                                                        self.ViewLoader = false
                                                        
                                                        
                                                        
                                                        self.ref.child("orders").child("all_orders").child(node).child("driver_id").observe(DataEventType.value) { (dataSnapshot) in
                                                            if(dataSnapshot.exists()){
                                                                
                                                            }
                                                            
                                                        }
                                                        
                                                        
                                                        
                                                        
                                                        
                                                    } else {
                                                        self.Connected = false
                                                        self.ErrorStatus = true
                                                        self.ErrorComments = "Check internet connection !"
                                                        print("oooooooo","Not connected")
                                                    }
                                                })
                                                
                                                
                                                //
                                                //                                                                        EnterOrderToDB(Node : node,Order: Order,VehicleList:self.VehicleList)
                                                //                                                                                                                       self.OrderID = node
                                                //
                                                //                                                                           self.ViewLoader = true
                                                //
                                                //                                                                        self.ref.child("orders").child("all_orders").child(node).child("driver_id").observe(DataEventType.value) { (dataSnapshot) in
                                                //                                                                            if(dataSnapshot.exists()){
                                                //                                                                                self.OrderID = node
                                                //
                                                //                                                                                                                                                                           self.BLUR = true
                                                //                                                                                                                                                                           self.ALERT3 = true
                                                //                                                                                                                                                                           self.ViewLoader = false
                                                //                                                                            }
                                                //
                                                //                                                                        }
                                                
                                            }
                                        }
                                        
                                        
                                        
                                        
                                    }else{
                                        self.ErrorStatus = true
                                        self.ErrorComments = "Check internet connection !"
                                    }
                                }){
                                    
                                    
                                    if(self.PaymentType == 3){
                                        
                                        ZStack{
                                            HStack{
                                                RoundedCorners(color: Color.black,tl: 50, tr: 0, bl: 0, br: 0)
                                            }
                                            .frame(height: 65)
                                            
                                        }.overlay(
                                            
                                            HStack{
                                                
                                                Image( "applepay").resizable().frame(width:76,height:32).foregroundColor(Color.white)
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                            }
                                            
                                        )
                                    }else{
                                        
                                        RoundedPanel()
                                            
                                            .frame(height:50)                            .overlay(
                                                
                                                HStack{
                                                    
                                                    Text(self.CashOnDelivery ? "Place carwash order" : "PAY").font(.custom("ExtraBold_Font".localized(), size: 18))
                                                        .foregroundColor(.white)
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                }
                                                
                                            )
                                    }
                                }
                                // Spacer()
                                
                                //.frame(maxWidth:.infinity)
                                
                                
                            }
                            
                            
                        }
                        
                        
                    }                        .blur(radius: (self.PStatus == "Failed" || self.PStatus == "Cancelled" || self.PStatus == "AuthFailed" ) || self.BLUR || self.displayETA
                                                    || self.failure ? 10 : 0)
                    .disabled((self.PStatus == "Failed" || self.PStatus == "Cancelled" || self.PStatus == "AuthFailed" ) || self.ALERT1||self.ALERT2 || self.displayETA
                                || self.failure)
                    
                    
                    
                    
                    if(self.ALERT1){
                        
                        ZStack{
                            
                            VStack{
                                
                                HStack{
                                    Text("Verify mobile number").font(.custom("Bold_Font".localized(), size: 16))
                                        .foregroundColor(Color("darkthemeletter"))
                                    Spacer()
                                    
                                }
                                HStack{
                                    
                                    Image(systemName: "phone")
                                        .foregroundColor(Color("darkthemeletter"))
                                    
                                    
                                    
                                    Text("+971"+self.MOBILE).font(.custom("Regular_Font".localized(), size: 16))    .foregroundColor(Color("darkthemeletter"))
                                    Spacer()
                                    Button(action: {
                                        
                                        self.ALERT1 = false
                                        self.BLUR = false
                                        self.presentationMode.wrappedValue.dismiss()
                                        
                                        
                                    }){
                                        
                                        Text("Change").font(.custom("Bold_Font".localized(), size: 14))    .foregroundColor(Color("darkthemeletter"))
                                    }
                                    
                                    
                                }
                                
                                HStack{
                                    Text("We will send you SMS with verfication code on this number above").font(.custom("Regular_Font".localized(), size: 14)).foregroundColor(Color(
                                                                                                                                                                                    "MainFontColor1"))
                                    
                                    
                                    Spacer()
                                    
                                }
                                HStack{
                                    
                                    Spacer()
                                    Button(action: {
                                        
                                        self.ALERT1 = false
                                        self.BLUR = false
                                        
                                        
                                    }){
                                        
                                        Text("Cancel").font(.custom("Regular_Font".localized(), size: 17)).foregroundColor(Color(
                                                                                                                            "MainFontColor1"))
                                    }
                                    
                                    Button(action: {
                                        
                                        
                                        self.ViewLoader = true
                                        
                                        self.VERCODE = String(format:"%06d", Int.random(in: 0 ... 999999))
                                        
                                        if(self.MOBILE.count == 9 || self.MOBILE.count == 10){
                                            
                                            let S : String =  "https://smartsmsgateway.com/api/api_http.php?username=twaddan&password=NaJPCB2iSy&senderid=TWADDAN&to=971\(self.MOBILE)&text=\(self.VERCODE)%20is%20your%20verification%20code&type=text&datetime=2020-08-20%2008%3A01%3A26"
                                            
                                            let url = URL(string:S
                                            )
                                            //        else {
                                            //        let error = NSError(domain: "LocalDomain", code: 0, userInfo: [NSLocalizedDescriptionKey: "Failed to create object URL"])
                                            //        print("Error: \(error)")
                                            //        return
                                            //    }
                                            let config = URLSessionConfiguration.default
                                            let session = URLSession(configuration: config)
                                            
                                            
                                            let task = session.dataTask(with: url!, completionHandler:{_,_,_  in
                                                
                                                self.ALERT1 = false
                                                self.showAlert = false
                                                self.ALERT2 = true
                                                self.ViewLoader = false
                                                
                                                
                                            } )
                                            task.resume()
                                            
                                            
                                            //                                                                                                                                           self.verificationID = VerificationID
                                            
                                            
                                            
                                        }else{
                                            self.showAlert = true
                                            self.ViewLoader = false
                                            self.errorStatus = "Invalid Phone Number"
                                        }
                                        
                                        //                                        Auth.auth().settings?.isAppVerificationDisabledForTesting = true
                                        //                                        PhoneAuthProvider.provider().verifyPhoneNumber("+91\(self.MOBILE)", uiDelegate: nil) { (VerificationID, error) in
                                        //
                                        //                                            if(error != nil){
                                        //
                                        //                                                let  e : Error = error!
                                        //
                                        //                                                let errorAuthStatus = AuthErrorCode.init(rawValue: e._code)!
                                        //
                                        //                                                self.showAlert = true
                                        //
                                        //                                                     self.ViewLoader = false
                                        //
                                        //                                                switch errorAuthStatus {
                                        //
                                        //
                                        //                                                case .invalidPhoneNumber:
                                        //                                                    self.errorStatus = "Invalid Phone Number"
                                        //
                                        //                                                    print("Invalid Phone Number")
                                        //
                                        //                                                case .invalidVerificationCode:
                                        //                                                    self.errorStatus = "Invalid Verification Code"
                                        //                                                    print("Invalid Verification Code")
                                        //                                                case .operationNotAllowed:
                                        //                                                    self.errorStatus = "operationNotAllowed"
                                        //                                                    print("operationNotAllowed")
                                        //                                                case .userDisabled:
                                        //                                                    self.errorStatus = "userDisabled"
                                        //                                                    print("userDisabled")
                                        //                                                case .userNotFound:
                                        //                                                    self.errorStatus = "userNotFound"
                                        //                                                    print("userNotFound")
                                        //                                                //            self.register(auth: Auth.auth())
                                        //                                                case .tooManyRequests:
                                        //                                                    self.errorStatus = "tooManyRequests, oooops"
                                        //                                                    print("tooManyRequests, oooops")
                                        //                                                default: print(e)
                                        //                                                }
                                        //
                                        //
                                        //
                                        //
                                        //                                                return
                                        //                                            }
                                        //                                            else{
                                        //
                                        //                                                self.verificationID = VerificationID
                                        //                                                self.ALERT1 = false
                                        //                                                self.ALERT2 = true
                                        //                                                     self.ViewLoader = false
                                        //                                            }
                                        //                                        }
                                        
                                        
                                        
                                        
                                        
                                    }){
                                        
                                        Text("GetCode").font(.custom("ExtraBold_Font".localized(), size: 17))
                                    }
                                    
                                }
                                
                                if(self.showAlert){
                                    
                                    
                                    Text(self.errorStatus).font(.custom("Regular_Font".localized(), size: 14)).foregroundColor(Color.red).padding()
                                    
                                }
                            }.frame(width :320 , height: 150)
                            
                            .padding()
                            
                        } .background(Color.white)
                        
                        .cornerRadius(5)
                    }
                    
                    
                    //ALERT 2
                    
                    if(self.ALERT2){
                        ZStack{
                            VStack{
                                
                                HStack{
                                    Text("Verify mobile number").font(.custom("Bold_Font".localized(), size: 16))
                                        .foregroundColor(Color("darkthemeletter"))
                                    Spacer()
                                    
                                }
                                HStack{
                                    
                                    Image(systemName: "phone")
                                        .foregroundColor(Color("darkthemeletter"))
                                    
                                    Text(self.MOBILE).font(.custom("Regular_Font".localized(), size: 16))    .foregroundColor(Color("darkthemeletter"))
                                    Spacer()
                                    Button(action: {
                                        
                                        self.ALERT2 = false
                                        self.BLUR = false
                                        self.presentationMode.wrappedValue.dismiss()
                                        
                                        
                                    }){
                                        
                                        Text("Change").font(.custom("Bold_Font".localized(), size: 14))    .foregroundColor(Color("darkthemeletter"))
                                    }
                                    
                                    
                                }
                                
                                
                                
                                HStack{
                                    Text("Please enter the code sent to your number").font(.custom("Regular_Font".localized(), size: 14)).foregroundColor(Color(
                                                                                                                                                            "MainFontColor1"))
                                    
                                    
                                    Spacer()
                                    
                                }
                                
                                
                                TextField("CODE",text: self.$VCode )
                                {
                                    
                                    
                                }.frame(height : 60)   .multilineTextAlignment(.center)
                                .font(.custom("ExtraBold_Font".localized(), size: 30))  .keyboardType(.numberPad)
                                Divider()
                                
                                //
                                if(self.showAlert){
                                    
                                    
                                    Text(self.errorStatus).font(.custom("Regular_Font".localized(), size: 14)).foregroundColor(Color.red).padding()
                                    
                                }
                                
                                
                                
                                Button(action:{
                                    self.ViewLoader = true
                                    
                                    if(self.VERCODE == self.VCode ){
                                        self.showAlert = false
                                        
                                        print("Success2")
                                        //                                                    self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("personal_details/phoneNumber").setValue(self.MOBILE)
                                        // User re-authenticated.
                                        
                                        UserDefaults.standard.set(self.MOBILE, forKey: "LAST_USED_PHONE")
                                        self.ALERT2 = false
                                        self.ALERT1 = false
                                        self.BLUR = false
                                        
                                        
                                        
                                        var total_time : Int = 0
                                        
                                        
                                        
                                        //  print("SUCCESS")
                                        
                                        var NumberOfServices : Int = 0
                                        // var ServiceProviders : Int = 0
                                        
                                        for cart in self.carts{
                                            
                                            
                                            
                                            
                                            
                                            
                                            for Service in cart.services {
                                                let S  = (Service.ServiceTime as NSString).doubleValue
                                                NumberOfServices += cart.services.count
                                                
                                                total_time = total_time + Int(S)
                                                
                                            }
                                            
                                            // total_time = total_time/60
                                            //  total_time = total_time+1
                                            
                                            for addons in cart.addons {
                                                
                                                
                                                
                                            }
                                            
                                            
                                            
                                        }
                                        
                                        
                                        
                                        
                                        var pMode =  0
                                        if(self.PaymentType > 0){
                                            pMode = 1
                                        }
                                        
                                        
                                        
                                        
                                        let node = String((Date().timeIntervalSince1970)).prefix(14).replacingOccurrences(of: ".", with: "") + " & " + UserDefaults.standard.string(forKey: "Aid")!
                                        
                                        
                                        
                                        
                                        let O = self.findOffers.VehicleDriverLink.filter {
                                            $0.DriverID.contains(self.selectedServiceProvider.DriverID)
                                        }
                                        var VNumber = ""
                                        if(O.count>0){
                                            VNumber = O[0].VehicleNumber
                                        }
                                        
                                        
                                        
                                        print("QQQQQQQQ",self.findOffers.VehicleDriverLink)
                                        print("QQQQQQQQ",self.selectedServiceProvider.DriverID)
                                        
                                        let Order : [String : Any] = [
                                            
                                            
                                            "customer_gmail_id" : self.settings.Email,
                                            "customer_id" : UserDefaults.standard.string(forKey: "Aid") ?? "0",
                                            "customer_latitude": Double(UserDefaults.standard.double(forKey: "latitude")),
                                            "customer_longitude" :Double(UserDefaults.standard.double(forKey: "longitude")),
                                            "customer_mobile_number":UInt64("971\(self.MOBILE)") ?? 971123456789,
                                            "customer_name" : self.NAME+" "+self.LNAME,
                                            "customer_raw_address": self.ADDRESS,
                                            //                                                        "driver_id" : self.selectedServiceProvider.DriverID,
                                            "number_of_services" :NumberOfServices,
                                            "order_id_number":node,
                                            "payment_mode" : pMode,
                                            "rating": 0,
                                            "review" :"",
                                            "sp_id":self.selectedServiceProvider.ServiceProvider_id,
                                            "sp_name":self.selectedServiceProvider.ServiceProvider,
                                            "status":0,
                                            "time_order_placed" : [".sv":"timestamp"],
                                            "total_discount":self.Discount,
                                            "total_due": self.getSubTotal()+self.ServiceCharge-self.Discount,
                                            "total_number_of_vehicles": self.carts.count,
                                            "total_price_of_order" :self.getSubTotal()+self.ServiceCharge-self.Discount,
                                            "total_time":total_time,
                                            "order_place_from":"iOS",
                                            "twaddanId" :String((Date().timeIntervalSince1970)).replacingOccurrences(of: ".", with: ""),
                                            "vehicle_number":   VNumber,
                                            "eta":(self.selectedServiceProvider.TimeToArrive*1000)+Date().timeIntervalSince1970*1000,
                                            
                                            
                                        ]
                                        
                                        
                                        
                                        let connectedRef = Database.database().reference(withPath: ".info/connected")
                                        connectedRef.observeSingleEvent(of: DataEventType.value, with: { snapshot in
                                            if snapshot.value as? Bool ?? false {
                                                print("oooooooo","Connected")
                                                
                                                EnterOrderToDB(Node : node,Order: Order,VehicleList:self.VehicleList)
                                                self.ViewLoader = true
                                                
                                                self.OrderID = node
                                                
                                                self.BLUR = true
                                                self.ALERT3 = true
                                                self.ViewLoader = false
                                                self.ref.child("orders").child("all_orders").child(node).child("driver_id").observe(DataEventType.value) { (dataSnapshot) in
                                                    if(dataSnapshot.exists()){
                                                        
                                                    }
                                                    
                                                }
                                                
                                                
                                                
                                                
                                                
                                            } else {
                                                self.Connected = false
                                                self.ErrorStatus = true
                                                self.ErrorComments = "Check internet connection !"
                                                print("oooooooo","Not connected")
                                            }
                                        })
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                    }else{
                                        self.showAlert = true
                                        self.errorStatus = "Invalid Phone Number"
                                    }
                                    
                                    
                                    
                                    //                                    if(self.verificationID != nil){
                                    //
                                    //                                        print("Verification Start\(self.verificationID)")
                                    //
                                    //                                        let credential = PhoneAuthProvider.provider().credential(withVerificationID: self.verificationID!, verificationCode: self.VCode)
                                    //
                                    //
                                    //
                                    //                                        Auth.auth().currentUser?.updatePhoneNumber(credential, completion: { (error) in
                                    //                                            if let error = error {
                                    //                                                // An error happened.
                                    //
                                    //
                                    //                                                let  e : Error = error
                                    //
                                    //                                                let errorAuthStatus = AuthErrorCode.init(rawValue: e._code)!
                                    //
                                    //                                                self.showAlert = true
                                    //
                                    //                                                switch errorAuthStatus {
                                    //
                                    //
                                    //                                                case .invalidPhoneNumber:
                                    //                                                    self.errorStatus = "Invalid Phone Number"
                                    //
                                    //                                                    print("Invalid Phone Number")
                                    //
                                    //                                                case .invalidVerificationCode:
                                    //                                                    self.errorStatus = "Invalid Verification Code"
                                    //                                                    print("Invalid Verification Code")
                                    //                                                case .operationNotAllowed:
                                    //                                                    self.errorStatus = "operationNotAllowed"
                                    //                                                    print("operationNotAllowed")
                                    //                                                case .userDisabled:
                                    //                                                    self.errorStatus = "userDisabled"
                                    //                                                    print("userDisabled")
                                    //                                                case .userNotFound:
                                    //                                                    self.errorStatus = "userNotFound"
                                    //                                                    print("userNotFound")
                                    //                                                //            self.register(auth: Auth.auth())
                                    //                                                case .tooManyRequests:
                                    //                                                    self.errorStatus = "tooManyRequests, oooops"
                                    //                                                    print("tooManyRequests, oooops")
                                    //
                                    //                                                    self.errorStatus = "tooManyRequests, oooops"
                                    //                                                    print("tooManyRequests, oooops")
                                    //                                                default: self.errorStatus = "Number Already Linked to other account"
                                    //                                                    print("Verification Failed")
                                    //                                                }
                                    //
                                    //
                                    //
                                    //                                                     self.ViewLoader = false
                                    //
                                    //                                                print(error)
                                    //                                            } else {
                                    //                                                print("Success2")
                                    //                                                self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("personal_details/phoneNumber").setValue(self.MOBILE)
                                    //                                                // User re-authenticated.
                                    //                                                self.ALERT2 = false
                                    //                                                self.ALERT1 = false
                                    //                                                self.BLUR = false
                                    //
                                    //
                                    //
                                    //                                                var total_time : Int = 0
                                    //
                                    //
                                    //
                                    //                                                //  print("SUCCESS")
                                    //
                                    //                                                var NumberOfServices : Int = 0
                                    //                                                // var ServiceProviders : Int = 0
                                    //
                                    //                                                for cart in self.carts{
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //                                                    for Service in cart.services {
                                    //                                                        let S  = (Service.ServiceTime as NSString).doubleValue
                                    //                                                         NumberOfServices += cart.services.count
                                    //
                                    //                                                        total_time = total_time + Int(S)
                                    //
                                    //                                                    }
                                    //
                                    //                                                   // total_time = total_time/60
                                    //                                                  //  total_time = total_time+1
                                    //
                                    //                                                    for addons in cart.addons {
                                    //
                                    //
                                    //
                                    //                                                    }
                                    //
                                    //
                                    //
                                    //                                                }
                                    //
                                    //
                                    //
                                    //
                                    //                                                var pMode =  0
                                    //                                                                                             if(self.PaymentType > 0){
                                    //                                                                                                 pMode = 1
                                    //                                                                                             }
                                    //
                                    //
                                    //
                                    //
                                    //                                                let node = String((Date().timeIntervalSince1970)).replacingOccurrences(of: ".", with: "") + " & " + UserDefaults.standard.string(forKey: "Aid")!
                                    //
                                    //
                                    //
                                    //
                                    //                                               let O = self.findOffers.VehicleDriverLink.filter {
                                    //                                                                                                                                                    $0.DriverID.contains(self.selectedServiceProvider.DriverID)
                                    //                                                                                                                      }
                                    //                                                                                                                      var VNumber = ""
                                    //                                                                                                                      if(O.count>0){
                                    //                                                                                                                          VNumber = O[0].VehicleNumber
                                    //                                                                                                                      }
                                    //
                                    //
                                    //
                                    //                                                print("QQQQQQQQ",self.findOffers.VehicleDriverLink)
                                    //                                                 print("QQQQQQQQ",self.selectedServiceProvider.DriverID)
                                    //
                                    //                                                let Order : [String : Any] = [
                                    //
                                    //
                                    //                                                    "customer_gmail_id" : self.settings.Email,
                                    //                                                    "customer_id" : UserDefaults.standard.string(forKey: "Aid") ?? "0",
                                    //                                                    "customer_latitude": Double(UserDefaults.standard.double(forKey: "latitude")),
                                    //                                                    "customer_longitude" :Double(UserDefaults.standard.double(forKey: "longitude")),
                                    //                                                   "customer_mobile_number":UInt64("91\(self.MOBILE)") ?? "91000000000",
                                    //                                                    "customer_name" : self.NAME+self.LNAME,
                                    //                                                    "customer_raw_address": self.ADDRESS,
                                    //                                                    "driver_id" : self.selectedServiceProvider.DriverID,
                                    //                                                    "number_of_services" :NumberOfServices,
                                    //                                                    "order_id_number":node,
                                    //                                                    "payment_mode" : pMode,
                                    //                                                    "rating": 0,
                                    //                                                    "review" :"",
                                    //                                                    "sp_id":self.selectedServiceProvider.ServiceProvider_id,
                                    //                                                    "sp_name":self.selectedServiceProvider.ServiceProvider,
                                    //                                                    "status":0,
                                    //                                                    "time_order_placed" : [".sv":"timestamp"],
                                    //                                                    "total_discount":self.Discount,
                                    //                                                    "total_due": self.getSubTotal()+self.ServiceCharge-self.Discount,
                                    //                                                    "total_number_of_vehicles": self.carts.count,
                                    //                                                    "total_price_of_order" :self.getSubTotal()+self.ServiceCharge-self.Discount,
                                    //                                                    "total_time":total_time,
                                    //                                                    "order_place_from":"iOS",
                                    //                                                    "twaddanId" :String((Date().timeIntervalSince1970)).replacingOccurrences(of: ".", with: ""),
                                    //                                                    "vehicle_number":   VNumber,
                                    //                                                                                                                                                                              "eta":self.selectedServiceProvider.TimeToArrive,
                                    //
                                    //
                                    //                                                ]
                                    //
                                    //
                                    //
                                    //
                                    //                                              EnterOrderToDB(Node : node,Order: Order,VehicleList:self.VehicleList)
                                    //                                                self.OrderID = node
                                    //                                                self.BLUR = true
                                    //                                                self.ALERT3 = true
                                    //                                                     self.ViewLoader = false
                                    //
                                    //
                                    //
                                    //
                                    //                                            }
                                    //                                        })
                                    //                                        //
                                    //                                        //                                            Auth.auth().currentUser?.link(with: credential, completion: { (authResult, error) in
                                    //                                        //
                                    //                                        //                                                if(error != nil){
                                    //                                        //                                                    print(error!)
                                    //                                        //
                                    //                                        //
                                    //                                        ////                                                     Auth.auth().currentUser?.reauthenticate(with: credential) { error in
                                    //                                        ////                                                     if let error = error {
                                    //                                        ////                                                        // An error happened.
                                    //                                        ////                                                      } else {
                                    //                                        ////
                                    //                                        ////                                                        // User re-authenticated.
                                    //                                        ////                                                      }
                                    //                                        ////                                                    }
                                    //                                        //                                                return
                                    //                                        //                                                }else{
                                    //                                        //                                                    print("Success")
                                    //                                        //                                                    self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("personal_information/phoneNumber").setValue(self.MOBILE)
                                    //                                        //
                                    //                                        //                                                }
                                    //                                        //                                            })
                                    //
                                    //
                                    //
                                    //                                    }else{
                                    //
                                    //                                        print("Verification id is nil")
                                    //                                    }
                                    
                                    
                                    
                                    
                                }){
                                    
                                    
                                    
                                    
                                    HStack{
                                        
                                        
                                        
                                        Spacer()
                                        
                                        Text("Verify")   .font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color.white)
                                        Spacer()
                                        
                                        
                                        
                                    }.padding().frame(width: 200)
                                    //                            .border(Color("ManualSignInGray"), width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
                                    
                                    //                                                               .frame(width: 300.0, height: 50)      .background(Color("su1"))
                                    //                                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color("su1"),lineWidth: 1))
                                    .background(Color("su1"))
                                    .cornerRadius(10)
                                    
                                    
                                    //
                                    //
                                    //                                    RoundedPanel()
                                    //
                                    //                                        .overlay(Text("Verify")
                                    //                                            .font(.custom("ExtraBold_Font".localized(), size: 20))
                                    //
                                    //                                            .foregroundColor(Color.white))
                                    //                                        .background(Color(red: 0.24, green: 0.56, blue: 0.87, opacity: 1.0))
                                    //                                        .cornerRadius(5)
                                    //                                    }   .frame(width: 200).padding()
                                    //
                                    //
                                    
                                }
                                HStack{
                                    
                                    Spacer()
                                    //
                                    
                                    Button(action: {
                                        
                                        self.ALERT2 = false
                                        self.ALERT1 = false
                                        self.BLUR = false
                                    }){
                                        
                                        Text("Cancel").font(.custom("ExtraBold_Font".localized(), size: 17))
                                    }.padding()
                                    
                                }
                            }.padding(20)
                            
                            
                        }.background(Color.white).clipped().shadow(radius: 1)
                        .padding(40)
                        .cornerRadius(5)
                        
                    }
                    
                    
                    
                    if(self.ALERT3){
                        
                        ZStack{
                            
                            VStack{
                                
                                Text("Thank You")   .font(.custom("Bold_Font".localized(), size: 0))
                                    .foregroundColor(Color.white).padding(.horizontal).padding(.top,30)
                                
                                Image("orderplaced").resizable().frame(width: 0 , height : 150)
                                    .clipShape(Circle())
                                
                                Text("Thank you for booking . Hope you loved the service").multilineTextAlignment(.center)
                                    .font(.custom("Regular_Font".localized(), size: 0))
                                    .foregroundColor(Color.white).padding()
                                
                                
                                
                            }
                            .overlay(
                                HStack{
                                    Spacer()
                                    VStack{
                                        
                                        NavigationLink(destination:HomePage(),isActive: self.$openHome){
                                            Text("")
                                            
                                            
                                        }
                                        
                                        
                                        Button(action:{
                                            //    asdfasdf
                                            
                                            
                                            //     self.openHome = true
                                            self.openTracking = true
                                            
                                            
                                        }){
                                            EmptyView()
                                            //                                            Image("close_1").resizable(
                                            //                                            ).frame(width: 0 , height : 0).padding(.all,10)
                                            //                                                .overlay(
                                            //                                                    Circle()
                                            //                                                        .stroke(
                                            //                                                            Color("ManBackColor"),lineWidth: 1
                                            //                                                    ).foregroundColor(Color("ManBackColor"))
                                            //                                            ).offset(x:16,y:-16).background(Color.white)
                                            
                                            
                                        }.sheet(isPresented: self.$openTracking) {
                                            
                                            TrackServiceProvider(openTrack:self.$openTracking,OrderID: self.OrderID, goToHome: self.$openHome).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                                            )).environment(\.colorScheme, .light)
                                            
                                        }
                                        
                                        Spacer()
                                        
                                    }
                                }
                                
                                
                            )
                            .background(Color.clear)
                            //            .clipped ()   .shadow(radius: 10, x: 0, y: 0)
                            
                            
                        }.padding(50).onAppear(){
                            self.ViewLoader = false
                            
                            self.timer = Timer.publish (every: 0, on: .current, in:
                                                            .common).autoconnect()
                        }
                        .onReceive(self.timer) { input in
                            self.timer.upstream.connect().cancel()
                            self.openTracking = true
                            
                            
                        }
                        
                    }
                    
                    
                    if(self.displayETA){
                        ZStack{
                            
                            VStack{
                                
                                HStack{
                                    Text("Your Location Changed !").font(.custom("Bold_Font".localized(), size: 17))
                                        .foregroundColor(Color("darkthemeletter"))
                                    
                                    
                                }
                                
                                
                                HStack{
                                    Text("New Expected Time of Arrival is").font(.custom("Regular_Font".localized(), size: 16)).padding()
                                        .foregroundColor(Color("darkthemeletter"))
                                    
                                    
                                }
                                
                                HStack{
                                    Text(getTime(timeIntervel: self.selectedServiceProvider.TimeToArrive)).font(.custom("ExtraBold_Font".localized(), size: 16))
                                        .foregroundColor(Color("darkthemeletter"))
                                    
                                    
                                }
                                
                                
                                
                                
                                HStack{
                                    
                                    Spacer()
                                    
                                    
                                    Button(action: {
                                        
                                        self.displayETA = false
                                    }){
                                        
                                        Text("OK").font(.custom("ExtraBold_Font".localized(), size: 17))
                                            .padding(.trailing).padding(.top,20)
                                    }
                                    
                                }
                                
                                //                                            if(self.showAlert){
                                //
                                //
                                //                                                                          Text(self.errorStatus).font(.custom("Regular_Font".localized(), size: 14)).foregroundColor(Color.red).padding()
                                //
                                //                                                                                                    }
                            }.frame(width :320 , height: 150)
                            
                            .padding()
                            
                        }.background(Color.white).clipped().shadow(radius: 2)
                    }
                    
                    
                    
                    if(self.failure){
                        
                        ZStack{
                            
                            VStack{
                                
                                HStack{
                                    Text("Failed to change location !").font(.custom("Bold_Font".localized(), size: 17))
                                        .foregroundColor(Color("darkthemeletter"))
                                    
                                    
                                }
                                
                                
                                HStack{
                                    Text("No driver available for you please try again later ").multilineTextAlignment(.center).font(.custom("Regular_Font".localized(), size: 16)).padding()
                                        .foregroundColor(Color("darkthemeletter"))
                                    
                                    
                                }
                                
                                
                                
                                
                                
                                HStack{
                                    
                                    Spacer()
                                    
                                    
                                    Button(action: {
                                        
                                        self.failure = false
                                    }){
                                        
                                        Text("OK").font(.custom("ExtraBold_Font".localized(), size: 17))
                                            .padding(.trailing).padding(.top,20)
                                    }
                                    
                                }
                                
                                //                                            if(self.showAlert){
                                //
                                //
                                //                                                                          Text(self.errorStatus).font(.custom("Regular_Font".localized(), size: 14)).foregroundColor(Color.red).padding()
                                //
                                //                                                                                                    }
                            }.frame(width :320 , height: 150)
                            
                            .padding()
                            
                        }.background(Color.white).clipped().shadow(radius: 2)
                    }
                    
                    if(self.ViewLoader){
                        Loader()
                    }
                    
                    //                    if(self.ShowApay){
                    
                    //                        ApplePayController(items: [PKPaymentSummaryItem(label: "Twaddan service Booking \(self.N)", amount: NSDecimalNumber(value: self.getSubTotal()))])
                    
                    //                    }
                    
                    if(self.showPaymentPortal){
                        
                        PaymentPage(Node: self.OrderID, Order: self.OR, VehicleList: self.VehicleList, status: self.$PStatus, ALERT3: self.$ALERT3,BLUR:self.$BLUR,products:self.k ,showPaymentPortal:self.$showPaymentPortal,billingAddress:self.billingAddress)
                    }
                    
                    
                    
                    if(self.PStatus == "Failed" || self.PStatus == "Cancelled" || self.PStatus == "AuthFailed" )  {
                        ZStack{
                            
                            VStack{
                                
                                HStack{
                                    Text("Payment Failed !").font(.custom("Bold_Font".localized(), size: 17))
                                        .foregroundColor(Color("darkthemeletter"))
                                    
                                    
                                }
                                
                                if(self.PStatus == "Failed"){
                                    HStack{
                                        
                                        Text("Card Payment Process Failed").font(.custom("Regular_Font".localized(), size: 16)).padding()
                                            .foregroundColor(Color("darkthemeletter"))
                                        
                                        
                                    }
                                }
                                if(self.PStatus == "Cancelled"){
                                    HStack{
                                        
                                        Text("Payment Cancelled by user").font(.custom("Regular_Font".localized(), size: 16)).padding()
                                            .foregroundColor(Color("darkthemeletter"))
                                        
                                        
                                    }
                                }
                                if(self.PStatus == "AuthFailed"){
                                    HStack{
                                        
                                        Text("Authentication Failed").font(.custom("Regular_Font".localized(), size: 16)).padding()
                                            .foregroundColor(Color("darkthemeletter"))
                                        
                                        
                                    }
                                }
                                
                                //                                                                    HStack{
                                //                                                                        Text(getTime(timeIntervel: self.selectedServiceProvider.TimeToArrive)).font(.custom("ExtraBold_Font".localized(), size: 30))
                                //                                                                                                                                               .foregroundColor(Color("darkthemeletter"))
                                //
                                //
                                //                                                                                                                                                          }
                                
                                
                                
                                
                                HStack{
                                    
                                    Spacer()
                                    
                                    
                                    Button(action: {
                                        
                                        self.PStatus = "Fresh"
                                    }){
                                        
                                        Text("OK").font(.custom("ExtraBold_Font".localized(), size: 17))
                                            .padding(.trailing).padding(.top,20)
                                    }
                                    
                                }
                                
                                //                                            if(self.showAlert){
                                //
                                //
                                //                                                                          Text(self.errorStatus).font(.custom("Regular_Font".localized(), size: 14)).foregroundColor(Color.red).padding()
                                //
                                //                                                                                                    }
                            }.frame(width :320 , height: 150)
                            
                            .padding()
                            
                        }.background(Color.white).clipped().shadow(radius: 2).onAppear(){
                            self.ViewLoader = false
                        }
                        
                    }
                    
                    
                }
                
                
            }
            
            
            
            
        }.background(Color("h1")).onAppear{
            
            self.billingAddress = Billing(firstName: self.NAME, lastName: "Twaddan Customer", address1: self.ADDRESS, city: UserDefaults.standard.string(forKey: "EMIRATES"), countryCode: "UAE")
            let connectedRef = Database.database().reference(withPath: ".info/connected")
            connectedRef.observe(.value, with: { snapshot in
                if snapshot.value as? Bool ?? false {
                    print("oooooooo","Connected")
                    
                    self.Connected = true
                    
                    
                } else {
                    self.Connected = false
                    
                    print("oooooooo","Not connected")
                }
            })
            
            
            
            
            
            self.ref.child("Users").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("personal_details/phoneNumber").observeSingleEvent(of: DataEventType.value) { (datasnapshot) in
                
                
                if(datasnapshot.exists()){
                    
                    
                    //                            if( datasnapshot.childSnapshot(forPath: "personal_details/phoneNumber").exists()){
                    self.storedPhone = String(String(describing:(datasnapshot.value)!).suffix(9))
                    
                    
                    
                    //   NOTE : String = ""
                    // ADDRESS : String = ""
                    //                            }
                    
                }
                
                
            }
            
            
            //                    let monitor = NWPathMonitor()
            //                    monitor.pathUpdateHandler = { path in
            //                        if path.status == .satisfied {
            //                            print("OOOOOOOOOOOO","We're connected!")
            //                        } else {
            //                            print("OOOOOOOOOOOO","No connection.")
            //                        }
            //
            //                        print("OOOOOOOOOOOO",path.isExpensive)
            //                    }
            //
            //                    let queue = DispatchQueue(label: "Monitor")
            //                    monitor.start(queue: queue)
            
            if(self.calculateETA){
                self.FindDriver()
            }
            print("i was called")
            //self.cartFetch.readCart(settings: self.settings)
            
            //            print("Started 2 \(UserDefaults.standard.string(forKey: "Aid") ?? "0")")
            
            
            self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").observe(DataEventType.value, with: {
                
                (Msnapshot) in
                if(Msnapshot.exists()){
                    
                    self.ref.child("service_providers").child(String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider_id").value)!)).observeSingleEvent(of: DataEventType.value) { (snap) in
                        
                        
                        
                        
                        self.carts.removeAll()
                        
                        self.selectedServiceProvider = SelectedServiceProvider(ServiceProvider: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider").value)!), ServiceProvider_id: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider_id").value)!), ServiceProvider_image: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider_image").value)!), DriverID: String(describing :  (Msnapshot.childSnapshot(forPath:"DriverID").value)!), TimeToArrive: Msnapshot.childSnapshot(forPath:"ETA").value as? Double ?? 0.0 )
                        
                        //                                    SelectedServiceProvider(
                        //
                        //                                                                           ServiceProvider: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider").value)!),
                        //                                                                                                           ServiceProvider_id: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider_id").value)!),
                        //                                                                                                           ServiceProvider_image: String(describing :  (Msnapshot.childSnapshot(forPath:"ServiceProvider_image").value)!),DriverID: String(describing :  (Msnapshot.childSnapshot(forPath:"DriverID").value)!),ETA :Msnapshot.childSnapshot(forPath:"ETA").value as? Double ?? 0.0 ,SPSnap: snap)
                        
                        
                        self.VehicleList.removeAll()
                        
                        for ID in Msnapshot.childSnapshot(forPath:"Vehicles").children {
                            let id = ID as! DataSnapshot
                            
                            
                            var addonslist = [Addons]()
                            var servicelist = [Services]()
                            
                            let vehiclesets:VehicleSets =  VehicleSets (VehicleType: String(describing :  (id.childSnapshot(forPath:"Vehicle").value)!), VehicleNumber: String(describing :  (id.childSnapshot(forPath:"Vehicle Number").value)!), AddonSnaps: id.childSnapshot(forPath:"addons"), ServiceSnap: id.childSnapshot(forPath:"services"))
                            
                            self.VehicleList.append(vehiclesets)
                            
                            for SERVICE in id.childSnapshot(forPath:"services").children {
                                let serv_snap = SERVICE as! DataSnapshot
                                
                                let service : Services = Services(id: serv_snap.key,
                                                                  serviceName: String(describing :  (serv_snap.childSnapshot(forPath:"name".localized()).value)!),serviceNameO: String(describing :  (serv_snap.childSnapshot(forPath:"name_ar".localized()).value)!),
                                                                  serviceDesc: String(describing :  (serv_snap.childSnapshot(forPath:"description".localized()).value)!),serviceDescO: String(describing :  (serv_snap.childSnapshot(forPath:"description_ar".localized()).value)!),
                                                                  ServiceImage: String(describing :  (serv_snap.childSnapshot(forPath:"image_url").value)!),
                                                                  ServicePrice: String(describing :  (serv_snap.childSnapshot(forPath:"price").value)!),ServiceTime: String(describing :  (serv_snap.childSnapshot(forPath:"time_required").value)!))
                                servicelist.append(service)
                            }
                            
                            for ADDONS in id.childSnapshot(forPath:"addons").children{
                                
                                
                                let add_snap = ADDONS as! DataSnapshot
                                
                                
                                let addon : Addons = Addons(id: add_snap.key,
                                                            addonName: String(describing :  (add_snap.childSnapshot(forPath:"name".localized()).value)!),addonNameO:String(describing :  (add_snap.childSnapshot(forPath:"name_ar".localized()).value)!) , addonDesc:  String(describing :  (add_snap.childSnapshot(forPath:"description".localized()).value ?? "")),addonDescO:  String(describing :  (add_snap.childSnapshot(forPath:"description_ar".localized()).value ?? "")),
                                                            addonImage: String(describing :  (add_snap.childSnapshot(forPath:"image_url").value)!),
                                                            addonPrice: String(describing :  (add_snap.childSnapshot(forPath:"price").value)!))
                                addonslist.append(addon)
                                
                            }
                            
                            let cart : CartStruct = CartStruct(id:id.key , addons:addonslist , services: servicelist , vehicle: String(describing :  (id.childSnapshot(forPath:"Vehicle").value)!), vehicleName: String(describing :  (id.childSnapshot(forPath:"VehicleName").value)!)
                                                               //                            ,ServiceProvider: String(describing :  (id.childSnapshot(forPath:"ServiceProvider").value)!),ServiceProvider_id: String(describing :  (id.childSnapshot(forPath:"ServiceProvider_id").value)!),ServiceProvider_image: String(describing :  (id.childSnapshot(forPath:"ServiceProvider_image").value)!)
                                                               
                            )
                            
                            //                         print("Started 3 \(self.carts.count)")
                            self.carts.append(cart)
                            
                        }
                        
                        
                        self.buttonArray.removeAll()
                        
                        
                        
                        for O in snap.childSnapshot(forPath: "offers").children  {
                            let Offers = O as! DataSnapshot
                            let button : ActionSheet.Button = .default(Text(String(describing :  (Offers.childSnapshot(forPath:"description".localized()).value ?? ""))),action:{
                                self.VoucherCode = String(describing :  (Offers.childSnapshot(forPath:"promo_code").value ?? ""))
                                
                            })
                            
                            self.buttonArray.append(button)
                        }
                        self.buttonArray.append(.cancel())
                        
                        
                    }
                }
            })
            
            
            
            
            //            var Offer = self.findOffers.Offers.filter {
            //                                               $0.ServiceProviderID.contains(self.cartFetch.selectedServiceProvider.ServiceProvider_id)
            //
            //            }
            //            print("AAAAAAA \(Offer.count)")
            
            
        }
        
        .navigationBarTitle("Check out",displayMode: .inline)
        //            .navigationBarHidden(true)
        .edgesIgnoringSafeArea(.top)
    }
    
    func FindDriver (){
        
        //                    self.Snap = self.booking.ServiceSnap
        
        var selectedETAD : ETAAndDriver = ETAAndDriver(ETA:10000000.0 , DriverID: "")
        
        self.ref.child("service_providers").child(self.selectedServiceProvider.ServiceProvider_id).observeSingleEvent(of: DataEventType.value, with: { (ServiceSnap) in
            
            
            self.ref.child("drivers").observeSingleEvent(of: DataEventType.value) { (allDriverSnapshot)
                in
                
                
                let     allDriver :DataSnapshot = allDriverSnapshot as! DataSnapshot
                
                var x = ServiceSnap.childSnapshot(forPath: "drivers_info/active_drivers_id").childrenCount
                
                
                for driver  in  ServiceSnap.childSnapshot(forPath: "drivers_info/active_drivers_id").children{
                    let  driverSnap = driver as! DataSnapshot
                    //                                   print("UUUUUUUU",driverSnap.key,(String(describing :  (allDriver.value)!)))
                    
                    print( (String(describing :  (allDriver.childSnapshot(forPath:"\(driverSnap.key)/live_location/latitude").value)!), (String(describing :  (allDriver.childSnapshot(forPath:"\(driverSnap.key)/live_location/longitude").value)!))))
                    
                    
                    let driverlat =
                        //10.797765
                        Double(String(describing :  (allDriver.childSnapshot(forPath:"\(driverSnap.key)/live_location/latitude").value)!))
                    
                    let driverlon =
                        //76.635908
                        Double(String(describing :  (allDriver.childSnapshot(forPath:"\(driverSnap.key)/live_location/longitude").value)!))
                    
                    
                    var driverloc : CLLocation = CLLocation(latitude: driverlat!, longitude: driverlon!)
                    var driver2D : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: driverlat!, longitude: driverlon!)
                    
                    
                    
                    
                    let userlat =
                        //                    10.874858
                        UserDefaults.standard.double(forKey: "latitude")
                    let userlon =
                        //                    76.449728
                        UserDefaults.standard.double(forKey: "longitude")
                    
                    
                    var userloc : CLLocation = CLLocation(latitude: userlat, longitude: userlon)
                    var user2D  : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: userlat, longitude: userlon)
                    
                    
                    
                    var eta:Double = 10000000
                    
                    let origin = "\(driver2D.latitude),\(driver2D.longitude)"
                    let destination = "\(user2D.latitude),\(user2D.longitude)"
                    
                    
                    let S : String =  "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=\(origin)&destinations=\(destination)&mode=driving&key=AIzaSyCYQeItZ7SVEQ24Grjzc3zA1B_2DU_GfPU"
                    
                    let url = URL(string:S
                    )
                    //        else {
                    //        let error = NSError(domain: "LocalDomain", code: 0, userInfo: [NSLocalizedDescriptionKey: "Failed to create object URL"])
                    //        print("Error: \(error)")
                    //        return
                    //    }
                    let config = URLSessionConfiguration.default
                    let session = URLSession(configuration: config)
                    let task = session.dataTask(with: url!, completionHandler: {
                        (data, response, error) in
                        
                        //                                                                                                                                                   print("11111111",driversnap.key)
                        x = x-1
                        if error != nil {
                            print(error!.localizedDescription)
                        }
                        else {
                            
                            //  print(response)
                            do {
                                if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                                    
                                    guard let routes = json["rows"] as? NSArray else {
                                        DispatchQueue.main.async {
                                        }
                                        return
                                    }
                                    if (routes.count > 0) {
                                        let overview_polyline = routes[0] as? NSDictionary
                                        //   let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                                        //  let points = dictPolyline?.object(forKey: "points") as? String
                                        
                                        DispatchQueue.main.async {
                                            //
                                            
                                            
                                            let legs = overview_polyline?["elements"] as! Array<Dictionary<String, AnyObject>>
                                            
                                            let distance = legs[0]["distance"] as? NSDictionary
                                            let distanceValue = distance?["value"] as? Int ?? 0
                                            let distanceDouleValue = distance?["value"] as? Double ?? 0.0
                                            let duration = legs[0]["duration"] as? NSDictionary
                                            let totalDurationInSeconds = duration?["value"] as? Int ?? 0
                                            let durationDouleValue = duration?["value"] as? Double ?? 0.0
                                            
                                            
                                            
                                            eta = durationDouleValue+300 ;
                                            
                                            
                                            if(eta < 301){
                                                eta = 10000000
                                            }
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            if (eta < selectedETAD.ETA){
                                                selectedETAD  = ETAAndDriver(ETA:eta, DriverID: driverSnap.key)
                                                
                                            }
                                            
                                            if(x==0){
                                                print("OOOOOO",x) ;
                                                //                                                                                                               self.ViewLoader = false
                                                if(selectedETAD.DriverID != ""){
                                                    
                                                    self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("DriverID").setValue(selectedETAD.DriverID)
                                                    self.selectedServiceProvider.DriverID = selectedETAD.DriverID
                                                    
                                                    self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ETA").setValue(selectedETAD.ETA)
                                                    self.selectedServiceProvider.TimeToArrive = selectedETAD.ETA
                                                    
                                                    self.displayETA = true
                                                    //                                                                                                                self.ReOrderToCart(item: item,DriverID:selectedETAD.DriverID, ETA: selectedETAD.ETA )
                                                    
                                                }else{
                                                    self.failure = true
                                                    
                                                }
                                                
                                            }
                                            print (S,eta)
                                            
                                            
                                        }
                                    }
                                    else {
                                        print(json)
                                        DispatchQueue.main.async {
                                            self.failure = true
                                            //   SVProgressHUD.dismiss()
                                        }
                                    }
                                }
                            }
                            catch {
                                print("error in JSONSerialization")
                                DispatchQueue.main.async {
                                    self.failure = true
                                    //  SVProgressHUD.dismiss()
                                }
                            }
                        }
                    })
                    task.resume()
                    
                    
                    
                    
                    
                    //                                                                    }
                    //
                    //
                    //
                    //                                                                }
                    
                    
                    
                    
                    
                    
                    
                    print("TTTTTTT",x)
                    
                    
                    
                    
                    
                    
                    
                }
                
                if(ServiceSnap.childSnapshot(forPath: "drivers_info/active_drivers_id").childrenCount == 0){
                    self.failure = true
                }
                
                
                
            }
            
            
            
        })
        
        
        
        
        
        
        
        //                                                    }
        
        //                                        }
        //                                                        }
        
        
    }
    
    func getSubTotal() -> Double {
        var SubTotal:Double = 0.0
        
        for cartStruct in ( self.carts ){
            
            //   var cartSum:Double = 0.0
            
            var sumOfServices:Double = 0.0
            
            for service in cartStruct.services {
                
                sumOfServices += Double(service.ServicePrice)!
            }
            
            var sumOfAddons:Double = 0.0
            
            for Addons in cartStruct.addons {
                
                sumOfAddons += Double(Addons.addonPrice)!
            }
            SubTotal += sumOfServices+sumOfAddons
        }
        
        return SubTotal
        
    }
    
    func AllButtonFalse(){
        
        ApplePay  = false
        CreditCard = false
        DebitCard = false
        NetBanking = false
        CashOnDelivery = false
        
        
    }
    
    
    
}
struct VehicleSets {
    
    var VehicleType : String
    var VehicleNumber : String
    var AddonSnaps: DataSnapshot
    var ServiceSnap: DataSnapshot
}

func getAddressType(Address:Int) -> String {
    switch Address {
    case 1:
        
        return "Apartment"
        break;
        
    case 2:
        return "Work"
        break;
        
    default:
        
        return "House"
        break;
        
        
    }
}

func EnterOrderToDB(Node :String ,Order : [String:Any],VehicleList:[VehicleSets]) {
    let ref = Database.database().reference()
    
    ref.child("orders").child("all_orders").child(Node).setValue(Order)
    
    //    print("OOOOOOOOOOOO",Node,Order["driver_id"] as! String,Order["customer_id"] as! String)
    //       ref.child("drivers").child(Order["driver_id"] as! String).child("new_orders").child(Node).setValue(Node)
    
    ref.child("Users").child(Order["customer_id"] as! String).child("active_orders").child("order_ids").child(Node).setValue(Node)
    
    for vehicleset in VehicleList {
        
        let V : [String : Any] = ["add_on_services":vehicleset.AddonSnaps.value,
                                  
                                  "services" :vehicleset.ServiceSnap.value,
        ]
        
        ref.child("orders").child("all_orders").child(Node).child("services").child("\(vehicleset.VehicleType) & \(vehicleset.VehicleNumber)").setValue(V)
        
    }
    
    ref.child("Cart").child(Order["customer_id"] as! String).removeValue()
    
    
    let savedAddress : [String : Any] = ["address":UserDefaults.standard.string(forKey: "PLACE") ?? "",
                                         "addressId" :Node,
                                         "addressNickName":UserDefaults.standard.string(forKey: "ANICK") ?? "",
                                         "addressType" :getAddressType(Address: UserDefaults.standard.integer(forKey: "ATYPE")),
                                         "buildingName":UserDefaults.standard.string(forKey: "AHOME") ?? "",
                                         "floor" :UserDefaults.standard.string(forKey: "AFLOOR") ?? "",
                                         "house":UserDefaults.standard.string(forKey: "AHOME") ?? "",
                                         "lat" :Double(UserDefaults.standard.double(forKey:  "latitude")) ,
                                         "lng" :Double(UserDefaults.standard.double(forKey: "longitude")),
                                         "notes" :UserDefaults.standard.string(forKey: "ADNOTE") ?? "",
                                         "office":UserDefaults.standard.string(forKey: "AOFFICE") ?? "",
                                         "streetName" :UserDefaults.standard.string(forKey: "ASTREET") ?? "",
    ]
    ref.child("Users").child(Order["customer_id"] as! String).child("saved_address").child(Node).setValue(savedAddress)
    UserDefaults.standard.set(nil,forKey: "PLACE")
    
    //   OrderID = Node
    
    
    
    
}




struct CheckOut_Previews: PreviewProvider {
    static var previews: some View {
        
        CheckOut(NAME: .constant(""), MOBILE: .constant(""), LNAME: .constant(""), ADDRESS: "")
        
    }
}


