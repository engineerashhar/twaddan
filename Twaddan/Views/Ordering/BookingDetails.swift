//
//  BookingDetails.swift
//  Twaddan
//
//  Created by Spine on 11/05/20.
//  Copyright © 2020 spine. All rights reserved.
//
import SDWebImageSwiftUI
import SwiftUI
import FirebaseDatabase
import MapKit
import SwitchLanguage
import GoogleMaps

struct BookingDetails: View {
    @EnvironmentObject var vehicleItems : VehicleItems
    @State var VehicleAlert:Bool = false
    @State var attempts = 0.0
    @State var sheetselectedVType: Bool = false
    @Binding  var OpenCart : Bool
    @Binding  var GoToBookDetails : Bool
    @ObservedObject private var locationManager = LocationManager()
    
    @State  var serviceSnap : DataSnapshot
    @State  var SelectedDriver : String
    
    @State  var ETA : Double
    //      var Reorder : String
    @State var PastOrders : [pastOrderSet] = [pastOrderSet]()
    //  @ObservedObject var cartFetch = CartFetch()
    
    @State var ALERT1 : Bool = false
    @State var DeleteConfirm : Bool = false
    @State var DeleteIndex : Int = 0
    @State var Alert3 : Bool = false
    @State var DirectCart : Bool = false
    @State var DirectAccess : Bool = false
    
    @State var ALERT1MESSAGE :String = ""
    
    
    @State var ALERT2 : Bool = false
    @State var ALERT2MESSAGE :String = ""
    
    @State var OpenPartialSheet : Bool = false
    @State var LastAddress : [String:String] = [String:String]()
    
    @State var goToMap : Bool = false
    
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @ObservedObject  var cartFetch = CartFetch()
    @ObservedObject  var firebaseEntry = FirebaseEntry()
    @EnvironmentObject var settings : UserSettings
    //  var serviceSnaspshot : Datasnapshot
    let ref = Database.database().reference()
    @State var VehicleArray : [VEHICLETYPES] = [VEHICLETYPES]()
    
    //    @State var selectedAddons : [Addons] = [Addons]()
    //    @State var selectedService : [Services] = [Services]()
    
    @State var selectedVType: String = UserDefaults.standard.string(forKey: "Vtype")!
    @State var VTypeBool : Bool = false
    @State var StreetAddress : String = ""
    // @State var VType : [String] = ["saloon", "SUV", "Super SUV", "Caravan","Bike"]
    @State var VType
        : [String] = ["saloon", "5_seaters", "7_seaters", "caravan","bike","boat_xtra"]
    //        : [String] = ["Saloon", "SUV", "Super SUV", "Caravan","Bike","Boat"]
    @State var VDType
        : [String] = ["saloon", "5_seaters", "7_seaters", "caravan","bike","boat_xtra"]
    @State var VImages : [String] = ["saloon", "5_seaters", "7_seaters", "caravan","bike","boat_xtra"]
    @State var selectedServices : [Services] = [Services]()
    @State var selectedAddons : [Addons] = [Addons]()
    @State var ForCartS : [DataS] = [DataS]()
    @State var ForCartA : [DataA] = [DataA]()
    @State var ForCartV : [VEHICLETYPES] = [VEHICLETYPES]()
    
    //    @State var VImages = ["saloon", "suv", "super_suv", "caravan","bike"]
    
    @State var Loader : Bool = false
    
    
    var providersFetch = getServiceAndAddons()
    
    @Binding var openVA : Bool
    @State var openSPD : Bool = false
    
    var body: some View {
        //  Text("Detail")
        
        
        NavigationView{
            
            GeometryReader{ geometry in
                ZStack{
                    
                    Color("h1").edgesIgnoringSafeArea(.all)
                    ZStack(alignment:.top){
                        
                        
                        
                        if(self.DirectAccess){
                            NavigationLink(destination: Cart(GoToBookDetailsOld: .constant(false))
                                            
                                            .environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                                            )), isActive: self.$DirectCart){
                                
                                EmptyView()
                            }
                        }
                        
                        
                        Group{
                            //First Layer
                            
                            VStack{
                                
                                
                                
                                //         RoundedCorners(color: Color("backcolor2"), tl: 0, tr: 0, bl: 60, br: 60)
                                //       .frame(width:geometry.size.width,height: geometry.size.height*0.25)
                                //        .background(Color("backcolor2"))
                                //  .foregroundColor(Color("ManBackColor"))
                                
                                
                                
                                
                                Spacer().frame(height:100)
                                
                                ScrollView{
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    // Spacer().frame(width:20)
                                    ZStack{
                                        HStack(spacing:0){
                                            //
                                            //                                                                //                                                                                                                       Image("basicbackimage")
                                            AnimatedImage(url : URL (string: String(describing: self.serviceSnap.childSnapshot(forPath:"personal_information/image").value ?? "") )).resizable()
                                                .frame(width: 75, height: 57)
                                                //
                                                .padding(10)
                                            //
                                            
                                            
                                            
                                            VStack(alignment: .leading,spacing: 0){
                                                
                                                
                                                
                                                
                                                Text(String(describing: self.serviceSnap.childSnapshot(forPath:"personal_information/name".localized()).value ?? ""))
                                                    .foregroundColor(Color.black)
                                                    .font(.custom("Bold_Font".localized(), size: 15))
                                                    .frame(height:17)
                                                //                            .padding(0)
                                                
                                                Text(String(describing: self.serviceSnap.childSnapshot(forPath:"personal_information/description".localized()).value ?? ""))
                                                    .lineLimit(1)
                                                    .foregroundColor(Color("s1"))
                                                    .font(.custom("Regular_Font".localized(), size: 13))
                                                
                                                
                                                RatingViewLarge(item:
                                                                    
                                                                    self.serviceSnap.childSnapshot(forPath:"personal_information/ratings").value != nil ?
                                                                    self.serviceSnap.childSnapshot(forPath:"personal_information/ratings").value as! Double : 0).padding(.top,3)
                                                
                                                
                                                //                                                                    HStack{
                                                
                                                
                                                
                                                
                                                //                                                                        Text(service.Servicetime < 1000000 ? getTime(timeIntervel:service.Servicetime).localized() : "Calculating ..".localized())
                                                //                                                                                                                               Text("Reach within 1 hour 30 minutes")
                                                //                                                                            .font(.custom("Regular_Font".localized(), size: 12))
                                                //                                                                            .foregroundColor(Color("s1"))
                                                
                                                //                                                                        Spacer()
                                                
                                                
                                                
                                                
                                                //                                                                    }
                                                //                        Spacer()
                                                
                                            }.padding(8)
                                            //                        .background(Color.red)
                                            
                                            Spacer()
                                        }.background(Color.white).cornerRadius(10).clipped().shadow(radius: 5).padding()
                                        .onTapGesture {
                                            //
                                            //                                                                    self.OpenCart = false
                                            self.openSPD = true
                                        }
                                        .sheet(isPresented: self.$openSPD) {
                                            
                                            ServiceProviderPage(serviceSnap: self.serviceSnap, ETA:self.ETA,Driver:self.SelectedDriver, GoButton: .constant(false)).environmentObject(self.vehicleItems).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                                            ))
                                            //
                                        }
                                        //
                                        
                                    }.frame(width:geometry.size.width)
                                    
                                    
                                    //                                                            .padding(.horizontal,5)
                                    
                                    // Spacer().frame(width:20)
                                    //                                                                        HStack(spacing:0){
                                    //
                                    //                        //                                                        Image("basicbackimage")
                                    //                                                                             AnimatedImage(url : URL (string: String(describing: self.serviceSnap.childSnapshot(forPath:"personal_information/image").value ?? "") ))
                                    //                                                                                    .resizable() .cornerRadius(5)
                                    //                                                                                    .frame(width: 93, height: 108)
                                    //                                                                                    .overlay(HStack{
                                    //                                                                                        Spacer()
                                    //                                                                                        VStack{
                                    //
                                    //                                                                                            Text(String(describing: self.serviceSnap.childSnapshot(forPath:"personal_information/ratings").value ?? "0").prefix(3))
                                    //                        //                                                                    Text("1.4")
                                    //                                                                                                .font(.custom("ExtraBold_Font".localized(), size: 12))
                                    //                                                                                                .foregroundColor(.white)
                                    //                                                                                                .frame(width: 20.0, height: 20.0)
                                    //                                                                                                .background(Color(self.getColor(Rate:  String(describing: self.serviceSnap.childSnapshot(forPath:"personal_information/ratings").value ?? "0"))))
                                    //                        //                                                                        .background(Color.red)
                                    //                                                                                                .cornerRadius(4)
                                    //                                                                                                .padding([.top, .trailing], 5.0)
                                    //                                                                                            Spacer()
                                    //                                                                                        }
                                    //
                                    //                                                                                    }).padding(8)
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //                                                                                VStack(alignment: .leading){
                                    //
                                    //
                                    //
                                    //
                                    //                                                                                   Text(String(describing: self.serviceSnap.childSnapshot(forPath:Localize(key:"personal_information/name")).value ?? ""))
                                    //                                                                                        .foregroundColor(Color("darkthemeletter"))
                                    //                                                                                        .font(.custom("Bold_Font".localized(), size: 14))
                                    //                                                                                        .frame(height:17)
                                    //                                                            //                            .padding(0)
                                    //
                                    //                                                                                    Text(String(describing: self.serviceSnap.childSnapshot(forPath:Localize(key:"personal_information/name")).value ?? ""))
                                    //                                                                                    .lineLimit(2)
                                    //                                                                                        .foregroundColor(Color("MainFontColor1"))
                                    //                                                                                        .font(.custom("Regular_Font".localized(), size: 12))
                                    //                        //                                                                  .frame(height:30)
                                    //                                                            //                            .padding(.top,3)
                                    //
                                    //                                                                                    Spacer()
                                    //                                                                                    Divider()
                                    //
                                    //                                                                                    HStack{
                                    //
                                    //
                                    //
                                    //                                                                                        Image("prefix__start_1").renderingMode(.template).resizable()
                                    //                                                                                            .frame(width: 10, height: 10).padding(.all,7)
                                    //                                                                                            .foregroundColor(Color.white)
                                    //                                                                                            .background(Color("destbackgreen")).clipShape(Circle())
                                    //
                                    //                                                                                        Text(self.ETA < 1000000 ? getTime(timeIntervel:self.ETA) : "Calculating ..")
                                    ////                                                                                        Text("Within 1 hour 30 minutes")
                                    //                                                                                            .font(.custom("Regular_Font".localized(), size: 12))
                                    //                                                                                            .foregroundColor(Color("darkthemeletter"))
                                    //
                                    //                                                                                        Spacer()
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //
                                    //                                                                                    }
                                    //                                                            //                        Spacer()
                                    //
                                    //                                                                                }.padding(8)
                                    //                                                            //                        .background(Color.red)
                                    //
                                    //
                                    
                                    
                                    //                                                                            .background(Color.white)
                                    //                                                                            .cornerRadius(5)
                                    //                                                                            .shadow(radius: 2)
                                    //                                                                            .frame(height:124)
                                    //                                                                        .padding(.all,10).padding(.top,20)
                                    // Spacer().frame(width:20)
                                    
                                    //                                                                       }.frame(height:124)
                                    
                                    VStack{
                                        Spacer()
                                        //Service Provider Summary
                                        //                            HStack{
                                        //
                                        //                                Spacer().frame(width:20)
                                        //                                HStack{
                                        //
                                        //                                    //Image("basicbackimage")
                                        //                                    AnimatedImage(url : URL (string: String(describing: self.serviceSnap.childSnapshot(forPath:"personal_information/image").value ?? "") ))
                                        //                                        .resizable()
                                        //                                        .padding(.all, 5)                                                           .frame(width: 100, height: 100)
                                        //                                        .overlay(HStack{
                                        //                                            Spacer()
                                        //                                            VStack{
                                        //
                                        //                                                Text(String(describing: self.serviceSnap.childSnapshot(forPath:"personal_information/ratings").value ?? "0").prefix(3))  .font(.custom("ExtraBold_Font".localized(), size: 12))
                                        //                                                    .foregroundColor(.white)
                                        //                                                    .frame(width: 25.0, height: 25.0)
                                        //                                                    .background(Color("morethanthree"))
                                        //                                                    .cornerRadius(4)
                                        //                                                    .padding([.top, .trailing], 10.0)
                                        //                                                Spacer()
                                        //                                            }
                                        //
                                        //                                        })
                                        //                                        .cornerRadius(5)
                                        //                                        .padding(.trailing, 15.0)
                                        //
                                        //
                                        //                                    VStack(alignment: .leading){
                                        //
                                        //
                                        //
                                        //
                                        //                                        Text(String(describing: self.serviceSnap.childSnapshot(forPath:Localize(key:"personal_information/name")).value ?? ""))
                                        //                                            .foregroundColor(Color("darkthemeletter"))
                                        //                                            .font(.custom("Bold_Font".localized(), size: 14))
                                        //                                            .padding(.top,20)
                                        //
                                        //
                                        //                                        Text(String(describing: self.serviceSnap.childSnapshot(forPath:Localize(key:"personal_information/name")).value ?? ""))
                                        //                                            .foregroundColor(Color("MainFontColor1"))
                                        //                                            .font(.custom("Regular_Font".localized(), size: 12))
                                        //
                                        //
                                        //
                                        //                                        Divider()
                                        //
                                        //                                        HStack{
                                        //
                                        //
                                        //
                                        //                                            Image("prefix__start_1").renderingMode(.template)
                                        //                                               .resizable()
                                        //                                                .frame(width: 10, height: 10).padding(.all,7)
                                        //                                                .foregroundColor(Color.white)
                                        //                                                .background(Color("destbackgreen")).clipShape(Circle())
                                        //
                                        //                                            Text(self.ETA<10000000 ? getTime(timeIntervel: self.ETA) : "Calculating")
                                        //                                                .font(.custom("Regular_Font".localized(), size: 12))
                                        //                                                .foregroundColor(Color("darkthemeletter"))
                                        //
                                        //                                            Spacer()
                                        //
                                        //
                                        //
                                        //
                                        //
                                        //                                        }.padding(.top,10)
                                        //                                        Spacer()
                                        //
                                        //                                    }
                                        //
                                        //                                    .padding(.trailing,15)
                                        //
                                        //
                                        //
                                        //                                }
                                        //
                                        //                                .background(Color.white)
                                        //                                .cornerRadius(5)
                                        //                                .shadow(radius: 2)
                                        //
                                        //                                Spacer().frame(width:20)
                                        //
                                        //                            }
                                        
                                        
                                        
                                        if(self.vehicleItems.vehicleItem.count == 0) {
                                            
                                            HStack{
                                                Text("Select Vehicle").font(.custom("Regular_Font".localized(), size: 16))
                                                Spacer()
                                            }.padding(.top).padding(.horizontal)
                                            
                                            HStack{
                                                Divider().padding(.horizontal,10)
                                            }.frame(height:1)
                                            
                                            HStack{
                                                
                                                
                                                Button(action:{
                                                    withAnimation{
                                                        self.openVA = true
                                                    }
                                                }){
                                                    HStack{
                                                        Image(systemName:
                                                                //                                        self.VEHICLES.contains(V) ?
                                                                "plus.circle.fill"
                                                              //                                            : "circle"
                                                        )
                                                        .resizable()
                                                        
                                                        .frame(width: 25, height: 24)
                                                        
                                                        .foregroundColor(Color("green")).padding(.trailing)
                                                        
                                                        
                                                        Text("ADD VEHICLE").font(.custom("Bold_Font".localized(), size: 13)).foregroundColor(Color("h2")
                                                        )
                                                        
                                                    }.frame(width:geometry.size.width*0.4)
                                                    
                                                }
                                            }.padding(.horizontal,10)
                                            
                                            HStack{
                                                Divider().padding(.horizontal,10)
                                            }.frame(height:1)
                                            
                                            
                                            
                                            
                                        }
                                        
                                        
                                        ForEach(self.vehicleItems.vehicleItem){
                                            Vselected in
                                            
                                            
                                            
                                            VStack{
                                                //                                HStack{
                                                //                             Text("Selected Vehicle").font(.custom("Regular_Font".localized(), size: 16))
                                                //                                    Spacer()
                                                //                                }.padding(.top).padding(.horizontal)
                                                //
                                                //                                HStack{
                                                //                                Divider().padding(.horizontal,10)
                                                //                                }.frame(height:1)
                                                
                                                HStack{
                                                    
                                                    HStack{
                                                        
                                                        
                                                        Text(LocNumbers(key:  String((self.vehicleItems.vehicleItem.firstIndex(of: Vselected) ?? 0 )+1))).font(.custom("Regular_Font".localized(), size: 14)).foregroundColor(Color.white).padding(10).background(Color(#colorLiteral(red: 0, green: 0.2509999871, blue: 0.4979999959, alpha: 1))).clipShape(Circle()).padding(.trailing)
                                                        
                                                        AnimatedImage(url: URL(string:Vselected.image ?? "")) .resizable().padding(10)
                                                            .overlay(Circle().stroke(Color("h8"),lineWidth: 2)).frame(width:44,height:44)
                                                        
                                                        Text(Vselected.name).font(.custom("Bold_Font".localized(), size: 13)).foregroundColor(Color("h2")
                                                        )
                                                        
                                                        Spacer()
                                                        
                                                        
                                                        Button(action:{
                                                            self.DeleteIndex =  self.vehicleItems.vehicleItem.firstIndex(of:  Vselected)!
                                                            self.DeleteConfirm = true
                                                            
                                                        }){
                                                            Image("Group 922"
                                                                  //                                        systemName:
                                                                  //                                        self.VEHICLES.contains(V) ?
                                                                  //                                            "x.circle.fill"
                                                                  //                                            : "circle"
                                                            )
                                                            .resizable()
                                                            
                                                            .frame(width: 22, height: 24)
                                                            
                                                            .foregroundColor(Color.black).padding(.trailing)
                                                        }.alert(isPresented: self.$DeleteConfirm){
                                                            Alert(title: Text("Move to Trash ?"), primaryButton:.default(Text("No")), secondaryButton: .default(Text("Yes"),action:{
                                                                self.vehicleItems.vehicleItem.remove(at:self.DeleteIndex)
                                                            }))
                                                        }
                                                        
                                                    }
                                                    //                                    .frame(width:geometry.size.width*0.40)
                                                    //                                    Divider()
                                                    
                                                }.padding().background(Color.white).overlay(
                                                    
                                                    RoundedRectangle(cornerRadius: 5).stroke(Color("h1"), lineWidth: 1)
                                                ).padding(.horizontal,10).padding(.top,20)
                                                
                                                
                                                
                                                
                                                if(self.providersFetch.getServiceData(Snap: self.serviceSnap, Vechicle: Vselected.Vid,id:Vselected.id).count>0){
                                                    HStack{
                                                        // Text(self.settings.Aid )
                                                        Text("Select Service").font(.custom("Regular_Font".localized(), size: 16)).padding(.top).padding(.horizontal)
                                                        Spacer()
                                                    }
                                                }
                                                //ServiceProvider Services
                                                
                                                VStack(spacing:10)    {
                                                    
                                                    ForEach(self.providersFetch.getServiceData(Snap: self.serviceSnap, Vechicle: Vselected.Vid,id:Vselected.id)){ service in
                                                        
                                                        HStack(){
                                                            Spacer().frame(width: self.selectedServices.contains(service) ? 10 : 20)
                                                            
                                                            ZStack{
                                                                
                                                                
                                                                
                                                                HStack(alignment: .top){
                                                                    
                                                                    //      Image("basicbackimage")
                                                                    
                                                                    
                                                                    ZStack{
                                                                        AnimatedImage(url : URL (string: service.ServiceImage)).resizable()
                                                                            .frame(width: 100, height: 100)
                                                                            
                                                                            .cornerRadius(5)
                                                                        //
                                                                        VStack{
                                                                            //                                                    Spacer()
                                                                            ZStack{
                                                                                
                                                                                if(self.selectedServices.contains(service)){
                                                                                    Circle().foregroundColor(Color.white).frame(width: 55, height: 55)
                                                                                    Image(systemName: "checkmark.circle.fill")
                                                                                        .resizable()
                                                                                        
                                                                                        .frame(width: 50, height: 50)
                                                                                        
                                                                                        .foregroundColor(Color("ManBackColor"))
                                                                                    //
                                                                                    //
                                                                                }
                                                                                //
                                                                            }
                                                                        }
                                                                        Spacer()
                                                                    }.padding(.leading,10)
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    VStack(alignment: .leading){
                                                                        Text(service.serviceName)
                                                                            .foregroundColor(Color("darkthemeletter"))
                                                                            .font(.custom("Regular_Font".localized(), size: 16))
                                                                        
                                                                        
                                                                        Text(service.serviceDesc)
                                                                            .foregroundColor(Color("s1"))
                                                                            .font(.custom("Regular_Font".localized(), size: 12))
                                                                        
                                                                        Spacer()
                                                                    }.padding(.vertical,15)
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    Spacer()
                                                                    
                                                                    
                                                                    Text("\(LocNumbers(key:service.ServicePrice))"+" AED".localized())
                                                                        .font(.custom("ExtraBold_Font".localized(), size: 15))
                                                                        .foregroundColor(Color("c2"))
                                                                        .padding(15)
                                                                    //                                                    Spacer()
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    //                                                Spacer()
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                }
                                                                HStack{
                                                                    
                                                                    Spacer()
                                                                    //                                                                                           VStack{
                                                                    //
                                                                    ////                                                                                            Button(action:{
                                                                    ////
                                                                    ////
                                                                    ////                                                                                            }){
                                                                    //
                                                                    //
                                                                    //
                                                                    //                                                                                          //  }
                                                                    //                                                                                               Spacer()
                                                                    //                                                                                           }
                                                                }
                                                                
                                                                
                                                                
                                                            }
                                                            
                                                            .background(Color.white).cornerRadius(10)
                                                            
                                                            .overlay(
                                                                
                                                                RoundedRectangle(cornerRadius: 10).stroke(Color("s1"), lineWidth: 1)
                                                            )
                                                            .frame(height:100)
                                                            Spacer().frame(width: self.selectedServices.contains(service) ? 10 : 20)
                                                            
                                                        }.onTapGesture {
                                                            withAnimation{
                                                                let Q = DataS(id: Vselected.id, VName: Vselected.name, Vid: Vselected.Vid, selectedService: service)
                                                                
                                                                if(!self.selectedServices.contains(service)){
                                                                    
                                                                    
                                                                    print("OOOOOOOO",Q)
                                                                    self.selectedServices.append(service)
                                                                    
                                                                    self.ForCartS.append(Q)
                                                                    if(!self.ForCartV.contains(Vselected)){
                                                                        self.ForCartV.append(Vselected)
                                                                    }
                                                                }else{
                                                                    self.selectedServices.remove(at: self.selectedServices.firstIndex(of: service)!)
                                                                    
                                                                    self.ForCartS.remove(at: self.ForCartS.firstIndex(of: Q)!)
                                                                    
                                                                    if(self.ForCartV.contains(Vselected)){
                                                                        self.ForCartV.remove(at: self.ForCartV.firstIndex(of: Vselected)!)
                                                                    }
                                                                }
                                                                //
                                                                //                                                                                                var arr : [Services] =      self.providersFetch.getServiceData(Snap: self.serviceSnap, Vechicle: self.selectedVType)
                                                                //
                                                                //                                                                                                arr[arr.firstIndex(of:
                                                                
                                                                //                                                                                                service )!].select = true
                                                            }
                                                        }
                                                    }
                                                    
                                                }.padding(.bottom,20)
                                                //Serviece Provider Addons
                                                if(self.providersFetch.getAddondata(Snap: self.serviceSnap,id:Vselected.id).count>0){
                                                    HStack{
                                                        Text("Add on’s").font(.custom("Regular_Font".localized(), size: 16)).padding(.top).padding(.horizontal)
                                                        Spacer()
                                                        
                                                    }
                                                }
                                                VStack{
                                                    
                                                    
                                                    ForEach(self.providersFetch.getAddondata(Snap: self.serviceSnap,id:Vselected.id)){ addons in
                                                        
                                                        HStack{
                                                            Spacer().frame(width:self.selectedAddons.contains(addons) ? 10 : 20)
                                                            ZStack{
                                                                
                                                                
                                                                HStack(alignment:.top){
                                                                    
                                                                    //Image("basicbackimage")
                                                                    
                                                                    VStack(alignment:.center){
                                                                        ZStack{
                                                                            AnimatedImage(url : URL (string: addons.addonImage)).resizable()
                                                                                .frame(width: 50, height: 70)
                                                                                
                                                                                .cornerRadius(5)
                                                                            
                                                                            
                                                                            if(self.selectedAddons.contains(addons)){
                                                                                Circle().foregroundColor(Color.white).frame(width: 33, height: 33)
                                                                                Image(systemName: "checkmark.circle.fill")
                                                                                    .resizable()
                                                                                    
                                                                                    .frame(width: 30, height: 30)
                                                                                    
                                                                                    .foregroundColor(Color("ManBackColor"))
                                                                            }
                                                                            
                                                                        }
                                                                    }
                                                                    
                                                                    
                                                                    VStack(alignment: .leading){
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        Text(addons.addonName)
                                                                            .foregroundColor(Color("darkthemeletter"))
                                                                            .font(.custom("Regular_Font".localized(), size: 16))
                                                                        
                                                                        
                                                                        Text(addons.addonDesc)
                                                                            .foregroundColor(Color("s1"))
                                                                            .font(.custom("Regular_Font".localized(), size: 12))
                                                                        
                                                                        //                                                Spacer()
                                                                        
                                                                    }.padding()
                                                                    
                                                                    
                                                                    Spacer()
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    Text("\(LocNumbers(key:addons.addonPrice))"+" AED".localized())
                                                                        .font(.custom("ExtraBold_Font".localized(), size: 15))
                                                                        .foregroundColor(Color("c2")).padding()
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                } .alert(isPresented: self.$Alert3) {
                                                                    
                                                                    
                                                                    Alert(title: Text( "Sorry".localized()), message: Text("All drivers are busy for this service provider".localized()),dismissButton : .default(Text("OK".localized()), action: {
                                                                        
                                                                        exit(0)
                                                                        
                                                                        
                                                                    }))
                                                                }
                                                                
                                                                
                                                                //                                            HStack{
                                                                //
                                                                //                                                                                            Spacer()
                                                                //                                                                                                                                       VStack{
                                                                //
                                                                ////                                                                                                                                        Button(action:{
                                                                ////
                                                                ////
                                                                ////                                                                                                                                        }){
                                                                //                                                                                                                                           Image(systemName: self.selectedAddons.contains(addons) ? "checkmark.circle.fill" : "circle")
                                                                //                                                                                                                                            .resizable()                                       .frame(width: 15, height: 15)
                                                                //
                                                                //                                                                                                                                               .foregroundColor(Color("ManBackColor"))
                                                                //                                                                                                                                               .padding([.top, .trailing], 10)
                                                                //
                                                                //
                                                                //                                                                                                                                        //}
                                                                //                                                                                                                                           Spacer()
                                                                //                                                                                                                                       }
                                                                //                                                                                        }
                                                                
                                                            }
                                                            
                                                            .background(Color.white)
                                                            .cornerRadius(5)
                                                            .overlay(
                                                                
                                                                RoundedRectangle(cornerRadius: 5).stroke(Color("s1"), lineWidth: 1)
                                                            )
                                                            .frame(height:70)
                                                            //Spacer().frame(width:20)
                                                            Spacer().frame(width:self.selectedAddons.contains(addons) ? 10 : 20)
                                                            
                                                        }.onTapGesture {
                                                            withAnimation{
                                                                let Q = DataA(id: Vselected.id, VName: Vselected.name, Vid: Vselected.Vid, selectedAddons: addons);                                 if(!self.selectedAddons.contains(addons)){
                                                                    self.selectedAddons.append(addons)
                                                                    self.ForCartA.append(Q)
                                                                    
                                                                    
                                                                }else{
                                                                    self.selectedAddons.remove(at: self.selectedAddons.firstIndex(of: addons)!)
                                                                    self.ForCartA.remove(at: self.ForCartA.firstIndex(of: Q)!)
                                                                    
                                                                    
                                                                }
                                                                //
                                                                //                                                                                                var arr : [Services] =      self.providersFetch.getServiceData(Snap: self.serviceSnap, Vechicle: self.selectedVType)
                                                                //
                                                                //                                                                                                arr[arr.firstIndex(of:
                                                                
                                                                //                                                                                                service )!].select = true
                                                            }
                                                        }
                                                    }
                                                    
                                                }.padding(.bottom,20)
                                                
                                                
                                            }.background(Color("ca1")).overlay(
                                                
                                                RoundedRectangle(cornerRadius: 10).stroke(Color(#colorLiteral(red: 0.6710000038, green: 0.8119999766, blue: 0.949000001, alpha: 1)), lineWidth: 1)
                                            ).padding()
                                            
                                        }
                                        Spacer().frame(height:120)
                                        
                                    }
                                }
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                Spacer().frame(height:80)
                                
                            }
                            
                            
                            
                            //Second layer
                            
                            
                            
                            
                            //                                    VStack(alignment: .leading){
                            //
                            //
                            //                                            Spacer().frame(height:geometry.size.height*0.1)
                            //
                            //
                            //
                            //                                            HStack(alignment:.bottom , spacing: 0){
                            //
                            //                    //                            Spacer().frame(width:15)
                            //
                            //                                                VStack(alignment:.leading){
                            //                    //                                 Spacer().frame(height:10)
                            //                                                    HStack{
                            //
                            //                                                        Text("Location").foregroundColor(Color("darkthemeletter"))
                            //                                                            .font(.custom("Bold_Font".localized(), size: 15))
                            ////                                                            .onTapGesture {
                            ////                                                            self.ALERT1 = true
                            ////                                                        }
                            //
                            //                                                        Spacer()
                            //                                                    }
                            //                                                    .padding(.bottom,10)
                            //
                            //                                                    NavigationLink(destination : FindLocation(CalculateETA: .constant(false)) ,isActive: self.$goToMap ){
                            //
                            //                                                                                       EmptyView()
                            //                                                                                   }
                            //                                                    Button(action:{
                            //
                            //
                            //                                                                                  self.goToMap = true
                            //
                            //
                            //
                            //                                                                              }){
                            //                                                        HStack{
                            //                                                            Image("placeholder")
                            //                                                                .resizable()
                            //                                                                .frame(width: 13, height: 18)
                            //                                                            Text(UserDefaults.standard.string(forKey: "PLACE") != nil ? String(UserDefaults.standard.string(forKey: "PLACE")!) : "Select Location")
                            //                                                                .foregroundColor(Color("darkthemeletter"))
                            //                                                                .font(.custom("Regular_Font".localized(), size: 14))
                            //                                                                .padding(.leading,10)
                            //                                                                .padding(.trailing,10)
                            //
                            //                                                            Spacer()
                            //                                                            //                                                Button(action: {}){
                            //                                                            //                                                    Image("close_1")
                            //                                                            //                                                        .resizable()
                            //                                                            //                                                        .frame(width: 12, height: 12)
                            //                                                            //
                            //                                                            //                                                }
                            //                                                            //                                                .padding(.trailing, 15.0)
                            //                                                            //
                            //                                                            //
                            //                                                            //                                                Button(action: {}){
                            //                                                            //
                            //                                                            //
                            //                                                            //                                                    Image("select")
                            //                                                            //
                            //                                                            //
                            //                                                            //
                            //                                                            //
                            //                                                            //                                                }
                            //                                                            // .padding(.trailing, 15.0)
                            //
                            //
                            //
                            //
                            //                                                        }   .padding(.leading, 20.0)
                            //                                                                                              .padding(.vertical,10.0)
                            //                                                                                                  .background(Color.white)
                            //                                                        .cornerRadius(4)
                            //                                                        .shadow(radius: 1)
                            //
                            //                                                    }.overlay(
                            //                                                        RoundedRectangle(cornerRadius: 4)
                            //                                                            .stroke(Color("ManBackColor"), lineWidth: 0.25)
                            //                                                    )
                            //                                                       .clipped() .shadow(radius: 1)
                            //                                                        .frame(height: 30)
                            //                    //                                  Spacer().frame(width:35)
                            //
                            //                                                }.frame(width:geometry.size.width*0.6).padding(.leading,20)
                            //                                                Spacer()
                            //
                            //                                           Button(action:{
                            //
                            //                                                                                self.sheetselectedVType = true
                            //                                                                                                          }){
                            //                                                                            VStack{
                            //                                                //                                      Spacer().frame(height:20)
                            //
                            //
                            //                                                                                Image(self.selectedVType).padding(self.selectedVType=="Choose Vehicle" ? 15:10).overlay(
                            //                                                                                                                                                       Circle()
                            //                                                                                                                                                           .stroke(
                            //                                                                                                                                                               Color("ManBackColor"),lineWidth: 1)
                            //                                                                                ).foregroundColor(self.VehicleAlert && self.self.selectedVType == "Choose Vehicle"  ? Color.red:Color("ManBackColor"))
                            //
                            //                                                                                Text(self.selectedVType) .font(.custom("Regular_Font".localized(), size: 14))
                            //                                                                                .foregroundColor(self.VehicleAlert && self.selectedVType == "Choose Vehicle" ? Color.red :Color("ManBackColor"))
                            //                                                //                                    .offset(x: self.VehicleAlert && self.selectedVType == "Choose Vehicle" ? -10 : 0)
                            //                                                //                                    .animation(Animation.default.repeatCount(5))
                            //                                                                                .modifier(Shake(animatableData: CGFloat(self.attempts)))
                            //                                                                                                                                           .animation(self.VehicleAlert ? Animation.default.repeatCount(5):nil)
                            //
                            //                                                                            }
                            //
                            //                                                                            }
                            //                                                                            .actionSheet(isPresented: self.$sheetselectedVType, content: {
                            //                                                                                 ActionSheet(title: Text("Vehicle Type"), message: Text("Choose Vehicle Type"), buttons: [
                            //                                                                                    .default(Text(self.VType[0])) {  self.selectedVType = self.VType[0] ;UserDefaults.standard.set(self.VType[0], forKey: "Vtype" );
                            ////                                                                                        self.ReadWithETA()
                            ////                                                self.NearestSortOnly()
                            //
                            //                                                                                    },
                            //                                                                                    .default(Text(self.VType[1])) {  self.selectedVType = self.VType[1] ;UserDefaults.standard.set(self.VType[1], forKey: "Vtype");
                            ////                                                                                        self.ReadWithETA()
                            ////                                                                                    self.NearestSortOnly()
                            ////
                            //                                                                                    },
                            //                                                                                    .default(Text(self.VType[2])) {  self.selectedVType = self.VType[2]; UserDefaults.standard.set(self.VType[2], forKey: "Vtype");
                            ////                                                                                        self.ReadWithETA()
                            ////                                                                                    self.NearestSortOnly()
                            ////
                            //                                                                                    },
                            //                                                                                    .default(Text(self.VType[3])) { self.selectedVType = self.VType[3]; UserDefaults.standard.set(self.VType[3], forKey: "Vtype");
                            ////                                                                                        self.ReadWithETA()
                            ////                                                                                    self.NearestSortOnly()
                            ////
                            //                                                                                    },
                            //                                                                                    .default(Text(self.VType[4])) {  self.selectedVType = self.VType[4] ;UserDefaults.standard.set(self.VType[4], forKey: "Vtype");
                            ////                                                                                        self.ReadWithETA()
                            ////                                                                                    self.NearestSortOnly()
                            ////
                            //                                                                                    },
                            //
                            //                                                                                .default(Text(self.VType[5])) {  self.selectedVType = self.VType[5] ;UserDefaults.standard.set(self.VType[5], forKey: "Vtype");
                            ////                                                                                    self.ReadWithETA()
                            ////                                                                                                                   self.NearestSortOnly()
                            //
                            //                                                                                    },
                            //                                                                                                                                   .cancel()])
                            //
                            //                                                                            })
                            //
                            //                                                Spacer()
                            //                    //                            .contextMenu{
                            //                    //
                            //                    //                                ForEach(self.VType, id: \.self){ Vehicle in
                            //                    //
                            //                    //                                    Button(action:{
                            //                    //                                     //   self.VTypeBool = false
                            //                    //                                        self.selectedVType = Vehicle
                            //                    //
                            //                    //                                       //
                            //                    //
                            //                    //                                        UserDefaults.standard.set(Vehicle, forKey: "Vtype")
                            //                    //                                        //self.services.removeAll()
                            //                    //                                       // self.loadDataSingle()
                            //                    //                                        self.ReadWithETA()
                            //                    //                                        self.NearestSortOnly()
                            //                    //
                            //                    //                                    }){
                            //                    //                                        HStack{
                            //                    //                                                  Image(self.VImages[self.VType.firstIndex(of: Vehicle) ?? 0])
                            //                    //                                                .resizable()
                            //                    //                                                .frame(width: 14, height: 14)
                            //                    //                                                .padding(.trailing, 10)
                            //                    //
                            //                    //
                            //                    //
                            //                    //                                            Text(Vehicle)
                            //                    //                                            Spacer()
                            //                    //
                            //                    //                                        }
                            //                    //                                    }
                            //                    //                                }
                            //                    //
                            //                    //                            }
                            //                    //                            Spacer()
                            //                    //                            VStack{
                            //                    //
                            //                    //
                            //                    //                                HStack{
                            //                    //                                    Text("Vehicle").foregroundColor(Color("darkthemeletter")).font(.custom("Bold_Font".localized(), size: 15))
                            //                    //
                            //                    //                                    Spacer()
                            //                    //                                }
                            //                    //
                            //                    //                                HStack{
                            //                    //
                            //                    //                                    TextField("Vehicle", text:self.$selectedVType )
                            //                    //                                    .disabled(true)
                            //                    // .foregroundColor(Color("darkthemeletter"))                                        .font(.custom("Regular_Font".localized(), size: 14))
                            //                    //                                        .padding(.leading,10)
                            //                    //
                            //                    //
                            //                    //                                    Spacer()
                            //                    //                                    Button(action: {
                            //                    //                                         withAnimation{
                            //                    //                                                                                   self.VTypeBool.toggle()
                            //                    //                                                                               }
                            //                    //                                    }){
                            //                    //                                        Image("up_and_down_2")
                            //                    //                                            .resizable()
                            //                    //                                            .frame(width: 11, height: 6)
                            //                    //
                            //                    //                                    }
                            //                    //                                    .padding(.trailing, 15.0)
                            //                    //
                            //                    //
                            //                    //
                            //                    //                                    // .padding(.trailing, 15.0)
                            //                    //
                            //                    //
                            //                    //                                }
                            //                    //
                            //                    //                                }
                            //                    //                                .contextMenu{
                            //                    //
                            //                    //                                    ForEach(self.VType, id: \.self){ Vehicle in
                            //                    //
                            //                    //                                        Button(action:{
                            //                    //                                         //   self.VTypeBool = false
                            //                    //                                            self.selectedVType = Vehicle
                            //                    //
                            //                    //                                           //
                            //                    //
                            //                    //                                            UserDefaults.standard.set(Vehicle, forKey: "Vtype")
                            //                    //                                            //self.services.removeAll()
                            //                    //                                           // self.loadDataSingle()
                            //                    //                                            self.ReadWithETA()
                            //                    //                                            self.NearestSortOnly()
                            //                    //
                            //                    //                                        }){
                            //                    //                                            HStack{
                            //                    //                                                      Image(self.VImages[self.VType.firstIndex(of: Vehicle) ?? 0])
                            //                    //                                                    .resizable()
                            //                    //                                                    .frame(width: 14, height: 14)
                            //                    //                                                    .padding(.trailing, 10)
                            //                    //
                            //                    //
                            //                    //
                            //                    //                                                Text(Vehicle)
                            //                    //                                                Spacer()
                            //                    //
                            //                    //                                            }
                            //                    //                                        }
                            //                    //                                    }
                            //                    //
                            //                    //                                }.frame(width:geometry.size.width*0.35 ,height:40).background(Color.white).background(Color.white)
                            //                    //                                    .cornerRadius(4)
                            //                    //                                    .shadow(radius: 1)
                            //                    //
                            //                    //                                VStack(spacing:10){
                            //                    //
                            //                    //                                    ForEach(self.VType, id: \.self){ Vehicle in
                            //                    //
                            //                    //                                        Button(action:{
                            //                    //                                            self.VTypeBool = false
                            //                    //                                            self.selectedVType = Vehicle
                            //                    //
                            //                    //                                        }){
                            //                    //                                            HStack{
                            //                    //                                                Image(self.VImages[self.VType.firstIndex(of: Vehicle) ?? 0])
                            //                    //                                                    .resizable()
                            //                    //                                                    .frame(width: 20.0, height: 20.0)
                            //                    //                                                    .padding(.trailing, 10)
                            //                    //
                            //                    //
                            //                    //
                            //                    //                                                Text(Vehicle)
                            //                    //                                                Spacer()
                            //                    //
                            //                    //                                            }
                            //                    //                                        }
                            //                    //                                    }
                            //                    //
                            //                    //                                }
                            //                    //                                     .frame(height:self.VTypeBool ? 200: 0)
                            //                    //                             .frame(width:self.VTypeBool ? geometry.size.width*0.3  :0)
                            //                    //                            //    .padding(10)
                            //                    //                                .foregroundColor(Color(red: 0.38, green: 0.51, blue: 0.64, opacity: 1.0))
                            //                    //
                            //                    //
                            //                    //
                            //                    //
                            //                    //                                .font(.custom("Regular_Font".localized(), size: 14))
                            //                    //                                .padding(.leading,10)
                            //                    //                                    // .frame(width:geometry.size.width*0.35 )
                            //                    //                                    //
                            //                    //
                            //                    //
                            //                    //                                    .background(Color.white).background(Color.white)
                            //                    //                                    .cornerRadius(4)
                            //                    //                                    .shadow(radius: 1)
                            //                    //
                            //                    //                            }
                            //                                                //
                            //
                            //                    //
                            //                    //                            VStack{
                            //                    //
                            //                    //
                            //                    //                                HStack{
                            //                    //
                            //                    //
                            //                    //                                    Spacer().frame(height:33)
                            //                    //
                            //                    //                                }
                            //                    //
                            //                    //
                            //                    //
                            //                    //                            }
                            //                    //
                            //                    //                            Spacer().frame(width:15)
                            //                    //
                            //
                            //
                            //
                            //                                            }.frame(width:geometry.size.width,height: 300)
                            //                    //                        ).background(Color.red)
                            //
                            //
                            //
                            //
                            //
                            //
                            //
                            //
                            //
                            //                    //                        Spacer()
                            //
                            //                                        }.frame(width:geometry.size.width,height: 200).foregroundColor(Color("ManBackColor"))
                            
                            //                                            .cornerRadius(20)
                            //
                            //                VStack(alignment: .leading){
                            //
                            //
                            //                                        Spacer().frame(height:geometry.size.height*0.1)
                            //
                            //
                            //
                            //                                        HStack( alignment:.top ,spacing:10){
                            //
                            //                //                            Spacer().frame(width:15)
                            //
                            //                                            VStack{
                            //                                                 Spacer().frame(height:10)
                            //                                                HStack{
                            //
                            //                                                    Text("Location").foregroundColor(Color("darkthemeletter")).font(.custom("Bold_Font".localized(), size: 15))
                            //
                            //                                                    Spacer()
                            //                                                }.padding(.bottom,10)
                            //
                            //                                                NavigationLink(destination : FindLocation(CalculateETA: .constant(false)) ,isActive: self.$goToMap ){
                            //
                            //                                                                                   EmptyView()
                            //                                                                               }
                            //                                                Button(action:{
                            //
                            //
                            //                                                                              self.goToMap = true
                            //
                            //
                            //
                            //                                                                          }){
                            //                                                    HStack{
                            //                                                        Image("placeholder")
                            //                                                            .resizable()
                            //                                                            .frame(width: 13, height: 18)
                            //                                                        Text(UserDefaults.standard.string(forKey: "PLACE") != nil ? String(UserDefaults.standard.string(forKey: "PLACE")!) : "Select Location")
                            //                                                            .foregroundColor(Color("darkthemeletter"))
                            //                                                            .font(.custom("Regular_Font".localized(), size: 14))
                            //                                                            .padding(.leading,10)
                            //                                                            .padding(.trailing,10)
                            //
                            //                                                        Spacer()
                            //                                                        //                                                Button(action: {}){
                            //                                                        //                                                    Image("close_1")
                            //                                                        //                                                        .resizable()
                            //                                                        //                                                        .frame(width: 12, height: 12)
                            //                                                        //
                            //                                                        //                                                }
                            //                                                        //                                                .padding(.trailing, 15.0)
                            //                                                        //
                            //                                                        //
                            //                                                        //                                                Button(action: {}){
                            //                                                        //
                            //                                                        //
                            //                                                        //                                                    Image("select")
                            //                                                        //
                            //                                                        //
                            //                                                        //
                            //                                                        //
                            //                                                        //                                                }
                            //                                                        // .padding(.trailing, 15.0)
                            //
                            //
                            //
                            //
                            //                                                    }   .padding(.leading, 20.0)
                            //                                                                                          .padding(.vertical,10.0)
                            //                                                                                              .background(Color.white)
                            //                                                    .cornerRadius(4)
                            //                                                    .shadow(radius: 1)
                            //
                            //                                                }.overlay(
                            //                                                    RoundedRectangle(cornerRadius: 4)
                            //                                                        .stroke(Color("ManBackColor"), lineWidth: 0.25)
                            //                                                )
                            //                                                   .clipped() .shadow(radius: 1)
                            //                                                    .frame(height: 30)
                            //                                                  Spacer().frame(width:35)
                            //                //                                HStack(spacing:10){
                            //                //
                            //                //                                                                      ForEach(self.VType,id:\.self){ V in
                            //                //                                                                        Button(action:{}){
                            //                //                                                                          VStack{
                            //                //
                            //                //                                                                            Image(V).padding(10).overlay(
                            //                //                                                                            Circle()
                            //                //                                                                                .stroke(
                            //                //                                                                                    Color("ManBackColor"),lineWidth: 1)
                            //                //                                                                            ).foregroundColor(Color("ManBackColor"))
                            //                //                                                                              Text(V) .font(.custom("Regular_Font".localized(), size: 14))
                            //                //
                            //                //                                                                          }
                            //                //                                                                        }
                            //                //                                                                      }
                            //                //
                            //                //                                    }.padding().background(Color.white).cornerRadius(20).clipped().shadow(radius: 5)
                            //                                                    .hidden()
                            //                //
                            //                //
                            //
                            //
                            //
                            //
                            //
                            //
                            //
                            //
                            //
                            //                                            }.padding()
                            //
                            //                                            Button(action:{
                            //
                            //                                                self.sheetselectedVType = true
                            //                                                                          }){
                            //                                            VStack{
                            //                                                      Spacer().frame(height:20)
                            //
                            //
                            //                                                Image(self.selectedVType).padding(10).overlay(
                            //                                                                                                                       Circle()
                            //                                                                                                                           .stroke(
                            //                                                                                                                               Color("ManBackColor"),lineWidth: 1)
                            //                                                                                                                       ).foregroundColor(Color("ManBackColor"))
                            //                                                                                                                         Text(self.selectedVType) .font(.custom("Regular_Font".localized(), size: 14))
                            //
                            //                                                                            }.padding()
                            //                                            }
                            //                                            .actionSheet(isPresented: self.$sheetselectedVType, content: {
                            //                                                 ActionSheet(title: Text("Vehicle Type"), message: Text("Choose Vehicle Type"), buttons: [
                            //                                                    .default(Text(self.VType[0])) {  self.selectedVType = self.VType[0] ;UserDefaults.standard.set(self.VType[0], forKey: "Vtype" );
                            //
                            //                                                    },
                            //                                                    .default(Text(self.VType[1])) {  self.selectedVType = self.VType[1] ;UserDefaults.standard.set(self.VType[1], forKey: "Vtype");},
                            //                                                    .default(Text(self.VType[2])) {  self.selectedVType = self.VType[2]; UserDefaults.standard.set(self.VType[2], forKey: "Vtype");},
                            //                                                    .default(Text(self.VType[3])) { self.selectedVType = self.VType[3]; UserDefaults.standard.set(self.VType[3], forKey: "Vtype");},
                            //                                                    .default(Text(self.VType[4])) {  self.selectedVType = self.VType[4] ;UserDefaults.standard.set(self.VType[4], forKey: "Vtype");},
                            //                                                                    .cancel()])
                            //
                            //                                            })
                            //                //                            .contextMenu{
                            //                //
                            //                //                                ForEach(self.VType, id: \.self){ Vehicle in
                            //                //
                            //                //                                    Button(action:{
                            //                //                                     //   self.VTypeBool = false
                            //                //                                        self.selectedVType = Vehicle
                            //                //
                            //                //                                       //
                            //                //
                            //                //                                        UserDefaults.standard.set(Vehicle, forKey: "Vtype")
                            //                //                                        //self.services.removeAll()
                            //                //                                       // self.loadDataSingle()
                            //                //                                        self.ReadWithETA()
                            //                //                                        self.NearestSortOnly()
                            //                //
                            //                //                                    }){
                            //                //                                        HStack{
                            //                //                                                  Image(self.VImages[self.VType.firstIndex(of: Vehicle) ?? 0])
                            //                //                                                .resizable()
                            //                //                                                .frame(width: 14, height: 14)
                            //                //                                                .padding(.trailing, 10)
                            //                //
                            //                //
                            //                //
                            //                //                                            Text(Vehicle)
                            //                //                                            Spacer()
                            //                //
                            //                //                                        }
                            //                //                                    }
                            //                //                                }
                            //                //
                            //                //                            }
                            //                                            Spacer()
                            //                //                            VStack{
                            //                //
                            //                //
                            //                //                                HStack{
                            //                //                                    Text("Vehicle").foregroundColor(Color("darkthemeletter")).font(.custom("Bold_Font".localized(), size: 15))
                            //                //
                            //                //                                    Spacer()
                            //                //                                }
                            //                //
                            //                //                                HStack{
                            //                //
                            //                //                                    TextField("Vehicle", text:self.$selectedVType )
                            //                //                                    .disabled(true)
                            //                // .foregroundColor(Color("darkthemeletter"))                                        .font(.custom("Regular_Font".localized(), size: 14))
                            //                //                                        .padding(.leading,10)
                            //                //
                            //                //
                            //                //                                    Spacer()
                            //                //                                    Button(action: {
                            //                //                                         withAnimation{
                            //                //                                                                                   self.VTypeBool.toggle()
                            //                //                                                                               }
                            //                //                                    }){
                            //                //                                        Image("up_and_down_2")
                            //                //                                            .resizable()
                            //                //                                            .frame(width: 11, height: 6)
                            //                //
                            //                //                                    }
                            //                //                                    .padding(.trailing, 15.0)
                            //                //
                            //                //
                            //                //
                            //                //                                    // .padding(.trailing, 15.0)
                            //                //
                            //                //
                            //                //                                }
                            //                //
                            //                //                                }
                            //                //                                .contextMenu{
                            //                //
                            //                //                                    ForEach(self.VType, id: \.self){ Vehicle in
                            //                //
                            //                //                                        Button(action:{
                            //                //                                         //   self.VTypeBool = false
                            //                //                                            self.selectedVType = Vehicle
                            //                //
                            //                //                                           //
                            //                //
                            //                //                                            UserDefaults.standard.set(Vehicle, forKey: "Vtype")
                            //                //                                            //self.services.removeAll()
                            //                //                                           // self.loadDataSingle()
                            //                //                                            self.ReadWithETA()
                            //                //                                            self.NearestSortOnly()
                            //                //
                            //                //                                        }){
                            //                //                                            HStack{
                            //                //                                                      Image(self.VImages[self.VType.firstIndex(of: Vehicle) ?? 0])
                            //                //                                                    .resizable()
                            //                //                                                    .frame(width: 14, height: 14)
                            //                //                                                    .padding(.trailing, 10)
                            //                //
                            //                //
                            //                //
                            //                //                                                Text(Vehicle)
                            //                //                                                Spacer()
                            //                //
                            //                //                                            }
                            //                //                                        }
                            //                //                                    }
                            //                //
                            //                //                                }.frame(width:geometry.size.width*0.35 ,height:40).background(Color.white).background(Color.white)
                            //                //                                    .cornerRadius(4)
                            //                //                                    .shadow(radius: 1)
                            //                //
                            //                //                                VStack(spacing:10){
                            //                //
                            //                //                                    ForEach(self.VType, id: \.self){ Vehicle in
                            //                //
                            //                //                                        Button(action:{
                            //                //                                            self.VTypeBool = false
                            //                //                                            self.selectedVType = Vehicle
                            //                //
                            //                //                                        }){
                            //                //                                            HStack{
                            //                //                                                Image(self.VImages[self.VType.firstIndex(of: Vehicle) ?? 0])
                            //                //                                                    .resizable()
                            //                //                                                    .frame(width: 20.0, height: 20.0)
                            //                //                                                    .padding(.trailing, 10)
                            //                //
                            //                //
                            //                //
                            //                //                                                Text(Vehicle)
                            //                //                                                Spacer()
                            //                //
                            //                //                                            }
                            //                //                                        }
                            //                //                                    }
                            //                //
                            //                //                                }
                            //                //                                     .frame(height:self.VTypeBool ? 200: 0)
                            //                //                             .frame(width:self.VTypeBool ? geometry.size.width*0.3  :0)
                            //                //                            //    .padding(10)
                            //                //                                .foregroundColor(Color(red: 0.38, green: 0.51, blue: 0.64, opacity: 1.0))
                            //                //
                            //                //
                            //                //
                            //                //
                            //                //                                .font(.custom("Regular_Font".localized(), size: 14))
                            //                //                                .padding(.leading,10)
                            //                //                                    // .frame(width:geometry.size.width*0.35 )
                            //                //                                    //
                            //                //
                            //                //
                            //                //                                    .background(Color.white).background(Color.white)
                            //                //                                    .cornerRadius(4)
                            //                //                                    .shadow(radius: 1)
                            //                //
                            //                //                            }
                            //                                            //
                            //
                            //                //
                            //                //                            VStack{
                            //                //
                            //                //
                            //                //                                HStack{
                            //                //
                            //                //
                            //                //                                    Spacer().frame(height:33)
                            //                //
                            //                //                                }
                            //                //
                            //                //
                            //                //
                            //                //                            }
                            //                //
                            //                //                            Spacer().frame(width:15)
                            //                //
                            //
                            //
                            //
                            //                                        }
                            //
                            //
                            //
                            //
                            //
                            //
                            //
                            //
                            //
                            //                                        Spacer()
                            //
                            //                                    }.foregroundColor(Color("ManBackColor"))
                            //
                            //                                            .cornerRadius(20)
                            //    //
                            //
                            //
                            //                                    //Tird Layer
                            //
                            //
                            //                VStack{
                            //
                            //                    Spacer().frame(height:geometry.size.height*0.03)
                            //
                            //                    HStack{
                            //
                            //
                            //
                            //
                            //                        Spacer().frame(width:20)
                            //                        Text("Booking Details").font(.custom("Bold_Font".localized(), size: 16))
                            //                        Spacer()
                            //
                            //                        Button(action:{
                            //                                                             withAnimation{
                            //                                                                   self.OpenCart = true
                            //                                                               self.presentationMode.wrappedValue.dismiss()
                            //                                                                                                   self.GoToBookDetails = false
                            //                                                                 recalculateETA()
                            //                                                                self.DirectCart = true
                            //                                                             }
                            //                                                           }){
                            //
                            //                                                               ZStack{
                            //                                                                  Image(systemName: "cart").resizable().renderingMode(.template).foregroundColor(.black).frame(width:25,height:25)
                            //                                                              //
                            //                                                                  if(self.cartFetch.carts.count>0){
                            //                                                                  Text(String(self.cartFetch.carts.count)).font(.custom("Regular_Font".localized(), size: 10))
                            //                                                                                                                  .frame(width: 15.0, height: 15.0)
                            //                                                                                                                  .background(Circle().fill(Color.red))
                            //                                                                                                                  .foregroundColor(.white)
                            //                                                                                                                  .padding([.trailing, .bottom])
                            //                                                                  }
                            //                                                              }
                            //
                            //
                            //                                                           }
                            //
                            //
                            //                         Spacer().frame(width:20)
                            //                    }
                            //
                            //
                            //
                            //
                            //
                            //
                            //                    //.padding(.top, 50)
                            //                    Spacer()
                            //                }.foregroundColor(Color("ManBackColor"))
                            
                            
                            //
                            //                                        .cornerRadius(20)
                            //
                            //  Spacer()
                            VStack(alignment:.trailing){
                                Spacer()
                                    
                                    //                    Button(action:{
                                    //
                                    //                        if(self.selectedVType != "Choose Vehicle"){
                                    //
                                    //                              self.VehicleAlert = false
                                    //                        print(UserDefaults.standard.string(forKey: "Aid") ?? "0")
                                    //                        if(!self.selectedServices.isEmpty){
                                    //
                                    //
                                    //                            if(self.cartFetch.carts.count>0 ){
                                    //
                                    //
                                    //                             //   print(self.cartFetch.carts[0].ServiceProvider)
                                    //                            //    print((String(describing: self.serviceSnap.childSnapshot(forPath:Localize(key:"personal_information/name")).value! )))
                                    //                                if(self.cartFetch.selectedServiceProvider.ServiceProvider == (String(describing: self.serviceSnap.childSnapshot(forPath:Localize(key:"personal_information/name")).value! ))){
                                    //
                                    //                            let node = String((Date().timeIntervalSince1970)).replacingOccurrences(of: ".", with: "")
                                    //
                                    //
                                    //
                                    //                            for service in self.selectedServices{
                                    //
                                    //
                                    //
                                    //                                self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("services").child(service.id).setValue(service.getDict())
                                    //                            }
                                    //
                                    //                            for addons in self.selectedAddons{
                                    //                                self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("addons").child(addons.id).setValue(addons.getDict())
                                    //
                                    //                            }
                                    //                            self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("Vehicle").setValue(self.selectedVType)
                                    //
                                    //                             self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider").setValue(self.serviceSnap.childSnapshot(forPath:Localize(key:"personal_information/name")).value!)
                                    //                                    self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("DriverID").setValue(self.SelectedDriver)
                                    //
                                    //                                    if(self.ETA<10000000){
                                    //                                      self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ETA").setValue(self.ETA)
                                    //                                    }else{
                                    //                                          self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ETA").setValue(0)
                                    //                                    }
                                    //                                    self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider_id").setValue(self.serviceSnap.key)
                                    //                             self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider_image").setValue(self.serviceSnap.childSnapshot(forPath:"personal_information/image").value!)
                                    //
                                    //
                                    //                                    self.selectedServices.removeAll()
                                    //                                self.selectedAddons.removeAll()
                                    //
                                    //                                    self.selectedVType =  "Choose Vehicle"
                                    //                                    UserDefaults.standard.set("Choose Vehicle", forKey: "Vtype")
                                    //                          //  self.cartFetch.readCart(settings: self.settings)
                                    //                          //  self.GoToBookDetails = false
                                    //                                // self.OpenCart = true
                                    //                         //    self.presentationMode.wrappedValue.dismiss()
                                    //
                                    //                                }else{
                                    //
                                    //
                                    //                                    self.ALERT2 = true
                                    //
                                    //
                                    //
                                    //                                }
                                    //
                                    //
                                    //                            }else{
                                    //
                                    //
                                    //                               let node = String((Date().timeIntervalSince1970)).replacingOccurrences(of: ".", with: "")
                                    //
                                    //
                                    //
                                    //                               for service in self.selectedServices{
                                    //
                                    //
                                    //
                                    //                                   self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("services").child(service.id).setValue(service.getDict())
                                    //                               }
                                    //
                                    //                               for addons in self.selectedAddons{
                                    //                                   self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("addons").child(addons.id).setValue(addons.getDict())
                                    //
                                    //                               }
                                    //                               self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("Vehicle").setValue(self.selectedVType)
                                    //
                                    //                                self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider").setValue(self.serviceSnap.childSnapshot(forPath:Localize(key:"personal_information/name")).value!)
                                    //                                self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("DriverID").setValue(self.SelectedDriver)
                                    //                                  if(self.ETA<10000000){
                                    //                                    self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ETA").setValue(self.ETA)
                                    //                                  }else{
                                    //                                        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ETA").setValue(0)
                                    //                                  }
                                    //                               self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider_id").setValue(self.serviceSnap.key)
                                    //                                self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider_image").setValue(self.serviceSnap.childSnapshot(forPath:"personal_information/image").value!)
                                    //
                                    //
                                    //                                self.selectedServices.removeAll()
                                    //                                self.selectedAddons.removeAll()
                                    //
                                    //
                                    //                                self.selectedVType =  "Choose Vehicle"
                                    //                                UserDefaults.standard.set("Choose Vehicle", forKey: "Vtype")
                                    //                             //  self.cartFetch.readCart(settings: self.settings)
                                    //                            //   self.GoToBookDetails = false
                                    //                            //   self.OpenCart = true
                                    //                            //    self.presentationMode.wrappedValue.dismiss()
                                    //
                                    //                               }
                                    //
                                    //                        }else{
                                    //
                                    //
                                    //                            self.ALERT1 = true
                                    //                            self.ALERT1MESSAGE = "Do you Want to select At least on Service to proceed".localized()
                                    //
                                    //                        }
                                    //                        }else{
                                    //                                self.VehicleAlert = true
                                    //                            self.attempts += 1
                                    //                             self.ALERT1 = true
                                    //                            self.ALERT1MESSAGE = "Please choose vehicle".localized()
                                    //                        }
                                    //
                                    //
                                    //                    })
                                    //                    {
                                    //                        HStack(alignment:.center){
                                    //
                                    //
                                    //                           Circle()
                                    //                                      .foregroundColor(Color("addgreen")).frame(width:17,height:17)
                                    //                                  .overlay(
                                    //
                                    //                                      Image(systemName: "plus").resizable().frame(width:8,height: 8).foregroundColor(Color.white)
                                    //                                  )
                                    //
                                    //
                                    //                            Text("Add Vehicles").font(.custom("Regular_Font".localized(), size: 14)).foregroundColor(Color("darkthemeletter"))
                                    //                        }
                                    //                            .frame(height : 50)
                                    //                            .frame(maxWidth:.infinity)
                                    //                            .background(Color.white)
                                    //                    .clipped()
                                    //                    .shadow(radius: 3)
                                    //
                                    //
                                    //                    }
                                    
                                    .alert(isPresented: self.$ALERT2) {
                                        Alert(title: Text( "Sorry".localized()), message: Text("You have already selected another service provider".localized()),primaryButton: .default(Text("Delete Cart".localized()), action: {
                                            
                                            self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").removeValue()
                                            
                                            
                                        }), secondaryButton: .default(Text("Go To Cart"),action: {
                                            self.presentationMode.wrappedValue.dismiss()
                                            self.OpenCart = true
                                        }))
                                    }
                                
                                //  Spacer()
                                VStack{
                                    HStack{
                                        Text("Total").font(.custom("Regular_Font".localized(), size: 16))
                                        Spacer()
                                        Text("\(LocNumbers(key: self.getSubTotal()))" + " AED".localized()).font(.custom("ExtraBold_Font".localized(), size: 16))
                                    }.padding(.horizontal).environment(\.layoutDirection, .leftToRight).padding(.top)
                                    HStack{
                                        
                                        
                                        Button(action:{
                                            withAnimation{
                                                self.openVA = true
                                            }
                                        }){
                                            HStack{
                                                Image(systemName:
                                                        //                                        self.VEHICLES.contains(V) ?
                                                        "plus.circle.fill"
                                                      //                                            : "circle"
                                                )
                                                .resizable()
                                                
                                                .frame(width: 25, height: 24)
                                                
                                                .foregroundColor(Color("green")).padding(.trailing)
                                                
                                                
                                                Text("ADD VEHICLE").font(.custom("Bold_Font".localized(), size: 13)).foregroundColor(Color("h2")
                                                )
                                                
                                            }.frame(width:geometry.size.width*0.4)
                                            
                                        }
                                        
                                        
                                        Button(action: {
                                            
                                            DispatchQueue.global(qos: .background).async {
                                                
                                                if(self.ETA != 9999999 && self.ETA != 10000000){
                                                    
                                                    if(UserDefaults.standard.string(forKey: "PLACE") != nil){
                                                        
                                                        
                                                        if(!self.selectedServices.isEmpty){
                                                            print("1")
                                                            
                                                            if(self.cartFetch.carts.count>0 ){
                                                                print("11")
                                                                //                                 print("Zzzzzzzzzzzzzzzzz")
                                                                if(self.cartFetch.selectedServiceProvider.ServiceProvider == (String(describing: self.serviceSnap.childSnapshot(forPath:"personal_information/name".localized()).value! ))){
                                                                    
                                                                    
                                                                    for V in self.ForCartV{
                                                                        
                                                                        
                                                                        
                                                                        print("888888888",V)
                                                                        let node = String((V.id+Int.random(in: 0 ... 1000000))).replacingOccurrences(of: ".", with: "")
                                                                        
                                                                        let I:Int = V.id
                                                                        
                                                                        let Q = self.ForCartS.filter{
                                                                            ($0.id == I ) }
                                                                        
                                                                        
                                                                        for Data in Q{
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            let service = Data.selectedService
                                                                            //                            for service in self.selectedServices{
                                                                            
                                                                            
                                                                            
                                                                            self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("services").child(service.id).setValue(service.getDict())
                                                                            //                            }
                                                                            
                                                                            
                                                                        }
                                                                        
                                                                        let P:Int = V.id
                                                                        
                                                                        let C = self.ForCartA.filter{
                                                                            ($0.id == P ) }
                                                                        
                                                                        for addon in  C {
                                                                            let addons = addon.selectedAddons
                                                                            self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("addons").child(addons.id).setValue(addons.getDict())
                                                                            
                                                                        }
                                                                        
                                                                        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("Vehicle").setValue(V.Vid)
                                                                        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("VehicleName").setValue(V.name)
                                                                        
                                                                    }
                                                                    print("111")
                                                                    self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider").setValue(self.serviceSnap.childSnapshot(forPath:"personal_information/name".localized()).value!)
                                                                    self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("DriverID").setValue(self.SelectedDriver)
                                                                    if(self.ETA<10000000){
                                                                        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ETA").setValue(self.ETA)
                                                                    }else{
                                                                        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ETA").setValue(0)
                                                                    }
                                                                    self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider_id").setValue(self.serviceSnap.key)
                                                                    self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider_image").setValue(self.serviceSnap.childSnapshot(forPath:"personal_information/image").value!)
                                                                    
                                                                    
                                                                    //  self.cartFetch.readCart(settings: self.settings)
                                                                    print("1111")
                                                                    //                                   self.presentationMode.wrappedValue.dismiss()
                                                                    
                                                                    DispatchQueue.main.async {
                                                                        print("This is run on the main queue, after the previous code in outer block")
                                                                        
                                                                        self.GoToBookDetails = false
                                                                        self.OpenCart = true
                                                                        print("999999999999999",self.SelectedDriver)
                                                                        print("ZZZZZZZZ \(self.OpenCart)")
                                                                        self.DirectCart = true
                                                                    }
                                                                    //    self.presentationMode.wrappedValue.dismiss()
                                                                    
                                                                    
                                                                }else{
                                                                    
                                                                    
                                                                    self.ALERT2 = true
                                                                    
                                                                    
                                                                    
                                                                }
                                                                
                                                                
                                                            }else{
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                DispatchQueue.global(qos: .background).async {
                                                                    
                                                                    print("888888888",self.ForCartV.count)
                                                                    for V in self.ForCartV{
                                                                        
                                                                        print("888888888",V)
                                                                        let node = String((V.id+Int.random(in: 0 ... 1000000))).replacingOccurrences(of: ".", with: "")
                                                                        
                                                                        let I:Int = V.id
                                                                        
                                                                        let Q = self.ForCartS.filter{
                                                                            ($0.id == I ) }
                                                                        
                                                                        //
                                                                        for Data in Q{
                                                                            
                                                                            //                                                                    let node = String(Data.id+Int.random(in: 0 ... 1000000)).replacingOccurrences(of: ".", with: "")
                                                                            
                                                                            
                                                                            let service = Data.selectedService
                                                                            //                            for service in self.selectedServices{
                                                                            
                                                                            
                                                                            
                                                                            self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("services").child(service.id).setValue(service.getDict())
                                                                            //                            }
                                                                            
                                                                            
                                                                            
                                                                        }
                                                                        let P:Int = V.id
                                                                        
                                                                        let C = self.ForCartA.filter{
                                                                            ($0.id == P ) }
                                                                        
                                                                        for addon in  C {
                                                                            let addons = addon.selectedAddons
                                                                            self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("addons").child(addons.id).setValue(addons.getDict())
                                                                            
                                                                        }
                                                                        
                                                                        
                                                                        DispatchQueue.main.async {
                                                                            
                                                                            //  self.cartFetch.readCart(settings: self.settings)
                                                                            self.presentationMode.wrappedValue.dismiss()
                                                                            self.GoToBookDetails = false
                                                                            self.OpenCart = true
                                                                            self.DirectCart = true
                                                                            
                                                                        }
                                                                        
                                                                        
                                                                        
                                                                        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("Vehicle").setValue(V.Vid)
                                                                        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(node).child("VehicleName").setValue(V.name)
                                                                        
                                                                        
                                                                    }
                                                                    
                                                                    self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider").setValue(self.serviceSnap.childSnapshot(forPath:"personal_information/name".localized()).value!)
                                                                    self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("DriverID").setValue(self.SelectedDriver)
                                                                    if(self.ETA<10000000){
                                                                        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ETA").setValue(self.ETA)
                                                                    }else{
                                                                        self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ETA").setValue(0)
                                                                    }
                                                                    self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider_id").setValue(self.serviceSnap.key)
                                                                    self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("ServiceProvider_image").setValue(self.serviceSnap.childSnapshot(forPath:"personal_information/image").value!)
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                }
                                                                
                                                                //                                self.firebaseEntry.CartEntry1(ForCartS: self.ForCartS, ForCartA: self.ForCartA, serviceSnap: self.serviceSnap, SelectedDriver: self.SelectedDriver, ETA: self.ETA)
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                //    self.presentationMode.wrappedValue.dismiss()
                                                                
                                                                
                                                            }
                                                            
                                                        }else{
                                                            
                                                            if(self.cartFetch.carts.count>0){
                                                                
                                                                DispatchQueue.main.async {
                                                                    print("This is run on the main queue, after the previous code in outer block")
                                                                    
                                                                    self.presentationMode.wrappedValue.dismiss()
                                                                    self.GoToBookDetails = false
                                                                    self.OpenCart = true
                                                                    self.DirectCart = true
                                                                    print("\(self.GoToBookDetails)\(self.OpenCart)")
                                                                }
                                                                
                                                            }else{
                                                                self.ALERT1MESSAGE = "Do you Want to select At least on Service to proceed".localized()
                                                                self.ALERT1 = true
                                                                
                                                            }
                                                            
                                                            
                                                        }
                                                    }else{
                                                        
                                                        self.ALERT1MESSAGE = "Please select location"
                                                        self.ALERT1 = true
                                                        
                                                        
                                                    }
                                                    
                                                } else{
                                                    
                                                    self.ALERT1MESSAGE = "No Driver Available"
                                                    self.ALERT1 = true
                                                    
                                                    
                                                }
                                            }
                                        }){
                                            RoundedPanel()
                                                
                                                .overlay(
                                                    
                                                    HStack{
                                                        
                                                        Text("ADD TO CART").font(.custom("ExtraBold_Font".localized(), size: 18))
                                                            .foregroundColor(.white)
                                                        //                                    Spacer()
                                                        //
                                                        //
                                                        //
                                                        //                                    Text("Total \(LocNumbers(key: displaySum(selectedServices: self.selectedServices, selectedAddons: self.selectedAddons))) AED").font(.custom("ExtraBold_Font".localized(), size: 18))
                                                        //                                        .foregroundColor(.white)
                                                    }
                                                )
                                            
                                        }.alert(isPresented: self.$ALERT1) {
                                            Alert(title: Text( "Message"), message: Text(self.ALERT1MESSAGE), dismissButton: .default(Text("OK")))
                                        }
                                        // Spacer()
                                        
                                        .frame(maxWidth:.infinity)
                                    }.environment(\.layoutDirection, .leftToRight)
                                    
                                }.background(Color("h1"))
                            }
                            
                            
                        }  .blur(radius: self.OpenPartialSheet ? 2 : 0)
                        .brightness(self.OpenPartialSheet ? -0.2 : 0).blur(radius: self.openVA ? 15 : 0)
                        
                        .disabled(self.OpenPartialSheet||self.openVA)
                        
                        .brightness(self.openVA ? -0.2 :  0)
                        
                        //                if(self.Loader){
                        //                    
                        //                    Loader()
                        //                }
                        
                        //last layer
                        
                        
                        //                LocForReorder(OpenPartialSheet: self.OpenPartialSheet, goToMap: self., LastAddress: <#T##Binding<[String : String]>#>, PastOrders: <#T##Binding<[pastOrderSet]>#>)
                        
                        
                        
                        
                        
                        
                        
                        
                    }
                    if(self.openVA){
                        VehicleAddition(openVA:self.$openVA)
                    }
                }
                
                .frame(width: geometry.size.width, height: geometry.size.height)
                .onAppear{
                    
                    
                    
                    if(UserDefaults.standard.string(forKey: "PLACE") == nil){
                        //                                              UserDefaults.standard.set(nil, forKey: "PLACE")
                        
                        //                                                                           DispatchQueue.global(qos: .background).async {
                        DispatchQueue.main.async {
                            
                            let geocoder = GMSGeocoder()
                            
                            geocoder.reverseGeocodeCoordinate(self.locationManager.location!.coordinate) { response, error in
                                //
                                if error != nil {
                                    print("reverse geodcode fail: \(error)")
                                } else {
                                    if let places = response?.results() {
                                        let places : [GMSAddress] = places
                                        
                                        
                                        print("ooooooooo","3")
                                        UserDefaults.standard.set(Double((self.locationManager.location?.coordinate.latitude)!), forKey: "latitude")
                                        
                                        
                                        UserDefaults.standard.set(Double((self.locationManager.location?.coordinate.longitude)!), forKey: "longitude")
                                        
                                        let str1 = String((places.first?.thoroughfare) ?? "")
                                        
                                        print("str1",str1)
                                        
                                        let str2 = String( (places.first?.subLocality) ?? ""
                                                           
                                                           
                                        )
                                        print("str2",places.first?.lines)
                                        let str3 = String( (places.first?.locality) ?? ""
                                        )
                                        
                                        print("str3",str3)
                                        
                                        //                                                    let str4 = String( (places.first?.postalCode) ?? ""
                                        
                                        
                                        
                                        
                                        
                                        let str5 = String( (places.first?.administrativeArea) ?? ""
                                        )
                                        
                                        print("str5",str5)
                                        
                                        
                                        //                                                    let str6 = String( (places.first?.locality) ?? ""
                                        
                                        
                                        
                                        
                                        
                                        let str7 = String( (places.first?.country) ?? "")
                                        print("str7",str7)
                                        
                                        UserDefaults.standard.set(str1, forKey: "HOME")
                                        
                                        UserDefaults.standard.set(str1+" , "+str2+" , "+str3, forKey: "STREET")
                                        
                                        UserDefaults.standard.set(str1, forKey: "BUILDING")
                                        
                                        UserDefaults.standard.set(places.first?.lines![0], forKey: "PLACE")
                                        
                                        UserDefaults.standard.set("",forKey: "AHOME")
                                        UserDefaults.standard.set(str1+" , "+str2+" , "+str3,forKey: "ASTREET")
                                        UserDefaults.standard.set("",forKey: "AFLOOR")
                                        UserDefaults.standard.set("",forKey: "AOFFICE")
                                        UserDefaults.standard.set("",forKey: "ADNOTE")
                                        UserDefaults.standard.set("",forKey: "ANICK")
                                        
                                        if(self.ETA == -1){
                                            
                                            self.DeepLinkWithETA(Spid: self.SelectedDriver)
                                            self.DirectAccess = true
                                            //                    self.DirectCart = true
                                        }
                                        //                                                                                                                          self.OpenPartialSheet = false
                                        
                                        
                                        //
                                        //                                                    if let place = places.first {
                                        //
                                        //
                                        //                                                        if let lines = place.lines {
                                        //                                                            print("GEOCODE: Formatted Address: \(lines)")
                                        //
                                        //
                                        //                                                        }
                                        //
                                        //
                                        //
                                        //                                                    } else {
                                        //                                                        print("GEOCODE: nil first in places")
                                        //                                                    }
                                    } else {
                                        print("GEOCODE: nil in places")
                                    }
                                }
                            }
                            //
                            //                                                                                     let georeader = CLGeocoder()
                            //                                                                               georeader.reverseGeocodeLocation(self.locationManager.location!){ (places,err) in
                            //
                            //                                                                                         if err != nil{
                            //
                            //                                                                                             print((err?.localizedDescription)!)
                            //                                                                                             return
                            //                                                                                         }
                            //
                            //
                            //                                               //                                            self.parent.centerLocation = places?.first?.subLocality as! String
                            //                                                                                       //      return places?.first?.locality
                            //                                                                                        //   print(places?.first?.subLocality!)
                            //
                            //
                            //
                            //
                            //
                            //
                            //
                            //                                                                                   UserDefaults.standard.set(Double((self.locationManager.location?.coordinate.latitude)!), forKey: "latitude")
                            //
                            //
                            //                                                                                   UserDefaults.standard.set(Double((self.locationManager.location?.coordinate.longitude)!), forKey: "longitude")
                            //
                            //                                                                                           let str1 = String((places?.first?.name) ?? "")
                            //
                            //
                            //
                            //                                                                                           let str2 = String( (places?.first?.subThoroughfare) ?? ""
                            //                                                                                               )
                            //
                            //                                                                                           let str3 = String( (places?.first?.thoroughfare) ?? ""
                            //                                                                                                                                          )
                            //
                            //
                            //
                            //                                                                                           let str4 = String( (places?.first?.postalCode) ?? ""
                            //                                                                                                                                          )
                            //
                            //
                            //
                            //
                            //                                                                                           let str5 = String( (places?.first?.subLocality) ?? ""
                            //                                                                                                                                          )
                            //
                            //
                            //
                            //
                            //                                                                                           let str6 = String( (places?.first?.locality) ?? ""
                            //                                                                                                                                          )
                            //
                            //
                            //
                            //
                            //                                                                                           let str7 = String( (places?.first?.country) ?? "")
                            //
                            //
                            //                                                                                           UserDefaults.standard.set(str2, forKey: "HOME")
                            //
                            //                                                                                           UserDefaults.standard.set(str5+" , "+str6+" , "+str4+" , "+str7, forKey: "STREET")
                            //
                            //                                                                                           UserDefaults.standard.set(str3, forKey: "BUILDING")
                            //
                            //                                                                                           UserDefaults.standard.set(str5+" , "+str6+" , "+str2+" , "+str3+" , "+str4+" , "+str7, forKey: "PLACE")
                            //
                            //                                                                                                            UserDefaults.standard.set("",forKey: "AHOME")
                            //                                                                                                                                                     UserDefaults.standard.set(str5+" , "+str6+" , "+str4+" , "+str7,forKey: "ASTREET")
                            //                                                                                                                                                      UserDefaults.standard.set("",forKey: "AFLOOR")
                            //                                                                                                                                                   UserDefaults.standard.set("",forKey: "AOFFICE")
                            //                                                                                                                                                   UserDefaults.standard.set("",forKey: "ADNOTE")
                            //                                                                                                 UserDefaults.standard.set("",forKey: "ANICK")
                            //
                            //                                                                                     }
                            
                            
                            //                                            self.placedisplay.toggle()
                            //                                            self.placedisplay.toggle()
                            
                            
                            
                            
                        }
                    }
                    
                    //                if(UserDefaults.standard.array(forKey: "VT") != nil){
                    //                    self.VehicleArray.removeAll()
                    //                for VT in  UserDefaults.standard.array(forKey: "VT")! {
                    //                    let VT = VT as! VehicleItems
                    //                    
                    //                    
                    //                   
                    //                    self.VehicleArray.append(VEHICLETYPES(image: VT.image, name: VT.name, id: VT.id))
                    //                        
                    //                        
                    //                    
                    //                }
                    //                }
                    
                    
                    
                    
                }
                
                
                
            }
            
            
            .environment(\.colorScheme,.light)
            .navigationBarTitle("Booking Details",displayMode: .inline)
            .navigationBarHidden(false)
            
            .navigationBarItems(trailing:  Button(action:{
                withAnimation{
                    self.OpenCart = true
                    self.DirectCart = true
                    recalculateETA()
                    self.presentationMode.wrappedValue.dismiss();
                }
            }){
                
                ZStack{
                    Image("cart").resizable().renderingMode(.template).foregroundColor(.black).frame(width:25,height:25)
                    //
                    if(self.cartFetch.carts.count>0){
                        Text(String(self.cartFetch.carts.count)).font(.custom("Regular_Font".localized(), size: 10))
                            .frame(width: 15.0, height: 15.0)
                            .background(Circle().fill(Color.red))
                            .foregroundColor(.white)
                            .padding([.trailing, .bottom])
                    }
                }
                
                
            })
            .edgesIgnoringSafeArea(.top)
        }
        //        .onAppear{
        //            if(self.Reorder != "0" ){
        //                self.OpenPartialSheet = true
        //            self.FetchPast()
        //                self.FetchLastAddress()
        //            }
        //        }
    }
    
    func DeepLinkWithETA(Spid:String){
        
        
        print("YYYYYYYY","1")
        self.ETA = 100000000
        
        self.ref.child("service_providers").child(Spid).observeSingleEvent(of: DataEventType.value) { (SpSnapshot) in
            print("YYYYYYYY","11")
            
            self.serviceSnap = SpSnapshot
            var count = 0 ;
            if(SpSnapshot.childSnapshot(forPath: "drivers_info/active_drivers_id").childrenCount == 0){
                self.Alert3 = true
            }
            for drivers in SpSnapshot.childSnapshot(forPath: "drivers_info/active_drivers_id").children{
                let drivers = drivers as! DataSnapshot
                
                
                print("YYYYYYYY","111")
                self.ref.child("drivers").child(drivers.key).observeSingleEvent(of: DataEventType.value, with: { (driversnap) in
                    print("YYYYYYYY","4")
                    count += 1 ;
                    
                    //                                        if(String(describing :  (driversnap.childSnapshot(forPath:"status").value)!) == "1"){
                    
                    //                            print("PPPPPPPPPP",driversnap.key)
                    //        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5)) {
                    
                    var driverlat =
                        25.0
                    
                    var driverlon = 55.635908
                    var lastETA : Double = 0
                    
                    if(String(describing:  driversnap.childSnapshot(forPath: "live_location/time_driver_engaged").value!) != "0"){
                        
                        lastETA = Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/time_driver_engaged").value)!)) ?? 0
                        
                        
                        
                        
                        
                        
                        driverlat =
                            Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/lc_lat").value)!)) ?? 25.0
                        
                        //  }
                        
                        
                        
                        //   if(driversnap.childSnapshot(forPath:"live_location/longitude").exists()){
                        driverlon =
                            Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/lc_lng").value)!)) ?? 55
                        
                        
                    }else{
                        
                        print("QQQQQQQQQQ2",driversnap.key)
                        driverlat =
                            Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/latitude").value)!)) ?? 25.0
                        
                        //  }
                        
                        
                        
                        //   if(driversnap.childSnapshot(forPath:"live_location/longitude").exists()){
                        driverlon =
                            Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/longitude").value)!)) ?? 55
                        
                    }
                    
                    
                    //  }
                    var driverloc : CLLocation = CLLocation(latitude: driverlat, longitude: driverlon)
                    var driver2D : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: driverlat, longitude: driverlon)
                    
                    let userlat = UserDefaults.standard.double(forKey: "latitude")
                    let userlon = UserDefaults.standard.double(forKey: "longitude")
                    
                    
                    var userloc : CLLocation = CLLocation(latitude: userlat, longitude: userlon)
                    var user2D  : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: userlat, longitude: userlon)
                    
                    
                    //ETA USING APPLE MAP
                    //
                    //                        let request = MKDirections.Request()
                    //                 request.source = MKMapItem(placemark:  MKPlacemark(coordinate: driver2D))
                    //                 request.destination = MKMapItem(placemark:  MKPlacemark(coordinate: user2D))
                    //
                    //                 request.requestsAlternateRoutes = false
                    
                    //   let directions = MKDirections(request: request)
                    //                    directions.calculateETA { (responds, error) in
                    //                         if error != nil {
                    //                                            print("Error getting directions")
                    //                                        } else {
                    //                            var ETA  =    responds?.expectedTravelTime
                    //
                    //
                    //
                    //                        }
                    //                    }
                    //                        directions.calculateETA{ response, error in
                    //
                    //
                    //
                    //
                    //                   //     directions.calculate { response, error in
                    //                                var eta:Double = 10000000
                    //                            if(error != nil){
                    //                                print(error.debugDescription)
                    //                            }else{
                    //
                    //                                    eta = response?.expectedTravelTime as! Double
                    
                    
                    var eta:Double = 10000000
                    //   eta = getETA(from: user2D, to: driver2D)
                    
                    //                        let routes = response?.routes
                    
                    
                    
                    
                    //                        if(routes != nil){
                    //                        let selectedRoute = routes![0]
                    //                        let distance = selectedRoute.distance
                    //                         eta = selectedRoute.expectedTravelTime
                    //
                    //
                    //                        }
                    print("cccccc","4")
                    
                    
                    let origin = "\(driver2D.latitude),\(driver2D.longitude)"
                    let destination = "\(user2D.latitude),\(user2D.longitude)"
                    
                    
                    
                    //guard
                    
                    let S : String =  "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=\(origin)&destinations=\(destination)&mode=driving&key=AIzaSyCYQeItZ7SVEQ24Grjzc3zA1B_2DU_GfPU"
                    
                    let url = URL(string:S
                    )
                    //        else {
                    //        let error = NSError(domain: "LocalDomain", code: 0, userInfo: [NSLocalizedDescriptionKey: "Failed to create object URL"])
                    //        print("Error: \(error)")
                    //        return
                    //    }
                    let config = URLSessionConfiguration.default
                    let session = URLSession(configuration: config)
                    let task = session.dataTask(with: url!, completionHandler: {
                        (data, response, error) in
                        
                        print("11111111",driversnap.key)
                        
                        if error != nil {
                            print("cccccc","5")
                            print(error!.localizedDescription)
                        }
                        else {
                            print("cccccc","6")
                            //  print(response)
                            do {
                                if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                                    
                                    guard let routes = json["rows"] as? NSArray else {
                                        DispatchQueue.main.async {
                                        }
                                        return
                                    }
                                    if (routes.count > 0) {
                                        let overview_polyline = routes[0] as? NSDictionary
                                        //   let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                                        //  let points = dictPolyline?.object(forKey: "points") as? String
                                        
                                        DispatchQueue.main.async {
                                            
                                            print("cccccc","7")
                                            //
                                            let legs = overview_polyline?["elements"] as! Array<Dictionary<String, AnyObject>>
                                            
                                            let distance = legs[0]["distance"] as? NSDictionary
                                            let distanceValue = distance?["value"] as? Int ?? 0
                                            let distanceDouleValue = distance?["value"] as? Double ?? 0.0
                                            let duration = legs[0]["duration"] as? NSDictionary
                                            let totalDurationInSeconds = duration?["value"] as? Int ?? 0
                                            let durationDouleValue = duration?["value"] as? Double ?? 0.0
                                            
                                            print (S)
                                            
                                            eta = durationDouleValue+300 ;
                                            
                                            if((lastETA-(Date().timeIntervalSince1970)*1000) > 0){
                                                eta = eta+(((lastETA/1000)-(Date().timeIntervalSince1970)))
                                                print("WWWWW",(((lastETA/1000)-(Date().timeIntervalSince1970))/60))
                                                
                                                
                                            }
                                            
                                            
                                            
                                            eta = eta+300 ;
                                            
                                            if(eta < 301){
                                                eta = 10000000
                                            }
                                            
                                            
                                            if(eta<self.ETA){
                                                self.ETA = eta
                                                self.SelectedDriver = drivers.key
                                                
                                            }
                                            //
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                        }
                                    }
                                    else {
                                        print(json)
                                        DispatchQueue.main.async {
                                            //   SVProgressHUD.dismiss()
                                        }
                                    }
                                }
                            }
                            catch {
                                print("error in JSONSerialization")
                                DispatchQueue.main.async {
                                    //  SVProgressHUD.dismiss()
                                }
                            }
                        }
                    })
                    task.resume()
                    
                    
                    //                                    }
                })
            }
        }
        
        
        
        
        
        
        
        
    }
    func getColor(Rate :String) -> String {
        
        
        let  Rato : Double = (Rate as NSString).doubleValue
        print(Rato)
        
        if(Rato >= 5){ return "morethanfour"}
        else if (Rato >= 4){ return "morethanthree"}
        else if (Rato >= 3){ return "morethantwo"}
        else if (Rato >= 2){ return "morethanone"}
        else { return "morethanone"}
        
        
        
    }
    //    func FetchLastAddress() {
    //        self.ref.child("orders").child("all_orders").child(self.Reorder).observeSingleEvent(of: DataEventType.value) { (datasnap) in
    //
    ////            print("LLLLLLL",self.Reorder,String(describing :  (datasnap.childSnapshot(forPath:"customer_raw_address").value)!))
    //
    //            self.LastAddress = ["Address":String(describing :  (datasnap.childSnapshot(forPath:"customer_raw_address").value)!)
    //                , "latitude": String(describing :  (datasnap.childSnapshot(forPath:"customer_latitude").value)!),"longitude":String(describing :  (datasnap.childSnapshot(forPath:"customer_longitude").value)!)]
    //
    //        }
    //    }
    
    
    func getSubTotal() -> String {
        var SubTotal:Double = 0.0
        
        
        
        //   var cartSum:Double = 0.0
        
        var sumOfServices:Double = 0.0
        
        for service in selectedServices {
            
            sumOfServices += Double(service.ServicePrice)!
        }
        
        var sumOfAddons:Double = 0.0
        
        for Addons in selectedAddons {
            
            sumOfAddons += Double(Addons.addonPrice)!
        }
        SubTotal += sumOfServices+sumOfAddons
        
        
        return String(format: "%.1f",SubTotal )
        
    }
    func FetchPast() {
        self.ref.child("Users").child(
            
            UserDefaults.standard.string(forKey: "Aid") ?? "0"
        ).child("past_orders").child("order_ids").observe(DataEventType.value) { (datasnapshot) in
            
            self.PastOrders.removeAll()
            for P in datasnapshot.children {
                let pastsnap = P as! DataSnapshot
                
                self.ref.child("orders").child("all_orders").child(pastsnap.key).observeSingleEvent(of: DataEventType.value) { (osnap) in
                    
                    self.ref.child("service_providers").child(String(describing :  (osnap.childSnapshot(forPath:"sp_id").value)!)).observeSingleEvent(of: DataEventType.value) { (snapshot) in
                        //    print(String(describing :  (snapshot.childSnapshot(forPath:"image").value)!))
                        
                        
                        
                        let milisecond:Int = Int(String(describing :  (osnap.childSnapshot(forPath:"time_order_placed").value)!)) ?? 0;
                        //                        print("KKKKKK",milisecond)
                        let dateVar = Date.init(timeIntervalSince1970: TimeInterval(milisecond)/1000)
                        var dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd-MM-yyyy"
                        dateFormatter.locale = Locale(identifier: UserDefaults.standard.string(forKey: "LANSEL") ?? "en")
                        print(dateFormatter.string(from: dateVar))
                        
                        
                        let pO : pastOrderSet = pastOrderSet(id: snapshot.key, driverID: String(describing :  (osnap.childSnapshot(forPath:"driver_id").value)!), SpName: String(describing :  (snapshot.childSnapshot(forPath:"personal_information/name".localized()).value)!), Date: dateFormatter.string(from: dateVar), Image: String(describing :  (snapshot.childSnapshot(forPath:"personal_information/image").value)!),snap: snapshot,Address:String(describing :  (osnap.childSnapshot(forPath:"customer_raw_address").value)!),latitude: String(describing :  (osnap.childSnapshot(forPath:"customer_latitude").value)!),longitude:String(describing :  (osnap.childSnapshot(forPath:"customer_longitude").value)!), OrderSnap: osnap )
                        //                        print("ZZZZZZZZZZZZ",pO.SpName,snapshot.)
                        
                        
                        self.PastOrders.append(pO)
                        
                    }
                }
                
            }
            
        }
    }
    
    
    //    struct ForCart {
    //        var id : Int = 0
    //    var VName : String = ""
    //    var Vid :String = ""
    //    var selectedService :Services = Services(id: "", serviceName: "", serviceDesc: "", ServiceImage: "", ServicePrice: "", ServiceTime: "")
    //    var selectedAddons : Addons = Addons(id: "", addonName: "", addonDesc: "", addonImage: "", addonPrice: "")
    //    }
}

//struct BookingDetails_Previews: PreviewProvider {
//    static var previews: some View {
//        BookingDetails()
//    }
//}

func displaySum(selectedServices:[Services], selectedAddons:[Addons]) -> String {
    
    var sumOfServices:Double = 0.0
    
    for service in selectedServices {
        
        sumOfServices += Double(service.ServicePrice)!
    }
    
    var sumOfAddons:Double = 0.0
    
    for Addons in selectedAddons {
        
        sumOfAddons += Double(Addons.addonPrice)!
    }
    
    return String(format: "%.1f", sumOfServices+sumOfAddons)
    
    
    
}

struct ETAAndDriver{
    
    var ETA:Double
    var DriverID :String
}
func removedupicates(bookings:[Bookings]) -> [loclist] {
    
    var locations :  [loclist] = [loclist]()
    for book in bookings{
        
        let l:loclist = loclist(location:book.ServiceLocation,latitude:book.latitude,longitude:book.longitude)
        
        locations.append(l)
    }
    
    let reduce = locations.reduce(into:[:],{ $0[$1, default:0] += 1})
    let sorted = reduce.sorted(by: {$0.value > $1.value})
    let map = sorted.map({$0.key})
    return map
}
struct loclist:Hashable {
    
    var location : String
    var latitude :String
    var longitude :String
    
    
}

func Loc(key:String) -> String {
    return NSLocalizedString(key, comment: "")
}

struct DataS : Equatable{
    var id : Int = 0
    var VName : String = ""
    var Vid :String = ""
    var selectedService :Services = Services(id: "", serviceName: "",serviceNameO: "", serviceDesc: "",serviceDescO: "", ServiceImage: "", ServicePrice: "", ServiceTime: "")
    //        var selectedAddons : Addons = Addons(id: "", addonName: "", addonDesc: "", addonImage: "", addonPrice: "")
}
struct DataV : Equatable{
    var id : Int = 0
    var VName : String = ""
    var Vid :String = ""
    
    //        var selectedAddons : Addons = Addons(id: "", addonName: "", addonDesc: "", addonImage: "", addonPrice: "")
}
struct DataA : Equatable{
    var id : Int = 0
    var VName : String = ""
    var Vid :String = ""
    //          var selectedService :Services = Services(id: "", serviceName: "", serviceDesc: "", ServiceImage: "", ServicePrice: "", ServiceTime: "")
    var selectedAddons : Addons = Addons(id: "", addonName: "",addonNameO: "", addonDesc: "", addonDescO: "", addonImage: "", addonPrice: "")
}
