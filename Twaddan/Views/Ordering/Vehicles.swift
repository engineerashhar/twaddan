//
//  Vehicles.swift
//  Twaddan
//
//  Created by Spine on 29/07/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import FirebaseDatabase
import SDWebImageSwiftUI

struct Vehicles: View {
    @EnvironmentObject var vehicleItems : VehicleItems
    let ref = Database.database().reference()
    
    
    @Binding var VEHICLES : [VEHICLETYPES]
    @State var VEHICLESList : [VEHICLETYPES] =  [VEHICLETYPES] ()
    var body: some View {
        
        
        VStack{
            
            
            ForEach(self.VEHICLESList,id: \.self) { (V) in
                
                
                
                
                RoundedCorners(color: Color.white, tl: 100, tr: 0, bl: 0, br: 0).frame(width:UIScreen.main.bounds.width*0.75,height:65)
                    .padding(.horizontal).padding(.vertical,1).overlay( HStack {
                        
                        Spacer().frame(width:25)
                        HStack{
                            AnimatedImage(url: URL(string: V.image)) .resizable().padding(10)
                                .overlay(Circle().stroke(Color("h8"),lineWidth: 2)).frame(width:44,height:44)
                            
                            Text(V.name).font(.custom("Bold_Font".localized(), size: 13)).foregroundColor(Color("h2")
                            )
                            Spacer()
                            
                            
                            
                            HStack(spacing:2){
                                
                                Button(action:{
                                    if(!self.VEHICLES.contains(V)){
                                        //                                      self.VEHICLES.append(V)
                                    }else{
                                        self.VEHICLES.remove(at: self.VEHICLES.firstIndex(of: V)!)
                                        //                                            self.vehicleItems.vehicleItem = AddVehicleData(VEHICLES: self.VEHICLES)
                                        
                                        
                                    }
                                }){
                                    ZStack{
                                        RoundedCornersStroke(color: Color("h1"), tl: 20, tr: 0, bl: 0, br: 0,lineWidth:1).frame(width:31,height:31)
                                        
                                        Image(systemName: "minus").resizable().frame(width:12.5,height:1.5).foregroundColor(Color("c1")).padding(5)
                                        
                                    }
                                }
                                
                                ZStack{
                                    RoundedCornersStroke(color: Color("h1"), tl: 0, tr: 0, bl: 0, br: 0,lineWidth:1).frame(width:31,height:31)
                                    
                                    Text(LocNumbers(key:  String(self.VEHICLES.filter{$0 == V}.count))).font(.custom("Regular_Font".localized(), size: 18))
                                    
                                }
                                
                                Button(action:{
                                    
                                    self.VEHICLES.append(V)
                                    //                                        self.vehicleItems.vehicleItem = AddVehicleData(VEHICLES: self.VEHICLES)
                                    
                                }){
                                    
                                    ZStack{
                                        RoundedCornersStroke(color: Color("h1"), tl: 0, tr: 0, bl: 0, br:100,lineWidth:1).frame(width:31,height:31)
                                        
                                        Image(systemName: "plus").resizable().frame(width:12.5,height:12.5).foregroundColor(Color("c1")).padding(5)
                                        
                                    }
                                    
                                }
                            }.environment(\.layoutDirection, .leftToRight  )
                            
                            
                            //                  Image(systemName: self.VEHICLES.contains(V) ? "checkmark.circle.fill" : "" )
                            //                                                                                                                                                                .resizable()
                            //
                            //                                                                                                                                                                   .frame(width: 25, height: 24)
                            
                            //                      .foregroundColor(Color("green")).padding(.trailing)
                        }
                        Spacer().frame(width:25)
                        
                    })
                
                
                
                
            }
            //            HStack{
            //            Image("Image 8").resizable().padding(10)
            //                          .overlay(Circle().stroke(Color("h8"),lineWidth: 2)).frame(width:44,height:44)
            //
            //                      Text("saloon").font(.custom("Bold_Font".localized(), size: 13)).foregroundColor(Color("h2")
            //                )
            //                    Spacer()
            //            }.padding(.horizontal).padding(.vertical,7).frame(width:260).background(Color.white).cornerRadius(20).clipped()
            
            
        }.padding().background(Color.clear)
        .onAppear{
            
            
            self.ref.child("twaddan_admin").child("vehicle_type").observeSingleEvent(of: DataEventType.value) { (dataSnapshot) in
                self.VEHICLESList.removeAll()
                
                for V in dataSnapshot.children{
                    let V = V as! DataSnapshot
                    let S = VEHICLETYPES(id: Int.random(in: 0 ... 10000000000), image: String(describing:(V.childSnapshot(forPath:"image").value)!), name: String(describing:(V.childSnapshot(forPath:"name".localized()).value)!), index: Double(String(describing:(V.childSnapshot(forPath:"index").value)!)) ?? 0.0, Vid: V.key)
                    
                    self.VEHICLESList.append(S)
                    
                    self.VEHICLESList.sort {
                        $0.index < $1.index
                    }
                }
                
            }
        }
        
    }
}

struct Vehicles_Previews: PreviewProvider {
    static var previews: some View {
        Vehicles(VEHICLES: .constant([VEHICLETYPES] ()))
    }
}

struct VEHICLETYPES :Hashable,Identifiable {
    var id : Int
    var image : String
    var name : String
    var index : Double
    var Vid : String
}
