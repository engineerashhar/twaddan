//
//  ServiceProviderPage.swift
//  Twaddan
//
//  Created by Spine on 10/05/20.
//  Copyright © 2020 spine. All rights reserved.
//


import SDWebImageSwiftUI
import SwiftUI
import FirebaseDatabase
import FirebaseDynamicLinks
struct ServiceProviderPage: View {
    
    let ref = Database.database().reference()
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var reviewSnap : [DataSnapshot] = [DataSnapshot]()
    @EnvironmentObject var vehicleItems : VehicleItems
    @State var GoToBookDetails : Bool = false
    @State var OpenCart : Bool = false
    //   @Binding  var OpenCart : Bool
    //   @Binding  var GoToBookDetails : Bool
    @State var openVA : Bool = false
    @State var openSPD : Bool = false
    var serviceSnap : DataSnapshot
    //  var SelectedDriver : String
    
    var ETA : Double
    var Driver : String
    //  @ObservedObject var cartFetch = CartFetch()
    
    //       @State var ALERT1 : Bool = false
    //       @State var ALERT1MESSAGE :String = ""
    //
    //
    //       @State var ALERT2 : Bool = false
    //       @State var ALERT2MESSAGE :String = ""
    
    
    //       @State var goToMap : Bool = false
    
    //         @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    //       @ObservedObject  var cartFetch = CartFetch()
    //         @EnvironmentObject var settings : UserSettings
    //  var serviceSnaspshot : Datasnapshot
    
    
    
    @State var selectedVType : String = "saloon"
    @State var VTypeBool : Bool = false
    @State var shareSheetStatus : Bool = false
    //       @State var StreetAddress : String = ""
    // @State var VType : [String] = ["saloon", "SUV", "Super SUV", "Caravan","Bike"]
    //       @State var VType
    //             : [String] = ["saloon", "suv", "super_suv", "caravan","bike"]
    @State var selectedServices : [Services] = [Services]()
    @State var selectedAddons : [Addons] = [Addons]()
    
    
    @Binding var GoButton : Bool
    
    
    
    
    var providersFetch = getServiceAndAddons()
    
    
    var body: some View {
        //  Text("Detail")
        
        
        
        
        GeometryReader{ geometry in
            
            ZStack{
                ZStack(alignment:.trailing){
                    
                    
                    
                    NavigationLink(destination: Cart(GoToBookDetailsOld: .constant(false))
                                    
                                    .environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                                    )), isActive: self.$OpenCart){
                        
                        Text("").frame(height:0)
                    }
                    
                    
                    
                    
                    //First Layer
                    
                    
                    
                    
                    VStack(){
                        
                        //                     Text("Hhh")
                        //                        AnimatedImage(url : URL (string: String(describing: self.serviceSnap.childSnapshot(forPath:"personal_information/image").value ?? "")))
                        //
                        
                        //            Spacer().frame(height:UIScreen.main.bounds.height*0.1)
                        ZStack{
                            
                            
                            AnimatedImage(url : URL (string: String(describing: self.serviceSnap.childSnapshot(forPath:"personal_information/image").value ?? "")))
                                //                            Image("basicbackimage")
                                .resizable()
                                
                                .frame(width:UIScreen.main.bounds.width,height: UIScreen.main.bounds.width)
                            
                            
                            Rectangle()                         // Shapes are resizable by default
                                .foregroundColor(.clear)        // Making rectangle transparent
                                .background(LinearGradient(gradient: Gradient(colors: [.clear, Color("ColorTea")]), startPoint: .top, endPoint: .bottom))
                                .frame(width:UIScreen.main.bounds.width,height: UIScreen.main.bounds.width)
                        }.overlay(
                            
                            HStack(alignment:.bottom){
                                VStack(alignment:.leading)
                                {
                                    Spacer()
                                    VStack(alignment:.leading){
                                        //                                    Text("Spine Car Wash")
                                        Text((String(describing: self.serviceSnap.childSnapshot(forPath:"personal_information/name".localized()).value ?? "")))
                                            .font(.custom("ExtraBold_Font".localized(), size: 22))
                                            .foregroundColor(.white)
                                        
                                        
                                        //                                          Text("Parvath Arcade Malappuram")
                                        Text((String(describing: self.serviceSnap.childSnapshot(forPath:"personal_information/name".localized()).value ?? "")))
                                            .font(.custom("Regular_Font".localized(), size: 15))
                                            .foregroundColor(.white)
                                        
                                    }
                                }
                                Spacer()
                                
                                Button(action:{
                                    self.shareSheetStatus.toggle()
                                    
                                    //                                    let url = URL(string: "Twaddan://profile?spid=\(self.serviceSnap.key)")
                                    //                                    let av = UIActivityViewController(activityItems: [url!], applicationActivities: nil)
                                    //                                    UIApplication.shared.windows.first?.rootViewController?.present(av,animated: true,completion: nil)
                                    
                                    guard let link = URL(string: "https://twaddan.com/?sp=\(self.serviceSnap.key)") else { return }
                                    let dynamicLinksDomainURIPrefix = "https://twaddan7.page.link"
                                    let linkBuilder :DynamicLinkComponents = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix)!
                                    
                                    linkBuilder.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.spine.twaddan7")
                                    linkBuilder.iOSParameters!.appStoreID = "1525130644"
                                    
                                    
                                    
                                    //                                    linkBuilder.iOSParameters.minimumAppVersion = "1.2.3"
                                    
                                    linkBuilder.androidParameters = DynamicLinkAndroidParameters(packageName: "com.twaddan.app")
                                    //                                    linkBuilder.androidParameters.minimumVersion = 123
                                    
                                    //                                    linkBuilder.analyticsParameters = DynamicLinkGoogleAnalyticsParameters(source: "orkut",
                                    //                                                                                                           medium: "social",
                                    //                                                                                                           campaign: "example-promo")
                                    //
                                    //                                    linkBuilder.iTunesConnectParameters = DynamicLinkItunesConnectAnalyticsParameters()
                                    //                                    linkBuilder.iTunesConnectParameters.providerToken = "123456"
                                    //                                    linkBuilder.iTunesConnectParameters.campaignToken = "example-promo"
                                    
                                    linkBuilder.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
                                    linkBuilder.socialMetaTagParameters!.title = self.serviceSnap.key
                                    linkBuilder.socialMetaTagParameters!.descriptionText = (String(describing: self.serviceSnap.childSnapshot(forPath:"personal_information/description".localized()).value ?? ""))
                                    linkBuilder.socialMetaTagParameters!.imageURL = URL(string: (String(describing: self.serviceSnap.childSnapshot(forPath:"personal_information/image").value ?? "")))
                                    
                                    guard let longDynamicLink = linkBuilder.url else { return }
                                    
                                    //                                    let av = UIActivityViewController(activityItems: [longDynamicLink], applicationActivities: nil)
                                    //                                                                                                                                                  UIApplication.shared.windows.first?.rootViewController?.present(av,animated: true,completion: nil)
                                    
                                    linkBuilder.shorten() { url, warnings, error in
                                        
                                        print("The short URL is: \(error)")
                                        print("The short URL is New: \(url)")
                                        //                                      guard let url = url, error != nil else { return }
                                        
                                        
                                        let av = UIActivityViewController(activityItems: [url], applicationActivities: nil)
                                        UIApplication.shared.windows.first?.rootViewController?.present(av,animated: true,completion: nil)
                                    }
                                    let url = URL(string: "Twaddan://profile?spid=\(self.serviceSnap.key)")
                                    
                                }){
                                    ZStack{
                                        Rectangle().cornerRadius(10).foregroundColor(Color("c1")).frame(width:42,height: 42).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color("s2"),lineWidth: 0.5))
                                        Image("share")
                                            .resizable().renderingMode(.template).foregroundColor(Color.white)
                                            .frame(width: 23, height: 20)
                                        
                                    }
                                    
                                }
                                
                            }.padding()
                            
                            
                        )
                        
                        
                        //                        Text("Opening Hours : Sat-Thur (*:00-20:00)").font(.custom("Regular_Font".localized(), size: 16)).padding()
                        
                        
                        
                        
                        
                        //         RoundedCorners(color: Color("backcolor2"), tl: 0, tr: 0, bl: 60, br: 60)
                        //       .frame(width:geometry.size.width,height: geometry.size.height*0.25)
                        //        .background(Color("backcolor2"))
                        //  .foregroundColor(Color("ManBackColor"))
                        
                        ScrollView{
                            VStack{
                                HStack{
                                    
                                    Group{
                                        //                                                                    Text("3.3")
                                        Text(LocNumbers(key:  String(String(describing: self.serviceSnap.childSnapshot(forPath:"personal_information/ratings").value  ?? "").prefix(3))))
                                            .font(.custom("ExtraBold_Font".localized(), size: 31))
                                            .foregroundColor(.black)
                                        
                                        VStack{
                                            
                                            
                                            RatingLargeYello(Value: self.serviceSnap.childSnapshot(forPath:"personal_information/ratings").value as! Double)
                                            //                                                                        self.serviceSnap.childSnapshot(forPath:"personal_information/ratings").value as! Double
                                            //                                       } Text("Reviews").font(.custom("Regular_Font".localized(), size: 17))
                                            //                                            .foregroundColor(.white).hidden()
                                            //OOOOOOOOOOO
                                            
                                            
                                            HStack{
                                                Text("Based on".localized())
                                                Text(" \(LocNumbers(key:String( self.reviewSnap.count)))" + " " + "Reviews".localized())
                                                    .font(.custom("Regular_Font".localized(), size: 15)).foregroundColor(Color("s1"))
                                                
                                            }
                                        }.padding(.leading)
                                    }
                                    
                                }.padding(.top,30)
                                
                                
                                VStack{
                                    
                                    HStack{
                                        Text("Value of Service").font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color("s1"))
                                        Spacer()
                                        
                                        RatingLargeYello(Value: 3)
                                        
                                    }
                                    
                                    HStack{
                                        Text("Arrival timing").font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color("s1"))
                                        Spacer()
                                        
                                        RatingLargeYello(Value: 3)
                                        
                                    }
                                    
                                    
                                    HStack{
                                        Text("Quality of Service").font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color("s1"))
                                        Spacer()
                                        
                                        RatingLargeYello(Value: 3)
                                        
                                    }
                                    
                                }.padding(.horizontal,60)
                                
                                
                                
                                VStack{
                                    
                                    HStack{
                                        Text("What people are saying")
                                        Spacer()
                                    }
                                    
                                    //                                LazyVStack
                                    if #available(iOS 14.0, *) {
                                        LazyVStack(alignment:.leading){
                                            
                                            ForEach(self.reviewSnap.reversed(),id: \.self){ snap in
                                                
                                                
                                                VStack(alignment:.leading){
                                                    HStack{
                                                        RatingLargeYello(Value:snap.childSnapshot(forPath:"rating").value as! Double)
                                                        Spacer()
                                                        Text(self.getDate(timeinmilli:snap.childSnapshot(forPath:"timestamp").value as! Double)).font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color("s1"))
                                                    }
                                                    
                                                    Text(String(describing: snap.childSnapshot(forPath:"review").value! )).font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color("s1")).fixedSize(horizontal: false, vertical: true)
                                                        .lineLimit(5)
                                                    Text(String(describing: snap.childSnapshot(forPath:"customerName").value! )).font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color("s1")).lineLimit(1).padding(.top,10)
                                                    
                                                }.padding()
                                                
                                            }
                                            
                                            
                                            
                                            
                                            
                                        }
                                    } else {
                                        VStack(alignment:.leading){
                                            
                                            ForEach(self.reviewSnap.reversed(),id: \.self){ snap in
                                                
                                                
                                                VStack(alignment:.leading){
                                                    HStack{
                                                        RatingLargeYello(Value:snap.childSnapshot(forPath:"rating").value as! Double)
                                                        Spacer()
                                                        Text(self.getDate(timeinmilli:snap.childSnapshot(forPath:"timestamp").value as! Double)).font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color("s1"))
                                                    }
                                                    
                                                    Text(String(describing: snap.childSnapshot(forPath:"review").value! )).font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color("s1")).fixedSize(horizontal: false, vertical: true)
                                                        .lineLimit(5)
                                                    Text(String(describing: snap.childSnapshot(forPath:"customerName").value! )).font(.custom("Regular_Font".localized(), size: 16)).foregroundColor(Color("s1")).lineLimit(1).padding(.top,10)
                                                    
                                                }.padding()
                                                
                                            }
                                            
                                            
                                            
                                            
                                            
                                        }
                                    }
                                    
                                    
                                    
                                }.padding().background(Color.white).cornerRadius(10).clipped().shadow( radius: 10).padding()
                            }
                            Spacer()
                            
                        }
                        
                        //                        Spacer().frame(height:30)
                        
                        
                    }
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                }
                
                
                
                
                VStack{
                    Spacer()
                    
                    Button(action:{
                        
                        if(self.GoButton){
                            self.GoToBookDetails = true
                            
                        }else{
                            self.presentationMode.wrappedValue.dismiss()
                        }
                        
                        
                    }){
                        RoundedPanel()
                            
                            .overlay(
                                
                                HStack{
                                    
                                    Text("Order from this provider").font(.custom("ExtraBold_Font".localized(), size: 18))
                                        .foregroundColor(.white)
                                    
                                    
                                    
                                }
                            )
                        
                    }.sheet(isPresented: self.$GoToBookDetails){
                        
                        
                        
                        BookingDetails(OpenCart: self.$OpenCart,GoToBookDetails:self.$GoToBookDetails ,serviceSnap: self.serviceSnap, SelectedDriver:
                                        self.Driver,ETA: self.ETA, openVA: self.$openVA).environmentObject(self.vehicleItems).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                                        ))
                        
                        
                        
                    }
                    
                    
                }
                
                //                                    .navigationBarTitle("yftyugh")
                //                                    .navigationBarHidden(true)
            }.frame(width:geometry.size.width)
            .background(Color("h1"))
            
            .edgesIgnoringSafeArea(.top)
            .navigationBarTitle("",displayMode: .inline)
            //                    .navigationBarHidden(true)
            
        }
        .onAppear{
            
            self.ref.child("service_providers_ratings").child(self.serviceSnap.key).observeSingleEvent(of:DataEventType.value, with: { (dataSnapshot) in
                for R in dataSnapshot.children
                {
                    self.reviewSnap.append(R as! DataSnapshot)                    }
                
            })
            
        }
        
        
        
    }
    
    func getDate(timeinmilli:Double) -> String {
        let milisecond = timeinmilli
        //                                                                    print("KKKKKK",milisecond)
        let dateVar = Date.init(timeIntervalSince1970: TimeInterval(milisecond)/1000)
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.locale = Locale(identifier: UserDefaults.standard.string(forKey: "LANSEL") ?? "en")
        //                                                                    print(dateFormatter.string(from: dateVar))
        return UnLocNumbers(key: dateFormatter.string(from: dateVar))
    }
}
struct ServiceProviderPage_Previews: PreviewProvider {
    static var previews: some View {
        ServiceProviderPage(serviceSnap: DataSnapshot(), ETA: 100, Driver: "", GoButton: .constant(true))
    }
}



struct RatingLargeYello: View {
    
    var Value :Double = 0
    var body: some View {
        
        //
        //
        HStack{
            Image(systemName: self.Value >= 1.0 ? "star.fill":"star" ).foregroundColor(
                Color.yellow
            )
            
            Image(systemName: self.Value >= 2.0 ? "star.fill":"star" ).foregroundColor(
                Color.yellow
            )
            
            Image(systemName: self.Value >= 3.0 ? "star.fill":"star" ).foregroundColor(
                Color.yellow
            )
            
            
            Image(systemName: self.Value >= 4.0 ? "star.fill":"star" ).foregroundColor(
                Color.yellow
            )
            Image(systemName: self.Value >= 5.0 ? "star.fill":"star" ).foregroundColor(
                Color.yellow
            )
            
        }
        
    }
}






