//
//  EditFromCart.swift
//  Twaddan
//
//  Created by Spine on 17/07/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI
import Firebase
import FirebaseDatabase

struct EditFromCart: View {
    
    @EnvironmentObject var settings : UserSettings
    @Binding   var Cart : CartStruct
    @State   var NewCart : CartStruct = CartStruct(id: "", addons: [Addons](), services: [Services](), vehicle: "", vehicleName: "")
    @Binding var selectedServiceProvider : SelectedSP
    let ref = Database.database().reference()
    
    var providersFetch = getServiceAndAddons()
    
    var body: some View {
        
        
        
        GeometryReader{ geometry in
            
            ZStack{
                
                
                
                Group{
                    //First Layer
                    
                    VStack{
                        Rectangle()
                            .fill(Color("backcolor2"))
                            
                            .frame(width:geometry.size.width,height: 100)
                            .cornerRadius(20)
                        
                        
                        //         RoundedCorners(color: Color("backcolor2"), tl: 0, tr: 0, bl: 60, br: 60)
                        //       .frame(width:geometry.size.width,height: geometry.size.height*0.25)
                        //        .background(Color("backcolor2"))
                        //  .foregroundColor(Color("ManBackColor"))
                        
                        
                        
                        
                        Spacer()
                        
                        ScrollView{
                            
                            ZStack {
                                
                                // Spacer().frame(width:20)
                                HStack(spacing:0){
                                    
                                    //                                                        Image("basicbackimage")
                                    AnimatedImage(url : URL (string: self.selectedServiceProvider.ServiceProvider_image))
                                        .resizable() .cornerRadius(5)
                                        .frame(width: 93, height: 108)
                                        .padding(8)
                                    
                                    
                                    
                                    
                                    
                                    VStack(alignment: .leading){
                                        
                                        
                                        
                                        
                                        Text(String(describing: self.selectedServiceProvider.ServiceProvider))
                                            .foregroundColor(Color("darkthemeletter"))
                                            .font(.custom("Bold_Font".localized(), size: 14))
                                            .frame(height:17)
                                        //                            .padding(0)
                                        
                                        //                                                                  .frame(height:30)
                                        //                            .padding(.top,3)
                                        
                                        Spacer()
                                        Divider()
                                        
                                        HStack{
                                            
                                            
                                            //
                                            //                                                                                                                        Image("prefix__start_1").renderingMode(.template).resizable()
                                            //                                                                                                                            .frame(width: 10, height: 10).padding(.all,7)
                                            //                                                                                                                            .foregroundColor(Color.white)
                                            //                                                                                                                            .background(Color("destbackgreen")).clipShape(Circle())
                                            //
                                            Text(self.selectedServiceProvider.ETA<10000000 ? getTime(timeIntervel: self.selectedServiceProvider.ETA) : "Calculating")
                                                //                                                                                        Text("Within 1 hour 30 minutes") fA
                                                .font(.custom("Regular_Font".localized(), size: 12))
                                                .foregroundColor(Color("darkthemeletter"))
                                            
                                            Spacer()
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                        }
                                        //                        Spacer()
                                        
                                    }.padding(8)
                                    //                        .background(Color.red)
                                    
                                    
                                }
                                
                                .background(Color.white)
                                .cornerRadius(5)
                                .shadow(radius: 2)
                                .frame(height:124)
                                .padding(.all,10).padding(.top,20)
                                // Spacer().frame(width:20)
                                
                            }.frame(height:124)
                            
                            
                            VStack{
                                Spacer()
                                //Service Provider Summary
                                //                                    HStack{
                                //
                                //                                        Spacer().frame(width:20)
                                //                                        HStack{
                                //
                                //                                            //Image("basicbackimage")
                                //                                            AnimatedImage(url : URL (string: self.selectedServiceProvider.ServiceProvider_image))
                                //                                                .resizable()
                                //                                                .padding(.all, 5)                                                           .frame(width: 100, height: 100)
                                //
                                //                                                .cornerRadius(5)
                                //                                                .padding(.trailing, 15.0)
                                //
                                //
                                //                                            VStack(alignment: .leading){
                                //
                                //
                                //
                                //
                                //                                                Text(String(describing: self.selectedServiceProvider.ServiceProvider))
                                //                                                    .foregroundColor(Color("darkthemeletter"))
                                //                                                    .font(.custom("Bold_Font".localized(), size: 14))
                                //                                                    .padding(.top,20)
                                //
                                //
                                //
                                //
                                //
                                //                                                Divider()
                                //
                                //                                                HStack{
                                //
                                //
                                //
                                //                                                    Image("prefix__start_1").renderingMode(.template)
                                //                                                       .resizable()
                                //                                                        .frame(width: 10, height: 10).padding(.all,7)
                                //                                                        .foregroundColor(Color.white)
                                //                                                        .background(Color("destbackgreen")).clipShape(Circle())
                                //
                                //                                                    Text(self.selectedServiceProvider.ETA<10000000 ? getTime(timeIntervel: self.selectedServiceProvider.ETA) : "Calculating")
                                //                                                        .font(.custom("Regular_Font".localized(), size: 12))
                                //                                                        .foregroundColor(Color("darkthemeletter"))
                                //
                                //                                                    Spacer()
                                //
                                //
                                //
                                //
                                //
                                //                                                }.padding(.top,10)
                                //                                                Spacer()
                                //
                                //                                            }
                                //
                                //                                            .padding(.trailing,15)
                                //
                                //
                                //
                                //                                        }
                                //
                                //                                        .background(Color.white)
                                //                                        .cornerRadius(5)
                                //                                        .shadow(radius: 2)
                                //
                                //                                        Spacer().frame(width:20)
                                //
                                //                                    }
                                
                                HStack{
                                    
                                    
                                    // Text(UserDefaults.standard.string(forKey: "Aid") ?? "0" )
                                    Text("Select Service").font(.custom("ExtraBold_Font".localized(), size: 16)).padding(.top).padding(.horizontal)
                                    Spacer()
                                }
                                //ServiceProvider Services
                                
                                VStack(spacing:10)    {
                                    
                                    ForEach(self.getServiceData(Snap: self.selectedServiceProvider.SPSnap, Vechicle: self.NewCart.`vehicle`, id: Int.random(in: 0 ... 1000000)),id: \.self){ service in
                                        
                                        HStack(){
                                            Spacer().frame(width: self.NewCart.services.contains(service) ? 10 : 20)
                                            
                                            ZStack{
                                                
                                                
                                                
                                                HStack(alignment: .top){
                                                    
                                                    //      Image("basicbackimage")
                                                    
                                                    
                                                    ZStack{
                                                        AnimatedImage(url : URL (string: service.ServiceImage)).resizable()
                                                            .frame(width: 100, height: 100)
                                                            
                                                            .cornerRadius(5)
                                                        
                                                        if(self.NewCart.services.contains(service)){
                                                            Circle().foregroundColor(Color.white).frame(width: 55, height: 55)
                                                            Image(systemName: "checkmark.circle.fill")
                                                                .resizable()
                                                                
                                                                .frame(width: 50, height: 50)
                                                                
                                                                .foregroundColor(Color("ManBackColor"))
                                                            
                                                            
                                                        }
                                                        
                                                    }
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    VStack(alignment: .leading){
                                                        Text(service.serviceName)
                                                            .foregroundColor(Color("darkthemeletter"))
                                                            .font(.custom("Bold_Font".localized(), size: 14))
                                                        
                                                        
                                                        Text(service.serviceDesc)
                                                            .foregroundColor(Color("MainFontColor1"))
                                                            .font(.custom("Regular_Font".localized(), size: 12))
                                                        
                                                        Spacer()
                                                    }.padding()
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    Spacer()
                                                    
                                                    
                                                    Text("\(LocNumbers(key:service.ServicePrice))"+" AED".localized())
                                                        .font(.custom("ExtraBold_Font".localized(), size: 15))
                                                        .foregroundColor(Color("darkthemeletter"))
                                                        .padding()
                                                    //                                                    Spacer()
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    //                                                Spacer()
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                }
                                                HStack{
                                                    
                                                    Spacer()
                                                    //                                                                                           VStack{
                                                    //
                                                    ////                                                                                            Button(action:{
                                                    ////
                                                    ////
                                                    ////                                                                                            }){
                                                    //
                                                    //
                                                    //
                                                    //                                                                                          //  }
                                                    //                                                                                               Spacer()
                                                    //                                                                                           }
                                                }
                                                
                                                
                                                
                                            }.onAppear{
                                                print("OOOOOOO",self.Cart.services,service)
                                            }
                                            
                                            .background(Color.white)
                                            .cornerRadius(5)
                                            .shadow(radius: 2)
                                            .frame(height:100)
                                            Spacer().frame(width: self.NewCart.services.contains(service) ? 10 : 20)
                                            
                                        }.onTapGesture {
                                            withAnimation{
                                                if(!self.NewCart.services.contains(service)){
                                                    self.NewCart.services.append(service)
                                                }else{
                                                    self.NewCart.services.remove(at: self.NewCart.services.firstIndex(of: service)!)
                                                }
                                                //
                                                //                                                                                                var arr : [Services] =      self.providersFetch.getServiceData(Snap: self.serviceSnap, Vechicle: self.selectedVType)
                                                //
                                                //                                                                                                arr[arr.firstIndex(of:
                                                
                                                //                                                                                                service )!].select = true
                                            }
                                        }
                                    }
                                    
                                }.padding()
                                //Serviece Provider Addons
                                HStack{
                                    Text("Add on’s").font(.custom("ExtraBold_Font".localized(), size: 16)).padding(.top).padding(.horizontal)
                                    Spacer()
                                    
                                }
                                
                                VStack{
                                    
                                    
                                    ForEach(self.getAddondata(Snap: self.selectedServiceProvider.SPSnap, id: Int.random(in: 0 ... 1000000)),id: \.self){ addons in
                                        
                                        HStack{
                                            Spacer().frame(width:self.NewCart.addons.contains(addons) ? 10 : 20)
                                            ZStack{
                                                
                                                
                                                HStack(alignment:.top){
                                                    
                                                    //Image("basicbackimage")
                                                    
                                                    VStack(alignment:.center){
                                                        ZStack{
                                                            AnimatedImage(url : URL (string: addons.addonImage)).resizable()
                                                                .frame(width: 50, height: 70)
                                                                
                                                                .cornerRadius(5)
                                                            
                                                            
                                                            if(self.NewCart.addons.contains(addons)){
                                                                Circle().foregroundColor(Color.white).frame(width: 33, height: 33)
                                                                Image(systemName: "checkmark.circle.fill")
                                                                    .resizable()
                                                                    
                                                                    .frame(width: 30, height: 30)
                                                                    
                                                                    .foregroundColor(Color("ManBackColor"))
                                                            }
                                                            
                                                        }
                                                    }
                                                    
                                                    
                                                    VStack(alignment: .leading){
                                                        
                                                        
                                                        
                                                        
                                                        Text(addons.addonName)
                                                            .foregroundColor(Color("darkthemeletter"))
                                                            .font(.custom("Bold_Font".localized(), size: 14))
                                                        
                                                        
                                                        Text(addons.addonDesc)
                                                            .foregroundColor(Color("MainFontColor1"))
                                                            .font(.custom("Regular_Font".localized(), size: 12))
                                                        
                                                        //                                                Spacer()
                                                        
                                                    }.padding()
                                                    
                                                    
                                                    Spacer()
                                                    
                                                    
                                                    
                                                    
                                                    Text("\(LocNumbers(key:addons.addonPrice))"+" AED".localized())
                                                        .font(.custom("ExtraBold_Font".localized(), size: 15))
                                                        .foregroundColor(Color("darkthemeletter")).padding()
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                }
                                                
                                                
                                                //                                            HStack{
                                                //
                                                //                                                                                            Spacer()
                                                //                                                                                                                                       VStack{
                                                //
                                                ////                                                                                                                                        Button(action:{
                                                ////
                                                ////
                                                ////                                                                                                                                        }){
                                                //                                                                                                                                           Image(systemName: self.selectedAddons.contains(addons) ? "checkmark.circle.fill" : "circle")
                                                //                                                                                                                                            .resizable()                                       .frame(width: 15, height: 15)
                                                //
                                                //                                                                                                                                               .foregroundColor(Color("ManBackColor"))
                                                //                                                                                                                                               .padding([.top, .trailing], 10)
                                                //
                                                //
                                                //                                                                                                                                        //}
                                                //                                                                                                                                           Spacer()
                                                //                                                                                                                                       }
                                                //                                                                                        }
                                                
                                            }
                                            
                                            .background(Color.white)
                                            .cornerRadius(5)
                                            .shadow(radius: 2)
                                            .frame(height:70)
                                            //Spacer().frame(width:20)
                                            Spacer().frame(width:self.NewCart.addons.contains(addons) ? 10 : 20)
                                            
                                        }.onTapGesture
                                        {
                                            withAnimation{
                                                if(!self.NewCart.addons.contains(addons)){
                                                    self.NewCart.addons.append(addons)
                                                }else{
                                                    self.NewCart.addons.remove(at: self.NewCart.addons.firstIndex(of: addons)!)
                                                }
                                                //
                                                //                                                                                                var arr : [Services] =      self.providersFetch.getServiceData(Snap: self.serviceSnap, Vechicle: self.selectedVType)
                                                //
                                                //                                                                                                arr[arr.firstIndex(of:
                                                
                                                //                                                                                                service )!].select = true
                                            }
                                        }
                                    }
                                    
                                }
                                Spacer().frame(height:120)
                                
                            }
                        }
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                    }
                    
                    
                    
                    
                    
                    
                    
                    
                }
                
                
                
                .navigationBarTitle("Edit",displayMode: .inline)
                
                .edgesIgnoringSafeArea(.all)
            }
        }.onDisappear{
            print("OOOOOOO",self.NewCart.services)
            
            if(self.NewCart.services.count == 0
            ){
                
                self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(self.NewCart.id).removeValue()
            }else{
                self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(self.NewCart.id).child("services").removeValue()
                self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(self.NewCart.id).child("addons").removeValue()
                
                
                
                for service in self.NewCart.services {
                    
                    self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(self.NewCart.id).child("services").child(service.serviceName+String(Int.random(in: 0 ... 1000000))).setValue(service.getDict())
                }
                
                for addons in self.NewCart.addons {
                    
                    self.ref.child("Cart").child(UserDefaults.standard.string(forKey: "Aid") ?? "0").child("Vehicles").child(self.NewCart.id).child("addons").child(addons.addonName+String(Int.random(in: 0 ... 1000000))).setValue(addons.getDict())
                }
            }
        }.onAppear{
            
            var s :[Services] = [Services]()
            for     Ser in   self.Cart.services{
                let service :Services = Services(id: "0",serviceName: Ser.serviceName,serviceNameO:Ser.serviceNameO , serviceDesc: Ser.serviceDesc, serviceDescO: Ser.serviceDescO,ServiceImage: Ser.ServiceImage,ServicePrice: Ser.ServicePrice,ServiceTime: Ser.ServiceTime,ServiceVehicle: Ser.ServiceVehicle)
                s.append(service)
            }
            var a: [Addons] = [Addons]()
            for     Ad in   self.Cart.addons{
                
                let addons :Addons = Addons(id: "0", addonName: Ad.addonName,addonNameO:Ad.addonNameO , addonDesc: Ad.addonDesc, addonDescO: Ad.addonDescO, addonImage: Ad.addonImage, addonPrice: Ad.addonPrice)
                a.append(addons)
                
            }
            
            
            self.NewCart = CartStruct(id: self.Cart.id, addons: a, services: s, vehicle: self.Cart.vehicle, vehicleName: self.Cart.vehicleName)
            print("PPPPPPP",s)
            print("PPPPPPP",self.getServiceData(Snap: self.selectedServiceProvider.SPSnap, Vechicle: self.NewCart.`vehicle`, id: Int.random(in: 0 ... 1000000)))
        }
    }
    
    func getServiceData( Snap : DataSnapshot,Vechicle : String, id : Int) -> [Services] {
        
        
        var services = [Services]()
        
        for child in Snap.childSnapshot(forPath: "services/\(Vechicle)").children{
            let snapshot = child as! DataSnapshot
            
            //  let snapshot = s.value
            //as! [String: Any]
            let service :Services = Services(id: "0",serviceName: String(describing :  (snapshot.childSnapshot(forPath:"name".localized()).value)!),serviceNameO: String(describing :  (snapshot.childSnapshot(forPath:"name_ar".localized()).value)!),serviceDesc: String(describing :  (snapshot.childSnapshot(forPath:"description".localized()).value)!),serviceDescO: String(describing :  (snapshot.childSnapshot(forPath:"description_ar".localized()).value)!),ServiceImage: String(describing :  (snapshot.childSnapshot(forPath:"image_url").value)!),ServicePrice: String(describing :  (snapshot.childSnapshot(forPath:"price").value)!),ServiceTime: String(describing :  (snapshot.childSnapshot(forPath:"time_required").value)!),ServiceVehicle: "Unknown")
            
            services.append(service)
            
            //let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            // ...
            
            //           var service :Services = Services(id: "", serviceName: "", serviceDesc: "", ServiceImage: "")
            //
            //
            //
            //                     service.id = snapshot.key
            //                     service.serviceName = String(describing :  (snapshot.childSnapshot(forPath:Localize(key:"name")).value)!)
            //                     service.serviceDesc = String(describing :  (snapshot.childSnapshot(forPath:Localize(key:"name")).value)!)
            //                     service.ServiceImage = String(describing :  (snapshot.childSnapshot(forPath:Localize(key:"name")).value)!)
        }
        return services
        
        
    }
    
    func getAddondata( Snap : DataSnapshot,id:Int) -> [Addons] {
        
        
        var addons = [Addons]()
        
        for child in Snap.childSnapshot(forPath: "add_on_services").children{
            let snapshot = child as! DataSnapshot
            
            //  let snapshot = s.value
            //as! [String: Any]
            let addon :Addons = Addons(id: "0",addonName: String(describing :  (snapshot.childSnapshot(forPath:"name".localized()).value)!),addonNameO: String(describing :  (snapshot.childSnapshot(forPath:"name_ar".localized()).value)!),  addonDesc:  String(describing :  (snapshot.childSnapshot(forPath:"description".localized()).value ?? "")),addonDescO:  String(describing :  (snapshot.childSnapshot(forPath:"description_ar".localized()).value ?? "")),addonImage: String(describing :  (snapshot.childSnapshot(forPath:"image_url").value)!),addonPrice: String(describing :  (snapshot.childSnapshot(forPath:"price").value)!),addonVehicle:"Unknown" )
            
            addons.append(addon)
            
            //let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            // ...
            
            //           var service :Services = Services(id: "", serviceName: "", serviceDesc: "", ServiceImage: "")
            //
            //
            //
            //                     service.id = snapshot.key
            //                     service.serviceName = String(describing :  (snapshot.childSnapshot(forPath:Localize(key:"name")).value)!)
            //                     service.serviceDesc = String(describing :  (snapshot.childSnapshot(forPath:Localize(key:"name")).value)!)
            //                     service.ServiceImage = String(describing :  (snapshot.childSnapshot(forPath:Localize(key:"name")).value)!)
        }
        return addons
        
        
    }
    
}

//struct EditFromCart_Previews: PreviewProvider {
//    static var previews: some View {
//        EditFromCart(Cart: Binding<CartStruct>, selectedServiceProvider: Binding<EditFromCart.SelectedServiceProvider>)
//    }
//}
