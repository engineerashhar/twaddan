//
//  ServiceProviders.swift
//  Twaddan
//
//  Created by Spine on 06/05/20.
//  Copyright © 2020 spine. All rights reserved.
//

///Users/home/Documents/Twaddan/Twaddan/Fonts

import SwiftUI
import FirebaseDatabase
import SDWebImageSwiftUI
import MapKit

struct ServiceProviders: View {
    @EnvironmentObject var vehicleItems : VehicleItems
    
    @State var Sorting = false
    @State var seesearch = false
    @State  var GoToBookDetailsOld : Bool = false
    
    @State var OpenSPD = false
    @State var openVA = false
    @State var attempts = 0.0
    @State var services = [ServiceProvider]()
    @State var goToMap : Bool = false
    
    @EnvironmentObject var settings : UserSettings
    @State var StreetAddress : String = ""
    @State var menuOpen : Bool = false
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @State var SF = ["Nearest", "Best Rating", "Lowest price", "By Offer"]
    @State var VType
        : [String] = ["saloon", "5_seaters", "7_seaters", "caravan","bike","boat_xtra"]
    //        : [String] = ["Saloon", "SUV", "Super SUV", "Caravan","Bike","Boat"]
    @State var VDType
        : [String] = ["saloon", "5_seaters", "7_seaters", "caravan","bike","boat_xtra"]
    @State var VImages : [String] = ["saloon", "5_seaters", "7_seaters", "caravan","bike","boat_xtra"]
    //      =  ["saloon", "suv", "super_suv", "caravan","bike","bike"]
    
    @State var selectedSF : String = "";
    @State var SearchTerms : String = ""
    
    @State var SortAndFilter:Bool = false
    @State var VTypeBool:Bool = false
    @State var VehicleAlert:Bool = false
    @State var GoToBookDetails : Bool = false
    @State var selectedVType: LocalizedStringKey = LocalizedStringKey( UserDefaults.standard.string(forKey: "Vtype") ?? "Choose Vehicle")
    
    @State var sheetselectedVType: Bool = false
    
    @State var SelectedDriver : String = ""
    @State var ETA : Double = 0.0
    @State var Connected : Bool = false;
    @State var ConnectedDisplay : Bool = false;
    @State var Message : LocalizedStringKey = ""
    @State var timer = Timer.publish(every: 2, on: .main, in: .common).autoconnect()
    
    let ref = Database.database().reference()
    @State var OpenCart : Bool = false
    
    @State var Snap : DataSnapshot = DataSnapshot()
    
    // @State var service :ServiceProvider = ServiceProvider(serviceName: "", serviceDesc: "", Servicerating: "", Servicetime: "", ServiceImage: "")
    @ObservedObject var cartFetch = CartFetch()
    
    // @ObservedObject var providersFetch = ServiceProvidersFetch()
    @State var Tester : String = "Not Updated"
    
    var body: some View {
        
        GeometryReader { geometry in
            
            
            
            
            
            
            ZStack(alignment:.top){
                
                
                
                
                //First Layer
                
                VStack{
                    //                        Rectangle()
                    //                            .fill(Color("backcolor2"))
                    Spacer()
                        .frame(width:geometry.size.width,height: 100)
                    //                            .cornerRadius(20)
                    
                    
                    //         RoundedCorners(color: Color("backcolor2"), tl: 0, tr: 0, bl: 60, br: 60)
                    //       .frame(width:geometry.size.width,height: geometry.size.height*0.25)
                    //        .background(Color("backcolor2"))
                    //  .foregroundColor(Color("ManBackColor"))
                    
                    
                    
                    
                    Spacer()
                    
                    NavigationLink(destination: ServiceProviderPage(serviceSnap: self.Snap, ETA:self.ETA,Driver:self.SelectedDriver, GoButton: .constant(true))    .edgesIgnoringSafeArea(.all),isActive: self.$OpenSPD){
                        Text("").frame(height:0)
                    }
                    
                    
                    
                    
                    
                    
                    //VStack(spacing:10)
                    List  {
                        
                        TextField("Search", text: self.$SearchTerms).font(.custom("Regular_Font".localized(), size: 16)).textFieldStyle(RoundedBorderTextFieldStyle()).listRowInsets(EdgeInsets()).padding(.horizontal,10).padding(.vertical,5).listRowBackground(Color("h1"))
                        //                                .overlay(
                        ////                                HStack{
                        ////                                    Image("lence").renderingMode(.template).foregroundColor(Color.gray)
                        ////                                    Spacer()
                        ////                                }
                        //                            )
                        //   Text("Lets Test")
                        ForEach(self.services.filter{
                            self.SearchTerms == "" ? true : $0.serviceName.localizedCaseInsensitiveContains(self.SearchTerms)
                            
                            
                        }){ service in
                            
                            
                            ZStack{
                                
                                // Spacer().frame(width:20)
                                HStack(spacing:0){
                                    
                                    //                                                                                                                       Image("basicbackimage")
                                    AnimatedImage(url : URL (string: service.ServiceImage))
                                        .resizable() .cornerRadius(5)
                                        .frame(width: 75, height: 57)
                                        //
                                        .padding(10)
                                        
                                        .onTapGesture {
                                            if(self.Connected){
                                                self.Snap = service.serviceSnap
                                                
                                                self.SelectedDriver = service.DriverID
                                                self.ETA =  service.Servicetime
                                                self.OpenSPD = true
                                                
                                            }else{
                                                self.Message = "Please check your network"
                                                withAnimation{
                                                    self.ConnectedDisplay = true
                                                }
                                            }
                                            
                                            
                                            
                                        }
                                    
                                    
                                    VStack(alignment: .leading,spacing: 0){
                                        
                                        
                                        Group{
                                            
                                            Text(service.serviceName)
                                                .foregroundColor(Color.black)
                                                .font(.custom("Bold_Font".localized(), size: 15))
                                                .frame(height:17)
                                            //                            .padding(0)
                                            
                                            Text(service.serviceDesc)
                                                .lineLimit(1)
                                                .foregroundColor(Color("s1"))
                                                .font(.custom("Regular_Font".localized(), size: 13))
                                            
                                            
                                            RatingViewLarge(item: Double(service.Servicerating) ?? 0)
                                        }
                                        HStack{
                                            
                                            
                                            
                                            
                                            Text(getTimeComment(key:  service.Servicetime ))
                                                //
                                                .font(.custom("Regular_Font".localized(), size: 12))
                                                .foregroundColor(Color("s1"))
                                            
                                            Spacer()
                                            
                                            Button(action:{
                                                
                                                
                                                //                                                                                                                                    if(self.selectedVType != "Choose Vehicle"){
                                                
                                                self.VehicleAlert = false
                                                // self.OpenCart = true
                                                if(service.Servicetime < 1000000 ){
                                                    if(self.Connected){
                                                        print(service.serviceSnap)
                                                        self.Snap = service.serviceSnap
                                                        withAnimation{
                                                            self.GoToBookDetails=true
                                                            self.SelectedDriver = service.DriverID
                                                            self.ETA =  service.Servicetime
                                                        }
                                                        
                                                    }else{
                                                        self.Message = "Please check your network"
                                                        withAnimation{
                                                            self.ConnectedDisplay = true
                                                        }
                                                    }
                                                }else{
                                                    self.Message = "Please wait , checking availablity of drivers"
                                                    
                                                    withAnimation{
                                                        self.ConnectedDisplay = true
                                                    }
                                                }
                                                
                                                //                                                                                                                                    }else{
                                                //
                                                //                                                                //                                                                        self.selectedVType = ""
                                                //
                                                //                                                                //                                                                        self.VehicleAlert.toggle()
                                                //                                                                                                                                        self.attempts += 1
                                                //                                                                                                                                        print(self.attempts)
                                                //                                                                                                                                        self.VehicleAlert = true
                                                //                                                                                                                                        self.Message = "Please Choose your vehicle"
                                                //                                                                //                                                                             self.selectedVType = "Choose Vehicle"
                                                //                                                                                                                                                                                                    withAnimation{
                                                //                                                                                                                                                                                                   self.ConnectedDisplay = true
                                                //                                                                                                                                                                                                   }
                                                //                                                                                                                                    }
                                                
                                                
                                            }){
                                                
                                                
                                                RoundedPanel().frame(width:80,height: 22)
                                                    
                                                    .overlay(Text("Order Now")
                                                                .font(.custom("Regular_Font".localized(), size: 12))
                                                                
                                                                
                                                                .foregroundColor(Color.white))
                                                    .background(Color("c2"))
                                                    .cornerRadius(10)
                                                
                                            }
                                            //
                                            
                                            
                                            
                                            
                                        }
                                        //                        Spacer()
                                        
                                    }.padding(8)
                                    //                        .background(Color.red)
                                    
                                    
                                }
                                
                                .background(Color.white)
                                .cornerRadius(10)
                                .shadow(radius: 5)
                                
                                // Spacer().frame(width:20)
                                
                            }
                        }
                        .listRowInsets(EdgeInsets()).padding(.horizontal,10).padding(.vertical,5)
                        .listRowBackground(Color("h1"))
                    }
                    //                          .colorMultiply(Color("h1"))
                    //                          .frame(height:geometry.size.height*0.8)
                    
                    
                    
                    
                    
                    
                    
                    if(self.ConnectedDisplay){
                        HStack{
                            
                            Text(self.Message).font(.custom("Regular_Font".localized(), size: 14)).foregroundColor(.white)
                            
                        }.padding().frame(width:geometry.size.width).clipped().shadow(radius: 10)
                        .offset(y: self.ConnectedDisplay ? 0 : 100)
                        .background(Color("ManBackColor"))
                        .onAppear{
                            self.timer = Timer.publish (every: 2, on: .current, in:
                                                            .common).autoconnect()
                            print("reached here")
                            
                        }
                        .onReceive(self.timer) { input in
                            withAnimation{
                                self.ConnectedDisplay = false
                            }
                            self.timer.upstream.connect().cancel()
                        }
                    }
                    //
                    
                    //                        Spacer()
                    
                    
                    
                }
                //                        .background(Color.red)
                NavigationLink(destination: Cart(GoToBookDetailsOld: self.$GoToBookDetailsOld)
                                
                                .environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                                )), isActive: self.$OpenCart){
                    
                    EmptyView()
                }
                //Second layer
                
                //                    VStack(alignment: .leading){
                //
                //
                //                        Spacer().frame(height:geometry.size.height*0.1)
                //
                //
                //
                //                        HStack(alignment:.bottom , spacing: 0){
                //
                ////                            Spacer().frame(width:15)
                //
                //                            VStack(alignment:.leading){
                ////                                 Spacer().frame(height:10)
                //                                HStack{
                //
                //                                    Text("Location").foregroundColor(Color("darkthemeletter")).font(.custom("Bold_Font".localized(), size: 15))
                //
                //                                    Spacer()
                //                                }
                //                                .padding(.bottom,10)
                //
                //                                NavigationLink(destination : FindLocation(CalculateETA: .constant(false)) ,isActive: self.$goToMap ){
                //
                //                                                                   EmptyView()
                //                                                               }
                //                                Button(action:{
                //
                //
                //                                                              self.goToMap = true
                //
                //
                //
                //                                                          }){
                //                                    HStack{
                //                                        Image("placeholder")
                //                                            .resizable()
                //                                            .frame(width: 13, height: 18)
                //                                        Text(UserDefaults.standard.string(forKey: "PLACE") != nil ? String(UserDefaults.standard.string(forKey: "PLACE")!) : "Select Location")
                //                                            .foregroundColor(Color("darkthemeletter"))
                //                                            .font(.custom("Regular_Font".localized(), size: 14))
                //                                            .padding(.leading,10)
                //                                            .padding(.trailing,10)
                //
                //                                        Spacer()
                //                                        //                                                Button(action: {}){
                //                                        //                                                    Image("close_1")
                //                                        //                                                        .resizable()
                //                                        //                                                        .frame(width: 12, height: 12)
                //                                        //
                //                                        //                                                }
                //                                        //                                                .padding(.trailing, 15.0)
                //                                        //
                //                                        //
                //                                        //                                                Button(action: {}){
                //                                        //
                //                                        //
                //                                        //                                                    Image("select")
                //                                        //
                //                                        //
                //                                        //
                //                                        //
                //                                        //                                                }
                //                                        // .padding(.trailing, 15.0)
                //
                //
                //
                //
                //                                    }   .padding(.leading, 20.0)
                //                                                                          .padding(.vertical,10.0)
                //                                                                              .background(Color.white)
                //                                    .cornerRadius(4)
                //                                    .shadow(radius: 1)
                //
                //                                }.overlay(
                //                                    RoundedRectangle(cornerRadius: 4)
                //                                        .stroke(Color("ManBackColor"), lineWidth: 0.25)
                //                                )
                //                                   .clipped() .shadow(radius: 1)
                //                                    .frame(height: 30)
                ////                                  Spacer().frame(width:35)
                //
                //                            }.frame(width:geometry.size.width*0.6).padding(.leading,20)
                //                            Spacer()
                //
                //                            Button(action:{
                //
                //                                self.sheetselectedVType = true
                //                                                          }){
                //                            VStack{
                ////                                      Spacer().frame(height:20)
                //
                //
                //                                Image("self.selectedVType").padding(self.selectedVType=="Choose Vehicle" ? 15:10).overlay(
                //                                                                                                       Circle()
                //                                                                                                           .stroke(
                //                                                                                                               Color("ManBackColor"),lineWidth: 1)
                //                                ).foregroundColor(self.VehicleAlert && self.self.selectedVType == "Choose Vehicle"  ? Color.red:Color("ManBackColor"))
                //
                //                                Text(self.selectedVType) .font(.custom("Regular_Font".localized(), size: 14))
                //                                .foregroundColor(self.VehicleAlert && self.selectedVType == "Choose Vehicle" ? Color.red :Color("ManBackColor"))
                ////                                    .offset(x: self.VehicleAlert && self.selectedVType == "Choose Vehicle" ? -10 : 0)
                ////                                    .animation(Animation.default.repeatCount(5))
                //                                .modifier(Shake(animatableData: CGFloat(self.attempts)))
                //                                    .animation(self.VehicleAlert ? Animation.default.repeatCount(5):nil)
                //
                //                            }
                //
                //                            }
                //                            .actionSheet(isPresented: self.$sheetselectedVType, content: {
                //                                 ActionSheet(title: Text("Vehicle Type"), message: Text("Choose Vehicle Type"), buttons: [
                //                                    .default(Text(self.VType[0])) {  self.selectedVType = LocalizedStringKey(self.VType[0]) ;UserDefaults.standard.set(self.VType[0], forKey: "Vtype" );  self.ReadWithETA()
                //self.NearestSortOnly() },
                //                                    .default(Text(self.VType[1])) {  self.selectedVType = LocalizedStringKey(self.VType[1]) ;UserDefaults.standard.set(self.VType[1], forKey: "Vtype");self.ReadWithETA()
                //                                    self.NearestSortOnly()},
                //                                    .default(Text(self.VType[2])) {  self.selectedVType = LocalizedStringKey(self.VType[2]); UserDefaults.standard.set(self.VType[2], forKey: "Vtype");self.ReadWithETA()
                //                                    self.NearestSortOnly()},
                //                                    .default(Text(self.VType[3])) { self.selectedVType = LocalizedStringKey(self.VType[3]); UserDefaults.standard.set(self.VType[3], forKey: "Vtype");self.ReadWithETA()
                //                                    self.NearestSortOnly()},
                //                                    .default(Text(self.VType[4])) {  self.selectedVType = LocalizedStringKey(self.VType[4]) ;UserDefaults.standard.set(self.VType[4], forKey: "Vtype");self.ReadWithETA()
                //                                    self.NearestSortOnly()},
                //
                //                                .default(Text(self.VType[5])) {  self.selectedVType =  LocalizedStringKey(self.VType[5]) ;UserDefaults.standard.set(self.VType[5], forKey: "Vtype");self.ReadWithETA()
                //                                                                   self.NearestSortOnly()},
                //                                                                                   .cancel()])
                //
                //                            })
                //
                //                            Spacer()
                ////                            .contextMenu{
                ////
                ////                                ForEach(self.VType, id: \.self){ Vehicle in
                ////
                ////                                    Button(action:{
                ////                                     //   self.VTypeBool = false
                ////                                        self.selectedVType = Vehicle
                ////
                ////                                       //
                ////
                ////                                        UserDefaults.standard.set(Vehicle, forKey: "Vtype")
                ////                                        //self.services.removeAll()
                ////                                       // self.loadDataSingle()
                ////                                        self.ReadWithETA()
                ////                                        self.NearestSortOnly()
                ////
                ////                                    }){
                ////                                        HStack{
                ////                                                  Image(self.VImages[self.VType.firstIndex(of: Vehicle) ?? 0])
                ////                                                .resizable()
                ////                                                .frame(width: 14, height: 14)
                ////                                                .padding(.trailing, 10)
                ////
                ////
                ////
                ////                                            Text(Vehicle)
                ////                                            Spacer()
                ////
                ////                                        }
                ////                                    }
                ////                                }
                ////
                ////                            }
                ////                            Spacer()
                ////                            VStack{
                ////
                ////
                ////                                HStack{
                ////                                    Text("Vehicle").foregroundColor(Color("darkthemeletter")).font(.custom("Bold_Font".localized(), size: 15))
                ////
                ////                                    Spacer()
                ////                                }
                ////
                ////                                HStack{
                ////
                ////                                    TextField("Vehicle", text:self.$selectedVType )
                ////                                    .disabled(true)
                //// .foregroundColor(Color("darkthemeletter"))                                        .font(.custom("Regular_Font".localized(), size: 14))
                ////                                        .padding(.leading,10)
                ////
                ////
                ////                                    Spacer()
                ////                                    Button(action: {
                ////                                         withAnimation{
                ////                                                                                   self.VTypeBool.toggle()
                ////                                                                               }
                ////                                    }){
                ////                                        Image("up_and_down_2")
                ////                                            .resizable()
                ////                                            .frame(width: 11, height: 6)
                ////
                ////                                    }
                ////                                    .padding(.trailing, 15.0)
                ////
                ////
                ////
                ////                                    // .padding(.trailing, 15.0)
                ////
                ////
                ////                                }
                ////
                ////                                }
                ////                                .contextMenu{
                ////
                ////                                    ForEach(self.VType, id: \.self){ Vehicle in
                ////
                ////                                        Button(action:{
                ////                                         //   self.VTypeBool = false
                ////                                            self.selectedVType = Vehicle
                ////
                ////                                           //
                ////
                ////                                            UserDefaults.standard.set(Vehicle, forKey: "Vtype")
                ////                                            //self.services.removeAll()
                ////                                           // self.loadDataSingle()
                ////                                            self.ReadWithETA()
                ////                                            self.NearestSortOnly()
                ////
                ////                                        }){
                ////                                            HStack{
                ////                                                      Image(self.VImages[self.VType.firstIndex(of: Vehicle) ?? 0])
                ////                                                    .resizable()
                ////                                                    .frame(width: 14, height: 14)
                ////                                                    .padding(.trailing, 10)
                ////
                ////
                ////
                ////                                                Text(Vehicle)
                ////                                                Spacer()
                ////
                ////                                            }
                ////                                        }
                ////                                    }
                ////
                ////                                }.frame(width:geometry.size.width*0.35 ,height:40).background(Color.white).background(Color.white)
                ////                                    .cornerRadius(4)
                ////                                    .shadow(radius: 1)
                ////
                ////                                VStack(spacing:10){
                ////
                ////                                    ForEach(self.VType, id: \.self){ Vehicle in
                ////
                ////                                        Button(action:{
                ////                                            self.VTypeBool = false
                ////                                            self.selectedVType = Vehicle
                ////
                ////                                        }){
                ////                                            HStack{
                ////                                                Image(self.VImages[self.VType.firstIndex(of: Vehicle) ?? 0])
                ////                                                    .resizable()
                ////                                                    .frame(width: 20.0, height: 20.0)
                ////                                                    .padding(.trailing, 10)
                ////
                ////
                ////
                ////                                                Text(Vehicle)
                ////                                                Spacer()
                ////
                ////                                            }
                ////                                        }
                ////                                    }
                ////
                ////                                }
                ////                                     .frame(height:self.VTypeBool ? 200: 0)
                ////                             .frame(width:self.VTypeBool ? geometry.size.width*0.3  :0)
                ////                            //    .padding(10)
                ////                                .foregroundColor(Color(red: 0.38, green: 0.51, blue: 0.64, opacity: 1.0))
                ////
                ////
                ////
                ////
                ////                                .font(.custom("Regular_Font".localized(), size: 14))
                ////                                .padding(.leading,10)
                ////                                    // .frame(width:geometry.size.width*0.35 )
                ////                                    //
                ////
                ////
                ////                                    .background(Color.white).background(Color.white)
                ////                                    .cornerRadius(4)
                ////                                    .shadow(radius: 1)
                ////
                ////                            }
                //                            //
                //
                ////
                ////                            VStack{
                ////
                ////
                ////                                HStack{
                ////
                ////
                ////                                    Spacer().frame(height:33)
                ////
                ////                                }
                ////
                ////
                ////
                ////                            }
                ////
                ////                            Spacer().frame(width:15)
                ////
                //
                //
                //
                //                        }.frame(width:geometry.size.width,height: 300)
                ////                        ).background(Color.red)
                //
                //
                //
                //
                //
                //
                //
                //
                //
                ////                        Spacer()
                //
                //                    }.frame(width:geometry.size.width,height: 200).foregroundColor(Color("ManBackColor"))
                
                //                        .cornerRadius(20)
                
                
                
                //Tird Layer
                //
                //                    VStack{
                //                        Spacer().frame(height:180)
                //                        HStack(){
                //                            Spacer()
                //                            VStack(){
                //
                //                                Text("Sort & Filter").font(.custom("ExtraBold_Font".localized(), size: 14))
                //                                    .foregroundColor(Color("darkthemeletter"))
                //                                // Divider()
                //
                //
                //                                HStack(){
                //
                //
                //
                //                                    Picker(selection: self.$selectedSF, label: Text("")) {
                //                                        ForEach(self.SF, id: \.self) { S in
                //                                            Text(S)
                //
                //                                                .onTapGesture(){
                //
                //
                //
                //
                //                                                    switch self.SF.firstIndex(of : S){
                //
                //
                //
                //
                //                                                case 1 :
                //
                //                                                     self.providersFetch.topRated()
                //
                //                                                    break;
                //
                //
                //                                                case 2 :
                //
                //                                                     self.providersFetch.lowestPrice()
                //
                //                                                        break;
                //
                //                                                case 3 :
                //                                                     self.providersFetch.Nearest()
                //
                //                                                     break ;
                //
                //
                //
                //                                                    default  :
                //
                //                                                            self.providersFetch.Nearest()
                //
                //                                                                        break ;
                //
                //                                                }
                //
                //
                //
                //
                //                                            }
                //
                //
                //
                //
                //                                        }
                //                                    }
                //
                //                                    .frame(height: 100.0)
                //
                //
                //
                //
                //
                //                                    //                                    RadioButtonGroups { selected in
                //                                    //                                                   print("Selected Gender is: \(selected)")
                //                                    //                                               }
                //
                //                                    //  Image(systemName: "circle")
                //                                    //   Spacer()
                //
                //                                    //
                //                                    //                                    Text("Nearest")
                //                                    //                                    .font(.custom("Regular_Font".localized(), size: 14))
                //                                    //                                    .foregroundColor(Color("darkthemeletter")).padding(.trailing,10)
                //
                //
                //
                //                                    Spacer()
                //                                }
                //
                //
                //                            }.padding(10)                                  .foregroundColor(Color(red: 0.38, green: 0.51, blue: 0.64, opacity: 1.0))
                //                                .font(.custom("Regular_Font".localized(), size: 17))
                //                                .padding(.leading,10)
                //                                .frame(width:self.SortAndFilter ? geometry.size.width*0.35:0)
                //                                .frame(height:self.SortAndFilter ? 200 : 0)
                //                                .background(Color.white).background(Color.white)
                //                                .cornerRadius(4)
                //                                .shadow(radius: 1)
                //
                //                        }
                //
                //
                //
                //                        Spacer()
                //
                //                    }
                //                    .frame(width:self.SortAndFilter ? geometry.size.width :0)
                //                    .frame(height:self.SortAndFilter ? geometry.size.height : 0)
                
                
                
                
            } .frame(width:geometry.size.width,height: geometry.size.height).background(Color("h1"))
            .onAppear(perform:{
                
                
                
                //                self.loadDataSingle()
                // self.Nearest()
                withAnimation{
                    self.ConnectedDisplay = true
                }
                self.Message = "Not Connected"
                let connectedRef = Database.database().reference(withPath: ".info/connected")
                connectedRef.observe(.value, with: { snapshot in
                    if snapshot.value as? Bool ?? false {
                        print("Connected")
                        self.ReadWithNewETA()
                        //                        self.NearestSortOnly()
                        self.Connected = true
                        withAnimation{
                            self.ConnectedDisplay = false
                        }
                        self.NearestSortOnly()
                    } else {
                        self.Connected = false
                        withAnimation{
                            self.Message = "Not Connected"
                            self.ConnectedDisplay = true
                        }
                        print("Not connected")
                    }
                })
                
            })
            
            //        }
            
            
            
        }.background(Color("h1"))
        
        
        //        .sheet(isPresented: self.$GoToBookDetails){
        //
        //
        //
        //            BookingDetails(OpenCart: self.$OpenCart,GoToBookDetails:self.$GoToBookDetails ,serviceSnap: self.Snap, SelectedDriver:
        //                self.SelectedDriver,ETA: self.ETA, openVA: self.$openVA).environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
        //                    )).environmentObject(self.vehicleItems)
        //
        //
        //
        //        }
        
        .sheet(isPresented: self.$GoToBookDetails){
            
            
            
            BookingDetails(OpenCart: self.$OpenCart,GoToBookDetails:self.$GoToBookDetails ,serviceSnap: self.Snap, SelectedDriver:
                            self.SelectedDriver,ETA: self.ETA, openVA: self.$openVA).environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                            )).environmentObject(self.vehicleItems)
            
            
            
        }.onAppear{
            print("QQQQQQQQ",self.$GoToBookDetailsOld)
        }
        
        
        .navigationBarTitle(Text("Service Providers"))
        .navigationBarItems(trailing:
                                
                                HStack(spacing:3){
                                    
                                    Button(action:{
                                        withAnimation{
                                            self.OpenCart = true
                                            recalculateETA()
                                        }
                                    }){
                                        ZStack{
                                            Rectangle().cornerRadius(10).foregroundColor(Color("h1")).frame(width:42,height: 42).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color("s2"),lineWidth: 0.5))
                                            ZStack{
                                                Image( "cart").resizable().renderingMode(.template).foregroundColor(Color("s2")).frame(width:25,height:25)
                                                //
                                                if(self.cartFetch.carts.count>0){
                                                    Text(String(self.cartFetch.carts.count)).font(.custom("Regular_Font".localized(), size: 10))
                                                        .frame(width: 15.0, height: 15.0)
                                                        .background(Circle().fill(Color.red))
                                                        .foregroundColor(.white)
                                                        .padding([.trailing, .bottom])
                                                }
                                            }
                                            
                                        }
                                    }
                                    
                                    
                                    Button(action:{
                                        self.Sorting.toggle()
                                        
                                    }){
                                        ZStack{
                                            Rectangle().cornerRadius(10).foregroundColor(Color("h1")).frame(width:42,height: 42).overlay(RoundedRectangle(cornerRadius: 10).stroke(Color("s2"),lineWidth: 0.5))
                                            Image("sortandfilterpng")
                                                .resizable().renderingMode(.template).foregroundColor(Color("s2"))
                                                .frame(width: 23, height: 20)
                                            
                                        }
                                        
                                        
                                    }.actionSheet(isPresented: self.$Sorting) {
                                        ActionSheet(title: Text("Sort"), message: Text("Select type of sorting"), buttons: [
                                            .default(Text("Nearest")) {  self.NearestSortOnly() },
                                            .default(Text("Best Rating")) {    self.topRatedSortOnly() },
                                            //                    .default(Text("Lowest Price")) { self.ReadWithETA()self.lowestPriceSortOnly(vehicle : self.selectedVType)
                                            //                    self.NearestSortOnly() },
                                            .default(Text("By Offer")) {
                                                //                        self.ReadWithETA()
                                                self.topOfferSortOnly() },
                                            .cancel()
                                        ])
                                    }.padding(.leading)
                                    
                                    
                                    
                                    
                                }
                            
        )
        
        
        
        //                                               .contextMenu{
        //
        //                                                                                                             ForEach(self.SF, id: \.self){ S in
        //
        //                                                                                                                 Button(action:{
        //
        //                                                                                                                     switch self.SF.firstIndex(of : S)  {
        //
        //
        //
        //
        //                                                                                                                     case 1 :
        //
        //                                                                                                                          self.topRatedSortOnly()
        //                                                                                                                         // self.topRated()
        //                                                                                                                         break;
        //
        //
        //                                                                                                                     case 2 :
        //
        //                                                                                                                       self.lowestPriceSortOnly(vehicle : self.selectedVType)
        //           //                                                                                                            self.providersFetch.lowestPrice(vehicle : self.selectedVType)
        //
        //                                                                                                                             break;
        //
        //                                                                                                                     case 3 :
        //                                                                                                                             self.ReadWithETA()
        //                                                                                                                                                                   self.NearestSortOnly()
        //
        //                                                                                                                          break ;
        //
        //
        //
        //                                                                                                                         default  :
        //
        //                                                                                                                                   self.ReadWithETA()
        //                                                                                                                                                                          self.NearestSortOnly()
        //
        //                                                                                                                                             break ;
        //
        //                                                                                                                     }
        //                                                                                                                 }){
        //           //                                                                                                          HStack{
        //           //                                                                                                              Image("lence")
        //           //                                                                                                                  .resizable()
        //           //                                                                                                                  .frame(width: 14, height: 14)
        //           //                                                                                                                  .padding(.trailing, 10)
        //
        //
        //
        //                                                                                                                         Text(S)
        //           //                                                                                                              Spacer()
        //           //
        //           //                                                                                                          }
        //                                                                                                                 }
        //                                                                                                             }
        //
        //                                                                                                         })
        //             .navigationBarHidden(true)
        .edgesIgnoringSafeArea(.all)
        
    }
    //
    //    func removedupicates(serviceProviders:[ServiceProvider]) -> [ServiceProvider] {
    //
    //
    //
    //        let reduce = serviceProviders.reduce(into:[:],{ $0[$1, default:0] += 1})
    //        let sorted = reduce.sorted(by: {$0.value > $1.value})
    //        let map = sorted.map({$0.key})
    //        return map
    //    }
    
    
    
    func getColor(Rate :String) -> String {
        
        
        let  Rato : Double = (Rate as NSString).doubleValue
        print(Rato)
        
        if(Rato >= 5){ return "morethanfour"}
        else if (Rato >= 4){ return "morethanthree"}
        else if (Rato >= 3){ return "morethantwo"}
        else if (Rato >= 2){ return "morethanone"}
        else { return "morethanone"}
        
        
        
    }
    func NearestSortOnly()  {
        
        self.services.sort {
            
            
            $0.Servicetime < $1.Servicetime
        }
    }
    
    
    
    
    func topOfferSortOnly()  {
        
        self.services.sort {
            
            
            
            $0.Offer > $1.Offer
        }
        print(self.services)
        
    }
    func topRatedSortOnly()  {
        
        self.services.sort {
            
            
            
            Double($0.Servicerating) ?? 6  > Double($1.Servicerating) ?? 6
        }
    }
    func lowestPriceSortOnly(vehicle: String)  {
        self.services.sort {
            
            
            
            
            var One : Double = 10000000
            
            if( ($0.serviceSnap as DataSnapshot).childSnapshot(forPath: "services/\(vehicle)/full_wash/price").exists()){
                print(($0.serviceSnap as DataSnapshot).childSnapshot(forPath: "services/\(vehicle)/full_wash/price").value! as! Double)
                One  = ($0.serviceSnap as DataSnapshot).childSnapshot(forPath: "services/\(vehicle)/full_wash/price").value! as! Double
                
                
            }
            var two : Double = 10000000
            
            print(($1.serviceSnap as DataSnapshot).childSnapshot(forPath: "services/\(vehicle)"))
            if( ($1.serviceSnap as DataSnapshot).childSnapshot(forPath: "services/\(vehicle)/full_wash/price").exists()){
                
                two  = ($1.serviceSnap as DataSnapshot).childSnapshot(forPath: "services/\(vehicle)/full_wash/price").value! as! Double
                
                
            }
            return One < two
            
            
        }
        
    }
    func ReadWithETA() {
        
        
        print("cccccc","1")
        self.ref.child("service_providers").keepSynced(true)
        self.ref.child("drivers").keepSynced(true)
        
        self.ref.child("service_providers").observeSingleEvent(of: DataEventType.value, with: {
            (Msnapshot) in
            self.ref.child("drivers").observeSingleEvent(of: DataEventType.value, with: { (datasnapshot) in
                
                
                self.services.removeAll()
                print("cccccc","2")
                var selectedsps : [String] = [String]()
                var selectedsp : [String : TimeInterval] = [String: TimeInterval]()
                var selectColl : [String : ServiceProvider] = [String: ServiceProvider]()
                
                var SPQ : [String : ServiceProvider] = [String : ServiceProvider]()
                
                
                //                self.services.removeAll()
                var TempServices : [ServiceProvider] = [ServiceProvider]()
                var NewTempServices : [ServiceProvider] = [ServiceProvider]()
                var x = 0;
                
                for driverdata in datasnapshot.children {
                    let driversnap = driverdata as! DataSnapshot
                    
                    var driverlat =
                        25.0
                    
                    var driverlon = 55.635908
                    var lastETA : Double = 0
                    
                    if(String(describing:  driversnap.childSnapshot(forPath: "live_location/time_driver_engaged").value!) != "0"){
                        
                        lastETA = Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/time_driver_engaged").value)!)) ?? 0
                        
                        
                        print("QQQQQQQQQQ1",driversnap.key)
                        
                        
                        
                        driverlat =
                            Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/lc_lat").value)!)) ?? 25.0
                        
                        //  }
                        
                        
                        
                        //   if(driversnap.childSnapshot(forPath:"live_location/longitude").exists()){
                        driverlon =
                            Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/lc_lng").value)!)) ?? 55
                        
                        
                    }else{
                        
                        print("QQQQQQQQQQ2",driversnap.key)
                        driverlat =
                            Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/latitude").value)!)) ?? 25.0
                        
                        //  }
                        
                        
                        
                        //   if(driversnap.childSnapshot(forPath:"live_location/longitude").exists()){
                        driverlon =
                            Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/longitude").value)!)) ?? 55
                        
                    }
                    
                    print("cccccc","3")
                    print("QQQQQQQQQQ","3")
                    
                    if(String(describing :  (driversnap.childSnapshot(forPath:"status").value)!) == "1"){
                        print("QQQQQQQQQQ4",driversnap.key)
                        //                            print("PPPPPPPPPP",driversnap.key)
                        //        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5)) {
                        
                        
                        
                        //  if(driversnap.childSnapshot(forPath:"live_location/latitude") != nil){
                        //
                        
                        //  }
                        var driverloc : CLLocation = CLLocation(latitude: driverlat, longitude: driverlon)
                        var driver2D : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: driverlat, longitude: driverlon)
                        
                        let userlat = UserDefaults.standard.double(forKey: "latitude")
                        let userlon = UserDefaults.standard.double(forKey: "longitude")
                        
                        
                        var userloc : CLLocation = CLLocation(latitude: userlat, longitude: userlon)
                        var user2D  : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: userlat, longitude: userlon)
                        
                        
                        //ETA USING APPLE MAP
                        //
                        //                        let request = MKDirections.Request()
                        //                 request.source = MKMapItem(placemark:  MKPlacemark(coordinate: driver2D))
                        //                 request.destination = MKMapItem(placemark:  MKPlacemark(coordinate: user2D))
                        //
                        //                 request.requestsAlternateRoutes = false
                        
                        //   let directions = MKDirections(request: request)
                        //                    directions.calculateETA { (responds, error) in
                        //                         if error != nil {
                        //
                        //                                        } else {
                        //                            var ETA  =    responds?.expectedTravelTime
                        //
                        //
                        //
                        //                        }
                        //                    }
                        //                        directions.calculateETA{ response, error in
                        //
                        //
                        //
                        //
                        //                   //     directions.calculate { response, error in
                        //                                var eta:Double = 10000000
                        //                            if(error != nil){
                        //                        //                            }else{
                        //
                        //                                    eta = response?.expectedTravelTime as! Double
                        
                        
                        var eta:Double = 10000000
                        //   eta = getETA(from: user2D, to: driver2D)
                        
                        //                        let routes = response?.routes
                        
                        
                        
                        
                        //                        if(routes != nil){
                        //                        let selectedRoute = routes![0]
                        //                        let distance = selectedRoute.distance
                        //                         eta = selectedRoute.expectedTravelTime
                        //
                        //
                        //                        }
                        
                        
                        
                        let origin = "\(driver2D.latitude),\(driver2D.longitude)"
                        let destination = "\(user2D.latitude),\(user2D.longitude)"
                        
                        
                        
                        //guard
                        
                        let S : String =  "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=\(origin)&destinations=\(destination)&mode=driving&key=AIzaSyCYQeItZ7SVEQ24Grjzc3zA1B_2DU_GfPU"
                        
                        let url = URL(string:S
                        )
                        //        else {
                        //        let error = NSError(domain: "LocalDomain", code: 0, userInfo: [NSLocalizedDescriptionKey: "Failed to create object URL"])
                        //
                        //        return
                        //    }
                        let config = URLSessionConfiguration.default
                        let session = URLSession(configuration: config)
                        let task = session.dataTask(with: url!, completionHandler: {
                            (data, response, error) in
                            
                            print("11111111",driversnap.key)
                            
                            if error != nil {
                                print("cccccc","5")
                                print(error!.localizedDescription)
                            }
                            else {
                                print("cccccc","6")
                                //  print(response)
                                do {
                                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                                        
                                        guard let routes = json["rows"] as? NSArray else {
                                            DispatchQueue.main.async {
                                            }
                                            return
                                        }
                                        if (routes.count > 0) {
                                            let overview_polyline = routes[0] as? NSDictionary
                                            //   let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                                            //  let points = dictPolyline?.object(forKey: "points") as? String
                                            
                                            DispatchQueue.main.async {
                                                
                                                print("cccccc","7")
                                                //
                                                let legs = overview_polyline?["elements"] as! Array<Dictionary<String, AnyObject>>
                                                
                                                let distance = legs[0]["distance"] as? NSDictionary
                                                let distanceValue = distance?["value"] as? Int ?? 0
                                                let distanceDouleValue = distance?["value"] as? Double ?? 0.0
                                                let duration = legs[0]["duration"] as? NSDictionary
                                                let totalDurationInSeconds = duration?["value"] as? Int ?? 0
                                                let durationDouleValue = duration?["value"] as? Double ?? 0.0
                                                
                                                print (S)
                                                
                                                eta = durationDouleValue
                                                
                                                if((lastETA-(Date().timeIntervalSince1970)*1000) > 0){
                                                    eta = eta+(lastETA/1000)-(Date().timeIntervalSince1970)
                                                    print("WWWWW",true)
                                                    
                                                    
                                                }
                                                
                                                
                                                
                                                eta = eta+300 ;
                                                
                                                if(eta < 301){
                                                    eta = 10000000
                                                }
                                                
                                                print("QQQQQQQQQQ1",driversnap.key ,eta,lastETA,durationDouleValue)
                                                
                                                print("WWWWW",lastETA,Date().timeIntervalSince1970*1000,eta+lastETA-(Date().timeIntervalSince1970)*1000,eta)
                                                
                                                //                                                if(Msnapshot.childSnapshot(forPath: "newsp1_gmail_com" + "/services/"+"saloon").exists()){
                                                //                                                eta += lastETA
                                                //
                                                //                                                }
                                                //                                                print("cccccc","8")
                                                //
                                                //
                                                //                                                print("QQQQQQQQQQQQQQQQQ",eta)
                                                //
                                                //                                                print("PPPPPPPPPP",      String(describing:Msnapshot.childSnapshot(forPath: "newsp1_gmail_com" + "/services/"+"saloon").exists()))
                                                //
                                                //                                                print(getTime(timeIntervel: durationDouleValue) + " HHHHH " + String(describing:  driversnap.childSnapshot(forPath:"sp_related").value),driversnap.key)
                                                
                                                //  print(eta)
                                                
                                                if((UserDefaults.standard.string(forKey: "Vtype") ?? "Choose Vehicle") == "Choose Vehicle" ||
                                                    Msnapshot.childSnapshot(forPath: (String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!) + "/services/"+(UserDefaults.standard.string(forKey: "Vtype")!))).exists()){
                                                    
                                                    print(getTime(timeIntervel: durationDouleValue) + " HHHHHI " + String(describing:  driversnap.childSnapshot(forPath:"sp_related").value),driversnap.key)
                                                    
                                                    
                                                    if(!selectedsps.contains(String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!))){
                                                        selectedsps.append(String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!))
                                                        
                                                        selectedsp[String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)] = eta
                                                        
                                                        //   print("UUUUUUU","New",(driversnap.childSnapshot(forPath:"sp_related").value)!,eta)
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        //  let MsDataSnap = Msnapshot as! DataSnapshot
                                                        
                                                        let snapshot : DataSnapshot = Msnapshot.childSnapshot(forPath: String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!))
                                                        //                                                        print("KKKKKKKKK",String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!))
                                                        
                                                        
                                                        var HighPercentage = 0.0
                                                        for O in snapshot.childSnapshot(forPath: "offers").children {
                                                            let Offers = O as! DataSnapshot
                                                            print("WWWWWW",Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value!))) ?? 0.0)
                                                            if(Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value!))) ?? 0.0 > HighPercentage){
                                                                
                                                                HighPercentage = Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value! ))) ?? 0.0
                                                            }
                                                            
                                                        }
                                                        
                                                        let S = snapshot.childSnapshot(forPath: "personal_information/name".localized()).value
                                                        
                                                        //                                                        print("personal_information/name"))
                                                        
                                                        let service :ServiceProvider
                                                            
                                                            = ServiceProvider(id:snapshot.key,
                                                                              serviceSnap:snapshot ,
                                                                              serviceName: String(describing :  (snapshot.childSnapshot(forPath:"personal_information/name".localized()).value) ?? " "),
                                                                              serviceDesc: String(describing :  (snapshot.childSnapshot(forPath:"personal_information/name".localized()).value ) ?? " "),
                                                                              Servicerating: String(String(describing :  (snapshot.childSnapshot(forPath:"personal_information/ratings").value)!).prefix(3)),
                                                                              Servicetime: eta,
                                                                              ServiceImage:  String(describing :  (snapshot.childSnapshot(forPath:"personal_information/image").value) ?? " "), DriverID: driversnap.key, Offer: HighPercentage)
                                                        
                                                        
                                                        SPQ[(driversnap.childSnapshot(forPath:"sp_related").value)! as! String] = service
                                                        
                                                        
                                                        
                                                        
                                                    }else{
                                                        
                                                        
                                                        
                                                        
                                                        if(selectedsp[String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)]! > eta){
                                                            
                                                            //                                       print("UUUUUUU","Edited",(driversnap.childSnapshot(forPath:"sp_related").value)!,selectedsp[String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)]!,eta)
                                                            
                                                            
                                                            //
                                                            
                                                            let snapshot : DataSnapshot = Msnapshot.childSnapshot(forPath: String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!))
                                                            
                                                            var HighPercentage = 0.0
                                                            for O in snapshot.childSnapshot(forPath: "offers").children {
                                                                let Offers = O as! DataSnapshot
                                                                if(Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value!))) ?? 0.0 > HighPercentage){
                                                                    
                                                                    HighPercentage = Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value! ))) ?? 0.0
                                                                }
                                                                
                                                            }
                                                            
                                                            var service :ServiceProvider
                                                                
                                                                
                                                                
                                                                = ServiceProvider(id:snapshot.key,
                                                                                  serviceSnap:snapshot ,
                                                                                  serviceName: String(describing :  (snapshot.childSnapshot(forPath:"personal_information/name".localized()).value) ?? " "),
                                                                                  serviceDesc: String(describing :  (snapshot.childSnapshot(forPath:"personal_information/name".localized()).value ) ?? " "),
                                                                                  Servicerating: String(String(describing :  (snapshot.childSnapshot(forPath:"personal_information/ratings").value)!).prefix(3)),
                                                                                  Servicetime: eta,
                                                                                  ServiceImage:  String(describing :  (snapshot.childSnapshot(forPath:"personal_information/image").value) ?? " "), DriverID: driversnap.key, Offer: HighPercentage)
                                                            
                                                            
                                                            SPQ[(driversnap.childSnapshot(forPath:"sp_related").value)! as! String] = service
                                                            
                                                            
                                                            
                                                            
                                                            //                                                                                  TempServices.sort {
                                                            //
                                                            //                                                                                           $0.Servicetime < $1.Servicetime
                                                            //                                                                                     }
                                                            
                                                            
                                                            //                                   self.services = TempServices
                                                            
                                                            selectedsp[String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)] = eta
                                                            
                                                            
                                                        }else{
                                                            
                                                            print("UUUUUUU","EditedFailed",(driversnap.childSnapshot(forPath:"sp_related").value)!,selectedsp[String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)]!,eta)
                                                        }
                                                        
                                                        
                                                    }
                                                    
                                                    
                                                    print("SSSSSSSS",String(describing:  SPQ))
                                                    
                                                    
                                                    
                                                    NewTempServices = Array(SPQ.values)
                                                    
                                                    NewTempServices.sort {
                                                        
                                                        $0.Servicetime < $1.Servicetime
                                                    }
                                                    self.services = NewTempServices
                                                    
                                                }
                                                
                                                //                        }
                                                
                                                
                                                //                        }
                                                
                                                
                                                
                                            }
                                        }
                                        else {
                                            print(json)
                                            DispatchQueue.main.async {
                                                //   SVProgressHUD.dismiss()
                                            }
                                        }
                                    }
                                }
                                catch {
                                    print("error in JSONSerialization")
                                    DispatchQueue.main.async {
                                        //  SVProgressHUD.dismiss()
                                    }
                                }
                            }
                        })
                        task.resume()
                        
                        
                    }
                    
                }
                
                
                
                
                //            for key in serviceProviders.keys {
                //
                //                            let snapshot = serviceProviders[key] as! DataSnapshot
                //
                //
                //                          print(key)
                //                print(snapshot.childSnapshot(forPath:Localize(key:"personal_information/name")).value as! String)
                //
                //                let service :ServiceProvider
                //
                //                = ServiceProvider(id:key,
                //                                  serviceSnap:snapshot ,
                //                                  serviceName: snapshot.childSnapshot(forPath:Localize(key:"personal_information/name")).value as! String,
                //                                  serviceDesc: snapshot.childSnapshot(forPath:Localize(key:"personal_information/name")).value as! String,
                //                                  Servicerating: String(String(describing :  (snapshot.childSnapshot(forPath:"personal_information/ratings").value)!).prefix(3)),
                //                                  Servicetime: selectedsp[snapshot.key] ?? 0.0,
                //                                  ServiceImage: snapshot.childSnapshot(forPath:"personal_information/image").value as! String)
                //
                //                        self.services.append(service)
                //                        self.services.sort {
                //
                //                              $0.Servicetime < $1.Servicetime
                //                        }
                //
                //
                //
                //                        }
                
                
                
                
                
            })
            
            
            
            
        })
        
        
    }
    
    
    
    func ReadWithNewETA() {
        self.ref.child("service_providers").keepSynced(true)
        self.ref.child("drivers").keepSynced(true)
        self.ref.child("twaddan_admin/emirates").keepSynced(true)
        if(UserDefaults.standard.bool(forKey: "RESTRICTED_AREA") ?? false){
            self.ref.child("twaddan_admin/restricted_areas").child(UserDefaults.standard.string(forKey: "RESTRICTED_AREA_NAME") ?? "district_court").child("allowed_sp").observeSingleEvent(of: DataEventType.value) { (sps) in
                
                self.services.removeAll()
                for sp in sps.children {
                    let sp = sp as! DataSnapshot
                    
                    self.ref.child("service_providers").child(String(describing: sp.value!) ).observeSingleEvent(of: DataEventType.value) { (sp_data) in
                        self.sp_data_func(sp_data: sp_data)
                    }
                    
                    
                }
                
                
            }
            
        }else{
            
            print(UserDefaults.standard.string(forKey: "EMIRATES") ?? "Umm al-Quwain")
            self.ref.child("twaddan_admin/emirates").child(UserDefaults.standard.string(forKey: "EMIRATES") ?? "Umm al-Quwain").observeSingleEvent(of: DataEventType.value) { (sps) in
                
                self.services.removeAll()
                for sp in sps.children {
                    let sp = sp as! DataSnapshot
                    if(sp.key != "name"){
                        self.ref.child("service_providers").child(sp.key).observeSingleEvent(of: DataEventType.value) { (sp_data) in
                            
                            self.sp_data_func(sp_data: sp_data)
                            
                        }
                    }
                    
                }
                
                
            }
            
        }
        
    }
    
    
    func sp_data_func(sp_data:DataSnapshot) {
        
        
        
        if(sp_data.childSnapshot(forPath: "drivers_info/active_drivers_id").childrenCount == 0){
            
            
            
            var HighPercentage = 0.0
            for O in sp_data.childSnapshot(forPath: "offers").children {
                let Offers = O as! DataSnapshot
                print("WWWWWW",Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value!))) ?? 0.0)
                if(Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value!))) ?? 0.0 > HighPercentage){
                    
                    HighPercentage = Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value! ))) ?? 0.0
                }
                
            }
            
            let S = sp_data.childSnapshot(forPath: "personal_information/name".localized()).value
            
            //                                                        print("personal_information/name"))
            
            let service :ServiceProvider
                
                = ServiceProvider(id:sp_data.key,
                                  serviceSnap:sp_data ,
                                  serviceName: String(describing :  (sp_data.childSnapshot(forPath:"personal_information/name".localized()).value) ?? " "),
                                  serviceDesc: String(describing :  (sp_data.childSnapshot(forPath:"personal_information/name".localized()).value ) ?? " "),
                                  Servicerating: String(String(describing :  (sp_data.childSnapshot(forPath:"personal_information/ratings").value)!).prefix(3)),
                                  Servicetime: 9999999,
                                  ServiceImage:  String(describing :  (sp_data.childSnapshot(forPath:"personal_information/image").value) ?? " "), DriverID: "driversnap.key", Offer: HighPercentage)
            
            
            
            self.services.append(service)
            self.services.sort {
                
                
                $0.Servicetime < $1.Servicetime
            }
            let reduce = self.services.reduce(into:[:],{ $0[$1, default:0] += 1})
            let sorted = reduce.sorted(by: {$0.value > $1.value})
            let map = sorted.map({$0.key})
            self.services = map
            self.services.sort {
                
                
                $0.Servicetime < $1.Servicetime
            }
            
            
            
        }
        
        for dd in sp_data.childSnapshot(forPath: "drivers_info/active_drivers_id").children {
            let dd = dd as! DataSnapshot
            
            
            
            self.ref.child("drivers").child(dd.key).observeSingleEvent(of: DataEventType.value) { (driversnap) in
                
                
                
                var driverlat =
                    25.0
                
                var driverlon = 55.635908
                var lastETA : Double = 0
                
                if(String(describing:  driversnap.childSnapshot(forPath: "live_location/time_driver_engaged").value!) != "0"){
                    
                    lastETA = Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/time_driver_engaged").value)!)) ?? 0
                    
                    
                    
                    
                    
                    
                    driverlat =
                        Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/lc_lat").value)!)) ?? 25.0
                    
                    //  }
                    
                    
                    
                    //   if(driversnap.childSnapshot(forPath:"live_location/longitude").exists()){
                    driverlon =
                        Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/lc_lng").value)!)) ?? 55
                    
                    
                }else{
                    
                    print("QQQQQQQQQQ2",driversnap.key)
                    driverlat =
                        Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/latitude").value)!)) ?? 25.0
                    
                    //  }
                    
                    
                    
                    //   if(driversnap.childSnapshot(forPath:"live_location/longitude").exists()){
                    driverlon =
                        Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/longitude").value)!)) ?? 55
                    
                }
                
                print("cccccc","3")
                print("QQQQQQQQQQ","3")
                
                if(String(describing :  (driversnap.childSnapshot(forPath:"status").value)!) == "1"){
                    //                                                      print("QQQQQQQQQQ4",driversnap.key)
                    //                            print("PPPPPPPPPP",driversnap.key)
                    //        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5)) {
                    
                    
                    
                    //  if(driversnap.childSnapshot(forPath:"live_location/latitude") != nil){
                    //
                    
                    //  }
                    var driverloc : CLLocation = CLLocation(latitude: driverlat, longitude: driverlon)
                    var driver2D : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: driverlat, longitude: driverlon)
                    
                    let userlat = UserDefaults.standard.double(forKey: "latitude")
                    let userlon = UserDefaults.standard.double(forKey: "longitude")
                    
                    
                    var userloc : CLLocation = CLLocation(latitude: userlat, longitude: userlon)
                    var user2D  : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: userlat, longitude: userlon)
                    
                    
                    //ETA USING APPLE MAP
                    //
                    //                        let request = MKDirections.Request()
                    //                 request.source = MKMapItem(placemark:  MKPlacemark(coordinate: driver2D))
                    //                 request.destination = MKMapItem(placemark:  MKPlacemark(coordinate: user2D))
                    //
                    //                 request.requestsAlternateRoutes = false
                    
                    //   let directions = MKDirections(request: request)
                    //                    directions.calculateETA { (responds, error) in
                    //                         if error != nil {
                    //
                    //                                        } else {
                    //                            var ETA  =    responds?.expectedTravelTime
                    //
                    //
                    //
                    //                        }
                    //                    }
                    //                        directions.calculateETA{ response, error in
                    //
                    //
                    //
                    //
                    //                   //     directions.calculate { response, error in
                    //                                var eta:Double = 10000000
                    //                            if(error != nil){
                    //                        //                            }else{
                    //
                    //                                    eta = response?.expectedTravelTime as! Double
                    
                    
                    var eta:Double = 10000000
                    //   eta = getETA(from: user2D, to: driver2D)
                    
                    //                        let routes = response?.routes
                    
                    
                    
                    
                    //                        if(routes != nil){
                    //                        let selectedRoute = routes![0]
                    //                        let distance = selectedRoute.distance
                    //                         eta = selectedRoute.expectedTravelTime
                    //
                    //
                    //                        }
                    
                    
                    
                    let origin = "\(driver2D.latitude),\(driver2D.longitude)"
                    let destination = "\(user2D.latitude),\(user2D.longitude)"
                    
                    
                    
                    //guard
                    
                    let S : String =  "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=\(origin)&destinations=\(destination)&mode=driving&key=AIzaSyCYQeItZ7SVEQ24Grjzc3zA1B_2DU_GfPU"
                    
                    let url = URL(string:S
                    )
                    //        else {
                    //        let error = NSError(domain: "LocalDomain", code: 0, userInfo: [NSLocalizedDescriptionKey: "Failed to create object URL"])
                    //
                    //        return
                    //    }
                    let config = URLSessionConfiguration.default
                    let session = URLSession(configuration: config)
                    let task = session.dataTask(with: url!, completionHandler: {
                        (data, response, error) in
                        
                        print("11111111",driversnap.key)
                        
                        if error != nil {
                            print("cccccc","5")
                            print(error!.localizedDescription)
                        }
                        else {
                            print("cccccc","6")
                            //  print(response)
                            do {
                                if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                                    
                                    guard let routes = json["rows"] as? NSArray else {
                                        DispatchQueue.main.async {
                                        }
                                        return
                                    }
                                    if (routes.count > 0) {
                                        let overview_polyline = routes[0] as? NSDictionary
                                        //   let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                                        //  let points = dictPolyline?.object(forKey: "points") as? String
                                        
                                        DispatchQueue.main.async {
                                            
                                            print("cccccc","7")
                                            //
                                            let legs = overview_polyline?["elements"] as! Array<Dictionary<String, AnyObject>>
                                            
                                            let distance = legs[0]["distance"] as? NSDictionary
                                            let distanceValue = distance?["value"] as? Int ?? 0
                                            let distanceDouleValue = distance?["value"] as? Double ?? 0.0
                                            let duration = legs[0]["duration"] as? NSDictionary
                                            let totalDurationInSeconds = duration?["value"] as? Int ?? 0
                                            let durationDouleValue = duration?["value"] as? Double ?? 0.0
                                            
                                            print (S)
                                            
                                            eta = durationDouleValue
                                            print("QQQQQQQQQQ1",driversnap.key ,eta,lastETA,durationDouleValue)
                                            if((lastETA-(Date().timeIntervalSince1970)*1000) > 0){
                                                eta = eta+(((lastETA/1000)-(Date().timeIntervalSince1970)))
                                                print("WWWWW",(((lastETA/1000)-(Date().timeIntervalSince1970))/60))
                                                
                                                
                                            }
                                            
                                            
                                            
                                            eta = eta+300 ;
                                            
                                            if(eta < 301){
                                                eta = 10000000
                                            }
                                            
                                            print("QQQQQQQQQQ1",driversnap.key ,eta,lastETA,durationDouleValue)
                                            print("QQQQQQQQQQ",user2D)
                                            print("WWWWW",lastETA,Date().timeIntervalSince1970*1000,eta+lastETA-(Date().timeIntervalSince1970)*1000,eta)
                                            
                                            //                                                if(Msnapshot.childSnapshot(forPath: "newsp1_gmail_com" + "/services/"+"saloon").exists()){
                                            //                                                eta += lastETA
                                            //
                                            //                                                }
                                            //                                                print("cccccc","8")
                                            //
                                            //
                                            //                                                print("QQQQQQQQQQQQQQQQQ",eta)
                                            //
                                            //                                                print("PPPPPPPPPP",      String(describing:Msnapshot.childSnapshot(forPath: "newsp1_gmail_com" + "/services/"+"saloon").exists()))
                                            //
                                            //                                                print(getTime(timeIntervel: durationDouleValue) + " HHHHH " + String(describing:  driversnap.childSnapshot(forPath:"sp_related").value),driversnap.key)
                                            
                                            //  print(eta)
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            //   print("UUUUUUU","New",(driversnap.childSnapshot(forPath:"sp_related").value)!,eta)
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            var HighPercentage = 0.0
                                            for O in sp_data.childSnapshot(forPath: "offers").children {
                                                let Offers = O as! DataSnapshot
                                                print("WWWWWW",Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value!))) ?? 0.0)
                                                if(Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value!))) ?? 0.0 > HighPercentage){
                                                    
                                                    HighPercentage = Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value! ))) ?? 0.0
                                                }
                                                
                                            }
                                            
                                            let S = sp_data.childSnapshot(forPath: "personal_information/name".localized()).value
                                            
                                            //                                                        print("personal_information/name"))
                                            
                                            let service :ServiceProvider
                                                
                                                = ServiceProvider(id:sp_data.key,
                                                                  serviceSnap:sp_data ,
                                                                  serviceName: String(describing :  (sp_data.childSnapshot(forPath:"personal_information/name".localized()).value) ?? " "),
                                                                  serviceDesc: String(describing :  (sp_data.childSnapshot(forPath:"personal_information/description".localized()).value ) ?? " "),
                                                                  Servicerating: String(String(describing :  (sp_data.childSnapshot(forPath:"personal_information/ratings").value)!).prefix(3)),
                                                                  Servicetime: eta,
                                                                  ServiceImage:  String(describing :  (sp_data.childSnapshot(forPath:"personal_information/image").value) ?? " "), DriverID: driversnap.key, Offer: HighPercentage)
                                            
                                            
                                            print("RRRRRRRR",getTime(timeIntervel: eta),driversnap.key)
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            let TempService =     self.services.filter { (Sp) -> Bool in
                                                Sp.id == service.id
                                            }
                                            
                                            if(TempService.count>0){
                                                if(TempService[0].Servicetime>service.Servicetime){
                                                    self.services.remove(at: self.services.firstIndex(of: TempService[0])!)
                                                    self.services.append(service)
                                                    
                                                }
                                            }else{
                                                self.services.append(service)
                                            }
                                            
                                            
                                            
                                            //                                                                                        self.services.sort {
                                            //                                                                                            
                                            //                                                                                            
                                            //                                                                                            $0.Servicetime < $1.Servicetime
                                            //                                                                                        }
                                            let reduce = self.services.reduce(into:[:],{ $0[$1, default:0] += 1})
                                            let sorted = reduce.sorted(by: {$0.value > $1.value})
                                            let map = sorted.map({$0.key})
                                            self.services = map
                                            
                                            //                        }
                                            
                                            
                                            //                        }
                                            
                                            self.services.sort {
                                                
                                                
                                                $0.Servicetime < $1.Servicetime
                                            }
                                            
                                        }
                                    }
                                    else {
                                        print(json)
                                        DispatchQueue.main.async {
                                            //   SVProgressHUD.dismiss()
                                        }
                                    }
                                }
                            }
                            catch {
                                print("error in JSONSerialization")
                                DispatchQueue.main.async {
                                    //  SVProgressHUD.dismiss()
                                }
                            }
                        }
                    })
                    task.resume()
                    
                    
                }
            }
        }
    }
    
    //        {
    //
    //
    //
    //                     self.ref.child("service_providers").keepSynced(true)
    //
    //                    self.ref.child("service_providers").observeSingleEvent(of: DataEventType.value, with: {
    //
    //                         (Msnapshot) in
    //
    //        //              //   self.services.removeAll()
    //        //               var serviceProviders :[String : Any] = [String : Any]()
    //        //
    //        //
    //        //                 for child in Msnapshot.children {
    //        //
    //        //                                          let snapshot = child as! DataSnapshot
    //        //
    //        //
    //        //                    serviceProviders[snapshot.key] =  snapshot
    //        //
    //        //                   // print(snapshot.key)
    //        //                }
    //        //
    //        //
    //
    //
    //
    //
    //
    //
    //
    //                        self.ref.child("drivers").observeSingleEvent(of: DataEventType.value, with: { (datasnapshot) in
    //
    //
    //                            print("Ccccccc")
    //                        var selectedsps : [String] = [String]()
    //                        var selectedsp : [String : TimeInterval] = [String: TimeInterval]()
    //                           var selectColl : [String : ServiceProvider] = [String: ServiceProvider]()
    //                        //self.services.removeAll()
    //                   //     var TempServices : [ServiceProvider] = [ServiceProvider]()
    //                            var x = 0;
    //
    //                            for driverdata in datasnapshot.children {
    //                                let driversnap = driverdata as! DataSnapshot
    //
    //
    //
    //
    //                                if(String(describing :  (driversnap.childSnapshot(forPath:"status").value)!) == "1"){
    //
    //                           //        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5)) {
    //
    //                                var driverlat =
    //                                    25.0
    //
    //                                  //  if(driversnap.childSnapshot(forPath:"live_location/latitude") != nil){
    //                                        driverlat =
    //                                            Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/latitude").value)!)) ?? 25.0
    //
    //                                  //  }
    //
    //                                var driverlon = 55.635908
    //
    //                                 //   if(driversnap.childSnapshot(forPath:"live_location/longitude").exists()){
    //                                        driverlon =
    //                                            Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/longitude").value)!)) ?? 55
    //
    //                              //  }
    //                                    var driverloc : CLLocation = CLLocation(latitude: driverlat, longitude: driverlon)
    //                                    var driver2D : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: driverlat, longitude: driverlon)
    //
    //                                let userlat = UserDefaults.standard.double(forKey: "latitude")
    //                                let userlon = UserDefaults.standard.double(forKey: "longitude")
    //
    //
    //                           var userloc : CLLocation = CLLocation(latitude: userlat, longitude: userlon)
    //                                var user2D  : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: userlat, longitude: userlon)
    //
    //
    //                         //ETA USING APPLE MAP
    //        //
    //        //                        let request = MKDirections.Request()
    //        //                 request.source = MKMapItem(placemark:  MKPlacemark(coordinate: driver2D))
    //        //                 request.destination = MKMapItem(placemark:  MKPlacemark(coordinate: user2D))
    //        //
    //        //                 request.requestsAlternateRoutes = false
    //
    //                      //   let directions = MKDirections(request: request)
    //            //                    directions.calculateETA { (responds, error) in
    //            //                         if error != nil {
    //            //                                            print("Error getting directions")
    //            //                                        } else {
    //            //                            var ETA  =    responds?.expectedTravelTime
    //            //
    //            //
    //            //
    //            //                        }
    //            //                    }
    //        //                        directions.calculateETA{ response, error in
    //        //
    //        //
    //        //
    //        //
    //        //                   //     directions.calculate { response, error in
    //        //                                var eta:Double = 10000000
    //        //                            if(error != nil){
    //        //                                print(error.debugDescription)
    //        //                            }else{
    //        //
    //        //                                    eta = response?.expectedTravelTime as! Double
    //
    //
    //                                    var eta:Double = 10000000
    //                                 //   eta = getETA(from: user2D, to: driver2D)
    //
    //            //                        let routes = response?.routes
    //
    //
    //
    //
    //            //                        if(routes != nil){
    //            //                        let selectedRoute = routes![0]
    //            //                        let distance = selectedRoute.distance
    //            //                         eta = selectedRoute.expectedTravelTime
    //            //
    //            //
    //            //                        }
    //
    //
    //
    //                                        let origin = "\(driver2D.latitude),\(driver2D.longitude)"
    //                                        let destination = "\(user2D.latitude),\(user2D.longitude)"
    //
    //
    //
    //                                        //guard
    //
    //                                    let S : String =  "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=\(origin)&destinations=\(destination)&mode=driving&key=AIzaSyCYQeItZ7SVEQ24Grjzc3zA1B_2DU_GfPU"
    //
    //                                            let url = URL(string:S
    //                                               )
    //                                    //        else {
    //                                    //        let error = NSError(domain: "LocalDomain", code: 0, userInfo: [NSLocalizedDescriptionKey: "Failed to create object URL"])
    //                                    //        print("Error: \(error)")
    //                                    //        return
    //                                    //    }
    //                                        let config = URLSessionConfiguration.default
    //                                        let session = URLSession(configuration: config)
    //                                        let task = session.dataTask(with: url!, completionHandler: {
    //                                            (data, response, error) in
    //
    //                                                                 print("11111111",driversnap.key)
    //
    //                                            if error != nil {
    //                                                print(error!.localizedDescription)
    //                                            }
    //                                            else {
    //
    //                                              //  print(response)
    //                                                do {
    //                                                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
    //
    //                                                        guard let routes = json["rows"] as? NSArray else {
    //                                                            DispatchQueue.main.async {
    //                                                            }
    //                                                            return
    //                                                        }
    //                                                        if (routes.count > 0) {
    //                                                            let overview_polyline = routes[0] as? NSDictionary
    //                                                         //   let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
    //                                                          //  let points = dictPolyline?.object(forKey: "points") as? String
    //
    //                                                            DispatchQueue.main.async {
    //                                                                //
    //                                                                let legs = overview_polyline?["elements"] as! Array<Dictionary<String, AnyObject>>
    //
    //                                                  let distance = legs[0]["distance"] as? NSDictionary
    //                                                  let distanceValue = distance?["value"] as? Int ?? 0
    //                                                  let distanceDouleValue = distance?["value"] as? Double ?? 0.0
    //                                                  let duration = legs[0]["duration"] as? NSDictionary
    //                                                  let totalDurationInSeconds = duration?["value"] as? Int ?? 0
    //                                                  let durationDouleValue = duration?["value"] as? Double ?? 0.0
    //
    //                                                                print (S)
    //
    //                                                            eta = durationDouleValue+600 ;
    //
    //
    //                                                                if(eta < 601){
    //                                                                    eta = 10000000
    //                                                                }
    //
    //
    //
    //
    //
    //                                         print("QQQQQQQQQQQQQQQQQ",eta)
    //
    //                                                                print(getTime(timeIntervel: durationDouleValue) + " HHHHH " + String(describing:  driversnap.childSnapshot(forPath:"sp_related").value))
    //
    //                                      //  print(eta)
    //
    //                                        if(Msnapshot.childSnapshot(forPath: (String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!) + "/services/"+(UserDefaults.standard.string(forKey: "Vtype") ?? "saloon"))).exists()){
    //
    //
    //
    //
    //                                    if(!selectedsps.contains(String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!))){
    //                                        selectedsps.append(String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!))
    //
    //                                        selectedsp[String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)] = eta
    //
    //
    //
    //                                 //  let MsDataSnap = Msnapshot as! DataSnapshot
    //
    //                                        let snapshot : DataSnapshot = Msnapshot.childSnapshot(forPath: String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!))
    //
    //                                       let S = snapshot.childSnapshot(forPath: Localize(key:"personal_information/name")).value
    //
    //                                        print("WWWWWW Newly Added", String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!),eta)
    //
    //
    //                                        let service :ServiceProvider
    //
    //                                            = ServiceProvider(id:snapshot.key,
    //                                                                         serviceSnap:snapshot ,
    //                                                                         serviceName: snapshot.childSnapshot(forPath:Localize(key:"personal_information/name")).value as! String,
    //                                                                         serviceDesc: snapshot.childSnapshot(forPath:Localize(key:"personal_information/name")).value as! String,
    //                                                                         Servicerating: String(String(describing :  (snapshot.childSnapshot(forPath:"personal_information/ratings").value)!).prefix(3)),
    //                                                                         Servicetime: selectedsp[snapshot.key] ?? 10000000,
    //                                                                         ServiceImage: snapshot.childSnapshot(forPath:"personal_information/image").value as! String, DriverID: driversnap.key)
    //
    //                                                               self.services.append(service)
    //
    //
    //                                                      selectColl[String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)] = service
    //
    //        //                                                       TempServices.sort {
    //        //
    //        //                                                             $0.Servicetime < $1.Servicetime
    //        //                                                       }
    //                                  //      self.services = TempServices
    //
    //
    //
    //
    //                                    }else{
    //
    //
    //
    //
    //                                        if(selectedsp[String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)]! > eta){
    //
    //
    //
    //                                              print("WWWWWW Edited to", String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!),eta)
    //
    //                                               print(getTime(timeIntervel: selectedsp[String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)]!) + "JJJJ" + getTime(timeIntervel: eta) + " KKKK " + String(describing:  driversnap.childSnapshot(forPath:"sp_related").value))
    //
    //
    //
    //                                            let snapshot : DataSnapshot = Msnapshot.childSnapshot(forPath: String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!))
    //
    //
    //
    //                                                                      let service :ServiceProvider
    //
    //                                                                          = ServiceProvider(id:snapshot.key,
    //                                                                                                       serviceSnap:snapshot ,
    //                                                                                                       serviceName: snapshot.childSnapshot(forPath:Localize(key:"personal_information/name")).value as! String ,
    //                                                                                                       serviceDesc: snapshot.childSnapshot(forPath:Localize(key:"personal_information/name")).value as! String ,
    //                                                                                                       Servicerating: String(String(describing :  (snapshot.childSnapshot(forPath:"personal_information/ratings").value)!).prefix(3)),
    //                                                                                                       Servicetime: selectedsp[snapshot.key] ?? 10000000,
    //                                                                                                       ServiceImage: snapshot.childSnapshot(forPath:"personal_information/image").value as! String, DriverID: driversnap.key)
    //
    //                                            let OldServiceProvider : ServiceProvider = selectColl[String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)]!
    //                                           //  Errrr Bug position
    //
    //                                            let index : Int = self.services.firstIndex(of : OldServiceProvider)!
    //
    //                                            self.services.insert(service, at: index)
    //
    //
    //
    //        //                                                                                  TempServices.sort {
    //        //
    //        //                                                                                           $0.Servicetime < $1.Servicetime
    //        //                                                                                     }
    //
    //
    //                                       //     self.services = TempServices
    //
    //                                             selectedsp[String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)] = eta
    //
    //
    //                                        }
    //
    //
    //                                    }
    //
    //                                    }
    //
    //        //                        }
    //
    //
    //        //                        }
    //
    //
    //
    //                                                        }
    //                                                    }
    //                                                    else {
    //                                                        print(json)
    //                                                        DispatchQueue.main.async {
    //                                                         //   SVProgressHUD.dismiss()
    //                                                        }
    //                                                    }
    //                                                }
    //                                            }
    //                                            catch {
    //                                                print("error in JSONSerialization")
    //                                                DispatchQueue.main.async {
    //                                                  //  SVProgressHUD.dismiss()
    //                                                }
    //                                            }
    //                                        }
    //                                    })
    //                                    task.resume()
    //
    //
    //                            }
    //                        }
    //
    //
    //            //            for key in serviceProviders.keys {
    //            //
    //            //                            let snapshot = serviceProviders[key] as! DataSnapshot
    //            //
    //            //
    //            //                          print(key)
    //            //                print(snapshot.childSnapshot(forPath:Localize(key:"personal_information/name")).value as! String)
    //            //
    //            //                let service :ServiceProvider
    //            //
    //            //                = ServiceProvider(id:key,
    //            //                                  serviceSnap:snapshot ,
    //            //                                  serviceName: snapshot.childSnapshot(forPath:Localize(key:"personal_information/name")).value as! String,
    //            //                                  serviceDesc: snapshot.childSnapshot(forPath:Localize(key:"personal_information/name")).value as! String,
    //            //                                  Servicerating: String(String(describing :  (snapshot.childSnapshot(forPath:"personal_information/ratings").value)!).prefix(3)),
    //            //                                  Servicetime: selectedsp[snapshot.key] ?? 0.0,
    //            //                                  ServiceImage: snapshot.childSnapshot(forPath:"personal_information/image").value as! String)
    //            //
    //            //                        self.services.append(service)
    //            //                        self.services.sort {
    //            //
    //            //                              $0.Servicetime < $1.Servicetime
    //            //                        }
    //            //
    //            //
    //            //
    //            //                        }
    //
    //
    //
    //
    //
    //                    })
    //
    //
    //
    //
    //                            })
    //
    //
    //                }
    
    func topRated()  {
        
        
        
        self.ref.child("service_providers").keepSynced(true)
        
        self.ref.child("service_providers").observeSingleEvent(of: DataEventType.value, with: {
            
            (Msnapshot) in
            
            //   self.services.removeAll()
            var serviceProviders :[String : Any] = [String : Any]()
            
            var HighPercentage = 0.0
            
            for child in Msnapshot.children {
                
                let snapshot = child as! DataSnapshot
                
                
                serviceProviders[snapshot.key] =  snapshot
                for O in snapshot.childSnapshot(forPath: "offers").children {
                    let Offers = O as! DataSnapshot
                    if(Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value!))) ?? 0.0 > HighPercentage){
                        
                        HighPercentage = Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value! ))) ?? 0.0
                    }
                    
                }
                
                // print(snapshot.key)
            }
            
            
            
            
            
            
            
            
            
            self.ref.child("drivers").observeSingleEvent(of: DataEventType.value, with: { (datasnapshot) in
                
                var selectedsps : [String] = [String]()
                var selectedsp : [String : TimeInterval] = [String: TimeInterval]()
                var selectColl : [String : ServiceProvider] = [String: ServiceProvider]()
                //self.services.removeAll()
                var TempServices : [ServiceProvider] = [ServiceProvider]()
                
                
                for driverdata in datasnapshot.children {
                    let driversnap = driverdata as! DataSnapshot
                    
                    if(String(describing :  (driversnap.childSnapshot(forPath:"status").value)!) == "1"){
                        var driverlat =
                            10.797765
                        
                        //  if(driversnap.childSnapshot(forPath:"live_location/latitude") != nil){
                        driverlat =  Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/latitude").value)!)) ?? 10
                        
                        //  }
                        
                        var driverlon = 76.635908
                        
                        //   if(driversnap.childSnapshot(forPath:"live_location/longitude").exists()){
                        driverlon = Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/longitude").value)!)) ?? 76
                        
                        //  }
                        var driverloc : CLLocation = CLLocation(latitude: driverlat, longitude: driverlon)
                        var driver2D : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: driverlat, longitude: driverlon)
                        
                        let userlat =
                            //                    10.874858
                            UserDefaults.standard.double(forKey: "latitude")
                        let userlon =
                            //                    76.449728
                            UserDefaults.standard.double(forKey: "longitude")
                        
                        
                        
                        var userloc : CLLocation = CLLocation(latitude: userlat, longitude: userlon)
                        var user2D  : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: userlat, longitude: userlon)
                        
                        
                        //    var distance : CLLocationDistance = userloc.distance(from: driverloc)
                        // var timeOfArrival :
                        
                        let request = MKDirections.Request()
                        request.source = MKMapItem(placemark:  MKPlacemark(coordinate: driver2D))
                        request.destination = MKMapItem(placemark:  MKPlacemark(coordinate: user2D))
                        request.requestsAlternateRoutes = false
                        
                        let directions = MKDirections(request: request)
                        //                    directions.calculateETA { (responds, error) in
                        //                         if error != nil {
                        //                                            print("Error getting directions")
                        //                                        } else {
                        //                            var ETA  =    responds?.expectedTravelTime
                        //
                        //
                        //
                        //                        }
                        //                    }
                        
                        directions.calculate { response, error in
                            let routes = response?.routes
                            
                            
                            var eta:Double = 10000000
                            
                            if(routes != nil){
                                let selectedRoute = routes![0]
                                let distance = selectedRoute.distance
                                eta = selectedRoute.expectedTravelTime
                                
                                
                            }
                            print(eta)
                            
                            
                            
                            if(Msnapshot.childSnapshot(forPath: (String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!) + "/services/"+(UserDefaults.standard.string(forKey: "Vtype") ?? "saloon"))).exists()){
                                if(!selectedsps.contains(String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!))){
                                    selectedsps.append(String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!))
                                    
                                    selectedsp[String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)] = eta
                                    
                                    
                                    
                                    //  let MsDataSnap = Msnapshot as! DataSnapshot
                                    
                                    let snapshot : DataSnapshot = Msnapshot.childSnapshot(forPath: String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!))
                                    
                                    
                                    
                                    let service :ServiceProvider
                                        
                                        = ServiceProvider(id:snapshot.key,
                                                          serviceSnap:snapshot ,
                                                          serviceName: snapshot.childSnapshot(forPath:"personal_information/name".localized()).value as! String,
                                                          serviceDesc: snapshot.childSnapshot(forPath:"personal_information/name".localized()).value as! String,
                                                          Servicerating: String(String(describing :  (snapshot.childSnapshot(forPath:"personal_information/ratings").value)!).prefix(3)),
                                                          Servicetime: selectedsp[snapshot.key] ?? 0.0,
                                                          ServiceImage: snapshot.childSnapshot(forPath:"personal_information/image").value as! String, DriverID: driversnap.key, Offer: HighPercentage)
                                    
                                    TempServices.append(service)
                                    
                                    
                                    selectColl[String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)] = service
                                    
                                    TempServices.sort {
                                        
                                        
                                        
                                        Double($0.Servicerating) ?? 6  > Double($1.Servicerating) ?? 6
                                    }
                                    self.services = TempServices
                                    
                                    
                                    
                                    
                                }else{
                                    
                                    if(selectedsp[String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)]! > eta){
                                        
                                        
                                        
                                        
                                        let snapshot : DataSnapshot = Msnapshot.childSnapshot(forPath: String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!))
                                        
                                        
                                        
                                        let service :ServiceProvider
                                            
                                            = ServiceProvider(id:snapshot.key,
                                                              serviceSnap:snapshot ,
                                                              serviceName: snapshot.childSnapshot(forPath:"personal_information/name".localized()).value as! String,
                                                              serviceDesc: snapshot.childSnapshot(forPath:"personal_information/name".localized()).value as! String,
                                                              Servicerating: String(String(describing :  (snapshot.childSnapshot(forPath:"personal_information/ratings").value)!).prefix(3)),
                                                              Servicetime: selectedsp[snapshot.key] ?? 0.0,
                                                              ServiceImage: snapshot.childSnapshot(forPath:"personal_information/image").value as! String, DriverID: driversnap.key, Offer: HighPercentage)
                                        
                                        let OldServiceProvider : ServiceProvider = selectColl[String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)]!
                                        
                                        
                                        let index : Int = self.services.firstIndex(of : OldServiceProvider)!
                                        
                                        TempServices.insert(service, at: index)
                                        
                                        
                                        
                                        TempServices.sort {
                                            
                                            
                                            
                                            Double($0.Servicerating) ?? 6  > Double($1.Servicerating) ?? 6
                                        }
                                        
                                        
                                        self.services = TempServices
                                        
                                        selectedsp[String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)] = eta
                                        
                                        
                                    }
                                    
                                    
                                }
                                
                                
                            }
                            
                            
                            
                        }
                        
                    }
                }
                
                
                //            for key in serviceProviders.keys {
                //
                //                            let snapshot = serviceProviders[key] as! DataSnapshot
                //
                //
                //                          print(key)
                //                print(snapshot.childSnapshot(forPath:Localize(key:"personal_information/name")).value as! String)
                //
                //                let service :ServiceProvider
                //
                //                = ServiceProvider(id:key,
                //                                  serviceSnap:snapshot ,
                //                                  serviceName: snapshot.childSnapshot(forPath:Localize(key:"personal_information/name")).value as! String,
                //                                  serviceDesc: snapshot.childSnapshot(forPath:Localize(key:"personal_information/name")).value as! String,
                //                                  Servicerating: String(String(describing :  (snapshot.childSnapshot(forPath:"personal_information/ratings").value)!).prefix(3)),
                //                                  Servicetime: selectedsp[snapshot.key] ?? 0.0,
                //                                  ServiceImage: snapshot.childSnapshot(forPath:"personal_information/image").value as! String)
                //
                //                        self.services.append(service)
                //                        self.services.sort {
                //
                //                              $0.Servicetime < $1.Servicetime
                //                        }
                //
                //
                //
                //                        }
                
                
                
                
                
            })
            
            
            
            
        })
        
        
    }
    
    
    func lowestPrice(vehicle:String)  {
        
        //  var vehicle : String
        
        self.ref.child("service_providers").keepSynced(true)
        
        self.ref.child("service_providers").observeSingleEvent(of: DataEventType.value, with: {
            
            (Msnapshot) in
            
            //   self.services.removeAll()
            var serviceProviders :[String : Any] = [String : Any]()
            var HighPercentage = 0.0
            
            for child in Msnapshot.children {
                
                let snapshot = child as! DataSnapshot
                
                
                serviceProviders[snapshot.key] =  snapshot
                for O in snapshot.childSnapshot(forPath: "offers").children {
                    let Offers = O as! DataSnapshot
                    if(Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value!))) ?? 0.0 > HighPercentage){
                        
                        HighPercentage = Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value! ))) ?? 0.0
                    }
                    
                }
                
                // print(snapshot.key)
            }
            
            
            
            
            
            
            
            
            
            
            self.ref.child("drivers").observeSingleEvent(of: DataEventType.value, with: { (datasnapshot) in
                
                var selectedsps : [String] = [String]()
                var selectedsp : [String : TimeInterval] = [String: TimeInterval]()
                var selectColl : [String : ServiceProvider] = [String: ServiceProvider]()
                //self.services.removeAll()
                var TempServices : [ServiceProvider] = [ServiceProvider]()
                
                
                for driverdata in datasnapshot.children {
                    let driversnap = driverdata as! DataSnapshot
                    
                    
                    
                    if(String(describing :  (driversnap.childSnapshot(forPath:"status").value)!) == "1"){
                        
                        var driverlat =
                            10.797765
                        
                        //  if(driversnap.childSnapshot(forPath:"live_location/latitude") != nil){
                        driverlat =  Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/latitude").value)!)) ?? 10
                        
                        //  }
                        
                        var driverlon = 76.635908
                        
                        //   if(driversnap.childSnapshot(forPath:"live_location/longitude").exists()){
                        driverlon = Double(String(describing :  (driversnap.childSnapshot(forPath:"live_location/longitude").value)!)) ?? 76
                        
                        //  }
                        var driverloc : CLLocation = CLLocation(latitude: driverlat, longitude: driverlon)
                        var driver2D : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: driverlat, longitude: driverlon)
                        
                        let userlat =
                            //                    10.874858
                            UserDefaults.standard.double(forKey: "latitude")
                        let userlon =
                            //                    76.449728
                            UserDefaults.standard.double(forKey: "longitude")
                        
                        
                        
                        var userloc : CLLocation = CLLocation(latitude: userlat, longitude: userlon)
                        var user2D  : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: userlat, longitude: userlon)
                        
                        
                        //    var distance : CLLocationDistance = userloc.distance(from: driverloc)
                        // var timeOfArrival :
                        
                        let request = MKDirections.Request()
                        request.source = MKMapItem(placemark:  MKPlacemark(coordinate: driver2D))
                        request.destination = MKMapItem(placemark:  MKPlacemark(coordinate: user2D))
                        request.requestsAlternateRoutes = false
                        
                        let directions = MKDirections(request: request)
                        //                    directions.calculateETA { (responds, error) in
                        //                         if error != nil {
                        //                                            print("Error getting directions")
                        //                                        } else {
                        //                            var ETA  =    responds?.expectedTravelTime
                        //
                        //
                        //
                        //                        }
                        //                    }
                        
                        directions.calculate { response, error in
                            let routes = response?.routes
                            
                            
                            var eta:Double = 10000000
                            
                            if(routes != nil){
                                let selectedRoute = routes![0]
                                let distance = selectedRoute.distance
                                eta = selectedRoute.expectedTravelTime
                                
                                
                            }
                            print(eta)
                            
                            if(Msnapshot.childSnapshot(forPath: (String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!) + "/services/"+(UserDefaults.standard.string(forKey: "Vtype") ?? "saloon"))).exists()){
                                
                                if(!selectedsps.contains(String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!))){
                                    selectedsps.append(String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!))
                                    
                                    selectedsp[String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)] = eta
                                    
                                    
                                    
                                    //  let MsDataSnap = Msnapshot as! DataSnapshot
                                    
                                    let snapshot : DataSnapshot = Msnapshot.childSnapshot(forPath: String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!))
                                    
                                    
                                    
                                    let service :ServiceProvider
                                        
                                        = ServiceProvider(id:snapshot.key,
                                                          serviceSnap:snapshot ,
                                                          serviceName: snapshot.childSnapshot(forPath:"personal_information/name".localized()).value as! String,
                                                          serviceDesc: snapshot.childSnapshot(forPath:"personal_information/name".localized()).value as! String,
                                                          Servicerating: String(String(describing :  (snapshot.childSnapshot(forPath:"personal_information/ratings").value)!).prefix(3)),
                                                          Servicetime: selectedsp[snapshot.key] ?? 0.0,
                                                          ServiceImage: snapshot.childSnapshot(forPath:"personal_information/image").value as! String, DriverID: driversnap.key, Offer: HighPercentage)
                                    
                                    TempServices.append(service)
                                    
                                    
                                    selectColl[String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)] = service
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    TempServices.sort {
                                        
                                        
                                        
                                        
                                        var One : Double = 10000000
                                        
                                        if( ($0.serviceSnap as DataSnapshot).childSnapshot(forPath: "services/\(vehicle)/full_wash/price").exists()){
                                            
                                            One  = ($0.serviceSnap as DataSnapshot).childSnapshot(forPath: "services/\(vehicle)/full_wash/price").value! as! Double
                                            
                                            
                                        }
                                        var two : Double = 10000000
                                        if( ($1.serviceSnap as DataSnapshot).childSnapshot(forPath: "services/\(vehicle)/full_wash/price").exists()){
                                            
                                            two  = ($1.serviceSnap as DataSnapshot).childSnapshot(forPath: "services/\(vehicle)/full_wash/price").value! as! Double
                                            
                                            
                                        }
                                        return One < two
                                        
                                        
                                    }
                                    
                                    
                                    
                                    
                                    
                                    self.services = TempServices
                                    
                                    
                                    
                                    
                                }else{
                                    
                                    if(selectedsp[String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)]! > eta){
                                        
                                        
                                        
                                        
                                        let snapshot : DataSnapshot = Msnapshot.childSnapshot(forPath: String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!))
                                        
                                        
                                        
                                        let service :ServiceProvider
                                            
                                            = ServiceProvider(id:snapshot.key,
                                                              serviceSnap:snapshot ,
                                                              serviceName: snapshot.childSnapshot(forPath:"personal_information/name".localized()).value as! String,
                                                              serviceDesc: snapshot.childSnapshot(forPath:"personal_information/name".localized()).value as! String,
                                                              Servicerating: String(String(describing :  (snapshot.childSnapshot(forPath:"personal_information/ratings").value)!).prefix(3)),
                                                              Servicetime: selectedsp[snapshot.key] ?? 0.0,
                                                              ServiceImage: snapshot.childSnapshot(forPath:"personal_information/image").value as! String, DriverID: driversnap.key, Offer: HighPercentage)
                                        
                                        let OldServiceProvider : ServiceProvider = selectColl[String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)]!
                                        
                                        
                                        let index : Int = self.services.firstIndex(of : OldServiceProvider)!
                                        
                                        TempServices.insert(service, at: index)
                                        
                                        TempServices.sort {
                                            
                                            
                                            
                                            
                                            var One : Double = 10000000
                                            
                                            if( ($0.serviceSnap as DataSnapshot).childSnapshot(forPath: "services/\(vehicle)/full_wash/price").exists()){
                                                
                                                One  = ($0.serviceSnap as DataSnapshot).childSnapshot(forPath: "services/\(vehicle)/full_wash/price").value! as! Double
                                                
                                                
                                            }
                                            var two : Double = 10000000
                                            if( ($1.serviceSnap as DataSnapshot).childSnapshot(forPath: "services/\(vehicle)/full_wash/price").exists()){
                                                
                                                two  = ($1.serviceSnap as DataSnapshot).childSnapshot(forPath: "services/\(vehicle)/full_wash/price").value! as! Double
                                                
                                                
                                            }
                                            return One < two
                                            
                                            
                                        }
                                        
                                        
                                        
                                        self.services = TempServices
                                        
                                        selectedsp[String(describing :  (driversnap.childSnapshot(forPath:"sp_related").value)!)] = eta
                                        
                                        
                                    }
                                    
                                    
                                }
                                
                            }
                            
                            
                            
                            
                        }
                        
                    }
                }
                
                
                //            for key in serviceProviders.keys {
                //
                //                            let snapshot = serviceProviders[key] as! DataSnapshot
                //
                //
                //                          print(key)
                //                print(snapshot.childSnapshot(forPath:Localize(key:"personal_information/name")).value as! String)
                //
                //                let service :ServiceProvider
                //
                //                = ServiceProvider(id:key,
                //                                  serviceSnap:snapshot ,
                //                                  serviceName: snapshot.childSnapshot(forPath:Localize(key:"personal_information/name")).value as! String,
                //                                  serviceDesc: snapshot.childSnapshot(forPath:Localize(key:"personal_information/name")).value as! String,
                //                                  Servicerating: String(String(describing :  (snapshot.childSnapshot(forPath:"personal_information/ratings").value)!).prefix(3)),
                //                                  Servicetime: selectedsp[snapshot.key] ?? 0.0,
                //                                  ServiceImage: snapshot.childSnapshot(forPath:"personal_information/image").value as! String)
                //
                //                        self.services.append(service)
                //                        self.services.sort {
                //
                //                              $0.Servicetime < $1.Servicetime
                //                        }
                //
                //
                //
                //                        }
                
                
                
                
                
            })
            
            
            
            
        })
        
        
    }
    
    
    func loadDataSingle(){
        
        
        self.ref.child("service_providers").keepSynced(true)
        
        self.ref.child("service_providers").observeSingleEvent(of: DataEventType.value, with: {
            
            (Msnapshot) in
            
            self.services.removeAll()
            
            
            for child in Msnapshot.children {
                let snapshot = child as! DataSnapshot
                
                //  let snapshot = s.value
                //as! [String: Any]
                var HighPercentage = 0.0
                for O in snapshot.childSnapshot(forPath: "offers").children {
                    let Offers = O as! DataSnapshot
                    if(Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value!))) ?? 0.0 > HighPercentage){
                        
                        HighPercentage = Double(String(describing: (Offers.childSnapshot(forPath:"percentage").value! ))) ?? 0.0
                    }
                    
                }
                
                //                        if(snapshot.childSnapshot(forPath:  "services/"+(UserDefaults.standard.string(forKey: "Vtype") ?? "saloon")).exists()){
                
                
                
                let service :ServiceProvider
                    
                    = ServiceProvider(id: snapshot.key,
                                      serviceSnap: snapshot,
                                      serviceName: String(describing : (snapshot.childSnapshot(forPath:"personal_information/name".localized()).value )! ),
                                      serviceDesc: String(describing : (snapshot.childSnapshot(forPath:"personal_information/name".localized()).value )! ),
                                      Servicerating:String(String(describing :  (snapshot.childSnapshot(forPath:"personal_information/ratings").value)!).prefix(3)),
                                      Servicetime: 10000000,
                                      ServiceImage:String(describing :  ( snapshot.childSnapshot(forPath:"personal_information/image").value )! ),
                                      DriverID: "", Offer: HighPercentage)
                
                
                
                
                
                
                
                //let postDict = snapshot.value as? [String : AnyObject] ?? [:]
                // ...
                
                // _ = snapshot.childSnapshot(forPath:"personal_information").value as! [String:Any]
                
                // print(pInfo["ratings"])
                
                //service.id = snapshot.key
                
                //                service.serviceName = snapshot.childSnapshot(forPath:Localize(key:"personal_information/name")).value as! String
                
                //self.Tester = snapshot.childSnapshot(forPath:Localize(key:"personal_information/name")).value as! String
                //             service.serviceDesc = snapshot.childSnapshot(forPath:Localize(key:"personal_information/name")).value as! String
                //                service.Servicerating = String(String(describing :  (snapshot.childSnapshot(forPath:"personal_information/ratings").value)!).prefix(3))
                
                
                //                service.Servicetime = 0.0
                
                //Double(describing : (snapshot.childSnapshot(forPath:"personal_information/times_rated").value)! )
                //             service.ServiceImage = snapshot.childSnapshot(forPath:"personal_information/image").value as! String
                
                self.services.append(service)
                
                
                
                //                    }
            }
            
            
            
            
            
        })
        
    }
}

struct ServiceProviders_Previews: PreviewProvider {
    static var previews: some View {
        ServiceProviders().environmentObject(UserSettings())
    }
}





func getTime(timeIntervel:TimeInterval) -> String {
    //    let date = Date().addingTimeInterval(timeIntervel)
    //    let f = DateFormatter()
    //    let g = DateFormatter()
    //    f.dateFormat = "mm"
    //    g.dateFormat = "HH"
    
    
    let Hours: Int = Int(timeIntervel/(60*60))
    let Minutes: Int = Int(timeIntervel/(60)) - (Hours*60)
    
    if(Hours == 0){
        
        return "Expected time of arrival".localized()+" \(LocNumbers(key: String(format:"%02d", Minutes))) "+"mins".localized()
        
    }else{
        
        return  ("Expected time of arrival".localized()+" \((LocNumbers(key:String(format:"%02d", Hours)))) "+":".localized()+" \(LocNumbers(key:String(format:"%02d", Minutes))) "+"h".localized())
    }
    
    
    //return "Within " + g.string(from: date) + f.string(from: date) + " Minutes"
}
func getTimeOnly(timeIntervel:TimeInterval) -> String {
    timeIntervel/(1000*60)
    
    return "\(timeIntervel/(1000*60)) Min"
}


func getETA(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D) -> Double {
    //self.mapView.clear()
    
    let origin = "\(source.latitude),\(source.longitude)"
    let destinationn = "\(destination.latitude),\(destination.longitude)"
    
    var  eta : Double = 0 ;
    
    //guard
    let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destinationn)&mode=driving&key=..")
    //        else {
    //        let error = NSError(domain: "LocalDomain", code: 0, userInfo: [NSLocalizedDescriptionKey: "Failed to create object URL"])
    //        print("Error: \(error)")
    //        return
    //    }
    let config = URLSessionConfiguration.default
    let session = URLSession(configuration: config)
    let task = session.dataTask(with: url!, completionHandler: {
        (data, response, error) in
        if error != nil {
            print(error!.localizedDescription)
        }
        else {
            do {
                if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                    
                    guard let routes = json["routes"] as? NSArray else {
                        DispatchQueue.main.async {
                        }
                        return
                    }
                    if (routes.count > 0) {
                        let overview_polyline = routes[0] as? NSDictionary
                        let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                        let points = dictPolyline?.object(forKey: "points") as? String
                        
                        DispatchQueue.main.async {
                            //
                            let legs = overview_polyline?["legs"] as! Array<Dictionary<String, AnyObject>>
                            
                            let distance = legs[0]["distance"] as? NSDictionary
                            let distanceValue = distance?["value"] as? Int ?? 0
                            let distanceDouleValue = distance?["value"] as? Double ?? 0.0
                            let duration = legs[0]["duration"] as? NSDictionary
                            let totalDurationInSeconds = duration?["value"] as? Int ?? 0
                            let durationDouleValue = duration?["value"] as? Double ?? 0.0
                            
                            
                            eta = durationDouleValue ;
                            
                            
                        }
                    }
                    else {
                        print(json)
                        DispatchQueue.main.async {
                            //   SVProgressHUD.dismiss()
                        }
                    }
                }
            }
            catch {
                print("error in JSONSerialization")
                DispatchQueue.main.async {
                    //  SVProgressHUD.dismiss()
                }
            }
        }
    })
    task.resume()
    return eta
}


//
//
//
//func drowRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D) {
//    //self.mapView.clear()
//
//    let origin = "\(source.latitude),\(source.longitude)"
//    let destinationn = "\(destination.latitude),\(destination.longitude)"
//
//    guard let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destinationn)&mode=driving&key=..") else {
//        let error = NSError(domain: "LocalDomain", code: 0, userInfo: [NSLocalizedDescriptionKey: "Failed to create object URL"])
//        print("Error: \(error)")
//        return
//    }
//    let config = URLSessionConfiguration.default
//    let session = URLSession(configuration: config)
//    let task = session.dataTask(with: url, completionHandler: {
//        (data, response, error) in
//        if error != nil {
//            print(error!.localizedDescription)
//        }
//        else {
//            do {
//                if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
//
//                    guard let routes = json["routes"] as? NSArray else {
//                        DispatchQueue.main.async {
//                        }
//                        return
//                    }
//                    if (routes.count > 0) {
//                        let overview_polyline = routes[0] as? NSDictionary
//                        let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
//                        let points = dictPolyline?.object(forKey: "points") as? String
//
//                        DispatchQueue.main.async {
//                            //
//                            let legs = overview_polyline?["legs"] as! Array<Dictionary<String, AnyObject>>
//
//              let distance = legs[0]["distance"] as? NSDictionary
//              let distanceValue = distance?["value"] as? Int ?? 0
//              let distanceDouleValue = distance?["value"] as? Double ?? 0.0
//              let duration = legs[0]["duration"] as? NSDictionary
//              let totalDurationInSeconds = duration?["value"] as? Int ?? 0
//              let durationDouleValue = duration?["value"] as? Double ?? 0.0
//
//                              if(distanceValue != 0) {
//                          self.speed = distanceDouleValue / durationDouleValue
//                                  print("speed", self.speed)
//                                                                          }
//
//                            let miles = Double(distanceValue) / 1609.344
//                            print("\(miles)")
//                            let km = Double(distanceValue) * 0.001609
//                            self.kmLabel.text = ("\(Int(km))" + " " + "KM")
//
//
//
//                            if distanceValue > Int(32186.9){
//
//                            }else{
//                                self.showPath(polyStr: points!)
//
//                                let startLocationDictionary = legs[0]["start_location"] as! Dictionary<String, AnyObject>
//                                let originCoordinate = CLLocationCoordinate2DMake(startLocationDictionary["lat"] as! Double, startLocationDictionary["lng"] as! Double)
//
//                                let endLocationDictionary = legs[legs.count - 1]["end_location"] as! Dictionary<String, AnyObject>
//                                let destinationCoordinate = CLLocationCoordinate2DMake(endLocationDictionary["lat"] as! Double, endLocationDictionary["lng"] as! Double)
//
//                                let marker1 = GMSMarker()
//                                marker1.position = CLLocationCoordinate2D(latitude:destinationCoordinate.latitude, longitude: destinationCoordinate.longitude)
//                                marker1.icon = UIImage(named: "placeholder")
//                                marker1.map = self.mapView
//
//                                let marker2 = GMSMarker()
//                                marker2.position = CLLocationCoordinate2D(latitude:originCoordinate.latitude, longitude: originCoordinate.longitude)
//                                marker2.icon = UIImage(named: "location")
//                                marker2.map = self.mapView
//
//                            }
//
//                        }
//                    }
//                    else {
//                        print(json)
//                        DispatchQueue.main.async {
//                         //   SVProgressHUD.dismiss()
//                        }
//                    }
//                }
//            }
//            catch {
//                print("error in JSONSerialization")
//                DispatchQueue.main.async {
//                  //  SVProgressHUD.dismiss()
//                }
//            }
//        }
//    })
//    task.resume()
//}
//
//func showPath(polyStr :String){
//  //  SVProgressHUD.dismiss()
//    let path = GMSPath(fromEncodedPath: polyStr)
//    let polyline = GMSPolyline(path: path)
//    polyline.strokeWidth = 5.0
//    polyline.strokeColor = UIColor.red
//    polyline.map = mapView
//    DispatchQueue.main.async {
//        let bounds = GMSCoordinateBounds(path: path!)
//        let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 170, left: 30, bottom: 30, right: 30))
//        self.mapView.moveCamera(update)
//
//    }
//}
struct Shake: GeometryEffect {
    var amount: CGFloat = 10
    var shakesPerUnit = 3
    var animatableData: CGFloat
    
    func effectValue(size: CGSize) -> ProjectionTransform {
        ProjectionTransform(CGAffineTransform(translationX:
                                                amount * sin(animatableData * .pi * CGFloat(shakesPerUnit)),
                                              y: 0))
    }
}

//func Localize(key:String) -> String {
//    //    print((UserDefaults.standard.string(forKey: "LANSEL") ?? ""))
//    if((UserDefaults.standard.string(forKey: "LANSEL") ?? "en") != "en"){
//        return key + "_ar"
//    }else{
//        return key
//    }
//}
func LocNumbers(key:String) -> String {
    //    print((UserDefaults.standard.string(forKey: "LANSEL") ?? ""))
    let A = Array(key)
    
    var value = ""
    for k in A {
        value += String(k).localized()
    }
    return key
}
struct RatingViewLarge : View {
    
    var item:Double
    
    var body : some View{
        HStack{
            Image(systemName: item > 0.0 ?  "star.fill" :"star").resizable().frame(width:8,height: 8).foregroundColor(Color.yellow)
            Image(systemName: item > 1.0 ?  "star.fill" :"star").resizable().frame(width:8,height: 8).foregroundColor(Color.yellow)
            Image(systemName: item > 2.0 ?  "star.fill" :"star").resizable().frame(width:8,height: 8).foregroundColor(Color.yellow)
            Image(systemName: item > 3.0 ?  "star.fill" :"star").resizable().frame(width:8,height: 8).foregroundColor(Color.yellow)
            Image(systemName: item > 4.0 ?  "star.fill" :"star").resizable().frame(width:8,height: 8).foregroundColor(Color.yellow)
        }
    }
}
func getTimeComment(key:Double) -> String {
    var k = ""
    switch key {
    case 9999999:
        k = "Closed"
        
        break;
        
    case 10000000:
        k = "Calculating"
        
        break;
        
        
        
        
    default:
        k = getTime(timeIntervel: key)
    }
    return k.localized()
}
