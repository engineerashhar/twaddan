//
//  VehicleSelection.swift
//  Twaddan
//
//  Created by Spine on 29/07/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI

struct VehicleSelection: View {
    @EnvironmentObject var settings : UserSettings
    
    @EnvironmentObject var vehicleItems : VehicleItems
    @Binding var openSPL : Bool
    @Binding var openSP : Bool
    @State var Vibrate : Int = 0
    @State var VEHICLES : [VEHICLETYPES] = [VEHICLETYPES]()
    
    @State var timer: Timer.TimerPublisher = Timer.publish (every: 0.01, on: .main, in: .common)
    @State var V = false
    
    
    
    var body: some View {
        
        
        ZStack{
            ScrollView{
                NavigationLink(destination:ServiceProviders().environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                )).environmentObject(self.settings).environment(\.layoutDirection,UserDefaults.standard.string(forKey: "LANSEL") ?? "en" == "en" ? .leftToRight : .rightToLeft ).environment(\.locale,.init(identifier:UserDefaults.standard.string(forKey: "LANSEL") ?? "en"
                )),isActive: self.$openSPL){
                    
                    EmptyView()
                }
                
                VStack{
                    
                    Spacer().frame(height:100)
                    HStack{
                        Spacer()
                        VStack{
                            Spacer()
                            HStack{
                                Spacer()
                                
                                Button(action: {
                                    self.openSP = false
                                }){
                                    Image(systemName: "x.circle.fill").resizable().frame(width:25,height: 25).padding(.trailing,10).foregroundColor(Color.white)
                                }
                            }
                            
                            Text("Good!, You’re Almost there Now Select your vehicle type").font(.custom("Bold_Font".localized(), size: 14)).foregroundColor(Color.white).multilineTextAlignment(.center).padding(.horizontal,70)
                            
                            Vehicles(VEHICLES: self.$VEHICLES).modifier(Shakes(animatableData: CGFloat(self.Vibrate)))
                                
                                
                                .animation(self.V ? Animation.default.repeatCount(3):nil)
                            
                            Button(action:{
                                if(self.VEHICLES.count>0){
                                    
                                    //                    UserDefaults.standard.set(self.VEHICLES, forKey: "VT")
                                    //                     var VArray : [[String:String]] =  [[String:String]]()
                                    //
                                    //                                        for V in self.VEHICLES{
                                    //                    //                        let Vname = ["name":V.name]
                                    //                    //                        let Vimage = ["image":V.image]
                                    //                                            let Vitem = ["name":V.name,"image":V.image]
                                    //                                            VArray.append(Vitem)
                                    //                                        }
                                    
                                    //                    var VData : [VehicleItems] = [VehicleItems]()
                                    //
                                    //
                                    //                    UserDefaults.standard.set(VData, forKey: "VT")
                                    //
                                    
                                    //                    self.vehicleItems.vehicleItem =  self.VEHICLES
                                    
                                    self.vehicleItems.vehicleItem = AddVehicleData(VEHICLES: self.VEHICLES)
                                    self.openSPL = true
                                    //                                                                         self.openSP.toggle()
                                    //                       self.timer.connect()
                                    print("ppppp")
                                    
                                    self.V = false
                                    
                                    
                                }else{
                                    //                      self.openSPL = true
                                    self.V = true
                                    self.Vibrate += 1
                                }
                            }){
                                HStack{
                                    Text("View Service Providers").font(.custom("Bold_Font".localized(), size: 13)).foregroundColor(Color.white)
                                    
                                    Image(systemName: "arrow.right".localized())
                                        .foregroundColor(Color.white)
                                }.padding(.horizontal).padding(.vertical,20).frame(width:300).background(LinearGradient(gradient: Gradient(colors: [Color("g1"),Color("g2")]), startPoint: .top, endPoint: .bottom)).cornerRadius(20).clipped()
                                
                                
                                
                            }
                            .onReceive(self.timer) { _ in
                                print("ppppp")
                                self.openSPL = true
                                self.openSP.toggle()
                            }
                            
                            
                            
                            Spacer()
                        }
                        Spacer()
                    }
                }
            }
        }.background(Color.clear).onAppear{
            //            self.openSPL = true
            self.VEHICLES.removeAll()
            UserDefaults.standard.set(self.VEHICLES, forKey: "VTYPE")
        }
        
        .edgesIgnoringSafeArea(.all)
    }
    struct Shakes: GeometryEffect {
        var amount: CGFloat = 10
        var shakesPerUnit = 3
        var animatableData: CGFloat
        
        func effectValue(size: CGSize) -> ProjectionTransform {
            ProjectionTransform(CGAffineTransform(translationX:
                                                    amount * sin(animatableData * .pi * CGFloat(shakesPerUnit)),
                                                  y: 0))
        }
    }
    
    
}


func AddVehicleData(VEHICLES:[VEHICLETYPES]) ->  [VEHICLETYPES] {
    var vehicleItems : VehicleItems = VehicleItems()
    
    var VEHICLES2 : [VEHICLETYPES] = [VEHICLETYPES]()
    
    
    vehicleItems.vehicleItem.removeAll()
    for V in VEHICLES{
        let NewV = VEHICLETYPES(id:Int.random(in: 0 ... 10000000000), image: V.image, name: V.name, index: V.index, Vid: V.Vid)
        vehicleItems.vehicleItem.append(NewV)
    }
    
    return vehicleItems.vehicleItem
    
    
}


struct VehicleSelection_Previews: PreviewProvider {
    static var previews: some View {
        VehicleSelection( openSPL: .constant(false), openSP: .constant(false))
    }
}
//class VehicleItems {
//
//      var name : String = ""
//      var image : String = ""
//      var id : String = ""
//      
//
//  }
