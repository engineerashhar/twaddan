//
//  MiddlePage.swift
//  Twaddan
//
//  Created by Spine on 26/09/20.
//  Copyright © 2020 spine. All rights reserved.
//

import SwiftUI

struct MiddlePage: View {
    
    @EnvironmentObject var vehicleItems : VehicleItems
    @EnvironmentObject var settings : UserSettings
    
    var body: some View {
        HomePage().environmentObject(self.vehicleItems).environmentObject(self.settings)
    }
}

struct MiddlePage_Previews: PreviewProvider {
    static var previews: some View {
        MiddlePage()
    }
}
